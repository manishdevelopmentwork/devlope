
#include "Intern.hpp"

#include "Expansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers 
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IExpansionInterface * Create_UsbModemExpansion     (IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbCanExpansion       (IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbDevNetExpansion    (IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbProfibusExpansion  (IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbModuleExpansion    (IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbRackExpansion      (IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbDongleExpansion    (IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbGenericExpansion   (IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbModuleExpansion    (IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbNetworkCdcExpansion(IUsbHostFuncDriver *pDriver);
extern IExpansionInterface * Create_UsbSerialCdcExpansion (IUsbHostFuncDriver *pDriver);
extern IUsbHostFuncEvents  * Create_UsbModuleBoot         (IUsbHostFuncDriver *pDriver, IExpansionFirmware *pFirmware);

//////////////////////////////////////////////////////////////////////////
//
// Expansion System 
//

IExpansionSystem * Create_Expansion(void)
{
	return New CExpansion;
	}

// Constructor

CExpansion::CExpansion(void)
{
	StdSetRef();

	// TODO -- Locate system objects here!

	m_pMutex = Create_Mutex();

	m_pSink  = NULL;

	m_pFirm  = NULL;

	m_pPower = NULL;

	InitSlots();

	DiagRegister();
	}

// Destructor

CExpansion::~CExpansion(void)
{
	DiagRevoke();

	m_pMutex->Release();

	FreeSlots();
	}

// IUnknown

HRESULT METHOD CExpansion::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IExpansionSystem);

	StdQueryInterface(IExpansionSystem);

	StdQueryInterface(IExpansionFirmware);

	StdQueryInterface(IUsbSystem);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG METHOD CExpansion::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CExpansion::Release(void)
{
	StdRelease();
	}

// Expansion System

void METHOD CExpansion::Attach(IExpansionEvents *pSink)
{
	m_pSink = pSink;
	}

void METHOD CExpansion::Attach(IExpansionFirmware *pFirm)
{
	m_pFirm = pFirm;
	}

void METHOD CExpansion::Attach(IUsbSystemPower *pPower)
{
	m_pPower = pPower;
	}

UINT METHOD CExpansion::GetSlotCount(void)
{
	return elements(m_Slots);
	}

UINT METHOD CExpansion::FindSlot(UsbTreePath const &Path)
{
	#if defined(_XDEBUG)	

	AfxTrace("CExpansion::FindSlot(%d:%d:%d:%d:%d:%d:%d:%d:%d)\n",
		 Path.a.dwCtrl,
		 Path.a.dwHost,
		 Path.a.dwTier,
		 Path.a.dwPort1,
		 Path.a.dwPort2,
		 Path.a.dwPort3,
		 Path.a.dwPort4,
		 Path.a.dwPort5,
		 Path.a.dwPort6
	);

	#endif

	return RegisterSlot(Path);
	}

BOOL METHOD CExpansion::WaitSlot(UINT iSlot, UINT uTimeout)
{
	// LATER -- Can we create the port object immediately and defer binding to the PNP event.

	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		if( s.m_pEvent->Wait(uTimeout) ) {

			#if defined(_XDEBUG)	

			AfxTrace("CExpansion::WaitSlot(%d:%d:%d:%d:%d:%d:%d:%d:%d) Done.\n",
				 s.m_Path.a.dwCtrl,
				 s.m_Path.a.dwHost,
				 s.m_Path.a.dwTier,
				 s.m_Path.a.dwPort1,
				 s.m_Path.a.dwPort2,
				 s.m_Path.a.dwPort3,
				 s.m_Path.a.dwPort4,
				 s.m_Path.a.dwPort5,
				 s.m_Path.a.dwPort6
			);

			#endif

			CheckFirmware(iSlot);

			return TRUE;
			}
		}

	return FALSE;
	}

IUsbDevice * METHOD CExpansion::GetDevice(UINT iSlot)
{
	return iSlot < elements(m_Slots) ? m_Slots[iSlot].m_pDevice : NULL;
	}

IExpansionInterface * METHOD CExpansion::GetInterface(UINT iSlot, UINT iInterface)
{
	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		if( iInterface < elements(s.m_Interface) ) {

			return s.m_Interface[iInterface].m_pExp;
			}
		}

	return NULL;
	}

IUsbHostFuncDriver * METHOD CExpansion::GetFuncDriver(UINT iSlot, UINT iInterface)
{
	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		if( iInterface < elements(s.m_Interface) ) {

			return s.m_Interface[iInterface].m_pDrv;
			}
		}

	return NULL;
	}

PVOID METHOD CExpansion::GetAppObject(UINT iSlot, UINT iInterface)
{
	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		if( iInterface < elements(s.m_Interface) ) {

			CInterface &i = s.m_Interface[iInterface];

			for(;;) {

				GuardThread(true);

				LockAppObject(i);

				if( i.m_pObj ) {

					AtomicIncrement(&i.m_nApp);

					GuardThread(false);

					return i.m_pObj;
					}

				FreeAppObject(i);

				GuardThread(false);

				Sleep(50);
				}
			}
		}

	return NULL;
	}

void METHOD CExpansion::FreeAppObject(UINT iSlot, UINT iInterface)
{
	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		if( iInterface < elements(s.m_Interface) ) {

			CInterface &i = s.m_Interface[iInterface];

			GuardThread(true);

			AtomicDecrement(&i.m_nApp);

			FreeAppObject(i);

			GuardThread(false);
			}
		}
	}

void METHOD CExpansion::FreeAppObjects(void)
{
	for( UINT iSlot = 0; iSlot < elements(m_Slots); iSlot ++ ) {

		CSlot &s = m_Slots[iSlot];

		for( UINT iInt = 0; iInt < elements(s.m_Interface); iInt ++ ) {

			CInterface &i = s.m_Interface[iInt];

			while( i.m_nApp ) {

				AtomicDecrement(&i.m_nApp);

				FreeAppObject(i);
				}
			}
		}
	}

PVOID METHOD CExpansion::GetSysObject(UINT iSlot, UINT iInterface)
{
	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		if( iInterface < elements(s.m_Interface) ) {

			CInterface &i = s.m_Interface[iInterface];

			for(;;) {

				LockAppObject(i);

				if( i.m_pObj ) {

					AtomicIncrement(&i.m_nSys);

					return i.m_pObj;
					}

				FreeAppObject(i);

				Sleep(50);
				}
			}
		}

	return NULL;
	}

void METHOD CExpansion::FreeSysObject(UINT iSlot, UINT iInterface)
{
	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		if( iInterface < elements(s.m_Interface) ) {

			CInterface &i = s.m_Interface[iInterface];

			AtomicDecrement(&i.m_nSys);

			FreeAppObject(i);
			}
		}
	}

// Firmware

BOOL METHOD CExpansion::SetFirmwareLock(BOOL fSet)
{
	return m_pFirm ? m_pFirm->SetFirmwareLock(fSet) : FALSE;
	}

PCBYTE METHOD CExpansion::GetFirmwareGuid(UINT uFirm)
{
	return m_pFirm ? m_pFirm->GetFirmwareGuid(uFirm) : NULL;
	}

UINT METHOD CExpansion::GetFirmwareSize(UINT uFirm)
{
	return m_pFirm ? m_pFirm->GetFirmwareSize(uFirm) : 0;
	}

PCBYTE METHOD CExpansion::GetFirmwareData(UINT uFirm)
{
	return m_pFirm ? m_pFirm->GetFirmwareData(uFirm) : NULL;
	}

// IUsbSystem

void METHOD CExpansion::OnDeviceArrival(IUsbHostFuncDriver *pDriver)
{
	#if defined(_XDEBUG)	

	AfxTrace("CExpansion::OnDeviceArrival(Vendor=0x%4.4X, Product=0x%4.4X, Class=0x%2.2X, Sub=0x%2.2X, Proto=0x%2.2X, Path=0x%8.8X)\n",
		 pDriver->GetVendor(),
		 pDriver->GetProduct(),
		 pDriver->GetClass(),
		 pDriver->GetSubClass(),
		 pDriver->GetProtocol(),
		 GetDevice(pDriver)->GetTreePath()
	);

	#endif

	if( pDriver->GetVendor() == vidRedLion ) {

		switch( pDriver->GetProduct() ) {

			case pidPid1:
			case pidPid2:
			case pidUin4:
			case pidOut4:
			case pidDio14:
			case pidTc8:
			case pidIni8:
			case pidInv8:
			case pidRtd6:
			case pidSg1:

				OnNewModule(pDriver);

				return;
			}

		switch( pDriver->GetClass() ) {

			case devHub:

				break;

			case devHid:

				break;

			case devComms:

				break;

			case devVendor:

				switch( pDriver->GetSubClass() ) {

					case subclassBoot:

						OnNewLoader(pDriver);

						return;

					case subclassDongle:

						OnNewDongle(pDriver);

						return;

					case subclassDriver:

						OnNewRemote(pDriver);

						return;

					case subclassRack:

						OnNewModule(pDriver);

						return;

					case subclassComms:

						switch( pDriver->GetProtocol() ) {

							case protoSerial:

								OnNewSerial(pDriver);

								return;

							case protoCan:

								OnNewCanBus(pDriver);

								return;

							case protoDevNet:

								OnNewDevNet(pDriver);

								return;

							case protoProfibus:

								OnNewProfibus(pDriver);

								return;

							case protoJ1939:

								OnNewCanBus(pDriver);

								return;
							}

						break;
						}

				return;
			}
		}

	switch( pDriver->GetClass() ) {

		case devHub:

			if( pDriver->GetVendor() == vidSmc ) {

				if( pDriver->GetProduct() == pidSmcHub2514 ) {

					OnNewRack(pDriver);

					return;
					}
				}

			break;

		case devHid:

			break;

		case devComms:

			if( pDriver->GetSubClass() == 0x02 && pDriver->GetProtocol() == 0x01 ) {

				OnNewModem(pDriver);

				return;
				}

			if( pDriver->GetSubClass() == 0x06 && pDriver->GetProtocol() == 0x00 ) {

				OnNewNetwork(pDriver);

				return;
				}

			if( pDriver->GetSubClass() == 0x0D && pDriver->GetProtocol() == 0x00 ) {

				OnNewNetwork(pDriver);

				return;
				}

			break;
		}
	}

void METHOD CExpansion::OnDeviceRemoval(IUsbHostFuncDriver *pDriver)
{
	#if defined(_XDEBUG)	

	AfxTrace("CExpansion::OnDeviceRemoval(Vendor=0x%4.4X, Product=0x%4.4X)\n",
		 pDriver->GetVendor(),
		 pDriver->GetProduct()
	);

	#endif

	if( pDriver->GetVendor() == vidRedLion ) {

		switch( pDriver->GetProduct() ) {

			case pidPid1:
			case pidPid2:
			case pidUin4:
			case pidOut4:
			case pidDio14:
			case pidTc8:
			case pidIni8:
			case pidInv8:
			case pidRtd6:
			case pidSg1:

				OnDelModule(pDriver);

				return;
			}

		switch( pDriver->GetClass() ) {

			case devHub:

				break;

			case devHid:

				break;

			case devComms:

				break;

			case devVendor:

				switch( pDriver->GetSubClass() ) {

					case subclassBoot:

						OnDelLoader(pDriver);

						return;

					case subclassDongle:

						OnDelDongle(pDriver);

						return;
						
					case subclassDriver:

						OnDelRemote(pDriver);

						return;

					case subclassRack:

						OnDelModule(pDriver);

						return;

					case subclassComms:

						switch( pDriver->GetProtocol() ) {

							case protoSerial:

								OnDelSerial(pDriver);

								return;

							case protoCan:

								OnDelCanBus(pDriver);

								return;

							case protoDevNet:

								OnDelDevNet(pDriver);

								return;

							case protoProfibus:

								OnDelProfibus(pDriver);

								return;

							case protoJ1939:

								OnDelCanBus(pDriver);

								return;
							}

						break;
					}

				return;
			}
		}

	switch( pDriver->GetClass() ) {

		case devHub:

			if( pDriver->GetVendor() == vidSmc ) {

				if( pDriver->GetProduct() == pidSmcHub2514 ) {

					OnDelRack(pDriver);

					return;
					}
				}

			break;

		case devHid:

			break;

		case devComms:

			if( pDriver->GetSubClass() == 0x02 && pDriver->GetProtocol() == 0x01 ) {

				OnDelModem(pDriver);

				return;
				}

			if( pDriver->GetSubClass() == 0x06 && pDriver->GetProtocol() == 0x00 ) {

				OnDelNetwork(pDriver);

				return;
				}

			if( pDriver->GetSubClass() == 0x0D && pDriver->GetProtocol() == 0x00 ) {

				OnDelNetwork(pDriver);

				return;
				}

			break;
		}
	}

// IDiagProvider

UINT METHOD CExpansion::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);
		}

	return 0;
	}

// Object Creation / Destruction

void CExpansion::OnNewSerial(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbSerialCdcExpansion(pDriver));
	}

void METHOD CExpansion::OnDelSerial(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

void CExpansion::OnNewModem(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbModemExpansion(pDriver));
	}

void CExpansion::OnDelModem(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

void CExpansion::OnNewCanBus(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbCanExpansion(pDriver));
	}

void CExpansion::OnDelCanBus(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

void CExpansion::OnNewDevNet(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbDevNetExpansion(pDriver));
	}

void CExpansion::OnDelDevNet(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

void CExpansion::OnNewProfibus(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbProfibusExpansion(pDriver));
	}

void CExpansion::OnDelProfibus(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

void CExpansion::OnNewModule(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbModuleExpansion(pDriver));
	}

void CExpansion::OnDelModule(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

void CExpansion::OnNewLoader(IUsbHostFuncDriver *pDriver)
{
	Create_UsbModuleBoot(pDriver, (IExpansionFirmware *) this);
	}

void CExpansion::OnDelLoader(IUsbHostFuncDriver *pDriver)
{
	}

void CExpansion::OnNewRack(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbRackExpansion(pDriver));
	}

void CExpansion::OnDelRack(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

void CExpansion::OnNewDongle(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbDongleExpansion(pDriver));
	}

void CExpansion::OnDelDongle(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

void CExpansion::OnNewRemote(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbGenericExpansion(pDriver));
	}

void CExpansion::OnDelRemote(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

void CExpansion::OnNewWiFi(IUsbHostFuncDriver *pDriver)
{
	}

void CExpansion::OnDelWiFi(IUsbHostFuncDriver *pDriver)
{
	}

void CExpansion::OnNewNetwork(IUsbHostFuncDriver *pDriver)
{
	RegisterDevice(pDriver, Create_UsbNetworkCdcExpansion(pDriver));
	}

void CExpansion::OnDelNetwork(IUsbHostFuncDriver *pDriver)
{
	UnregisterDevice(pDriver);
	}

// Implementation

IUsbDevice * CExpansion::GetDevice(IUsbHostFuncDriver *pDriver)
{
	IUsbDevice *pDev = NULL;

	pDriver->GetDevice(pDev);

	return pDev;
	}

void CExpansion::RegisterDevice(IUsbHostFuncDriver *pDrv, IExpansionInterface *pExp)
{
	IUsbDevice *pDev = GetDevice(pDrv);

	UINT iSlot = RegisterSlot(pDev->GetTreePath());

	UINT iInt  = pDrv->GetInterface();

	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		s.m_pDevice = pDev;

		if( iInt < elements(s.m_Interface) ) {

			CInterface &Int = s.m_Interface[iInt];

			Int.m_pExp = pExp;

			Int.m_pDrv = pDrv;

			LockAppObject(Int);

			if( Int.m_pObj == NULL ) {

				Int.m_uPid = pExp->GetIdent();

				Int.m_pObj = pExp->MakeObject(pDrv);

				Int.m_pPnp = pExp->QueryPnpInterface(Int.m_pObj);
				}
			else {
				if( Int.m_uPid == pExp->GetIdent() ) {

					if( Int.m_pPnp ) {

						Int.m_pPnp->OnDeviceArrival(pDrv);
						}
					}
				}

			pDev->SetObject(iInt, Int.m_pObj);

			if( !s.m_uActive ) {

				RegisterPower(pDev, pExp);
				}

			if( ++s.m_uActive == pDev->GetDriverCount() ) {

				s.m_uState = statePresent;

				s.m_pEvent->Set();
				}

			if( m_pSink ) {

				m_pSink->OnDeviceArrival(iSlot, iInt);
				}
			}
		}
	}

void CExpansion::UnregisterDevice(IUsbHostFuncDriver *pDrv)
{
	// TODO - Shouldn't we just do this once per driver call?

	IUsbDevice *pDev = GetDevice(pDrv);

	UINT iSlot = FindRegisteredSlot(pDev->GetTreePath());

	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		if( s.m_pDevice ) {

			s.m_uState = stateRemoved;

			for( UINT i = 0; i < elements(s.m_Interface); i++ ) {

				CInterface &Int = s.m_Interface[i];

				Int.m_pDrv = NULL;

				if( Int.m_pExp ) {

					if( !--s.m_uActive ) {

						UnregisterPower(s.m_pDevice, Int.m_pExp);
						}

					if( m_pSink ) {

						m_pSink->OnDeviceRemoval(iSlot, i);
						}

					if( Int.m_pPnp ) {

						Int.m_pPnp->OnDeviceRemoval(pDrv);
						}

					Int.m_pExp->Release();

					Int.m_pExp = NULL;

					FreeAppObject(Int);
					}
				}

			s.m_pEvent->Clear();

			s.m_pDevice = NULL;

			s.m_uActive = 0;
			}
		}
	}

void CExpansion::CheckFirmware(UINT iSlot)
{
	if( iSlot < elements(m_Slots) ) {

		CSlot &s = m_Slots[iSlot];

		if( s.m_Interface[0].m_pExp->HasBootLoader() ) {

			s.m_pMutex->Wait(FOREVER);

			s.m_pEvent->Wait(FOREVER);

			IUsbHostModule *pDriver = (IUsbHostModule *) s.m_Interface[0].m_pDrv;

			PCBYTE pGuid = GetFirmwareGuid(pDriver->GetProduct());

			if( pGuid ) {

				BYTE bGuid[16];

				if( pDriver->ReadVersion(bGuid) ) {

					/*AfxDump(bGuid, 16);*/

					/*AfxDump(pGuid, 16);*/

					if( memcmp(bGuid, pGuid, 16) ) {

						s.m_pEvent->Clear();

						pDriver->Reset();

						s.m_pEvent->Wait(FOREVER);
						}
					}
				}

			s.m_pMutex->Free();
			}
		}
	}

// Slot Management

void CExpansion::InitSlots(void)
{
	memset(&m_Slots, 0, sizeof(m_Slots));

	for( UINT i = 0; i < elements(m_Slots); i++ ) {

		m_Slots[i].m_pEvent = Create_ManualEvent();

		m_Slots[i].m_pMutex = Create_Mutex();
		}
	}

void CExpansion::FreeSlots(void)
{
	for( UINT i = 0; i < elements(m_Slots); i++ ) {

		m_Slots[i].m_pEvent->Release();

		m_Slots[i].m_pMutex->Release();
		}

	memset(&m_Slots, 0, sizeof(m_Slots));
	}

UINT CExpansion::FindRegisteredSlot(UsbTreePath const &Path)
{
	for( UINT i = 0; i < elements(m_Slots); i++ ) {

		CSlot &s = m_Slots[i];

		if( s.m_Path.dw == Path.dw ) {

			return i;
			}
		}

	return NOTHING;
	}

UINT CExpansion::RegisterSlot(UsbTreePath const &Path)
{
	m_pMutex->Wait(FOREVER);

	UINT iSlot = FindRegisteredSlot(Path);

	if( iSlot == NOTHING ) {

		for( UINT i = 0; i < elements(m_Slots); i++ ) {

			CSlot &s = m_Slots[i];

			if( s.m_uState == stateFree ) {

				s.m_uState  = stateUsed;

				s.m_Path.dw = Path.dw;

				s.m_pDevice = NULL;

				s.m_pEvent->Clear();

				memset(&s.m_Interface, sizeof(s.m_Interface), 0);

				m_pMutex->Free();

				return i;
				}
			}
		}

	m_pMutex->Free();

	return iSlot;
	}

void CExpansion::UnRegisterSlot(UsbTreePath const &Path)
{
	for( UINT i = 0; i < elements(m_Slots); i++ ) {

		CSlot &s = m_Slots[i];

		if( s.m_Path.dw == Path.dw ) {

			s.m_Path.dw = 0;

			s.m_pDevice = NULL;

			s.m_uState  = stateFree;
			}
		}
	}

// Power Management

void CExpansion::RegisterPower(IUsbDevice *pDevice, IExpansionInterface *pExpansion)
{
	UINT iSlot = FindParentRack(pDevice);

	if( iSlot != NOTHING ) {

		IExpansionRack *pRack = (IExpansionRack *) m_Slots[iSlot].m_Interface[0].m_pExp;

		pRack->RegisterPower(pExpansion);
		}

	if( m_pPower ) {

		m_pPower->OnNewDevice(pDevice->GetTreePath(), pExpansion->GetPower());
		}
	}

void CExpansion::UnregisterPower(IUsbDevice *pDevice, IExpansionInterface *pExpansion)
{
	UINT iSlot = FindParentRack(pDevice);

	if( iSlot != NOTHING ) {

		IExpansionRack *pRack = (IExpansionRack *) m_Slots[iSlot].m_Interface[0].m_pExp;

		pRack->UnregisterPower(pExpansion);
		}

	if( m_pPower ) {

		m_pPower->OnDelDevice(pDevice->GetTreePath(), pExpansion->GetPower());
		}
	}

UINT CExpansion::FindParentRack(IUsbDevice *pDevice)
{
	UINT uHubAddr = pDevice->GetPortPath().a.dwHubAddr;

	if( uHubAddr ) {

		UINT uCtrl = pDevice->GetPortPath().a.dwCtrl;

		UINT uHost = pDevice->GetPortPath().a.dwHost;

		for( UINT i = 0; i < elements(m_Slots); i++ ) {

			CSlot &s = m_Slots[i];

			if( s.m_uState == statePresent ) {

				if( s.m_Path.a.dwCtrl == uCtrl && s.m_Path.a.dwHost == uHost ) {

					if( s.m_pDevice->GetAddr() == uHubAddr ) {

						if( s.m_Interface[0].m_pExp->GetClass() == rackClassRack ) {

							return i;
							}
						}
					}
				}
			}
		}

	return NOTHING;
	}

// Lifetime Management

void CExpansion::LockAppObject(CInterface &Interface)
{
	m_pMutex->Wait(FOREVER);

	Interface.m_nRef++;

	m_pMutex->Free();
	}

void CExpansion::FreeAppObject(CInterface &Interface)
{
	m_pMutex->Wait(FOREVER);

	Interface.m_nRef--;

	if( Interface.m_nRef == 0 ) {

		Interface.m_nApp = 0;

		Interface.m_nSys = 0;

		if( Interface.m_pObj ) {

			IDevice *p = Interface.m_pObj;

			AfxRelease(Interface.m_pPnp);

			Interface.m_pObj = NULL;

			Interface.m_pPnp = NULL;

			m_pMutex->Free();

			p->Release();

			return;
			}
		}

	m_pMutex->Free();
	}

// Diagnostics

bool CExpansion::DiagRegister(void)
{
	#if defined(_DEBUG)	

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "rack");

		pDiag->RegisterCommand(m_uProv, 1, "status");

		return true;
		}

	#endif

	return false;
	}

bool CExpansion::DiagRevoke(void)
{
	#if defined(_DEBUG)	

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return true;
		}

	#endif

	return false;
	}

UINT CExpansion::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(9);

		pOut->SetColumn(0, "Slot", "%u");
		pOut->SetColumn(1, "Path", "%8.8X");
		pOut->SetColumn(2, "State", "%s");
		pOut->SetColumn(3, "Addr", "%u");
		pOut->SetColumn(4, "Name", "%s");
		pOut->SetColumn(5, "Index", "%u");
		pOut->SetColumn(6, "Ref", "%u");
		pOut->SetColumn(7, "App", "%u");
		pOut->SetColumn(8, "Sys", "%u");

		static PCTXT pState[3] = { "USED", "PRESENT", "REMOVED" };

		pOut->AddHead();

		pOut->AddRule('-');

		for( UINT i = 0; i < elements(m_Slots); i++ ) {

			CSlot &s = m_Slots[i];

			if( s.m_uState != stateFree ) {

				for( UINT n = 0; n < elements(s.m_Interface); n++ ) {

					if( !n || s.m_Interface[n].m_pExp ) {

						pOut->AddRow();

						if( !n ) {

							pOut->SetData(0, i);
							pOut->SetData(1, s.m_Path.dw);
							pOut->SetData(2, pState[s.m_uState-1]);

							if( s.m_pDevice ) {

								pOut->SetData(3, s.m_pDevice->GetAddr());
								}
							}

						if( s.m_Interface[n].m_pExp ) {

							pOut->SetData(4, s.m_Interface[n].m_pExp->GetName());
							}

						pOut->SetData(5, n);
						pOut->SetData(6, s.m_Interface[n].m_nRef);
						pOut->SetData(7, s.m_Interface[n].m_nApp);
						pOut->SetData(8, s.m_Interface[n].m_nSys);

						pOut->EndRow();
						}
					}
				}
			}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

// End of File
