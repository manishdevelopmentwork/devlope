
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3WEB_HPP

#define	INCLUDE_G3WEB_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <g3comms.hpp>

#include <cxhttp.hpp>

// End of File

#endif
