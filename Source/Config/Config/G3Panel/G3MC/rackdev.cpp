
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device View
//

class CCommsDeviceRackWnd : public CProxyViewWnd
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CCommsDeviceRackWnd(void);

protected:
	// Overridables
	void OnAttach(void);
};

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device View
//

// Dynamic Class

AfxImplementDynamicClass(CCommsDeviceRackWnd, CProxyViewWnd);

// Constructor

CCommsDeviceRackWnd::CCommsDeviceRackWnd(void)
{
	m_fRecycle = FALSE;
}

// Overridables

void CCommsDeviceRackWnd::OnAttach(void)
{
	CCommsDeviceRack *pDev = (CCommsDeviceRack *) m_pItem;

	CGenericModule   *pMod = pDev->m_pModule;

	m_pView = pMod->CreateView(viewItem);

	m_pView->Attach(pMod);
}

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device List
//

// Dynamic Class

AfxImplementDynamicClass(CCommsDeviceRackList, CCommsDeviceList);

// Constructor

CCommsDeviceRackList::CCommsDeviceRackList(void)
{
}

// Persistance

void CCommsDeviceRackList::Init(void)
{
	CCommsDeviceList::Init();

	FindSlots();
}

void CCommsDeviceRackList::Load(CTreeFile &File)
{
	CCommsDeviceList::Load(File);
}

void CCommsDeviceRackList::PostLoad(void)
{
	CCommsDeviceList::PostLoad();

	FindSlots();
}

void CCommsDeviceRackList::Save(CTreeFile &File)
{
	CCommsDeviceList::Save(File);

	FindSlots();
}

// Operations

void CCommsDeviceRackList::SaveFixed(CTreeFile &File)
{
	File.PutValue(L"Fixed", m_Fixed);

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		CItem *pItem = m_List[Index];

		CCommsDeviceRack *pDev = (CCommsDeviceRack *) pItem;

		CCommsPortSlot  *pSlot = m_pSlots->FindItem(pDev);

		if( !pSlot || pSlot->m_Rack == NOTHING ) {

			if( !m_Class ) {

				CString Name = pItem->GetClassName();

				File.PutObject(Name.Mid(1));
			}
			else
				File.PutElement();

			File.SetPadding(pItem->GetPadding());

			File.PutValue(L"NDX", pItem->GetIndex());

			pItem->Save(File);

			File.EndObject();
		}

		m_List.GetNext(Index);
	}
}

void CCommsDeviceRackList::SaveTether(CTreeFile &File)
{
	File.PutValue(L"Fixed", m_Fixed);

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		CItem *pItem = m_List[Index];

		AfxAssert(m_Class);

		CCommsDeviceRack *pDev = (CCommsDeviceRack *) pItem;

		CCommsPortSlot  *pSlot = m_pSlots->FindItem(pDev);

		if( pSlot && pSlot->m_Rack != NOTHING ) {

			if( !m_Class ) {

				CString Name = pItem->GetClassName();

				File.PutObject(Name.Mid(1));
			}
			else
				File.PutElement();

			File.SetPadding(pItem->GetPadding());

			File.PutValue(L"NDX", pItem->GetIndex());

			pItem->Save(File);

			File.EndObject();
		}

		m_List.GetNext(Index);
	}
}

// Implementation

void CCommsDeviceRackList::FindSlots(void)
{
	CCommsPortRack *pRack = (CCommsPortRack *) GetParent(AfxRuntimeClass(CCommsPortRack));

	m_pSlots = pRack->m_pSlots;
}

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device
//

// Dynamic Class

AfxImplementDynamicClass(CCommsDeviceRack, CCommsDevice);

// Constructor

CCommsDeviceRack::CCommsDeviceRack(void)
{
	m_pModule = NULL;
}

// UI Creation

CViewWnd * CCommsDeviceRack::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return New CCommsDeviceRackWnd;
	}

	return NULL;
}

// Operations

void CCommsDeviceRack::SetClass(CLASS Class)
{
	m_pModule = AfxNewObject(CGenericModule, Class);

	m_pModule->SetParent(this);

	m_pModule->Init();
}

// Naming

CString CCommsDeviceRack::GetHumanName(void) const
{
	CString Name = m_Name;

	Name += L" - ";

	if( m_pModule ) {

		CString Model = m_pModule->m_Model;

		C3OemStrings(Model);

		Name += Model;
	}

	return Name;
}

CString CCommsDeviceRack::GetItemOrdinal(void) const
{
	return L"";
}

// Persistance

void CCommsDeviceRack::Init(void)
{
	CCommsManager *pManager;

	CCodedHost::Init();

	UpdateDriver();

	pManager = (CCommsManager *) GetParent(AfxRuntimeClass(CCommsManager));

	m_Number = pManager->AllocDeviceNumber();
}

void CCommsDeviceRack::PostPaste(void)
{
	CCodedHost::PostPaste();

	UpdateDriver();

	CCommsManager *pManager = (CCommsManager *) GetParent(AfxRuntimeClass(CCommsManager));

	m_Number = pManager->AllocDeviceNumber();
}

// Conversion

BOOL CCommsDeviceRack::PostConvert(CString const &Conv)
{
	CLASS Class = AfxNamedClass(Conv);

	if( AfxPointerClass(m_pModule) != Class ) {

		CPropConverter Converter;

		Converter.LoadFromItem(m_pModule);

		SetClass(Class);

		CPropValue   *pRoot = &Converter.m_Root;

		m_pModule->Convert(pRoot);

		return TRUE;
	}

	return FALSE;
}

// Meta Data Creation

void CCommsDeviceRack::AddMetaData(void)
{
	CCommsDevice::AddMetaData();

	Meta_AddVirtual(Module);

	Meta_SetName((IDS_MODULE_MODULE));
}

// End of File
