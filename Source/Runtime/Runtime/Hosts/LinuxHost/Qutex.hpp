
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Qutex_HPP

#define INCLUDE_Qutex_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Waitable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Quick Mutex Object
//

class CQutex : public CWaitable, public IMutex
{
public:
	// Constructor
	CQutex(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IWaitable
	PVOID METHOD GetWaitable(void);
	BOOL  METHOD Wait(UINT uTime);
	BOOL  METHOD HasRequest(void);

	// IMutex
	void METHOD Free(void);

protected:
	// Data Members
	ULONG m_uRefs;
	int   m_count;
	int   m_guard;
};

// End of File

#endif
