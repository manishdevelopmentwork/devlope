
#include "Intern.hpp"

#include "DevConPartHardware.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevCon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Hardware Part
//

// Base Class

#define CBaseClass CDevConPart

// Runtime Class

AfxImplementRuntimeClass(CDevConPartHardware, CBaseClass);

// Constructor

CDevConPartHardware::CDevConPartHardware(void) : CDevConPart('h')
{
	m_uChange = NOTHING;

	m_pActive = New CJsonData;

	m_Level   = 0;
}

// Destructor

CDevConPartHardware::~CDevConPartHardware(void)
{
	delete m_pActive;
}

// Change Check

BOOL CDevConPartHardware::HasChanged(void)
{
	if( m_uChange == NOTHING ) {

		if( m_Active.CompareC(m_Config) ) {

			m_uChange = 1;
		}
		else
			m_uChange = 0;
	}

	return m_uChange == 1;
}

// Attributes

CJsonData * CDevConPartHardware::GetActive(void) const
{
	return m_pActive;
}

UINT CDevConPartHardware::GetSoftwareGroup(void) const
{
	return watoi(m_pActive->GetValue(L"general.group", L"0"));
}

// Operations

void CDevConPartHardware::ImportFromJson(void)
{
	m_Config  = m_pConfig->GetAsText(FALSE);

	m_Active  = m_Config;

	m_uChange = 0;

	m_pActive->Parse(m_Active);
}

void CDevConPartHardware::Commit(void)
{
	m_Active  = m_Config;

	m_uChange = 0;

	m_pActive->Parse(m_Active);

	m_pDevCon->UpdateHardware();
}

void CDevConPartHardware::Revert(void)
{
	m_Config  = m_Active;

	m_uChange = NOTHING;

	m_pConfig->Parse(m_Config);
}

void CDevConPartHardware::SetSoftwareGroup(UINT uGroup)
{
	// Only used when importing databases! Do not try
	// and set the group using the method otherwise.

	m_pActive->AddValue(L"general.group", CPrintf(L"%u", uGroup), jsonString);
}

// Persistance

void CDevConPartHardware::Init(void)
{
	CBaseClass::Init();

	m_Active = m_Config;

	m_pActive->Parse(m_Active);

	m_Level = 1;
}

void CDevConPartHardware::Load(CTreeFile &Tree)
{
	CBaseClass::Load(Tree);

	m_pActive->Parse(m_Active);
}

void CDevConPartHardware::PostLoad(void)
{
	if( !m_Level ) {

		if( FixList(m_Active, L"sleds.slist", L"{\"type\":\"0\"}") ) {

			m_pActive->Parse(m_Active);

			FixList(m_Config, L"sleds.slist", L"{\"type\":\"0\"}");
		}

		m_Level = 1;
	}

	CBaseClass::PostLoad();
}

// Active Config

CString const & CDevConPartHardware::GetInitJson(void)
{
	return m_Active;
}

// Meta Data Creation

void CDevConPartHardware::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddString(Active);
	Meta_AddInteger(Level);

	Meta_SetName(IDS("Hardware Configuration"));
}

// Dirty Control

void CDevConPartHardware::OnSetDirty(void)
{
	m_Config  = m_pConfig->GetAsText(FALSE);

	m_uChange = NOTHING;

	CBaseClass::OnSetDirty();
}

// Implementation

BOOL CDevConPartHardware::FixList(CString &Data, CString const &Name, CString const &Null)
{
	CJsonData Json;

	if( Json.Parse(Data) ) {

		CJsonData *pList = Json.GetChild(Name);

		if( pList ) {

			if( !pList->IsList() ) {

				// This is a broken database where we created the sled list
				// as an object rather than as a list. We need to read it
				// and then convert it to to the correct format.

				CStringArray List;

				for( INDEX i = pList->GetHead(); !pList->Failed(i); pList->GetNext(i) ) {

					UINT n = watoi(pList->GetName(i));

					while( List.GetCount() < n ) {

						List.Append(Null);
					}

					List.Append(pList->GetChild(i)->GetAsText(FALSE));
				}

				if( Json.Delete(Name) ) {

					Json.AddChild(Name, TRUE, pList);

					for( UINT n = 0; n < List.GetCount(); n++ ) {

						CJsonData *pRow;

						pList->AddChild(FALSE, pRow);

						pRow->Parse(List[n]);
					}

					Data = Json.GetAsText(TRUE);

					AfxTrace(L"Fixed %s\n", PCTXT(Name));

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// End of File
