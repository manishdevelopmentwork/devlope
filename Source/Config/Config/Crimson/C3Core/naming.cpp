
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// API for Name Validation
//

DLLAPI BOOL C3ValidateName(CError &Error, CString Name)
{
	if( Name.IsEmpty() ) {

		Error.Set(IDS_NAME_EMPTY);

		return FALSE;
		}

	if( Name.GetLength() > 32 ) {

		Error.Set(IDS_NAME_LONG);

		return FALSE;
		}

	for( UINT n = 0; Name[n]; n++ ) {

		if( Name[n] == '_' ) {

			continue;
			}

		if( n ? !wisalnum(Name[n]) : !wisalpha(Name[n]) ) {

			Error.Set(IDS_NAME_INVALID);

			return FALSE;
			}
		}

	return TRUE;
	}

// End of File
