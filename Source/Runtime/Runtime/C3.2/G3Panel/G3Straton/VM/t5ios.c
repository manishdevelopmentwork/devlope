/*****************************************************************************
T5Ios.c : mmanage built-in IOs - TO BE COMPLETED WHEN PORTING
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

#include "..\platform.h"

/*****************************************************************************
T5Ios_GetMemorySizing
Returs the size of wished private data to be allocated
Parameters:
    pDef (IN) static definition of devices in the application code
Return: number of bytes to be allocated
*****************************************************************************/

T5_DWORD T5Ios_GetMemorySizing (T5_PTR pDef)
{
#ifdef T5DEF_DLLIOS
    return T5IOdll_Call (T5DLLIO_MEMSIZING, pDef, T5_PTNULL, T5_PTNULL);
#endif /*T5DEF_DLLIOS*/
    
    /* TO BE FILLED */
    return iosGetMemorySizing(pDef);
}

/*****************************************************************************
T5Ios_Open
Open and set up IO devices
Parameters:
    pDef (IN) static definition of devices in the application code
    pDB (IN) pointer to the VM database
    pMem (IN) pointer to private data
Return: ok or error
*****************************************************************************/

T5_RET T5Ios_Open (T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
#ifdef T5DEF_DLLIOS
    return (T5_RET)T5IOdll_Call (T5DLLIO_OPEN, pDef, pDB, pMem);
#endif /*T5DEF_DLLIOS*/
    
    /* TO BE FILLED */
    return iosOpen(pDef, pDB, pMem);
}

/*****************************************************************************
T5Ios_CanHotRestart
Check if Hot restart is accepted
Parameters:
    pDef (IN) static definition of devices in the application code
    pDB (IN) pointer to the VM database
    pMem (IN) pointer to private data
Return: ok or error
*****************************************************************************/

T5_RET T5Ios_CanHotRestart (T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
#ifdef T5DEF_DLLIOS
    return (T5_RET)T5IOdll_Call (T5DLLIO_CANHOT, pDef, pDB, pMem);
#endif /*T5DEF_DLLIOS*/
    
    /* TO BE FILLED */
    return iosCanHotRestart(pDef, pDB, pMem);
}

/*****************************************************************************
T5Ios_HotRestart
Performs a hot restart
Parameters:
    pDef (IN) static definition of devices in the application code
    pDB (IN) pointer to the VM database
    pMem (IN) pointer to private data
Return: ok or error
*****************************************************************************/

T5_RET T5Ios_HotRestart (T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
#ifdef T5DEF_DLLIOS
    return (T5_RET)T5IOdll_Call (T5DLLIO_HOT, pDef, pDB, pMem);
#endif /*T5DEF_DLLIOS*/
    
    /* TO BE FILLED */
    return iosHotRestart(pDef, pDB, pMem);
}

/*****************************************************************************
T5Ios_Close
Close all IO devices
Parameters:
    pDef (IN) static definition of devices in the application code
    pDB (IN) pointer to the VM database
    pMem (IN) pointer to private data
Return: ok or error
*****************************************************************************/

T5_RET T5Ios_Close (T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
#ifdef T5DEF_DLLIOS
    return (T5_RET)T5IOdll_Call (T5DLLIO_CLOSE, pDef, pDB, pMem);
#endif /*T5DEF_DLLIOS*/
    
    /* TO BE FILLED */
    return iosClose(pDef, pDB, pMem);
}

/*****************************************************************************
T5Ios_Exchange
Exchange all input and output variables
Parameters:
    pDef (IN) static definition of devices in the application code
    pDB (IN) pointer to the VM database
    pMem (IN) pointer to private data
Return: ok or error
*****************************************************************************/

T5_RET T5Ios_Exchange (T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
#ifdef T5DEF_DLLIOS
    return (T5_RET)T5IOdll_Call (T5DLLIO_EXCHANGE, pDef, pDB, pMem);
#endif /*T5DEF_DLLIOS*/
    
    /* TO BE FILLED */
    return iosExchange(pDef, pDB, pMem);
}

/* eof **********************************************************************/
