
#include "intern.hpp"

#include "bacmap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// BACNet Constant Mappings
//

// Raw Mapping Data

CBACNetMapping::CSchemaEntry CBACNetMapping::m_SchemaList[] = {

	{
		
	0, // Analog Input

	{
	{ "object-identifier",		"bacnetobjectidentifier"		},
	{ "object-name",		"characterstring"			},
	{ "object-type",		"bacnetobjecttype"			},
	{ "present-value",		"real"					},
	{ "description",		"characterstring"			},
	{ "device-type",		"characterstring"			},		
	{ "status-flags",		"bacnetstatusflags"			},
	{ "event-state",		"bacneteventstate"			},
	{ "reliability",		"bacnetreliability"			},
	{ "out-of-service",		"boolean"				},
	{ "update-interval",		"unsigned"				},
	{ "units",			"bacnetengineeringunits"		},
	{ "min-pres-value",		"real"					},
	{ "max-pres-value",		"real"					},
	{ "resolution",			"real"					},
	{ "cov-increment",		"real"					},
	{ "time-delay",			"unsigned"				},
	{ "notification-class",		"unsigned"				},
	{ "high-limit",			"real"					},
	{ "low-limit",			"real"					},
	{ "deadband",			"real"					},
	{ "limit-enable",		"bacnetlimitenable"			},
	{ "event-enable",		"bacneteventtransitionbits"		},
	{ "acked-transitions",		"bacneteventtransitionbits"		},
	{ "notify-type",		"bacnetnotifytype"			},
	{ "profile-name",		"characterstring"			},
	{ "END",			"END"					},
	}
	},

	{

	1, // Analog Output

	{
	{ "object-identifier",		"bacnetobjectidentifier"		},
	{ "object-name",		"characterstring"			},
	{ "object-type",		"bacnetobjecttype"			},
	{ "present-value",		"real"					},
	{ "description",		"characterstring"			},
	{ "device-type",		"characterstring"			},
	{ "status-flags",		"bacnetstatusflags"			},
	{ "event-state",		"bacneteventstate"			},
	{ "reliability",		"bacnetreliability"			},
	{ "out-of-service",		"boolean"				},
	{ "units",			"bacnetengineeringunits"		},
	{ "min-pres-value",		"real"					},
	{ "max-pres-value",		"real"					},
	{ "resolution",			"real"					},
	{ "priority-array",		"bacnetpriorityarray"			},
	{ "relinquish-default",		"real"					},
	{ "cov-increment",		"real"					},
	{ "time-delay",			"unsigned"				},
	{ "notification-class",		"unsigned"				},
	{ "high-limit",			"real"					},
	{ "low-limit",			"real"					},
	{ "deadband",			"real"					},
	{ "limit-enable",		"bacnetlimitenable"			},
	{ "event-enable",		"bacneteventtransitionbits"		},
	{ "acked-transitions",		"bacneteventtransitionbits"		},
	{ "notify-type",		"bacnetnotifytype"			},
	{ "event-time-stamps",		"bacnetarray"				},
	{ "profile-name",		"charcterstring"			},
	{ "END",			"END"					},
	}
	},

	{

	2, // Analog Value

	{
	{ "object-identifier",		"bacnetobjectidentifier"		},
	{ "object-name",		"characterstring"			},
	{ "object-type",		"bacnetobjecttype"			},
	{ "present-value",		"real"					},
	{ "description",		"characterstring"			},
	{ "status-flags",		"bacnetstatusflags"			},
	{ "event-state",		"bacneteventstate"			},
	{ "reliability",		"bacnetreliability"			},
	{ "out-of-service",		"boolean"				},
	{ "units",			"bacnetengineeringunits"		},
	{ "priority-array",		"bacnetpriorityarray"			},
	{ "relinquish-default",		"real"					},
	{ "cov-increment",		"real"					},
	{ "time-delay",			"unsigned"				},
	{ "notification-class",		"unsigned"				},
	{ "high-limit",			"real"					},
	{ "low-limit",			"real"					},
	{ "deadband",			"real"					},
	{ "limit-enable",		"bacnetlimitenable"			},
	{ "event-enable",		"bacneteventtransitionbits"		},
	{ "acked-transitions",		"bacneteventtransitionbits"		},
	{ "notify-type",		"bacnetnotifytype"			},
	{ "event-time-stamps",		"bacnetarray"				},
	{ "profile-name",		"charcterstring"			},
	{ "END",			"END"					},
	}
	},

	{

	3, // Binary Input
		
	{
	{ "object-identifier",		"bacnetobjectidentifier"		},
	{ "object-name",		"characterstring"			},
	{ "object-type",		"bacnetobjecttype"			},
	{ "present-value",		"bacnetbinarypv"			},
	{ "description",		"characterstring"			},
	{ "device-type",		"characterstring"			},
	{ "status-flags",		"bacnetstatusflags"			},
	{ "event-state",		"bacneteventstate"			},
	{ "reliability",		"bacnetreliability"			},
	{ "out-of-service",		"boolean"				},
	{ "polarity",			"bacnetpolarity"			},
	{ "inactive-text",		"characterstring"			},
	{ "active-text",		"characterstring"			},
	{ "change-of-state-time",	"bacnetdatetime"			},
	{ "change-of-state-count",	"unsigned"				},
	{ "time-of-state-count-reset",	"bacnetdatetime"			},
	{ "elapsed-active-time",	"unsigned32"				},
	{ "time-of-active-time-reset",	"bacnetdatetime"			},
	{ "time-delay",			"unsigned"				},
	{ "notification-class",		"unsigned"				},
	{ "alarm-value",		"bacnetbinarypv"			},
	{ "event-enable",		"bacneteventtransitionbits"		},
	{ "acked-transitions",		"bacneteventtransitionbits"		},
	{ "notify-type",		"bacnetnotifytype"			},
	{ "profile-name",		"characterstring"			},
	{ "END",			"END"					},
	}
	},

	{

	4, // Binary Output
		
	{
	{ "object-identifier",		"bacnetobjectidentifier"		},
	{ "object-name",		"characterstring"			},
	{ "object-type",		"bacnetobjecttype"			},
	{ "present-value",		"bacnetbinarypv"			},
	{ "description",		"characterstring"			},
	{ "device-type",		"characterstring"			},
	{ "status-flags",		"bacnetstatusflags"			},
	{ "event-state",		"bacneteventstate"			},
	{ "reliability",		"bacnetreliability"			},
	{ "out-of-service",		"boolean"				},
	{ "polarity",			"bacnetpolarity"			},
	{ "inactive-text",		"characterstring"			},
	{ "active-text",		"characterstring"			},
	{ "change-of-state-time",	"bacnetdatetime"			},
	{ "change-of-state-count",	"unsigned"				},
	{ "time-of-state-count-reset",	"bacnetdatetime"			},
	{ "elapsed-active-time",	"unsigned32"				},
	{ "time-of-active-time-reset",	"bacnetdatetime"			},
	{ "minimum-off-time",		"unsigned32"				},
	{ "minimum-on-time",		"unsigned32"				},
	{ "priority-array",		"bacnetpriorityarray"			},
	{ "relinquish-default",		"real"					},
	{ "time-delay",			"unsigned",				},
	{ "notification-class",		"unsigned"				},
	{ "feedback-value",		"bacnetbinarypv"			},
	{ "event-enable",		"bacneteventtransitionbits"		},
	{ "acked-transitions",		"bacneteventtransitionbits"		},
	{ "notify-type",		"bacnetnotifytype"			},
	{ "profile-name",		"characterstring"			},
	{ "END",			"END"					},
	}
	},

	{

	5, // Binary Value
		
	{
	{ "object-identifier",		"bacnetobjectidentifier"		},
	{ "object-name",		"characterstring"			},
	{ "object-type",		"bacnetobjecttype"			},
	{ "present-value",		"bacnetbinarypv"			},
	{ "description",		"characterstring"			},
	{ "status-flags",		"bacnetstatusflags"			},
	{ "event-state",		"bacneteventstate"			},
	{ "reliability",		"bacnetreliability"			},
	{ "out-of-service",		"boolean"				},
	{ "inactive-text",		"characterstring"			},
	{ "active-text",		"characterstring"			},
	{ "change-of-state-time",	"bacnetdatetime"			},
	{ "change-of-state-count",	"unsigned"				},
	{ "time-of-state-count-reset",	"bacnetdatetime"			},
	{ "elapsed-active-time",	"unsigned32"				},
	{ "time-of-active-time-reset",	"bacnetdatetime"			},
	{ "minimum-off-time",		"unsigned32"				},
	{ "minimum-on-time",		"unsigned32"				},
	{ "priority-array",		"bacnetpriorityarray"			},
	{ "relinquish-default",		"bacnetbinarypv"			},
	{ "time-delay",			"unsigned",				},
	{ "notification-class",		"unsigned"				},
	{ "alarm-value",		"bacnetbinarypv"			},
	{ "event-enable",		"bacneteventtransitionbits"		},
	{ "acked-transitions",		"bacneteventtransitionbits"		},
	{ "notify-type",		"bacnetnotifytype"			},
	{ "profile-name",		"characterstring"			},
	{ "END",			"END"					},
	}
	},

	{

	13, // Multi-State Input
		
	{
	{ "object-identifier",		"bacnetobjectidentifier"		},
	{ "object-name",		"characterstring"			},
	{ "object-type",		"bacnetobjecttype"			},
	{ "present-value",		"unsigned"				},
	{ "description",		"characterstring"			},
	{ "device-type",		"characterstring"			},
	{ "status-flags",		"bacnetstatusflags"			},
	{ "event-state",		"bacneteventstate"			},
	{ "reliability",		"bacnetreliability"			},
	{ "out-of-service",		"boolean"				},
	{ "number-of-states",		"unsigned",				},
	{ "time-delay",			"unsigned"				},
	{ "notification-class",		"unsigned"				},
	{ "event-enable",		"bacneteventtransitionbits"		},
	{ "acked-transitions",		"bacneteventtransitionbits"		},
	{ "notify-type",		"bacnetnotifytype"			},
	{ "profile-name",		"characterstring"			},
	{ "END",			"END"					},
	}
	},

	{

	14, // Multi-State Output
		
	{
	{ "object-identifier",		"bacnetobjectidentifier"		},
	{ "object-name",		"characterstring"			},
	{ "object-type",		"bacnetobjecttype"			},
	{ "present-value",		"unsigned"				},
	{ "description",		"characterstring"			},
	{ "device-type",		"characterstring"			},
	{ "status-flags",		"bacnetstatusflags"			},
	{ "event-state",		"bacneteventstate"			},
	{ "reliability",		"bacnetreliability"			},
	{ "out-of-service",		"boolean"				},
	{ "number-of-states",		"unsigned",				},
	{ "priority-array",		"bacnetpriorityarray"			},
	{ "relinquish-default",		"unsigned"				},
	{ "time-delay",			"unsigned",				},
	{ "notification-class",		"unsigned"				},
	{ "feedback-value",		"unsigned"				},
	{ "event-enable",		"bacneteventtransitionbits"		},
	{ "acked-transitions",		"bacneteventtransitionbits"		},
	{ "notify-type",		"bacnetnotifytype"			},
	{ "profile-name",		"characterstring"			},
	{ "END",			"END"					},
	}
	},

	{

	19, // Multi-State Value
		
	{
	{ "object-identifier",		"bacnetobjectidentifier"		},
	{ "object-name",		"characterstring"			},
	{ "object-type",		"bacnetobjecttype"			},
	{ "present-value",		"unsigned"				},
	{ "description",		"characterstring"			},
	{ "status-flags",		"bacnetstatusflags"			},
	{ "event-state",		"bacneteventstate"			},
	{ "reliability",		"bacnetreliability"			},
	{ "out-of-service",		"boolean"				},
	{ "number-of-states",		"unsigned",				},
	{ "priority-array",		"bacnetpriorityarray"			},
	{ "relinquish-default",		"unsigned"				},
	{ "time-delay",			"unsigned",				},
	{ "notification-class",		"unsigned"				},
	{ "event-enable",		"bacneteventtransitionbits"		},
	{ "acked-transitions",		"bacneteventtransitionbits"		},
	{ "notify-type",		"bacnetnotifytype"			},
	{ "profile-name",		"characterstring"			},
	{ "END",			"END"					},
	}
	}

	};

CBACNetMapping::CTypeEntry CBACNetMapping::m_TypeList[] = {

	{  1, "boolean"    },
	{  2, "unsigned"   },
	{  3, "signed "    },
	{  4, "real"       },
	{  8, "bitstring"  },
	{  9, "enumerated" },
	{ 10, "date"	   },
	{ 11, "time"	   },
	{ 12, "objectid"   },
	{ 13, "relinquish" },

	};

CBACNetMapping::CAliasEntry CBACNetMapping::m_AliasList[] = {

	{ "unsigned32",					"unsigned"	},
	{ "bacnetobjectidentifier",			"objectid",	},
	{ "bacnetobjecttype",				"enumerated"	},
	{ "bacnetstatusflags",				"bitstring"	},
	{ "bacneteventstate",				"enumerated"	},
	{ "bacnetreliability",				"enumerated"	},
	{ "bacnetengineeringunits",			"enumerated"	},
	{ "bacnetlimitenable",				"bitstring"	},
	{ "bacneteventtransitionbits",			"bitstring"	},
	{ "bacnetnotifytype",				"enumerated"	},
	{ "bacnetbinarypv",				"enumerated"	},
	{ "bacnetpolarity",				"enumerated"	},

	};

CBACNetMapping::CObjectEntry CBACNetMapping::m_ObjectList[] = {

	// TODO -- Abbreviations for accumulator and pulse converter
	// have not been checked as I can't find a suitable reference.

	{ "AI",   "analog-input",		0	},
	{ "AO",   "analog-output",		1	},
	{ "AV",   "analog-value",		2	},
	{ "BI",   "binary-input",		3	},
	{ "BO",   "binary-output",		4	},
	{ "BV",   "binary-value",		5	},
	{ "CAL",  "calendar",			6	},
	{ "COM",  "command",			7	},
	{ "DEV",  "device",			8	},
	{ "EVE",  "event-enrollment",		9	},
	{ "FILE", "file",			10	},
	{ "GRP",  "group",			11	},
	{ "LOOP", "loop",			12	},
	{ "MI",   "multi-state-input",		13	},
	{ "MO",   "multi-state-output",		14	},
	{ "NC",   "notification-class",		15	},
	{ "PRO",  "program",			16	},
	{ "SCH",  "schedule",			17	},
	{ "AVG",  "averaging",			18	},
	{ "MV",   "multi-state-value",		19	},
	{ "TR",   "trend-log",			20	},
	{ "LSP",  "life-safety-point",		21	},
	{ "LSZ",  "life-safety-zone",		22	},
	{ "ACC",  "accumulator",		23	},
	{ "PC",   "pulse-converter",		24	},

	};

CBACNetMapping::CPropEntry CBACNetMapping::m_PropList[] = {

	{ "accepted-modes",			175	},
	{ "acked-transitions",			0	},
	{ "ack-required",			1	},
	{ "action",				2	},
	{ "action-text",			3	},
	{ "active-text",			4	},
	{ "active-vt-sessions",			5	},
	{ "active-cov-subscriptions",		152	},
	{ "adjust-value",			176	},
	{ "alarm-value",			6	},
	{ "alarm-values",			7	},
	{ "all",				8	},
	{ "all-writes-successful",		9	},
	{ "apdu-segment-timeout",		10	},
	{ "apdu-timeout",			11	},
	{ "application-software-version",	12	},
	{ "archive",				13	},
	{ "attempted-samples",			124	},
	{ "auto-slave-discovery",		169	},
	{ "average-value",			125	},
	{ "backup-failure-timeout",		153	},
	{ "bias",				14	},
	{ "buffer-size",			126	},
	{ "change-of-state-count",		15	},
	{ "change-of-state-time",		16	},
	{ "client-cov-increment",		127	},
	{ "configuration-files",		154	},
	{ "controlled-variable-reference",	19	},
	{ "controlled-variable-units",		20	},
	{ "controlled-variable-value",		21	},
	{ "count",				177	},
	{ "count-before-change",		178	},
	{ "count-change-time",			179	},
	{ "cov-increment",			22	},
	{ "cov-period",				180	},
	{ "cov-resubscription-interval",	128	},
	{ "database-revision",			155	},
	{ "date-list",				23	},
	{ "daylight-savings-status",		24	},
	{ "deadband",				25	},
	{ "derivative-constant",		26	},
	{ "derivative-constant-units",		27	},
	{ "description",			28	},
	{ "description-of-halt",		29	},
	{ "device-address-binding",		30	},
	{ "device-type",			31	},
	{ "direct-reading",			156	},
	{ "effective-period",			32	},
	{ "elapsed-active-time",		33	},
	{ "error-limit",			34	},
	{ "event-enable",			35	},
	{ "event-state",			36	},
	{ "event-time-stamps",			130	},
	{ "event-type",				37	},
	{ "event-parameters",			83	},
	{ "exception-schedule",			38	},
	{ "fault-values",			39	},
	{ "feedback-value",			40	},
	{ "file-access-method",			41	},
	{ "file-size",				42	},
	{ "file-type",				43	},
	{ "firmware-revision",			44	},
	{ "high-limit",				45	},
	{ "inactive-text",			46	},
	{ "in-process",				47	},
	{ "input-reference",			181	},
	{ "instance-of",			48	},
	{ "integral-constant",			49	},
	{ "integral-constant-units",		50	},
	{ "last-notify-record",			173	},
	{ "last-restore-time",			157	},
	{ "life-safety-alarm-values",		166	},
	{ "limit-enable",			52	},		
	{ "limit-monitoring-interval",		182	},
	{ "list-of-group-members",		53	},
	{ "list-of-object-property-references", 54	},
	{ "list-of-session-keys",		55	},
	{ "local-date",				56	},
	{ "local-time",				57	},
	{ "location",				58	},
	{ "log-buffer",				131	},
	{ "log-device-object-property",		132	},
	{ "log-enable",				133	},
	{ "log-interval",			134	},
	{ "logging-object",			183	},
	{ "logging-record",			184	},
	{ "low-limit",				59	},
	{ "maintenance-required",		158	},
	{ "manipulated-variable-reference",	60	},
	{ "manual-slave-address-binding",	170	},
	{ "maximum-output",			61	},
	{ "maximum-value",			135	},
	{ "maximum-value-timestamp",		149	},
	{ "max-apdu-length-accepted",		62	},
	{ "max-info-frames",			63	},
	{ "max-master",				64	},
	{ "max-pres-value",			65	},
	{ "max-segments-accepted",		167	},
	{ "member-of",				159	},
	{ "minimum-off-time",			66	},
	{ "minimum-on-time",			67	},
	{ "minimum-output",			68	},
	{ "minimum-value",			136	},
	{ "minimum-value-timestamp",		150	},
	{ "min-pres-value",			69	},
	{ "mode",				160	},
	{ "model-name",				70	},
	{ "modification-date",			71	},
	{ "notification-class",			17	},
	{ "notification-threshold",		137	},
	{ "notify-type",			72	},
	{ "number-of-APDU-retries",		73	},
	{ "number-of-states",			74	},
	{ "object-identifier",			75	},
	{ "object-list",			76	},
	{ "object-name",			77	},
	{ "object-property-reference",		78	},
	{ "object-type",			79	},
	{ "operation-expected",			161	},
	{ "optional",				80	},
	{ "out-of-service",			81	},
	{ "output-units",			82	},
	{ "polarity",				84	},
	{ "prescale",				185	},
	{ "present-value",			85	},
	{ "priority",				86	},
	{ "pulse-rate",				186	},
	{ "priority-array",			87	},
	{ "priority-for-writing",		88	},
	{ "process-identifier",			89	},
	{ "profile-name",			168	},
	{ "program-change",			90	},
	{ "program-location",			91	},
	{ "program-state",			92	},
	{ "proportional-constant",		93	},
	{ "proportional-constant-units",	94	},
	{ "protocol-object-types-supported",	96	},
	{ "protocol-revision",			139	},
	{ "protocol-services-supported",	97	},
	{ "protocol-version",			98	},
	{ "read-only",				99	},
	{ "reason-for-halt",			100	},
	{ "recipient-list",			102	},
	{ "records-since-notification",		140	},
	{ "record-count",			141	},
	{ "reliability",			103	},
	{ "relinquish-default",			104	},
	{ "required",				105	},
	{ "resolution",				106	},
	{ "scale",				187	},	
	{ "scale-factor",			188	},
	{ "schedule-default",			174	},
	{ "segmentation-supported",		107	},
	{ "setpoint",				108	},
	{ "setpoint-reference",			109	},
	{ "slave-address-binding",		171	},
	{ "setting",				162	},
	{ "silenced",				163	},
	{ "start-time",				142	},
	{ "state-text",				110	},
	{ "status-flags",			111	},
	{ "stop-time",				143	},
	{ "stop-when-full",			144	},
	{ "system-status",			112	},
	{ "time-delay",				113	},
	{ "time-of-active-time-reset",		114	},
	{ "time-of-state-count-reset",		115	},
	{ "time-synchronization-recipients",	116	},
	{ "total-record-count",			145	},
	{ "tracking-value",			164	},
	{ "units",				117	},
	{ "update-interval",			118	},
	{ "update-time",			189	},
	{ "utc-offset",				119	},
	{ "valid-samples",			146	},
	{ "value-before-change",		190	},
	{ "value-set",				191	},
	{ "value-change-time",			192	},
	{ "variance-value",			151	},
	{ "vendor-identifier",			120	},
	{ "vendor-name",			121	},
	{ "vt-classes-supported",		122	},
	{ "weekly-schedule",			123	},
	{ "window-interval",			147	},
	{ "window-samples",			148	},
	{ "zone-members",			165	},

	};

// Constructor

CBACNetMapping::CBACNetMapping(void)
{
	ScanTypes();

	ScanAliases();

	ScanObjects();

	ScanProps();

	/*ValidatePropTypes();*/
	}	

// Enumeration

BYTE CBACNetMapping::EnumObjectTypes(UINT uIndex)
{
	if( uIndex < elements(m_SchemaList) ) {

		return m_SchemaList[uIndex].m_Obj;
		}

	return 0xFF;
	}

WORD CBACNetMapping::EnumObjectProps(BYTE ObjectType, UINT uIndex)
{
	for( UINT o = 0; o < elements(m_SchemaList); o++ ) {

		if( m_SchemaList[o].m_Obj == ObjectType ) {

			CString Prop = m_SchemaList[o].m_Row[uIndex].m_Prop;

			CString Type = m_SchemaList[o].m_Row[uIndex].m_Type;

			if( Prop == "END" ) {

				break;
				}

			BYTE PropID = GetPropID(Prop);

			BYTE TypeID = GetTypeID(Type);

			return MAKEWORD(PropID, TypeID);
			}
		}

	return 0xFFFF;
	}

BYTE CBACNetMapping::GetPropType(BYTE ObjectType, BYTE PropID)
{
	for( UINT o = 0; o < elements(m_SchemaList); o++ ) {

		if( m_SchemaList[o].m_Obj == ObjectType ) {

			for( UINT n = 0;; n++ ) {

				CString Prop = m_SchemaList[o].m_Row[n].m_Prop;

				CString Type = m_SchemaList[o].m_Row[n].m_Type;

				if( Prop == "END" ) {

					break;
					}

				if( GetPropID(Prop) == PropID ) {

					BYTE TypeID = GetTypeID(Type);

					return TypeID;
					}
				}

			break;
			}
		}

	return FALSE;
	}

// Lookups

CString CBACNetMapping::GetTypeName(BYTE TypeID)
{
	return m_TypeFwd[TypeID];
	}

CString CBACNetMapping::GetObjectName(BYTE ObjectType)
{
	return m_ObjectFwdName[ObjectType];
	}

CString CBACNetMapping::GetObjectAbbr(BYTE ObjectType)
{
	return m_ObjectFwdAbbr[ObjectType];
	}

CString CBACNetMapping::GetPropName(BYTE PropID)
{
	return m_PropFwd[PropID];
	}

BYTE CBACNetMapping::GetTypeID(CString Name)
{
	INDEX n = m_TypeRev.FindName(Name);

	if( !m_TypeRev.Failed(n) ) {

		return m_TypeRev.GetData(n);
		}

	return 0xFF;
	}

BYTE CBACNetMapping::GetObjectType(CString Abbr)
{
	INDEX n = m_ObjectRevAbbr.FindName(Abbr);

	if( !m_ObjectRevAbbr.Failed(n) ) {

		return m_ObjectRevAbbr.GetData(n);
		}

	return 0xFF;
	}

BYTE CBACNetMapping::GetPropID(CString Name)
{
	INDEX n = m_PropRev.FindName(Name);

	if( !m_PropRev.Failed(n) ) {

		return m_PropRev.GetData(n);
		}

	return 0xFF;
	}

// Implementation

void CBACNetMapping::ScanTypes(void)
{
	for( UINT n = 0; n < elements(m_TypeList); n++ ) {

		m_TypeFwd.Insert(m_TypeList[n].m_ID, m_TypeList[n].m_Name);

		m_TypeRev.Insert(m_TypeList[n].m_Name, m_TypeList[n].m_ID);
		}
	}

void CBACNetMapping::ScanAliases(void)
{
	for( UINT n = 0; n < elements(m_AliasList); n++ ) {

		BYTE TypeID = GetTypeID(m_AliasList[n].m_Type);

		if( TypeID == 0xFF ) {

			AfxTrace(TEXT("Can't convert %s\n"), CString(m_AliasList[n].m_Type));

			continue;
			}

		m_TypeRev.Insert(m_AliasList[n].m_Name, TypeID);
		}
	}

void CBACNetMapping::ScanObjects(void)
{
	for( UINT n = 0; n < elements(m_ObjectList); n++ ) {

		m_ObjectFwdName.Insert(m_ObjectList[n].m_ID, m_ObjectList[n].m_Name);

		m_ObjectRevName.Insert(m_ObjectList[n].m_Name, m_ObjectList[n].m_ID);

		m_ObjectFwdAbbr.Insert(m_ObjectList[n].m_ID, m_ObjectList[n].m_Abbr);

		m_ObjectRevAbbr.Insert(m_ObjectList[n].m_Abbr, m_ObjectList[n].m_ID);
		}
	}

void CBACNetMapping::ScanProps(void)
{
	for( UINT n = 0; n < elements(m_PropList); n++ ) {

		m_PropFwd.Insert(m_PropList[n].m_ID, m_PropList[n].m_Name);

		m_PropRev.Insert(m_PropList[n].m_Name, m_PropList[n].m_ID);
		}
	}

// Validation

void CBACNetMapping::ValidatePropTypes(void)
{
	CTree <CString> Got;

	for( UINT o = 0; o < elements(m_SchemaList); o++ ) {

		for( UINT n = 0;; n++ ) {

			CString Prop = m_SchemaList[o].m_Row[n].m_Prop;

			CString Type = m_SchemaList[o].m_Row[n].m_Type;

			if( Prop == "END" ) {

				break;
				}
			
			if( GetTypeID(Type) == 0xFF ) {

				if( Got.Failed(Got.Find(Type)) ) {

					Got.Insert(Type);

					AfxTrace(TEXT("{ \"%s\",\n"), Type);
					}
				}
			}
		}
	}

// End of File
