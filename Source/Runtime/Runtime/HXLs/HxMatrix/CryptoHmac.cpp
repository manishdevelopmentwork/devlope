
#include "Intern.hpp"

#include "CryptoHmac.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic HMAC
//

// Constructor

CCryptoHmac::CCryptoHmac(void)
{
	StdSetRef();

	m_uState = stateNew;

	m_uHash  = 0;

	m_pHash  = NULL;
	}

// IUnknown

HRESULT CCryptoHmac::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ICryptoHmac);

	StdQueryInterface(ICryptoHmac);

	return E_NOINTERFACE;
	}

ULONG CCryptoHmac::AddRef(void)
{
	StdAddRef();
	}

ULONG CCryptoHmac::Release(void)
{
	StdRelease();
	}

// ICryptoHmac

void CCryptoHmac::GetHashOid(CByteArray &oid)
{
	AfxAssert(FALSE);
	}

UINT CCryptoHmac::GetHashSize(void)
{
	return m_uHash;
	}

PCBYTE CCryptoHmac::GetHashData(void)
{
	AfxAssert(m_uState == stateDone);

	return m_pHash;
	}

BOOL CCryptoHmac::GetHashData(CByteArray &Data)
{
	AfxAssert(m_uState == stateDone);

	Data.Empty();

	Data.Append(m_pHash, m_uHash);

	return TRUE;
	}

CString CCryptoHmac::GetHashString(UINT Code)
{
	AfxAssert(m_uState == stateDone);

	return Encode(Code, m_pHash, m_uHash);
	}

void CCryptoHmac::Initialize(void)
{
	AfxAssert(FALSE);
	}

void CCryptoHmac::Initialize(CByteArray const &Pass)
{
	((ICryptoHmac *) this)->Initialize(Pass.GetPointer(), Pass.GetCount());
	}

void CCryptoHmac::Initialize(CString const &Pass)
{
	((ICryptoHmac *) this)->Initialize(PCBYTE(PCTXT(Pass)), Pass.GetLength());
	}

void CCryptoHmac::Update(CByteArray const &Data)
{
	((ICryptoHmac *) this)->Update(Data.GetPointer(), Data.GetCount());
	}

void CCryptoHmac::Update(CString const &Data)
{
	((ICryptoHmac *) this)->Update(PCBYTE(PCTXT(Data)), Data.GetLength());
	}

// End of File
