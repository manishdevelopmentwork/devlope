
#include "Intern.hpp"

#include "PppNegotiate.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ppp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// LCP Frame Wrapper
//

// Conversion

void CLcpFrame::NetToHost(void)
{
	m_wLength = ::NetToHost(m_wLength);
	}

void CLcpFrame::HostToNet(void)
{
	m_wLength = ::HostToNet(m_wLength);
	}

//////////////////////////////////////////////////////////////////////////
//
// PPP Negotiator
//

// Constructor

CPppNegotiate::CPppNegotiate(CPpp *pPpp, WORD wProtocol) : CPppLayer(pPpp, wProtocol)
{
	m_bID    = 0;

	m_uState = stateInitial;

	m_uCount = 0;

	m_fTimer = FALSE;

	m_uRetry = ToTicks(pPpp->IsDirect() ? 1000 : 10000);
	}

// Destructor

CPppNegotiate::~CPppNegotiate(void)
{
	}

// Management

void CPppNegotiate::Reset(void)
{
	m_uState = stateInitial;

	m_uCount = 0;

	m_fTimer = FALSE;
	}

void CPppNegotiate::Open(void)
{
	switch( m_uState ) {
		
		case stateInitial:

			SetState(stateStarting);

			ThisLayerStarted();

			break;

		case stateStarting:

			break;

		case stateClosed:

			InitRestartCount();

			LocalDefault();

			SendConfigRequest(TRUE);

			SetState(stateReqSent);

			break;

		case stateClosing:

			SetState(stateStopping);

			break;
		}
	}

void CPppNegotiate::Close(void)
{
	switch( m_uState ) {
		
		case stateStarting:

			SetState(stateInitial);

			ThisLayerFinished();

			break;

		case stateStopped:

			SetState(stateClosed);

			break;

		case stateStopping:

			SetState(stateClosing);

			break;

		case stateReqSent:
		case stateAckRcvd:
		case stateAckSent:

			InitRestartCount();

			SendTerminateRequest();

			SetState(stateClosing);

			break;

		case stateOpened:

			InitRestartCount();

			SendTerminateRequest();

			SetState(stateClosing);

			ThisLayerDown();

			break;
		}
	}

void CPppNegotiate::LowerLayerUp(void)
{
	switch( m_uState ) {
		
		case stateInitial:
			
			SetState(stateClosed);
			
			break;

		case stateStarting:

			InitRestartCount();

			LocalDefault();

			SendConfigRequest(TRUE);

			SetState(stateReqSent);

			break;
		}
	}

void CPppNegotiate::LowerLayerDown(void)
{
	switch( m_uState ) {
		
		case stateClosed:

			SetState(stateInitial);

			break;

		case stateStopped:

			SetState(stateStarting);

			ThisLayerStarted();

			break;

		case stateClosing:

			SetState(stateInitial);

			break;

		case stateStopping:
		case stateReqSent:
		case stateAckRcvd:
		case stateAckSent:
		case stateOpened:

			SetState(stateStarting);

			ThisLayerDown();

			break;
		}
	}

void CPppNegotiate::OnTime(void)
{
	if( m_fTimer ) {

		UINT uTime = GetTickCount();

		UINT uGone = uTime - m_uStart;
		
		if( uGone >= m_uTimer ) {

			if( m_uCount ) {

				m_fTimer = FALSE;

				m_uCount = m_uCount - 1;

				TcpDebug(m_uObj, LEV_TRACE, "timeout+\n");

				OnTimeoutPos();
				}
			else {
				m_fTimer = FALSE;

				TcpDebug(m_uObj, LEV_TRACE, "timeout-\n");

				OnTimeoutNeg();
				}
			}
		}
	}

// Frame Handing

void CPppNegotiate::OnRecv(CBuffer *pBuff)
{
	m_pLcp = BuffStripHead(pBuff, CLcpFrame);

	m_pLcp->NetToHost();

	TcpDebug(m_uObj, LEV_TRACE, "recv code %u\n", m_pLcp->m_bCode);

	switch( m_pLcp->m_bCode ) {

		// LATER -- Reject, Discard and Echo frames.

		// LATER -- Send code reject on unkown code.

		case codeConfigRequest:

			OnRecvConfigRequest();
			
			break;

		case codeConfigAck:

			OnRecvConfigAck();
			
			break;

		case codeConfigNak:

			OnRecvConfigNak();
			
			break;

		case codeConfigReject:

			OnRecvConfigReject();
			
			break;

		case codeTerminateRequest:

			OnRecvTerminateRequest();
			
			break;

		case codeTerminateAck:

			OnRecvTerminateAck();
			
			break;

		case codeCodeReject:

			break;

		default:
			OnRecvCodeUnknown(pBuff);
			
			break;
		}
	}

// Internal Events

void CPppNegotiate::OnTimeoutPos(void)
{
	switch( m_uState ) {

		case stateClosing:
		case stateStopping:

			SendTerminateRequest();

			break;

		case stateReqSent:
		case stateAckRcvd:

			SendConfigRequest(FALSE);

			SetState(stateReqSent);

			break;

		case stateAckSent:

			SendConfigRequest(FALSE);

			break;

		default:
			m_uTimer = 0;

			break;
		}
	}

void CPppNegotiate::OnTimeoutNeg(void)
{
	switch( m_uState ) {

		case stateClosing:

			SetState(stateClosed);

			ThisLayerFinished();

			break;

		case stateStopping:

			SetState(stateStopped);

			ThisLayerFinished();

			break;

		case stateReqSent:
		case stateAckRcvd:
		case stateAckSent:

			SetState(stateStopped);

			ThisLayerFinished();

			break;

		default:
			m_uTimer = 0;

			break;
		}
	}

void CPppNegotiate::OnRecvConfigRequest(void)
{
	switch( m_uState ) {

		case stateClosed:

			SendTerminateAck();

			break;

		case stateStopped:

			if( CheckRemoteLoop() ) {

				InitRestartCount();

				if( SendConfigReply() ) {

					SetState(stateAckSent);
					}
				else
					SetState(stateReqSent);
				}

			break;

		case stateReqSent:
		case stateAckSent:

			if( CheckRemoteLoop() ) {

				if( SendConfigReply() ) {

					SetState(stateAckSent);
					}
				else
					SetState(stateReqSent);
				}

			break;

		case stateAckRcvd:

			if( CheckRemoteLoop() ) {

				if( SendConfigReply() ) {

					SetState(stateOpened);

					ThisLayerUp();
					}
				else {
					// NOTE -- This is not in the spec. If we leave it
					// out and we take a long time NAKing remote options,
					// we end up re-sending our own configuration, which
					// seems a bad move. Perhaps I'm missing something?

					LoadRestartTimer();
					}
				}

			break;

		case stateOpened:

			SendConfigRequest(TRUE);

			if( SendConfigReply() ) {

				SetState(stateAckSent);
				}
			else
				SetState(stateReqSent);

			ThisLayerDown();

			break;
		}
	}

void CPppNegotiate::OnRecvConfigAck(void)
{
	switch( m_uState ) {

		case stateClosed:
		case stateStopped:

			SendTerminateAck();

			break;

		case stateReqSent:

			LocalAgreed();

			InitRestartCount();

			SetState(stateAckRcvd);

			break;

		case stateAckRcvd:

			SendConfigRequest(FALSE);

			SetState(stateReqSent);

			break;

		case stateAckSent:

			LocalAgreed();

			InitRestartCount();

			SetState(stateOpened);

			ThisLayerUp();

			break;

		case stateOpened:

			SendConfigRequest(TRUE);

			SetState(stateReqSent);

			ThisLayerDown();

			break;
		}
	}

void CPppNegotiate::OnRecvConfigNak(void)
{
	if( CheckLocalLoop() ) {

		BYTE  bID   = m_pLcp->m_bID;

		PBYTE pData = m_pLcp->m_bData;

		UINT  uSize = m_pLcp->m_wLength - 4;

		AfxTouch(bID);

		for( UINT n = 0; n < uSize; ) {

			BYTE bType = pData[n+0];

			BYTE bSize = pData[n+1];

			TcpDebug(m_uObj, LEV_TRACE, "recv nak for %u\n", bType);

			if( !LocalNak(bType, pData + n) ) {

				TcpDebug(m_uObj, LEV_WARN, "can't handle NAK\n");

				ThisLayerFailed();

				return;
				}

			n += bSize;
			}

		OnRecvConfigNakRej();
		}
	}

void CPppNegotiate::OnRecvConfigReject(void)
{
	if( CheckLocalLoop() ) {

		BYTE  bID   = m_pLcp->m_bID;

		PBYTE pData = m_pLcp->m_bData;

		UINT  uSize = m_pLcp->m_wLength - 4;

		AfxTouch(bID);

		for( UINT n = 0; n < uSize; ) {

			BYTE bType = pData[n+0];

			BYTE bSize = pData[n+1];

			TcpDebug(m_uObj, LEV_TRACE, "recv reject for %u\n", bType);

			if( !LocalReject(bType, pData + n) ) {

				TcpDebug(m_uObj, LEV_WARN, "can't handle reject\n");

				ThisLayerFailed();

				return;
				}

			n += bSize;
			}

		OnRecvConfigNakRej();
		}
	}

void CPppNegotiate::OnRecvConfigNakRej(void)
{
	switch( m_uState ) {

		case stateClosed:
		case stateStopped:

			SendTerminateAck();

			break;

		case stateReqSent:
		case stateAckSent:

			InitRestartCount();

			SendConfigRequest(FALSE);

			break;

		case stateAckRcvd:

			InitRestartCount();

			SendConfigRequest(FALSE);

			SetState(stateReqSent);

			break;

		case stateOpened:

			SendConfigRequest(TRUE);

			SetState(stateReqSent);

			ThisLayerDown();

			break;
		}
	}

void CPppNegotiate::OnRecvTerminateRequest(void)
{
	switch( m_uState ) {

		case stateClosed:
		case stateStopped:
		case stateClosing:
		case stateStopping:

			SendTerminateAck();

			break;

		case stateReqSent:
		case stateAckRcvd:
		case stateAckSent:

			SendTerminateAck();

			SetState(stateReqSent);

			break;

		case stateOpened:

			ZeroRestartCount();

			SendTerminateAck();

			SetState(stateStopping);

			ThisLayerDown();

			break;
		}
	}

void CPppNegotiate::OnRecvTerminateAck(void)
{
	switch( m_uState ) {

		case stateClosing:

			SetState(stateClosed);

			ThisLayerFinished();

			break;

		case stateStopping:

			SetState(stateStopped);

			ThisLayerFinished();

			break;

		case stateAckRcvd:

			SetState(stateReqSent);

			break;

		case stateOpened:

			SendConfigRequest(TRUE);

			SetState(stateReqSent);

			ThisLayerDown();

			break;
		}
	}

void CPppNegotiate::OnRecvCodeUnknown(CBuffer *pBuff)
{
	if( CodeUnknown() ) {
		
		SendCodeReject(pBuff);

		return;
		}
	}

// Actions

void CPppNegotiate::ZeroRestartCount(void)
{
	m_uCount = 0;

	LoadRestartTimer();
	}

void CPppNegotiate::InitRestartCount(void)
{
	m_uCount = 4;
	}

void CPppNegotiate::LoadRestartTimer(void)
{
	m_uStart = GetTickCount();

	m_uTimer = m_uRetry;

	m_fTimer = TRUE;
	}

void CPppNegotiate::StopRestartTimer(void)
{
	m_fTimer = FALSE;
	}

void CPppNegotiate::SendConfigRequest(BOOL fInit)
{
	if( fInit ) {

		m_uLocLimit = 8;

		m_uRemLimit = 8;
		}

	LoadRestartTimer();

	CBuffer *pBuff = BuffAllocate(0);

	LocalRequest(pBuff);

	PutFrame(pBuff, codeConfigRequest, m_bID++);
	}

BOOL CPppNegotiate::SendConfigReply(void)
{
	BYTE  bID   = m_pLcp->m_bID;

	PBYTE pData = m_pLcp->m_bData;

	UINT  uSize = m_pLcp->m_wLength - 4;

	if( !RemoteReject(bID, pData, uSize) ) {

		if( !RemoteNak(bID, pData, uSize) ) {

			CBuffer *pBuff = BuffAllocate(0);

			memcpy(pBuff->AddTail(uSize), pData, uSize);

			RemoteDefault();

			RemoteAgreed(pData, uSize);

			PutFrame(pBuff, codeConfigAck, bID);

			return TRUE;
			}
		}

	return FALSE;
	}

void CPppNegotiate::SendTerminateRequest(void)
{
	LoadRestartTimer();

	CBuffer *pBuff = BuffAllocate(0);

	PutFrame(pBuff, codeTerminateRequest, m_bID++);
	}

void CPppNegotiate::SendTerminateAck(void)
{
	CBuffer *pBuff = BuffAllocate(0);

	PutFrame(pBuff, codeTerminateAck, m_bID++);
	}

void CPppNegotiate::SendCodeReject(CBuffer *pBuff)
{
	UINT     uSize = pBuff->GetSize();

	CBuffer *pSend = BuffAllocate(uSize);

	memcpy(pSend->AddTail(uSize), pBuff->GetData(), uSize);

	PutFrame(pSend, codeCodeReject, m_bID++);
	}

void CPppNegotiate::SendEchoReply(void)
{
	// LATER -- Implement
	}

// Remote Options

BOOL CPppNegotiate::RemoteReject(BYTE bID, PBYTE pData, UINT uSize)
{
	CBuffer *pBuff = NULL;

	for( UINT n = 0; n < uSize; ) {

		BYTE bType = pData[n+0];

		BYTE bSize = pData[n+1];

		if( RemoteReject(bType, pData+n) ) {

			BYTE bSize = pData[n+1];

			if( !bSize ) {

				return TRUE;
				}

			if( !pBuff ) {

				pBuff = BuffAllocate(0);
				}

			memcpy(pBuff->AddTail(bSize), pData + n, bSize);

			TcpDebug(m_uObj, LEV_TRACE, "send reject for %u\n", bType);
			}

		n += bSize;
		}

	if( pBuff ) {

		PutFrame(pBuff, codeConfigReject, bID);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPppNegotiate::RemoteNak(BYTE bID, PBYTE pData, UINT uSize)
{
	CBuffer *pBuff = NULL;

	for( UINT n = 0; n < uSize; ) {

		BYTE bType = pData[n+0];

		BYTE bSize = pData[n+1];

		if( RemoteNak(bType, pData+n) ) {

			BYTE bSize = pData[n+1];

			if( !bSize ) {

				return TRUE;
				}

			if( !pBuff ) {

				pBuff = BuffAllocate(0);
				}

			memcpy(pBuff->AddTail(bSize), pData + n, bSize);

			TcpDebug(m_uObj, LEV_TRACE, "send nak for %u\n", bType);
			}

		n += bSize;
		}

	if( pBuff ) {

		PutFrame(pBuff, codeConfigNak, bID);

		return TRUE;
		}

	return FALSE;
	}

// Remote Options

BOOL CPppNegotiate::RemoteReject(BYTE bType, PBYTE pData)
{
	return TRUE;
	}

BOOL CPppNegotiate::RemoteNak(BYTE bType, PBYTE pData)
{
	return TRUE;
	}

void CPppNegotiate::RemoteDefault(void)
{
	}

void CPppNegotiate::RemoteAgreed(PCBYTE pData, UINT uSize)
{
	}

// Load Options

void CPppNegotiate::LocalDefault(void)
{
	}

void CPppNegotiate::LocalRequest(CBuffer *pBuff)
{
	}

BOOL CPppNegotiate::LocalReject(BYTE bType, PBYTE pData)
{
	return FALSE;
	}

BOOL CPppNegotiate::LocalNak(BYTE bType, PBYTE pData)
{
	return FALSE;
	}

void CPppNegotiate::LocalAgreed(void)
{
	}

// Extended Code

BOOL CPppNegotiate::CodeUnknown(void)
{
	return TRUE;
	}

// Loop Detection

BOOL CPppNegotiate::CheckLocalLoop(void)
{
	if( !m_uLocLimit ) {

		TcpDebug(m_uObj, LEV_WARN, "local negotiation failure\n");

		ThisLayerFailed();

		return FALSE;
		}

	m_uLocLimit--;

	return TRUE;
	}

BOOL CPppNegotiate::CheckRemoteLoop(void)
{
	if( !m_uRemLimit ) {

		TcpDebug(m_uObj, LEV_WARN, "remote negotiation failure\n");

		ThisLayerFailed();

		return FALSE;
		}

	m_uRemLimit--;

	return TRUE;
	}

// Implementation

void CPppNegotiate::PutFrame(CBuffer *pBuff, BYTE bCode, BYTE bID)
{
	TcpDebug(m_uObj, LEV_TRACE, "send code %u\n", bCode);

	CLcpFrame *pRep = BuffAddHead(pBuff, CLcpFrame);

	pRep->m_bCode   = bCode;

	pRep->m_bID     = bID;

	pRep->m_wLength = WORD(pBuff->GetSize());

	pRep->HostToNet();

	m_pPpp->PutPPP(m_wProtocol, pBuff);

	BuffRelease(pBuff);
	}

void CPppNegotiate::SetState(UINT uState)
{
	switch( uState ) {

		case stateInitial:
		case stateStarting:
		case stateClosed:
		case stateStopped:
		case stateOpened:

			StopRestartTimer();

			break;
		}

	m_uState = uState;
	}

// End of File
