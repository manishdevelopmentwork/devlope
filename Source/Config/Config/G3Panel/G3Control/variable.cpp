
#include "intern.hpp"

#include "varview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Page
//

class CControlVariablePage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CControlVariablePage(CControlVariable *pVariable, CString Title, UINT uPage);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CControlVariable * m_pVariable;
		CControlProgram  * m_pProgram;

		// Implementation
		BOOL LoadSource(IUICreate *pView, CItem *pItem);
		BOOL LoadInitial(IUICreate *pView, CItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Page
//

// Runtime Class

AfxImplementRuntimeClass(CControlVariablePage, CUIStdPage);

// Constructor

CControlVariablePage::CControlVariablePage(CControlVariable *pVariable, CString Title, UINT uPage)
{
	m_pVariable = pVariable;

	m_Title     = Title;

	m_Class     = AfxPointerClass(pVariable);

	m_uPage     = uPage;
	}

// Operations

BOOL CControlVariablePage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	if( m_uPage == 1 ) {

		CUIStdPage::LoadIntoView(pView, pItem);

		LoadInitial(pView, pItem);
		
		pView->NoRecycle();
		}

	if( m_uPage == 3 ) {

		LoadSource(pView, pItem);

		CUIStdPage::LoadIntoView(pView, pItem);

		LoadInitial(pView, pItem);
		
		pView->NoRecycle();
		}

	if( m_uPage == 4 ) {

		CUIStdPage::LoadIntoView(pView, pItem);		
		}

	return TRUE;
	}

// Implementation

BOOL CControlVariablePage::LoadSource(IUICreate *pView, CItem *pItem)
{
	m_pProgram = (CControlProgram *) m_pVariable->GetParent(AfxRuntimeClass(CControlProgram));

	DWORD dwProject = m_pProgram->GetProject();

	DWORD dwProgram = m_pProgram->GetHandle();

	CStratonProgramDescriptor Prog(dwProject, dwProgram);

	if( !(Prog.IsCalled() || Prog.IsUdFb()) ) {

		pView->StartGroup(L"Data Source", 1);

		pView->AddUI(pItem, L"root", L"Value");

		pView->EndGroup(TRUE);			
		}

	return FALSE;
	}

BOOL CControlVariablePage::LoadInitial(IUICreate *pView, CItem *pItem)
{
	if( m_pVariable->m_Extent ) {

		pView->StartGroup(CString(IDS_INITIAL_VALUE), 1);

		CUIData UIData;

		UIData.m_Tag       = L"Initial";

		UIData.m_Label     = CString(IDS_INITIAL_VALUE);

		UIData.m_ClassText = AfxNamedClass(L"CUITextVarInitArray");

		UIData.m_ClassUI   = AfxNamedClass(L"CUIVarInitArray");

		UIData.m_Format    = L"";

		UIData.m_Tip       = CString(IDS_INDICATES_INITIAL);

		pView->AddUI(pItem, L"root", &UIData);

		pView->EndGroup(TRUE);			
		}
	else {
		CVariableInitial *pInitial = m_pVariable->m_pInitial;

		if( pInitial ) {
		
			CUIPageList List;

			pInitial->OnLoadPages(&List);

			if( List.GetCount() ) {

				List[0]->LoadIntoView(pView, pInitial);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable
//

// Dynamic Class

AfxImplementDynamicClass(CControlVariable, CControlObject);

// Constructor

CControlVariable::CControlVariable(void)
{
	m_pValue     = NULL;

	m_Extent     = 0;

	m_Persist    = 0;

	m_TypeIdent  = 0;

	m_Length     = 0;

	m_Private    = 0;

	m_pInitial   = NULL;

	m_pVariable  = NULL;

	m_pGroup     = NULL;
	}

// Destructor

CControlVariable::~CControlVariable(void)
{
	if( m_pVariable ) {
		
		delete m_pVariable;

		m_pVariable = NULL;
		}
	}

// Overridables

UINT CControlVariable::GetTreeImage(void) const
{
	return m_Extent ? IDI_VARIABLE + 3 : IDI_VARIABLE;
	}

void CControlVariable::Validate(void)
{
	UpdateEmbed();
	}

// Overridables

BOOL CControlVariable::OnRename(CError &Error, CString Name)
{
	CVariableNamer Var(GetProject());

	if( Var.Validate(Error, Name) ) {

		if( m_pVariable->Rename(Name) ) {
		
			SetName(Name);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CControlVariable::CanRename(CError &Error, CString Name)
{
	DWORD dwProject = GetProject();

	DWORD dwGroup   = GetGroup(dwProject);

	CVariableNamer Namer(dwProject, dwGroup);

	if( Namer.Validate(Error, Name) ) {

		if( !Namer.CanFindName(Error, Name) ) {

			return TRUE;
			}
			
		Error.Set(CString(IDS_VARIABLE_WITH));
		}

	return FALSE;
	}

void CControlVariable::OnNewIdent(DWORD dwIdent)
{
	m_pVariable->SetHandle(dwIdent);

	CStratonVariableDescriptor Desc( GetProject(), GetHandle());

	CStratonDataTypeDescriptor  Type(GetProject(), Desc.m_dwType);

	CStratonGroupDescriptor    Group(GetProject(), Desc.m_dwGroup);

	m_TypeIdent  = Desc.m_dwType;

	m_TypeName   = Type.m_Name;

	m_Extent     = Desc.m_dwDim;

	m_Length     = Desc.m_dwLen;

	m_Persist    = Group.IsRetentive();
	}

// Attributes

DWORD CControlVariable::GetHandle(void)
{
	return m_pVariable ? m_pVariable->GetHandle() : 0;
	}

DWORD CControlVariable::GetProject(void)
{
	return m_pVariable ? m_pVariable->GetProject() : 0;
	}

DWORD CControlVariable::GetGroup(DWORD dwProject)
{
	if( m_pVariable ) {

		return m_pVariable->GetGroup();
		}

	if( m_pGroup->m_Name.IsEmpty() ) {

		return afxDatabase->FindGroup(dwProject, m_Persist ? L"(Retain)" : L"(Global)");
		}

	return afxDatabase->FindGroup(dwProject, m_pGroup->m_Name);
	}

// UI Creation

CViewWnd * CControlVariable::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		CUIPageList *pList = New CUIPageList;

		return New CControlVariableView(pList);
		}

	return NULL;
	}

// UI Creation

BOOL CControlVariable::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CControlVariablePage(this, CString(IDS_PROPERTIES), 1));

	#if defined(_DEBUG)

	pList->Append(New CUIStdPage(CString(IDS_INFORMATION), AfxPointerClass(this), 4));

	#endif

	return FALSE;
	}

// UI Update

void CControlVariable::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		
		UpdateEmbed();
		}

	if( Tag == L"Value" ) {

		if( !pHost->InReplay() ) {

			if( m_pProject->m_AutoName ) {

				UINT uCode = m_pValue ? IDM_CTRL_AUTONAME : IDM_CTRL_UNDONAME;

				afxMainWnd->SendMessage(WM_COMMAND, uCode);
				}
			}

		if( m_pValue ) {

			if( !m_pValue->IsWritable() ) {

				MakeReadOnly(pHost);
				}
			}

		CheckPersist(pHost);

		UpdateEmbed();

		DoEnables(pHost);
		}

	if( Tag == L"Persist" ) {

		pHost->UpdateUI("GroupIdent");

		pHost->UpdateUI("Ident");

		m_pInitial->OnUIChange(pHost, pItem, L"Data");

		DoEnables(pHost);
		}

	if( Tag == L"Extent" ) {

		if( pHost->InReplay() ) {

			CStratonVariableDescriptor Desc(GetProject(), GetHandle());

			Desc.m_dwDim = m_Extent;

			pHost->UpdateUI("Extent");
			}

		CheckInitial(pHost);

		Check64BitValue(pHost);
		}

	if( Tag == L"TypeIdent" ) {

		CheckInitial(pHost);

		Check64BitValue(pHost);

		if( TRUE ) {

			// Check Data Type

			CStratonVariableDescriptor Desc(GetProject(), GetHandle());

			m_Length = Desc.m_dwLen;

			pHost->UpdateUI("Length");
			}

		DoEnables(pHost);
		}

	if( Tag == "Flags" ) {
			
		UpdateEmbed();
		}

	CControlObject::OnUIChange(pHost, pItem, Tag);
	}

// Name Hint

BOOL CControlVariable::GetNameHint(CString Tag, CString &Name)
{
	if( Tag == L"Value" ) {

		DWORD dwProject  = m_pVariable->GetProject();

		DWORD dwVariable = m_pVariable->GetHandle();

		CStratonVariableDescriptor Desc(dwProject, dwVariable);

		CString Group = Desc.GetGroupName();

		Name.Empty();

		if( !Group.IsEmpty() ) {

			if( !Group.CompareC(L"(Global)") ) {
				
				Group = L"Project";
				}

			if( !Group.CompareC(L"(Retain)") ) {
				
				Group = L"Project";
				}
			
			Name += Group;

			Name += L".";
			}

		Name += m_Name;

		return TRUE;
		}

	return FALSE;
	}

// Type Access

BOOL CControlVariable::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		DWORD dwProject = m_pVariable->GetProject();

		CStratonDataTypeDescriptor Desc(dwProject, m_TypeIdent);

		if( Desc.IsBool() ) {

			Type.m_Type  = typeLogical;

			Type.m_Flags = flagSoftWrite | flagInherent;

			if( m_Extent ) {
			
				Type.m_Flags |= flagArray;
				}

			return TRUE;
			}

		if( Desc.IsReal() ) {

			Type.m_Type  = typeReal;

			Type.m_Flags = flagSoftWrite | flagInherent;

			if( m_Extent ) {
			
				Type.m_Flags |= flagArray;
				}

			return TRUE;
			}

		if( Desc.IsNumeric() ) {

			Type.m_Type  = typeNumeric;

			Type.m_Flags = flagSoftWrite | flagInherent;

			if( m_Extent ) {
			
				Type.m_Flags |= flagArray;
				}

			return TRUE;
			}

		if( Desc.IsString() ) {

			Type.m_Type  = typeString;

			Type.m_Flags = flagSoftWrite | flagInherent;

			if( m_Extent ) {
			
				Type.m_Flags |= flagArray;
				}

			return TRUE;
			}
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// Item Naming

CString CControlVariable::GetItemOrdinal(void) const
{
	return GetHumanName();
	}

// Persistance

void CControlVariable::Init(void)
{		
	CControlObject::Init();

	FindGroup();

	Bind();

	SetInitialClass();
	}

void CControlVariable::Load(CTreeFile &File)
{		
	CControlObject::Load(File);

	FindGroup();
	}

void CControlVariable::PostLoad(void)
{		
	AfxAssert(m_pGroup);

	Bind();

	CControlObject::PostLoad();

	SetInitialClass();
	}

void CControlVariable::Kill(void)
{
	CControlObject::Kill();

	if( m_pVariable ) {

		m_pVariable->Delete();
		}
	}

// Download Support

BOOL CControlVariable::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);

	return TRUE;
	}

// Property Filters

BOOL CControlVariable::SaveProp(CString const &Tag) const
{
	if( Tag == L"TypeIdent" ){
		
		return FALSE;
		}

	return CMetaItem::SaveProp(Tag);
	}

// Meta Data Creation

void CControlVariable::AddMetaData(void)
{
	CControlObject::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddInteger(Extent);
	Meta_AddInteger(Persist);
	Meta_AddInteger(TypeIdent);
	Meta_AddString (TypeName);
	Meta_AddInteger(Length);
	Meta_AddInteger(Flags);
	Meta_AddInteger(Private);
	Meta_AddVirtual(Initial);

	Meta_SetName((IDS_CONTROL_VARIABLE));
	}

// Binding

void CControlVariable::Bind(void)
{
	if( !m_pVariable && !m_Name.IsEmpty() ) {

		DWORD dwProject = m_pProject->GetHandle();

		DWORD dwGroup   = 0;
	
		m_pVariable = New CStratonVariable;

		m_pVariable->SetProject(dwProject);	

		if( m_pGroup->m_Name.IsEmpty() ) {

			dwGroup = afxDatabase->FindGroup(dwProject, m_Persist ? L"(Retain)" : L"(Global)");
			}
		else
			dwGroup = afxDatabase->FindGroup(dwProject, m_pGroup->m_Name);

		if( m_pVariable->Connect(dwGroup, m_Name) ) {

			CStratonVariableDescriptor  Desc(dwProject, GetHandle());

			CStratonDataTypeDescriptor  Type(dwProject, Desc.m_dwType);

			CStratonGroupDescriptor    Group(dwProject, dwGroup);

			m_TypeIdent = Desc.m_dwType;

			m_TypeName  = Type.m_Name;

			m_Flags     = Desc.m_dwFlags;

			m_Length    = Desc.m_dwLen;

			m_Persist   = Group.IsRetentive();
			}
		else {
			if( !m_TypeName.IsEmpty() ) {

				DWORD dwType = afxDatabase->FindType(dwProject, m_TypeName);

				if( m_pVariable->Create( m_Name, 
							 dwGroup, 
							 dwType, 
							 m_Extent, 
							 m_Flags
							 ) ) {

					CStratonVariableDescriptor Desc(dwProject, GetHandle());

					CStratonDataTypeDescriptor Type(dwProject, dwType);

					CStratonGroupDescriptor   Group(dwProject, dwGroup);

					m_TypeIdent = Desc.m_dwType;

					m_TypeName  = Type.m_Name;

					m_Extent    = Desc.m_dwDim;

					m_Length    = Desc.m_dwLen;
				
					m_Persist   = Group.IsRetentive();
					}
				else {
					delete m_pVariable;

					m_pVariable = NULL;

					AfxAssert(FALSE);
					}
				}
			else {
				delete m_pVariable;

				m_pVariable = NULL;

				AfxAssert(FALSE);
				}
			}
		}
	}

// Implementation

void CControlVariable::FindGroup(void)
{
	m_pGroup = (CGroupVariables *) GetParent(AfxRuntimeClass(CGroupVariables));
	}

void CControlVariable::DoEnables(IUIHost *pHost)
{
	CStratonDataTypeDescriptor Type(GetProject(), m_TypeIdent);

	BOOL  f64Bit = Type.Is64Bit();

	BOOL fString = Type.IsString();

	BOOL  fBasic = Type.IsBasic();

	BOOL fAtomic = fBasic || fString;

	BOOL fIsTag  = m_pValue ? m_pValue->IsTagRef() : FALSE;

	BOOL fWrite  = m_pValue ? m_pValue->IsWritable() : TRUE;

	BOOL fParam  = IsKindOf(AfxRuntimeClass(CControlInOutVariable));

	pHost->EnableUI("Value",     fAtomic && !f64Bit);

	pHost->EnableUI("TypeIdent", fAtomic);

	pHost->EnableUI("Length",    fString);

	pHost->EnableUI("Extent",    fAtomic);
	
	pHost->EnableUI("Persist",   fAtomic);

	pHost->EnableUI("Flags",     fIsTag || (!fParam && fWrite) );

	pHost->EnableUI("Initial",   fAtomic && m_Persist == 0);
	}

BOOL CControlVariable::MakeReadOnly(IUIHost *pHost)
{
	if( !(m_Flags & attrReadOnly) ) {

		m_Flags |= attrReadOnly;

		CStratonVariableDescriptor Var(GetProject(), GetHandle());

		Var.m_dwFlags |= attrReadOnly;

		AfxAssert(Var.Set());

		pHost->UpdateUI(this, "Flags");

		return TRUE;
		}

	return FALSE;
	}

BOOL CControlVariable::CheckPersist(IUIHost *pHost)
{
	DWORD dwProject = GetProject();

	DWORD dwVariable = GetHandle();

	DWORD  dwRetain = afxDatabase->FindGroup(dwProject, L"(Retain)");

	DWORD  dwGlobal = afxDatabase->FindGroup(dwProject, L"(Global)");

	if( m_Persist ) {

		if( !m_pValue ) {

			return TRUE;
			}

		// retain => global

		CStratonVariableSerializeBuffer Var(dwProject, dwRetain, dwVariable);

		if( Var.Serialize() && Var.DeleteVar() ) {

			DWORD dwIdent;

			if( (dwIdent = Var.Paste(dwGlobal)) ) {
				
				OnNewIdent(dwIdent);

				pHost->UpdateUI("Ident");

				pHost->UpdateUI("Persist");

				pHost->UpdateUI("GroupIdent");
				
				return TRUE;
				}

			AfxAssert(FALSE);
			}
		}

	return FALSE;
	}

void CControlVariable::CheckInitial(IUIHost *pHost)
{
	if( !pHost->InReplay() ) {

		DWORD dwProject = m_pProject->GetHandle();

		CStratonDataTypeDescriptor Type(dwProject, m_TypeIdent);

		CLASS Class = CVariableInitial::GetClass(Type, m_Extent > 0);

		if( Class && Class == AfxPointerClass(m_pInitial) ) {

			if( m_pInitial->m_Type == Type.m_Name ) {

				return;
				}
			}

		HGLOBAL hPrev = m_pInitial ? m_pInitial->TakeSnapshot() : NULL;

		SetInitialClass(m_pInitial, Class);

		HGLOBAL hData = m_pInitial ? m_pInitial->TakeSnapshot() : NULL;

		CCmd *  pCmd  = New CCmdSubItem(L"Initial", hPrev, hData);

		pHost->SaveExtraCmd(pCmd);
		}

	pHost->SendUpdate(updateValue);

	pHost->RemakeUI();
	}

void CControlVariable::Check64BitValue(IUIHost *pHost)
{
	CStratonDataTypeDescriptor Type(GetProject(), m_TypeIdent);

	if( Type.Is64Bit() ) {

		if( m_pValue ) {
					
			m_pValue->Empty();

			pHost->UpdateUI("Value");
			}
		}
	}

void CControlVariable::UpdateEmbed(void)
{
	DWORD  dwProject = GetProject();

	DWORD dwVariable = GetHandle();

	CStratonProperties Props(dwProject, dwVariable);

	if( m_pValue ) {

		CString VarEmbed;

		VarEmbed += L"(";

		if( TRUE ) {

			DWORD dwRef = 0;

			if( m_pValue ) {

				if( !m_pValue->IsBroken() ) {

					dwRef = m_pValue->GetRef(0).m_Ref;
					}				
				}

			VarEmbed += CPrintf(L"Src=%d", dwRef);
			}

		if( TRUE ) {

			VarEmbed += L"\t";

			VarEmbed += CPrintf(L"Acc=%d", m_Flags);
			}
		
		VarEmbed += L")";

		Props.Set(propVarEmbed, VarEmbed);

		//

		CString Profile(L"Crimson");

		Props.Set(propVarProfile, Profile);
		
		return;
		}

	Props.Set(propVarEmbed,   L"");

	Props.Set(propVarProfile, L"");
	}

BOOL CControlVariable::SetInitialClass(CVariableInitial * &pInitial, CLASS Class)
{
	CVariableInitial *pOld = pInitial;

	if( Class != (pOld ? AfxPointerClass(pOld) : NULL) ) {

		if( Class ) {

			pInitial = AfxNewObject(CVariableInitial, Class);

			pInitial->SetParent(this);

			pInitial->Init();

			if( pOld ) {

				pInitial->Preserve(pOld);
				}
			}
		else {
			pInitial = NULL;

			DWORD  dwProject = GetProject();

			DWORD dwVariable = GetHandle();

			DWORD    dwGroup = GetGroup(dwProject);

			afxDatabase->SetVarInitValue(dwProject,
						     dwGroup,
						     dwVariable,
						     L""
						     );
		}

		if( pOld ) {

			pOld->Kill();

			delete pOld;
			}
		}

	return TRUE;
	}

void CControlVariable::SetInitialClass(void)
{
	DWORD dwProject = m_pProject->GetHandle();

	CStratonDataTypeDescriptor Type(dwProject, m_TypeIdent);

	CLASS Class = CVariableInitial::GetClass(Type, m_Extent > 0);

	SetInitialClass(m_pInitial, Class);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable
//

// Dynamic Class

AfxImplementDynamicClass(CControlGlobalVariable, CControlVariable);

// Constructor

CControlGlobalVariable::CControlGlobalVariable(void)
{
	m_Flags = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable
//

// Dynamic Class

AfxImplementDynamicClass(CControlLocalVariable, CControlVariable);

// Constructor

CControlLocalVariable::CControlLocalVariable(void)
{
	m_Flags = 0;
	}

// UI Creation

BOOL CControlLocalVariable::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CControlVariablePage(this, CString(IDS_PROPERTIES), 3));

	#if defined(_DEBUG)

	pList->Append(New CUIStdPage(CString(IDS_INFORMATION), AfxPointerClass(this), 4));

	#endif

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable
//

// Dynamic Class

AfxImplementDynamicClass(CControlInOutVariable, CControlVariable);

// Constructor

CControlInOutVariable::CControlInOutVariable(void)
{
	m_Flags = 4;
	}

// UI Creation

BOOL CControlInOutVariable::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CControlVariablePage(this, CString(IDS_PROPERTIES), 1));

	#if defined(_DEBUG)

	pList->Append(New CUIStdPage(CString(IDS_INFORMATION), AfxPointerClass(this), 4));

	#endif

	return FALSE;
	}

// End of File
