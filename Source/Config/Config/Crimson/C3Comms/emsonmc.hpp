
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//
#include "emsonep.hpp"

#ifndef	INCLUDE_EMERSONMC_HPP

#define	INCLUDE_EMERSONMC_HPP

#define	BB	addrBitAsBit
#define	WW	addrWordAsWord

#define	SPHOLD	1
#define	SPOUT	3
#define	SPINP	4

//////////////////////////////////////////////////////////////////////////
//
// Emerson MC Device Options
//

class CEmersonMCDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonMCDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Unit;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson MC Driver
//

class CEmersonMCDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEmersonMCDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Device Configuration
		CLASS	GetDriverConfig(void);
		CLASS	GetDeviceConfig(void);

	protected:
 		// Data

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
