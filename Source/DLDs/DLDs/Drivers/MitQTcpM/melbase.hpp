//////////////////////////////////////////////////////////////////////////
//
//  Constants
//

#define MITQ_WORD	0
#define MITQ_BIT	1

#define CMD_READ	0x0401
#define CMD_WRITE	0x1401

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Melsec Base Driver
//

class CMelBaseMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CMelBaseMasterDriver(void);

		// Destructor
		~CMelBaseMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Base Context
		struct CBaseCtx
		{
			BYTE m_fCode;
			};

	protected:
		// Device Data
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		BYTE	   m_bCheck;
		CBaseCtx * m_pBase;
		LPCTXT 	   m_pHex;
		BYTE	   m_DLE;

		// Implementation
		CCODE GetBits(PDWORD pData, UINT uCount);
		CCODE GetASCIIBits(PDWORD pData, UINT uCount);
		BOOL  GetBitData(PCTXT pText);
		CCODE GetWords(PDWORD pData, UINT uCount);
		CCODE GetASCIIWords(PDWORD pData, UINT uCount);
		WORD  GetWordData(PCTXT pText, UINT uCount);
		CCODE GetLongs(PDWORD pData, UINT uCount);
		CCODE GetASCIILongs(PDWORD pData, UINT uCount);
		UINT  GetSubCommand(UINT Type);
		void  SetWords(PDWORD pData, UINT uCount);
		void  SetASCIIWords(PDWORD pData, UINT uCount);
		void  SetLongs(PDWORD pData, UINT uCount);
		void  SetASCIILongs(PDWORD pData, UINT uCount);
		void  SetBits(PDWORD pData, UINT uCount);
		void  SetASCIIBits(PDWORD pData, UINT uCount);
		BOOL  IsSpecial(UINT uTable);
			
		// Frame Building
		void AddByte(BYTE bValue, BOOL fDLE = FALSE);
		void AddPrefix(UINT Table);
		void AddASCIIPrefix(UINT Table);
		void AddASCIISpecialPrefix1(UINT uTable);
		void AddASCIISpecialPrefix2(UINT uTable);
		void AddBinaryPrefix(UINT Table);
		void AddHex(UINT uValue, UINT uMask);
		void AddDec(UINT uValue, UINT uMask);
		void AddWord(WORD Word);
		void AddLong(DWORD dwWord);

		// Overridables
		virtual BOOL Start(void);
		virtual	void AddID(void);
		virtual void AddCommand(UINT uType, UINT uCommand, UINT uCount);
		virtual void AddAddress(AREF Addr);
		virtual void AddOffset(UINT uOffset);
		virtual void AddCount(UINT uCount);
	       	virtual void GetCount(UINT &uCount, UINT uType, BOOL fWrite);
		virtual WORD GetCommand(UINT uType, BOOL fWrite);
		virtual BOOL Transact(UINT uTotal);
	};
		

// End of File
