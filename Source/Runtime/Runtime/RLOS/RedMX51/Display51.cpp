
#include "Intern.hpp"

#include "Display51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Gpio51.hpp"

#include "Ipu51.hpp"

#include "Pwm51.hpp"

#include "../RedDev/Identity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Display Driver
//

// Instantiator

IDevice * Create_Display51(UINT uModel, CIpu51 *pIpu, CPwm51 *pPwm)
{
	CDisplay51 *p = New CDisplay51(uModel, pIpu, pPwm);

	p->Open();

	return (IDevice *) (IDisplay *) p;
	}

// Constructor

CDisplay51::CDisplay51(UINT uModel, CIpu51 *pIpu, CPwm51 *pPwm)
{
	AfxGetObject("ident", 0, IIdentity, m_pIdentity);

	AfxGetObject("gpio",  0, IGpio, m_pGpioBacklight);

	AfxGetObject("gpio",  3, IGpio, m_pGpioDisplayOn);

	m_uType          = uModel;

	m_pPwmBacklight  = pPwm;

	m_uLineBacklight = 5;

	m_uLineDisplayOn = 9;

	m_pIpu           = pIpu;

	m_pIdentity      = NULL;

	FindDisplay();
	}

// Destructor

CDisplay51::~CDisplay51(void)
{
	free(m_pBuff);

	AfxRelease(m_pIdentity);

	AfxRelease(m_pGpioBacklight);

	AfxRelease(m_pGpioDisplayOn);
	}

// IDevice

BOOL METHOD CDisplay51::Open(void)
{
	if( CDisplaySoftPoint::Open() ) {

		m_pPwmBacklight->SetFreq(m_uType == MODEL_GRAPHITE_07 ? 20000 : 200);

		m_pGpioDisplayOn->SetDirection(m_uLineDisplayOn, true);

		m_pGpioDisplayOn->SetState(m_uLineDisplayOn, true);

		phal->SpinDelay(200);
	
		EnableBacklight(true);

		SetBacklight(m_nBacklight = LoadBacklight(100));
	
		return TRUE;
		}
	
	return FALSE;
	}

// IDisplay

void METHOD CDisplay51::Update(PCVOID pData)
{
	Claim();

	if( m_pBuff ) {

		LockPointer(true);

		SavePointer(PDWORD(pData));

		DrawPointer(PDWORD(pData));

		ArrayCopy(m_pBuff, PDWORD(pData), m_cx * m_cy);

		LoadPointer(PDWORD(pData));
	
		m_pIpu->Update();

		LockPointer(false);
		}

	Free();
	}

BOOL METHOD CDisplay51::SetBacklight(UINT nPercent)
{
	BOOL fTask = (Hal_GetIrql() < IRQL_HARDWARE);

	CAutoGuard Guard(fTask);

	m_nBacklight = nPercent;

	if( m_pIdentity ) {

		UINT uAdjust = m_pIdentity->GetPropWord(CIdentity::propBacklight);

		if( uAdjust != 0 ) {

			nPercent = (nPercent * uAdjust) / 100;
			}
		}

	if( fTask ) {

		// Calls at higher levels are from flashing the display
		// from the timer routine, and there is no point saving
		// that brightness setting.

		SaveBacklight(m_nBacklight);
		}

	m_pPwmBacklight->SetDuty(nPercent);

	return TRUE;
	}

UINT METHOD CDisplay51::GetBacklight(void)
{
	return m_nBacklight;
	}

BOOL METHOD CDisplay51::EnableBacklight(BOOL fOn)
{
	CAutoGuard Guard;

	m_pGpioBacklight->SetDirection(m_uLineBacklight, true);

	m_pGpioBacklight->SetState(m_uLineBacklight, m_fBacklight = fOn);
	
	return TRUE;
	}

BOOL METHOD CDisplay51::IsBacklightEnabled(void)
{
	return m_fBacklight;
	}

// Implementation

void CDisplay51::FindDisplay(void)
{
	CDisplayConfig Config;

	switch( m_uType ) {

              case MODEL_GRAPHITE_07:
                     
			// Hantronix-HDA700L 7"
       
			Config.m_xSize       = 800;
			Config.m_ySize       = 480;
			Config.m_uHorzFront  = 85;
			Config.m_uHorzSync   = 86; 
			Config.m_uHorzBack   = 85;
			Config.m_uVertFront  = 15;
			Config.m_uVertSync   = 15;
			Config.m_uVertBack   = 15;
			Config.m_uFrequency  = 33250000;
       
			break;

		case MODEL_GRAPHITE_09:
                     
			// NEC-NL8048BC24-09D  9"
       
			Config.m_xSize       = 800;
			Config.m_ySize       = 480;
			Config.m_uHorzFront  = 85;
			Config.m_uHorzSync   = 86; 
			Config.m_uHorzBack   = 85;
			Config.m_uVertFront  = 15;
			Config.m_uVertSync   = 15;
			Config.m_uVertBack   = 15;
			Config.m_uFrequency  = 33250000;
       
			break;

		case MODEL_GRAPHITE_10V:
                     
			// NEC-NL6448BC33-71D 10" VGA

			Config.m_xSize       = 640;
			Config.m_ySize       = 480;
			Config.m_uHorzFront  = 68;
			Config.m_uHorzSync   = 68; 
			Config.m_uHorzBack   = 68;
			Config.m_uVertFront  = 15;
			Config.m_uVertSync   = 15;
			Config.m_uVertBack   = 15;
			Config.m_uFrequency  = 26600000;
       
			break;

		case MODEL_GRAPHITE_10S:
                     
			// NEC-NL8060BC26-35D 10" SVGA

			Config.m_xSize       = 800;
			Config.m_ySize       = 600;
			Config.m_uHorzFront  = 29;
			Config.m_uHorzSync   = 30; 
			Config.m_uHorzBack   = 29;
			Config.m_uVertFront  = 8;
			Config.m_uVertSync   = 8;
			Config.m_uVertBack   = 8;
			Config.m_uFrequency  = 33250000;
       
			break;

		case MODEL_GRAPHITE_12:
                     
			// NEC-NL12880BC20-05 12" WXGA
       
			Config.m_xSize       = 1280;
			Config.m_ySize       = 800;
			Config.m_uHorzFront  = 40;
			Config.m_uHorzSync   = 23;
			Config.m_uHorzBack   = 40;
			Config.m_uVertFront  = 7;
			Config.m_uVertSync   = 9;
			Config.m_uVertBack   = 7;
			Config.m_uFrequency  = 66500000;
       
			break;

		case MODEL_GRAPHITE_15:
                     
			// NEC-NL10276BC30-34D 15" XGA
       
			Config.m_xSize       = 1024;
			Config.m_ySize       = 768;
			Config.m_uHorzFront  = 117;
			Config.m_uHorzSync   = 117; 
			Config.m_uHorzBack   = 117;
			Config.m_uVertFront  = 12;
			Config.m_uVertSync   = 13;
			Config.m_uVertBack   = 12;
			Config.m_uFrequency  = 66500000;
       
			break;

		default:

			Config.m_xSize	    = 0;
			Config.m_ySize	    = 0;
			Config.m_uHorzFront = 0;
			Config.m_uHorzSync  = 0; 
			Config.m_uHorzBack  = 0;
			Config.m_uVertFront = 0;
			Config.m_uVertSync  = 0;
			Config.m_uVertBack  = 0;
			Config.m_uFrequency = 0;

			break;
		}
	
	m_cx    = Config.m_xSize;
	
	m_cy    = Config.m_ySize;

	m_pBuff = PDWORD(memalign(32, m_cx * m_cy * 4));

	m_pIpu->SetFrame(PBYTE(m_pBuff));

	m_pIpu->Configure(Config);

	ArrayZero(m_pBuff, m_cx * m_cy);
	}

// End of File
