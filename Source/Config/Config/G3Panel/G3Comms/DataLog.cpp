
#include "Intern.hpp"

#include "DataLog.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"

#include "TagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Log Item
//

// Dynamic Class

AfxImplementDynamicClass(CDataLog, CCodedHost);

// Constructor

CDataLog::CDataLog(void)
{
	m_Type      = 0;

	m_Update    = 600;

	m_FileLimit = 60 * 24;

	m_FileCount = 7;

	m_WithBatch = FALSE;

	m_SignLogs  = FALSE;

	m_Comments  = FALSE;

	m_Merge     = FALSE;

	m_pEnable   = NULL;

	m_pTrigger  = NULL;

	m_pSet      = New CTagSet;

	m_pMon      = New CTagSet;

	m_Mail      = NOTHING;

	m_Drive	    = 0;
}

// UI Management

CViewWnd * CDataLog::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return CreateItemView(FALSE);
	}

	return NULL;
}

// UI Creation

BOOL CDataLog::OnLoadPages(CUIPageList *pList)
{
	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		pList->Append(New CUIStdPage(CString(IDS_SETUP), AfxThisClass(), 1));

		pList->Append(New CUIStdPage(CString(IDS_CONTENTS), AfxThisClass(), 2));

		pList->Append(New CUIStdPage(CString(IDS_MONITOR), AfxThisClass(), 3));

		return TRUE;
	}

	pList->Append(New CUIStdPage(AfxThisClass(), 4));

	return FALSE;
}

// UI Update

void CDataLog::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag == "Path" ) {

		CString Out;

		for( UINT i = 0; m_Path[i]; i++ ) {

			WCHAR c = m_Path[i];

			if( c < 255 ) {

				if( isalpha(c) || isdigit(c) || c == '-' || c == '_' ) {

					Out += c;
				}
			}
		}

		if( Out.GetLength() > 8 ) {

			if( !pHost->InReplay() ) {

				pHost->GetWindow().Information(CString(IDS_NAMES_LONGER_THAN_2), L"LongLogName");
			}
		}

		if( m_Path != Out ) {

			m_Path = Out;

			pHost->UpdateUI(this, Tag);
		}
	}

	if( Tag == "Type" ) {

		DoEnables(pHost);
	}

	if( Tag == "Comments" ) {

		if( !m_Comments ) {

			if( m_Merge ) {

				m_Merge = FALSE;

				pHost->UpdateUI("Merge");
			}
		}

		DoEnables(pHost);
	}

	if( Tag == "Merge" ) {

		DoEnables(pHost);
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CDataLog::GetTypeData(CString Tag, CTypeDef &Type)
{
	return FALSE;
}

// Item Naming

CString CDataLog::GetHumanName(void) const
{
	return m_Name;
}

CString CDataLog::GetItemOrdinal(void) const
{
	return CPrintf(IDS_LOG_FMT, GetIndex());
}

// Download Support

BOOL CDataLog::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	if( !C3OemFeature(L"OemSD", FALSE) ) {

		Init.AddText(m_Name);

		if( m_Path.IsEmpty() ) {

			// REV3 -- Trim this name to be valid in 8.3 format,
			// and make sure we don't get any duplicated.

			Init.AddText(m_Name);
		}
		else
			Init.AddText(m_Path);
	}
	else {
		Init.AddText(m_Name);

		Init.AddText(m_Path);
	}

	Init.AddByte(BYTE(m_Type));
	Init.AddLong(LONG(m_Update));
	Init.AddLong(LONG(m_FileLimit));
	Init.AddWord(WORD(m_FileCount));
	Init.AddByte(BYTE(m_WithBatch));
	Init.AddByte(BYTE(m_SignLogs));
	Init.AddByte(BYTE(m_Comments));

	Init.AddItem(itemVirtual, m_pEnable);

	Init.AddItem(itemVirtual, m_pTrigger);

	Init.AddItem(itemSimple, m_pSet);

	if( m_Merge ) {

		Init.AddByte(BYTE(1));

		Init.AddItem(itemSimple, m_pMon);
	}
	else
		Init.AddByte(BYTE(0));

	Init.AddByte(BYTE(m_Drive));

	return TRUE;
}

// Meta Data Creation

void CDataLog::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Name);
	Meta_AddString(Path);
	Meta_AddInteger(Type);
	Meta_AddInteger(Update);
	Meta_AddInteger(FileLimit);
	Meta_AddInteger(FileCount);
	Meta_AddInteger(WithBatch);
	Meta_AddInteger(SignLogs);
	Meta_AddInteger(Comments);
	Meta_AddInteger(Merge);
	Meta_AddVirtual(Enable);
	Meta_AddVirtual(Trigger);
	Meta_AddObject(Set);
	Meta_AddObject(Mon);
	Meta_AddInteger(Mail);
	Meta_AddInteger(Drive);

	Meta_SetName((IDS_DATA_LOG));
}

// Implementation

void CDataLog::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("Trigger", m_Type);

	pHost->EnableUI("Merge", m_Comments);

	pHost->EnableUI("Mon", m_Merge && m_Comments);
}

// End of File
