
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Abstract Data Item
//

// Runtime Class

AfxImplementRuntimeClass(CItem, CObject);

// Static Data

static DWORD zdi = 0;

static DWORD seq = 0;

// Constructor

CItem::CItem(void)
{
	while( !zdi ) {

		zdi = (rand() ^ GetTickCount());
		}

	m_zdi     = zdi;

	m_seq     = seq++;

	m_pDbase  = NULL;

	m_pParent = NULL;

	m_uPos    = NOTHING;
	}

// Destructor

CItem::~CItem(void)
{
	m_zdi = 0;
	}

// Attributes

CDatabase * CItem::GetDatabase(void) const
{
	return m_pDbase;
	}

CItem * CItem::GetParent(void) const
{
	return m_pParent;
	}

CItem * CItem::GetParent(UINT uLevel) const
{
	CItem const *pItem = this;

	while( uLevel-- ) {

		pItem = pItem->GetParent();

		AfxAssert(pItem);
		}

	return (CItem *) pItem;
	}

CItem * CItem::GetParent(CLASS Class) const
{
	CItem const *pItem = this;

	while( !pItem->IsKindOf(Class) ) {

		pItem = pItem->GetParent();

		AfxAssert(pItem);
		}

	return (CItem *) pItem;
	}

CItem * CItem::HasParent(CLASS Class) const
{
	CItem const *pItem = this;

	while( !pItem->IsKindOf(Class) ) {

		if( !(pItem = pItem->GetParent()) ) {

			break;
			}
		}

	return (CItem *) pItem;
	}

UINT CItem::GetIndex(void) const
{
	return m_uPos;
	}

CString CItem::GetHumanPath(void) const
{
	CString Name = GetHumanName();

	if( !IsHumanRoot() && m_pParent ) {

		CString Root = m_pParent->GetHumanPath();

		PTXT    pSep = L" - ";

		if( Name.IsEmpty() ) {

			return Root;
			}

		return Root.IsEmpty() ? Name : (Root + pSep + Name);
		}

	return Name;
	}

CString CItem::GetFixedPath(void) const
{
	if( m_pParent ) {

		CString Root = m_pParent->GetFixedPath();

		CString Name = GetFixedName();

		PTXT    pSep = L"/";

		return Root.IsEmpty() ? Name : (Root + pSep + Name);
		}

	return GetFixedName();
	}

CString CItem::GetFindInfo(void) const
{
	CString Text;

	Text += GetHumanPath();

	Text += '\n';

	Text += GetFixedPath();

	return Text;
	}

// Operations

void CItem::SetDatabase(CDatabase *pDbase)
{
	m_pDbase = pDbase;
	}

void CItem::SetParent(CItem *pParent)
{
	m_pDbase  = pParent ? pParent->GetDatabase() : NULL;

	m_pParent = pParent;
	}

void CItem::SetDirty(void)
{
	if( m_pDbase ) {

		m_pDbase->SetDirty();
		}

	for( CItem *pScan = this; pScan; pScan = pScan->GetParent() ) {

		pScan->OnSetDirty();
		}
	}

void CItem::SetIndex(UINT uPos)
{
	m_uPos = uPos;
	}

// Snapshot Imaging

HGLOBAL CItem::TakeSnapshot(void)
{
	return TakeSnapshot(GetClassName());
}

HGLOBAL CItem::TakeSnapshot(PCTXT pClass)
{
	CTextStreamMemory Stream;

	if( Stream.OpenSave() ) {

		CTreeFile Tree;

		if( Tree.OpenSave(Stream) ) {

			if( IsKindOf(AfxRuntimeClass(CItemList)) ) {

				Tree.PutCollect(GetClassName());

				PreSnapshot();

				Save(Tree);

				Tree.EndCollect();
				}
			else {
				Tree.PutObject(pClass);

				PreSnapshot();

				Save(Tree);

				Tree.EndObject();
				}

			Tree.Close();

			return Stream.TakeOver();
			}
		}

	return NULL;
	}

BOOL CItem::LoadSnapshot(HGLOBAL hBuffer)
{
	CTextStreamMemory Stream;

	if( Stream.OpenLoad(hBuffer) ) {

		CTreeFile Tree;

		if( Tree.OpenLoad(Stream) ) {

			if( !Tree.IsEndOfData() ) {

				if( Tree.GetName() == GetClassName() ) {

					if( IsKindOf(AfxRuntimeClass(CItemList)) ) {

						Tree.GetCollect();

						Kill();

						Load(Tree);

						PostLoad();

						Tree.EndCollect();
						}
					else {
						Tree.GetObject();

						Kill();

						Load(Tree);

						PostLoad();

						Tree.EndObject();
						}

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Snapshot Imaging

CItem * CItem::MakeFromSnapshot(CItem *pParent, HGLOBAL hBuffer)
{
	CTextStreamMemory Stream;

	if( Stream.OpenLoad(hBuffer) ) {

		CTreeFile Tree;

		if( Tree.OpenLoad(Stream) ) {

			if( !Tree.IsEndOfData() ) {

				CString Name = Tree.GetName();
				
				if( Name[0] == 'C' ) {

					CLASS Class  = AfxNamedClass(CEntity(Name));

					if( Class ) {

						CItem *pItem = AfxNewObject(CItem, Class);

						if( pItem ) {

							if( pItem->IsKindOf(AfxRuntimeClass(CItemList)) ) {

								Tree.GetCollect();

								pItem->SetParent(pParent);

								pItem->Load(Tree);

								pItem->PostLoad();

								Tree.EndCollect();
								}
							else {
								Tree.GetObject();

								pItem->SetParent(pParent);

								pItem->Load(Tree);

								pItem->PostLoad();

								Tree.EndObject();
								}

							return pItem;
							}
						}
					}
				}
			}
		}

	return NULL;
	}

CItem * CItem::MakeFromItem(CItem *pParent, CItem *pSource)
{
	if( pSource ) {

		HGLOBAL hData = pSource->TakeSnapshot();

		CItem * pItem = MakeFromSnapshot(pParent, hData);

		GlobalFree(hData);

		return pItem;
		}

	return NULL;
	}

// Item Naming

BOOL CItem::IsHumanRoot(void) const
{
	return FALSE;
	}

CString CItem::GetHumanName(void) const
{
	return L"";
	}

CString CItem::GetItemOrdinal(void) const
{
	return L"";
	}

CString CItem::GetFixedName(void) const
{
	AfxAssert(FALSE);

	return L"";
	}

void CItem::SetFixedName(CString Name)
{
	AfxAssert(FALSE);
	}

// Item Privacy

BOOL CItem::HasPrivate(void)
{
	return FALSE;
	}

UINT CItem::GetPrivate(void)
{
	return 0;
	}

// File Padding

UINT CItem::GetPadding(void) const
{
	return 0;
	}

// Persistance

void CItem::Init(void)
{
	}

void CItem::Kill(void)
{
	}

void CItem::Load(CTreeFile &File)
{
	}

void CItem::PostLoad(void)
{
	}

void CItem::PostSave(void)
{
	}

void CItem::PreSnapshot(void)
{
	}

void CItem::PreCopy(void)
{
	}

void CItem::PostPaste(void)
{
	}

void CItem::Save(CTreeFile &File)
{
	}

// Data Access

IDataAccess * CItem::GetDataAccess(PCTXT pTag)
{
	return NULL;
	}

// Download Support

void CItem::PrepareData(void)
{
	}

BOOL CItem::MakeInitData(CInitData &Init)
{
	Init.AddWord(0x1234);

	return TRUE;
	}

void CItem::CompressData(void)
{
	}

// UI Management

CViewWnd * CItem::CreateView(UINT uType)
{
	return NULL;
	}

// Dirty Control

void CItem::OnSetDirty(void)
{
	}

// Legacy Thunks

IDataAccess * CItem::GetDataAccess(PCSTR pTag)
{
	return GetDataAccess(CString(pTag));
	}


// Validation

void CItem::ZdiCheckItem(size_t s) const
{
	if( IsBadReadPtr(this, s) || m_zdi != zdi ) {

		throw (int *) 0;
		}
	}

void CItem::ZdiCheckRead(void const *p, size_t s) const
{
	if( IsBadReadPtr(p, s) ) {

		throw (int *) 0;
		}
	}

void CItem::ZdiCheckWrite(void *p, size_t s) const
{
	if( IsBadWritePtr(p, s) ) {

		throw (int *) 0;
		}
	}

void CItem::ZdiAssert(BOOL predicate) const
{
	if( !predicate ) {

		throw (int *) 0;
		}
	}

// End of File
