// K5MW API

#if !defined(_K5MWAPI_H__INCLUDED_)
#define _K5MWAPI_H__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef K5MWDLL
	/* Build the DLL */
	#define K5MW_APICALL __declspec(dllexport)
#else
	#define K5MW_APICALL __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// return values and message IDs

#define K5MW_OK							0

#define K5MWERR_INTERNAL				1	// undocumented error
#define K5MWERR_UNKNOWNCLIENT			2	// unknown client handle
#define K5MWERR_UNKNOWNPROJECT			3	// unknown project handle
#define K5MWERR_UNKNOWNSYMBOL			4	// unknown symbol
#define K5MWERR_LOOPIDUSED				5	// loop ID already used
#define K5MWERR_UNKNOWNLOOP				6	// unknown loop ID
#define K5MWERR_NOCOMMOK				7

#define K5MWSTAT_CNXOK					8
#define K5MWSTAT_CNXINVALID				9
#define K5MWSTAT_CNXLOGON				10
#define K5MWSTAT_CNXLOGOFF				11
#define K5MWSTAT_CNXNOAPP				12
#define K5MWSTAT_CNXOTHERAPP			13
#define K5MWSTAT_CNXCOMMERROR			14
#define K5MWSTAT_APPRUN					15
#define K5MWSTAT_APPSTOP				16
#define K5MWSTAT_APPSFCBREAK			17
#define K5MWSTAT_APPERROR				18
#define K5MWSTAT_APPINVALID				19

#define K5MWPRGSTAT_INACTIVE			20
#define K5MWPRGSTAT_ACTIVE				21
#define K5MWPRGSTAT_FROZEN				22

#define M5MWBUT_FORCE					23
#define M5MWBUT_TRUE					24
#define M5MWBUT_FALSE					25
#define M5MWBUT_LOCK					26
#define M5MWBUT_UNLOCK					27
#define M5MWBUT_STARTTIMER				28
#define M5MWBUT_STOPTIMER				29
#define M5MWBUT_BREAKSTEP				30
#define M5MWBUT_REMBREAKSTEP			31
#define M5MWBUT_FORCETRANS				32
#define M5MWBUT_BREAKTRANS				33
#define M5MWBUT_REMBREAKTRANS			34
#define M5MWBUT_STARTPROG				35
#define M5MWBUT_STOPPROG				36
#define M5MWBUT_SUSPENDPROG				37
#define M5MWBUT_RESTARTPROG				38

#define K5MWERR_BADDINTVALUE			39
#define K5MWERR_BADREALVALUE			40
#define K5MWERR_BADTIMEVALUE			41
#define K5MWERR_BADSTRINGVALUE			42

#define K5MWSTAT_MISSRSC				43

#define K5MWPSW_PASSWORD                44
#define K5MWPSW_REMEMBER                45
#define K5MWPSW_OK                      46
#define K5MWPSW_CANCEL                  47

#define K5MWUP_CANTSTORE                48
#define K5MWUP_CAPTION                  49
#define K5MWUP_NOCNX                    50
#define K5MWUP_CANTUPLOAD               51
#define K5MWUP_PROGRESS                 52
#define K5MWUP_ERRORS                   53
#define K5MWUP_CANCEL                   54
#define K5MWUP_CLOSE                    55
#define K5MWUP_COMPLETE                 56

#define K5MWSTAT_APP                    57
#define K5MWSTAT_BADVERS                58
#define K5MWSTAT_APPSTEPPING            59

#define K5MWERR_FTBUSY                  60
#define K5MWERR_FTOPENREADLOCAL         61
#define K5MWERR_FTRETCODE               62
#define K5MWERR_FTFRAME                 63
#define K5MWERR_FTOPENWRITELOCAL        64
#define K5MWERR_FTWRITELOCAL            65
#define K5MWERR_FTCRC                   66
#define K5MWERR_FTCONNECTION            67
#define K5MWERR_FTREGISTERCLASS         68
#define K5MWERR_FTCREATECALLBACK        69
#define K5MWERR_FTMWCONNECT             70
#define K5MWERR_FTMWOPEN                71
#define K5MWERR_FTMWOBJ                 72
#define K5MWERR_FTTIMEOUT               73
#define K5MWERR_FTT5_UNKNOWN            74
#define K5MWERR_FTT5_OPENWRITE          75
#define K5MWERR_FTT5_WRITE              76
#define K5MWERR_FTT5_CLOSE              77
#define K5MWERR_FTT5_OPENREAD           78
#define K5MWERR_FTT5_READ               79
#define K5MWERR_FTT5_DELETE             80
#define K5MWERR_FTT5_OPENDIR            81
#define K5MWERR_FTT5_CLOSEDIR           82
#define K5MWERR_FTT5_GETDIR             83
#define K5MWERR_FTABORT                 84

#define K5MWFT_LOAD                     85
#define K5MWFT_FROM                     86
#define K5MWFT_TO                       87

#define K5MWERR_BADSYNTAX               88
#define K5MWERR_BUSY                    89

#define K5MWERR_BKPROMPT                90
#define K5MWERR_BKERROR                 91
#define K5MWERR_LKPROMPT                92
#define K5MWERR_LKERROR                 93

#define K5MWDLG_LISTLOCK                94
#define K5MWDLG_UNLOCKALL               95

#define K5MWPSW_OLD                     96
#define K5MWPSW_NEW                     97

#define K5MWSTAT_CNXIDLE                98

/////////////////////////////////////////////////////////////////////////////
// nofitications

#define K5MWEV_CHANGE					1	// variable value has changed
#define K5MWEV_STATUS					2	// comm/app status has changed
#define K5MWEV_TIMING					3	// cycle timing has changed
#define K5MWEV_PROGRESS					4	// load progress
#define K5MWEV_OBJREAD                  5   // client object is updated
#define K5MWEV_POLL                     6   // end of polling sequence
#define K5MWEV_UPLOAD                   7   // upload status has changed

#define K5MWEV_EASTATUS                 8   // event connection status
#define K5MWEV_EASUBSCRIBEFAIL          9   // event susbcribe failed (VFI)
#define K5MWEV_EAEVENT                  10  // value change event (VFI)
#define K5MWEV_EASYSEVENT               11  // system event (event ID)

#define K5MWEV_LOADCOMPLETE				12	// download complete
#define K5MWEV_CHANGEOK 				13	// on-line change complete and OK

/////////////////////////////////////////////////////////////////////////////
// compatibility

int K5MW_APICALL K5MW_GetVersion		// get API version number
	(
	void
	);									// returns the API version number

/////////////////////////////////////////////////////////////////////////////
// misc

void K5MW_APICALL K5MW_ToggleTrace		// get API version number
	(
	void
	);									// returns the API version number

/////////////////////////////////////////////////////////////////////////////
// object Identification

#define K5MWID_USER		256	// first available id for declared objects

// types of objects

#define K5MWOBJ_CLIENT	1	// client
#define K5MWOBJ_PROJECT	2	// open project
#define K5MWOBJ_UPLOAD  3   // upload connection

// predefined object names

#define K5MW_LOCALAPPNAME		"<LocalAppName>"
#define K5MW_LOCALAPPVERS		"<LocalAppVers>"
#define K5MW_LOCALAPPCODECRC	"<LocalAppCodeCrc>"
#define K5MW_LOCALAPPDATACRC	"<LocalAppDataCrc>"
#define K5MW_LOCALAPPTIMESTAMP	"<LocalAppTimeStamp>"

#define K5MW_TARGETAPPNAME		"<TargetAppName>"
#define K5MW_TARGETAPPVERS		"<TargetAppVers>"
#define K5MW_TARGETAPPCODECRC	"<TargetAppCodeCrc>"
#define K5MW_TARGETAPPDATACRC	"<TargetAppDataCrc>"
#define K5MW_TARGETAPPTIMESTAMP	"<TargetAppTimeStamp>"
#define K5MW_TARGETCODESUFFIX	"<TargetCodeSuffix>"

#define K5MW_CNXSTATUS			"<CnxStatus>"
#define K5MW_APPSTATUS			"<AppStatus>"
#define K5MW_MAINSTATUS			"<MainStatus>"
#define K5MW_APPFLAGS           "<AppFlags>"
#define K5MW_MTSIMUL            "<MTSimul>"

#define K5MW_CYCLECURRENT		"<CycleCurrent>"
#define K5MW_CYCLEALLOWED		"<CycleAllowed>"
#define K5MW_CYCLEMAXIMUM		"<CycleMaximum>"
#define K5MW_CYCLEOVERFLOW		"<CycleOverflow>"

#define K5MW_STEPPINGPOSITION   "<SteppingPosition>"

#define K5MW_SYSFLAGSEX         "<SysFlagsEx>"
#define K5MW_SYSCAPS1           "<SysCaps1>"
#define K5MW_SYSCAPS2           "<SysCaps2>"

#define K5MW_SYSVERS            "<SYSVERS>"
#define K5MW_SYSFB              "<SYSFB>"
#define K5MW_SYSFBNX            "<SYSFBNX>"
#define K5MW_SYSNAME            "<SYSNAME>"
#define K5MW_SYSAUTHOR          "<SYSAUTHOR>"
#define K5MW_SYSOXC             "<SYSOXC>"
#define K5MW_LOCKLST            "<SYSLOCKLST>"
#define K5MW_DBSIZE             "<SYSDBSIZE>"
#define K5MW_HEAPF              "<SYSHEAPFREE>"
#define K5MW_ELAPSEDS           "<SYSELAPSEDS>"
#define K5MW_ELAPSEDMS          "<SYSELAPSEDMS>"
#define K5MW_NBSMLK             "<SYSNBSMLK>"
#define K5MW_SMLK               "<SYSSMLK%u>"
#define K5MW_LOCKLIST           "<LOCKLIST>"

// methods

DWORD K5MW_APICALL K5MW_GetKindOfObject (	// get the type of an object
	DWORD dwID								// ID of the specified object
	); // RETURN: type of object for this ID or 0 if invalid

/////////////////////////////////////////////////////////////////////////////
// error messages and prompt command lines

// methods

LPCSTR K5MW_APICALL K5MW_GetMessage (	// get text description of an error
	DWORD dwRC							// K5MW error report
	); // RETURN: description text for the specified error

void K5MW_APICALL K5MW_LoadMessages (	// load error messages from INI file
	LPCSTR szIniFileName,				// full pathname of INI file
	LPCSTR szSection					// section in INI file
	);

/////////////////////////////////////////////////////////////////////////////
// settings dialog box

BOOL K5MW_APICALL K5MW_EditSettings (   // edit settings through dialog
    HWND   hwndParent,                  // calling window handle
    LPCSTR szProjectPath,               // project folder (optional)
    LPSTR  szDriver,                    // driver DLL - must be [_MAX_PATH]
    LPSTR  szSettings                   // settings - must be [K5NETMAX_CONFIGSTRING]
    ); // TRUE=ok - FALSE=cancel

/////////////////////////////////////////////////////////////////////////////
// connection

#define K5MW_POLLEVENTS     0x0001      // want polling events
#define K5MW_QUIETMODE      0x1000      // no error report
#define K5MW_MULTITASK      0x0002      // multitasking context

// methods

DWORD K5MW_APICALL K5MW_Connect (		// connect to the MW database
	HWND hwndCallback,					// handle of the parent window for callbacks
	DWORD msgCallback,					// wished callback message
	DWORD dwFlags,						// reserved
	DWORD dwData,						// reserved for extension
	LPCSTR szClientName					// client name
	); // RETURN: client ID or NULL if error

DWORD K5MW_APICALL K5MW_SubscribeToLog ( // pass callback windows for log messages
	DWORD hClient,						// client ID
	HWND hwndLogListbox,				// listbox that receives log messages
	HWND hwndTraceListbox				// listbox that receives trace messages
	); // RETURN: ok or error

DWORD K5MW_APICALL K5MW_SetCallback (	// change client callback
	DWORD hClient,						// client ID
	HWND hwndCallback,					// handle of the parent window for callbacks
	DWORD msgCallback					// wished callback message
	); // RETURN: ok or error

void K5MW_APICALL K5MW_Disconnect (		// disconnect from the K5 database
	DWORD hClient						// client ID
	);

LPCSTR K5MW_APICALL K5MW_GetClientName ( // get the name of a registered client
	DWORD hClient						// client ID
	); // RETURN: name of the client

/////////////////////////////////////////////////////////////////////////////
// projects

// methods

DWORD K5MW_APICALL K5MW_FindProject (	// check if a project is open for debug
	LPCSTR szProjectPath 				// project pathname (no file name)
	);	// RETURN: non zero if open for OnLine debug or simulation

DWORD K5MW_APICALL K5MW_OpenProjectPass (	// open an existing project with password
    DWORD dwPassword,
	DWORD hClient,						// client ID
	LPCSTR szProjectPath,				// project pathname (no file name)
	LPCSTR szCommDriver,				// comm driver selection
	LPCSTR szCommSetup,					// comm configuration
	DWORD dwTargetID,					// Target identifier
	DWORD dwProtocol					// protocol selector
	);	// RETURN: handle of a project - NULL if not found

DWORD K5MW_APICALL K5MW_OpenProject (	// open an existing project
	DWORD hClient,						// client ID
	LPCSTR szProjectPath,				// project pathname (no file name)
	LPCSTR szCommDriver,				// comm driver selection
	LPCSTR szCommSetup,					// comm configuration
	DWORD dwTargetID,					// Target identifier
	DWORD dwProtocol					// protocol selector
	);	// RETURN: handle of a project - NULL if not found

void K5MW_APICALL K5MW_CloseProject (	// close the project and unlocks it
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	);

DWORD K5MW_APICALL K5MW_IsCommOK (		// get comm status
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	);	// RETURN TRUE if comm is OK (variables can be accessed)

DWORD K5MW_APICALL K5MW_IsSimulMode (   // is project simulated
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	);	// RETURN TRUE if project open in simul mode

DWORD K5MW_APICALL K5MW_HasSysInfo (    // SYS info objects available
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
    ); // RETURN TRUE if info is available

DWORD K5MW_APICALL K5MW_AcceptAppCtl (	// test if app control rq are accepted
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	);	// TRUE if app start/stop/change/load accepted by teh driver

DWORD K5MW_APICALL K5MW_Log (           // emulate a LOG message
	DWORD hClient,						// client ID
	DWORD hProject, 					// project ID
    LPCSTR szLog                        // message to log
	);

HWND K5MW_APICALL K5MW_GetTaskMan (	    // get task manager handle
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	);	// task manager window handle or NULL if no task manager available

DWORD K5MW_APICALL K5MW_HasBreakpoints ( // is there any breakpoint installed
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	);	// RETURN TRUE if at least one breakpoint installed

void K5MW_APICALL K5MW_SetHexMode (     // set HEXA display mode
	DWORD hClient,						// client ID
	DWORD hProject,					    // project ID
    DWORD dwHexMode                     // non zero for hexadecimal
	);	// RETURN TRUE if at least one breakpoint installed

DWORD K5MW_APICALL K5MW_GetHexMode (    // get HEXA display mode
	DWORD hClient,						// client ID
	DWORD hProject					    // project ID
	);	// RETURN: non zero for hexadecimal

LPCSTR K5MW_APICALL K5MW_Prompt (       // prompt (blocking call)
	DWORD hClient,                      // client ID
	DWORD hProject,                     // project ID
    LPCSTR szCommand,                   // command line
    LPCSTR szParent                     // parent program or NULL
	); // return: formatted answer

#define K5MWPROMPTEX_CHECKONLY          0x00000001
#define K5MWPROMPTEX_ABORT              0x00000002

LPCSTR K5MW_APICALL K5MW_PromptEx (     // prompt (blocking call)
	DWORD hClient,                      // client ID
	DWORD hProject,                     // project ID
    LPCSTR szCommand,                   // command line
    BOOL *pbOK,                         // TRUE if successful
    DWORD dwOptions                     // option flags (see upper)
	); // return: formatted answer

void K5MW_APICALL K5MW_EnableBkCheck (  // enable check of remaining bpts when exiting On Line
	DWORD hClient,						// client ID
	DWORD hProject,					    // project ID
    BOOL  bEnable                       // enable/disable check
	);	// RETURN TRUE if at least one breakpoint installed

BOOL K5MW_APICALL K5MW_Profiling (
	DWORD hClient,
	DWORD hProject,
    DWORD *pdwNb,
    LARGE_INTEGER **pli
    );

LPCSTR K5MW_APICALL K5MW_GetStatus (    // get project comm status info
	DWORD hClient,                      // client ID
	DWORD hProject,                     // project ID
    DWORD *pdwCnxStatus,                // OUT: connection status
    DWORD *pdwAppStatus                 // OUT: application status
    ); // return NULL if fail

/////////////////////////////////////////////////////////////////////////////
// launch commands

DWORD K5MW_APICALL K5MW_Execute (		// execute a command
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szCommand					// command
	);									// ok or error

// Available commands:
//
//	AppStart
//	AppStop
//	Pause
//	Resume
//	CycleStep
//  StepIn
//  StepOver
//  StepOut
//	Change
//	SetCycleTime:<value>
//  BkpSimple:<location>
//  BkpView:<location>
//  BkpOneShot:<location>
//  BkpRemove:<location>
//  BkpRemoveAll
//
//  DoCC
//  DoPC
//
//  SetSfcBreak:<step/trans>
//  ResetSfcBreak:<step/trans>
//  ResetSfcBreak:*
//
//	LoadCode:<pathname>
//	LoadSymbol:<pathname>
//	LoadChangeNow:<pathname>
//	LoadChangeLater:<pathname>
//	
//	Force:<variable>=<value>
//  W:<variable>=<value>{\t\r\n<variable>=<value>}
//  Toggle:<variable>
//	Lock:<variable>
//	Unlock:<variable>
//  UnlockAll
//  LockList
//
//  Hex:On:<variable>
//  Hex:Off:<variable>
//
//	PrgStart:<program>
//	PrgStop:<program>
//	PrgSuspend:<program>
//	PrgRestart:<program>
//
//	TimerStart:<timer>
//	TimerStop:<timer>
//
//  BSmp:0,flags,period,OnDelay,OffDelay,OnTrigger,OffTrigger,Var,...
//  BSmpStart:0
//  BSmpStop:0
//
//  Pass:item,password

DWORD K5MW_APICALL K5MW_CanForce (		// check a FORCE command
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szSymbol,                    // symbol
    LPCSTR szValue                      // forced value
	);									// ok or error

/////////////////////////////////////////////////////////////////////////////
// control a variable (manages the dialog box)

DWORD K5MW_APICALL K5MW_Control (		// control a variable (dialog)
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	HWND hwndParent,					// handle of the parent window
	DWORD x,							// where to open the dialog
	DWORD y,							// (parent window coordinates
	LPCSTR szSymbol,					// symbol of the variable
	LPCSTR szParentSymbol,				// parent symbol if local (or NULL)
	DWORD dwFlags						// reserved
	);									// ok or error

DWORD K5MW_APICALL K5MW_EditValue (     // edit the value of a variable
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	HWND hwndParent,					// handle of the parent window
	DWORD x,							// where to open the dialog
	DWORD y,							// (parent window coordinates
	LPCSTR szSymbol,					// symbol of the variable
	LPCSTR szParentSymbol,				// parent symbol if local (or NULL)
    LPSTR szValue,                      // OUT: value buffer - must be char[256];
	DWORD dwFlags						// reserved
	);									// ok or error

/////////////////////////////////////////////////////////////////////////////
// subscribe to variable loops

DWORD K5MW_APICALL K5MW_Subscribe (		// subscribe to a notification loop
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	DWORD hLoopID,						// loop ID (allocated by thecaller!)
	LPCSTR szSymbol,					// symbol of the variable
	LPCSTR szParentSymbol				// parent symbol if local (or NULL)
	);

DWORD K5MW_APICALL K5MW_Unsubscribe (	// unsubscribe to a notification loop
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	DWORD hLoopID						// loop ID (allocated by thecaller!)
	);

/////////////////////////////////////////////////////////////////////////////
// read objects

BOOL K5MW_APICALL K5MW_IsSymbolHexOn (	// check if symbol is hexa
	DWORD hClient,						// client ID
	DWORD hProject, 					// project ID
	LPCSTR szSymbol,					// symbol of the variable
	LPCSTR szParentSymbol				// parent symbol if local (or NULL)
	);

LPCSTR K5MW_APICALL K5MW_GetStringValue ( // get value of an object
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szSymbol,					// symbol of the variable
	LPCSTR szParentSymbol				// parent symbol if local (or NULL)
	); // value in a TEMP buffer or "" if value not available

LPCSTR K5MW_APICALL K5MW_GetDumpValue ( // get dump text of object value
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szSymbol,					// symbol of the variable
	LPCSTR szParentSymbol				// parent symbol if local (or NULL)
	); // value in a TEMP buffer or "" if value not available

DWORD K5MW_APICALL K5MW_GetBinValue (	// get value of an object
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szSymbol,					// symbol of the variable
	LPCSTR szParentSymbol,				// parent symbol if local (or NULL)
	BYTE *pbValue,						// buffer for value: size=K5NETMAX_VALUEBUFF
	DWORD *pdwValid,					// FALSE if value not available
	DWORD *pdwLocked					// TRUE if variable is locked
	); // OK or error

LPCSTR K5MW_APICALL K5MW_GetIDStringValue ( // get value of an object by ID
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	DWORD hLoopID						// loop ID (allocated by thecaller!)
	); // value in a TEMP buffer or "" if value not available

DWORD K5MW_APICALL K5MW_GetIDBinValue (	// get value of an object by ID
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	DWORD hLoopID,						// loop ID (allocated by thecaller!)
	BYTE *pbValue,						// buffer for value: size=K5NETMAX_VALUEBUFF
	DWORD *pdwValid,					// FALSE if value not available
	DWORD *pdwLocked					// TRUE if variable is locked
	); // OK or error

long K5MW_APICALL K5MW_GetLongValue (	// get value of an object
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szSymbol,					// symbol of the variable
	LPCSTR szParentSymbol,				// parent symbol if local (or NULL)
	DWORD *pdwValid,					// FALSE if value not available
	DWORD *pdwLocked					// TRUE if variable is locked
	); // value as LONG

long K5MW_APICALL K5MW_GetIDLongValue (	// get value of an object by ID
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	DWORD hLoopID,						// loop ID (allocated by thecaller!)
	DWORD *pdwValid,					// FALSE if value not available
	DWORD *pdwLocked					// TRUE if variable is locked
	); // value as LONG

double K5MW_APICALL K5MW_GetDoubleValue (	// get value of an object
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szSymbol,					// symbol of the variable
	LPCSTR szParentSymbol,				// parent symbol if local (or NULL)
	DWORD *pdwValid,					// FALSE if value not available
	DWORD *pdwLocked					// TRUE if variable is locked
	); // value as double

double K5MW_APICALL K5MW_GetIDDoubleValue (	// get value of an object by ID
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	DWORD hLoopID,						// loop ID (allocated by thecaller!)
	DWORD *pdwValid,					// FALSE if value not available
	DWORD *pdwLocked					// TRUE if variable is locked
	); // value as double

LPCSTR K5MW_APICALL K5MW_GetIDBoolAlias (	// get TRUE/FALSE alias of an object by ID
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	DWORD hLoopID,						// loop ID (allocated by thecaller!)
	DWORD dwValue 					    // wished value (0/1)
	); // alias or NULL if failed

/////////////////////////////////////////////////////////////////////////////
// Objects

// object styles                description                 dwData  pSettings

#define K5MWO_BSMPSETTINGS  1   // sampling trace settings  0       NULL
#define K5MWO_BSMPDATA      2   // sampling trace data      0       NULL
#define K5MWO_USER          3   // user request             length  data
#define K5MWO_FT            4   // file transfer            length  data

// services

DWORD K5MW_APICALL K5MW_CreateObject (  // create an object
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD dwStyle,                      // style of object
    DWORD dwData,                       // object dependent parameter
    void *pSettings                     // object dependent settings
    ); // return: handle of the object or NULL if failed

void K5MW_APICALL K5MW_DeleteObject (   // destroy an object
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD hObject                       // handle of the object
    );

void K5MW_APICALL K5MW_RefreshObject (  // ask for refresh of an object
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD hObject                       // handle of the object
    );

DWORD K5MW_APICALL K5MW_GetObjData (    // get object data
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD hObject,                      // handle of the object
    void *pData                         // application buffer 
    ); // return: OK or error

DWORD K5MW_APICALL K5MW_GetObjDataLength ( // get object data length
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD hObject                       // handle of the object
    ); // return: length or 0 if error

DWORD K5MW_APICALL K5MW_SetObjData (    // set object data
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD hObject,                      // handle of the object
    DWORD dwLength,                     // length of data (bytes)
    void *pData                         // application buffer 
    ); // return: OK or error

/////////////////////////////////////////////////////////////////////////////
// exploring the symbol table

DWORD K5MW_APICALL K5MW_FindSymbolByName (  // finds the VFI of a symbol
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    LPCSTR szSymbol
    ); // return: found VFI or NULL if not found

DWORD K5MW_APICALL K5MW_FindSymbolsByVFI (  // finds symbols of VFIs
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD dwNbSymbol,                   // number of searched symbols
    DWORD *pdwVFI,                      // VFIs of searched symbols (only type and index are used)
    LPCSTR *pszName,                    // OUT: found symbol names or NULL if not found
    DWORD *pdwIndex                     // OUT array elt index or -1 if not an array
    ); // return: OK or error if at least one not found

#define K5MWMAX_INDEXSTR    20          // max length of a formatted index

DWORD K5MW_APICALL K5MW_FormatSymbolIndex ( // format index of an array
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    LPCSTR szSymbol,                    // name of the array
    DWORD dwRawIndex,                   // raw index
    LPSTR szBuffer                      // output string buffer (size K5MWMAX_INDEXSTR)
    );

LPCSTR K5MW_APICALL K5MW_IsConstAlias ( // check if a symbol is the alias of a constant value
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    LPCSTR szSymbol                     // name of the symbol
    ); // NULL or pointer to the constant value (STRING)

/////////////////////////////////////////////////////////////////////////////
// upload

// status

#define K5MWUPSTA_READY         0       // inactive - no job defined
#define K5MWUPSTA_RUNNING       1       // upload in progress
#define K5MWUPSTA_OK            2       // upload complete - data is ready
#define K5MWUPSTA_ERROR         3       // upload stopped on error

// methods

DWORD K5MW_APICALL K5MWUP_Connect (	    // connect for upload
	HWND hwndCallback,					// handle of the parent window for callbacks
	DWORD msgCallback,					// wished callback message
	LPCSTR szCommDriver,				// comm driver selection
	LPCSTR szCommSetup,					// comm configuration
	DWORD dwTargetID,					// Target identifier
	DWORD dwProtocol					// protocol selector
	);	// RETURN: handle of upload connection - NULL if not found

void K5MW_APICALL K5MWUP_Disconnect (   // close the upload connection
	DWORD hUpload                       // ID of the upload connection
	);

DWORD K5MW_APICALL K5MWUP_LoadSymbols (	// start uploading symbols
	DWORD hUpload                       // ID of the upload connection
	);  // RETURN: ok or error

DWORD K5MW_APICALL K5MWUP_GetStatus (	// get status of uploading process
	DWORD hUpload                       // ID of the upload connection
	);  // RETURN: upload status

LPCSTR K5MW_APICALL K5MWUP_GetAppName ( // get app name
	DWORD hUpload,                      // ID of the upload connection
    DWORD *pdwVersion,                  // app version number
    DWORD *pdwDateStamp,                // app date stamp
    DWORD *pdwCodeCrc,                  // app code CRC
    DWORD *pdwDataCrc                   // app symbols CRC
	);  // RETURN: connected app name or empty string if not connected

DWORD K5MW_APICALL K5MWUP_SaveSymbols (	// save symbol table to file
	DWORD hUpload,                      // ID of the upload connection
    LPCSTR szFileName                   // full pathname of the output file
	);  // RETURN: ok or error

void K5MW_APICALL K5MWUP_StartEnumSymbols ( // prepare enumeration of symbols
	DWORD hUpload                       // ID of the upload connection
	);

LPCSTR K5MW_APICALL K5MWUP_EnumSymbols ( // get next symbol
	DWORD hUpload,                      // ID of the upload connection
    DWORD *pdwVFI,                      // out: VFI
    DWORD *pdwDim,                      // out: raw dimension
    DWORD *pdwStringLength,             // out: string length
    void  *pData                        // reserved: must be NULL
    ); // RETURN: symbol name in temp buffer or NULL if finished

DWORD K5MW_APICALL K5MWUP_Dialog1 (
    HWND hwndParent,
    LPCSTR szDstFile,
    LPCSTR szDriver,
    LPCSTR szConf
    );

/////////////////////////////////////////////////////////////////////////////
// Passwords

DWORD K5MW_APICALL K5MW_SetRights (     // set access rights for a data
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    LPCSTR szItemName,                  // item name or NULL for all
    DWORD dwProtect,                    // TRUE for protection
    LPCSTR szPassword                   // optionnal password or NULL
    );

DWORD K5MW_APICALL K5MW_AskPassword (   // ask for password
    HWND    hwndParent,                 // parent window handle
    LPCSTR szItemName,                  // item name (title of the box)
    LPCSTR szExpectedPassword           // expected password
    );  // TRUE if ok

/////////////////////////////////////////////////////////////////////////////
// Call stack and stepping

DWORD K5MW_APICALL K5MW_IsDebugInfoAvailable ( // check if DEBUG info here
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
); // RETURN: TRUE if debug info available

DWORD K5MW_APICALL K5MW_SubscribeToCallStack ( // subscribe to callstack items
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	DWORD hFirstLoopID					// first loop ID (allocated by thecaller!)
); // RETURN: 0 is fail or number of consecutive IDs allocated
   // WARNING: you are responsible for calls to Unsubscribe()

DWORD K5MW_APICALL K5MW_SubscribeToBreakpoints ( // subscribe to breakpoint items
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	DWORD hFirstLoopID					// first loop ID (allocated by thecaller!)
); // RETURN: 0 is fail or number of consecutive IDs allocated
   // WARNING: you are responsible for calls to Unsubscribe()

LPCSTR K5MW_APICALL K5MW_GetCallStackString ( // get callstack in string format
	DWORD hClient,						// client ID
	DWORD hProject,					    // project ID
	DWORD hFirstLoopID,					// first loop ID allocated for callstack items
    DWORD dwNbLoop                      // number of declared loops
); // RETURN: full call stack (singleline) or NULL if not stepping

DWORD K5MW_APICALL K5MW_GetCallStackLB ( // get callstack in a listbox
	DWORD hClient,						// client ID
	DWORD hProject,					    // project ID
	DWORD hFirstLoopID, 				// first loop ID allocated for callstack items
    HWND  hwndListBox,                  // handle of listbox to fill with callstack
    DWORD dwNbLoop                      // number of declared loops
); // RETURN: full call stack (singleline) or NULL if not stepping

LPCSTR K5MW_APICALL K5MW_GetSteppingLocation ( // get step position (compiler format)
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
); // RETURN: location in compiler format or NULL if not stepping

LPCSTR K5MW_APICALL K5MW_GetSteppedProgram ( // get name of top program in callstack
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
); // RETURN: name of the top program in callstack or NULL if not stepping
   // this program may be the parent of the stepped instance if local

LPCSTR K5MW_APICALL K5MW_GetSteppedInstance ( // get name of stepped FB instance
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
); // RETURN: name of the stepped instance or NULL if none

DWORD K5MW_APICALL K5MW_SetBreakpoint ( // set or remove a breakpoint
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD dwStyle,                      // breakpoint style defined in K5NETAPI.H
    LPCSTR szProgram,                   // program name
    LPCSTR szSfcLocation,               // step or transition name or NULL
    LPCSTR szSfcActionBlock,            // step action block or NULL
    DWORD  *pdwX,                       // X position (can be updated on output)
                                        // X must be 0 if Y is a line
    DWORD  *pdwYorLine                  // Y position (can be updated on output)
); // return: OK or error

BOOL K5MW_APICALL K5MW_CheckBreakpoint ( // check/fix a breakpoint position
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    BOOL  bStrict,                      // if TRUE, return FALSE if position fixed
    LPCSTR szProgram,                   // program name
    LPCSTR szSfcLocation,               // step or transition name or NULL
    LPCSTR szSfcActionBlock,            // step action block or NULL
    DWORD  *pdwX,                       // X position (can be updated on output)
                                        // X must be 0 if Y is a line
    DWORD  *pdwYorLine                  // Y position (can be updated on output)
); // return: OK or error

// available breakpoint styles
// #define K5NETBKP_DELETED 0   Use this style to remove the breakpoint
// #define K5NETBKP_SIMPLE  1   Simple breakpoint
// #define K5NETBKP_ONESHOT 2   Brekpoint automatically remove when reached

/////////////////////////////////////////////////////////////////////////////
// misc

void K5MW_APICALL K5MWUP_CheckDialogBox (
    HWND hwndParent,
    LPCSTR szDriver,
    LPCSTR szConf
);

/////////////////////////////////////////////////////////////////////////////
// File Transfer - only one connection at a time

typedef void (*PFK5MWFT_ProgressCallback)(DWORD dwPercent, DWORD_PTR dwData);
typedef void (*PFK5MWFT_Loop)(DWORD_PTR dwData);

DWORD K5MW_APICALL K5MW_FTOpen (    // open a file transfer session
    LPCSTR szSettings,              // connection settings
    DWORD dwTimeout,                // timeout for a complete transfer
    LPCSTR szProject,               // project alerady open in K5MW or "?"
    LPCSTR szDriver                 // driver DLL name (eg: "K5NET5.DLL")
    ); // OK or error

DWORD K5MW_APICALL K5MW_FTClose (   // close the file transfer session
    void
    ); // OK or error

BOOL K5MW_APICALL K5MW_FTIsBusy (   // test if a session is open
    void
    ); // RUE if file transfer session already open

void K5MW_APICALL K5MW_FTAbort (    // abort the current transfer
    void
    );

void K5MW_APICALL K5MW_FTSetProgressCallback (  // set application callback (percentage of transfer)
    PFK5MWFT_ProgressCallback pfCallback,       // application callback function
    DWORD_PTR dwData                            // application argument for callback
    );

void K5MW_APICALL K5MW_FTSetLoopCallback (      // set callback (called on every request)
    PFK5MWFT_Loop pfLoop,                       // application callback function
    DWORD_PTR dwData                            // application argument for callback
    );

// follosing calls are blocking

DWORD K5MW_APICALL K5MW_FTSend (                // send a file
    LPCSTR szLocal,                             // local pathname
    LPCSTR szRemote                             // remote pathname (on the runtime)
    ); // OK or error

DWORD K5MW_APICALL K5MW_FTReceive (             // receive a file
    LPCSTR szLocal,                             // local pathname
    LPCSTR szRemote                             // remote pathname (on the runtime)
    ); // OK or error

DWORD K5MW_APICALL K5MW_FTDirectory (
    void
    );

DWORD K5MW_APICALL K5MW_FTRemove (
    LPCSTR szRemote
    );

DWORD K5MW_APICALL K5MW_FTBeginEnumDir (
    void
    );

void K5MW_APICALL K5MW_FTEndEnumDir (
    void
    );

LPCSTR K5MW_APICALL K5MW_FTEnumDir (
    void
    );

// ready to use box including progress bar

DWORD K5MW_APICALL K5MW_FTDialog1 ( // send or receive a file
    HWND   hwndParent,              // parent window handle
    DWORD  dwSend,                  // 1=send - 0=receive
    LPCSTR szLocal,                 // local pathname
    LPCSTR szRemote,                // remote pathname (on the runtime)
    LPCSTR szTitle,                 // box title or NULL for default
    LPCSTR szSettings,              // communication settings
    LPCSTR szProject,               // project alerady open in K5MW or "?"
    LPCSTR szDriver                 // driver DLL name (eg: "K5NET5.DLL")
    ); // OK or error

// reserved to COPALP

DWORD K5MW_APICALL K5MW_FTDialog2 (
    HWND   hwndParent,
    POINT  *ptPos,
    DWORD  dwSend,
    LPCSTR szLocal,
    LPCSTR szRemote,
    LPCSTR szTitle,
    LPCSTR szSettings,
    LPCSTR szProject,
    LPCSTR szDriver
    );

/////////////////////////////////////////////////////////////////////////////
// project source embedding - zip

#define K5MW_PRJF_SOURCE   0x0001   // source level files
#define K5MW_PRJF_SYBTABLE 0x0002   // symbol table (for monitoring)
#define K5MW_PRJF_DEBUG    0x0004   // debug information (for stepping)
#define K5MW_PRJF_WATCH    0x0008   // all watch/act files
#define K5MW_PRJF_SETTINGS 0x0010   // wizards settings
#define K5MW_PRJF_HISTORY  0x0020   // project history
#define K5MW_PRJF_COMMENT  0x0040   // comments (folders)
#define K5MW_PRJF_BITMAP   0x0080   // bitmaps and icons
#define K5MW_PRJF_EXTRA    0x8000   // other specified by the user

DWORD K5MW_APICALL K5MW_ZipProjectSource (
    LPCSTR szProject,               // project path
    LPCSTR szName,                  // project name
    DWORD dwFlags,                  // items to zip
    LPCSTR szExtra,                 // list of extra files
    LPCSTR szZipFile                // the destination zip file
    ); // OK or error

/////////////////////////////////////////////////////////////////////////////
// events

DWORD K5MW_APICALL K5MW_CanProcessEvents (
	DWORD hClient,
	DWORD hProject
	);

DWORD K5MW_APICALL K5MW_RegisterEvent (
    DWORD hClient,
    DWORD hProject,
    LPCSTR szName,
    LPCSTR szParent,
    double dHP,
    double dHN
    );

DWORD K5MW_APICALL K5MW_UnregisterEvent (
    DWORD hClient,
    DWORD hProject,
    LPCSTR szName,
    LPCSTR szParent
    );

LPCSTR K5MW_APICALL K5MW_PopEvent (
    DWORD hClient,
    DWORD hProject,
    DWORD dwVFI,
    DWORD *pdwDateStamp,
    DWORD *pdwTimeStamp,
    double *pdValue,
    long *plValue,
    BYTE *pRawValue,
    LPSTR szFormattedValue
    );

LPCSTR K5MW_APICALL K5MW_PopEventVSI (
    DWORD hClient,
    DWORD hProject,
    DWORD dwVFI,
    DWORD *pdwDateStamp,
    DWORD *pdwTimeStamp,
    double *pdValue,
    long *plValue,
    BYTE *pRawValue,
    LPSTR szFormattedValue,
    DWORD *pdwStatHi,
    DWORD *pdwStatLo
    );

/////////////////////////////////////////////////////////////////////////////
// CT Segment symbols enumeration

typedef void  (*PFK5MW_CBEnumCTSeg)(DWORD dwType, LPCSTR szName,
                                    DWORD dwOffset, void *pData);

LPCSTR K5MW_APICALL K5MW_EnumCTSeg ( // enumerate CT Seg items
    DWORD hClient,
    DWORD hProject,
    PFK5MW_CBEnumCTSeg pfCB,
    void *pData
    );

LPCSTR K5MW_APICALL K5MW_EnumHexOn ( // enumerate symbols with "Hex=ON" - dwOffset is unused
    DWORD hClient,
    DWORD hProject,
    PFK5MW_CBEnumCTSeg pfCB,
    void *pData
    );

/////////////////////////////////////////////////////////////////////////////
// smart load

#define K5MWPRJLIST_WLSPATH     0x00000001L     // option: dwData is the W5L pathname (LPCSTR)

DWORD K5MW_APICALL K5MW_SendPrjList (   // multiple download
    HWND hwndParent,                    // parent window handle
    LPCSTR szProjectList,               // list of project paths (sep is \n)
    LPCSTR szDriverDef,                 // default driver
    LPCSTR szSettingsDef,               // default comm settings
    DWORD dwOptions,                    // options - future
    DWORD_PTR dwData                    // Options - future
    ); // K5MW_OK if successful

DWORD K5MW_APICALL K5MW_SendScript (    // run download script
    HWND hwndParent,                    // parent window handle
    LPCSTR szScript,                    // XML script file
    DWORD dwOptions,                    // options - future
    DWORD_PTR dwData                    // Options - future
    ); // K5MW_OK if successful

/////////////////////////////////////////////////////////////////////////////
// node discovery

LPCSTR K5MW_APICALL K5MW_BrowseNetwork ( // browse network for runtimes
    HWND hwndParent,                    // parent window handle
    LPSTR szAddress,                    // selected address - expected [256]
    LPSTR szDriver                      // selected driver DLL - expected [256]
    ); // list of addresses separated by ","

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // !defined(_K5MWAPI_H__INCLUDED_)
