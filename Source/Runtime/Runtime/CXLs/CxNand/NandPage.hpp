
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandPage_HPP

#define	INCLUDE_NandPage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "NandBlock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Page Selector
//

class DLLAPI CNandPage : public CNandBlock
{
	public:
		// Constructors
		CNandPage(void);
		CNandPage(CNandBlock const &Block);
		CNandPage(CNandBlock const &Block, UINT uPage);
		CNandPage(UINT uChip, UINT uBlock, UINT uPage);

		// Public Data
		UINT m_uPage;

		// Operators
		bool operator == (CNandPage const &That) const;
		bool operator != (CNandPage const &That) const;
		bool operator  > (CNandPage const &That) const;
		bool operator  < (CNandPage const &That) const;

		// Attributes
		bool IsValid(void) const;

		// Operations
		void Invalidate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructor

STRONG_INLINE CNandPage::CNandPage(void)
{
	m_uPage = 0;
	}

STRONG_INLINE CNandPage::CNandPage(CNandBlock const &Block) : CNandBlock(Block)
{
	m_uPage = 0;
	}

STRONG_INLINE CNandPage::CNandPage(CNandBlock const &Block, UINT uPage) : CNandBlock(Block)
{
	m_uPage = uPage;
	}

STRONG_INLINE CNandPage::CNandPage(UINT uChip, UINT uBlock, UINT uPage) : CNandBlock(uChip, uBlock)
{
	m_uPage = uPage;
	}

// Operators

STRONG_INLINE bool CNandPage::operator == (CNandPage const &That) const
{
	if( CNandBlock::operator == (That) ) {

		if( m_uPage == That.m_uPage ) {

			return true;
			}
		}

	return false;
	}

STRONG_INLINE bool CNandPage::operator != (CNandPage const &That) const
{	
	if( CNandBlock::operator == (That) ) {

		if( m_uPage == That.m_uPage ) {

			return false;
			}
		}

	return true;
	}

STRONG_INLINE bool CNandPage::operator > (CNandPage const &That) const
{
	if( CNandBlock::operator > (That) ) {

		return true;
		}

	if( CNandBlock::operator < (That) ) {

		return false;
		}

	return m_uPage > That.m_uPage;
	}

STRONG_INLINE bool CNandPage::operator < (CNandPage const &That) const
{	
	if( CNandBlock::operator < (That) ) {

		return true;
		}

	if( CNandBlock::operator > (That) ) {

		return false;
		}

	return m_uPage < That.m_uPage;
	}

// Attributes

STRONG_INLINE bool CNandPage::IsValid(void) const
{
	if( m_uPage == NOTHING ) {

		return false;
		}

	return CNandBlock::IsValid();
	}

// Operations

STRONG_INLINE void CNandPage::Invalidate(void)
{
	m_uPage = NOTHING;

	CNandBlock::Invalidate();
	}

// End of File

#endif
