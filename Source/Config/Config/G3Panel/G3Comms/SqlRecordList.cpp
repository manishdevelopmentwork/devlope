
#include "Intern.hpp"

#include "SqlRecordList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SqlRecord.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Record List
//

AfxImplementDynamicClass(CSqlRecordList, CItemList);

// Constructor

CSqlRecordList::CSqlRecordList(void)
{
	m_Class = AfxRuntimeClass(CSqlRecord);
	}

// Item Access

CSqlRecord * CSqlRecordList::GetItem(INDEX Index) const
{
	return (CSqlRecord *) CItemList::GetItem(Index);
	}

CSqlRecord * CSqlRecordList::GetItem(UINT uPos) const
{
	return (CSqlRecord *) CItemList::GetItem(uPos);
	}

CSqlRecord * CSqlRecordList::GetRecord(UINT uRow)
{
	UINT n = 0;

	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CSqlRecord * pRecord = GetItem(i);

		if( n == uRow ) {

			return pRecord;
			}

		n++;
		}

	return NULL;
	}

UINT CSqlRecordList::GetRecordPos(CSqlRecord *pFind)
{
	UINT n = 0;

	for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

		CSqlRecord * pRecord = GetItem(i);

		if( pFind == pRecord ) {

			return n;
			}

		n++;
		}

	return NOTHING;
	}

// Download Support

BOOL CSqlRecordList::MakeInitData(CInitData &Init)
{
	CItemList::MakeInitData(Init);

	return TRUE;
	}

// End of File
