#!/bin/sh

# c3-drop-dns <interface>
#
# Remove the servers tagged as provided by this interface from the DNS
# Masquerade configuration file. If the file is empty, add two default
# entries so that we at least have something to work with.

conf="/tmp/crimson/dns/dhcp.conf"

temp="/tmp/crimson/dns/dhcp.temp"

face=$1

(
	flock -x 9
	
	cat $conf | grep -v "int_$face" | grep -v "int_default" > $temp

	if [ `cat $temp | wc -l` -eq 0 ]
	then
		echo "nameserver 8.8.8.8 # int_default" >> $temp
		echo "nameserver 8.8.4.4 # int_default" >> $temp
	fi
		
	cp $temp $conf
	
	killall -q -SIGHUP dnsmasq

) 9> /var/lock/c3-dns
