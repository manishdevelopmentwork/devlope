
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_StringTable_HPP

#define	INCLUDE_StringTable_HPP

//////////////////////////////////////////////////////////////////////////
//
// String API
//

global void  SetWebLanguage(PCTXT pHeader);

global PCTXT GetWebString(UINT id);

//////////////////////////////////////////////////////////////////////////
//
// Language Codes
//

#define	C3L_SYSTEM		0x0000
#define	C3L_GENERIC		0x0001
#define	C3L_ENGLISH		0x0100
#define	C3L_ENGLISH_US		0x0101
#define	C3L_ENGLISH_UK		0x0102
#define	C3L_ENGLISH_IN		0x0103
#define	C3L_FRENCH		0x0200
#define	C3L_FRENCH_FR		0x0201
#define	C3L_FRENCH_CA		0x0202
#define	C3L_FRENCH_BE		0x0203
#define	C3L_FRENCH_CH		0x0204
#define	C3L_SPANISH		0x0300
#define	C3L_SPANISH_ES		0x0301
#define	C3L_SPANISH_MX		0x0302
#define	C3L_GERMAN		0x0400
#define	C3L_GERMAN_DE		0x0401
#define	C3L_GERMAN_AT		0x0402
#define	C3L_GERMAN_CH		0x0403
#define	C3L_ITALIAN		0x0500
#define	C3L_ITALIAN_IT		0x0501
#define	C3L_ITALIAN_CH		0x0502
#define	C3L_FINNISH		0x0600
#define	C3L_SIMP_CHINESE	0x8101
#define	C3L_TRAD_CHINESE	0x8201
#define	C3L_RUSSIAN		0x8301
#define	C3L_KOREAN_JAMO		0x8401
#define	C3L_KOREAN_FULL		0x8402
#define	C3L_JAPANESE		0x8500

//////////////////////////////////////////////////////////////////////////
//
// String Identifers
//

#define IDS_NEW_VALUE			1000
#define IDS_OLD_VALUE			1001
#define IDS_OK				1002
#define IDS_CANCEL			1003
#define IDS_PLS_CLOSE_ALL		1004
#define IDS_PLS_LOG_ON			1005
#define IDS_USER_NAME			1006
#define IDS_PASSWORD			1007
#define IDS_HOME			1008
#define IDS_DATA			1009
#define IDS_LOGS			1010
#define IDS_REMOTE			1011
#define IDS_USER			1012
#define IDS_LOG_OFF			1013
#define IDS_VIEW_DATA			1014
#define IDS_VIEW_DATA_DESC		1015
#define IDS_VIEW_LOGS			1016
#define IDS_VIEW_CONT_DESC		1017
#define IDS_VIEW_BATCHES		1018
#define IDS_VIEW_BATCHES_DESC		1019
#define IDS_VIEW_LOGS_DESC		1020
#define IDS_REMOTE_VIEW			1021
#define IDS_REMOTE_CTRL_DESC		1022
#define IDS_REMOTE_VIEW_DESC		1023
#define IDS_USER_SITE			1024
#define IDS_USER_SITE_DESC		1025
#define IDS_NO_PERMISSIONS		1026
#define IDS_NAME			1027
#define IDS_VALUE			1028
#define IDS_EDITING			1029
#define IDS_EDIT_DOTS			1030
#define IDS_EVENT_LOG			1031
#define IDS_SECURITY_LOG		1032
#define IDS_LIST_BATCHES		1033
#define IDS_LIST_LOGS_IN		1034
#define IDS_LIST_FILES_IN_IN		1035
#define IDS_LIST_FILES			1036
#define IDS_LIST_FILES_IN		1037
#define IDS_NO_DATA			1038
#define IDS_FAILED_WRITE_X		1039
#define IDS_FAILED_WRITE_TAG		1040
#define IDS_X_SET_TO_Y			1041
#define	IDS_BATCHES			1042
#define	IDS_DESCRIPTION			1043
#define	IDS_LIST_LOGS			1044
#define	IDS_EVENTS			1045
#define	IDS_SECURITY			1046
#define	IDS_PAGE			1047
#define	IDS_AVAILABLE_PAGES		1048
#define	IDS_LOG_OFF_CURRENT		1049
#define	IDS_LOG_ON			1050
#define	IDS_ACCESS_DENIED		1051
#define	IDS_BACK			1052
#define	IDS_ENH_WEB_SERVER		1053	
#define	IDS_WELCOME			1054
#define	IDS_OPTION			1055
#define	IDS_SIZE			1056
#define IDS_WRITE_OKAY_HEAD		1057
#define IDS_WRITE_OKAY_BODY		1058
#define IDS_WRITE_FAIL_HEAD		1059
#define IDS_WRITE_FAIL_BODY		1060
#define IDS_CLICK_TO_RETURN		1061
#define IDS_SYSTEM			1062
#define IDS_SYSTEM_TITLE		1063
#define IDS_SYSTEM_PAGES		1064
#define IDS_SYSTEM_DESC			1065
#define IDS_PCAP_NAME			1066
#define IDS_PCAP_DESC			1067
#define IDS_PCAP_START			1068
#define IDS_PCAP_STOP			1069
#define IDS_PCAP_BYTES			1070
#define IDS_PCAP_SRC			1071
#define IDS_PCAP_NOT			1072
#define IDS_PCAP_TCP			1073
#define IDS_PCAP_TCP_PORT		1074
#define IDS_PCAP_UDP			1075
#define IDS_PCAP_UDP_PORT		1076
#define IDS_PCAP_ARP			1077
#define IDS_PCAP_DOWN			1078
#define IDS_CMD_UPDATE			1079
#define IDS_DBG_CONSOLE			1080
#define IDS_DBG_FREEZE			1081
#define IDS_PCAP_SSL			1082
#define IDS_PCAP_DISABLED		1083
#define IDS_PCAP_ENABLED		1084
#define IDS_PCAP_SPECIFIC		1085
#define IDS_PCAP_ALL			1086
#define IDS_DBG_INTERACT		1087
#define IDS_NET_CFG			1088
#define IDS_NET_STS			1089
#define IDS_NET_RTS			1090
#define IDS_NET_STS1			1091
#define IDS_NET_STS2			1092
#define IDS_NET_DCFG			1093
#define IDS_NET_DSTS			1094
#define IDS_NET_DRTS			1095
#define IDS_NET_DSTS1			1096
#define IDS_NET_DSTS2			1097
#define IDS_PCAP_NON_WEB		1098
#define IDS_SYSTEM_LOG			1099
#define IDS_MODEM_NOT_INIT		1100
#define IDS_MODEM_SHOW_ACT		1101
#define IDS_NET_ARP			1102
#define IDS_NET_DARP			1103
#define IDS_NET_FIRE1			1104
#define IDS_NET_DFIRE1			1105
#define IDS_DSK_SPACE			1106
#define IDS_DSK_DSPACE			1107
#define IDS_MEM_SPACE			1108
#define IDS_MEM_DSPACE			1109
#define IDS_CPU_USAGE			1110
#define IDS_CPU_DUSAGE			1111
#define IDS_NET_DHCPD			1112
#define IDS_NET_DDHCPD			1113
#define IDS_NET_INFO			1114
#define	IDS_SYS_INFO			1115
#define IDS_NET_MACFILT			1116
#define IDS_NET_DMACFILT		1117
#define IDS_NET_FIRE2			1118
#define IDS_NET_DFIRE2			1119
#define IDS_NET_FIRE3			1120
#define IDS_NET_DFIRE3			1121

// End of File

#endif
