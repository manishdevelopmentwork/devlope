
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_FileManager_HPP

#define	INCLUDE_FileManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// File Manager
//

class CFileManager : public IFileManager
{
	public:
		// Constructors
		CFileManager(void);

		// Destructor
		~CFileManager(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IFileManager
		IFilingSystem * METHOD FindFileSystem(void);
		IFilingSystem * METHOD FindFileSystem(CHAR cDrive);
		IFilingSystem * METHOD FindFileSystem(UINT iVolume);
		IFilingSystem * METHOD LockFileSystem(void);
		IFilingSystem * METHOD LockFileSystem(CHAR cDrive);
		IFilingSystem * METHOD LockFileSystem(UINT iVolume);
		BOOL            METHOD LockFileSystem(IFilingSystem *pFileSys);
		void		METHOD FreeFileSystem(IFilingSystem *pFileSys, BOOL fLocked);
		BOOL		METHOD RegisterTask(IFilingSystem *pFileSys);
		BOOL		METHOD UnregisterTask(IFilingSystem *pFileSys);
		void		METHOD Purge(UINT uTask);

	protected:
		// Object Reference
		struct CRef
		{
			IFilingSystem * m_pRef;
			UINT            m_uRef;
			UINT	        m_uTask;
			CRef          * m_pNext;
			CRef          * m_pPrev;
			};

		// Data
		ULONG	          m_uRefs;
		IVolumeManager	* m_pVolMan;
		IDiskManager	* m_pDiskMan;
		IMutex		* m_pMutex;
		CRef		* m_pHead;
		CRef		* m_pTail;

		// Implementation
		CRef * Find(IFilingSystem *pFileSys);
		void   Register(IFilingSystem *pFileSys);
		void   Unregister(IFilingSystem *pFileSys);
	};

// End of File

#endif
