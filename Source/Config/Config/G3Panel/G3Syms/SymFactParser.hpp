
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SymFactParser_HPP

#define INCLUDE_SymFactParser_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SymFactParser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Parser
//

class CSymFactParser
{
	protected:
		// Overridables
		virtual void OnParseTag(char const *pName, char const *pData) = 0;

		// Operations
		void ParseLine(char *pLine);
	};

// End of File

#endif
