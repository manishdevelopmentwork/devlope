
#include "Intern.hpp"

#include "DispFormatNumber.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "CommsSystem.hpp"
#include "DispFormatSci.hpp"
#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Number Display Format
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatNumber, CDispFormat);

// Constructor

CDispFormatNumber::CDispFormatNumber(void)
{
	m_uType   = 1;

	m_fLimits = TRUE;

	m_Radix	  = 0;

	m_Before  = 5;

	m_After	  = 0;

	m_Leading = 1;

	m_Group	  = 0;

	m_Signed  = 1;

	m_pPrefix = NULL;

	m_pUnits  = NULL;

	m_pDynDP  = NULL;
	}

// UI Update

void CDispFormatNumber::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	if( Tag == "Radix" ) {

		if( m_Radix ) {

			m_Signed  = 0;
			m_After   = 0;
			m_Leading = 0;

			pHost->UpdateUI(this, "Signed");
			pHost->UpdateUI(this, "After");
			pHost->UpdateUI(this, "DynDP");
			pHost->UpdateUI(this, "Leading");
			}

		DoEnables(pHost);
		}

	if( Tag == "Radix" ) {

		UINT uLimit = GetLimit();

		MakeMin(m_After,  uLimit - m_Before);

		MakeMin(m_Before, uLimit - m_After);

		pHost->UpdateUI(this, "Before");

		pHost->UpdateUI(this, "After");

		pHost->UpdateUI(this, "DynDP");
		}

	if( Tag == "Before" ) {

		UINT uLimit = GetLimit();

		MakeMin(m_Before, uLimit - m_After);

		pHost->UpdateUI(this, "Before");
		}

	if( Tag == "After" ) {

		UINT uLimit = GetLimit();

		MakeMin(m_After, uLimit - m_Before);

		pHost->UpdateUI(this, "After");
		}

	CDispFormat::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CDispFormatNumber::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Prefix" || Tag == "Units" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "DynDP" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Formatting

CString CDispFormatNumber::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		CString Text;

		if( m_pPrefix ) {

			Text += m_pPrefix->GetText();
			}

		if( m_pUnits ) {

			Text += m_pUnits->GetText();
			}

		return Text;
		}

	if( Type == typeReal || Type == typeInteger ) {

		INT64   nData = 0;

		CString Text  = L"";

		if( Type == typeReal ) {

			double r = I2R(Data);

			if( _isnan(r) ) {

				return L"NAN";
				}

			if( !_finite(r) ) {

				if( r > 0 ) {

					return L"+INF";
					}

				return L"-INF";
				}

			r *= double(Power(GetAfter()));

			if( r < 0 ) r -= 0.5;

			if( r > 0 ) r += 0.5;

			nData = INT64(r);
			}

		if( Type == typeInteger ) {

			nData = C3INT(Data);
			}

		UINT  uTotal = m_Before + GetAfter();

		UINT  uRadix = GetRadix();

		INT64 Factor = Power(uTotal - 1);

		BOOL  fHide  = (m_Leading >= 1) ? TRUE  : FALSE;

		BOOL  fPad   = (m_Leading == 2) ? FALSE : !!(Flags & fmtPad);

		TCHAR cGroup = GetGroupChar();

		TCHAR cPoint = GetPointChar();

		// LATER -- Can we use some special characters for the plus
		// and minus to ensure that they are the same size as a digit
		// when using a variable pitch font?

		if( !m_Signed ) {

			nData &= 0x7FFFFFFF;
			}
		else {
			if( nData < 0 ) {

				nData = -nData;

				Text += '-';
				}
			else {
				if( m_Signed == 1 ) {

					if( fPad ) {

						Text += spaceFigure;
						}
					}
				else
					Text += '+';
				}
			}

		if( m_Radix == 1 ) {

			if( afxVista ) {

				Text += uniLRM;
				}
			}

		for( UINT n = 0; n < uTotal; n++ ) {

			PCTXT m_pHex = L"0123456789ABCDEF";

			TCHAR  cDigit = '*';

			if( m_Radix < 4 ) {

				cDigit = m_pHex[nData / Factor % uRadix];

				if( n == m_Before - 1 ) {

					fHide = FALSE;
					}

				if( cDigit == '0' ) {

					if( fHide ) {

						if( fPad ) {

							cDigit = spaceFigure;
							}
						else
							cDigit = 0;
						}
					}
				else
					fHide = FALSE;
				}

			if( cDigit ) {

				Text += cDigit;
				}

			if( n == m_Before - 1 && GetAfter() ) {

				Text += cPoint;
				}

			if( m_Group ) {

				if( n < m_Before - 1 ) {

					if( IsGroupBoundary(m_Before - 1 - n) ) {

						if( fHide ) {

							if( fPad ) {

								Text += spaceFigure;
								}
							}
						else
							Text += cGroup;
						}
					}
				}

			Factor /= uRadix;
			}

		if( m_Radix == 1 ) {

			if( afxVista ) {

				Text += uniPDF;
				}
			}

		if( Flags & fmtPad ) {

			if( m_Radix == 1 ) {

				MakeLettersFixed(Text);
				}

			MakeDigitsFixed(Text);
			}

		if( !(Flags & fmtBare) ) {

			if( m_pPrefix ) {

				Text = m_pPrefix->GetText() + Text;
				}

			if( m_pUnits ) {

				Text = Text + m_pUnits->GetText();
				}
			}

		return Text;
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Limit Access

DWORD CDispFormatNumber::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		C3INT nData = 0;

		if( m_Signed ) {

			UINT    uRadix = GetRadix();

			if( uRadix ) {

				C3INT nWork = uRadix - 1;

				nWork *= -1;

				UINT uTotal = m_Before + GetAfter();

				for( UINT n = 0; n < uTotal; n ++ ) {

					nData += nWork;

					nWork *= uRadix;
					}
				}
			}

		return nData;
		}

	if( Type == typeReal ) {

		C3REAL rData = 0;

		if( m_Signed ) {

			UINT    uRadix = GetRadix();

			if( uRadix ) {

				C3REAL rWork = 0;

				UINT uTotal = m_Before + GetAfter();

				for( UINT n = 0; n < uTotal; n ++ ) {

					if( n == 0 || n == m_Before ) {

						rWork = C3REAL(uRadix - 1);

						rWork *= -1.0;
						}

					if( n < m_Before ) {

						rData += rWork;

						rWork *= uRadix;
						}
					else {
						rWork /= uRadix;

						rData += rWork;
						}
					}
				}
			}

		return R2I(rData);
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatNumber::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		C3INT nData = 0;

		UINT    uRadix = GetRadix();

		if( uRadix ) {

			C3INT nWork = uRadix - 1;

			UINT uTotal = m_Before + GetAfter();

			for( UINT n = 0; n < uTotal; n ++ ) {

				nData += nWork;

				nWork *= uRadix;
				}
			}

		return nData;
		}

	if( Type == typeReal ) {

		C3REAL rData = 0;

		UINT    uRadix = GetRadix();

		if( uRadix ) {

			C3REAL rWork = 0;

			UINT uTotal = m_Before + GetAfter();

			for( UINT n = 0; n < uTotal; n ++ ) {

				if( n == 0 || n == m_Before ) {

					rWork = C3REAL(uRadix - 1);
					}

				if( n < m_Before ) {

					rData += rWork;

					rWork *= uRadix;
					}
				else {
					rWork /= uRadix;

					rData += rWork;
					}
				}
			}

		return R2I(rData);
		}

	return CDispFormat::GetMax(Type);
	}

// Property Preservation

BOOL CDispFormatNumber::Preserve(CDispFormat *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CDispFormatSci)) ) {

		CDispFormatSci *pSci = (CDispFormatSci *) pOld;

		m_pPrefix = (CCodedText *) CItem::MakeFromItem(this, pSci->m_pPrefix);

		m_pUnits  = (CCodedText *) CItem::MakeFromItem(this, pSci->m_pUnits);

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CDispFormatNumber::MakeInitData(CInitData &Init)
{
	CDispFormat::MakeInitData(Init);

	Init.AddByte(BYTE(m_Radix));
	Init.AddByte(BYTE(m_Before));
	Init.AddByte(BYTE(m_After));
	Init.AddByte(BYTE(m_Leading));
	Init.AddByte(BYTE(m_Group));
	Init.AddByte(BYTE(m_Signed));

	Init.AddItem(itemVirtual, m_pPrefix);

	Init.AddItem(itemVirtual, m_pUnits);

	Init.AddItem(itemVirtual, m_pDynDP);

	return TRUE;
	}

// Meta Data Creation

void CDispFormatNumber::AddMetaData(void)
{
	CDispFormat::AddMetaData();

	Meta_AddInteger(Radix);
	Meta_AddInteger(Before);
	Meta_AddInteger(After);
	Meta_AddInteger(Leading);
	Meta_AddInteger(Group);
	Meta_AddInteger(Signed);
	Meta_AddVirtual(Prefix);
	Meta_AddVirtual(Units);
	Meta_AddVirtual(DynDP);

	Meta_SetName((IDS_NUMERIC_FORMAT));
	}

// Implementation

void CDispFormatNumber::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Signed",  m_Radix == 0);

	pHost->EnableUI(this, "After",   m_Radix == 0);

	pHost->EnableUI(this, "Leading", m_Radix == 0);

	pHost->EnableUI(this, "Group",   m_Radix <= 3);

	pHost->EnableUI(this, "DynDP",   m_Radix == 0);
	}

UINT CDispFormatNumber::GetRadix(void)
{
	switch( m_Radix ) {

		case 0: return 10;
		case 1: return 16;
		case 2: return 2;
		case 3: return 8;
		case 4: return 10;
		}

	return 0;
	}

UINT CDispFormatNumber::GetLimit(void)
{
	switch( m_Radix ) {

		case 0: return 10;
		case 1: return 8;
		case 2: return 32;
		case 3: return 10;
		case 4: return 10;
		}

	return 0;
	}

INT64 CDispFormatNumber::Power(UINT n)
{
	INT64 p = 1;

	C3INT r = GetRadix();

	while( n-- ) p *= r;

	return p ? p : r;
	}

BOOL CDispFormatNumber::IsGroupBoundary(UINT n)
{
	if( n ) {

		switch( m_Radix ) {

			case 1: return n % 4 == 0;
			case 2: return n % 4 == 0;
			case 3: return n % 3 == 0;
			}

		CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

		CLangManager *pLang   = pSystem->m_pLang;

		return pLang->IsGroupBoundary(n);
		}

	return FALSE;
	}

TCHAR CDispFormatNumber::GetGroupChar(void)
{
	switch( m_Radix ) {

		case 1: return '-';
		case 2: return '-';
		case 3: return '-';
		}

	CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	CLangManager *pLang   = pSystem->m_pLang;

	return pLang->GetNumGroupChar();
	}

TCHAR CDispFormatNumber::GetPointChar(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	CLangManager *pLang   = pSystem->m_pLang;

	return pLang->GetDecPointChar();
	}

UINT CDispFormatNumber::GetAfter(void)
{
	if( m_pDynDP ) {

		UINT uAfter = m_pDynDP->ExecVal();

		return max(0, min(m_After, uAfter));
		}

	return m_After;
	}

// End of File
