
#include "intern.hpp"

#include "MTSDDA.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA Comms Driver - Application Master / Network Slave
//

// Instantiator

ICommsDriver *	Create_MTSDDADriver(void)
{
	return New CMTSDDADriver;
	}

// Constructor

CMTSDDADriver::CMTSDDADriver(void)
{
	m_wID		= 0x4033;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "MTS";
	
	m_DriverName	= "DDA Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "MTS DDA Slave";

	m_DevRoot	= "Gauge";

	AddSpaces();
	}

// Binding Control

UINT	CMTSDDADriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CMTSDDADriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 4800;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void	CMTSDDADriver::AddSpaces(void)
{
	AddSpace( New CSpace(1, "ADD",	"Unit Address Array Assignment",	10, 1,  8, addrByteAsByte ) );
	AddSpace( New CSpace(2, "PROD",	"Product (Output Level 1)",		10, 1,  8, addrRealAsReal ) );
	AddSpace( New CSpace(3, "INTF",	"Interface (Output Level 2)",		10, 1,  8, addrRealAsReal ) );
	AddSpace( New CSpace(4, "TAVG",	"Average Temperature",			10, 1,  8, addrRealAsReal ) );
	AddSpace( New CSpace(5, "TIDT",	"Individual DT Temperature (5 / ADD)",	10, 1, 40, addrRealAsReal ) );
	AddSpace( New CSpace(8, "CADD",	"Change Unit Address ([ADD] -> Unit)",	10, 1,  8, addrByteAsByte ) );
	AddSpace( New CSpace(9, "SGEN", "Unrecognized Command String",		10, 0, 19, addrLongAsLong ) );
	AddSpace( New CSpace(10,"COMM", "Communications Active",		10, 0,  4, addrByteAsByte ) );
	}

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA Master Comms Driver
//

// Instantiator

ICommsDriver *	Create_MTSDDAMaster(void)
{
	return New CMTSDDAMasterDriver;
	}

// Constructor

CMTSDDAMasterDriver::CMTSDDAMasterDriver(void)
{
	m_wID		= 0x4050;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "MTS";
	
	m_DriverName	= "DDA Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "MTS DDA Master";

	m_DevRoot	= "Gauge";

	AddSpaces();
	}

// Binding Control

UINT	CMTSDDAMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CMTSDDAMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 4800;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void	CMTSDDAMasterDriver::AddSpaces(void)
{
	AddSpace( New CSpace(10, "PROD", "Product (# of decimal places)",		10, 1,  3, addrRealAsReal ) );
	AddSpace( New CSpace(13, "INTF", "Interface (# of decimal places)",		10, 1,  3, addrRealAsReal ) );
	AddSpace( New CSpace(25, "TAVG", "Average Temperature (# of decimal places)",	10, 0,  2, addrRealAsReal ) );
	AddSpace( New CSpace(28, "TID0", "Individual RTD Temp. (1 degree)",		10, 1,  5, addrRealAsReal ) );
	AddSpace( New CSpace(29, "TID1", "Individual RTD Temp. (0.2 degrees)",		10, 1,  5, addrRealAsReal ) );
	AddSpace( New CSpace(30, "TID2", "Individual RTD Temp. (0.02 degrees)",		10, 1,  5, addrRealAsReal ) );
	AddSpace( New CSpace(37, "FTMP", "Fast Temperatures (Fast Average = FTMP0)",	10, 0,  5, addrWordAsWord ) );
	AddSpace( New CSpace(75, "QFR",  "# of Floats and RTD's",			10, 1,  3, addrWordAsWord ) );
	AddSpace( New CSpace(76, "GRCV", "Gradient Control Variable",			10, 0,  0, addrRealAsReal ) );
	AddSpace( New CSpace(77, "FZP",  "Float Zero Positions",			10, 1,  2, addrRealAsReal ) );
	AddSpace( New CSpace(88, "WFZP", "Write FZ Positions using Calibrate Mode",	10, 1,  2, addrRealAsReal ) );
	AddSpace( New CSpace(78, "RTDP", "RTD Positions",				10, 1,  5, addrRealAsReal ) );
	AddSpace( New CSpace(97, "TERR", "Error Value for Read of RTDn",		10, 1,  5, addrWordAsWord ) );
	AddSpace( New CSpace(98, "CERR", "Bits 23-16 = Command #; 11-0 = Error #",	10, 0,  0, addrLongAsLong ) );
	AddSpace( New CSpace(99, "WNAK", "NAK on Write Command #",			10, 0,  0, addrLongAsLong ) );
	}

// End of File
