
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_HttpConnectionOptions_HPP

#define INCLUDE_HttpConnectionOptions_HPP

//////////////////////////////////////////////////////////////////////////
//
// HTTP Connection Options
//

class CHttpConnectionOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHttpConnectionOptions(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Property Save Filter
		BOOL SaveProp(CString Tag) const;

		// Conversion
		void PostConvert(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT    m_Advanced;
		BOOL    m_Tls;
		UINT    m_Port;
		UINT    m_SendTimeout;
		UINT    m_RecvTimeout;
		UINT    m_CertSource;
		CString m_CertFile;
		CString m_CertName;
		CString m_CertPass;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
