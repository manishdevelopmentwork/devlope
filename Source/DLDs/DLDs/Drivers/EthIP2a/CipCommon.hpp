
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CipCommon_HPP

#define	INCLUDE_CipCommon_HPP

//////////////////////////////////////////////////////////////////////////
//
// CIP Byte Ordering Macro
//

#define CipRo(x)		x = CipReOrder(x)

#define	CipAddHead(p, n, v)	*BuffAddHead(p, n) = CipReOrder((n) (v))

#define	CipAddTail(p, n, v)	*BuffAddTail(p, n) = CipReOrder((n) (v)) 

#define	CipStripHead(p, n)	CipReOrder(*BuffStripHead(p, n))

#define	CipStripTail(p, n)	CipReOrder(*BuffStripTail(p, n))

#define CipStripWord(p)		CipReOrder(*PU2(BuffStripHead(p, WORD)))

#define CipStripLong(p)		CipReOrder(*PU4(BuffStripHead(p, DWORD)))

//////////////////////////////////////////////////////////////////////////
//
// CIP Common Items
//

class CCipCommon
{
	protected:
		// CIP Status
		enum CIPStatus
		{
			statSuccess	= 0,
			statFailure	= 1,
			statPartial	= 6
			};

		// CIP Services
		enum CipService
		{
			cipGetAttrAll	    = 0x01,
			cipGetAttributeList = 0x03,
			cipCreate	    = 0x08,
			cipDelete	    = 0x09,
			cipReadItem         = 0x4C,
			cipWriteItem        = 0x4D,
			cipLoadWatchList    = 0x4E,
			cipForwardOpen      = 0x54,
			cipDumpFolder       = 0x55,
			};

		// CIP Classes
		enum CipClass
		{
			cipMessageRouter     = 0x02,
			cipConnectionManager = 0x06,
			cipProgram	     = 0x68,
			cipTagDirectory	     = 0x6B,
			cipStructuredType    = 0x6C,
			cipWatchList	     = 0xB2,
			};

		// Byte Ordering
		INLINE static BYTE  CipReOrder(BYTE  Data);
		INLINE static WORD  CipReOrder(WORD  Data);
		INLINE static DWORD CipReOrder(DWORD Data);

		// Buffer Allocation
		static CBuffer * CipAllocBuffer  (void);
		static bool      CipReleaseBuffer(CBuffer * &pBuff);

		// Trace Output
		static void AfxTrace(PCTXT pText, ...);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

INLINE BYTE CCipCommon::CipReOrder(BYTE Data)
{
	return Data;
	}

INLINE WORD CCipCommon::CipReOrder(WORD Data)
{
	return HostToIntel(Data);
	}

INLINE DWORD CCipCommon::CipReOrder(DWORD Data)
{
	return HostToIntel(Data);
	}

// End of File

#endif
