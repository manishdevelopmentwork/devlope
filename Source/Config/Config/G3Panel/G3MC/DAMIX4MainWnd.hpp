
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4MainWnd_HPP

#define INCLUDE_DAMix4MainWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAMix4Module;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Main Window
//

class CDAMix4MainWnd : public CProxyViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix4MainWnd(void);

protected:
	// Data Members
	CMultiViewWnd * m_pMult;
	CDAMix4Module * m_pItem;

	// Overridables
	void OnAttach(void);

	// Implementation
	void AddAnalogInputPages(void);
	void AddAnalogOutputPages(void);
	void AddDigitalInputPages(void);
	void AddDigitalOutputPages(void);
	void AddDigitalConfigPage(void);
};

// End of File

#endif
