
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WATCH_HPP

#define	INCLUDE_WATCH_HPP

//////////////////////////////////////////////////////////////////////////
//
// Comms Commands
//

#define	IDINIT	500
#define	IDDONE	501
#define	IDFAIL	502
#define	IDKILL	503

//////////////////////////////////////////////////////////////////////////
//
// Watch Window
//

class CWatchWnd : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CWatchWnd(CSystemWnd *pSystem, CWnd *pView);

		// Attributes
		BOOL IsShown(void) const;

		// Operations
		void Create(CRect Rect);
		void Show(BOOL fShow);

	protected:
		// Data Members
		CSystemWnd * m_pSystem;
		CWnd       * m_pView;
		BOOL	     m_fActive;
		BOOL	     m_fShow;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnClose(void);
		void OnActivate(UINT uMethod, BOOL fMinimize, CWnd &Wnd);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);
		void OnActivateApp(BOOL fActive, DWORD dwThread);
		void OnSize(UINT uCode, CSize Size);
		void OnSetFocus(CWnd &Wnd);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnKeyDown(UINT uCode, DWORD Flags);

		// Implementation
		BOOL LoadConfig(CRect &Rect);
		void SaveConfig(void);
	};

// End of File

#endif
