#ifndef	INCLUDE_ML1SER_HPP
	
#define	INCLUDE_ML1SER_HPP

#include "ml1drv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//


class CML1SerialDriver : public CML1Driver
{
	public:
		// Constructor
		CML1SerialDriver(void);

		// Destructor
		~CML1SerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
								
	protected:

		// Data Members
		CRC16	m_CRC;
		UINT	m_Baud;
		BOOL	m_fTrack;
		
		// Transport
	virtual BOOL Send(void);
	virtual BOOL Recv(void);
	virtual BOOL Respond(PBYTE pBuff);
	virtual BOOL Respond(CML1Dev * pDev);

		// Transport Helpers
		UINT FindRxSize(void);
					
	};

#endif

// End of File
