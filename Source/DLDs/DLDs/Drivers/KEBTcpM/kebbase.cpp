#include "intern.hpp"

#include "kebbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// KEB Base Master Driver
//

// Constructor

CKEBBaseMasterDriver::CKEBBaseMasterDriver(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	m_pBase = NULL;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;

	m_bLastDrop = 0xFF;

	m_bIID	= 1;
  	}

// Destructor

CKEBBaseMasterDriver::~CKEBBaseMasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CKEBBaseMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = STD;
	Addr.a.m_Offset = 0x2;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CKEBBaseMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsBroadcast() ) {

		return uCount;
		}
	
	if( Addr.a.m_Table == ERR ) {

		pData[0] = m_pBase->m_uError;

		return uCount;
		}

	if( IsProcessData(Addr.a.m_Table) ) {

		if( Addr.a.m_Table & 0x3 ) {

			return uCount;
			}
		}

	MakeRead(Addr);

	if( !IsProcessData(Addr.a.m_Table) ) {

		MakeMin(uCount, UINT((IsText(Addr.a.m_Table)) ? 20 : 1));
		}
	
	if( Transact() ) {

		BuffFromAscii();

		if( Addr.a.m_Type == addrLongAsLong ) {

			GetLongData(Addr, pData, uCount);

			return uCount;
			}

		GetWordData(Addr, pData, uCount);

		return uCount;
		}
		
	return SetError(Addr);
	}

CCODE MCALL CKEBBaseMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == ERR ) {

		if( pData[0] == 0 ) {
			
			m_pBase->m_uError = 0;
			}

		return uCount;
		}

	if( IsProcessData(Addr.a.m_Table) ) {

		if( !(Addr.a.m_Table & 0x3) ) {

			return uCount;
			}

		if( !(Addr.a.m_Table & SEND) ) {

			SaveProcessData(Addr, pData, uCount);

			return uCount;
			}

		if( pData[0] == 0 ) {

			return uCount;
			}
		}

	UINT uPtr = 0;
	
	if( NeedRead(Addr) ) {

		// If address is multi/broadcast, it is not possible to retrieve 
		// values of other data needed for the write request.

		if( IsBroadcast() ) {

			return CCODE_ERROR | CCODE_NO_RETRY;
			}

		MakeRead(Addr);
		
		if( !Transact() ) {

			return CCODE_ERROR;
			}

		for( UINT x = 7, y = 3; y < m_uPtr - 2; x++, y++ ) {

			m_bTxBuff[x] = m_bRxBuff[y];
			}

		uPtr = m_uPtr + 4;
		}
	
	Begin(GetService(Addr.a.m_Table), TRUE);

	if( !IsProcessData(Addr.a.m_Table) ) {

		WORD wOffset = GetOffset(Addr);

		AddAsciiWord(wOffset);

		if( IsText(Addr.a.m_Table) ) {

			AddByte('1');

			MakeMin(uCount, 20);

			AddAsciiByte(uCount * 4);
			}
		else {
			MakeMin(uCount, 1);
			}
		}

	AccumBCC(GetPosition(Addr, TRUE));

	AddData(Addr, pData, uCount);
	
	AddSet(Addr);

	if( uPtr > 1 ) {

		AccumBCC(uPtr - 2);
		}

	End(TRUE);

	if( IsBroadcast() ) {

		if( Send() ) {

			return uCount;
			}

		return CCODE_ERROR;
		}

	if( Transact() ) {

		return uCount;
		}
			
	return SetError(Addr);
	}

// Implementation

void CKEBBaseMasterDriver::NewAddress(void)
{
	if( m_bLastDrop != m_pBase->m_bDrop ) {
	
		AddByte(EOT);

		AddAsciiByte(m_pBase->m_bDrop);

		m_bLastDrop = m_pBase->m_bDrop;
		}     
	}

void CKEBBaseMasterDriver::Begin(BYTE bService, BOOL fWrite)
{
	m_uPtr = 0;

	NewAddress();

	if( fWrite ) {

		AddByte(STX);
		}

	m_bBCC = 0;

	AddByte(bService + 0x47);

	Increment(m_bIID);

	AddByte(m_pHex[m_bIID & 0xF]);	
	}

void CKEBBaseMasterDriver::End(BOOL fWrite)
{
	AddByte(fWrite ? ETX : ENQ);

	if( m_bBCC < 0x20 ) {

		m_bBCC += 0x20;
		}

	AddByte(m_bBCC);
	}

void CKEBBaseMasterDriver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uPtr] = bByte;

	m_uPtr++;

	m_bBCC = m_bBCC ^ bByte;
	}

void CKEBBaseMasterDriver::AddAsciiByte(BYTE bByte)
{
	AddByte(m_pHex[bByte / 0x10]);
	
	AddByte(m_pHex[bByte % 0x10]);
	}

void CKEBBaseMasterDriver::AddAsciiWord(WORD wWord)
{
	AddAsciiByte(HIBYTE(wWord));

	AddAsciiByte(LOBYTE(wWord));
	}

void CKEBBaseMasterDriver::AddAsciiLong(DWORD dwLong)
{
	AddAsciiWord(HIWORD(dwLong));

	AddAsciiWord(LOWORD(dwLong));
	}

void CKEBBaseMasterDriver::AddText(DWORD dwLong)
{
	AddByte(HIBYTE(HIWORD(dwLong)));

	AddByte(LOBYTE(HIWORD(dwLong)));

	AddByte(HIBYTE(LOWORD(dwLong)));

	AddByte(LOBYTE(LOWORD(dwLong)));
	}

void CKEBBaseMasterDriver::AddAsciiData(DWORD dwLong, UINT uType)
{
	if( uType == addrLongAsLong ) {

		AddAsciiLong(dwLong);
		}
	else {
		AddAsciiWord(LOWORD(dwLong));
		}
	}

void CKEBBaseMasterDriver::AddData(AREF Addr, PDWORD pData, UINT uCount)
{
	BOOL fProc = IsProcessData(Addr.a.m_Table);

	BOOL fText = IsText(Addr.a.m_Table);
		   
	for( UINT u = 0; u < uCount; u++ ) {

		if( fText ) {

			AddText(pData[u]);

			continue;
			}

		if( fProc ) {
		
			if( ((Addr.a.m_Table & PD1) == PD1) ) {

				if( u < elements(m_pBase->m_PDL) ) {

					AddAsciiLong(m_pBase->m_PDL[u]);
					}

				uCount = 2;

				continue;
				}

			if( u < elements(m_pBase->m_PDW) ) {

				AddAsciiWord(m_pBase->m_PDW[u]);
				}

			uCount = 4;

			continue;
			}
	
		AddAsciiData(pData[u], Addr.a.m_Type);
		}
	}

void CKEBBaseMasterDriver::SaveProcessData(AREF Addr, PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		switch( Addr.a.m_Table & 0xFC ) {

			case PD1: 

				m_pBase->m_PDL[Addr.a.m_Offset + u - 1] = pData[u];
						
				break;

			case PD2:

				m_pBase->m_PDW[Addr.a.m_Offset + u - 1] = pData[u];

				break;
				
			}
			
		} 
	}

void CKEBBaseMasterDriver::GetWordData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uPos = GetPosition(Addr, FALSE);

	for( UINT u = 0; u < uCount; u++ ) {

		WORD x = PU2(m_bRxBuff + uPos)[u];

		pData[u] = LONG(SHORT(MotorToHost(x)));
		}
	}

void CKEBBaseMasterDriver::GetLongData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uPos = GetPosition(Addr, FALSE);

	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = PU4(m_bRxBuff + uPos)[u];

		pData[u] = MotorToHost(x);
		}
	}

BYTE CKEBBaseMasterDriver::GetService(UINT uTable)
{
	return (uTable >> 2) - 1;
	}

WORD CKEBBaseMasterDriver::GetOffset(AREF Addr)
{
	if( Addr.a.m_Table == ETD ) {

		return (Addr.a.m_Extra << 13) | (Addr.a.m_Offset >> 3);
		}

	return Addr.a.m_Offset;
	}

void CKEBBaseMasterDriver::AddSet(AREF Addr)
{
	BYTE bSet = GetSet(Addr);
	
	if( Addr.a.m_Table <= STD ) {

		AddAsciiByte(1 << bSet);
		}
	}

BYTE CKEBBaseMasterDriver::GetSet(AREF Addr)
{
	BYTE bSet = Addr.a.m_Extra;
	
	if( Addr.a.m_Table == ETD ) {

		bSet = (Addr.a.m_Offset & 0x7);
		}

	return bSet;
	}

BYTE CKEBBaseMasterDriver::GetPosition(AREF Addr, BOOL fAscii)
{
	UINT uTable = Addr.a.m_Table;
	
	BYTE bBytes = ((Addr.a.m_Type == addrWordAsWord) ? 2 : 4);

	if( fAscii ) {

		bBytes *= 2;
		}
	
	BYTE bIndex = 0;

	if( !IsProcessData(uTable) ) {

		bIndex = uTable & 0x3;
		}
	
	BYTE bBegin = 3;

	if( IsText(uTable) ) {

		bBegin += 2;
		} 

	return bBegin + bBytes * bIndex;
	}

BOOL CKEBBaseMasterDriver::NeedRead(AREF Addr)
{
	switch( Addr.a.m_Table & 0xFC ) {

		case LMT:
		case CHR:
		case DSP:
		case PLI:
		
			return TRUE;
		}

	return FALSE;
	}

BOOL CKEBBaseMasterDriver::CheckFrame(void)
{
	m_bBCC = 0;

	if( m_bRxBuff[0] == STX ) {

		for( UINT u = 1; u < m_uPtr - 1; u++ ) {

			m_bBCC = m_bBCC ^ m_bRxBuff[u];
			}

		if( m_bBCC < 0x20 ) {

			m_bBCC += 0x20;
			}

		return (m_bBCC == m_bRxBuff[m_uPtr - 1]); 
		}

	return TRUE;
	}

void CKEBBaseMasterDriver::BuffFromAscii(void)
{
	for( UINT x = 3, y = 3;  y < m_uPtr - 3; x++, y++ ) {

		BYTE bByte = (FromAscii(m_bRxBuff[y++]) << 4);

		bByte |= FromAscii(m_bRxBuff[y]);

		m_bRxBuff[x] = bByte;
		}
	}

void CKEBBaseMasterDriver::MakeRead(AREF Addr)
{
	Begin(GetService(Addr.a.m_Table), FALSE);
	
	if( !IsProcessData(Addr.a.m_Table) ) {

		WORD wOffset = GetOffset(Addr);

		AddAsciiWord(wOffset);

		AddSet(Addr);
		
		if( IsText(Addr.a.m_Table) ) {

			AddByte('1');
			}
		}

	End(FALSE);
	}

void CKEBBaseMasterDriver::AccumBCC(UINT uPos)
{
	while ( m_uPtr < uPos ) {
	
		m_bBCC = m_bBCC ^ m_bTxBuff[m_uPtr];

		m_uPtr++;
		}
	}

BYTE CKEBBaseMasterDriver::GetIID(void)
{
	return ( m_bIID & 0xF );
	}

CCODE CKEBBaseMasterDriver::SetError(AREF Addr)
{
	if( m_uPtr ) {

		m_pBase->m_uError  = (GetService(Addr.a.m_Table) << 24);
	
		m_pBase->m_uError |= (GetOffset(Addr) << 8); 

		m_pBase->m_uError |= (GetSet(Addr) << 4);

		if( m_bRxBuff[2] == EOT || m_bRxBuff[2] == NAK ) {

			BYTE bError = FromAscii(m_bRxBuff[1]);

			m_pBase->m_uError |= bError;

			if( bError ) {

				return CCODE_ERROR | CCODE_NO_RETRY;
				}
			}
		}

	return CCODE_ERROR;
	}

// Helpers

void CKEBBaseMasterDriver::Increment(BYTE &bByte)
{
	bByte++;

	if( (bByte & 0xF) == 0 ) {

		bByte++;
		}
	}

BYTE CKEBBaseMasterDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' ) {
	
		if( bByte <= '9' ) {

			return bByte - '0';
			}

		return	bByte - '@' + 9;
		}

	return bByte;
	}

BOOL CKEBBaseMasterDriver::IsBroadcast(void)
{
	return m_pBase->m_bDrop >= MULTI;
	}

BOOL CKEBBaseMasterDriver::IsText(UINT uTable)
{
	switch( uTable ) {

		case NAM:

			return TRUE;
		}

	return FALSE;
	}

BOOL CKEBBaseMasterDriver::IsProcessData(UINT uTable)
{
	return !( uTable < PD1 || uTable == ERR );
	}

// Transport

BOOL CKEBBaseMasterDriver::Transact(void)
{
	return FALSE;
	}

BOOL CKEBBaseMasterDriver::Send(void)
{
	return FALSE;
	}

// End of File
