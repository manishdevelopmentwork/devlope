
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3PRIMS_HPP

#define	INCLUDE_G3PRIMS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <g3comms.hpp>

#include <g3ctrl.hpp>

#include <cxruby.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Class Identifiers
//

#define IDC_FONT	0x7001
#define IDC_IMAGE	0x7002
#define IDC_UI_MANAGER	0x7003
#define IDC_DISP_PAGE	0x7004

//////////////////////////////////////////////////////////////////////////
//
// Not-Available Colors
//

#define naBack		GetRGB(0,0,0)
#define	naFeature	GetRGB(10,10,10)
#define	naPen		GetRGB(12,12,12)
#define	naText		GetRGB(12,12,12)
#define	naNone		0x8000

//////////////////////////////////////////////////////////////////////////
//
// Collections
//

typedef CArray<R2> CR2Array;

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUITask;
class CUIDataServer;
class CMusicPlayer;
class CPDFTask;
class CFontManager;
class CRubyPatternLib;
class CPrim;
class CPrimList;
class CPrimColor;
class CEventMap;
class CSecDesc;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUISystem;
class CUIManager;
class CDispPage;
class CDispPageProps;
class CRemoteTask;

//////////////////////////////////////////////////////////////////////////
//
// Standard Properties for Pages
//

enum
{
	ppName		= 1,
	ppLabel		= 2,
	ppDescription	= 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// UI System Item
//

class CUISystem : public CCommsSystem
{
	public:
		// Constructor
		CUISystem(void);

		// Destructor
		~CUISystem(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Server Access
		IDataServer * GetDataServer(void) const;

		// Operations
		void SetDataServer(IDataServer *pData);
		void RegisterUITask(void);

		// System Calls
		void SystemSave(void);
		void GetTaskList(CTaskList &List);
		BOOL IsPriority(void);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Item Properties
		CUIManager    * m_pUI;
		CUITask       * m_pTask;
		CMusicPlayer  * m_pMusic;
		CPDFTask      * m_pPDF;
		CRemoteTask   * m_pRemote;

		// Data Members
		CUIDataServer * m_pUserServer;

		// Instance Pointer
		static CUISystem * m_pThis;

	protected:
		// Data Members
		HTASK m_hTask;

		// Implementation
		void DoLoad(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// UI Manager Object
//

class CUIManager : public CCodedHost
{
	public:
		// Constructor
		CUIManager(void);

		// Destructor
		~CUIManager(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		UINT GetEditFlags(void) const;

		// Operations
		void SetScan(UINT Code);
		void ApplyMute(void);
		void CancelMute(void);
		BOOL Protect(PVOID pItem, UINT Protect, UINT uTrans);

		// Data Members
		UINT              m_hPage;
		CDispPage       * m_pPage;
		CEventMap       * m_pEvents;
		CFontManager    * m_pFonts;
		CRubyPatternLib * m_pPatterns;
		CCodedItem      * m_pOnStart;
		CCodedItem      * m_pOnInit;
		CCodedItem      * m_pOnUpdate;
		CCodedItem	* m_pOnSecond;
		UINT	          m_TimeLock;
		UINT              m_TimePad;
		UINT              m_TimeDisp;
		UINT	          m_PadSize;
		UINT	          m_PadLayout;
		UINT	          m_PadNumeric;
		UINT	          m_ShowNext;
		UINT	          m_AutoEntry;
		UINT	          m_EntryOrder;
		UINT	          m_TwoFlag;
		UINT	          m_TwoMulti;
		UINT	          m_TwoNumeric;
		UINT	          m_Beeper;
		UINT	          m_PopAlignH;
		UINT	          m_PopAlignV;
		UINT	          m_GMC1;
		UINT              m_LedAlarm;
		UINT              m_LedOrb;
		UINT              m_LedHome;
		UINT	          m_PopKeypad;
		CCodedItem      * m_pTimeDisp;

	protected:
		// Data Members
		PVOID m_pLast;
		UINT  m_uLast;
		UINT  m_uOkay;

		// Implementation
		BOOL AreYouSure(void);
		BOOL UnlockAction(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Page Item
//

class CDispPage : public CItem
{
	public:
		// Constructor
		CDispPage(void);

		// Destructor
		~CDispPage(void);

		// Initialization
		void Load(PCBYTE &pData);
		void Fast(PCBYTE &pData);

		// Popup Adjustment
		void FindPopup(void);
		void MovePopup(int cx, int cy);
		void PopupMenu(void);

		// Attributes
		COLOR GetBackColor(void) const;
		BOOL  IsEventReady(UINT uEvent) const;

		// Operations
		BOOL SetScan(UINT Code);
		void FireEvent(UINT uEvent);
		BOOL BuildFocusList(CDispPage *pMaster);
		BOOL FindFocus(CPrim * &pFocus, UINT &uFocus, P2 const &Pos);
		BOOL NextFocus(CPrim * &pFocus, UINT &uFocus);
		BOOL PrevFocus(CPrim * &pFocus, UINT &uFocus);
		BOOL DrawPrep(IGDI *pGDI, IRegion *pErase, R2 const *pClip, UINT uClip);
		BOOL DrawExec(IGDI *pGDI, IRegion *pDirty, R2 const *pClip, UINT uClip);
		BOOL LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);

		// Property Access
		DWORD GetProp(WORD ID, UINT Type);

		// Data Members
		CUnicode         m_Name;
		CPrimList      * m_pList;
		CEventMap      * m_pEvents;
		CDispPageProps * m_pProps;
		S2		 m_Size;
		R2		 m_Rect;

	protected:
		// Sort Pointer
		static CDispPage * m_pSort;

		// Focus List
		CArray <CPrim *> m_Focus;
		CArray <CPrim *> m_Event;
		CArray <UINT   > m_Order;

		// Implementation
		void  MakeEventList(CPrimList *pList);
		void  MakeFocusList(CPrimList *pList);
		void  SortFocusList(void);
		DWORD FindLabel(void);
		DWORD FindDesc(void);

		// Sort Function
		static int SortFuncRows(PCVOID s1, PCVOID s2);
		static int SortFuncCols(PCVOID s1, PCVOID s2);
	};

//////////////////////////////////////////////////////////////////////////
//
// Page Properties
//

class CDispPageProps : public CCodedHost
{
	public:
		// Constructor
		CDispPageProps(void);

		// Destructor
		~CDispPageProps(void);

		// Initialization
		void Load(PCBYTE &pData);
		void Fast(PCBYTE &pData);

		// Operations
		void SetScan(UINT Code);

		// Data Members
		CCodedText * m_pLabel;
		CUnicode     m_Desc;
		CSecDesc   * m_pSec;
		CPrimColor * m_pBack;
		UINT         m_Master;
		CCodedItem * m_pOnSelect;
		CCodedItem * m_pOnRemove;
		CCodedItem * m_pOnUpdate;
		CCodedItem * m_pOnSecond;
		CCodedItem * m_pOnTimeout;
		UINT	     m_PageNext;
		UINT	     m_PagePrev;
		UINT	     m_PageExit;
		UINT	     m_AutoEntry;
		UINT	     m_EntryOrder;
		UINT	     m_NoBack;
		UINT	     m_Timeout;
		UINT	     m_Update;
		UINT	     m_PopAlign;
		UINT	     m_PopAlignH;
		UINT	     m_PopAlignV;
		UINT	     m_PopMaster;
		UINT         m_PopKeypad;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMusicPlayer;

//////////////////////////////////////////////////////////////////////////
//
// Music Player
//

class CMusicPlayer : public ITaskEntry
{
	public:
		// Constructor
		CMusicPlayer(void);

		// Destructor
		~CMusicPlayer(void);

		// Task List
		void GetTaskList(CTaskList &List);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Operations
		BOOL PlayTune(PUTF pTune);
		BOOL StopTune(void);

	protected:
		// Data Members
		IEvent * m_pEvent;
		IMutex * m_pLock;
		PUTF     m_pTune;
		BOOL     m_fStop;

		// Implementation
		BOOL PlayRTTTL(PCUTF pText);
		void StopRTTTL(void);
		UINT GetNote  (WCHAR c);
		void PlayNote (UINT uNote, UINT uScale, UINT uDuration, BOOL fDotted, UINT uBPM);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUIDataServer;

//////////////////////////////////////////////////////////////////////////
//
// Data Server
//

class CUIDataServer : public CDataServer
{
	public:
		// Constructor
		CUIDataServer(CUISystem *pSystem);

		// Destructor
		~CUIDataServer(void);

		// Attributes
		CPrim * GetPrim(void) const;

		// Operations
		void SetPrim(CPrim *pPrim);

		// IBase Methods
		UINT Release(void);

		// IDataServer Methods
		WORD  CheckID(WORD  ID);
		BOOL  IsAvail(DWORD ID);
		BOOL  SetScan(DWORD ID, UINT Code);
		DWORD GetData(DWORD ID, UINT Type, UINT Flags);
		BOOL  SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data);
		DWORD GetProp(DWORD ID, WORD Prop, UINT Type);
		DWORD RunFunc(WORD  ID, UINT uParam, PDWORD pParam);
		BOOL  GetName(DWORD ID, UINT m, PSTR pName, UINT uName);

	protected:
		// Function Table
		static CFunc const m_FuncTable[];

		// Static Data
		static CUIDataServer * m_pThis;

		// Data Members
		CUISystem  * m_pSystem;
		CUIManager * m_pUI;
		CUITask    * m_pTask;
		CPrim      * m_pPrim;
		BOOL         m_fFreeze;

		// Implementation
		BOOL RunFunc(DWORD &Data, WORD ID, UINT uParam, PDWORD pParam);
		BOOL IsPressed(void);
		WORD GetWord(PCBYTE &pData);

		// Function Library
		static void  GotoPage(UINT p);
		static void  GotoPrevious(void);
		static void  GotoNext(void);
		static C3INT CanGotoPrevious(void);
		static C3INT CanGotoNext(void);
		static void  SetLanguage(UINT c);
		static C3INT GetLanguage(void);
		static void  PlayRTTTL(PUTF p);
		static void  StopRTTTL(void);
		static void  SirenOff(void);
		static void  SirenOn(void);
		static void  ShowPopup(UINT p);
		static void  ShowNested(UINT p);
		static void  ShowMenu(UINT p);
		static void  HidePopup(void);
		static void  HideAllPopups(void);
		static C3INT ShowModal(UINT p);
		static void  EndModal(C3INT c);
		static void  DispOn(void);
		static void  DispOff(void);
		static void  LogOn(void);
		static void  LogOff(void);
		static C3INT TestAccess(C3INT r, PUTF p);
		static C3INT HasAccess(C3INT r);
		static void  PostKey(C3INT c, C3INT t);
		static C3INT GetUpDownData(C3INT d, C3INT r);
		static C3INT GetUpDownStep(C3INT d, C3INT r);
		static C3INT Flash(C3INT f);
		static C3INT ColFlash(C3INT f, C3INT c1, C3INT c2);
		static C3INT ColSelFlash(C3INT e, C3INT f, C3INT c1, C3INT c2, C3INT c3);
		static C3INT ColPick2(C3INT n, C3INT c1, C3INT c2);
		static C3INT ColBlend(C3REAL d, C3REAL f, C3REAL t, C3INT c1, C3INT c2) DS_ABI;
		static C3INT ColGetRed(C3INT c);
		static C3INT ColGetGreen(C3INT c);
		static C3INT ColGetBlue(C3INT c);
		static C3INT ColGetRGB(C3INT r, C3INT g, C3INT b);
		static C3INT PrintScreenToFile(PUTF p, PUTF f, C3INT c);
		static C3INT ColPick4(C3INT n1, C3INT n2, C3INT c1, C3INT c2, C3INT c3, C3INT c4);
		static C3INT SaveSecurityDatabase(C3INT m, PUTF p);
		static C3INT LoadSecurityDatabase(C3INT m, PUTF p);
		static PUTF  GetCurrentUserName(void);
		static PUTF  GetCurrentUserRealName(void);
		static C3INT GetCurrentUserRights(void);
		static C3INT HasAllAccess(C3INT r);

		// Screen Dump Helpers
		static BOOL SaveRaw(CAutoFile &File);
		static BOOL SaveRLE(CAutoFile &File);
		static BOOL SavePal(CAutoFile &File);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CFontManager;
class CFontItem;

//////////////////////////////////////////////////////////////////////////
//
// Font Manager
//

class CFontManager : public CItem
{
	public:
		// Constructor
		CFontManager(void);

		// Destructor
		~CFontManager(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Font Selection
		BOOL Select(IGDI *pGDI, UINT uFont);

		// Attributes
		int  GetHeight(UINT uFont);
		BOOL IsAntiAliased(UINT uFont);
		
	protected:
		// Data Members
		UINT         m_uCount;
		CFontItem ** m_ppFont;
	};

//////////////////////////////////////////////////////////////////////////
//
// Font Item
//

class CFontItem : public CItem, public IGDIFont
{
	public:
		// Constructor
		CFontItem(void);

		// Destructor
		~CFontItem(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAntiAliased(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IGDIFont Attributes
		BOOL IsProportional(void);
		int  GetBaseLine(void);
		int  GetGlyphWidth(WORD c);
		int  GetGlyphHeight(WORD c);

		// IGDIFont Operations
		void InitBurst(IGDI *pGDI, CLogFont const &Font);
		void DrawGlyph(IGDI *pGDI, int &x, int &y, WORD c);
		void BurstDone(IGDI *pGDI, CLogFont const &Font);

	protected:
		// Data Members
		ULONG  m_uRefs;
		int    m_Size;
		int    m_nBase;
		UINT   m_uCount;
		PCWORD m_pList;
		PCWORD m_pWidth;
		PCUINT m_pPos;
		PCBYTE m_pData;
		WCHAR  m_cFrom;
		WCHAR  m_cTo;
		BOOL   m_fFill;
		COLOR  m_Fore;
		COLOR  m_Back;
		int    m_xSpace;
		int    m_xDigit;

		// Implementation
		UINT FindGlyph(WCHAR cData);
		BOOL IsFixed(WORD c);
		BOOL IsFixedDigit(WORD c);
		BOOL IsFixedLetter(WORD c);
		BOOL IsSpace(WCHAR cData);
		BOOL IsExplicitCode(WCHAR cData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTextFlow;

//////////////////////////////////////////////////////////////////////////
//
// Text Flow Object
//

class CTextFlow
{
	public:
		// Constructor
		CTextFlow(void);

		// Format
		struct CFormat
		{
			INT   m_AlignH;
			INT   m_AlignV;
			INT   m_Lead;
			COLOR m_Color;
			COLOR m_Shadow;
			BOOL  m_fMove;
			UINT  m_MoveDir;
			UINT  m_MoveStep;
			};

		// Operations
		void Dirty(void);
		BOOL Flow(IGDI *pGDI, CUnicode const &Text, R2 const &Rect);
		BOOL Draw(IGDI *pGDI, CFormat  const &Fmt);

	protected:
		// Data Members
		CUnicode        m_Text;
		R2              m_Rect;
		S2		m_Size;
		CArray <CRange> m_Lines;

		// Implementation
		BOOL IsBreak(WCHAR cData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimColor;
class CCodedHost;
class CPrimBrush;
class CPrimTankFill;
class CPrimRubyShade;
class CPrimPen;
class CPrimImage;
class CPrimBlock;
class CPrimData;
class CPrimText;
class CPrimAction;

//////////////////////////////////////////////////////////////////////////
//
// Primitive Color
//

class CPrimColor : public CCodedItem
{
	public:
		// Constructor
		CPrimColor(COLOR NotAvail);

		// Destructor
		~CPrimColor(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Color Access
		COLOR GetColor(void);

	protected:
		// Data Members
		COLOR m_NotAvail;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Brush
//

class CPrimBrush : public CCodedHost
{
	public:
		// Constructor
		CPrimBrush(void);

		// Destructor
		~CPrimBrush(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsNull(void) const;

		// Operations
		void SetScan(UINT Code);
		BOOL DrawPrep(IGDI *pGDI);

		// Drawing
		void FillRect   (IGDI *pGDI, R2 Rect);
		void FillEllipse(IGDI *pGDI, R2 Rect, UINT uType);
		void FillWedge  (IGDI *pGDI, R2 Rect, UINT uType);
		void FillPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound);

		// Item Properties
		UINT         m_Pattern;
		CPrimColor * m_pColor1;
		CPrimColor * m_pColor2;
		UINT         m_Flip;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR m_Color1;
			COLOR m_Color2;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Static Data
		static BOOL  m_ShadeMode;
		static COLOR m_ShadeCol1;
		static COLOR m_ShadeCol2;

		// Color Mixing
		static COLOR Mix16(COLOR a, COLOR b, int p, int c);
		static DWORD Mix32(COLOR a, COLOR b, int p, int c);

		// Shaders
		static BOOL Shader1(IGDI *pGDI, int p, int c);
		static BOOL Shader2(IGDI *pGDI, int p, int c);

		// Implementation
		void DoLoad(PCBYTE &pData);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Tank Fill
//

class CPrimTankFill : public CPrimBrush
{
	public:
		// Constructor
		CPrimTankFill(void);

		// Destructor
		~CPrimTankFill(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		void SetScan(UINT Code);
		BOOL DrawPrep(IGDI *pGDI);

		// Drawing
		void FillRect   (IGDI *pGDI, R2 Rect);
		void FillEllipse(IGDI *pGDI, R2 Rect, UINT uType);
		void FillWedge  (IGDI *pGDI, R2 Rect, UINT uType);
		void FillPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound);

		// Item Properties
		UINT         m_Mode;
		CCodedItem * m_pValue;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		CPrimColor * m_pColor3;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR  m_Color3;
			C3REAL m_Data;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Static Data
		static COLOR  m_ShadeCol3;
		static C3REAL m_ShadeData;

		// Shader
		static BOOL Shader(IGDI *pGDI, int p, int c);

		// Rounding
		static C3INT Round(C3REAL Data);

		// Implementation
		BOOL PrepTankFill(void);
		BOOL IsValueAvail(void);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Pen
//

class CPrimPen : public CCodedHost
{
	public:
		// Constructor
		CPrimPen(void);

		// Destructor
		~CPrimPen(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsNull(void) const;
		int  GetAdjust(void) const;
		int  GetWidth(void) const;

		// Operations
		BOOL AdjustRect(R2 &Rect);
		void Configure(IGDI *pGDI);
		void SetScan(UINT Code);
		BOOL DrawPrep(IGDI *pGDI);

		// Drawing
		void DrawRect   (IGDI *pGDI, R2 Rect);
		void DrawWedge  (IGDI *pGDI, R2 Rect, UINT uType);
		void DrawEllipse(IGDI *pGDI, R2 Rect, UINT uType);
		void DrawPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound);

		// Item Properties
		UINT         m_Width;
		UINT         m_Corner;
		CPrimColor * m_pColor;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR m_Color;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Image
//

class CPrimImage : public CItem
{
	public:
		// Constructor
		CPrimImage(void);

		// Destructor
		~CPrimImage(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsTransparent(R2 Rect) const;
		BOOL IsAntiAliased(void) const;

		// Operations
		void DrawItem(IGDI *pGDI, R2 Rect, UINT rop);
		void DrawItem(IGDI *pGDI, R2 Rect, UINT rop, CPrimRubyShade *pShade);

		// Data Members
		UINT   m_Image;
		PCBYTE m_pData;
		BOOL   m_fTile;
		int    m_xAct;
		int    m_yAct;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Block
//

class CPrimBlock : public CCodedHost
{
	public:
		// Constructor
		CPrimBlock(CPrim *pPrim);

		// Destructor
		~CPrimBlock(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAntiAliased(void) const;

		// Operations
		BOOL SelectFont(IGDI *pGDI);

		// Data Members
		INT   m_AlignH;
		INT   m_AlignV;
		INT   m_Lead;
		UINT  m_Font;
		UINT  m_MoveDir;
		UINT  m_MoveStep;
		R2    m_Margin;

	protected:
		// Data Members
		CPrim * m_pPrim;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Data Block
//

class CPrimData : public CPrimBlock
{
	public:
		// Constructor
		CPrimData(CPrim *pPrim);

		// Destructor
		~CPrimData(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Atrributes
		BOOL IsAvail(void) const;
		BOOL IsTransparent(void) const;
		BOOL IsTagRef(void) const;
		BOOL GetTagItem(CTag * &pTag) const;
		BOOL GetTagLabel(CCodedText * &pLabel) const;
		BOOL GetTagFormat(CDispFormat * &pFormat) const;
		BOOL GetTagColor(CDispColor * &pColor) const;
		BOOL GetDataLabel(CCodedText * &pLabel) const;
		BOOL GetDataFormat(CDispFormat * &pFormat) const;
		BOOL GetDataColor(CDispColor * &pColor) const;

		// Operations
		void SetScan(UINT Code);
		BOOL DrawPrep(IGDI *pGDI);
		void DrawItem(IGDI *pGDI, R2 Rect);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		CCodedItem *  m_pValue;
		UINT	      m_Entry;
		UINT	      m_Flash;
		UINT	      m_Layout;
		UINT	      m_Content;
		UINT	      m_TagLimits;
		UINT	      m_TagLabel;
		UINT	      m_TagFormat;
		UINT	      m_TagColor;
		CCodedItem  * m_pEnable;
		CCodedItem  * m_pValidate;
		CCodedItem  * m_pOnSetFocus;
		CCodedItem  * m_pOnKillFocus;
		CCodedItem  * m_pOnComplete;
		CCodedItem  * m_pOnError;
		CCodedText  * m_pLabel;
		CCodedItem  * m_pLimitMin;
		CCodedItem  * m_pLimitMax;
		UINT          m_Local;
		UINT	      m_FormType;
		UINT	      m_ColType;
		UINT	      m_UseBack;
		CDispFormat * m_pFormat;
		CDispColor  * m_pColor;
		CPrimColor  * m_pTextColor;
		CPrimColor  * m_pTextShadow;
		CCodedText  * m_pKeyTitle;
		CCodedText  * m_pKeyStatus;
		UINT	      m_KeyNumeric;
		UINT	      m_Accel;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			CUnicode m_Label;
			CUnicode m_Value;
			COLOR    m_Back1;
			COLOR    m_Back2;
			COLOR    m_Fore1;
			COLOR    m_Fore2;
			COLOR    m_Shadow;
			BOOL     m_fFocus;
			UINT	 m_uPress;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Data Members
		UINT        m_uTag;
		BOOL        m_fFocus;
		UINT        m_uPress;
		CEditCtx  * m_pEdit;
		CTextFlow   m_Flow;

		// Message Handlers
		UINT OnTakeFocus(UINT uParam);
		UINT OnSetFocus(void);
		UINT OnKillFocus(void);
		UINT OnTouchInit(void);
		UINT OnTouchDown(void);
		UINT OnTouchRepeat(void);
		UINT OnTouchUp(void);
		UINT OnKeyDown(UINT uCode);
		UINT OnKeyUp(UINT uCode);
		UINT OnTestKeypad(void);
		UINT OnShowKeypad(void);
		UINT OnInitKeypad(IKeypad *pKeypad);
		UINT OnDrawKeypad(IKeypad *pKeypad);

		// Edit Support
		BOOL InitEdit(void);
		BOOL KillEdit(void);
		BOOL ReadEditData(void);
		UINT EditFunc(UINT uCode);

		// Drawing Helpers
		void DrawSingle(IGDI *pGDI, R2 Rect);
		void DrawMulti (IGDI *pGDI, R2 Rect);

		// Color Setting
		void SetColors(IGDI *pGDI, UINT uMode);
		void SetColors(IGDI *pGDI, COLOR Back, COLOR Fore);

		// Limit Access
		DWORD GetMinValue(UINT Type) const;
		DWORD GetMaxValue(UINT Type) const;

		// Text Extraction
		CUnicode FindLabelText(void);
		CUnicode FindValueText(DWORD Data, UINT Type);

		// Implementation
		BOOL  Execute(CCodedItem *pAction);
		COLOR FindCompColor(COLOR c);
		UINT  GetEditFlags(void);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Text Block
//

class CPrimText : public CPrimBlock
{
	public:
		// Constructor
		CPrimText(CPrim *pPrim);

		// Destructor
		~CPrimText(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL IsTransparent(void) const;

		// Operations
		void SetScan(UINT Code);
		BOOL DrawPrep(IGDI *pGDI);
		void DrawItem(IGDI *pGDI, R2 Rect);

		// Data Members
		CCodedText * m_pText;
		CPrimColor * m_pColor;
		CPrimColor * m_pShadow;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			CUnicode m_Text;
			COLOR    m_Color;
			COLOR    m_Shadow;
			BOOL     m_fPress;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Layout Data
		CTextFlow m_Flow;

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Action
//

class CPrimAction : public CCodedHost
{
	public:
		// Constructor
		CPrimAction(void);

		// Destructor
		~CPrimAction(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAvail(void) const;
		BOOL IsEnabled(void) const;

		// Operations
		void SetScan(UINT Code);
		BOOL RunEvent(UINT uTrans, BOOL fLocal, int px, int py);
		BOOL RunEvent(UINT uTrans, BOOL fLocal);

		// Data Members
		UINT	      m_Delay;
		UINT	      m_Protect;
		UINT	      m_Local;
		CCodedItem  * m_pEnable;
		CCodedItem  * m_pPress;
		CCodedItem  * m_pRepeat;
		CCodedItem  * m_pRelease;

	protected:
		// Implementation
		void DoLoad(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CEventMap;
class CEventEntry;
class CDualEvent;

//////////////////////////////////////////////////////////////////////////
//
// Event Map
//

class CEventMap : public CItem
{
	public:
		// Constructor
		CEventMap(void);

		// Destructor
		~CEventMap(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAvail(UINT uCode) const;

		// Operations
		void SetScan(UINT Code);
		BOOL RunEvent(UINT uCode, UINT uTrans, BOOL fLocal);

	protected:
		// Data Members
		UINT          m_uCount;
		CEventEntry * m_pEvent;

		// Implementation
		BOOL SetDelayTimer(CEventEntry &Event);
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Map Entry
//

class CEventEntry : public CPrimAction
{
	public:
		// Constructor
		CEventEntry(void);

		// Destructor
		~CEventEntry(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		UINT m_Code;
		BOOL m_fTime;
		UINT m_uTime;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimList;
class CPrim;
class CPrimLine;
class CPrimWithText;
class CPrimSet;
class CPrimMove;
class CPrimMove2D;
class CPrimMovePolar;
class CPrimGroup;
class CPrimWidget;
class CPrimWidgetPropData;
class CPrimWidgetPropList;
class CPrimWidgetProp;

//////////////////////////////////////////////////////////////////////////
//
// Primitive Collections
//

typedef CArray <CPrimWidget *> CWidgetStack;

//////////////////////////////////////////////////////////////////////////
//
// Primitive Messages
//

enum
{
	msgTakeFocus	= 201,
	msgLocalOnly	= 202,
	msgOneSecond	= 203,
	msgIsGroup	= 204,
	msgIsPressed	= 205,
	msgUsesKeypad   = 206,
	msgTestKeypad	= 207,
	msgShowKeypad   = 208,
	msgInitKeypad   = 209,
	msgDrawKeypad	= 210,
	msgHasEvents    = 211,
	msgTestEvent    = 212,
	msgFireEvent    = 213,
	msgTimer	= 214,
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive List
//

class CPrimList : public CItem
{
	public:
		// Constructor
		CPrimList(void);

		// Destructor
		~CPrimList(void);

		// Initialization
		void Load(PCBYTE &pData, CPrim *pParent);
		
		// Data Members
		UINT     m_uCount;
		CPrim ** m_ppPrim;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Primitive
//

class CPrim : public CCodedHost
{
	public:
		// Class Creation
		static CPrim * MakeObject(UINT uType);

		// Constructor
		CPrim(void);

		// Destructor
		~CPrim(void);

		// IBase
		UINT Release(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Stack Control
		static void ClearWidgetStack(void);
		static void SuspendWidgetStack(CWidgetStack &Stack);
		static void RestoreWidgetStack(CWidgetStack &Stack);
		static void SuspendWidgetStack(void);
		static void RestoreWidgetStack(void);
		static void TempLeave(CPrimWidget *pWidget);
		static void TempEnter(CPrimWidget *pWidget);

		// Attributes
		BOOL IsPressed(void) const;
		BOOL IsVisible(void) const;
		UINT GetHandle(void);

		// Operations
		UINT SendMessage(UINT uMsg, UINT uParam);
		void SetHandle(UINT uHandle);

		// Overridables
		virtual void SetScan(UINT Code);
		virtual BOOL HitTest(P2 const &Pos);
		virtual BOOL HitTest(IRegion *pDirty);
		virtual void MovePrim(int cx, int cy);
		virtual void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		virtual void DrawExec(IGDI *pGDI, IRegion *pDirty);
		virtual void DrawPrim(IGDI *pGDI);
		virtual void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);
		virtual UINT OnMessage(UINT uMsg, UINT uParam);
		virtual R2   GetBackRect(void);

		// Public Data
		R2           m_DrawRect;
		CCodedItem * m_pVisible;
		CPrim      * m_pParent;

	protected:
		// Static Data
		static BOOL	    m_fSuspend;
		static BOOL         m_fStacked;
		static BOOL         m_fExStack;
		static CWidgetStack m_Stack;

		// Data Members
		BOOL m_fShow;
		BOOL m_fChange;
		UINT m_uHandle;

		// Stack Helpers
		static void EnterWidgetStack(CWidgetStack &List);
		static void LeaveWidgetStack(CWidgetStack &List);

		// Implementation
		BOOL SelectFont(IGDI *pGDI, UINT Font);
		BOOL BuildWidgetStack(CWidgetStack &List);
		BOOL SetShow(CR2Array &Erase, BOOL fShow);
		BOOL SetDelayTimer(UINT uStart, UINT uDelay);
	};

//////////////////////////////////////////////////////////////////////////
//
// Line Primitive
//

class CPrimLine : public CPrim
{
	public:
		// Constructor
		CPrimLine(void);

		// Destructor
		~CPrimLine(void);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		R2   GetBackRect(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		CPrimPen * m_pPen;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Primitive with Text
//

class CPrimWithText : public CPrim
{
	public:
		// Constructor
		CPrimWithText(void);

		// Destructor
		~CPrimWithText(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(ITouchMap *pTouch);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Public Data
		R2            m_TextRect;
		CPrimText   * m_pTextItem;
		CPrimData   * m_pDataItem;
		CPrimAction * m_pAction;

	protected:
		// Data Members
		BOOL m_fTrans;
		BOOL m_fAlias;
		BOOL m_fPress;
		BOOL m_fTime;
		UINT m_uTime;
		UINT m_Delay;
		int  m_px;
		int  m_py;

		// Message Handlers
		UINT OnTakeFocus(UINT uMsg, UINT uParam);
		UINT OnTouchDown(void);
		UINT OnTouchRepeat(void);
		UINT OnTouchUp(void);
		UINT OnBlockRemote(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Set Primitive
//

class CPrimSet : public CPrim
{
	public:
		// Constructor
		CPrimSet(void);

		// Destructor
		~CPrimSet(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);
		void DrawExec(IGDI *pGDI, IRegion *pDirty);
		UINT OnMessage(UINT uMsg, UINT uParam);
		R2   GetBackRect(void);

		// Public Data
		CPrimList * m_pList;

	protected:
		// Implementation
		void MoveList(int cx, int cy);
		void PrepList(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
	};

//////////////////////////////////////////////////////////////////////////
//
// Movement Primitive
//

class CPrimMove : public CPrimSet
{
	public:
		// Constructor
		CPrimMove(void);

		// Destructor
		~CPrimMove(void);

		// Overridables
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			int m_xp;
			int m_yp;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;
		R2   m_InitRect;
		BOOL m_fValid;
		BOOL m_fInit;
		
		// Implementation
		void GetUsedRect(R2 &Rect);
		void MoveList(int cx, int cy);

		// Context Creation
		virtual void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// 2D Movement Primitive
//

class CPrimMove2D : public CPrimMove
{
	public:
		// Constructor
		CPrimMove2D(void);

		// Destructor
		~CPrimMove2D(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);

		// Data Members
		CCodedItem * m_pPosX;
		CCodedItem * m_pMinX;
		CCodedItem * m_pMaxX;
		CCodedItem * m_pPosY;
		CCodedItem * m_pMinY;
		CCodedItem * m_pMaxY;

	protected:
		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Polar Movement Primitive
//

class CPrimMovePolar : public CPrimMove
{
	public:
		// Constructor
		CPrimMovePolar(void);

		// Destructor
		~CPrimMovePolar(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);

		// Data Members
		CCodedItem * m_pPosT;
		CCodedItem * m_pMinT;
		CCodedItem * m_pMaxT;
		CCodedItem * m_pPosR;
		CCodedItem * m_pMinR;
		CCodedItem * m_pMaxR;

	protected:
		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Group Primitive
//

class CPrimGroup : public CPrimSet
{
	public:
		// Constructor
		CPrimGroup(void);

		// Destructor
		~CPrimGroup(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		UINT OnMessage(UINT uMsg, UINT uParam);
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive
//

class CPrimWidget : public CPrimGroup, public IDataServer
{
	public:
		// Constructor
		CPrimWidget(void);

		// Destructor
		~CPrimWidget(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		void Enter(void);
		void Leave(void);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawExec(IGDI *pGDI, IRegion *pDirty);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Base Methods
		UINT Release(void);

		// Data Server Methods
		WORD  CheckID(WORD  ID);
		BOOL  IsAvail(DWORD ID);
		BOOL  SetScan(DWORD ID, UINT Code);
		DWORD GetData(DWORD ID, UINT Type, UINT Flags);
		BOOL  SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data);
		DWORD GetProp(DWORD ID, WORD Prop, UINT Type);
		DWORD RunFunc(WORD  ID, UINT uParam, PDWORD pParam);
		BOOL  GetName(DWORD ID, UINT m, PSTR pName, UINT uName);

		// Public Data
		CCodedItem          * m_pOnSelect;
		CCodedItem          * m_pOnRemove;
		CCodedItem          * m_pOnUpdate;
		CCodedItem          * m_pOnSecond;
		CPrimWidgetPropData * m_pData;

	protected:
		// Data Members
		IDataServer * m_pAbove;
		IDataServer * m_pServer;
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property Data
//

class CPrimWidgetPropData : public CItem
{
	public:
		// Constructor
		CPrimWidgetPropData(void);

		// Destructor
		~CPrimWidgetPropData(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Public Data
		CPrimWidgetPropList * m_pList;
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property List
//

class CPrimWidgetPropList : public CItem
{
	public:
		// Constructor
		CPrimWidgetPropList(void);

		// Destructor
		~CPrimWidgetPropList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Public Data
		UINT		   m_uCount;
		CPrimWidgetProp ** m_ppProp;
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property
//

class CPrimWidgetProp : public CCodedHost
{
	public:
		// Constructor
		CPrimWidgetProp(void);

		// Constructor
		~CPrimWidgetProp(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL  HasLocal(void) const;
		DWORD GetData(UINT Type) const;

		// Operations
		BOOL SetData(DWORD Data, UINT Type);

		// Item Properties
		UINT         m_Type;
		UINT         m_Flags;
		CCodedItem * m_pValue;
		DWORD	     m_Data;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimSimpleImage;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Single Image
//

class CPrimSimpleImage : public CPrimWithText
{
	public:
		// Constructor
		CPrimSimpleImage(void);

		// Destructor
		~CPrimSimpleImage(void);

		// Initialization
		void Load(PCBYTE &pData);
		
		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CCodedItem * m_pGray;
		CPrimImage * m_pImage;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			BOOL m_fColor;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Remote Display Request
//

class CRemoteRequest : public IBase
{
	public:
		// Constructor
		CRemoteRequest(void);

		// Destructor
		~CRemoteRequest(void);

		// IBase
		UINT Release(void);

		// Operations
		void Service(void);
		void Capture(PCBYTE pDispBuff, int nWidth);

	public:
		enum CRemoteState {
			
			stateIdle	= 0,
			stateCapture	= 1,
			stateDisplay	= 2,
			};

		// Request Data
		HTASK		  m_hTask;
		UINT		  m_uPort;
		UINT		  m_uHandle;
		UINT		  m_uDrop;
		int		  m_px;
		int		  m_py;
		int		  m_cx;
		int		  m_cy;
		BOOL		  m_fPortrait;
		ICommsDisplay   * m_pDriver;
		PBYTE		  m_pCopy;
		PBYTE		  m_pData;
		UINT		  m_uAlloc;

		UINT		  m_uState;
		
		// List Data
		CRemoteRequest * m_pNext;
		CRemoteRequest * m_pPrev;
	};

// End of File

#endif
