#include "melbase.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Constants
//
 
#define FRAME_TIMEOUT	500

//////////////////////////////////////////////////////////////////////////
//
// CPU Access Codes
//
//

enum {
	cpuLocal = 0x03FF,
	cpu1	 = 0x03E0,
	cpu2	 = 0x03E1,
	cpu3	 = 0x03E2,
	cpu4	 = 0x03E3,
	cpuCtrl  = 0x03D0,
	cpuStdBy = 0x03D1,
	cpuSysA  = 0x03D2,
	cpuSysB	 = 0x03D3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Melsec PLC Master Driver - Supports Melsec 1 and 4
//

class CMelsecMasterDriver : public CMelBaseMasterDriver
{
	public:
		// Constructor
		CMelsecMasterDriver(void);

		// Destructor
		~CMelsecMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
		
		// Device Data
		struct CContext : CMelBaseMasterDriver::CBaseCtx
		{
			BYTE m_bDrop;
			UINT m_Format;
			BYTE m_bPc;
			BYTE m_bCpu;
			BYTE m_bNet;
	       		};

		// Data Members
		CContext * m_pCtx;
		BOOL	   m_fCheck;
				
		// Implementation
		UINT	GetMask(UINT Table);
		BOOL	IsHex(UINT Table);
							
		// Data Link Layer
		BOOL	SendReq(void);
		void	Send(void);
		void	SendAck(void);
		void	SendNak(void);
		BOOL	RecvFrame(void);
		BOOL	Check(void);
		BOOL	CheckBinary(void);
		WORD	xval(char cText);

		// Overidables
		void	AddID(void);
		void	AddCommand(UINT uType, UINT uCommand, UINT uCount);
		void	AddAddress(AREF Addr);
		void	AddOffset(UINT uOffset);
		void	AddCount(UINT uCount);
	       	WORD	GetCommand(UINT uType, BOOL fWrite);
		BOOL	Transact(UINT uTotal);

		// Helpers
		BOOL	IsAscii(void);
		void	AddCpu(void);

	};

// End of File
