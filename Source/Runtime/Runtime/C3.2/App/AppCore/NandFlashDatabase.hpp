
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandFlashDatabase_HPP

#define	INCLUDE_NandFlashDatabase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include "CxNand.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

struct CItemInfo;

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Database
//

class CNandFlashDatabase : public CNandClient,
			   public IDatabase
{
	public:

		// Constructor
		CNandFlashDatabase(CNandBlock const &Start, CNandBlock const &End, UINT uPool);

		// Destructor
		~CNandFlashDatabase(void);

		// IDatabase
		void   Init(void);
		void   Clear(void);
		BOOL   IsValid(void);
		void   SetValid(BOOL fValid);
		void   SetRunning(BOOL fRun);
		BOOL   GarbageCollect(void);
		BOOL   GetVersion(PBYTE pGuid);
		DWORD  GetRevision(void);
		BOOL   SetVersion(PCBYTE pData);
		BOOL   SetRevision(DWORD dwRes);
		BOOL   CanCompress(void);
		BOOL   CheckSpace(UINT uSize);
		BOOL   WriteItem(CItemInfo const &Info, PCVOID pData);
		BOOL   GetItemInfo(UINT uItem, CItemInfo &Info);
		PCVOID LockItem(UINT uItem, CItemInfo &Info);
		PCVOID LockItem(UINT uItem);
		void   PendItem(UINT uItem, BOOL fPend);
		void   LockPendingItems(BOOL fLock);
		void   FreeItem(UINT uItem);

	protected:
		// Constants
		static UINT  const dataItemCount = 4096;
		static DWORD const magicProps	 = 0x58444E49;
		static DWORD const magicItem     = 0x4D455449;
		static DWORD const magicBody     = 0x59444F42;
		static DWORD const magicEmpty	 = 0xFFFFFFFF;
		static DWORD const magicFRAM     = 0x12190848;

		// Page Header
		struct CPageHeader
		{
			DWORD		Magic;
			DWORD		Generation;
			};

		// Property Page
		struct CPropsPage
		{
			CPageHeader	Page;
			GUID		Guid;
			DWORD		Revision;
			};

		// Item Data Page
		struct CItemDataPage
		{
			CPageHeader	Page;
			DWORD		Index;
			DWORD		Pad[1];
			};

		// Item Header Page
		struct CItemHeadPage
		{
			CItemDataPage	Header;
			DWORD		Class;
			DWORD		CRC;
			DWORD		Size;
			DWORD		Comp;
			};

		// Item State
		struct CState
		{
			CNandPage	m_Page;
			DWORD		m_Class;
			DWORD		m_CRC;
			DWORD		m_Size;
			DWORD		m_Comp;
			BOOL		m_fValid;
			UINT		m_uGeneration;
			UINT		m_uLock;
			PBYTE		m_pData;
			CState *	m_pNext;
			CState *	m_pPrev;
			};

		// Type Definitions
		typedef	CPageHeader	    * PPAGE;
		typedef	CPageHeader   const * PCPAGE;
		typedef	CPropsPage          * PPROPS;
		typedef	CPropsPage    const * PCPROPS;
		typedef	CItemDataPage       * PDATA;
		typedef	CItemDataPage const * PCDATA;
		typedef	CItemHeadPage       * PHEAD;
		typedef	CItemHeadPage const * PCHEAD;

		// Data
		IMutex	      * m_pMutex;
		UINT		m_uFreePages;
		UINT		m_uPoolSize;
		UINT		m_uPoolUsed;
		CNandPage	m_PageProps;
		UINT		m_uPropsGen;
		CState	      * m_pState;
		CState	      * m_pHead;
		CState	      * m_pTail;
		DWORD		m_Revision;
		GUID		m_Guid;
		
		// Item State Array
		bool MountItems(void);
		bool FindItems(void);
		void CheckPoolUsage(void);

		// Property Page Support
		bool CommitPropsPage(void);

		// Obsolete Page Support
		bool EraseObsoleteBlocks(void);
		bool IsBlockObsolete(CNandBlock Block);
		bool IsPageObsolete(CNandPage Page);
		bool IsPageEmpty(CNandPage Page);
		bool FindEmptyBlock(CNandBlock &Block);
		bool FindValidBlock(CNandBlock &Block);
		bool CopyBlock(CNandBlock Dest, CNandBlock From);

		// Overridables
		bool OnPageReloc(PCBYTE pData, CNandPage const &Dest);
	};

// End of File

#endif
