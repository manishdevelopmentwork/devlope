
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CDIO14MOD_HPP

#define INCLUDE_CDIO14MOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "dio01.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDIO14Module;
class CDIO14MainWnd;

//////////////////////////////////////////////////////////////////////////
//
// 8in 6out simple Digital Module
//

class CDIO14Module : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDIO14Module(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Data Members
		CDIO14ModCfg * m_pInput;
		
	protected:
		// Implementation
		void AddMetaData(void);

		// Download Support
		void MakeObjectData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8in 6out simple Digital Module Window
//

class CDIO14MainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDIO14MainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd *	m_pMult;
		CDIO14Module  * m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddDIO14Pages(void);
	};

// End of File

#endif
