
#include "intern.hpp"

#include "daro8module.hpp"

#include "DARO8mainwnd.hpp"

#include "daro8output.hpp"

#include "daro8outputconfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/daro8props.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DARO8 Module
//

// Dynamic Class

AfxImplementDynamicClass(CDARO8Module, CManticoreGenericModule);

// Constructor

CDARO8Module::CDARO8Module(void)
{
	m_pOutput       = New CDARO8Output;

	m_pOutputConfig = New CDARO8OutputConfig;

	m_Ident         = LOBYTE(ID_DARO8);

	m_FirmID        = FIRM_DARO8;

	m_Model         = "RO8";

	m_Power         = 36;
	}

// UI Management

CViewWnd * CDARO8Module::CreateMainView(void)
{
	return New CDARO8MainWnd;
	}

// Comms Object Access

UINT CDARO8Module::GetObjectCount(void)
{
	return 2;
	}

BOOL CDARO8Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_OUTPUT;
			
			Data.Name  = L"DO";
			
			Data.pItem = m_pOutput;

			return TRUE;

		case 1:
			Data.ID    = OBJ_CONFIG_DO;

			Data.Name  = L"DO Config";
			
			Data.pItem = m_pOutputConfig;

			return FALSE;
		}

	return FALSE;
	}

// Implementation

void CDARO8Module::AddMetaData(void)
{
	Meta_AddObject(Output);
	Meta_AddObject(OutputConfig);

	CManticoreGenericModule::AddMetaData();
	}

// End of File
