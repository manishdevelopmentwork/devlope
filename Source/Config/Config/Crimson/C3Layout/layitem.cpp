
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Layout Item
//

// Constructors

CLayItem::CLayItem(void)
{
	}

CLayItem::CLayItem(BOOL fEmpty)
{
	m_MinSize.cx = fEmpty ? 0 : 1;

	m_MinSize.cy = fEmpty ? 0 : 1;

	m_MaxSize.cx = m_MinSize.cx;

	m_MaxSize.cy = m_MinSize.cy;
	}

// Destructor

CLayItem::~CLayItem(void)
{
	}

// Attributes

CSize CLayItem::GetMinSize(void) const
{
	return m_MinSize;
	}

CSize CLayItem::GetMaxSize(void) const
{
	return m_MaxSize;
	}

CRect CLayItem::GetRect(void) const
{
	CRect Rect = m_Rect;

	Rect.left   += m_Offset.cx;

	Rect.top    += m_Offset.cy;
	
	Rect.right  += m_Offset.cx;

	Rect.bottom += m_Offset.cy;

	return Rect;
	}

// Operations

void CLayItem::SetRect(CRect const &Rect)
{
	m_Rect = Rect;

	OnSetRect();

	if( m_Rect.cx() < m_MinSize.cx ) {

		AfxThrowUserException();
		}

	if( m_Rect.cy() < m_MinSize.cy ) {

		AfxThrowUserException();
		}
	}

void CLayItem::Prepare(CDC &DC)
{
	OnPrepare(DC);

	MakeMax(m_MaxSize.cx, m_MinSize.cx);

	MakeMax(m_MaxSize.cy, m_MinSize.cy);
	}

void CLayItem::SetOffset(CSize const &Offset)
{
	m_Offset = Offset;

	OnSetOffset();
	}

// Overidables

void CLayItem::OnPrepare(CDC &DC)
{
	}

void CLayItem::OnSetRect(void)
{
	}

void CLayItem::OnSetOffset(void)
{
	}

// End of File
