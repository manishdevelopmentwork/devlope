
#include "Intern.hpp"

#include "CryptoHashMd5.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MD5 Cryptographic Hash
//

// Instantiator

static IUnknown * Create_CryptoHashMd5(PCTXT pName)
{
	return New CCryptoHashMd5;
	}

// Registration

global void Register_CryptoHashMd5(void)
{
	piob->RegisterInstantiator("crypto.hash-md5", Create_CryptoHashMd5);
	}

// Constructor

CCryptoHashMd5::CCryptoHashMd5(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;

	psMd5PreInit(&m_Ctx);
	}

// ICryptoHash

CString CCryptoHashMd5::GetName(void)
{
	return "md5";
	}

void CCryptoHashMd5::GetHashOid(CByteArray &oid)
{
	BYTE b[] = { 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x02, 0x05 };

	oid.Empty();

	oid.Append(b, sizeof(b));
	}

void CCryptoHashMd5::Initialize(void)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	psMd5Init(&m_Ctx);

	m_uState = stateActive;
	}

void CCryptoHashMd5::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	psMd5Update(&m_Ctx, pData, uData);
	}

void CCryptoHashMd5::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	psMd5Final(&m_Ctx, m_pHash);

	m_uState = stateDone;
	}

// End of File
