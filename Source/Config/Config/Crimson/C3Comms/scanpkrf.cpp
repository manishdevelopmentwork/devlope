
#include "intern.hpp"

#include "scanpkrf.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scanpak RFID TCP/IP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CScanpakRFIDTCPDriverOptions, CUIItem);

// Constructor

CScanpakRFIDTCPDriverOptions::CScanpakRFIDTCPDriverOptions(void)
{
	m_Count    = 2;
	}

// UI Managament

void CScanpakRFIDTCPDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CScanpakRFIDTCPDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Count));

	return TRUE;
	}

// Meta Data Creation

void CScanpakRFIDTCPDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Count);
	}

//////////////////////////////////////////////////////////////////////////
//
// Scanpak RFID TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CScanpakRFIDTCPDeviceOptions, CUIItem);       

// Constructor

CScanpakRFIDTCPDeviceOptions::CScanpakRFIDTCPDeviceOptions(void)
{
	m_ReaderIP = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;
	}

// UI Managament

void CScanpakRFIDTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CScanpakRFIDTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_ReaderIP));

	return TRUE;
	}

// Meta Data Creation

void CScanpakRFIDTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(ReaderIP);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Scanpak RFID TCP/IP Driver
//

// Instantiator

ICommsDriver *	Create_ScanpakRFIDTCPDriver(void)
{
	return New CScanpakRFIDTCPDriver;
	}

// Constructor

CScanpakRFIDTCPDriver::CScanpakRFIDTCPDriver(void)
{
	m_wID		= 0x3514;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Scanpak";
	
	m_DriverName	= "AR1000-IP";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Scanpak AR1000-IP";

	AddSpaces();
	}

// Binding Control

UINT CScanpakRFIDTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CScanpakRFIDTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CScanpakRFIDTCPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CScanpakRFIDTCPDriverOptions);
	}

CLASS CScanpakRFIDTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CScanpakRFIDTCPDeviceOptions);
	}

// Implementation

void CScanpakRFIDTCPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "ID",	"User ID Numbers",	10, 0, 1023,	addrLongAsLong));
	
	AddSpace(New CSpace(2, "Time",	"Login Data and Time",	10, 0, 1023,	addrLongAsLong));
					  
	AddSpace(New CSpace(3, "Signal","Signal Strength",	10, 0, 1023,	addrLongAsLong));
	
	AddSpace(New CSpace(4, "Count",	"Logged In Count",	10, 0, 0,	addrWordAsWord));
	
	AddSpace(New CSpace(5, "Reader","Reader ID",		10, 0, 0,	addrLongAsLong));

	AddSpace(New CSpace(6, "Clean",	"Clean All Tag Data",	10, 0, 0,	addrBitAsBit));

	AddSpace(New CSpace(7, "Change","Replace This Device",	10, 0, 0,	addrBitAsBit));
	}

// End of File
