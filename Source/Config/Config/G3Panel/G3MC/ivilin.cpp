
#include "intern.hpp"

#include "legacy.h"

#include "iviinp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Linearizer Graph
//

// Runtime Class

AfxImplementRuntimeClass(CUILinCurve, CUIControl);

// Constructor

CUILinCurve::CUILinCurve(CIVIInput *pInput)
{
	m_pLayout   = NULL;

	m_pCurveWnd = New CIVICurveWnd(pInput);
	}

// Destructor

CUILinCurve::~CUILinCurve(void)
{
	}

// Operations

void CUILinCurve::Update(void)
{
	m_pCurveWnd->Update();
	}

void CUILinCurve::Exclude(CWnd &Wnd, CDC &DC)
{
	m_pCurveWnd->Exclude(Wnd, DC);
	}

// Core Overidable

void CUILinCurve::OnLayout(CLayFormation *pForm)
{
	m_pLayout = New CLayFormPad( CRect(4, 4, 4, 4), horzNone | vertNone );

	pForm->AddItem(m_pLayout);
	}

void CUILinCurve::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pCurveWnd->Create( L"",
			     WS_CHILD,
			     m_pLayout->GetChildRect(),
			     Wnd,
			     uID++,
			     NULL
			     );

	m_pCurveWnd->SetFont(afxFont(Dialog));

	AddControl((CCtrlWnd *) m_pCurveWnd);
	}

void CUILinCurve::OnMove(void)
{
	m_pCurveWnd->MoveWindow(m_pLayout->GetChildRect(), TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Linearizer Graph Window
//

// Runtime Class

AfxImplementRuntimeClass(CIVICurveWnd, CWnd);

// Constructor

CIVICurveWnd::CIVICurveWnd(CIVIInput *pInput)
{
	m_pInput = pInput;
	}

// Destructor

CIVICurveWnd::~CIVICurveWnd(void)
{
	}

// Operations

void CIVICurveWnd::Update(void)
{
	Invalidate(TRUE);
	}

void CIVICurveWnd::Exclude(CWnd &Wnd, CDC &DC)
{
	if( IsWindowVisible() ) {

		CRect Rect = GetClientRect();

		ClientToScreen(Rect);

		Wnd.ScreenToClient(Rect);

		DC.ExcludeClipRect(Rect);
		}
	}

// Message Map

AfxMessageMap(CIVICurveWnd, CWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)

	AfxDispatchMessage(WM_PAINT)

	AfxDispatchMessage(WM_LBUTTONUP)

	AfxMessageEnd(CIVICurveWnd)
	};

// Message Handlers

BOOL CIVICurveWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

void CIVICurveWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);

	Rect -= 2;

	CSize Size = Rect.GetSize();

	CRect Work = CRect(Size);

	CBitmap   Bitmap(DC, Size);

	CMemoryDC WorkDC(DC);

	WorkDC.Select(Bitmap);

	WorkDC.FillRect(Work, afxBrush(BLACK));

	Draw(WorkDC, Work);

	DC.BitBlt(Rect, WorkDC, CPoint(), SRCCOPY);

	WorkDC.Deselect();
	}

void CIVICurveWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( m_pInput->GetLinEnable() ) {

		CIVILinData Dlg(m_pInput);

		Dlg.Execute(ThisObject);

		Update();
		}
	}

// Implementation

void CIVICurveWnd::Draw(CDC &DC, CRect Rect)
{
	BOOL fLinEnable  = m_pInput->GetLinEnable();

	UINT Channel     = m_pInput->m_LinChannel - 1;

	UINT TopSegPoint = m_pInput->m_LinSegments[Channel];

	DC.Save();

	DC.SetMapMode(MM_ANISOTROPIC);

	DC.SetWindowExt(CSize(IVI_XMAX - IVI_XMIN, IVI_YMIN - IVI_YMAX));

	DC.SetWindowOrg(CPoint(IVI_XMIN, IVI_YMAX));

	DC.SetViewportExt(Rect.GetSize() - CSize(1, 1));

	DC.SetViewportOrg(Rect.GetTopLeft());

	CPen PenGray(CColor(0x80, 0x80, 0x80));

	CPen PenWhite(CColor(0xFF, 0xFF, 0xFF));
	
	CPen PenGreen(CColor(0x00, 0xFF, 0x00));
	
	CPen PenRed(CColor(0xFF, 0x00, 0x00));
	
	CPen PenTeal(CColor(0x00, 0xFF, 0xFF));

	for( INT x = IVI_XGRIDMIN; x <= IVI_XGRIDMAX; x += 50 ) {

		if( !((x - IVI_XGRIDMIN) % 100) )

			DC.Select(PenWhite);

		else
			DC.Select(PenGray);

		DC.MoveTo(CPoint(x, IVI_YGRIDMIN));

		DC.LineTo(CPoint(x, IVI_YGRIDMAX));

		DC.Deselect();
		}

	for( INT y = IVI_YGRIDMIN; y <= IVI_YGRIDMAX; y += 50 ) {

		if( !((y - IVI_YGRIDMIN) % 100) )

			DC.Select(PenWhite);

		else
			DC.Select(PenGray);

		DC.MoveTo(CPoint(IVI_XGRIDMIN, y));

		DC.LineTo(CPoint(IVI_XGRIDMAX, y));

		DC.Deselect();
		}

	///////////////////////////////

	CFont Font(TEXT("Tahoma"), CSize(15, 25), FALSE);

	DC.Select(Font);

	DC.SetBkMode(TRANSPARENT);

	DC.SetTextColor(afxColor(WHITE));
	
	DC.DrawText(L"PV",
		    CRect( IVI_XMIN,
		           ((IVI_YGRIDMAX - IVI_YGRIDMIN)/2) + IVI_YGRIDMIN + (IVI_YTEXT/2),
			   IVI_XGRIDMIN,
			   ((IVI_YGRIDMAX - IVI_YGRIDMIN)/2) + IVI_YGRIDMIN - (IVI_YTEXT/2)),
		    DT_LEFT);

	CString xLabel = CString(IDS_LINEAR_VOLTS);

	if( m_pInput->m_IVIModel == ID_CSINI8L )

		xLabel = CString(IDS_LINEAR_MA);

	DC.DrawText(xLabel,
		    CRect( ((IVI_XGRIDMAX - IVI_XGRIDMIN)/2) + IVI_XGRIDMIN - (IVI_XTEXT/2),
		           IVI_YMIN + IVI_YTEXT,
			   ((IVI_XGRIDMAX - IVI_XGRIDMIN)/2) + IVI_XGRIDMIN + (IVI_XTEXT/2),
			   IVI_YMIN),
		    DT_CENTER);
	
	if( fLinEnable ) {

		DC.DrawText(Format(m_pInput->GetScaledPV(Channel, TopSegPoint), m_pInput->GetLinDP()),
			    CRect( IVI_XMIN,
				   IVI_YGRIDMAX + (IVI_YTEXT/2),
				   IVI_XGRIDMIN - (IVI_YTEXT/2),
				   IVI_YGRIDMAX - IVI_YTEXT),
			    DT_RIGHT);

		DC.DrawText(Format(m_pInput->GetScaledPV(Channel, 0), m_pInput->GetLinDP()),
			    CRect( IVI_XMIN,
				   IVI_YGRIDMIN + (IVI_YTEXT/2),
				   IVI_XGRIDMIN - (IVI_YTEXT/2),
				   IVI_YGRIDMIN - (IVI_YTEXT/2)),
			    DT_RIGHT);

		DC.DrawText(Format(m_pInput->m_LinInput[Channel][0], 3),
			    CRect( IVI_XGRIDMIN - (IVI_XTEXT/2),
				   IVI_YGRIDMIN - IVI_YTEXT,
				   IVI_XGRIDMIN + (IVI_XTEXT/2),
				   IVI_YMIN),
			    DT_CENTER);

		DC.DrawText(Format(m_pInput->m_LinInput[Channel][TopSegPoint], 3),
			    CRect( IVI_XGRIDMAX - (IVI_XTEXT/2),
				   IVI_YGRIDMIN - IVI_YTEXT,
				   IVI_XGRIDMAX + (IVI_XTEXT/2),
				   IVI_YMIN),
			    DT_CENTER);
		}

	DC.Deselect();

	///////////////////////////////

	if( fLinEnable ) {

		DC.Select(PenGreen);

		DC.MoveTo( GetPointLocation(0) );

		UINT i;

		for( i = 1; i <= TopSegPoint; i++ ) {

			DC.LineTo( GetPointLocation(i) );
			}

		DC.Deselect();

		DC.Select(PenRed);

		for( i = 0; i <= TopSegPoint; i++ ) {

			DC.Rectangle( DrawPoint(i) );
			}

		DC.Deselect();
		}

	DC.Restore();
	}

CPoint CIVICurveWnd::GetPointLocation(UINT SegPoint)
{
	UINT Channel  = m_pInput->m_LinChannel - 1;

	LONG Input    = LONG(m_pInput->m_LinInput[Channel][SegPoint]);

	LONG InputMin = LONG(m_pInput->m_LinInput[Channel][0]);

	LONG InputMax = LONG(m_pInput->m_LinInput[Channel][m_pInput->m_LinSegments[Channel]]);

	LONG PV       = LONG(m_pInput->GetScaledPV(Channel, SegPoint));

	LONG PVMin    = LONG(m_pInput->GetScaledPV(Channel, 0));

	LONG PVMax    = LONG(m_pInput->GetScaledPV(Channel, m_pInput->m_LinSegments[Channel]));

	if( !(InputMax - InputMin) || !(PVMax - PVMin) )

		return CPoint(IVI_XGRIDMIN, IVI_YGRIDMIN);

	LONG x = ((Input - InputMin) * (IVI_XGRIDMAX - IVI_XGRIDMIN)) / (InputMax - InputMin);

	x += IVI_XGRIDMIN;

	LONG y = ((PV - PVMin) * (IVI_YGRIDMAX - IVI_YGRIDMIN)) / (PVMax - PVMin);

	y += IVI_YGRIDMIN;

	return CPoint((INT)x, (INT)y);
	}

CRect CIVICurveWnd::DrawPoint(UINT SegPoint)
{
	CPoint Location = GetPointLocation(SegPoint);

	UINT Radius = 5;

	return CRect( Location.x - Radius,
		      Location.y - Radius,
		      Location.x + Radius,
		      Location.y + Radius );
	}

CString CIVICurveWnd::Format(INT nData, UINT DP)
{
	double  f = double(nData);

	double  p = pow(double(10), int(DP));

	return CPrintf("%.*f", DP, f / p);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Linearizer Data Entry Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CIVILinData, CStdDialog);

// Constructor

CIVILinData::CIVILinData(CIVIInput *pInput)
{		
	m_pInput   = pInput;

	m_Channel  = m_pInput->m_LinChannel - 1;

	m_Segments = m_pInput->m_LinSegments[m_Channel];

	m_DP       = m_pInput->GetLinDP();

	for( INT i = 0; i < LIN_POINTS; i++ ) {

		m_Input[i] = m_pInput->m_LinInput[m_Channel][i];

		m_PV[i]    = m_pInput->m_LinPV[m_Channel][i];
		}

	FormatLinData();

	SetName(L"IVILinData");
	}

// Message Map

AfxMessageMap(CIVILinData, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchMessage(WM_PAINT)

	AfxDispatchCommand(IDOK, OnCommandOK)

	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxDispatchNotify(101, EN_KILLFOCUS, OnKillFocus)

	AfxMessageEnd(CIVILinData)
	};

// Message Handlers

BOOL CIVILinData::OnInitDialog(CWnd &Focus, DWORD dwData)
{			
	SetDlgFocus(Focus);

	OnPaint();

	CEditCtrl &Edit2 = (CEditCtrl &) GetDlgItem(101);

	Edit2.SetWindowText(m_LinData);

	Edit2.SetSel(CRange(TRUE));

	return TRUE;
	}

void CIVILinData::OnKillFocus(UINT uID, CWnd &Wnd)
{
	if( uID == 101 ){

		m_LinData = GetDlgItem(101).GetWindowText();

		ParseLinData();

		FormatLinData();

		CEditCtrl &Edit2 = (CEditCtrl &) GetDlgItem(101);

		Edit2.SetWindowText(m_LinData);
		}
	}

// Command Handlers

BOOL CIVILinData::OnCommandOK(UINT uID)
{
	BOOL fDataGood = TRUE;

	if( m_Input[m_Segments] <= m_Input[0] )

		fDataGood = FALSE;

	if( m_PV[m_Segments] == m_PV[0] )

		fDataGood = FALSE;

	if( m_Segments < 1 || m_Segments > 99 )

		fDataGood = FALSE;

	for( INT i = 0; i < LIN_POINTS; i++ ) {

		if( m_PV[i] < -30000 || m_PV[i] > 30000 )

			fDataGood = FALSE;
		}

	if( fDataGood ) {

		m_pInput->m_LinSegments[m_Channel] = m_Segments;

		for( INT i = 0; i < LIN_POINTS; i++ ) {

			m_pInput->m_LinInput[m_Channel][i] = m_Input[i];

			m_pInput->m_LinPV[m_Channel][i]    = m_PV[i];
			}

		m_pInput->CCommsItem::SetDirty();

		EndDialog(TRUE);
		}

	return TRUE;
	}

BOOL CIVILinData::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

// Implementation

void CIVILinData::OnPaint(void)
{
	PAINTSTRUCT Paint;

	HWND hWnd = GetActiveWindow();

	BeginPaint(hWnd, &Paint);

	SetWindowText(CPrintf(CString(IDS_LINEAR_DLG_TITLE), CPrintf("%u", m_Channel+1)));

	CString PVMin = Format(-30000, m_DP, FALSE);

	CString PVMax = Format(30000, m_DP, FALSE);

	CString InputMin = L"-0.300";
		
	CString InputMax = L"10.300V";

	if( m_pInput->m_IVIModel == ID_CSINV8L && m_pInput->m_PVRange == 2 ) {
		
		InputMin = L"-10.300";
		}

	if( m_pInput->m_IVIModel == ID_CSINI8L ) {

		if( m_pInput->m_PIRange == 1 )

			InputMin = L"-2.000";

		else
			InputMin = L"2.000";

		InputMax = L"22.000mA";
		}

	GetDlgItem(205).SetWindowText(CString(IDS_MODULE_INP) + L"\t   " + CString(IDS_NAME_PV));

	GetDlgItem(206).SetWindowText(CString(IDS_LINEAR_MAX_PTS));

	GetDlgItem(207).SetWindowText(CString(IDS_LINEAR_SEPARATED));

	GetDlgItem(208).SetWindowText(CPrintf(CString(IDS_LINEAR_INPUT_RANGE), InputMin, InputMax));

	GetDlgItem(209).SetWindowText(CPrintf(CString(IDS_LINEAR_PV_RANGE), PVMin, PVMax));
	
	EndPaint(hWnd, &Paint);
	}

CString CIVILinData::Format(INT nData, UINT DP, BOOL PlusSign)
{
	double  f = double(nData);

	double  p = pow(double(10), double(DP));

	CPrintf t = CPrintf("%.*f", DP, f / p);

	return (PlusSign && nData >= 0) ? L' ' + t : t;
	}

void CIVILinData::FormatLinData(void)
{
	m_LinData = "";

	for( UINT i = 0; i <= m_Segments; i++ ) {

		m_LinData += (Format(m_Input[i], 3, TRUE) + L"\t");

		m_LinData += (Format(m_PV[i], m_DP, TRUE) + L"\r\n");
		}
	}

INT CIVILinData::Parse(PCTXT pText, UINT DP)
{
	double f = watof(pText);

	double p = pow(double(10), int(DP));

	return (f > 0 ) ? INT(f * p + 0.5) : INT(f * p - 0.5);
	}

void CIVILinData::ParseLinData(void)
{
	if( m_LinData == "" )

		return;

	UINT SegPoint  = 0;

	CString pValue = L"";

	INT len = wstrlen(m_LinData);

	for( INT i = 0; i < len; i++ ){

		if( SegPoint > 99 )

			break;

		switch( m_LinData[i] ) {

			case ',':
			case '\t':

				if( pValue != "" ){

					m_Input[SegPoint] = CheckInputRange(Parse(pValue, 3));

					pValue = "";
					}

				break;

			case '\r':

				if( pValue != "" ){

					m_PV[SegPoint++] = CheckPVRange(Parse(pValue, m_DP));

					pValue = "";
					}

				i++;

				break;

			default:

				pValue += m_LinData[i];

				break;
			}
		}

	if( pValue != "" )

		m_PV[SegPoint++] = CheckPVRange(Parse(pValue, m_DP));

	if( SegPoint >= 2 && SegPoint <= 100 ) {

		m_Segments = SegPoint - 1;

		for( SegPoint; SegPoint < LIN_POINTS; SegPoint++ ) {

			m_Input[SegPoint] = 0;

			m_PV[SegPoint]    = 0;
			}
		}

	else {
		m_Segments = m_pInput->m_LinSegments[m_Channel];

		for( INT i = 0; i < LIN_POINTS; i++ ) {

			m_Input[i] = m_pInput->m_LinInput[m_Channel][i];

			m_PV[i]    = m_pInput->m_LinPV[m_Channel][i];
			}
		}
	}

INT CIVILinData::CheckInputRange(INT nData)
{
	INT nMin =  -300;

	INT nMax = 10300;

	if( m_pInput->m_IVIModel == ID_CSINV8L && m_pInput->m_PVRange == 2 )

		nMin = -10300;

	if( m_pInput->m_IVIModel == ID_CSINI8L ) {

		if( m_pInput->m_PIRange == 1 )

			nMin = -2000;
		else
			nMin = 2000;

		nMax = 22000;
		}

	if( nData < nMin )

		nData = nMin;

	if( nData > nMax )

		nData = nMax;

	return nData;
	}

INT CIVILinData::CheckPVRange(INT nData)
{
	if( nData < -30000 )

		nData = -30000;

	if( nData > 30000 )

		nData = 30000;

	return nData;
	}

// End of File
