
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Grid
//

// Constructor

CLayFormGrid::CLayFormGrid(UINT uCols)
{
	m_uCols = uCols;
	}

// Overridables

void CLayFormGrid::OnPrepare(CDC &DC)
{
	ClearData();

	FindRows();

	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CLayItem *pItem = m_List[n];

		pItem->Prepare(DC);
		}

	for( UINT r = 0; r < m_uRows; r++ ) {

		int nMin = GetRowMinSize(r);

		int nMax = GetRowMaxSize(r);

		m_MinSize.cy += nMin;

		m_MaxSize.cy += nMax;
		}

	for( UINT c = 0; c < m_uCols; c++ ) {

		int nMin = GetColMinSize(c);

		int nMax = GetColMaxSize(c);

		m_MinSize.cx += nMin;

		m_MaxSize.cx += nMax;
		}
	}

void CLayFormGrid::OnSetRect(void)
{
	int nRowSpace = m_Rect.GetHeight();

	int nColSpace = m_Rect.GetWidth();

	RowSetMins(nRowSpace);

	ColSetMins(nColSpace);
	
	while( RowDealOut(nRowSpace) );
	
	while( ColDealOut(nColSpace) );

	CPoint Pos;

	Pos.x = m_Rect.left;

	for( UINT c = 0; c < m_uCols; c++ ) {

		Pos.y = m_Rect.top;

		for( UINT r = 0; r < m_uRows; r++ ) {

			CLayItem *pItem = GetItem(r, c);

			CSize Size(m_ColSize[c], m_RowSize[r]);

			for( UINT p = c + 1; p < m_uCols; p++ ) {

				if( GetItem(r, p)->GetMinSize().cx == 0 ) {

					Size.cx += m_ColSize[p];
					}
				else
					break;
				}

			pItem->SetRect(CRect(Pos, Size));

			Pos.y += m_RowSize[r];
			}

		Pos.x += m_ColSize[c];
		}

	m_MinSize.cy = Pos.y - m_Rect.top;
	}

// Item Location

CLayItem * CLayFormGrid::GetItem(UINT uRow, UINT uCol)
{
	CLayItem *pItem = m_List[uCol + uRow * m_uCols];

	AfxAssert(pItem);

	return pItem;
	}

// Size Helpers

int CLayFormGrid::GetColMinSize(UINT uCol)
{
	int nMin = 0;

	for( UINT r = 0; r < m_uRows; r++ ) {

		CLayItem *pItem = GetItem(r, uCol);

		int nSize = pItem->GetMinSize().cx;

		for( UINT p = uCol + 1; p < m_uCols; p++ ) {

			if( GetItem(r, p)->GetMinSize().cx == 0 ) {

				nSize -= GetColMinSize(p);
				}
			else
				break;
			}

		MakeMax(nMin, nSize);
		}

	return nMin;
	}

int CLayFormGrid::GetColMaxSize(UINT uCol)
{
	int nMax = 0;

	for( UINT r = 0; r < m_uRows; r++ ) {

		CLayItem *pItem = GetItem(r, uCol);

		int nSize = pItem->GetMaxSize().cx;

		MakeMax(nMax, nSize);
		}

	return nMax;
	}

int CLayFormGrid::GetRowMinSize(UINT uRow)
{
	int nMin = 0;

	for( UINT c = 0; c < m_uCols; c++ ) {

		CLayItem *pItem = GetItem(uRow, c);

		int nSize = pItem->GetMinSize().cy;

		MakeMax(nMin, nSize);
		}

	return nMin;
	}

int CLayFormGrid::GetRowMaxSize(UINT uRow)
{
	int nMax = 0;

	for( UINT c = 0; c < m_uCols; c++ ) {

		CLayItem *pItem = GetItem(uRow, c);

		int nSize = pItem->GetMaxSize().cy;

		MakeMax(nMax, nSize);
		}

	return nMax;
	}

// Implementation

void CLayFormGrid::ClearData(void)
{
	m_MinSize = CSize();

	m_MaxSize = CSize();

	m_RowSize.Empty();

	m_ColSize.Empty();
	}

void CLayFormGrid::FindRows(void)
{
	UINT uCount = m_List.GetCount();

	m_uRows = uCount / m_uCols;

	AfxAssert(m_uRows * m_uCols == uCount);
	}

void CLayFormGrid::ColSetMins(int &nSpace)
{
	for( UINT c = 0; c < m_uCols; c++ ) {

		int nMin = GetColMinSize(c);

		m_ColSize.Append(nMin);

		nSpace -= nMin;
		}
	}

BOOL CLayFormGrid::ColDealOut(int &nSpace)
{
	int nEdit = 0;

	if( nSpace > 0 ) {

		int nItem = 0;

		int nEach = 0;

		for( UINT p = 0; p < 2; p++ ) {

			for( UINT c = 0; c < m_uCols; c++ ) {

				int nSize = m_ColSize[c];

				int nMax  = GetColMaxSize(c);

				if( nSize < nMax ) {

					if( p == 0 ) {

						nItem++;
						}
					else {
						int nDeal = Min(nEach, nMax - nSize);

						nSize  += nDeal;

						nSpace -= nDeal;

						m_ColSize.SetAt(c, nSize);

						nEdit++;
						}
					}
				}

			if( p == 0 ) {

				if( nItem && (nEach = nSpace / nItem) ) {

					continue;
					}

				break;
				}
			}
		}

	return nEdit ? TRUE : FALSE;
	}

void CLayFormGrid::RowSetMins(int &nSpace)
{
	for( UINT r = 0; r < m_uRows; r++ ) {

		int nMin = GetRowMinSize(r);

		m_RowSize.Append(nMin);

		nSpace -= nMin;
		}
	}

BOOL CLayFormGrid::RowDealOut(int &nSpace)
{
	int nEdit = 0;

	if( nSpace > 0 ) {

		int nItem = 0;

		int nEach = 0;

		for( UINT p = 0; p < 2; p++ ) {

			for( UINT r = 0; r < m_uRows; r++ ) {

				int nSize = m_RowSize[r];

				int nMax  = GetRowMaxSize(r);

				if( nSize < nMax ) {

					if( p == 0 ) {

						nItem++;
						}
					else {
						int nDeal = Min(nEach, nMax - nSize);

						nSize  += nDeal;

						nSpace -= nDeal;

						m_RowSize.SetAt(r, nSize);

						nEdit++;
						}
					}
				}

			if( p == 0 ) {

				if( nItem && (nEach = nSpace / nItem) ) {

					continue;
					}

				break;
				}
			}
		}

	return nEdit ? TRUE : FALSE;
	}

// End of File
