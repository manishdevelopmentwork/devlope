
#include "intern.hpp"

#include "proxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Camera Proxy Object
//

class CProxyCamera : public CProxy, public IProxyCamera
{
	public:
		// Constructor
		CProxyCamera(void);

		// Destructor
		~CProxyCamera(void);

		// Binding
		BOOL Bind(CCommsPort *pPort, IDriver *pDriver);

		// IProxyCamera
		BOOL CheckPort(UINT uPort);
		
		// Primitive Calls
		void   ClaimData(void);
		void   FreeData (void);
		PCBYTE GetData  (UINT uDevice);
		UINT   GetInfo(UINT uDevice, UINT uIndex);

		// Inspection Transfer
		BOOL SLOW SaveSetup(UINT uDevice, UINT uIndex, PCTXT pName);
		BOOL SLOW LoadSetup(UINT uDevice, UINT uIndex, PCTXT pName);
		BOOL SLOW UseSetup (UINT uDevice, UINT uIndex);

		// Entry Points
		void Init(UINT uID);
		void Task(UINT uID);
		void Term(UINT uID);

		// Suspension
		void Suspend(void);
		void Resume(void);

	protected:
		// Data Members
		BOOL               m_fOpen;
		ICommsCamera     * m_pCamera;
		CCommsManager    * m_pManager;
		CCommsDeviceList * m_pDevices;
		UINT               m_uDevices;
		CCommsDevice     * m_pCurrent;
		CCommsDevice     * m_pDevice;
		UINT		   m_uLast;
		UINT		   m_uScan;
		UINT		   m_uNext;
		IMutex		 * m_pMutex;

		// Service Routine
		void ServiceDriver(void);

		// Device Handling
		void ServiceDevice(void);
		BOOL IsDeviceOnline(void);
		BOOL SendDevicePing(BOOL fDef);
		BOOL SwitchDevice(void);
		BOOL CheckDevice(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Camera Proxy Object
//

// Instantiator

global CProxy * Create_ProxyCamera(void)
{
	return New CProxyCamera;
	}

// Constructor

CProxyCamera::CProxyCamera(void)
{
	m_fOpen   = FALSE;

	m_pCamera = NULL;

	m_pMutex  = Create_Mutex();

	CCommsSystem::m_pThis->m_pComms->m_pCameras->Append(this);
	}

// Destructor

CProxyCamera::~CProxyCamera(void)
{
	AfxRelease(m_pCamera);

	m_pMutex->Release();
	}

// Binding

BOOL CProxyCamera::Bind(CCommsPort *pPort, IDriver *pDriver)
{
	if( CProxy::Bind(pPort, pDriver) ) {

		m_pDriver->QueryInterface(AfxAeonIID(ICommsCamera), (void **) &m_pCamera);

		if( m_pCamera ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// IProxyCamera

BOOL CProxyCamera::CheckPort(UINT uPort)
{
	return uPort == m_pPort->m_Number;
	}

// Primitive Calls

void CProxyCamera::ClaimData(void)
{
	GuardTask(TRUE);

	m_pMutex->Wait(FOREVER);
	}

void CProxyCamera::FreeData(void)
{
	m_pMutex->Free();

	GuardTask(FALSE);
	}

PCBYTE CProxyCamera::GetData(UINT uDevice)
{
	UINT   uBits = 32;

	PCBYTE pData = m_pCamera->GetData(uDevice);

	if( pData ) {

		if( uBits == 8 ) {

			// TODO -- convert to  8 bit display (not needed)
			
			return pData;
			}

		if( uBits == 15 || uBits == 16 ) {

			BITMAP_RLC *pSource = (BITMAP_RLC *) pData;

			int              cx = pSource->Width;

			int              cy = pSource->Height;

			UINT          uSize = cx * cy;

			UINT         uAlloc = sizeof(BITMAP_RLC) + uSize * sizeof(WORD);
				
			PBYTE         pWork = New BYTE [uAlloc];

			BITMAP_RLC *pTarget = (BITMAP_RLC *) pWork;

			pTarget->Frame      = pSource->Frame;

			pTarget->Format     = pSource->Format;

			pTarget->Width      = pSource->Width;

			pTarget->Height     = pSource->Height;

			pTarget->Stride     = cx * sizeof(WORD);

			PBYTE          pSrc = pSource->Data;

			PWORD         pDest = PWORD(pTarget->Data);

			if( pSource->Format == image8bitGreyScale ) {

				while( uSize-- ) {

					BYTE grey = *pSrc++ >> 3;

					WORD  rgb = (uBits == 16 ? (grey | (grey<<6) | (grey<<11)) : (grey | (grey<<5) | (grey<<10)));

					*pDest++  = HostToIntel(rgb);
					}

				return pWork;
				}

			if( pSource->Format == image24bitColor ) {

				while( uSize-- ) {

					BYTE    r = (*pSrc++ >> 3);
								
					BYTE    g = (*pSrc++ >> 3);
								
					BYTE    b = (*pSrc++ >> 3);

					WORD  rgb = (uBits == 16 ? (r | (g<<6) | (b<<11)) : (r | (g<<5) | (b<<10)));

					*pDest++  = HostToIntel(rgb);
					}

				return pWork;
				}

			delete pWork;
			
			return NULL;
			}

		if( uBits == 32 ) {

			BITMAP_RLC *pSource = (BITMAP_RLC *) pData;

			int              cx = pSource->Width;

			int              cy = pSource->Height;

			UINT          uSize = cx * cy;

			UINT         uAlloc = sizeof(BITMAP_RLC) + uSize * sizeof(DWORD);
				
			PBYTE         pWork = New BYTE [uAlloc];

			BITMAP_RLC *pTarget = (BITMAP_RLC *) pWork;

			pTarget->Frame      = pSource->Frame;

			pTarget->Format     = pSource->Format;

			pTarget->Width      = pSource->Width;

			pTarget->Height     = pSource->Height;

			pTarget->Stride     = cx * sizeof(DWORD);

			PBYTE          pSrc = pSource->Data;

			PDWORD         pDest = PDWORD(pTarget->Data);

			if( pSource->Format == image8bitGreyScale ) {

				while( uSize-- ) {

					BYTE grey = *pSrc++;

					*pDest++  = (grey | grey<<8 | grey<<16);
					}

				return pWork;
				}

			if( pSource->Format == image24bitColor ) {

				while( uSize-- ) {

					BYTE  r  = *pSrc++;
					BYTE  g  = *pSrc++;
					BYTE  b  = *pSrc++;
					
					*pDest++ = (r | g<<8 | b<<16);
					}

				return pWork;
				}

			delete pWork;
			
			return NULL;
			}
		}

	return NULL;
	}

UINT CProxyCamera::GetInfo(UINT uDevice, UINT uIndex)
{
	return m_pCamera->GetInfo(uDevice, uIndex);
	}

// Inspection Transfer

BOOL CProxyCamera::SaveSetup(UINT uDevice, UINT uIndex, PCTXT pName)
{
	for( UINT n = 0; n < m_uDevices; n ++ ) {
		
		CCommsDevice *pDev = m_pDevices->m_ppDevice[n];

		if( pDev && pDev->m_Number == uDevice ) {

			CAutoFile File(pName, "r+", "w+");

			if( File ) {

				File.Truncate();

				if( m_pCamera->SaveSetup(pDev->GetContext(), uIndex, File) ) {

					return TRUE;
					}

				File.Close();
				}

			unlink(pName);
			}
		}

	return FALSE;
	}

BOOL CProxyCamera::LoadSetup(UINT uDevice, UINT uIndex, PCTXT pName)
{
	for( UINT n = 0; n < m_uDevices; n ++ ) {
		
		CCommsDevice *pDev = m_pDevices->m_ppDevice[n];

		if( pDev && pDev->m_Number == uDevice ) {

			CAutoFile File(pName, "r");

			if( File ) {
			
				if( m_pCamera->LoadSetup(pDev->GetContext(), uIndex, File) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CProxyCamera::UseSetup(UINT uDevice, UINT uIndex)
{
	for( UINT n = 0; n < m_uDevices; n ++ ) {
		
		CCommsDevice *pDev = m_pDevices->m_ppDevice[n];

		if( pDev && pDev->m_Number == uDevice ) {
			
			return m_pCamera->UseSetup(pDev->GetContext(), uIndex);
			}
		}

	return FALSE;
	}

// Entry Points

void CProxyCamera::Init(UINT uID)
{
	m_pDevices = m_pPort->m_pDevices;

	m_uDevices = m_pDevices->m_uCount;

	m_pCurrent = NULL;

	m_pDevice  = NULL;

	m_uLast    = 0;

	m_uScan	   = 0;

	m_uNext	   = 0;
	}

void CProxyCamera::Task(UINT uID)
{
	if( m_pPort->Open(m_pComms) ) {

		m_fOpen = TRUE;

		for(;;) {

			ServiceDriver();

			ForceSleep(50);
			}
		}

	Sleep(FOREVER);
	}

void CProxyCamera::Term(UINT uID)
{
	if( m_fOpen ) {

		m_pPort->Term();
		}
	}

// Suspension

void CProxyCamera::Suspend(void)
{
	for( UINT uDevice = 0; uDevice < m_uDevices; uDevice++ ) {

		CCommsDevice *pDev = m_pDevices->m_ppDevice[uDevice];

		if( pDev ) {

			pDev->Suspend();
			}
		}
	}

void CProxyCamera::Resume(void)
{
	for( UINT uDevice = 0; uDevice < m_uDevices; uDevice++ ) {

		CCommsDevice *pDev = m_pDevices->m_ppDevice[uDevice];

		if( pDev ) {

			pDev->Resume();
			}
		}

	m_uLast = 0;

	m_uScan	= 0;

	m_uNext	= 0;
	}

// Service Routine

void CProxyCamera::ServiceDriver(void)
{
	m_uScan = m_uScan + 1;

	m_uNext = max(m_uNext, m_uScan + 4);

	for( UINT uDevice = 0; uDevice < m_uDevices; uDevice++ ) {

		m_pDevice = m_pDevices->m_ppDevice[uDevice];

		if( m_pDevice ) {

			if( !m_pDevice->IsEnabled() ) {

				m_pDevice->SetError(errorInit);
				}
			else {
				if( IsDeviceOnline() ) {

					if( CheckDevice() ) {
					
						ServiceDevice();
						}
					}

				m_pComms->Service();
				}			
			}

		m_pPort->Poll();
		}
	}

void CProxyCamera::ServiceDevice(void)
{
	m_pPort->AddCount(ioGet, countAttempt);

	PBYTE pData = NULL;

	CCODE cCode = m_pCamera->ReadImage(pData);

	if( COMMS_SUCCESS(cCode) && pData ) {

		if( m_pMutex->Wait(FOREVER) ) {

			m_pCamera->SwapImage(pData);
			
			m_pMutex->Free();
			}

		m_pPort->AddCount(ioGet, countSuccess);
		}
	else {
		if( COMMS_ERROR(cCode) ) {

			if( m_pMutex->Wait(FOREVER) ) {

				m_pCamera->KillImage();
			
				m_pMutex->Free();
				}
			}

		if( cCode & CCODE_HARD ) {

			m_pPort->AddCount(ioGet, countError);
			}
		else {
			if( !(cCode & CCODE_NO_RETRY) ) {

				if( cCode & CCODE_BUSY ) {

					m_pPort->AddCount(ioGet, countRetry);
					
					return;
					}
				}
			else {
				m_pPort->AddCount(ioGet, countRetry);

				return;
				}

			if( cCode & CCODE_BUSY ) {

				m_pPort->AddCount(ioGet, countBusy);
				
				return;
				}

			if( cCode & CCODE_SILENT ) {

				m_pPort->AddCount(ioGet, countSilent);
				
				return;
				}
			}
		}
	}

BOOL CProxyCamera::IsDeviceOnline(void)
{
	UINT uError;
	
	switch( (uError = m_pDevice->GetError()) ) {

		case errorNone:

			return TRUE;

		case errorSoft:
		case errorInit:

			if( uError == errorSoft ) {

				if( m_pDevice->GetInvite() != m_uScan ) {

					return FALSE;
					}
				}

			if( SendDevicePing(TRUE) ) {

				m_pDevice->SetError(errorNone);

				return TRUE;
				}

			m_pDevice->SetError(errorSoft);

			m_pDevice->SetInvite(m_uNext++);

			return FALSE;

		case errorHard:

			return FALSE;
		}

	return FALSE;
	}

#define PX_DEBUG	FALSE

BOOL CProxyCamera::SendDevicePing(BOOL fDef)
{
	if( SwitchDevice() ) {

		CCODE cCode = m_pCamera->Ping();

		if( PX_DEBUG || COMMS_SUCCESS(cCode) ) {

			if( cCode == 2 ) {

				// NOTE -- Basis returns 2 as a default, so
				// we use this to indicate that Ping was not
				// really implemented by the driver.

				return fDef;
				}

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CProxyCamera::SwitchDevice(void)
{
	if( m_pCurrent != m_pDevice ) {

		if( m_pCurrent ) {

			m_pComms->DeviceClose(TRUE);
			}

		if( COMMS_SUCCESS(m_pComms->DeviceOpen(m_pDevice)) ) {

			m_pCurrent = m_pDevice;

			return TRUE;
			}

		m_pCurrent = NULL;

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyCamera::CheckDevice(void)
{
	if( !SendDevicePing(FALSE) ) {

		m_pDevice->SetInvite(m_uNext++);

		m_pDevice->SetError (errorSoft);

		return FALSE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Camera Proxy Definition
//

class CProxyCameraDef
{
	public:
		// Constructor
		CProxyCameraDef(IProxyCamera *pProxy);

		// Attributes
		BOOL CheckPort(UINT uPort);

		// Public Data
		IProxyCamera	    * m_pProxy;
		CProxyCameraDef     * m_pNext;
		CProxyCameraDef     * m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Camera Proxy Definition
//

// Constructor

CProxyCameraDef::CProxyCameraDef(IProxyCamera *pProxy)
{
	m_pProxy           = pProxy;
	}

// Attribute

BOOL CProxyCameraDef::CheckPort(UINT uPort)
{
	return m_pProxy->CheckPort(uPort);
	}

//////////////////////////////////////////////////////////////////////////
//
// Camera Proxy Definition List
//

class CProxyCameraDefList : public IProxyCameraList
{
	public:
		// Constructor
		CProxyCameraDefList(void);

		// Destructor
		~CProxyCameraDefList(void);

		// IProxyCameraList
		IProxyCamera * FindProxy(UINT uPort);
		void           Append(IProxyCamera *pProxy);

		// List
		CProxyCameraDef * m_pHead;
		CProxyCameraDef * m_pTail;
	};

//////////////////////////////////////////////////////////////////////////
//
// Camera Proxy Definition List
//

// Instantiator

IProxyCameraList * Create_ProxyCameraList(void)
{
	return New CProxyCameraDefList;
	}

// Constructor

CProxyCameraDefList::CProxyCameraDefList(void)
{
	m_pHead = NULL;

	m_pTail = NULL;
	}

// Destructor

CProxyCameraDefList::~CProxyCameraDefList(void)
{
	}

// Attributes

IProxyCamera * CProxyCameraDefList::FindProxy(UINT uPort)
{
	CProxyCameraDef *pScan = m_pHead;

	while( pScan && !pScan->CheckPort(uPort) ) {

		pScan = pScan->m_pNext;
		}

	return pScan ? pScan->m_pProxy : NULL;
	}

// Operations

void CProxyCameraDefList::Append(IProxyCamera *pProxy)
{
	CProxyCameraDef   *pDef = New CProxyCameraDef(pProxy);

	AfxListAppend( m_pHead, 
		       m_pTail, 
		       pDef, 
		       m_pNext, 
		       m_pPrev
		       );
	}

// End of File
