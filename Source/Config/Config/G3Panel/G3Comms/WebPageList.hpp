
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_WebPageList_HPP

#define INCLUDE_WebPageList_HPP

//////////////////////////////////////////////////////////////////////////
//
// Web Page List
//

class CWebPageList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CWebPageList(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

// End of File

#endif
