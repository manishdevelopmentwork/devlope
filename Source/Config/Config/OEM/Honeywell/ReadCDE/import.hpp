
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMPORT_HPP
	
#define	INCLUDE_IMPORT_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CParser;
class CDataFile;
class CMakeTags;

//////////////////////////////////////////////////////////////////////////
//
// Table Identifiers
//

const int DOCUMENT_TABLE					= 0;
const int CONTROL_BLOCK_TABLE					= 1;
const int CONFIG_VALUE_TABLE					= 2;
const int INPUT_SOURCE_TABLE					= 3;
const int SIGNAL_ATTRIBUTE_TABLE				= 4;
const int DESCRIPTOR_TEXT_TABLE					= 5;
const int EVENT_DISPLAY_TABLE					= 6;
const int OVERVIEW_DISPLAY_TABLE				= 7;
const int LOOP_TAG_TABLE					= 8;
const int RECIPE_HEADER_TABLE					= 9;
const int USER_DEF_MODBUS_MAP_TABLE				= 10;
const int RECIPE_ARCHIVE_TABLE					= 11;
const int SET_POINT_PROG_HEADER_ARCHIVE_TABLE			= 12;
const int SET_POINT_PROFILE_ARCHIVE_TABLE			= 13;
const int BLOCK_GRAPHICS_TABLE					= 14;
const int SYSTEM_CONFIGURATION_TABLE				= 15;
const int ALARM_GROUP_DISPLAY_TABLE				= 16;
const int VARIABLE_GRAPHICS_TABLE				= 17;
const int SECURITY_TABLE					= 18;
const int DATA_STORAGE_PARAMETER_TABLE				= 19;
const int TREND_STORAGE_TABLE					= 20;
const int POINT_LOG_STORAGE_TABLE				= 21;
const int ALARM_EVENT_STORAGE_TABLE				= 22;
const int ALARM_HELP_TEXT_TABLE					= 23;
const int EXECUTION_SEQUENCE_TABLE				= 24;
const int FAST_EXECUTION_SEQUENCE_TABLE				= 25;
const int IO_HARDWARE_TABLE					= 26;
const int NUMERIC_CONSTANT_TABLE				= 27;
const int GENERIC_TAG_TABLE					= 28;
const int RECIPE_POOL_HEADER_TABLE				= 29;
const int TEXT_TABLE						= 30;
const int SPP_TAG_TABLE						= 31;
const int OFF_PAGE_CONNECTOR_TABLE				= 32;
const int WIRE_GRAPHICS_TABLE					= 33;
const int RECIPE_ALLOCATION_TABLE				= 34;
const int TREND_DISPLAY_TABLE					= 35;
const int BAR_DISPLAY_TABLE					= 36;
const int PANEL_DISPLAY_TABLE					= 37;
const int MULTIPANEL_DISPLAY_TABLE				= 38;
const int STARTUP_DISPLAY_TABLE					= 39;
const int MESSAGE_DISPLAY_TABLE					= 40;
const int PANEL_METER_DISPLAY_TABLE				= 41;
const int SCREEN_BUTTON_DISPLAY_TABLE				= 42;
const int PUSHBUTTON_DISPLAY_TABLE				= 43;
const int OI_FILE_NAME_TABLE					= 44;
const int CONTROL_FILE_SYSTEM_TABLE				= 45;
const int FOUR_SELECTOR_SWITCH_DISPLAY_TABLE			= 46;
const int MODBUS_MAP_TABLE					= 47;
const int SPS_HEADER_DATA_TABLE					= 48;
const int SPS_SEGMENT_TABLE					= 49;
const int SPS_DISPLAY_TABLE					= 50;
const int SPS_POOL_HEADER_TABLE					= 51;
const int MODBUS_MASTER_SLAVE_DEVICE_TABLE			= 52;
const int STAGE_TAG_TABLE					= 53;
const int RAMP_TAG_TABLE					= 54;
const int ALTERNATOR_TAG_TABLE					= 55;
const int HOA_TAG_TABLE						= 56;
const int DEVICE_CONTROL_TAG_TABLE				= 57;
const int DRUM_SEQUENCE_HEADER_TABLE				= 58;
const int DRUM_SEQUENCE_SEGMENT_TABLE				= 59;
const int DRUM_SEQUENCE_TAG_TABLE				= 60;
const int MODBUS_MASTER_TCPSLAVE_TABLE				= 61;
const int SPS_TAG_TABLE						= 62;
const int CALENDAR_EVENT_DISPLAY_TABLE				= 63;
const int WORKSHEET_TABLE					= 64;
const int VARIABLE_TABLE					= 65;
const int EMAIL_ADDRESS_TABLE					= 66;
const int EMAIL_TEXT_TABLE					= 67;
const int ACCESS_ACCOUNTS_TABLE					= 68;
const int CALENDAR_EVENT_TABLE					= 69;
const int PPO_TAG_TABLE						= 70;
const int MODBUS_MAP_PARTITION_TABLE				= 71;
const int ANALOG_INPUT_PT_SUMMARY_TABLE				= 72;
const int ANALOG_OUTPUT_PT_SUMMARY_TABLE			= 73;
const int DIGITAL_INPUT_PT_SUMMARY_TABLE			= 74;
const int DIGITAL_OUTPUT_PT_SUMMARY_TABLE			= 75;
const int ORDER_ENTRY_TABLE					= 76;
const int PEER_TABLE						= 77;
								
//////////////////////////////////////////////////////////////////////////
//					
// Function Block Types
//

const int OMC_PID	=  5;
const int OMC_CARBON	= 62;
const int OMC_ONOFF	= 63;
const int OMC_THREE_POS	= 71;
const int OMC_AMB	= 75;
					
//////////////////////////////////////////////////////////////////////////
//					
// Special Function Block Numbers
//

const int BLKNUM_ASYS			= 1;	// Analog system block
const int BLKNUM_FSYS			= 2;	// Fast logic system block
const int BLKNUM_VARS			= 3;	// Variables block number
const int BLKNUM_ALARMDETAILS		= 6;	// Alarm Details
const int BLKNUM_HOSTS			= 10;	// Host Block
const int BLKNUM_SERIAL_S1		= 11;	// Serial Port A
const int BLKNUM_SERIAL_S2		= 12;	// Serial Port B
const int BLKNUM_NTWK_E1		= 13;	// Network Port A
const int BLKNUM_MBSLAVE		= 14;	// Modbus Serial Slave Block
const int BLKNUM_NTWK_E2		= 16;	// Network Port B
const int BLKNUM_NET_IF			= 17;	// Network Interface Block
const int BLKNUM_RSYS			= 18;	// Redundancy System Block
const int BLKNUM_RLINK			= 19;	// Redundancy Link Block
const int BLKNUM_SCNR2LINK		= 20;	// Redundancy Scanner 2 Link Block
const int BLKNUM_LEADCPU		= 21;	// Redundancy Lead CPU Monitor
const int BLKNUM_RESVCPU		= 22;	// Redundancy Reserve CPU Monitor
const int BLKNUM_RSTAT			= 24;	// Redundancy Statsus Block
const int BLKNUM_TIMEDATE		= 25;	// Time Date System Block (release 4.100 and later)
const int BLKNUM_MBTCP_SLVSTATUS	= 38;	// Modbus TCP Slave Status
const int BLKNUM_MBSER_SLVSTATUS	= 39;	// Modbus Serial Slave Status
const int BLKNUM_RACK1			= 100;	// Rack 1 (main rack)
const int BLKNUM_RACK2			= 99;	// Rack 2
const int BLKNUM_RACK3			= 98;	// Rack 3 
const int BLKNUM_RACK4			= 97;	// Rack 4
const int BLKNUM_RACK5			= 96;	// Rack 5

//////////////////////////////////////////////////////////////////////////
//					
// CDE File Parser
//

class CParser
{
	public:
		// Constructor
		CParser(FILE *pFile);

		// Attributes
		int GetPos(void) const;

		// Operations
		BYTE    ReadByte   (void);
		WORD    ReadWord   (void);
		LONG    ReadLong   (void);
		WORD    ReadBigWord(void);
		LONG    ReadBigLong(void);
		BOOL    ReadData   (CByteArray &Data, UINT uCount);
		float   ReadFloat  (void);
		CString ReadString (UINT uLen);
		void    Skip       (UINT uLen);
		void    SetPos     (int  nPos);

	protected:
		// Data Members
		FILE *m_pFile;
	};

//////////////////////////////////////////////////////////////////////////
//
// CDE File Wrapper
//

class CDataFile
{
	public:
		// Constructor
		CDataFile(void);

		// Destructor
		~CDataFile(void);

		// Operations
		BOOL Import(FILE *pFile, CMakeTags &Tags, CString &Rev);

		// Public Data
		CDeviceTypeInfo	  m_DeviceTypeInfo;
		CDeviceCaps	* m_pDeviceCaps;

	protected:
		// File Header
		struct CFileHeader
		{
			// Operations
			BOOL Read(CParser *pParse);

			// Data Members
			int m_nPlatformType;
			int m_nProductType;
			int m_nSchemaNumber;
			int m_nNumberTables;
			int m_nDemoIndicator;
			int m_nFeatureSet;
			int m_nSafety;
			};

		// Table Header
		struct CTableHeader
		{
			// Operations
			BOOL Read(CParser *pParse);

			// Data Members
			int m_nNumber;
			int m_nRecordCount;
			int m_nRecordSize;
			int m_nModCtr;
			int m_nRevNum;
			int m_nInUse;
			int m_nCrc;
			};

		// Document Data
		struct CDocumentTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CString m_Title;
			CString m_Author;
			CString m_DocumentPassword;
			CString m_WorksheetPassword;
			long    m_lCreateTime;
			long    m_lModifyTime;
			};

		// Descriptor Text
		struct CDescTextTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Attributes
			CString GetTagDescriptor(int nPos);

			// Data Members
			CByteArray m_Data;
			};

		// Control Block
		struct CControlBlockRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			int  m_bBlockType;
			int  m_bDeletedBlock;
			int  m_wBlockNum;
			long m_lIsIndex;
			long m_lSvIndex;
			long m_lCvIndex;
			int  m_bySafety;
			};

		// Control Blocks
		struct CControlBlockTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CArray <CControlBlockRecord> m_List;
			};

		// Signal Record
		struct CSignalRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void FindDescText(CDescTextTable &Desc);
			void EmitTags(CMakeTags &Tags, BOOL fAnalog);

			// Comparison
			int operator > (CSignalRecord const &That) const;

			// Data Members
			CString m_Tag;
			CString m_Desc;
			CString m_Label0;
			CString m_Label1;
			int     m_nBlockNum;
			int     m_nOutIndex;
			int     m_nDecPlaces;
			int     m_nTagType;
			int	m_nDescOffset;
			BOOL	m_fEvent;
			CString m_Folder;
			};

		// Signals
		struct CSignalTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void FindDescText(CDescTextTable &Desc);
			void EmitAnalog(CMakeTags &Tags);
			void EmitDigital(CMakeTags &Tags);
			void EmitLookup(CMakeTags &Tags);

			// Data Members
			CArray <CSignalRecord> m_List;
			};

		// Event Displays
		struct CEventDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CSignalTable &Sigs);

			// Data Members
			CStringArray m_Title;
			UINT         m_Sigs[64];
			};

		// Overview Displays
		struct COverviewDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CStringArray m_Titles;
			};

		// Loop Tag
		struct CLoopTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &DataFile);

			// Implementation
			void EmitTagsPID   (CMakeTags &Tags, CDataFile &DataFile);
			void EmitTagsOnOff (CMakeTags &Tags, CDataFile &DataFile);
			void EmitTags3Pos  (CMakeTags &Tags, CDataFile &DataFile);
			void EmitTagsCarbon(CMakeTags &Tags, CDataFile &DataFile);
			void EmitTagsAMB   (CMakeTags &Tags, CDataFile &DataFile);

			// Data Members
			CString m_Tag;
			CString m_Desc;
			CString m_Units;
			int	m_nBlockNum;
			int	m_nDecimals;
			int	m_nBlockType;
			int     m_nBlockEnum;
			};

		// Loop Tags
		struct CLoopTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &DataFile);

			// Data Members
			CArray <CLoopTagRecord> m_List;
			};

		// System Config
		struct CSysConfigTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			int m_b50Hz;
			int m_bLang;
			int m_bSaveInhibit;
			int m_bMenu1Inhibit;
			int m_bMenu2Inhibit;
			int m_bMapType;
			int m_bAlphaTest;
			};

		// Alarm Help Text
		struct CAlarmHelpTextTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CByteArray m_Data;
			};

		// Alarm Group Displays
		struct CAlarmGroupDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CSignalTable &Sigs, CAlarmHelpTextTable &Help);

			// Data Members
			CStringArray  m_Titles;
			CArray <WORD> m_Sigs;
			CArray <WORD> m_Help;
			};

		// Generic Tag
		struct CGenericTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, int nBlockType, CDataFile &DataFile);
			void CreatePushbuttonBlock(CMakeTags &Tags, int nPBB1to4, int nPBBIndex, CDataFile &DataFile);
			void CreateFourSelectorSwitchBlock(CMakeTags &Tags, int nFSS1to4, int nFSSIndex, CDataFile &DataFile);

			// Data Members
			CString m_Tag;
			CString m_Desc;
			int	m_nBlockType;
			int	m_nBlockNum;
			};

		// Generic Tags
		struct CGenericTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, int nBlockType, BOOL fEndFolder, CDataFile &DataFile);

			// Data Members
			CArray <CGenericTagRecord> m_List;
			};

		// SPP Tag
		struct CSppTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &File);

			// Data Members
			CString m_Tag;
			CString m_Desc;
			CString m_Units;
			int     m_nBlockNum;
			int     m_nDecimals;
			int     m_nAuxDec;
			};

		// SPP Tags
		struct CSppTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &File);
			void EmitWork(CMakeTags &Tags);

			// Data Members
			CArray <CSppTagRecord> m_List;
			};

		// Recipe Allocation
		struct CRecipeAllocTable
		{
			// Constructor
			CRecipeAllocTable(void);

			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			UINT m_uCount[4];
			};

		// Trend Displays
		struct CTrendDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CStringArray m_Titles;
			};

		// Bar Displays
		struct CBarDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CStringArray m_Titles;
			};

		// Panel Displays
		struct CPanelDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CStringArray m_Titles;
			};

		// Multi-Panel Displays
		struct CMultiPanelDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CStringArray m_Titles;
			};

		// Message Displays
		struct CMessageDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CStringArray m_Titles;
			};

		// Panel Meter Displays
		struct CPanelMeterDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CStringArray m_Titles;
			};

		// Screen Button Displays
		struct CScreenButtonDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			BYTE m_bFormat [8][10];
			BYTE m_bChannel[8][10];
			};

		// Push Button Display
		struct CPushButtonDisplayRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString m_Title;
			int     m_nSig[4];
			CString m_Desc[4];
			int     m_nBlockNum;
			};

		// Push Button Displays
		struct CPushButtonDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CPushButtonDisplayRecord> m_List;
			};

		// Four-State Display
		struct CFssDisplayRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString m_Title;
			CString m_Desc [4];
			CString m_BankA[4];
			CString m_BankB[4];
			CString m_BankC[4];
			CString m_BankD[4];
			int     m_nBlockNum;
			};

		// Four-State Displays
		struct CFssDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CFssDisplayRecord> m_List;
			};

		// Stage Tag
		struct CStageTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString	m_Tag;
			CString	m_Desc;
			CString	m_Labels[4];
			CString	m_PV1Units;
			CString	m_PV2Units;
			int	m_nPV1Decimals;
			int	m_nPV2Decimals;
			CString	m_SPOnUnits [4];
			CString	m_SPOffUnits[4];
			int	m_nSP1OnDecimals;
			int	m_nSP2OnDecimals;
			int	m_nSP3OnDecimals;
			int	m_nSP4OnDecimals;
			int	m_nSP1OffDecimals;
			int	m_nSP2OffDecimals;
			int	m_nSP3OffDecimals;
			int	m_nSP4OffDecimals;
			int	m_nBlockNum;
			};

		// Stage Tags
		struct CStageTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CStageTagRecord> m_List;
			};

		// Ramp Tag
		struct CRampTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString m_Tag;
			CString m_Desc;
			CString m_Label[4];
			CString m_OutUnits;
			CString m_PVUnits;
			int     m_nPVDecimal;
			int     m_nOutDecimal;
			int	m_nBlockNum;
			};

		// Ramp Tags
		struct CRampTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CRampTagRecord> m_List;
			};

		// Alternator Tag
		struct CAlternatorTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString m_Tag;
			CString m_Desc;
			int     m_nBlockNum;
			};

		// Alternator Tags
		struct CAlternatorTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CAlternatorTagRecord> m_List;
			};

		// Hand-Off-Auto Tag
		struct CHoaTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString m_Tag;
			CString m_Desc;
			CString m_State[9];
			CString m_Feedback;
			int     m_nBlockNum;
			};

		// Hand-Off-Auto Tags
		struct CHoaTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CHoaTagRecord> m_List;
			};

		// Device Control Tag
		struct CDevCtlTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString m_Tag;
			CString m_Desc;
			int     m_nBlockNum;
			};

		// Device Control Tags
		struct CDevCtlTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CDevCtlTagRecord> m_List;
			};

		// Drum Sequencer Tag
		struct CDrumSeqTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString	m_Tag;
			CString	m_Desc;
			int	m_nBlockNum;
			CString	m_PhaseLabels [50];
			CString	m_OutLabels   [16];
			int	m_nOutStates  [50];
			int	m_nEvent1Index[50];
			int	m_nEvent2Index[50];
			CString	m_AuxLabel;
			CString	m_AuxEU;
			int	m_nAuxDecPos;
			};

		// Drum Sequencer Tags
		struct CDrumSeqTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);
			void EmitWork(CMakeTags &Tags);

			// Data Members
			CArray <CDrumSeqTagRecord> m_List;
			};

		// Setpoint Sequencer Display
		struct CSpsDisplayRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			int m_nDec[8];
			int m_nAuxDec[8];
			};

		// Setpoint Sequencer Displays
		struct CSpsDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			CArray <CSpsDisplayRecord> m_List;
			};

		// Setpoint Sequencer Tag
		struct CSpsTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CSpsDisplayRecord &Display);

			// Data Members
			CString m_Tag;
			CString m_Desc;
			CString m_Units;
			int	m_nBlockNum;
			int	m_nAuxBlock;
			};

		// Setpoint Sequencer Tags
		struct CSpsTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CSpsDisplayTable &Disp);
			void EmitWork(CMakeTags &Tags);

			// Data Members
			CArray <CSpsTagRecord> m_List;
			};

		// Calendar Event Display
		struct CCalEvtDisplayRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString m_Title;
			CString m_EventTitle[5];
			CString m_EventLabel[8];
			CString m_SpecDayLabel[16];
			int	m_nTagIndex[8];
			int	m_nBlockNum;
			};

		// Calendar Event Displays
		struct CCalEvtDisplayTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CCalEvtDisplayRecord> m_List;
			};

		// Worksheet Record
		struct CWorksheetRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, int index);

			// Data Members
			CString m_Name;
			CString m_Comment;
			int     m_nType;
			int     m_nAccess;
			int     m_nIndex;
			int     m_nSpare;
			};

		// Worksheet
		struct CWorksheetTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CWorksheetRecord> m_List;
			};

		// Variable Record
		struct CVariableRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void FindDescText(CDescTextTable &Desc);
			void EmitTags(CMakeTags &Tags, BOOL fAnalog, int index);

			// Comparison
			int operator > (CVariableRecord const &That) const;

			// Data Members
			CString m_Name;
			CString m_Desc;
			CString m_Label0;
			CString m_Label1;
			int     m_nDecPlaces;
			int     m_nTagType;
			int     m_nDescOffset;
			float   m_InitValue;
			int     m_bySafety;
			CString m_Folder;
			};

		// Variables
		struct CVariableTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void FindDescText(CDescTextTable &Desc);
			void EmitAnalog(CMakeTags &Tags);
			void EmitDigital(CMakeTags &Tags);
			void EmitLookup(CMakeTags &Tags);

			// Data Members
			CArray <CVariableRecord> m_List;
			};

		// Cal Event Tag
		struct CCalEvtTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &DataFile);

			// Data Members
			CString m_Tag;
			int	m_nBlockNum;
			};

		// Cal Event Tags
		struct CCalEvtTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &DataFile);

			// Data Members
			CArray <CCalEvtTagRecord> m_List;
			};

		// PPO Tag
		struct CPpoTagRecord
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CString m_Tag;
			CString m_Desc;
			int	m_nBlockNum;
			};

		// PPO Tags
		struct CPpoTagTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <CPpoTagRecord> m_List;
			};

		// Peers
		struct CPeerTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags);

			// Data Members
			CArray <int> m_List;
			};

		// Analog Input
		struct CAnalogInput
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			BYTE    m_bRack;
			BYTE    m_bSlot;
			BYTE    m_bChan;
			CString m_Units;
			int     m_nSignal;
			int     m_nBlockNum;
			int     m_nValue;
			int     m_nStatus;
			};

		// Analog Inputs
		struct CAnalogInputTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &File);
			void EmitSafetyTags(CMakeTags &Tags, CDataFile &File);
			
			// Data Members
			CArray <CAnalogInput> m_List;
			};

		// Analog Output
		struct CAnalogOutput
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			BYTE    m_bRack;
			BYTE    m_bSlot;
			BYTE    m_bChan;
			CString m_Units;
			int     m_nSignal;
			int     m_nBlockNum;
			int     m_nValue;
			int     m_nStatus;
			};

		// Analog Outputs
		struct CAnalogOutputTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &File);

			// Data Members
			CArray <CAnalogOutput> m_List;
			};

		// Digital Input
		struct CDigitalInput
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			BYTE    m_bRack;
			BYTE    m_bSlot;
			BYTE    m_bChan;
			CString m_Text0;
			CString m_Text1;
			int     m_nSignal;
			int     m_nBlockNum;
			int     m_nValue;
			int     m_nStatus;
			};

		// Digital Inputs
		struct CDigitalInputTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &File);

			// Data Members
			CArray <CDigitalInput> m_List;
			};

		// Digital Output
		struct CDigitalOutput
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);

			// Data Members
			BYTE    m_bRack;
			BYTE    m_bSlot;
			BYTE    m_bChan;
			BYTE    m_bType;
			CString m_Text0;
			CString m_Text1;
			int     m_nSignal;
			int     m_nBlockNum;
			int     m_nValue;
			int     m_nStatus;
			};

		// Digital Outputs
		struct CDigitalOutputTable
		{
			// Operations
			BOOL Read(CParser *pParse, CTableHeader &Header);
			void EmitTags(CMakeTags &Tags, CDataFile &File);

			// Data Members
			CArray <CDigitalOutput> m_List;
			};

		// Data Members
		CFileHeader				m_FileHeader;
		CTableHeader				m_TableHeader;
		CDocumentTable				m_DocumentTable;		// 0
		CDescTextTable				m_DescTextTable;		// 5
		CControlBlockTable			m_ControlBlockTable;		// 1
		CSignalTable				m_SignalTable;			// 4
		CEventDisplayTable			m_EventDisplayTable;		// 6
		COverviewDisplayTable			m_OverviewDisplayTable;		// 7
		CLoopTagTable				m_LoopTagTable;			// 8
		CSysConfigTable				m_SysConfigTable;		// 15
		CAlarmGroupDisplayTable			m_AlarmGroupDisplayTable;	// 16
		CAlarmHelpTextTable			m_AlarmHelpTextTable;		// 23
		CGenericTagTable			m_GenericTagTable;		// 28
		CSppTagTable				m_SppTagTable;			// 31
		CRecipeAllocTable			m_RecipeAllocTable;		// 34
		CTrendDisplayTable			m_TrendDisplayTable;		// 35
		CBarDisplayTable			m_BarDisplayTable;		// 36
		CPanelDisplayTable			m_PanelDisplayTable;		// 37
		CMultiPanelDisplayTable			m_MultiPanelDisplayTable;	// 38
		CMessageDisplayTable			m_MessageDisplayTable;		// 40
		CPanelMeterDisplayTable			m_PanelMeterDisplayTable;	// 41
		CScreenButtonDisplayTable		m_ScreenButtonDspTable;		// 42
		CPushButtonDisplayTable			m_PushbuttonDisplayTable;	// 43
		CFssDisplayTable			m_FssDisplayTable;		// 46
		CSpsDisplayTable			m_SpsDisplayTable;		// 50
		CStageTagTable				m_StageTagTable;		// 53
		CRampTagTable				m_RampTagTable;			// 54
		CAlternatorTagTable			m_AlternatorTagTable;		// 55
		CHoaTagTable				m_HoaTagTable;			// 56
		CDevCtlTagTable				m_DevCtlTagTable;		// 57
		CDrumSeqTagTable			m_DrumSeqTagTable;		// 60
		CSpsTagTable				m_SpsTagTable;        		// 62
		CCalEvtDisplayTable			m_CalEvtDisplayTable;   	// 63
		CWorksheetTable				m_WorksheetTable;		// 64
		CVariableTable				m_VariableTable;		// 65
		CCalEvtTagTable				m_CalEvtTagTable;        	// 69
		CPpoTagTable				m_PpoTagTable;        		// 70
		CPeerTable				m_PeerTable;		    	// 77
		CAnalogInputTable			m_AnalogInputTable;		// 72
		CAnalogOutputTable			m_AnalogOutputTable;		// 73
		CDigitalInputTable			m_DigitalInputTable;		// 74
		CDigitalOutputTable			m_DigitalOutputTable;		// 75

		// Friends
		friend struct CGenericTagRecord;
		friend struct CCalEvtTagRecord;

		// Implementation
		void FindDescText(void);
		void EmitTags(CMakeTags &Tags);
		void EmitDriverTags(CMakeTags &Tags);
		void EmitSystemTags(CMakeTags &Tags);
		void EmitControllerTags(CMakeTags &Tags);
		void EmitSerialTags(CMakeTags &Tags, int nBlockNum);
		void EmitEthernetTags(CMakeTags &Tags, int nBlockNum);
		void EmitModbusSlavesTags(CMakeTags &Tags, int nSlaveNum, int nBlockVersion);
		void EmitModbusTcpSlavesTags(CMakeTags &Tags, int nSlaveNum);
		void EmitHostConnectionsTags(CMakeTags &Tags, int nHostNum);
		void EmitIORacksTags(CMakeTags &Tags, int nRackNum, BOOL fReal);
		void EmitRedundantOverviewTags(CMakeTags &Tags, BOOL fReal);
		void EmitRedundantLeadCPUTags(CMakeTags &Tags, BOOL fReal);
		void EmitRedundantReserveCPUTags(CMakeTags &Tags, BOOL fReal);
		void EmitScreens(void);

		// Friends
		friend int Func(PCVOID p1, PCVOID p2);
	};

// End of File

#endif
