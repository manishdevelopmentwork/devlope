
static BYTE const Table[]  = { 1, 2, 3, 4, 5, 6, 7 };

static BYTE const Region[] = { 1, 3, 2, 4, 5, 6, 7 };

static BYTE const Span[]   = { 0, 0, 0, 0, 0, 0, 0 };

static BYTE const Attrib[] = {14, 5, 4, 7, 2, 2, 2 };

static BYTE const Type[]   = {addrRealAsReal, 
			      addrRealAsReal, 
			      addrRealAsReal, 
			      addrWordAsWord, 
			      addrRealAsReal, 
			      addrWordAsWord, 
			      addrByteAsByte };

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Johnson Controls MetaSys N2 System Slave Driver
//
//

class CMetaSysN2SystemDriver : public CSlaveDriver
{
	public:
		// Constructor
		CMetaSysN2SystemDriver(void);

		// Destructor
		~CMetaSysN2SystemDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Entry Points
		DEFMETH(void) Service(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

	protected:

		// Data Members
		UINT	m_Drop;
		BYTE	m_Rx[256];
		BYTE	m_Tx[256];
		UINT	m_Ptr;
		BYTE	m_Check;
		LPCTXT	m_pHex;
		BOOL	m_fOnline;

		// Extra Help
		IExtraHelper   * m_pExtra;
		
		// Transport		
		BOOL Rx(void);
		BOOL Tx(void);

		BOOL DoListen(void);
		void HandleFrame(void);
		void HandleSynchTimeCmd(void);
		void HandlePollNoAck(void);
		void HandlePollWithAck(void);
		void HandleIdentifyDeviceCmd(void);
	virtual	void HandleWriteSingle(UINT uRegion);
	virtual	void HandleReadSingle(UINT uRegion);
	virtual	void HandleWriteMulti(void);
	virtual	void HandleReadMulti(void);
		void HandleOverride(void);
		void HandleOverrideRelease(void);
		void HandleError(UINT uCode);

		BOOL SetData(CAddress Addr, UINT uLook, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos);
		BOOL Set(CAddress Addr, PDWORD pData, UINT uCount);
	virtual	BOOL SetAnalogs(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos, UINT uLook);
	virtual	BOOL SetBinarys(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos, UINT uLook);
	virtual	BOOL SetInternals(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos);
		
		void SendData(UINT uType, UINT uLook, UINT uAtr, PDWORD pData, UINT uCount);
	virtual	void SendAnalogs(UINT uAtr, PDWORD pData, UINT uCount);
	virtual	void SendBinaryInputs(UINT uAtr, PDWORD pData, UINT uCount);
	virtual	void SendBinaryOutputs(UINT uAtr, PDWORD pData, UINT uCount);
	virtual	void SendInternals(UINT uType, UINT uAtr, PDWORD pData, UINT uCount);

		// Frame Building
		void Start(UINT uByte);
		void AddByte(BYTE bByte);
		void AddCheck(void);

		void PutChar1(DWORD Data);
		void PutChar2(DWORD Data);
		void PutChar4(DWORD Data);
		void PutChar8(DWORD Data); 

		// Frame Access
		UINT GetChar1(UINT& uPos);
		UINT GetChar2(UINT& uPos,  BOOL fHex = TRUE);
		UINT GetChar4(UINT& uPos,  BOOL fHex = TRUE);
		UINT GetChar8(UINT& uPos);

		// Helpers
		BOOL IsOnline(void);
		BOOL IsSynchTimeCmd(UINT uCmd, UINT uSub);
		BOOL IsPollNoAck(UINT uCmd, UINT uSub);
		BOOL IsPollWithAck(UINT uCmd, UINT uSub);
		BOOL IsIdentifyDeviceTypeCmd(UINT uCmd);
	 	BOOL IsWriteSingle(UINT uCmd);
		BOOL IsReadSingle(UINT uCmd);
		BOOL IsWriteMulti(UINT uCmd, UINT uSub);
		BOOL IsReadMulti(UINT uCmd, UINT uSub);
		BOOL IsOverride(UINT uCmd, UINT uSub);
		BOOL IsOverrideReleaseReq(UINT uCmd, UINT uSub);
		UINT FindLookup(UINT uRegion);
		void FindAttribute(UINT uLook, UINT& uAtr);
		UINT FindCount(UINT uLook, UINT uAtr);
	};

// End of File
