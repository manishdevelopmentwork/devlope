
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Alignment Dialog Box
//

// Runtime Class

AfxImplementRuntimeClass(CAlignDialog, CStdDialog);
		
// Constructor

CAlignDialog::CAlignDialog(BOOL fPage)
{
	m_fPage = fPage;

	LoadConfig();

	SetName(L"AlignDialog");
	}
		
// Attributes

UINT CAlignDialog::GetHorz(void) const
{
	return m_uHorz;
	}

UINT CAlignDialog::GetVert(void) const
{
	return m_uVert;
	}

BOOL CAlignDialog::UseRef(void) const
{
	return m_fRef;
	}
		
// Message Map

AfxMessageMap(CAlignDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnCommandOK)
	
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxMessageEnd(CAlignDialog)
	};

// Message Handlers

BOOL CAlignDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	if( m_fPage ) {

		SetWindowText(CString(IDS_ALIGN_TO_PAGE));
		}
	else
		SetWindowText(CString(IDS_ALIGN_PRIMITIVES));
		
	SetRadioGroup(100, 103, m_uHorz);

	SetRadioGroup(200, 203, m_uVert);

	SetRadioGroup(300, 301, m_fRef && !m_fPage);

	GetDlgItem(300).EnableWindow(!m_fPage);
	
	GetDlgItem(301).EnableWindow(!m_fPage);

	return TRUE;
	}

BOOL CAlignDialog::OnCommandOK(UINT uID)
{
	m_uHorz = GetRadioGroup(100, 103);

	m_uVert = GetRadioGroup(200, 203);

	m_fRef  = GetRadioGroup(300, 301);

	SaveConfig();

	EndDialog(1);

	return TRUE;
	}

BOOL CAlignDialog::OnCommandCancel(UINT uID)
{
	EndDialog(0);

	return TRUE;
	}

// Implementation

void CAlignDialog::LoadConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"G3Graph");

	Key.MoveTo(L"Align");

	m_uHorz = Key.GetValue(L"Horz", UINT(0));

	m_uVert = Key.GetValue(L"Vert", UINT(0));

	m_fRef  = Key.GetValue(L"Ref",  UINT(0));
	}

void CAlignDialog::SaveConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"G3Graph");

	Key.MoveTo(L"Align");

	Key.SetValue(L"Horz", m_uHorz);

	Key.SetValue(L"Vert", m_uVert);

	Key.SetValue(L"Ref",  m_fRef);
	}

// End of File
