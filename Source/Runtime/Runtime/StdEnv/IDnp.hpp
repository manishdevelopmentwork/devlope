
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IDnp_HPP

#define INCLUDE_IDnp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 24 -- DNP3
//

interface IDnpChannelConfig;
interface IDnpChannel;
interface IDnpSession;
interface IDnpMasterSession;
interface IDnpSlaveSession;

//////////////////////////////////////////////////////////////////////////
//
// DNP Config Interface
//

interface IDnpChannelConfig : public IUnknown
{
	AfxDeclareIID(24, 1);

	virtual IPADDR METHOD GetIP(void)		 = 0;
	virtual WORD   METHOD GetTcpPort(void)		 = 0;
	virtual WORD   METHOD GetUdpPort(void)		 = 0;
	virtual WORD   METHOD GetConnectionTimeout(void) = 0;
	virtual void   METHOD SetFail(void)		 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP Channel Port Interface
//

interface IDnpChannel : public IUnknown
{
	AfxDeclareIID(24, 2);

	virtual BOOL		    METHOD Open(void)							 = 0;
	virtual BOOL		    METHOD Close(void)							 = 0;
	virtual IDnpSession *	    METHOD OpenSession (WORD wDest, WORD wTO, DWORD dwLink, void * pCfg) = 0;
	virtual BOOL		    METHOD CloseSession(WORD wDest)					 = 0;
	virtual BOOL		    METHOD CloseSessions(void)						 = 0;
	virtual BOOL		    METHOD EnableSessions(BOOL fEnable)					 = 0;
	virtual WORD		    METHOD GetSource(void)						 = 0;
	virtual IDnpChannelConfig * METHOD GetConfig(void)						 = 0;
	virtual void *		    METHOD GetChannel(void)						 = 0;
	virtual void		    METHOD Service(void)						 = 0;
	virtual void		    METHOD Share(BOOL fOpen, UINT uBackOff = 0)				 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP Session Device Interface
//

interface IDnpSession : public IUnknown
{
	AfxDeclareIID(24, 3);

	virtual BOOL METHOD Open(IDnpChannel * pChan)					= 0;
	virtual BOOL METHOD Close(void)							= 0;
	virtual BOOL METHOD Ping(void)							= 0;
	virtual UINT METHOD Validate(BYTE o, WORD i, BYTE t, UINT uCount)		= 0;
	virtual UINT METHOD GetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount)	= 0;
	virtual UINT METHOD GetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount)		= 0;
	virtual UINT METHOD GetTimeStamp(BYTE o, WORD i, PDWORD pData, UINT uCount)	= 0;
	virtual UINT METHOD GetClass(BYTE o, WORD i, PDWORD pData, UINT uCount)		= 0;
	virtual UINT METHOD GetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount)	= 0;
	virtual UINT METHOD GetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP Master Session Device Interface
//

interface IDnpMasterSession : public IDnpSession
{
	AfxDeclareIID(24, 4);

	virtual BOOL METHOD PollClass(BYTE bClass)						 = 0;
	virtual BOOL METHOD Poll(BYTE bObject, WORD i, UINT uCount)				 = 0;
	virtual BOOL METHOD PollEvents(BYTE bObject, WORD i, UINT uCount)			 = 0;
	virtual BOOL METHOD SyncTime(void)							 = 0;
	virtual BOOL METHOD ColdRestart(void)							 = 0;
	virtual BOOL METHOD WarmRestart(void)							 = 0;
	virtual BOOL METHOD UnsolicitedEnable(BYTE bMask)					 = 0;
	virtual BOOL METHOD UnsolicitedDisable(BYTE bMask)					 = 0;
	virtual UINT METHOD AssignClass(BYTE bObject, WORD i, PDWORD pData, UINT uCount)	 = 0;
	virtual BOOL METHOD AnalogCmd(WORD i, BYTE t, PDWORD pData, UINT uCount)		 = 0;
	virtual BOOL METHOD BinaryCmd(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount) = 0;
	virtual BOOL METHOD AnalogDbd(WORD i, BYTE t, PDWORD pData, UINT uCount)		 = 0;
	virtual BOOL METHOD FreezeCtr(WORD i, PDWORD pData)					 = 0;
	virtual UINT METHOD GetFeedBack(void)							 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP Slave Session Device Interface
//

interface IDnpSlaveSession : public IDnpSession
{
	AfxDeclareIID(24, 5);

	virtual UINT METHOD SetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount, BYTE eMask) = 0;
	virtual UINT METHOD SetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount, BYTE eMask)	    = 0;
	virtual UINT METHOD SetClass(BYTE o, WORD i, PDWORD pData, UINT uCount)			    = 0;
	virtual UINT METHOD SetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount)		    = 0;
	virtual UINT METHOD SetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount)		    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP Interface
//

interface IDnp  : public IUnknown
{
	AfxDeclareIID(24, 6);

	virtual IDnpChannelConfig * METHOD GetConfig(IPADDR IP, IPADDR IP2, WORD wTcp, WORD wUdp, WORD wTO)			= 0;
	virtual BOOL		    METHOD Init(void)										= 0;
	virtual void		    METHOD Service(void)									= 0;
	virtual IDnpChannel *	    METHOD OpenChannel(IDataHandler *pHandler, WORD wSrc, BOOL fMaster)				= 0;
	virtual IDnpChannel *	    METHOD OpenChannel(IDnpChannelConfig *pSocket, WORD wSrc, BOOL fMaster)			= 0;
	virtual BOOL		    METHOD CloseChannel(void *pVoid, WORD wSrc, BOOL fMaster)					= 0;
	virtual IDnpSession *	    METHOD OpenSession(IDnpChannel *pChannel, WORD wDest, WORD wTO, DWORD dwLink, void *pCfg)	= 0;
	virtual BOOL		    METHOD CloseSession(IDnpSession *pSession)							= 0;
	virtual void		    METHOD SetColdStart(void)									= 0;
	virtual void		    METHOD SetWarmStart(void)									= 0;
	virtual void		    METHOD InitLock(void *pLock)								= 0;
	virtual	void		    METHOD Lock(void *pLock)									= 0;
	virtual	void		    METHOD Free(void *pLock)									= 0;
	virtual	void		    METHOD DeleteLock(void *pLock)								= 0;
	};

// End of File

#endif
