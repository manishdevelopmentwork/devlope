
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Table Heading
//

// Runtime Class

AfxImplementRuntimeClass(CUIHeading, CUIControl);

// Constructor

CUIHeading::CUIHeading(PCTXT pTag, PCTXT pLabel)
{
	m_UIData.m_Tag     = pTag;

	m_UIData.m_ClassUI = AfxThisClass();

	m_Label            = pLabel;

	m_pCtrlLayout      = NULL;

	m_pStatic          = New CStatic;
	}

// Core Overridables

void CUIHeading::OnLayout(CLayFormation *pForm)
{
	m_pCtrlLayout = New CLayItemText( m_Label,
					  1,
					  1
					  );

	m_pMainLayout = New CLayFormPad(  m_pCtrlLayout,
					  CRect(4, 4, 4, 4),
					  horzLeft | vertCenter
					  );

	pForm->AddItem(New CLayItem(TRUE));

	pForm->AddItem(m_pMainLayout);
	}

void CUIHeading::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pStatic->Create( m_Label,
			   SS_LEFT,
			   m_pCtrlLayout->GetRect(),
			   Wnd,
			   NOTHING
			   );

	m_pStatic->SetFont(afxFont(Dialog));

	AddControl(m_pStatic);
	}

void CUIHeading::OnPosition(void)
{
	m_pStatic->MoveWindow(m_pCtrlLayout->GetRect(), TRUE);
	}

// End of File
