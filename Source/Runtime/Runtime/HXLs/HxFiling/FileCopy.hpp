
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_FileCopy_HPP

#define	INCLUDE_FileCopy_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS File Copier 
//

class CRlosFileCopy : public IFileCopier
{
	public:
		// Constructors
		CRlosFileCopy(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IFileCopier
		BOOL CopyFile(PCTXT pSrc, PCTXT pDst, UINT uFlags);
		BOOL MoveFile(PCTXT pSrc, PCTXT pDst, UINT uFlags);
		BOOL CopyDir (PCTXT pSrc, PCTXT pDst, UINT uFlags);
		BOOL MoveDir (PCTXT pSrc, PCTXT pDst, UINT uFlags);

	protected:
		// Internal Flags
		enum Flags
		{
			flagDelete = Bit(31)
			};

		// Data
		ULONG	         m_uRefs;
		IFileManager   * m_pFileMan;
		IFilingSystem  * m_pFileSys1;
		IFilingSystem  * m_pFileSys2;
		IFile          * m_pFile1;
		IFile          * m_pFile2;
		PBYTE            m_pBuffer;
		UINT             m_uBuffer;

		// Destructor
		~CRlosFileCopy(void);

		// Helpers
		BOOL OpenFiles(CFilename const &Src, CFilename const &Dst, UINT uFlags);
		BOOL CopyFile (CFilename const &Src, CFilename const &Dst, UINT uFlags);
		BOOL MoveFile (CFilename const &Src, CFilename const &Dst, UINT uFlags);
		BOOL CopyDir  (CFilename const &Src, CFilename const &Dst, UINT uFlags);
		BOOL MoveDir  (CFilename const &Src, CFilename const &Dst, UINT uFlags);
		
		// File / Dir
		BOOL IsDirectoryEmpty(IFilingSystem *pFileSystem, FSIndex const &Index);
		BOOL HasChanged(void);
		BOOL IsMoreRecent(void);

		// Filesystem 
		BOOL LockFileSystems(CFilename const &Src, CFilename const &Dst);
		void FreeFileSystems(void);
		BOOL LockFileSystem(CFilename const &Name, IFilingSystem *&pFileSystem);
		void FreeFileSystem(IFilingSystem *&pFileSys);
		void MakeFiles(void);
		void FreeFiles(void);
		void FreeFile(IFile *&pFile);

		// Buffer
		void AllocBuffer(UINT uSize);
		void FreeBuffer(void);
	};

// End of File

#endif
