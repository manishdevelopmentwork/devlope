
//////////////////////////////////////////////////////////////////////////
//
// MX APP Protocol Support for Automation Direct Do-More PLC
//

#include "intern.hpp"

#include "MxAppProtocol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MX APP Protocol Base Object
//

// Constructor

CMxAppCommsObject::CMxAppCommsObject(void)
{
	m_uFunc = 0;
	}

// Comms Function

UINT CMxAppCommsObject::GetFuncCode(void) const
{
	return m_uFunc;
	}


//////////////////////////////////////////////////////////////////////////
//
// Data Encapsulation
//

// Constructor

CMxAppData::CMxAppData(BYTE bTypeSize, UINT uCount, WORD wOffset, BYTE bTable)
{	
	m_bHeapOffset	= 0;
	m_bFlags	= mxFlagNone;

	m_bTable	= bTable;
	m_wMemOffset	= wOffset;
	m_uCount	= uCount;
	m_bTypeSize	= bTypeSize;
	m_uSize		= m_bTypeSize * m_uCount;

	m_pData	= new DWORD[ m_uSize ];

	memset(m_pData, 0, m_uSize);
	}

// Destructor

CMxAppData::~CMxAppData(void)
{
	delete [] m_pData;
	}

// Data Access

DWORD CMxAppData::GetData(UINT uPos)
{
	if( uPos < m_uSize ) {

		return m_pData[uPos];
		}

	return 0;
	}

BOOL CMxAppData::SetData(DWORD dwData, UINT uPos)
{
	if( uPos < m_uSize ) {

		m_pData[uPos] = dwData;

		return TRUE;
		}

	return FALSE;
	}

WORD CMxAppData::GetRequestSize(void)
{
	return WORD(m_uSize);
	}

UINT CMxAppData::GetRequestCount(void)
{
	return m_uCount;
	}

//////////////////////////////////////////////////////////////////////////
//
// MX APP Request Base Class
//

// Constructor

CMxAppRequest::CMxAppRequest(void)
{
	m_wSize	= 0;

	memset(m_Buff, 0, sizeof(m_Buff));
	}

// Destructor

CMxAppRequest::~CMxAppRequest(void)
{
	}

// Buffer Operations

void CMxAppRequest::AddByte(BYTE b)
{
	if( m_wSize < sizeof(m_Buff) ) {

		m_Buff[m_wSize] = b;

		m_wSize++;
		}
	}

void CMxAppRequest::AddWord(WORD w)
{
	AddByte(LOBYTE(w));

	AddByte(HIBYTE(w));
	}

void CMxAppRequest::AddLong(DWORD dw)
{
	AddWord(LOWORD(dw));

	AddWord(HIWORD(dw));
	}

WORD CMxAppRequest::Encode(PBYTE pBuff, UINT uSize)
{
	Process();

	if( uSize > m_wSize ) {

		memcpy(pBuff, m_Buff, m_wSize);

		return m_wSize;
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Register Session Request
//

// Constructor

CMxAppRegisterRequest::CMxAppRegisterRequest(PTXT pPassword, BYTE bKey)
{
	m_uFunc	= mxRequestSession;

	m_bKey	= bKey;

	memset(m_Pass, 0, sizeof(m_Pass));

	strncpy(m_Pass, pPassword, sizeof(m_Pass) - 1);
	}

// Destructor

CMxAppRegisterRequest::~CMxAppRegisterRequest(void)
{
	}

void CMxAppRegisterRequest::Process(void)
{
	AddByte(m_uFunc);
	
	if( m_Pass[0] ) {

		AddByte(m_bKey);

		EncryptString(m_bKey, m_Pass);
		}
	else {
		for( UINT n = 0; n < 10; n++ ) {

			AddByte(0x00);
			}
		}
	}

// Password Support

BYTE CMxAppRegisterRequest::EncryptChar(CHAR Char, UINT uPos, BYTE bKey)
{
	return Char ^ EncryptionKey[uPos & 0x7][bKey & 0x7F];
	}

void CMxAppRegisterRequest::EncryptString(BYTE bKey, PCTXT pStr)
{
	UINT n = 0;

	for( n = 0; n < 8; n++ ) {

		BYTE c = EncryptChar(pStr[n], n, bKey);

		AddByte(c);
		}

	AddByte(0x00);
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi-Read Request
//

// Constructor

CMxAppMultiReadRequest::CMxAppMultiReadRequest(WORD wSession)
{
	m_wSession = wSession;

	m_pHead    = NULL;

	m_pTail    = NULL;

	m_uCount   = 0;

	m_uFunc    = mxMultiRead;
	}

// Destructor

CMxAppMultiReadRequest::~CMxAppMultiReadRequest(void)
{
	}

void CMxAppMultiReadRequest::AddRead(CMxAppData *pReq)
{
	AfxListAppend(m_pHead, m_pTail, pReq, m_pNext, m_pPrev);

	m_uCount++;
	}

void CMxAppMultiReadRequest::Process(void)
{
	if( m_uCount > 0 ) {

		AddByte(m_uFunc);

		AddWord(m_wSession);

		AddByte(m_uCount);

		CMxAppData *pReq = m_pHead;

		while( pReq ) {

			AddByte(pReq->m_bTable);
			AddWord(pReq->m_wMemOffset);
			AddByte(pReq->m_bFlags);
			AddByte(pReq->m_bHeapOffset);
			AddByte(BYTE(pReq->GetRequestSize()));

			pReq = pReq->m_pNext;
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Bulk Read Request
//

// Constructor

CMxAppBulkReadRequest::CMxAppBulkReadRequest(WORD wSession)
{
	m_uFunc	   = mxReadBulk;

	m_pRead	   = NULL;

	m_wSession = wSession;
	}

// Destructor

CMxAppBulkReadRequest::~CMxAppBulkReadRequest(void)
{
	}

// Read List

void CMxAppBulkReadRequest::AddRead(CMxAppData *pReq)
{
	m_pRead = pReq;
	}

// Implementation

void CMxAppBulkReadRequest::Process(void)
{
	if( m_pRead ) {

		AddByte(m_uFunc);

		AddWord(m_wSession);

		AddByte(m_pRead->m_bTable);
		AddWord(m_pRead->m_wMemOffset);
		AddByte(m_pRead->m_bFlags);
		AddWord(WORD(m_pRead->m_bHeapOffset));
		AddWord(m_pRead->GetRequestSize());
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi-Write Request
//

// Constructor

CMxAppMultiWriteRequest::CMxAppMultiWriteRequest(WORD wSession)
{
	m_uFunc	   = mxMultiWrite;

	m_uCount   = 0;

	m_pHead	   = NULL;

	m_pTail    = NULL;

	m_wSession = wSession;
	}

// Destructor

CMxAppMultiWriteRequest::~CMxAppMultiWriteRequest(void)
{
	}

// Read List

void CMxAppMultiWriteRequest::AddWrite(CMxAppData *pReq)
{
	AfxListAppend(m_pHead, m_pTail, pReq, m_pNext, m_pPrev);

	m_uCount++;
	}
		
// Implementation

void CMxAppMultiWriteRequest::Process(void)
{
	if( m_uCount > 0 ) {

		AddByte(m_uFunc);

		AddWord(m_wSession);

		AddByte(m_uCount);

		CMxAppData *pReq = m_pHead;

		while( pReq ) {

			AddByte(pReq->m_bTable);
			AddWord(pReq->m_wMemOffset);
			AddByte(pReq->m_bFlags);
			AddByte(pReq->m_bHeapOffset);

			BYTE bReqSize = BYTE(pReq->GetRequestSize());

			AddByte(bReqSize);

			UINT uCount = pReq->GetRequestCount();

			for( UINT n = 0; n < uCount; n++ ) {

				switch( pReq->m_bTypeSize ) {

					case 1:
						AddByte(pReq->GetData(n));
						break;

					case 2:
						AddWord(pReq->GetData(n));
						break;

					case 4:
						AddLong(pReq->GetData(n));
						break;
					}
				}

			pReq = pReq->m_pNext;
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi-Write with Mask Request 
//

// Constructor

CMxAppMultiWriteMaskRequest::CMxAppMultiWriteMaskRequest(WORD wSession, BOOL fIsBit, UINT uBitOffset)
{
	m_uFunc		= mxMultiWriteMask;

	m_uCount	= 0;

	m_pHead		= NULL;

	m_pTail		= NULL;

	m_wSession	= wSession;

	m_fIsBit	= fIsBit;

	m_uBitOffset	= uBitOffset;
	}

// Destructor

CMxAppMultiWriteMaskRequest::~CMxAppMultiWriteMaskRequest(void)
{
	}

// Read List

void CMxAppMultiWriteMaskRequest::AddWrite(CMxAppData *pReq)
{
	AfxListAppend(m_pHead, m_pTail, pReq, m_pNext, m_pPrev);

	m_uCount++;
	}
		
// Implementation

void CMxAppMultiWriteMaskRequest::Process(void)
{
	if( m_uCount > 0 ) {

		AddByte(m_uFunc);

		AddWord(m_wSession);

		AddByte(m_uCount);

		CMxAppData *pReq = m_pHead;

		while( pReq ) {

			AddByte(pReq->m_bTable);
			AddWord(pReq->m_wMemOffset);
			AddByte(pReq->m_bFlags);
			AddByte(pReq->m_bHeapOffset);

			BYTE bReqSize = m_fIsBit ? BYTE(pReq->GetRequestSize()) : 4;

			AddByte(bReqSize);

			UINT uCount = pReq->GetRequestCount();

			for( UINT n = 0; n < uCount; n++ ) {

				WORD wOffset = pReq->m_wMemOffset;

				DWORD dwData = pReq->GetData(n);

				dwData <<= m_uBitOffset;

				UINT uMaskSize = m_fIsBit ? 0 : pReq->m_bTypeSize;

				DWORD dwMask = GetMask(uMaskSize);

				AddData(bReqSize, dwData);

				AddData(bReqSize, dwMask);
				}

			pReq = pReq->m_pNext;
			}
		}
	}

DWORD CMxAppMultiWriteMaskRequest::GetMask(UINT uSize)
{
	DWORD dwMask = 0;

	switch( uSize ) {

		case 0:
			dwMask = 1 << m_uBitOffset;
			break;
		case 1:
			dwMask = 0xFF << m_uBitOffset;
			break;
		case 2:
			dwMask = 0xFFFF << m_uBitOffset;
			break;
		case 4:
			dwMask = 0xFFFFFFFF;
			break;
		}

	return dwMask;
	}

void CMxAppMultiWriteMaskRequest::AddData(UINT uSize, DWORD dwData)
{
	switch( uSize ) {

		case 1:
			AddByte(BYTE(dwData));

			break;
		case 2:
			AddWord(WORD(dwData));

			break;
		case 4:
			AddLong(dwData);

			break;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Bulk Write Request
//

// Constructor

CMxAppBulkWriteRequest::CMxAppBulkWriteRequest(WORD wSession)
{
	m_uFunc		= mxWriteBulk;

	m_wSession	= wSession;
	}

// Destructor

CMxAppBulkWriteRequest::~CMxAppBulkWriteRequest(void)
{
	}

// Read List

void CMxAppBulkWriteRequest::AddWrite(CMxAppData *pReq)
{
	m_pWrite = pReq;
	}
		
// Implementation

void CMxAppBulkWriteRequest::Process(void)
{
	AddByte(m_uFunc);

	AddWord(m_wSession);

	if( m_pWrite ) {

		AddByte(m_pWrite->m_bTable);
		AddWord(m_pWrite->m_wMemOffset);
		AddByte(m_pWrite->m_bFlags);
		AddWord(WORD(m_pWrite->m_bHeapOffset));
		AddWord(m_pWrite->GetRequestSize());

		UINT uCount = m_pWrite->GetRequestCount();

		for( UINT n = 0; n < uCount; n++ ) {

			switch( m_pWrite->m_bTypeSize ) {

				case 1:
					AddByte(m_pWrite->GetData(n));
					break;

				case 2:
					AddWord(m_pWrite->GetData(n));
					break;

				case 4:
					AddLong(m_pWrite->GetData(n));
					break;
				}
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// MX APP Reply Base Class
//

// Constructor

CMxAppReply::CMxAppReply(void)
{
	m_bError = 0;
	}

// Destructor

CMxAppReply::~CMxAppReply(void)
{
	}

BYTE CMxAppReply::GetError(void)
{
	return m_bError;
	}

// Implementation

BOOL CMxAppReply::CheckStatus(PBYTE pBuff, UINT uCount)
{
	BYTE bFunc = pBuff[0];

	if( bFunc == m_uFunc ) {

		return TRUE;
		}

	if( bFunc == mxError ) {

		m_bError = pBuff[1];
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Register Session Reply
//

// Constructor

CMxAppRegisterReply::CMxAppRegisterReply(void)
{
	m_wSession	= 0;

	m_wPermLevel	= 0;

	m_uFunc		= mxRequestSession;

	memset(m_User, 0, sizeof(m_User));
	}

// Destructor

CMxAppRegisterReply::~CMxAppRegisterReply(void)
{
	}

// Reply Parsing

BOOL CMxAppRegisterReply::Parse(PBYTE pBuff, UINT uSize)
{
	if( !CheckStatus(pBuff, uSize) ) {

		return FALSE;
		}

	m_wSession	= IntelToHost(*PU2(pBuff + 1));

	m_wPermLevel	= IntelToHost(*PU2(pBuff + 3));

	PTXT pUser	= PTXT(pBuff + 5);

	strncpy(m_User, pUser, sizeof(m_User) - 1);

	return TRUE;
	}

WORD CMxAppRegisterReply::GetSession(void)
{
	return m_wSession;
	}

WORD CMxAppRegisterReply::GetPermLevel(void)
{
	return m_wPermLevel;
	}

PCTXT CMxAppRegisterReply::GetUser(void)
{
	return m_User;
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi Read Reply
//

// Constructor

CMxAppMultiReadReply::CMxAppMultiReadReply(CMxAppData *pRead)
{
	m_pRead  = pRead;

	m_uFunc  = mxMultiRead;
	}

// Destructor

CMxAppMultiReadReply::~CMxAppMultiReadReply(void)
{

	}

// Reply Parsing

BOOL CMxAppMultiReadReply::Parse(PBYTE pBuff, UINT uSize)
{
	if( !CheckStatus(pBuff, uSize) ) {

		return FALSE;
		}

	for( UINT n = 0; n < m_pRead->GetRequestCount(); n++ ) {

		DWORD dwData = 0;

		BYTE bTypeSize = m_pRead->m_bTypeSize;

		PBYTE pData = pBuff + 1 + (bTypeSize * n);

		switch( bTypeSize ) {

			case 1:
				dwData = *(pData);
				break;

			case 2:
				dwData = IntelToHost(*PU2(pData));
				break;

			case 4:
				dwData = IntelToHost(*PU4(pData));
				break;
			}

		m_pRead->SetData(dwData, n);
		}

	return TRUE;
	}

// Data Access

CMxAppData * CMxAppMultiReadReply::GetDataObject()
{
	return m_pRead;
	}

//////////////////////////////////////////////////////////////////////////
//
// Bulk Read Reply
//

// Constructor

CMxAppBulkReadReply::CMxAppBulkReadReply(CMxAppData *pRead)
{
	m_pRead  = pRead;

	m_uFunc  = mxReadBulk;
	}

// Destructor

CMxAppBulkReadReply::~CMxAppBulkReadReply(void)
{
	}

// Reply Parsing

BOOL CMxAppBulkReadReply::Parse(PBYTE pBuff, UINT uSize)
{
	if( !CheckStatus(pBuff, uSize) ) {

		return FALSE;
		}

	for( UINT n = 0; n < m_pRead->GetRequestCount(); n++ ) {

		DWORD dwData = 0;

		BYTE bTypeSize = m_pRead->m_bTypeSize;

		PBYTE pData = pBuff + 1 + (bTypeSize * n);

		switch( bTypeSize ) {

			case 1:
				dwData = *(pData);
				break;

			case 2:
				dwData = IntelToHost(*PU2(pData));
				break;

			case 4:
				dwData = IntelToHost(*PU4(pData));
				break;
			}

		m_pRead->SetData(dwData, n);
		}

	return TRUE;
	}

// Data Access

CMxAppData * CMxAppBulkReadReply::GetDataObject()
{
	return m_pRead;
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi Write Reply
//

// Constructor

CMxAppMultiWriteReply::CMxAppMultiWriteReply(void)
{
	m_uFunc = mxMultiWrite;
	}

// Destructor

CMxAppMultiWriteReply::~CMxAppMultiWriteReply(void)
{
	}

// Reply Parsing

BOOL CMxAppMultiWriteReply::Parse(PBYTE pBuff, UINT uSize)
{
	return CheckStatus(pBuff, uSize);
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi Write Mask Reply
//

// Constructor

CMxAppMultiWriteMaskReply::CMxAppMultiWriteMaskReply(void)
{
	m_uFunc = mxMultiWriteMask;
	}

// Destructor

CMxAppMultiWriteMaskReply::~CMxAppMultiWriteMaskReply(void)
{
	}

// Reply Parsing

BOOL CMxAppMultiWriteMaskReply::Parse(PBYTE pBuff, UINT uSize)
{
	return CheckStatus(pBuff, uSize);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bulk Write Reply
//

// Constructor

CMxAppBulkWriteReply::CMxAppBulkWriteReply(void)
{
	m_uFunc = mxWriteBulk;
	}

// Destructor

CMxAppBulkWriteReply::~CMxAppBulkWriteReply(void)
{
	}

// Reply Parsing

BOOL CMxAppBulkWriteReply::Parse(PBYTE pBuff, UINT uSize)
{
	return CheckStatus(pBuff, uSize);
	}

// End of File
