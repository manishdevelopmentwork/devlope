
#include "Intern.hpp"

#include "dnp3m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CDnp3Master::CDnp3Master(void)
{
	m_pCtx	    = NULL;

	m_uFbNext   = 0;

	m_pFeedBack = NULL;
	}

// Destructor

CDnp3Master::~CDnp3Master(void)
{
	
	}

// Management

void MCALL CDnp3Master::Attach(IPortObject *pPort)
{
	CDnp3Base::Attach(pPort);

	if( !m_pFeedBack ) {

		m_pFeedBack = new Feedback[limitFB];

		for( UINT i = 0; i < limitFB; i++ ) {

			m_pFeedBack[i].m_bObject	= 0;

			m_pFeedBack[i].m_wPoint		= 0;
	
			m_pFeedBack[i].m_uFlags		= 0;

			m_pFeedBack[i].m_uTimeStamp	= 0;
			}
		}
	}

void MCALL CDnp3Master::Detach(void)
{
	CDnp3Base::Detach();

	if( m_pFeedBack ) {

		delete [] m_pFeedBack;

		m_pFeedBack = NULL;
		}
	}

// User Access

UINT MCALL CDnp3Master::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	return 0;
	}

// Entry Points

CCODE MCALL CDnp3Master::Ping(void)
{
	CDnp3Base::Ping();

	if( m_pCtx->m_pSession ) {

		if( m_pCtx->m_pSession->Ping() ) {
			
			return CCODE_SUCCESS;
			}

		CloseSession();
		}

	CDnp3Base::Service();
	
	return CCODE_ERROR;
	}

CCODE MCALL CDnp3Master::Read(AREF Address, PDWORD pData, UINT uCount)
{
	CDnp3Base::Ping();

	if( m_pCtx->m_pSession ) {

		BYTE o = FindObject(Address);

		WORD i = FindIndex(Address);

		BYTE e = Address.a.m_Extra;

		if( IsFeedback(o) ) {

			GetFeedBack(i, e, pData, uCount);

			return uCount;
			}

		if( IsWriteOnly(o, e) ) {

			return uCount;
			}

		MakeMin(uCount, 448);

		BYTE t = FindType(Address);

		if( Validate(o, i, t, uCount) ) {

			if( IsValue(e) ) {

				if( m_pCtx->m_Poll ) {

					if( !PollClass() ) {

						return CCODE_ERROR;
						}
					}

				else if( IsEvent(o) ) {

					if( !m_pCtx->m_pSession->PollEvents(o, i, uCount) ) {

						return FindCount(t, uCount, TRUE);
						}
					}
				else {
					if( !m_pCtx->m_pSession->Poll(o, i, uCount) ) {

						return CCODE_ERROR;
						}
					}
				}

			return GetData(o, i, t, e, pData, uCount);
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDnp3Master::Write(AREF Address, PDWORD pData, UINT uCount)
{
	CDnp3Base::Ping();

	if( m_pCtx->m_pSession ) {

		BYTE o = FindObject(Address);

		BYTE e = Address.a.m_Extra;
	
		if( IsReadOnly(o, e) ) {

			return uCount;
			}

		WORD i = FindIndex(Address);

		if( IsNamedCmd(o) ) {

			return HandleNamedCmd(o, i, pData);
			}

		MakeMin(uCount, 448);

		BYTE t = FindType(Address);

		if( Validate(o, i, t, uCount) ) {

			return HandleCmd(o, i, t, e, pData, uCount);
			}
		
		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_ERROR;
	}

// Implementation

BOOL  CDnp3Master::Validate(BYTE o, WORD i, BYTE t, UINT &uCount)
{
	if( m_pCtx->m_pSession ) {

		if( (uCount = m_pCtx->m_pSession->Validate(o, i, t, FindCount(t, uCount, FALSE))) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL  CDnp3Master::PollClass(void)
{
	BYTE bMask = 0;

	for( UINT u = 0; u < elements(m_pCtx->m_Class); u++ ) {

		if( IsTimedOut(u) ) {

			bMask |= 1 << u;
			}
		}

	if( bMask ) {

		if( !m_pCtx->m_pSession->PollClass(bMask) ) {

			return FALSE;
			}

		for( UINT u = 0; u < elements(m_pCtx->m_Class); u++ ) {

			if( bMask & (1 << u) ) {

				m_pCtx->m_Time[u] = GetTickCount();
				}
			}
		}

	return TRUE;
	}

CCODE CDnp3Master::GetData(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount)
{
	CCODE Result = CCODE_ERROR | CCODE_HARD;;

	switch( e ) {

		case eValue: Result = m_pCtx->m_pSession->GetValue    (o, i, t, pData, uCount);	break;

		case eFlags: Result = m_pCtx->m_pSession->GetFlags    (o, i,    pData, uCount);	break;

		case eTimeS: Result = m_pCtx->m_pSession->GetTimeStamp(o, i,    pData, uCount);	break;

		case eClass: Result = m_pCtx->m_pSession->GetClass    (o, i,    pData, uCount);	break;

		case eBinOn: Result = m_pCtx->m_pSession->GetOnTime   (o, i,    pData, uCount);	break;

		case eBinOf: Result = m_pCtx->m_pSession->GetOffTime  (o, i,    pData, uCount);	break;
		}

	return COMMS_SUCCESS(Result) ? FindCount(t, Result, TRUE) : CCODE_ERROR;
	}

CCODE CDnp3Master::HandleNamedCmd(BYTE o, WORD i, PDWORD pData)
{
	if( pData[0] ) {

		BOOL fReturn = FALSE;

		switch( i ) {

			case 1:	fReturn = m_pCtx->m_pSession->ColdRestart();				break;

			case 2:	fReturn = m_pCtx->m_pSession->WarmRestart();				break;

			case 3:	fReturn = m_pCtx->m_pSession->UnsolicitedEnable (pData[0] & 0xFF);	break;

			case 4:	fReturn = m_pCtx->m_pSession->UnsolicitedDisable(pData[0] & 0xFF);	break;

			case 5: fReturn = m_pCtx->m_pSession->SyncTime();				break;
			}

		if( !fReturn ) {

			return SetFeedBack(o, i) ? 1 : CCODE_ERROR;
			}
		}

	return 1;
	}

CCODE CDnp3Master::HandleCmd(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount)
{
	if( e == 3 ) {

		UINT Count = m_pCtx->m_pSession->AssignClass(o, i, pData, uCount);

		if( Count != uCount ) {

			return SetFeedBack(o, i) ? uCount : Count;
			}

		return uCount;
		}

	BOOL fReturn = TRUE;
					
	switch( o ) {

		case 10: fReturn = m_pCtx->m_pSession->BinaryCmd(o, i, t, e, pData, uCount);	break;

		case 40: fReturn = m_pCtx->m_pSession->AnalogCmd(i, t, pData, uCount);		break;

		case 34: fReturn = m_pCtx->m_pSession->AnalogDbd(i, t, pData, uCount);		break;

		case 20: fReturn = e != 4;

			if( !fReturn ) {

				fReturn = m_pCtx->m_pSession->FreezeCtr(i, pData);

				uCount  = 1;
				}
			break;				 						
			}

	UINT uResult = FindCount(t, uCount, TRUE);

	if( !fReturn ) {

		return SetFeedBack(o, i) ? uResult : CCODE_ERROR;
		}
				
	return uResult;
	}


BOOL CDnp3Master::SetFeedBack(BYTE bObject, WORD wIndex)
{
	DWORD dwFailMask = m_pCtx->m_pSession->GetFeedBack();

	if( m_pFeedBack && dwFailMask ) {

		m_pFeedBack[m_uFbNext].m_bObject    = bObject;

		m_pFeedBack[m_uFbNext].m_wPoint     = wIndex;

		m_pFeedBack[m_uFbNext].m_uFlags     = dwFailMask;

		m_pFeedBack[m_uFbNext].m_uTimeStamp = m_pExtra->GetNow();

		m_uFbNext = (m_uFbNext + 1) % limitFB;

		return TRUE;
		}

	return FALSE;
	}

void CDnp3Master::GetFeedBack(WORD i, BYTE e, PDWORD pData, UINT uCount)
{
	if( m_pFeedBack ) {

		MakeMin(uCount, limitFB);

		for( UINT u = 0; u < uCount; u++ ) {

			switch( e ) {

				case 0:	pData[u] = m_pFeedBack[u + i].m_bObject;	break;

				case 1: pData[u] = m_pFeedBack[u + i].m_wPoint;		break;

				case 2: pData[u] = m_pFeedBack[u + i].m_uFlags;		break;

				case 3: pData[u] = m_pFeedBack[u + i].m_uTimeStamp;	break;
				}
			}
		}
	}

BOOL CDnp3Master::IsTimedOut(UINT uClass)
{
	if( m_pCtx->m_Time[uClass] == 0 ) {

		return TRUE;
		}

	return int(GetTickCount() - m_pCtx->m_Time[uClass] - m_pCtx->m_Class[uClass]) >= 0;
	}

// Session

void CDnp3Master::OpenSession(void)
{
	if( m_pDnp && m_pChannel && !m_pCtx->m_pSession ) {

		m_pCtx->m_pSession = (IDnpMasterSession *)m_pDnp->OpenSession(m_pChannel,
									      m_pCtx->m_Dest,
									      m_pCtx->m_TO,
									      m_pCtx->m_Link,
									      NULL);
		}
	}

void CDnp3Master::CloseSession(void)
{
	if( m_pDnp && m_pChannel && m_pCtx->m_pSession ) {

		if( m_pDnp->CloseSession(m_pCtx->m_pSession) ) {
		
			m_pCtx->m_pSession = NULL;
			}
		}
	}


// Helpers

BOOL CDnp3Master::IsEvent(BYTE bObject)
{
	switch( bObject ) {

		case 2:	
		case 4:
		case 11:
		case 13:
		case 22:
		case 23:
		case 32:
		case 42:
		case 43:
			return TRUE;
		}
	
	return FALSE;
	}

BOOL CDnp3Master::IsFeedback(BYTE bObject)
{
	return bObject == 239;
	}

BOOL CDnp3Master::IsValue(BYTE e)
{
	return e == 0;
	}

BOOL CDnp3Master::IsNamedCmd(BYTE o)
{
	return o == addrNamed;
	}

BOOL CDnp3Master::IsReadOnly(BYTE bObject, BYTE e)
{
	switch( bObject ) {

		case 1:
		case 3:
		case 20:
		case 21:
		case 30:
			return e < 3;

		case 10:
		case 34:
		case 40:
			return e && e < 3;

		case 239:
			return TRUE;
		}

	return IsEvent(bObject);
	}

BOOL CDnp3Master::IsWriteOnly(BYTE bObject, BYTE e)
{
	if( bObject == 20 && e == 4 ) {	// Counter Freeze Cmd

		return TRUE;
		}

	return bObject == addrNamed;
	}


// End of File
