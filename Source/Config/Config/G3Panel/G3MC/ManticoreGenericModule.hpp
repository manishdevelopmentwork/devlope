
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ManticoreGenericModule_HPP

#define INCLUDE_ManticoreGenericModule_HPP

//////////////////////////////////////////////////////////////////////////
//
// Manticore Generic Module
//

class CManticoreGenericModule : public CGenericModule
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CManticoreGenericModule(void);

protected:
	// Download Data
	void MakeConfigData(CInitData &Init);
};

// End of File

#endif
