
//////////////////////////////////////////////////////////////////////////
//
// R245 Modular Controller - AMAT POD Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PODDBASE_H

#define	INCLUDE_PODDBASE_H

//////////////////////////////////////////////////////////////////////////
//
// RS232 Port Configuration
//

#define	POD_BAUD_1200	0
#define	POD_BAUD_2400	1
#define	POD_BAUD_4800	2
#define	POD_BAUD_9600	3
#define	POD_BAUD_19200	4

#define	POD_BITS_7	0
#define	POD_BITS_8	1

#define	POD_PAR_NONE	0
#define	POD_PAR_ODD	1
#define	POD_PAR_EVEN	2

#define	POD_STOP_1	0
#define	POD_STOP_2	1

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfigPOD
{
	BYTE	RS232Baud  [4];
	BYTE	RS232Bits  [4];
	BYTE	RS232Parity[4];
	BYTE	RS232Stop  [4];

	BYTE	GUID[16];
	BYTE	Valid;

	} CONFIGPOD;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatusPOD
{
	BYTE	Running;

	} STATUSPOD;
	
// End of File

#endif
