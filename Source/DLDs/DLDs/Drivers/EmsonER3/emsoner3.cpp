
#include "intern.hpp"

#include "emsoner3.hpp"

WORD CODE_SEG CEmersonER3KDriver::ER3CRCTable[] = {

	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
	0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
	0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
	0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
	0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
	0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
	0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
	0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
	0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
	0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
	0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
	0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
	0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
	0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
	0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
	0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
	0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
	0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
	0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
	0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
	0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
	0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
	0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
	0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
	0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
	0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
	0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
	0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
	0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
	0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
	0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson ER3000 Driver
//

// Instantiator

INSTANTIATE(CEmersonER3KDriver);

// Constructor

CEmersonER3KDriver::CEmersonER3KDriver(void)
{
	m_Ident		= DRIVER_ID;

	m_uWErrCt	= 0;
	}

// Destructor

CEmersonER3KDriver::~CEmersonER3KDriver(void)
{
	}

// Configuration

void MCALL CEmersonER3KDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CEmersonER3KDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEmersonER3KDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmersonER3KDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmersonER3KDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmersonER3KDriver::Ping(void)
{
	CAddress Addr;

	Addr.a.m_Table	= addrNamed;
	Addr.a.m_Offset	= 53;
	Addr.a.m_Extra  = 0;

	DWORD Data[1];

	return Read(Addr, Data, 1);
	}

CCODE MCALL CEmersonER3KDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nREAD %x %x %x", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( NoReadTransmit(Addr, pData, uCount) ) return uCount;

	return DoRead(Addr, pData, FALSE) ? 1 : CCODE_ERROR;
	}

CCODE MCALL CEmersonER3KDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\nWRITE %x %x %x", Addr.a.m_Table, Addr.a.m_Offset, *pData);

	if( NoWriteTransmit(Addr, pData, uCount) ) return uCount;

	if( DoWrite(Addr, pData) || ++m_uWErrCt >= 3 ) {

		m_uWErrCt = 0;

		return 1;
		}

	return CCODE_ERROR;
	}

// User Function
UINT MCALL CEmersonER3KDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	return 0;
	}

// Frame Building

void CEmersonER3KDriver::StartFrame(BYTE bLen, BYTE bOp, BYTE bCommand)
{
	m_uPtr = 0;

	AddByte(m_pCtx->m_bDrop);
	AddByte(bLen);
	AddByte(bOp);
	AddByte(bCommand);
	}

void CEmersonER3KDriver::AddByte(BYTE bData)
{
	if( m_uPtr < PBSZ ) {
	
		m_bTx[m_uPtr++] = bData;
		}
	}

void CEmersonER3KDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));
	AddByte(LOBYTE(wData));
	}

void CEmersonER3KDriver::EndFrame(void)
{
	WORD wCRC = MakeCRC(m_bTx, m_uPtr);

	AddByte(HIBYTE(wCRC));

	AddByte(LOBYTE(wCRC));
	}

WORD CEmersonER3KDriver::MakeCRC(PBYTE pBuff, UINT uCount)
{
	WORD wCRC = 0xFFFF;

	for( UINT i = 0; i < uCount; i++ ) {
	
		BYTE bData = pBuff[i];

		wCRC = (LOBYTE(wCRC) << 8) ^ ER3CRCTable[ HIBYTE(wCRC) ^ bData ];
		}

	return wCRC;
	}
		
// Transport Layer

BOOL CEmersonER3KDriver::Transact(void)
{
	EndFrame();

	Put();

	return GetReply();
	}

BOOL CEmersonER3KDriver::DoRead(AREF Addr, PDWORD pData, BOOL fStoreProf)
{
	if( IsScale(Addr) ) {

		return DoReadScale(Addr.a.m_Offset, pData);
		}

	BOOL fAN = Addr.a.m_Table == addrNamed;

	StartFrame(2, fAN ? 2 : 0xA, Addr.a.m_Offset);

	if( Transact() ) {

		if( fAN ) {
		
			WORD x   = PU2(m_bRx + 2)[0];
			
			pData[0] = LONG(SHORT(MotorToHost(x)));

			if( !IsSigned(Addr) ) {

				pData[0] = DWORD(LOWORD(pData[0]));
				}

			return TRUE;
			}

		GetProfileData(pData, Addr, fStoreProf);

		return TRUE;
		}

	return FALSE;
	}

BOOL CEmersonER3KDriver::DoReadScale(UINT uOffset, PDWORD pData)
{
	CAddress A;

	A.a.m_Table  = addrNamed;

	A.a.m_Offset = uOffset - SCL_DIF; // Real offset->HI offset

	DWORD Data[1];

	if( DoRead(A, Data, FALSE) ) {

		DWORD dData = LOWORD(Data[0]) << 16;

		A.a.m_Offset--; // read LO value

		if( DoRead(A, Data, FALSE) ) {

			*pData = dData + LOWORD(Data[0]);

			return TRUE;
			}
		}

	return FALSE;
	}

void CEmersonER3KDriver::GetProfileData(PDWORD pData, AREF Addr, BOOL fStoreProf)
{
	WORD x   = *PU2(m_bRx + 3);

	DWORD d1 = LONG(SHORT(MotorToHost(x)));

	x        = *PU2(m_bRx + 5);

	DWORD d2 = LONG(SHORT(MotorToHost(x)));

	if( fStoreProf ) {

		UINT uItem = Addr.a.m_Offset;

		m_pCtx->m_bWProfT[uItem] = m_bRx[2];
		m_pCtx->m_wWProf1[uItem] = LOWORD(d1);
		m_pCtx->m_wWProf2[uItem] = LOWORD(d2);

		return;
		}
	
	switch( Addr.a.m_Table ) {

		case PRT:
			pData[0] = DWORD(m_bRx[2]);
			break;

		case PR1:
			pData[0] = d1;
			break;

		case PR2:
			pData[0] = d2;
			break;
		}
	}

// Write Handlers

BOOL CEmersonER3KDriver::DoWrite(AREF Addr, PDWORD pData)
{
	if( IsScale(Addr) ) {

		return DoWriteScale(Addr.a.m_Offset, pData);
		}

	BOOL fAN = Addr.a.m_Table == addrNamed;

	UINT uO  = LOBYTE(Addr.a.m_Offset);

	if( fAN ) {

		StartFrame( 4, 1, uO );

		AddWord(LOWORD(*pData));
		}

	else {
		StartFrame( 7, 9, uO );

		AddByte(m_pCtx->m_bWProfT[uO]);

		AddWord(m_pCtx->m_wWProf1[uO]);

		AddWord(m_pCtx->m_wWProf2[uO]);
		}

	return Transact();
	}

BOOL CEmersonER3KDriver::DoWriteScale(UINT uOffset, PDWORD pData)
{	
	CAddress A;

	A.a.m_Table  = addrNamed;
	A.a.m_Offset = uOffset - SCL_DIF - 1; // Write LO value first

	DWORD dData  = LOWORD(*pData);

	if( DoWrite(A, &dData) ) {

		A.a.m_Offset++; // Write HI value

		dData = HIWORD(*pData);

		return DoWrite(A, &dData);
		}

	return FALSE;
	}

// Helpers
BOOL CEmersonER3KDriver::NoReadTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uT = Addr.a.m_Table;

	if( uT == PWR ) {

		pData[0] = 0;

		return TRUE;
		}

	if( IsProfileDataWrite(Addr.a.m_Table) ) {

		UINT uItem = Addr.a.m_Offset;

		UINT i;

		if( uT == PWT ) {

			for( i = 0; i < uCount; i++ ) {

				pData[i] = DWORD(m_pCtx->m_bWProfT[uItem + i]);
				}

			return TRUE;
			}

		PWORD p = uT == PW1 ? m_pCtx->m_wWProf1 : m_pCtx->m_wWProf2;

		for( i = 0; i < uCount; i++ ) {

			pData[i] = DWORD(p[uItem + i]);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CEmersonER3KDriver::NoWriteTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uT    = Addr.a.m_Table;

	UINT uItem = Addr.a.m_Offset;

	switch( uT ) {

		case PRT: // write to PRT reads existing profile data into write profile cache
			CAddress A;

			A.a.m_Table  = PRT;
			A.a.m_Offset = uItem;

			DWORD Data[1];

			DoRead(A, Data, TRUE);

			return TRUE;

		case PR1:
		case PR2:
			return TRUE;

		case PWR:
			return !pData[0];

		case addrNamed:

			switch( LOBYTE(uItem) ) {
			// Read Only
				case   0:
				case   1:
				case   5:
				case   6:
				case   7:
				case   8:
				case  10:
				case  11:
				case  14:
				case  18:
				case  34:
				case  38:
				case  41:
				case  44:
				case  51:
				case  65:
				case  69:
				case  71:
				case  74:
				case  75:
				case  77:
				case  78:
				case 100:
				case 101:
				case 102:
				case 103:
				case 104:
				case 105:
				case 106:
				case 107:
				case 112:
				case 113:
				case 118:
				case 120:
				case 121:
				case 122:

			// Invalid
				case  2:
				case 15:
				case 24:
				case 31:
				case 32:
				case 33:
				case 36:
				case 45:
				case 97:
				case 99:
					return TRUE;
				}
		}

	if( IsProfileDataWrite(uT) ) {

		UINT i;

		if( uT == PWT ) {

			for( i = 0; i < uCount; i++ ) {

				m_pCtx->m_bWProfT[uItem + i] = LOBYTE(pData[i]);
				}

			return TRUE;
			}

		PWORD p = uT == PW1 ? m_pCtx->m_wWProf1 : m_pCtx->m_wWProf2;

		for( i = 0; i < uCount; i++ ) {

			p[uItem + i] = LOWORD(pData[i]);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CEmersonER3KDriver::IsSigned(AREF Addr) {

	if( Addr.a.m_Table == addrNamed ) {

		switch( Addr.a.m_Offset ) {

			case  0:
			case  3:
			case  9:
			case 10:
			case 11:
			case 14:
			case 16:
			case 18:
			case 21:
			case 22:
			case 23:
			case 25:
			case 28:
			case 29:
			case 30:
			case 34:
			case 35:
			case 43:
			case 46:
			case 49:
			case 50:
			case 60:
			case 61:
			case 62:
			case 63:
			case 64:
			case 65:
				return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEmersonER3KDriver::IsProfileDataWrite(UINT uTable)
{
	switch( uTable ) {

		case PWT:
		case PW1:
		case PW2:
			return TRUE;
		}

	return FALSE;
	}

BOOL CEmersonER3KDriver::IsScale(AREF Addr)
{
	return Addr.a.m_Table == addrNamed && (Addr.a.m_Offset == SCL_MIN || Addr.a.m_Offset == SCL_MAX);
	}

// Port Access
void CEmersonER3KDriver::Put(void)
{
	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write( m_bTx, m_uPtr, FOREVER );
	}

BOOL CEmersonER3KDriver::GetReply(void)
{
	UINT uState	= 0;
	UINT uTimeout	= 1000;
	UINT uPtr	= 0;
	UINT uEnd	= PBSZ - 1;

	UINT uData;

	SetTimer(uTimeout);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (uData = Get(uTimeout)) < NOTHING ) {

			BYTE bData  = LOBYTE(uData);

			m_bRx[uPtr] = bData;

//**/			AfxTrace1("<%2.2x>", bData);

			switch( uState ) {

				case 0:
					if( uPtr == 1 ) {

						uEnd   = min( PBSZ - 3, uPtr + bData );

						uState = 1;
						}

					break;

				case 1:
					if( uPtr >= uEnd ) uState = 2;

					break;

				case 2: // CRC byte 1
					uState = 3;

					uEnd++;

					break;

				case 3: // CRC byte 2

					PBYTE p;

					p = &m_bRx[uEnd]; // point to 1st CRC

					WORD c1;

					c1 = MakeCRC(m_bRx, uEnd);

					return HIBYTE(c1) == p[0] && LOBYTE(c1) == p[1] && CheckReply();
				}

			uPtr++;
			}
		}

	return FALSE;
	}

BOOL CEmersonER3KDriver::CheckReply(void)
{
	if( !m_bRx[0] ) {

		BYTE b = m_bRx[1];

		switch( m_bTx[2] ) { // command number

			case 1:	// Write Variable
			case 9: // Write Profile

				return !m_bRx[2] && b == 1;

			case 2:	// Read Variable

				return b == 2;

			case 10: // Read Profile

				return b == 5;
			}
		}

	return FALSE;
	}

UINT CEmersonER3KDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
