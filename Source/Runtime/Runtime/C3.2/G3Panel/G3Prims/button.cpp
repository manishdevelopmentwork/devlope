
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Graduated Button
//

// Static Data

BOOL  CPrimGradButton::m_ShadeMode = 0;

COLOR CPrimGradButton::m_ShadeCol1 = 0;

COLOR CPrimGradButton::m_ShadeCol2 = 0;

// Constructor

CPrimGradButton::CPrimGradButton(void)
{
	m_pColor1  = New CPrimColor(naBack);

	m_pColor2  = New CPrimColor(naFeature);

	m_pEdge    = New CPrimPen;
	}

// Destructor

CPrimGradButton::~CPrimGradButton(void)
{
	delete m_pColor1;

	delete m_pColor2;

	delete m_pEdge;
	}

// Initialization

void CPrimGradButton::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimGradButton", pData);

	CPrimWithText::Load(pData);

	m_pColor1->Load(pData);

	m_pColor2->Load(pData);

	m_pEdge->Load(pData);
	}

// Overridables

void CPrimGradButton::SetScan(UINT Code)
{
	m_pColor1->SetScan(Code);

	m_pColor2->SetScan(Code);

	m_pEdge->SetScan(Code);

	CPrimWithText::SetScan(Code);
	}

void CPrimGradButton::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimWithText::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

void CPrimGradButton::DrawPrim(IGDI *pGDI)
{
	R2 r = m_DrawRect;

	m_pEdge->AdjustRect(r);

	r.x2 -= 1;
	r.y2 -= 1;

	int nCorn = 8;

	MakeMin(nCorn, (r.y2 - r.y1) / 2);

	MakeMin(nCorn, (r.x2 - r.x1) / 2);

	P2 p[8];

	p[0].x = r.x1;
	p[0].y = r.y1 + nCorn;

	p[1].x = r.x1 + nCorn,
	p[1].y = r.y1;

	p[2].x = r.x2 - nCorn;
	p[2].y = r.y1;

	p[3].x = r.x2;
	p[3].y = r.y1 + nCorn;

	p[4].x = r.x2;
	p[4].y = r.y2 - nCorn;

	p[5].x = r.x2 - nCorn;
	p[5].y = r.y2;

	p[6].x = r.x1 + nCorn;
	p[6].y = r.y2;

	p[7].x = r.x1;
	p[7].y = r.y2 - nCorn;

	m_ShadeMode = 0;

	if( IsPressed() ) {

		m_ShadeCol1 = m_Ctx.m_Color2;

		m_ShadeCol2 = m_Ctx.m_Color1;
		}
	else {
		m_ShadeCol1 = m_Ctx.m_Color1;

		m_ShadeCol2 = m_Ctx.m_Color2;
		}

	pGDI->SetBrushStyle(brushFore);

	pGDI->ShadePolygon(p, 8, 0x000055, Shader);

	m_pEdge->DrawPolygon(pGDI, p, 8, 0x000055);

	// REV3 -- Show disabled text differently?

	CPrimWithText::DrawPrim(pGDI);
	}

void CPrimGradButton::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_DrawRect));
	}

// Color Mixing

#if 0

COLOR CPrimGradButton::Mix16(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	int rb = GetRED  (b);
	int gb = GetGREEN(b);
	int bb = GetBLUE (b);

	int rc = ra + ((rb - ra) * p / c);
	int gc = ga + ((gb - ga) * p / c);
	int bc = ba + ((bb - ba) * p / c);

	return GetRGB(rc, gc, bc);
	}

#else

DWORD CPrimGradButton::Mix32(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	int rb = GetRED  (b);
	int gb = GetGREEN(b);
	int bb = GetBLUE (b);

	ra = (ra << 3) | (ra >> 2);
	ga = (ga << 3) | (ga >> 2);
	ba = (ba << 3) | (ba >> 2);

	rb = (rb << 3) | (rb >> 2);
	gb = (gb << 3) | (gb >> 2);
	bb = (bb << 3) | (bb >> 2);

	int rc = ra + ((rb - ra) * p / c);
	int gc = ga + ((gb - ga) * p / c);
	int bc = ba + ((bb - ba) * p / c);

	return MAKELONG(MAKEWORD(bc, gc), MAKEWORD(rc, 0xFF));
	}

#endif

// Shaders

BOOL CPrimGradButton::Shader(IGDI *pGDI, int p, int c)
{
	static MIXCOL q = 0;

	if( c ) {

		MIXCOL k = Mixer(m_ShadeCol1, m_ShadeCol2, p, c);

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = MIXINV;

	return m_ShadeMode;
	}

// Context Creation

void CPrimGradButton::FindCtx(CCtx &Ctx)
{
	Ctx.m_Color1 = m_pColor1->GetColor();
	
	Ctx.m_Color2 = m_pColor2->GetColor();
	}

// Context Check

BOOL CPrimGradButton::CCtx::operator == (CCtx const &That) const
{
	return m_Color1 == That.m_Color1 &&
	       m_Color2 == That.m_Color2 ;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Button Base Class
//

// Constructor

CPrimImageBtnBase::CPrimImageBtnBase(void)
{
	memset(m_pImage, 0, sizeof(m_pImage));

	m_fAlias = FALSE;
	}

// Destructor

CPrimImageBtnBase::~CPrimImageBtnBase(void)
{
	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		delete m_pImage[n];
		}
	}

// Initialization

void CPrimImageBtnBase::Load(PCBYTE &pData)
{
	CPrimWithText::Load(pData);

	BOOL fTrans = FALSE;

	BOOL fAlias = FALSE;

	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		if( GetByte(pData) ) {

			m_pImage[n] = New CPrimImage;

			m_pImage[n]->Load(pData);

			if( m_pImage[n]->IsTransparent(m_DrawRect) ) {

				fTrans = TRUE;
				}

			if( m_pImage[n]->IsAntiAliased() ) {

				fAlias = TRUE;
				}
			}
		}

	m_fTrans = (m_fTrans && fTrans);

	m_fAlias = (m_fAlias && fAlias);

	m_ImageRect.x1 = GetWord(pData);
	m_ImageRect.y1 = GetWord(pData);
	m_ImageRect.x2 = GetWord(pData);
	m_ImageRect.y2 = GetWord(pData);
	}

// Overridables

void CPrimImageBtnBase::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimWithText::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_fTrans ) {

			if( m_fAlias ) {

				(m_fChange ? Erase : Trans).Append(m_DrawRect);
				}
			else {
				if( m_fChange ) {

					Erase.Append(m_DrawRect);
					}
				}
			}
		}
	}

void CPrimImageBtnBase::DrawPrim(IGDI *pGDI)
{
	UINT n;

	for( n = 0; n < elements(m_pImage); n++ ) {

		if( m_pImage[n] ) {

			break;
			}
		}

	if( n < elements(m_pImage) ) {

		UINT i = m_Ctx.m_uIndex;

		if( m_pImage[i] ) {

			UINT rop = m_Ctx.m_fEnable ? 0 : ropDisable;

			m_pImage[i]->DrawItem(pGDI, m_ImageRect, rop);
			}
		}
	else {
		pGDI->ResetFont();

		pGDI->SetTextTrans(modeTransparent);

		PCUTF pt = L"PRESS";

		int   cx = pGDI->GetTextWidth(pt);

		int   cy = pGDI->GetTextHeight(pt);

		int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, pt);
		}

	CPrimWithText::DrawPrim(pGDI);
	}

void CPrimImageBtnBase::MovePrim(int cx, int cy)
{
	m_ImageRect.x1 += cx;

	m_ImageRect.y1 += cy;

	m_ImageRect.x2 += cx;

	m_ImageRect.y2 += cy;

	CPrimWithText::MovePrim(cx, cy);
	}

// Image Selection

UINT CPrimImageBtnBase::GetIndex(void)
{
	return 0;
	}

// Context Creation

void CPrimImageBtnBase::FindCtx(CCtx &Ctx)
{
	Ctx.m_uIndex  = GetIndex();

	Ctx.m_fEnable = m_pAction ? m_pAction->IsEnabled() : TRUE;
	}

// Context Check

BOOL CPrimImageBtnBase::CCtx::operator == (CCtx const &That) const
{
	return m_uIndex  == That.m_uIndex  &&
	       m_fEnable == That.m_fEnable ;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Indicator
//

// Constructor

CPrimImageIndicator::CPrimImageIndicator(void)
{
	m_pState = NULL;
	}

// Destructor

CPrimImageIndicator::~CPrimImageIndicator(void)
{
	delete m_pState;
	}

// Initialization

void CPrimImageIndicator::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimImageIndicator", pData);

	CPrimImageBtnBase::Load(pData);

	GetCoded(pData, m_pState);
	}

// Overridables

void CPrimImageIndicator::SetScan(UINT Code)
{
	SetItemScan(m_pState, Code);

	CPrimImageBtnBase::SetScan(Code);
	}

// Image Selection

UINT CPrimImageIndicator::GetIndex(void)
{
	if( m_pState ) {
		
		if( m_pState->IsAvail() ) {
			
			if( m_pState->Execute(typeInteger) ) {

				return 1;
				}
			}
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Illuminated Button
//

// Constructor

CPrimImageIllumButton::CPrimImageIllumButton(void)
{
	m_pValue   = NULL;

	m_Button   = 0;

	m_Delay    = 0;

	m_pEnable  = NULL;

	m_Local    = 0;

	m_Protect  = 0;

	m_fValue   = FALSE;

	m_fState   = FALSE;

	m_fTime    = FALSE;
	}

// Destructor

CPrimImageIllumButton::~CPrimImageIllumButton(void)
{
	delete m_pValue;

	delete m_pEnable;
	}

// Initialization

void CPrimImageIllumButton::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimImageIllumButton", pData);

	CPrimImageBtnBase::Load(pData);

	GetCoded(pData, m_pState);

	GetCoded(pData, m_pValue);

	m_Button = GetByte(pData);

	m_Delay  = GetWord(pData);

	GetCoded(pData, m_pEnable);

	m_Local   = GetByte(pData);

	m_Protect = GetByte(pData);
	}

// Attributes

BOOL CPrimImageIllumButton::IsAvail(void) const
{
	if( !IsItemAvail(m_pState) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pValue) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pEnable) ) {

		return FALSE;
		}

	return TRUE;
	}

// Overridables

void CPrimImageIllumButton::SetScan(UINT Code)
{
	SetItemScan(m_pState, Code);

	SetItemScan(m_pValue, Code);
	
	SetItemScan(m_pEnable, Code);

	CPrimImageBtnBase::SetScan(Code);
	}

UINT CPrimImageIllumButton::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgIsPressed:

			return m_fPress;
		}

	if( m_Button < 6 ) {

		switch( uMsg ) {

			case msgTakeFocus:

				return OnTakeFocus(uMsg, uParam);

			case msgKillFocus:

				if( !m_Delay && m_fPress ) {

					OnTouchUp();

					return TRUE;
					}
				
				return FALSE;

			case msgTouchInit:
			case msgTouchDown:

				if( !m_fTime ) {

					BOOL rv = OnTouchDown();

					m_uTime = GetTickCount();

					return rv;
					}

				return 0;

			case msgTouchRepeat:

				return OnTouchRepeat();

			case msgTouchUp:

				if( SetDelayTimer(m_uTime, m_Delay) ) {

					m_fTime = TRUE;

					return 0;
					}

				return OnTouchUp();

			case msgTimer:

				m_fTime = FALSE;

				return OnTouchUp();

			case msgBlockRemote:

				return m_Local == 1;
			}
		}

	return CPrimImageBtnBase::OnMessage(uMsg, uParam);
	}

void CPrimImageIllumButton::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_DrawRect));
	}

// Message Handlers

UINT CPrimImageIllumButton::OnTakeFocus(UINT uMsg, UINT uParam)
{
	if( uParam == 0 ) {

		if( IsAvail() ) {

			if( m_pEnable && !m_pEnable->ExecVal() ) {

				return FALSE;
				}
			
			return TRUE;
			}

		return FALSE;
		}

	if( uParam == 1 ) {

		return TRUE;
		}

	return FALSE;
	}

UINT CPrimImageIllumButton::OnTouchDown(void)
{
	m_fPress = TRUE;

	if( CUISystem::m_pThis->m_pUI->Protect(this, m_Protect, stateDown) ) {

		switch( m_Button ) {

			case 0:
				SetValue(!m_fValue);
				
				break;

			case 1:
				if( !(m_fInit = m_fValue) ) {

					SetValue(1);
					}
				break;

			case 2:
			case 4:
				SetValue(1);
				
				break;

			case 3:
			case 5:
				SetValue(0);
				
				break;
			}
		}

	return TRUE;
	}

UINT CPrimImageIllumButton::OnTouchRepeat(void)
{
	return FALSE;
	}

UINT CPrimImageIllumButton::OnTouchUp(void)
{
	m_fPress = FALSE;

	if( CUISystem::m_pThis->m_pUI->Protect(this, m_Protect, stateUp) ) {

		switch( m_Button ) {

			case 1:
				if( m_fInit ) {

					SetValue(0);
					}
				break;

			case 2:
				SetValue(0);
				
				break;

			case 3:
				SetValue(1);
				
				break;
			}
		}

	return TRUE;
	}

// Image Selection

UINT CPrimImageIllumButton::GetIndex(void)
{
	GetValue();

	UINT nState = 0;

	if( IsPressed() ) {

		nState += 1;
		}

	if( m_fState ) {

		nState += 2;
		}

	return nState;
	}

// Implementation

void CPrimImageIllumButton::GetValue(void)
{
	if( m_pValue ) {

		m_fValue = m_pValue->Execute(typeInteger);
		}

	if( m_pState ) {

		m_fState = m_pState->Execute(typeInteger);
		}
	else
		m_fState = m_fValue;

	}

void CPrimImageIllumButton::SetValue(C3INT Data)
{
	if( m_pValue ) {

		m_pValue->SetValue(Data, typeInteger, setNone);
		}

	m_fValue = Data;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Button
//

// Constructor

CPrimImageButton::CPrimImageButton(void)
{
	}

// Destructor

CPrimImageButton::~CPrimImageButton(void)
{
	}

// Overridables

void CPrimImageButton::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_DrawRect));
	}

// Initialization

void CPrimImageButton::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimImageButton", pData);

	CPrimImageBtnBase::Load(pData);
	}

// Image Selection

UINT CPrimImageButton::GetIndex(void)
{
	return IsPressed();
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Toggle Base Class
//

// Constructor

CPrimImageToggle::CPrimImageToggle(void)
{
	m_pValue    = NULL;

	m_pEnable   = NULL;

	m_Local     = 0;

	m_Protect   = 0;

	m_Axis      = 0;

	m_Mode      = 0;

	m_Delay     = 0;

	m_Default   = 0;

	m_pPress1   = NULL;

	m_pRelease1 = NULL;

	m_pPress2   = NULL;

	m_pRelease2 = NULL;

	m_fTime     = FALSE;
	}

// Destructor

CPrimImageToggle::~CPrimImageToggle(void)
{
	delete m_pValue;

	delete m_pEnable;

	delete m_pPress1;

	delete m_pRelease1;

	delete m_pPress2;

	delete m_pRelease2;
	}

// Initialization

void CPrimImageToggle::Load(PCBYTE &pData)
{
	CPrimImageBtnBase::Load(pData);

	GetCoded(pData, m_pValue);
	GetCoded(pData, m_pEnable);
	
	m_Local   = GetByte(pData);
	m_Protect = GetByte(pData);
	m_Axis    = GetByte(pData);
	m_Mode    = GetByte(pData);
	m_Delay   = GetWord(pData);
	m_Default = GetByte(pData);

	if( m_Mode == 4 ) {

		GetCoded(pData, m_pPress1);
		GetCoded(pData, m_pRelease1);
		GetCoded(pData, m_pPress2);
		GetCoded(pData, m_pRelease2);
		}
	}

// Attributes

BOOL CPrimImageToggle::IsAvail(void) const
{
	if( !IsItemAvail(m_pValue) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pEnable) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pPress1) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pRelease1) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pPress2) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pRelease2) ) {

		return FALSE;
		}

	return TRUE;
	}

// Overridables

void CPrimImageToggle::SetScan(UINT Code)
{
	SetItemScan(m_pValue, Code);
	
	SetItemScan(m_pEnable, Code);

	SetItemScan(m_pPress1, Code);

	SetItemScan(m_pRelease1, Code);

	SetItemScan(m_pPress2, Code);

	SetItemScan(m_pRelease2, Code);

	CPrimImageBtnBase::SetScan(Code);
	}

void CPrimImageToggle::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_DrawRect));
	}

UINT CPrimImageToggle::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgBlockRemote:

			return m_Local == 1;
		}

	return CPrimImageBtnBase::OnMessage(uMsg, uParam);
	}

// Message Handlers

UINT CPrimImageToggle::OnTakeFocus(UINT uMsg, UINT uParam)
{
	if( uParam == 0 ) {

		if( IsAvail() ) {

			if( m_pEnable && !m_pEnable->ExecVal() ) {

				return FALSE;
				}
			
			return TRUE;
			}

		return FALSE;
		}

	if( uParam == 1 ) {

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CPrimImageToggle::GetValue(void)
{
	if( m_pValue ) {

		m_uValue = m_pValue->Execute(typeInteger);
		}
	}

void CPrimImageToggle::SetValue(C3INT Data)
{
	if( m_pValue ) {

		m_pValue->SetValue(Data, typeInteger, setNone);
		}

	m_uValue = Data;
	}

UINT CPrimImageToggle::FindSeg(UINT uParam)
{
	if( m_Axis ) {

		int xPos = LOWORD(uParam);

		if( xPos < (m_DrawRect.x1 + m_DrawRect.x2) / 2 ) {

			return 0;
			}

		return 1;
		}

	int yPos = HIWORD(uParam);

	if( yPos < (m_DrawRect.y1 + m_DrawRect.y2) / 2 ) {

		return 1;
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image 2-State Toggle
//

// Constructor

CPrimImageToggle2::CPrimImageToggle2(void)
{
	m_ValA = 0;
	m_ValB = 1;
	}

// Destructor

CPrimImageToggle2::~CPrimImageToggle2(void)
{
	}

// Initialization

void CPrimImageToggle2::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimImageToggle2", pData);

	CPrimImageToggle::Load(pData);

	m_ValA = GetWord(pData);
	m_ValB = GetWord(pData);

	m_uValue = (m_Mode == 1) ? m_ValB : m_ValA;
	}

// Overridables

UINT CPrimImageToggle2::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTakeFocus:

			return OnTakeFocus(uMsg, uParam);

		case msgKillFocus:

			if( !m_Delay && m_fPress ) {

				OnTouchUp();

				return TRUE;
				}
			
			return FALSE;

		case msgTouchInit:
		case msgTouchDown:

			if( !m_fTime ) {

				m_uSeg  = FindSeg(uParam);

				BOOL rv = OnTouchDown();

				m_uTime = GetTickCount();

				return rv;
				}

			return 0;

		case msgTouchRepeat:

			return OnTouchRepeat();

		case msgTouchUp:

			if( SetDelayTimer(m_uTime, m_Delay) ) {

				m_fTime = TRUE;

				return 0;
				}

			return OnTouchUp();

		case msgTimer:

			m_fTime = FALSE;

			return OnTouchUp();

		case msgIsPressed:

			return m_fPress;
		}

	return CPrimImageToggle::OnMessage(uMsg, uParam);
	}

// Message Handlers

UINT CPrimImageToggle2::OnTouchDown(void)
{
	m_fPress = TRUE;

	if( CUISystem::m_pThis->m_pUI->Protect(this, m_Protect, stateDown) ) {

		switch( m_Mode ) {

			case 0:
			case 1:
			case 2:
				switch( (m_uInit = m_Ctx.m_uIndex) ) {

					case 0:
						if( m_uSeg == 1 ) {

							SetValue(m_ValB);
							}
						break;

					case 1:
						if( m_uSeg == 0 ) {

							SetValue(m_ValA);
							}
						break;
					}
				break;

			case 4:
				Execute(m_uSeg ? m_pPress2 : m_pPress1);
				
				break;
			}
		}

	return TRUE;
	}

UINT CPrimImageToggle2::OnTouchRepeat(void)
{
	return FALSE;
	}

UINT CPrimImageToggle2::OnTouchUp(void)
{
	m_fPress = FALSE;

	if( CUISystem::m_pThis->m_pUI->Protect(this, m_Protect, stateUp) ) {

		switch( m_Mode ) {

			case 1:
				if( m_uSeg == 0 && m_uInit == 1 ) {
				    
					SetValue(m_ValB);
					}
				break;

			case 2:
				if( m_uSeg == 1 && m_uInit == 0 ) {

					SetValue(m_ValA);
					}
				break;

			case 4:
				Execute(m_uSeg ? m_pRelease2 : m_pRelease1);
				
				break;
			}
		}

	return TRUE;
	}

// Image Selection

UINT CPrimImageToggle2::GetIndex(void)
{
	GetValue();

	if( m_uValue == m_ValA ) {

		return 0;
		}

	if( m_uValue == m_ValB ) {

		return 1;
		}

	return m_Default;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image 3-State Toggle
//

// Constructor

CPrimImageToggle3::CPrimImageToggle3(void)
{
	m_ValA = 0;
	m_ValB = 1;
	m_ValC = 2;
	}

// Destructor

CPrimImageToggle3::~CPrimImageToggle3(void)
{
	}

// Initialization

void CPrimImageToggle3::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimImageToggle3", pData);

	CPrimImageToggle::Load(pData);

	m_ValA = GetWord(pData);
	m_ValB = GetWord(pData);
	m_ValC = GetWord(pData);

	m_uValue = m_ValB;
	}

// Overridables

UINT CPrimImageToggle3::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTakeFocus:

			return OnTakeFocus(uMsg, uParam);

		case msgKillFocus:

			if( !m_Delay && m_fPress ) {

				OnTouchUp();

				return TRUE;
				}
			
			return FALSE;

		case msgTouchInit:
		case msgTouchDown:

			if( !m_fTime ) {

				m_uSeg  = FindSeg(uParam);

				BOOL rv = OnTouchDown();

				m_uTime = GetTickCount();

				return rv;
				}

			return 0;

		case msgTouchRepeat:

			return OnTouchRepeat();

		case msgTouchUp:

			if( SetDelayTimer(m_uTime, m_Delay) ) {

			//	TODO -- Why is this disabled?
			//
			//	m_fTime = TRUE;

				return 0;
				}

			return OnTouchUp();

		case msgTimer:

			m_fTime = FALSE;

			return OnTouchUp();

		case msgIsPressed:

			return m_fPress;
		}

	return CPrimImageToggle::OnMessage(uMsg, uParam);
	}

// Message Handlers

UINT CPrimImageToggle3::OnTouchDown(void)
{
	m_fPress = TRUE;

	if( CUISystem::m_pThis->m_pUI->Protect(this, m_Protect, stateDown) ) {

		switch( m_Mode ) {

			case 0:
			case 1:
			case 2:
			case 3:
				switch( (m_uInit = m_Ctx.m_uIndex) ) {

					case 0:
						if( m_uSeg == 1 ) {

							SetValue(m_ValB);
							}
						break;

					case 1:
						if( m_uSeg == 0 ) {

							SetValue(m_ValA);
							}
						else
							SetValue(m_ValC);
						break;

					case 2:
						if( m_uSeg == 0 ) {

							SetValue(m_ValB);
							}
						break;
					}
				break;

			case 4:
				Execute(m_uSeg ? m_pPress2 : m_pPress1);
				
				break;
			}
		}

	return TRUE;
	}

UINT CPrimImageToggle3::OnTouchRepeat(void)
{
	return FALSE;
	}

UINT CPrimImageToggle3::OnTouchUp(void)
{
	m_fPress = FALSE;

	if( CUISystem::m_pThis->m_pUI->Protect(this, m_Protect, stateUp) ) {

		switch( m_Mode ) {

			case 1:
				if( m_uInit == 1 && m_uSeg == 0 ) {

					SetValue(m_ValB);
					}
				break;

			case 2:
				if( m_uInit == 1 && m_uSeg == 1 ) {

					SetValue(m_ValB);
					}
				break;
			
			case 3:
				if( m_uInit == 1 ) {

					SetValue(m_ValB);
					}
				break;
			
			case 4:
				Execute(m_uSeg ? m_pRelease2 : m_pRelease1);

				break;
			}
		}

	return TRUE;
	}

// Image Selection

UINT CPrimImageToggle3::GetIndex(void)
{
	GetValue();

	if( m_uValue == m_ValA ) {

		return 0;
		}

	if( m_uValue == m_ValB ) {

		return 1;
		}

	if( m_uValue == m_ValC ) {

		return 2;
		}

	return m_Default;
	}

// End of File
