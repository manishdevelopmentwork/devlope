
#include "Intern.hpp"

#include "CryptoHmacSha256.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SHA256 Cryptographic HMAC
//

// Instantiator

static IUnknown * Create_CryptoHmacSha256(PCTXT pName)
{
	return New CCryptoHmacSha256;
	}

// Registration

global void Register_CryptoHmacSha256(void)
{
	piob->RegisterInstantiator("crypto.hmac-sha256", Create_CryptoHmacSha256);
	}

// Constructor

CCryptoHmacSha256::CCryptoHmacSha256(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;
	}

// ICryptoHmac

CString CCryptoHmacSha256::GetName(void)
{
	return "sha256";
	}

void CCryptoHmacSha256::Initialize(PCBYTE pPass, UINT uPass)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	psHmacSha256Init(&m_Ctx, pPass, uPass);

	m_uState = stateActive;
	}

void CCryptoHmacSha256::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	psHmacSha256Update(&m_Ctx, pData, uData);
	}

void CCryptoHmacSha256::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	psHmacSha256Final(&m_Ctx, m_pHash);

	m_uState = stateDone;
	}

// End of File
