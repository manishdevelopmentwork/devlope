
#include "Intern.hpp"

#include "UIIPAddr.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedHost.hpp"
#include "CommsSystem.hpp"
#include "NameServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- IP Address
//

// Dynamic Class

AfxImplementDynamicClass(CUIIPAddr, CUICategorizer);

// Constructor

CUIIPAddr::CUIIPAddr(void)
{
	m_cfCode = RegisterClipboardFormat(L"C3.1 Code Fragment");
	}

// Core Overridables

void CUIIPAddr::OnBind(void)
{
	m_pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CUICategorizer::OnBind();
	}

// Data Overridables

BOOL CUIIPAddr::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	if( CanAcceptText(pData, m_cfCode) ) {

		dwEffect = DROPEFFECT_LINK;

		return TRUE;
		}

	if( CanAcceptText(pData, CF_UNICODETEXT) ) {

		dwEffect = DROPEFFECT_COPY;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIIPAddr::OnAcceptData(IDataObject *pData)
{
	if( AcceptText(pData, m_cfCode) ) {

		SetBestFocus();

		return TRUE;
		}

	if( CUIControl::AcceptText(pData, CF_UNICODETEXT) ) {

		SetBestFocus();

		return TRUE;
		}

	return FALSE;
	}

// Category Overridables

BOOL CUIIPAddr::LoadModeButton(void)
{
	CModeButton::COption Opt;

	m_pModeCtrl->ClearOptions();

	if( TRUE ) {

		Opt.m_uID     = modeExpressionWas;
		Opt.m_Text    = CString(IDS_WAS);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = TRUE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = modeFixedIP;
		Opt.m_Text    = CString(IDS_FIXED_IP);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = modeFixedName;
		Opt.m_Text    = CString(IDS_FIXED_NAME);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = NOTHING;
		Opt.m_Text    = L"";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = modeDynamicName;
		Opt.m_Text    = CString(IDS_DYNAMIC_NAME);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = NOTHING;
		Opt.m_Text    = L"";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = modeExpression;
		Opt.m_Text    = CString(IDS_EXPRESSION);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	return TRUE;
	}

BOOL CUIIPAddr::SwitchMode(UINT uMode)
{
	if( uMode == modeFixedIP ) {

		m_pModeCtrl->SetData(m_uMode = uMode);

		StdSave(FALSE, FindDataText(L"0.0.0.0", m_uMode));

		ForceUpdate();

		return TRUE;
		}

	if( uMode == modeFixedName ) {

		m_pModeCtrl->SetData(m_uMode = uMode);

		LoadData(FindDataText(L"", m_uMode), TRUE);

		return TRUE;
		}

	if( uMode == modeDynamicName ) {

		if( m_uMode == modeFixedName ) {

			CString Text = m_pText->GetAsText();

			AfxAssert(Text.StartsWith(L"ResolveDNS("));

			Text = Text.Mid(wstrlen(L"ResolveDNS("));

			Text = Text.Left(Text.GetLength() - 1);

			Text = Text.Mid(1, Text.GetLength() - 2);

			if( !Text.IsEmpty() ) {

				CString Data = FindDataText(Text, uMode);

				if( StdSave(TRUE, Data) != saveError ) {

					m_pModeCtrl->SetData(m_uMode = uMode);

					ForceUpdate();

					return TRUE;
					}

				return FALSE;
				}
			}

		m_pModeCtrl->SetData(m_uMode = uMode);

		LoadData(FindDataText(L"", m_uMode), TRUE);

		return TRUE;
		}

	if( uMode == modeExpression ) {

		m_pModeCtrl->SetData(m_uMode = uMode);

		LoadData(m_pText->GetAsText(), TRUE);

		return TRUE;
		}

	return FALSE;
	}

UINT CUIIPAddr::FindDispMode(CString Text)
{
	if( Text.StartsWith(L"WAS ") ) {

		return modeExpressionWas;
		}

	if( Text.StartsWith(L"0x") ) {

		return modeFixedIP;
		}

	if( Text.StartsWith(L"ResolveDNS(") ) {

		Text = Text.Mid(wstrlen(L"ResolveDNS("));

		Text = Text.Left(Text.GetLength() - 1);

		if( Text[0] == '"' ) {

			if( Text[Text.GetLength() - 1] == '"' ) {

				return modeFixedName;
				}
			}

		return modeDynamicName;
		}

	CError Error(FALSE);

	ParseFixedIP(Error, Text);

	if( Error.IsOkay() )  {

		return modeFixedIP;
		}

	return modeExpression;
	}

CString CUIIPAddr::FindDispText(CString Text, UINT uMode)
{
	if( uMode == modeExpressionWas ) {

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);

			return Text;
			}
		}

	if( uMode == modeFixedIP ) {

		AfxAssert(Text.StartsWith(L"0x"));

		UINT uData = wcstoul(Text.Mid(2), NULL, 16);

		return FormatFixedIP(uData);
		}

	if( uMode == modeFixedName ) {

		AfxAssert(Text.StartsWith(L"ResolveDNS("));

		Text = Text.Mid(wstrlen(L"ResolveDNS("));

		Text = Text.Left(Text.GetLength() - 1);

		Text = Text.Mid(1, Text.GetLength() - 2);

		return Text;
		}

	if( uMode == modeDynamicName ) {

		AfxAssert(Text.StartsWith(L"ResolveDNS("));

		Text = Text.Mid(wstrlen(L"ResolveDNS("));

		Text = Text.Left(Text.GetLength() - 1);

		return Text;
		}

	if( uMode == modeExpression ) {

		if( Text.StartsWith(L"0x") ) {

			UINT uData = wcstoul(Text.Mid(2), NULL, 16);

			return CPrintf(L"0x%8.8X", uData);
			}

		CError Error(FALSE);

		UINT uData = ParseFixedIP(Error, Text);

		if( Error.IsOkay() )  {

			return CPrintf(L"0x%8.8X", uData);
			}
		}

	return Text;
	}

CString CUIIPAddr::FindDataText(CString Text, UINT uMode)
{
	if( uMode == modeExpressionWas ) {

		return L"WAS " + Text;
		}

	if( uMode == modeFixedIP ) {

		CError Error(FALSE);

		UINT uData = ParseFixedIP(Error, Text);

		if( Error.IsOkay() ) {

			return CPrintf(L"0x%8.8X", uData);
			}

		return L"!" + Text;
		}

	if( uMode == modeFixedName ) {

		CString Data;

		Data += L"ResolveDNS(";

		Data += "\"";

		Data += Text;

		Data += "\"";

		Data += L")";

		return Data;
		}

	if( uMode == modeDynamicName ) {

		CString Data;

		Data += L"ResolveDNS(";

		Data += Text;

		Data += L")";

		return Data;
		}

	if( uMode == modeExpression ) {

		CError Error(FALSE);

		UINT uData = ParseFixedIP(Error, Text);

		if( Error.IsOkay() ) {

			return CPrintf(L"0x%8.8X", uData);
			}

		if( Text.StartsWith(L"ResolveDNS(") ) {

			if( Text.EndsWith(L")") ) {

				return Text;
				}
			}

		// NOTE -- this traps a partially formed Fixed IP

		CStringArray List;

		Text.Tokenize(List, '.');

		for( UINT n = 0; n < List.GetCount(); n ++ ) {

			PTXT p1 = NULL;

			// cppcheck-suppress ignoredReturnValue

			wcstoul(List[n],&p1,10);

			if( !*p1 ) {

				return L"0x0";
				}
			}
		}

	return Text;
	}

UINT CUIIPAddr::FindDispType(UINT uMode)
{
	if( uMode == modeExpressionWas ) {

		return 1;
		}

	return 2;
	}

// Implementation

CString CUIIPAddr::FormatFixedIP(UINT uData)
{
	BYTE b1 = PBYTE(&uData)[0];
	BYTE b2 = PBYTE(&uData)[1];
	BYTE b3 = PBYTE(&uData)[2];
	BYTE b4 = PBYTE(&uData)[3];

	return CPrintf(L"%u.%u.%u.%u", b4, b3, b2, b1);
	}

UINT CUIIPAddr::ParseFixedIP(CError &Error, CString Text)
{
	CStringArray List;

	Text.Tokenize(List, '.');

	if( List.GetCount() == 4 ) {

		PTXT p1 = NULL;
		PTXT p2 = NULL;
		PTXT p3 = NULL;
		PTXT p4 = NULL;

		UINT u1 = wcstoul(List[0],&p1,10);
		UINT u2 = wcstoul(List[1],&p2,10);
		UINT u3 = wcstoul(List[2],&p3,10);
		UINT u4 = wcstoul(List[3],&p4,10);

		if( !*p1 && !*p2 && !*p3 && !*p4 ) {

			if( u1 < 256 && u2 < 256 && u3 < 256 && u4 < 256 ) {

				UINT uData = 0;

				((PBYTE) &uData)[0] = BYTE(u4);
				((PBYTE) &uData)[1] = BYTE(u3);
				((PBYTE) &uData)[2] = BYTE(u2);
				((PBYTE) &uData)[3] = BYTE(u1);

				return uData;
				}
			}
		}

	Error.Set(CString(IDS_INVALID_IP));

	return 0;
	}

BOOL CUIIPAddr::AcceptText(IDataObject *pData, UINT cfText)
{
	FORMATETC Fmt = { WORD(cfText), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		if( IsStringType(Text) ) {

			Text = FindDataText(Text, modeDynamicName);
			}

		CError Error = CError(TRUE);

		UINT   uCode = m_pText->SetAsText(Error, Text);

		if( uCode == saveError ) {

			Error.Show(*afxMainWnd);

			return TRUE;
			}

		if( uCode == saveChange ) {

			LoadUI();

			ForceUpdate();

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUIIPAddr::GetDataType(CString Text, CTypeDef &Type)
{
	CNameServer *pName  = m_pSystem->GetNameServer();

	INameServer *pFind  = pName;

	CError      *pError = New CError(FALSE);

	WORD         ID     = 0;

	pName->ResetServer();

	if( m_pItem->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

		CCodedHost *pHost = (CCodedHost *) m_pItem;

		pFind             = pHost->GetNameServer(pName);
		}

	if( pFind->FindIdent(pError, Text, ID, Type) ) {

		delete pError;

		return TRUE;
		}

	UINT uCount;

	Text = Text.Left(Text.Find('('));

	if( pFind->FindFunction(pError, Text, ID, Type, uCount) ) {

		delete pError;

		return TRUE;
		}

	delete pError;

	return FALSE;
	}

BOOL CUIIPAddr::IsStringType(CString Text)
{
	CTypeDef     Type   = { 0, 0 };

	if( GetDataType(Text, Type) ) {

		if( Type.m_Type == typeString ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
