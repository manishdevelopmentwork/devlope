
#include "Intern.hpp"

#include "ThreadObject.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Thread Object
//

IEvent * e1;
IEvent * e2;
IEvent * e3;

// Constructor

CThreadObject::CThreadObject(UINT n)
{
	if( n == 0 ) {

		e1 = Create_AutoEvent();
		e2 = Create_AutoEvent();
		e3 = Create_AutoEvent();
		}

	m_n = n;

	StdSetRef();
	}

// Destructor

CThreadObject::~CThreadObject(void)
{
	}

// IUnknown

HRESULT CThreadObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IDiagProvider);

	StdQueryInterface(ISdHost);

	return E_NOINTERFACE;
	}

ULONG CThreadObject::AddRef(void)
{
	StdAddRef();
	}

ULONG CThreadObject::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CThreadObject::TaskInit(UINT uTask)
{
	SetThreadName(CPrintf("task%u", m_n));

	AfxTrace("task%u: init start\n", m_n);

	Sleep(500 + m_n * 100);

	AfxTrace("task%u: init done\n", m_n);

	return TRUE;
	}

INT CThreadObject::TaskExec(UINT uTask)
{
	AfxTrace("task%u: exec start\n", m_n);

	Sleep(1000);

	DWORD d = 0;

	for(;;) {

		if( uTask < 2 ) {

			FloatStuff(uTask, d);
			}

		Sleep(5 + rand() % 10);
		}

	return 0;
	}

void CThreadObject::TaskStop(UINT uTask)
{
	AfxTrace("task%u: stop start\n", m_n);

	Sleep(500 + m_n * 100);

	AfxTrace("task%u: stop done\n", m_n);
	}

void CThreadObject::TaskTerm(UINT uTask)
{
	AfxTrace("task%u: term start\n", m_n);

	Sleep(500 + m_n * 100);

	AfxTrace("task%u: term done\n", m_n);
	}

// Implementation

void CThreadObject::FloatStuff(UINT uTask, DWORD &d)
{
//	DWORD t = GetTickCount();
			
	for( int n = 0; n < (uTask ? 100 : 5000); n++ ) {

		CRubyMatrix m1, m2, m3;

		m1.SetScaleFactor(2.0, 2.0);

		m2.SetRotation(45);

		m3.Compose(m1);

		m3.Compose(m2);

		char s[128];

		sprintf(s, "sqrt(det(m3)) = %f and %f\n", m3.GetScale(), m3.GetInverseScale());
		}

	if( uTask == 0 ) {

		if( !(d++ % 20) ) {

//			printf("%u\n", GetTickCount() - t);
					
			d = 0;
			}
		}
	}

// End of File
