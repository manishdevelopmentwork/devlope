
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_OptionCardPage_HPP

#define INCLUDE_OptionCardPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class COptionCardItem;

//////////////////////////////////////////////////////////////////////////
//
// Option Card Page
//

class COptionCardPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		COptionCardPage(COptionCardItem *pOpt);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		COptionCardItem * m_pOpt;

		// Implementation
		BOOL LoadBasePage(IUICreate *pView);
		BOOL LoadButtons(IUICreate *pView);
	};

// End of File

#endif
