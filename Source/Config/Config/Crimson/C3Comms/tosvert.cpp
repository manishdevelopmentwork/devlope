
#include "intern.hpp"

#include "tosvert.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Toshiba G3 Tosvert-130 Inverter Serial Device Options
//
//
// Dynamic Class

AfxImplementDynamicClass(CTosvertMasterSerialDeviceOptions, CUIItem);

// Constructor

CTosvertMasterSerialDeviceOptions::CTosvertMasterSerialDeviceOptions(void)
{
	m_Drop 	= 1;
	}

// Download Support	     

BOOL CTosvertMasterSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CTosvertMasterSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	} 

//////////////////////////////////////////////////////////////////////////
//
// Toshiba G3 Tosvert-130 Inverter Master Driver
//

// Instantiator

ICommsDriver *Create_TosvertMasterDriver(void)
{
	return New CTosvertMasterDriver;
	}

// Constructor

CTosvertMasterDriver::CTosvertMasterDriver(void)
{
	m_wID		= 0x3363;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Toshiba";
	
	m_DriverName	= "Tosvert Inverter";
	
	m_Version	= "BETA";
	
	m_ShortName	= "Tosvert";

	AddSpaces();
	}


// Binding Control

UINT CTosvertMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CTosvertMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}


 // Implementation

void CTosvertMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace('A',	"RAM",	"RAM Data",		10, 0, 999,	addrByteAsWord));
	AddSpace(New CSpace('P',	"EEPR",	"EEPROM Data",		10, 0, 999,	addrByteAsWord));
	AddSpace(New CSpace('I',	"INTR",	"Internal ROM Data",	10, 0, 999,	addrByteAsWord));
	AddSpace(New CSpace('E',	"EXTR",	"External ROM Data",	10, 0, 999,	addrByteAsWord));
	AddSpace(New CSpace('O',	"OPT",	"Optional Bus Data",	10, 0, 999,	addrByteAsWord));
	AddSpace(New CSpace('M',	"MISC",	"Misc Data",		10, 0, 999,	addrWordAsWord));
	}

BOOL CTosvertMasterDriver::CheckAlignment(CSpace * pSpace)
{
	return TRUE;
	}


// End of File
