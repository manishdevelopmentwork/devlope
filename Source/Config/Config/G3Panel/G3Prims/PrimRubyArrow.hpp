
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyArrow_HPP
	
#define	INCLUDE_PrimRubyArrow_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyOriented.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Arrow Primitive
//

class CPrimRubyArrow : public CPrimRubyOriented
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyArrow(void);

		// Overridables
		void UpdateLayout(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);

	protected:
		// Data Members
		INT m_Min1;
		INT m_Max1;
		INT m_Pos1;
		INT m_Min2;
		INT m_Max2;
		INT m_Pos2;

		// Meta Data
		void AddMetaData(void);

		// Path Generation
		void MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2);
	};

// End of File

#endif
