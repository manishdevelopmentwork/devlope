
#include "Intern.hpp"

#include "SdHost437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 SD Host Controller
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

#define Sys(x) (m_pSys [reg##x])

// Instantiator

IDevice * Create_SdHost437(CClock437 *pClock)
{
	CSdHost437 *pDevice = New CSdHost437(pClock);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CSdHost437::CSdHost437(CClock437 *pClock)
{
	m_pSys	   = PVDWORD(ADDR_MMCSD0);
		
	m_pBase    = PVDWORD(ADDR_MMCSD0 + 0x200);
	
	m_uLine	   = INT_MMCSD0;
		
	m_uClock   = pClock->GetPerM2Freq() / 2;

	m_uDetect  = 24;

	m_uVoltage = Bit(20); 

	AfxGetObject("gpio", 4, IGpio, m_pGpio);

	m_pTimer    = CreateTimer();

	m_pCommand  = Create_AutoEvent();
	
	m_pTransfer = Create_AutoEvent();
	
	m_pWrite    = Create_AutoEvent();
	
	m_pRead     = Create_AutoEvent();
	
	m_pFailCmd  = Create_AutoEvent();
	
	m_pFailData = Create_AutoEvent();
	}

// Destructor

CSdHost437::~CSdHost437(void)
{
	m_pGpio->Release();

	m_pTimer->Release();

	m_pCommand->Release();

	m_pTransfer->Release();

	m_pWrite->Release();

	m_pRead->Release();

	m_pFailCmd->Release();

	m_pFailData->Release();
	}

// IDevice

BOOL METHOD CSdHost437::Open(void)
{
	if( CSdHostGeneric::Open() ) {

		ResetController();

		EnableTimer();

		return TRUE;
		}

	return FALSE;
	}

// IEventSink

void CSdHost437::OnEvent(UINT uLine, UINT uParam)
{
	if( uParam == 0 ) {

		if( Reg(IrqStat) & irqFailCmd ) {

			m_pFailCmd->Set();

			Reg(IrqStat) = irqCommand;

			Reg(IrqStat) = irqFailCmd;
			}

		if( Reg(IrqStat) & irqFailData ) {

			m_pFailData->Set();

			Reg(IrqStat) = irqTransfer | irqWrite | irqRead;

			Reg(IrqStat) = irqFailData;
			}

		if( Reg(IrqStat) & irqCommand ) {
		
			m_pCommand->Set();

			Reg(IrqStat) = irqCommand;
			}

		if( Reg(IrqStat) & irqTransfer ) {

			m_pTransfer->Set();

			Reg(IrqStat) = irqTransfer;
			}

		if( Reg(IrqStat) & Reg(IrqLineEn) & irqWrite ) {
		
			Reg(IrqLineEn) &= ~irqWrite;

			Reg(IrqStat)    = irqWrite;

			m_pWrite->Set();
			}

		if( Reg(IrqStat) & Reg(IrqLineEn) & irqRead ) {
		
			Reg(IrqLineEn) &= ~irqRead;

			Reg(IrqStat)    = irqRead;

			m_pRead->Set();
			}
		}

	if( uParam == 1 ) {

		switch( m_Card ) {
			
			case cardNone:

				if( !m_pGpio->GetState(m_uDetect) ) {

					InitController();

					m_uBounce = 10;

					m_Card    = cardBounce;
					}

				break;

			case cardBounce:

				if( !m_pGpio->GetState(m_uDetect) ) {

					if( !--m_uBounce ) {

						SetClockSpeed(clockSlow);

						m_Card = cardInserted;
						
						FireCardEvent();
						}

					break;
					}

				m_Card = cardNone;

				break;

			default:
				if( m_pGpio->GetState(m_uDetect) ) {

					StopController();

					m_Card = cardNone;

					FireCardEvent();
					}
				
				break;
			}
		}
	}

// Controller Interface

void CSdHost437::ResetController(void)
{
	HardReset();
	}

void CSdHost437::InitController(void)
{
	SoftReset();

	Reg(IrqStatEn)  = irqAll;

	Reg(IrqStat  )  = irqAll;

	Reg(IrqLineEn)  = irqAll;

	Reg(ProCtl   )  = 0x00000E00;

	Reg(SysCtl   )  = 0x000E0000;

	Reg(HostCaps )  = Bit(26) | Bit(24) | Bit(21);

	Reg(ProCtl   ) |= Bit(8);
		
	ClearEvents();

	EnableEvents();
	}

void CSdHost437::StopController(void)
{
	DisableEvents();

	SoftReset();
	}

void CSdHost437::SetClockSpeed(EClock Clock)
{
	DisableExtClock();

	DWORD dwReg = Reg(SysCtl);

	UINT  uFreq = 400000;

	bool  fFast = false;

	switch( Clock ) {

		case clockSlow:

			uFreq = 400000;
			
			fFast = false;
			
			break;

		case clock25MHz:

			uFreq = 25000000;
			
			fFast = false;
			
			break;

		case clock50MHz:

			uFreq = 50000000;
			
			fFast = true;
			
			break;
		}

	UINT uWait = 1000000 / uFreq;

	UINT uDiv  = Max(m_uClock / uFreq, UINT(1));

	while( m_uClock / uDiv > uFreq ) {

		uDiv ++;
		}

	dwReg &= ~(0x3FF << 6);

	dwReg |=  (uDiv  << 6);

	Reg(SysCtl) = dwReg;

	EnableIntClock();

	EnableExtClock();

	phal->SpinDelayFast(uWait);
	
	SetBusWidth(fFast);
	}

void CSdHost437::SetBusWidth(bool fWide)
{
	if( fWide ) {

		AtomicBitSet(&Reg(ProCtl), 1);
		}
	else {
		AtomicBitClr(&Reg(ProCtl), 1);
		}
	}

void CSdHost437::SetLength(UINT uBytes)
{
	Reg(BlkAttr) = uBytes;
	}

// Blocking Calls

void CSdHost437::ClearEvents(void)
{
	m_pCommand ->Clear();

	m_pTransfer->Clear();

	m_pWrite   ->Set();

	m_pRead    ->Clear();

	m_pFailCmd ->Clear();

	m_pFailData->Clear();
	
	m_fBusy = false;
	}

bool CSdHost437::WaitCmdComplete(void)
{
	return WaitMultiple(m_pCommand, m_pFailCmd, 50) == 1;
	}

bool CSdHost437::SendData(PCBYTE pData, UINT uCount)
{
	if( uCount % 4 == 0 ) {

		UINT nSend = 0;

		while( uCount ) {

			UINT uWait = WaitMultiple(m_pWrite, m_pFailData, 50);

			if( uWait == 1 ) {

				while( (Reg(State) & Bit(10) ) && uCount ) {

					Reg(DatPort) = ((PDWORD) pData)[nSend++];

					uCount -= 4;
					}
				
				Reg(IrqStat)    = irqWrite;
			
				Reg(IrqLineEn) |= irqWrite;

				continue;
				}

			return false;
			}

		return true;
		}

	return false;
	}

bool CSdHost437::ReadData(PBYTE pData, UINT uCount)
{
	if( uCount % 4 == 0 ) {

		UINT uRecv = 0;

		while( uCount ) {

			UINT uWait = WaitMultiple(m_pRead, m_pFailData, 50);

			if( uWait == 1 ) {

				while( (Reg(State) & Bit(11)) && uCount ) {

					((PDWORD) pData)[uRecv++] = Reg(DatPort);
					
					uCount -= 4;
					}
							
				Reg(IrqStat)    = irqRead;

				Reg(IrqLineEn) |= irqRead;
		
				continue;
				}

			return false;
			}

		return true;
		}
		
	return false;
	}

bool CSdHost437::CheckBusy(void)
{
	if( m_fBusy ) {

		UINT uWait = WaitMultiple(m_pTransfer, m_pFailData, 1000);

		m_fBusy	= false;

		while( Reg(State) & (Bit(0) | Bit(1)) ) {

			Sleep(5);
			}

		return uWait == 1;
		}

	return true;
	}

// Implementation

void CSdHost437::EnableTimer(void)
{
	m_pTimer->SetPeriod(50);

	m_pTimer->SetHook(this, 1);

	m_pTimer->Enable(true);
	}

void CSdHost437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
	}

void CSdHost437::DisableEvents(void)
{
	phal->EnableLine(m_uLine, false);
	}

void CSdHost437::HardReset(void)
{
	Sys(Config) |= Bit(1);

	while( !(Sys(Status) & Bit(0)) );
	}

void CSdHost437::SoftReset(void)
{
	Reg(SysCtl) = Bit(24);

	phal->SpinDelayFast(1);

	while( (Reg(SysCtl) & Bit(24)) );
	}

void CSdHost437::EnableIntClock(void)
{
	Reg(SysCtl) |= Bit(0);

	while( !(Reg(SysCtl) & Bit(1)) );
	}

void CSdHost437::EnableExtClock(void)
{
	Reg(SysCtl) |= Bit(2);
	}

void CSdHost437::DisableExtClock(void)
{
	Reg(SysCtl) &= ~Bit(2);
	}

// End of File
