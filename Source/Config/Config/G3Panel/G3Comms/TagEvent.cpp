
#include "Intern.hpp"

#include "TagEvent.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Tag Event
//

// Dynamic Class

AfxImplementDynamicClass(CTagEvent, CCodedHost);

// Constructor

CTagEvent::CTagEvent(void)
{
	m_Mode      = 0;
	m_pLabel    = NULL;
	m_Trigger   = 0;
	m_Delay	    = 0;
	m_Accept    = 0;
	m_Priority  = 1;
	m_Print	    = FALSE;
	m_Siren	    = FALSE;
	m_Mail      = NOTHING;
	m_pOnActive = NULL;
	m_pOnClear  = NULL;
	m_pOnAccept = NULL;
	m_pOnEvent  = NULL;
	m_pEnable   = NULL;
	}

// UI Update

void CTagEvent::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Trigger" ) {

		if( m_Trigger ) {

			if( m_Accept ) {

				m_Accept = 0;

				pHost->UpdateUI(this, "Accept");
				}
			}

		DoEnables(pHost);
		}

	if( Tag == "Mode" ) {

		if( FindLabel() == DWORD(wstrdup(L"")) ) {

			pHost->UpdateUI(this, "Label");
			}

		DoEnables(pHost);
		}

	if( Tag == "Accept" ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CTagEvent::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Label" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "SubIndex" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "TagIndex" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.StartsWith(L"On") ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	return FALSE;
	}

// Searching

void CTagEvent::FindActive(CStringArray &List)
{
	if( m_Mode ) {

		CString Text;

		Text += GetHumanPath();

		Text += L" - ";

		Text += ((CUIItem *) GetParent())->GetSubItemLabel(this);

		Text += L" (";

		Text += GetPropAsText(L"Label");

		Text += L", ";

		Text += GetPropAsText(L"Mode");

		if( FindMetaData(L"Value") ) {

			Text += L", ";

			Text += GetPropAsText(L"Value");
			}

		Text += L")\n";

		Text += GetFixedPath();

		Text += L':';

		Text += L"Mode";

		List.Append(Text);
		}
	}

// Download Support

BOOL CTagEvent::MakeInitData(CInitData &Init)
{
	if( m_Mode ) {

		Init.AddByte(1);

		CCodedHost::MakeInitData(Init);

		Init.AddByte(BYTE(m_Mode));

		Init.AddByte(BYTE(m_pLabel ? m_pLabel->IsConst() : TRUE));

		Init.AddItem(itemVirtual, m_pLabel);

		Init.AddByte(BYTE(m_Trigger));
		Init.AddWord(WORD(m_Delay));
		Init.AddByte(BYTE(m_Accept));
		Init.AddByte(BYTE(m_Priority));
		Init.AddByte(BYTE(m_Print));
		Init.AddByte(BYTE(m_Siren));
		Init.AddWord(WORD(m_Mail));

		Init.AddItem(itemVirtual, m_pOnActive);
		Init.AddItem(itemVirtual, m_pOnClear);
		Init.AddItem(itemVirtual, m_pOnAccept);
		Init.AddItem(itemVirtual, m_pOnEvent);

		Init.AddItem(itemVirtual, m_pEnable);

		return TRUE;
		}

	Init.AddByte(0);

	return FALSE;
	}

// Meta Data

void CTagEvent::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddVirtual(Label);
	Meta_AddInteger(Trigger);
	Meta_AddInteger(Delay);
	Meta_AddInteger(Accept);
	Meta_AddInteger(Priority);
	Meta_AddInteger(Print);
	Meta_AddInteger(Siren);
	Meta_AddInteger(Mail);
	Meta_AddVirtual(OnActive);
	Meta_AddVirtual(OnClear);
	Meta_AddVirtual(OnAccept);
	Meta_AddVirtual(OnEvent);
	Meta_AddVirtual(Enable);

	Meta_SetName((IDS_TAG_EVENT));
	}

// Implementation

void CTagEvent::DoEnables(IUIHost *pHost)
{
	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		pHost->ShowUI(this, "Mail", TRUE);

		pHost->EnableUI(this, "Mail", m_Mode > 0);
	}
	else {
		pHost->ShowUI  (this, "Mail", FALSE);

		pHost->EnableUI(this, "Mail", FALSE);
		}

	pHost->EnableUI(this, "Label",    m_Mode > 0);
	pHost->EnableUI(this, "Enable",   m_Mode > 0);
	pHost->EnableUI(this, "Trigger",  m_Mode > 0);
	pHost->EnableUI(this, "Print",    m_Mode > 0);
	pHost->EnableUI(this, "Delay",    m_Mode > 0);

	pHost->EnableUI(this, "Accept",   m_Mode && m_Trigger < 1);
	pHost->EnableUI(this, "Priority", m_Mode && m_Trigger < 2);
	pHost->EnableUI(this, "Siren",    m_Mode && m_Trigger < 2);

	// LATER -- Is OnClear always possible???

	pHost->EnableUI(this, "OnActive", m_Mode > 0 && m_Trigger < 2);
	pHost->EnableUI(this, "OnClear",  m_Mode > 0 && m_Trigger < 1);
	pHost->EnableUI(this, "OnAccept", m_Mode > 0 && m_Trigger < 2 && !m_Accept);
	pHost->EnableUI(this, "OnEvent",  m_Mode > 0 && m_Trigger == 2);
	}

// Property Access

DWORD CTagEvent::FindLabel(void)
{
	// LATER -- This is a little dangerous as it is possible
	// that we have a circular reference in the label that
	// uses the tag property syntax to reference itself!!!

	if( m_pLabel ) {

		return m_pLabel->ExecVal();
		}

	return DWORD(wstrdup(L""));
	}

// End of File
