
//////////////////////////////////////////////////////////////////////////
//
// Raw Serial Driver
//

class CRawSerialDriver : public CRawPortDriver
{
	public:
		// Constructor
		CRawSerialDriver(void);

		// Destructor
		~CRawSerialDriver(void);

		// Management
		void MCALL Attach(IPortObject *pPort);

		// Configuration
		void MCALL Load(LPCBYTE pData);
		void MCALL CheckConfig(CSerialConfig &Config);

		// Operations
		void MCALL SetRTS(BOOL fState);
		BOOL MCALL GetCTS(void);

	protected:
		// Data Members
		IPortObject *m_pPort;
		BYTE	     m_bMode;
		WORD	     m_wBuffer;
	};

// End of File
