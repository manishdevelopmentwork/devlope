
#include "intern.hpp"

#include "PrimRubyConicalTank.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyGeom.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Conical Tank Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyConicalTank, CPrimRubyGeom);

// Constructor

CPrimRubyConicalTank::CPrimRubyConicalTank(void)
{
	m_Base = 8;

	m_Flat = 8;
	}

// Overridables

void CPrimRubyConicalTank::FindTextRect(void)
{
	int nAdjust   = m_pEdge->GetWidth();

	m_TextRect.x1 = m_DrawRect.x1 + nAdjust;
	
	m_TextRect.y1 = m_DrawRect.y1 + nAdjust;
	
	m_TextRect.x2 = m_DrawRect.x2 - nAdjust;
	
	m_TextRect.y2 = m_DrawRect.y2 - nAdjust - m_Base;

	//AdjustTextRect(); !!!!
	}

BOOL CPrimRubyConicalTank::GetHand(UINT uHand, CPrimHand &Hand)
{
	if( uHand == 0 ) {

		int cy = m_DrawRect.y2 - m_DrawRect.y1;

		Hand.m_Pos.x = 0;

		Hand.m_Pos.y = m_Base;

		Hand.m_pTag  = L"Base";

		Hand.m_uRef  = 3;

		Hand.m_Clip  = CRect(0, 8, 0, cy - 8);

		return TRUE;
		}

	if( uHand == 1 ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1;
	
		Hand.m_Pos.x = m_Flat;

		Hand.m_Pos.y = 0;

		Hand.m_pTag  = L"Flat";

		Hand.m_uRef  = 3;

		Hand.m_Clip  = CRect(0, 0, cx / 2, 0);

		return TRUE;
		}

	return FALSE;
	}

void CPrimRubyConicalTank::SetHand(BOOL fInit)
{
	if( fInit ) {

		int cx = m_DrawRect.y2 - m_DrawRect.y1;

		m_Base = 1 * cx / 3;

		m_Flat = 1 * cx / 4;
		}

	CPrimRubyGeom::SetHand(fInit);
	}

// Meta Data

void CPrimRubyConicalTank::AddMetaData(void)
{
	CPrimRubyGeom::AddMetaData();

	Meta_AddInteger(Base);
	Meta_AddInteger(Flat);

	Meta_SetName(IDS_CONICAL_TANK_2);
	}

// Path Management

void CPrimRubyConicalTank::MakePaths(void)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	CRubyPoint p1(Rect, 1);
	
	CRubyPoint p2(Rect, 4);

	number yBase = p2.m_y - min(m_Base, p2.m_y - 8);

	m_pathFill.Append(p1.m_x, p1.m_y);

	m_pathFill.Append(p2.m_x, p1.m_y);

	m_pathFill.Append(p2.m_x, yBase);

	m_pathFill.Append(p2.m_x - m_Flat, p2.m_y);

	m_pathFill.Append(p1.m_x + m_Flat, p2.m_y);

	m_pathFill.Append(p1.m_x, yBase);

	m_pathFill.AppendHardBreak();

	CPrimRubyGeom::MakePaths();
	}

// End of File
