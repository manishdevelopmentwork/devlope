
#include "intern.hpp"

#include "DAMix2DigitalInputConfigWnd.hpp"

#include "DAMix2DigitalInputConfig.hpp"

#include "DAMix2Module.hpp"

#include "DAMixDigitalConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Input Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix2DigitalInputConfigWnd, CUIViewWnd);

// Overibables

void CDAMix2DigitalInputConfigWnd::OnAttach(void)
{
	m_pItem   = (CDAMix2DigitalInputConfig *) CViewWnd::m_pItem;

	m_pModule = (CDAMix2Module *) m_pItem->GetParent(AfxRuntimeClass(CDAMix2Module));

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("damix_di_cfg"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAMix2DigitalInputConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddInputs();

	EndPage(FALSE);
}

void CDAMix2DigitalInputConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(1);
		DoEnables(2);
		DoEnables(3);
		DoEnables(4);
		DoEnables(5);
		DoEnables(6);
		DoEnables(7);
		DoEnables(8);
	}
}

// Implementation

void CDAMix2DigitalInputConfigWnd::AddInputs(void)
{
	StartTable(CString(IDS_MODULE_INPUTS), 2);

	AddColHead(IDS("Input Mode"));

	AddColHead(IDS("Source/Sink Mode"));

	for( UINT n = 0; n < 8; n++ ) {

		AddRowHead(CPrintf(IDS_MODULE_INPUT, n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("Mode%d", n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("Pull%d", n+1));
	}

	EndTable();
}

void CDAMix2DigitalInputConfigWnd::DoEnables(UINT uIndex)
{
	BOOL fMode = GetInteger(m_pModule->m_pDigitalConfig, CPrintf(L"ChanMode%u", uIndex));

	EnableUI(CPrintf(L"Mode%u", uIndex), fMode == 1);

	EnableUI(CPrintf(L"Pull%u", uIndex), fMode == 1);
}

// Data Access

UINT CDAMix2DigitalInputConfigWnd::GetInteger(CMetaItem *pItem, CString Tag)
{
	CMetaData const *pData = pItem->FindMetaData(Tag);

	return pData->ReadInteger(pItem);
}

// End of File
