
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ExecTimer_HPP
	
#define	INCLUDE_ExecTimer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BaseExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Timer Implementation 
//

class CExecTimer : public ITimer
{
	public:
		// Constuctor
		CExecTimer(CBaseExecutive *pExec);

		// Destructor
		~CExecTimer(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ITimer
		void METHOD Enable(bool fEnable);
		void METHOD SetPeriod(UINT uPeriod);
		bool METHOD SetHook(IEventSink *pSink, UINT uParam);

		// Operations
		void OnTick(UINT uTicks);

		// Linked List
		CExecTimer * m_pNext;
		CExecTimer * m_pPrev;

	protected:
		// Data Members
		ULONG            m_uRefs;
		CBaseExecutive * m_pExec;
		bool	         m_fEnable;
		UINT             m_uPeriod;
		IEventSink     * m_pSink;
		UINT             m_uParam;
		UINT             m_uTimer;
	};

// End of File

#endif
