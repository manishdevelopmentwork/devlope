
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCDMSG_HPP
	
#define	INCLUDE_PCDMSG_HPP

//////////////////////////////////////////////////////////////////////////
//
// Message Dispatch Macros
//

#define	On_WM_SETCURRENT(n)	DPM(n,(void(CTP)(BOOL))&OnSetCurrent,"V-1L")
#define	On_WM_UPDATEUI(n)	DPM(n,(void(CTP)(void))&OnUpdateUI,"V-")
#define	On_WM_LOADMENU(n)	DPM(n,(void(CTP)(UINT,CMenu&))&OnLoadMenu,"Z-1L2L")
#define	On_WM_LOADTOOL(n)	DPM(n,(void(CTP)(UINT,CMenu&))&OnLoadTool,"Z-1L2L")
#define	On_WM_SHOWUI(n)		DPM(n,(void(CTP)(BOOL))&OnShowUI,"V-1L")
#define On_WM_MAINMOVED(n)	DPM(n,(void(CTP)(CRect const *pRect))&OnMainMoved,"V-1L")

// End of File

#endif
