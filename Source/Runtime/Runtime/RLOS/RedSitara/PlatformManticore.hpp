
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformManticore_HPP

#define INCLUDE_PlatformManticore_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPwm437;
class CPru437;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PlatformSitara.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Manticore
//

class CPlatformManticore : public CPlatformSitara, 
			   public IPortSwitch, 
			   public ILeds, 
			   public IInputSwitch, 
			   public IUsbSystem,
			   public IUsbSystemPortMapper,
			   public IUsbSystemPower
{
	public:
		// Constructor
		CPlatformManticore(UINT uModel);

		// Destructor
		~CPlatformManticore(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IPlatform
		PCTXT METHOD GetModel(void);
		PCTXT METHOD GetVariant(void);
		UINT  METHOD GetFeatures(void);

		// ILeds
		void METHOD SetLed(UINT uLed, UINT uState);
		void METHOD SetLedLevel(UINT uPercent, bool fPersist);
		UINT METHOD GetLedLevel(void);

		// IInputSwitch
		UINT METHOD GetSwitches(void);
		UINT METHOD GetSwitch(UINT uSwitch);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Usb Power
		enum
		{
			powerLow,
			powerMed,
			powerHigh
			};

		// Static Data
		static const UINT m_uPorts[8][4];

		// Data Members
		CPwm437 * m_pPwm[4];
		CPru437 * m_pPru[2];
		int	  m_nLedLevel;
		UINT      m_uSerial;

		// Events
		void OnTempAlert(void);

		// IPortSwitch
		UINT METHOD GetCount(UINT uUnit);
		UINT METHOD GetMask(UINT uUnit);
		UINT METHOD GetType(UINT uUnit, UINT uLog);
		BOOL METHOD EnablePort(UINT uUnit, BOOL fEnable);
		BOOL METHOD SetPhysical(UINT uUnit, BOOL fRS485);
		BOOL METHOD SetFull(UINT uUnit, BOOL fFull);
		BOOL METHOD SetMode(UINT uUnit, BOOL fAuto);

		// IUsbSystem
		void METHOD OnDeviceArrival(IUsbHostFuncDriver *pDriver);
		void METHOD OnDeviceRemoval(IUsbHostFuncDriver *pDriver);
		
		// IUsbSystemPortMapper
		UINT METHOD GetPortCount(void);
		UINT METHOD GetPortType(UsbTreePath const &Path);
		UINT METHOD GetPortReset(UsbTreePath const &Path);

		// IUsbSystemPower
		void METHOD OnNewDevice(UsbTreePath const &Path, UINT uPower);
		void METHOD OnDelDevice(UsbTreePath const &Path, UINT uPower);

		// Inititialisation
		void InitClocks(void);
		void InitMux(void);
		void InitGpio(void);
		void InitMisc(void);
		void InitPru(void);
		void InitUarts(void);
		void InitUsb(void);
		void InitRack(void);
		void InitBackplane(void);
		void InitLeds(void);

		// Implementation
		void SetHostPower(UINT uPower);
		void SetSledReset(UINT uSled, bool fReset); 
	};

// End of File

#endif
