
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IIdentity_HPP

#define INCLUDE_IIdentity_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 17 -- Identity and Personality Objects
//

interface IIdentity;

//////////////////////////////////////////////////////////////////////////
//
// Platform Identity 
//

interface IIdentity : public IDevice
{
	// LINK

	AfxDeclareIID(17, 1);

	virtual BOOL METHOD Save(void)						= 0;
	virtual BOOL METHOD Load(void)						= 0;
	virtual BOOL METHOD Import(PCBYTE pData, UINT uSize)			= 0;
	virtual BOOL METHOD Export(PBYTE  pData, UINT uSize)			= 0;
	virtual BYTE METHOD GetPropByte(UINT uProp)				= 0;
	virtual BOOL METHOD SetPropByte(UINT uProp, BYTE Data)			= 0;
	virtual WORD METHOD GetPropWord(UINT uProp)				= 0;
	virtual BOOL METHOD SetPropWord(UINT uProp, WORD Data)			= 0;
	virtual LONG METHOD GetPropLong(UINT uProp)				= 0;
	virtual BOOL METHOD SetPropLong(UINT uProp, DWORD Data)			= 0;
	virtual BOOL METHOD GetPropData(UINT uProp, PBYTE pData, UINT uSize)	= 0;
	virtual BOOL METHOD SetPropData(UINT uProp, PCBYTE pData, UINT uSize)	= 0;
	};

// End of File

#endif
