
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ZeroMap_HPP
	
#define	INCLUDE_ZeroMap_HPP

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE TEXT(__FILE__)

/////////////////////////////////////////////////////////////////////////
//
// Zeroed Map Collection
//

template <typename CName, typename CData> class CZeroMap : public CMap <CName, CData>
{
	public:
		// Constructors
		CZeroMap(void);
		CZeroMap(CZeroMap const &That);

		// Assignment
		CZeroMap const & operator = (CZeroMap const &That);

		// Lookup Operator
		CData const & operator [] (CName const &Name) const;

	protected:
		// Data Members
		CData m_Null;
	};

/////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CName, typename CData>

#define	TP2 CZeroMap <CName, CData>

/////////////////////////////////////////////////////////////////////////
//
// Map Collection
//

// Constructors

TP1 TP2::CZeroMap(void)
{
	AfxSetZero(m_Null);
	}

TP1 TP2::CZeroMap(CZeroMap const &That) : m_Tree(That.m_Tree)
{
	AfxSetZero(m_Null);
	}

// Assignment

// cppcheck-suppress operatorEqVarError

TP1 TP2 const & TP2::operator = (CZeroMap const &That)
{
	m_Tree = That.m_Tree;

	return ThisObject;
	}

// Lookup Operator

TP1 CData const & TP2::operator [] (CName const &Name) const
{
	INDEX Index = m_Tree.Find(CPair(Name));

	return Failed(Index) ? m_Null : m_Tree[Index].GetData();
	}

// End of File

#endif
