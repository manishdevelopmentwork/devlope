
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Printf_HPP
	
#define	INCLUDE_Printf_HPP

/////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/JASgE

/////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "String.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Printf Formatted String
//

class DLLAPI CPrintf : public CString
{
	public:
		// Constructors
		CPrintf(PCTXT pFormat, ...);
	};

// End of File

#endif
