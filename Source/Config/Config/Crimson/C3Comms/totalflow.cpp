
#include "intern.hpp"

#include "totalflow.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlowMasterDeviceOptions, CUIItem);

// Constructor

CTotalFlowMasterDeviceOptions::CTotalFlowMasterDeviceOptions(void)
{
	m_Name = L"TOTALFLOW";

	m_Code = L"0000";

	m_App  = 0;
	}

// UI Management

void CTotalFlowMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Data Access

UINT CTotalFlowMasterDeviceOptions::GetApp(void)
{
	return m_App;
	}

// Download Support

BOOL CTotalFlowMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddText(m_Name);
	Init.AddText(m_Code);
	Init.AddByte(BYTE(m_App));

	return TRUE;
	}

// Meta Data Creation

void CTotalFlowMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddString (Code);
	Meta_AddInteger(App);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Serial Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlowSerialMasterDeviceOptions, CTotalFlowMasterDeviceOptions);

// Constructor

CTotalFlowSerialMasterDeviceOptions::CTotalFlowSerialMasterDeviceOptions(void)
{
	m_Estab = 1;

	m_Time1 = 2000;
	}

// UI Management

void CTotalFlowSerialMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CTotalFlowSerialMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CTotalFlowMasterDeviceOptions::MakeInitData(Init);

	Init.AddWord(WORD(m_Estab));
	Init.AddWord(WORD(m_Time1));

	return TRUE;
	}

// Meta Data Creation

void CTotalFlowSerialMasterDeviceOptions::AddMetaData(void)
{
	CTotalFlowMasterDeviceOptions::AddMetaData();

	Meta_AddInteger(Estab);
	Meta_AddInteger(Time1);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow TCP/IP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlowTcpMasterDeviceOptions, CTotalFlowMasterDeviceOptions);

// Constructor

CTotalFlowTcpMasterDeviceOptions::CTotalFlowTcpMasterDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_Port   = 9999;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Management

void CTotalFlowTcpMasterDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CTotalFlowTcpMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CTotalFlowMasterDeviceOptions::MakeInitData(Init);

	Init.AddLong(m_IP);
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CTotalFlowTcpMasterDeviceOptions::AddMetaData(void)
{
	CTotalFlowMasterDeviceOptions::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Master Comms Driver
//

// Constructor

CTotalFlowMasterDriver::CTotalFlowMasterDriver(void)
{
	m_uType		= driverMaster;
	
	m_Manufacturer	= "ABB";
	
	m_DriverName	= "TotalFlow Master";
	
	m_Version	= "1.10";
	
	m_ShortName	= "TotalFlow Master";

	m_DevRoot       = "METER";
	}

// Detructor

CTotalFlowMasterDriver::~CTotalFlowMasterDriver(void)
{
	}

// Address Management

BOOL CTotalFlowMasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CTotalFlowDialog Dlg(this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CTotalFlowMasterDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CStringArray Part;

	Text.Tokenize(Part, '.');

	if( Part.GetCount() == 3 || Part.GetCount() == 4 ) {

		Addr.a.m_Table  = watoi(Part[1]) + 1;

		Addr.a.m_Offset = watoi(Part[2]);

		Addr.a.m_Extra  = 0;

		if( Part.GetCount() == 4 ) {

			if( Part[3].GetLength() == 1 ) {

				switch( Part[3].ToUpper()[0] ) {

					case 'B':

						Addr.a.m_Type = addrByteAsByte;

						return TRUE;

					case 'W':

						Addr.a.m_Type = addrWordAsWord;

						return TRUE;

					case 'L':

						Addr.a.m_Type = addrLongAsLong;

						return TRUE;

					case 'R':

						Addr.a.m_Type = addrRealAsReal;

						return TRUE;

					case 'S':

						Error.Set(CString(IDS_STRING_REFERENCES));

						return FALSE;
					}
				}

			Error.Set(CString(IDS_TYPE_SUFFIX_MUST));

			return FALSE;
			}

		Addr.a.m_Type = addrRealAsReal;

		return TRUE;
		}

	if( Part.GetCount() == 5 ) {

		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Table  = watoi(Part[1]) + 1;

		Addr.a.m_Offset = watoi(Part[2]);

		Addr.a.m_Extra  = 0;

		if( Part[3] == L"S" ) {

			Addr.a.m_Offset *= 16;

			Addr.a.m_Offset += watoi(Part[4]);

			Addr.a.m_Extra |= 1;

			return TRUE;
			}
		}

	Error.Set(CString(IDS_INVALID_ADDRESS));

	return FALSE;
	}

BOOL CTotalFlowMasterDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CTotalFlowMasterDeviceOptions * pOpt = (CTotalFlowMasterDeviceOptions *) pConfig;
	
	if( Addr.a.m_Extra & 1 ) {

		Text.Printf( "%u.%u.%u.S.%u",
			     pOpt->GetApp(),
			     Addr.a.m_Table - 1,
			     Addr.a.m_Offset / 16,
			     Addr.a.m_Offset % 16
			     );

		return TRUE;
		}
	else {
		char cType = 'R';

		switch( Addr.a.m_Type ) {

			case addrByteAsByte: cType = 'B'; break;
			case addrWordAsWord: cType = 'W'; break;
			case addrLongAsLong: cType = 'L'; break;
			case addrRealAsReal: cType = 'R'; break;
			}

		Text.Printf( "%u.%u.%u.%c",
			     pOpt->GetApp(),
			     Addr.a.m_Table - 1,
			     Addr.a.m_Offset,
			     cType
			     );

		return TRUE;
		}

	return FALSE;
	}

BOOL CTotalFlowMasterDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Serial Master Comms Driver
//

// Instantiator

ICommsDriver * Create_TotalFlowSerialMasterDriver(void)
{
	return New CTotalFlowSerialMasterDriver;
	}

// Constructor

CTotalFlowSerialMasterDriver::CTotalFlowSerialMasterDriver(void)
{
	m_wID = 0x40B9;
	}

// Detructor

CTotalFlowSerialMasterDriver::~CTotalFlowSerialMasterDriver(void)
{
	}

// Binding

UINT CTotalFlowSerialMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CTotalFlowSerialMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CTotalFlowSerialMasterDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CTotalFlowSerialMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTotalFlowSerialMasterDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow TCP/IP Master Comms Driver
//

// Instantiator

ICommsDriver * Create_TotalFlowTcpMasterDriver(void)
{
	return New CTotalFlowTcpMasterDriver;
	}

// Constructor

CTotalFlowTcpMasterDriver::CTotalFlowTcpMasterDriver(void)
{
	m_wID = 0x40B8;
	}

// Detructor

CTotalFlowTcpMasterDriver::~CTotalFlowTcpMasterDriver(void)
{
	}

// Binding

UINT CTotalFlowTcpMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CTotalFlowTcpMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CTotalFlowTcpMasterDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CTotalFlowTcpMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTotalFlowTcpMasterDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CTotalFlowDialog, CStdDialog);
		
// Constructor

CTotalFlowDialog::CTotalFlowDialog(CTotalFlowMasterDriver * pDriver, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_pDriver = pDriver;

	m_fPart   = fPart;
	
	SetName(TEXT("TotalFlowDlg"));
	}

// Message Map

AfxMessageMap(CTotalFlowDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CTotalFlowDialog)
	};

// Message Handlers

BOOL CTotalFlowDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadTypeList();

	CComboBox &Box = (CComboBox &) GetDlgItem(204);

	CTotalFlowMasterDeviceOptions * pOpt = (CTotalFlowMasterDeviceOptions *) m_pConfig;

	if( m_pAddr->m_Ref ) {

		if( m_pAddr->a.m_Extra & 1 ) {

			Box.SelectData(0);

			GetDlgItem(201).SetWindowText(CPrintf(L"%u", pOpt->GetApp()));
		
			GetDlgItem(202).SetWindowText(CPrintf(L"%u", m_pAddr->a.m_Table - 1));
		
			GetDlgItem(203).SetWindowText(CPrintf(L"%u", (m_pAddr->a.m_Offset / 16)));
			}
		else {
			Box.SelectData(m_pAddr->a.m_Type);

			GetDlgItem(201).SetWindowText(CPrintf(L"%u", pOpt->GetApp()));
		
			GetDlgItem(202).SetWindowText(CPrintf(L"%u", m_pAddr->a.m_Table - 1));
		
			GetDlgItem(203).SetWindowText(CPrintf(L"%u", m_pAddr->a.m_Offset));
			}
		}
	else {
		Box.SelectData(addrWordAsWord);

		GetDlgItem(201).SetWindowText(CPrintf(L"%u", pOpt->GetApp()));

		GetDlgItem(202).SetWindowText(L"0");

		GetDlgItem(203).SetWindowText(L"0");
		}

	GetDlgItem(201).EnableWindow(FALSE);

	return FALSE;
	}

// Command Handlers

BOOL CTotalFlowDialog::OnOkay(UINT uID)
{
	CError Error(TRUE);

	CAddress Addr;

	CString Text;

	Text += GetDlgItem(201).GetWindowText() + ".";

	Text += GetDlgItem(202).GetWindowText() + ".";

	Text += GetDlgItem(203).GetWindowText() + ".";

	CComboBox &Box = (CComboBox &) GetDlgItem(204);

	UINT uData = Box.GetCurSelData();

	switch( uData ) {

		case addrByteAsByte:	Text += "B";	break;
		case addrWordAsWord:	Text += "W";	break;
		case addrLongAsLong:	Text += "L";	break;
		case addrRealAsReal:	Text += "R";	break;
		case 0:			Text += "S.0";	break;	
		}

	if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	Error.Show(ThisObject);

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CTotalFlowDialog::LoadTypeList(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(204);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	Box.AddString(CString(IDS_SPACE_BYTE),   DWORD(addrByteAsByte));

	Box.AddString(CString(IDS_SPACE_WORD),   DWORD(addrWordAsWord));

	Box.AddString(CString(IDS_SPACE_LONG),   DWORD(addrLongAsLong));

	Box.AddString(CString(IDS_SPACE_REAL),   DWORD(addrRealAsReal));

	Box.AddString(CString(IDS_STRING), DWORD(0));

	Box.SetRedraw (TRUE);

	Box.Invalidate(TRUE);
	}

// End of File
