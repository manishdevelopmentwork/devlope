
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MSHAWK_HPP
	
#define	INCLUDE_MSHAWK_HPP

//////////////////////////////////////////////////////////////////////////
//
// MicroScan Hawk Camera Driver Options
//

class CMicroScanHawkCameraDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMicroScanHawkCameraDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MicroScan Hawk Camera Device Options
//

class CMicroScanHawkCameraDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMicroScanHawkCameraDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Camera;
		UINT m_IP;
		UINT m_Port;
		UINT m_Time4;
		UINT m_Scale;
		UINT m_Width;
		UINT m_Height;
		UINT m_Graphic;
		UINT m_Border;
		UINT m_Failed;

		// Persistence
		void Load(CTreeFile &File);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MicroScan Hawk Camera Driver
//

class CMicroScanHawkCameraDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CMicroScanHawkCameraDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
