
#include "dnp3ipm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Tcp Master Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CDnp3IpMaster);

// Constructor

CDnp3IpMaster::CDnp3IpMaster(void)
{
	m_Ident   = 0x40C2;	

	m_pIpCtx  = NULL;

	m_uKeep   = 0;
	}

// Destructor

CDnp3IpMaster::~CDnp3IpMaster(void)
{
	}

// Device

CCODE MCALL CDnp3IpMaster::DeviceOpen(IDevice *pDevice)
{
	CDnp3Base::DeviceOpen(pDevice);

	if( !(m_pIpCtx = (CIpCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pIpCtx = new CIpCtx;

			m_pCtx = m_pIpCtx;

			m_pIpCtx->m_Dest		= GetWord(pData);

			m_pIpCtx->m_TO			= GetWord(pData);

			m_pCtx->m_Link			= GetLong(pData); 

			m_pCtx->m_Poll			= GetByte(pData);

			for( UINT u= 0; u < elements(m_pCtx->m_Class); u++ ) {

				m_pCtx->m_Class[u]	= ToTicks(GetLong(pData));

				m_pCtx->m_Time [u]	= 0;
				}

			m_pIpCtx->m_IP1			= GetAddr(pData);

			m_pIpCtx->m_TcpPort		= GetWord(pData);

			m_pIpCtx->m_UdpPort		= GetWord(pData);

			m_pIpCtx->m_uTime1		= GetWord(pData);

			m_pIpCtx->m_fKeep		= GetByte(pData) ? TRUE : FALSE;

			m_pIpCtx->m_uTime3		= GetWord(pData);

			m_pIpCtx->m_pSession		= NULL;

			m_pIpCtx->m_pChannel		= NULL;

			IPADDR IP1;

			memcpy(&IP1, &m_pIpCtx->m_IP1, sizeof(IPADDR));

			IPADDR IP2 = { 0 };

			m_pIpCtx->m_pConfig = m_pDnp->GetConfig(IP1,
								IP2,
								m_pIpCtx->m_TcpPort,
								m_pIpCtx->m_UdpPort,
								m_pIpCtx->m_uTime1);

			pDevice->SetContext(m_pIpCtx);

			m_uKeep++;

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx = m_pIpCtx;

	m_pChannel = m_pIpCtx->m_pChannel;

	if( IsShared() ) {

		m_pChannel->Share(TRUE, m_pIpCtx->m_uTime3);
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CDnp3IpMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( IsShared() ) {

			m_pChannel->Share(FALSE);
			}
		}
	else {
		CloseSession();

		delete m_pIpCtx;

		m_pIpCtx = NULL;

		m_pDevice->SetContext(NULL);

		m_uKeep--;
		}
	
	return CDnp3Base::DeviceClose(fPersist);
	}

// Channels

void CDnp3IpMaster::OpenChannel(void)
{
	if( m_pDnp ) {

		if( !m_pIpCtx->m_pChannel) {

			if( (m_pChannel = m_pDnp->OpenChannel(m_pIpCtx->m_pConfig, m_Source, TRUE)) ) {

				m_fOpen = TRUE;

				m_pIpCtx->m_pChannel = m_pChannel;
				}
			}
		}
	}

BOOL CDnp3IpMaster::CloseChannel(void)
{
	if( CDnp3Base::CloseChannel() ) {

		if( m_pDnp->CloseChannel(m_pIpCtx->m_pConfig, m_Source, TRUE) ) {

			m_pChannel = NULL;

			m_fOpen    = FALSE;

			m_pIpCtx->m_pChannel = m_pChannel;
			}
		}

	return !m_fOpen;
	}

// Helpers

BOOL CDnp3IpMaster::IsShared(void)
{
	return !m_pIpCtx->m_fKeep || m_uKeep > 4;
	}


// End of File
