// -*- C++ -*- Manage the thread-local exception globals.
// Copyright (C) 2001-2017 Free Software Foundation, Inc.
//
// This file is part of GCC.
//
// GCC is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3, or (at your option)
// any later version.
//
// GCC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Under Section 7 of GPL version 3, you are granted additional
// permissions described in the GCC Runtime Library Exception, version
// 3.1, as published by the Free Software Foundation.

// You should have received a copy of the GNU General Public License and
// a copy of the GCC Runtime Library Exception along with this program;
// see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
// <http://www.gnu.org/licenses/>.

#include "max_align.h"

#include "unwind-cxx.h"

#include <sys/tls.h>

using namespace __cxxabiv1;

#if defined(_tls_get_data)

extern "C" __cxa_eh_globals *
__cxxabiv1::__cxa_get_globals_fast() _GLIBCXX_NOTHROW
{ return (__cxa_eh_globals *) _tls_get_data()->eh; };

extern "C" __cxa_eh_globals *
__cxxabiv1::__cxa_get_globals() _GLIBCXX_NOTHROW
{ return (__cxa_eh_globals *) _tls_get_data()->eh; };

#else

static __cxa_eh_globals   _eh_globals;

void * _thread_eh_data = &_eh_globals;

extern "C" __cxa_eh_globals *
__cxxabiv1::__cxa_get_globals_fast() _GLIBCXX_NOTHROW
{ return (__cxa_eh_globals *) _thread_eh_data; };

extern "C" __cxa_eh_globals *
__cxxabiv1::__cxa_get_globals() _GLIBCXX_NOTHROW
{ return (__cxa_eh_globals *) _thread_eh_data; };

#endif
