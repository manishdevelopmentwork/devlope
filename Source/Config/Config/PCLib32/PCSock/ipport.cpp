
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2004 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IP Port Address
//

// Constructors

CIPPort::CIPPort(void)
{
	m_Port = 0;
	}

CIPPort::CIPPort(CIPPort const &That)
{
	Create(That);
	}

CIPPort::CIPPort(sockaddr const &addr)
{
	Create(addr);
	}

CIPPort::CIPPort(PCTXT pHost)
{
	Create(pHost);
	}

CIPPort::CIPPort(PCTXT pHost, WORD Port)
{
	Create(pHost, Port);
	}

CIPPort::CIPPort(CIPAddr const &Addr, WORD Port)
{
	Create(Addr, Port);
	}

CIPPort::CIPPort(WORD Port)
{
	m_Port = Port;
	}

// Assignment Operators

CIPPort const & CIPPort::operator = (CIPPort const &That)
{
	Create(That);

	return ThisObject;
	}

CIPPort const & CIPPort::operator = (sockaddr const &addr)
{
	Create(addr);

	return ThisObject;
	}

CIPPort const & CIPPort::operator = (PCTXT pHost)
{
	Create(pHost);
	
	return ThisObject;
	}

// Initialisation

BOOL CIPPort::Create(CIPPort const &That)
{
	if( CIPAddr::Create(That) ) {
	
		m_Port = That.m_Port;
	
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CIPPort::Create(sockaddr const &addr)
{
	if( CIPAddr::Create(addr) ) {
	
		sockaddr_in &in = (sockaddr_in &) addr;
	
		m_Port = MAKEWORD(HIBYTE(in.sin_port), LOBYTE(in.sin_port));
	
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CIPPort::Create(PCTXT pHost)
{
	CString Host = pHost;
	
	UINT Find = Host.Find(':');
	
	WORD Port = 0;
	
	if( Find < NOTHING ) {
	
		Port = WORD(atoi(Host.Mid(Find+1)));
		
		Host = Host.Left(Find);
		}
		
	return Create(Host, Port);
	}

BOOL CIPPort::Create(PCTXT pHost, WORD Port)
{
	if( CIPAddr::Create(pHost) ) {
	
		m_Port = Port;
	
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CIPPort::Create(CIPAddr const &Addr, WORD Port)
{
	if( CIPAddr::Create(Addr) ) {
	
		m_Port = Port;
	
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CIPPort::Create(WORD Port)
{
	m_Port = Port;

	return TRUE;
	}

// Conversions

CIPPort::operator CString(void) const
{
	return GetHostWithPort();
	}

// Comparison Operators

int CIPPort::operator == (CIPPort const &That) const
{
	return CIPAddr::operator == (That) && m_Port == That.m_Port;
	}

int CIPPort::operator != (CIPPort const &That) const
{
	return CIPAddr::operator != (That) || m_Port != That.m_Port;
	}

// Attributes

WORD CIPPort::GetPort(void) const
{
	return m_Port;
	}

CString CIPPort::GetDottedWithPort(void) const
{
	return GetDotted() + CPrintf(":%u", m_Port);
	}

CString CIPPort::GetHostWithPort(void) const
{
	return GetHost() + CPrintf(":%u", m_Port);
	}

// Operations

void CIPPort::SetPort(WORD Port)
{
	m_Port = Port;
	}

// Socket Helpers

void CIPPort::LoadSockAddr(sockaddr &addr) const
{
	CIPAddr::LoadSockAddr(addr, m_Port);
	}

// End of File
