
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Modal Dialogs
//

// Code

function showButton(button, cname, label) {

	button.removeClass("btn-danger btn-success btn-primary btn-secondary btn-default");

	button.addClass(cname);

	button.html(label);

	button.show();
}

function hideButton(button) {

	button.hide();
}

function confirm(label, body, action) {

	$("#modal-label").html(label);

	$("#modal-body").html(body);

	showButton($("#modal-b1"), "btn-danger", "OK");

	showButton($("#modal-b2"), "btn-secondary", "Cancel");

	modal(false, action);
}

function display(label, body, action) {

	$("#modal-label").html(label);

	$("#modal-body").html(body);

	showButton($("#modal-b1"), "btn-primary", "OK");

	hideButton($("#modal-b2"));

	modal(true, action);
}

function modal(cancel, action) {

	var b1 = function () {

		hideModal($('#modal'));

		if (!!action) {

			action();
		}
	};

	var b2 = function () {

		hideModal($('#modal'));
	};

	$("#modal-b1").off("click").on("click", b1);

	$("#modal-b2").off("click").on("click", b2);

	$("#modal-bx").off("click").on("click", cancel ? b1 : b2);

	showModal($('#modal'));
}

function showModal(item) {

	item.modal("show");
}

function hideModal(item) {

	item.modal("hide");
}

function showProgress(abort) {

	showButton($("#progress-b1"), "btn-danger", "ABORT");

	$("#progress-b1").off("click").on("click", abort);

	$("#progress-bx").off("click").on("click", abort);

	setProgress(0);

	showModal($("#progress"));
}

function setProgress(p) {

	$("#progress-text").html(p + "%");

	$("#progress-bar").css("width", p + "%");
}

function hideProgress() {

	hideModal($("#progress"));
}

function showWait() {

	$("body").addClass("wait");

	$("#waiter").show().css("opacity", "0.5");
}

function hideWait() {

	$("#waiter").css("opacity", "0").hide();

	$("body").removeClass("wait");
}

// End of File
