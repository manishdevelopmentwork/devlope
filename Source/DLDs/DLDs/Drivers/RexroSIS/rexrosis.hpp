
//////////////////////////////////////////////////////////////////////////
//
// Rexroth SIS
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Model definition
#define	MODELECO	0
#define	MODELSYNAX	1

// Remote DP Address Array Size
#define RDPSIZE		64

// Miscellaneous
#define	ISREAD	TRUE
#define	ISWRITE	FALSE

// Type values (00TT TSSS)
#define TYPESV	0	//S-0
#define	TYPEPV	8	//P-0
#define	TYPEAV	0x10	//A-0
#define	TYPECV	0x20	//C-0

// Spaces
#define	PCUR	 1
#define	PMIN	 2
#define	PMAX	 3
#define	PATT	 4
#define	PUNIT	 5
#define LISTP	10
#define SP_ERR	11
#define LISTS	12
#define LISTA	13
#define LISTC	14

//Service Opcodes
#define STERMINATE	1
#define SINITIALIZE	3
#define	SGENERAL	0xF
#define ECOPARREAD	0x80
#define ECOLISTREAD	0x81
#define ECOLISTWRITE	0x8E
#define ECOPARWRITE	0x8F
#define SYNPARREAD	0x90
#define SYNLISTREAD	0x91
#define SYNLISTWRITE	0x9E
#define SYNPARWRITE	0x9F

//Data Block Elements - uses bits 3-5
#define	DBEATT	(3<<3)
#define DBEUNIT (4<<3)
#define DBEMIN	(5<<3)
#define DBEMAX	(6<<3)
#define DBECUR	(7<<3)

// Control Byte
#define TFINAL	4

// Attribute Defs
#define	ATT_TYPEP	20
#define	ATT_TYPEMASK	(7 << ATT_TYPEP)
#define	ATT_BIN		(0 << ATT_TYPEP)
#define	ATT_UDEC	(1 << ATT_TYPEP)
#define	ATT_SDEC	(2 << ATT_TYPEP)
#define	ATT_HEX		(3 << ATT_TYPEP)
#define	ATT_ASC		(4 << ATT_TYPEP)
#define	ATT_IDN		(5 << ATT_TYPEP)
#define	ATT_REAL	(6 << ATT_TYPEP)

//*****
#define ATT_SZP		16
#define	ATT_SZMASK	(3 << ATT_SZP)
#define ATT_SZONE	0
#define ATT_SZTWO	1
#define ATT_SZFOUR	2
#define	ATT_SZVAR1	4
#define	ATT_SZVAR2	5
#define	ATT_SZVAR4	6
#define	ATT_SZ2		(ATT_SZTWO << ATT_SZP)
#define	ATT_SZ4		(ATT_SZFOUR << ATT_SZP)
#define	ATT_SZV1	(ATT_SZVAR1 << ATT_SZP)
#define	ATT_SZV2	(ATT_SZVAR2 << ATT_SZP)
#define	ATT_SZV4	(ATT_SZVAR4 << ATT_SZP)
#define ATT_DPP		24
#define ATT_DPMASK	(0xF << ATT_DPP)

//*****

// Reply Control (Cntrl >> 5)
#define	NOERROR	0
#define	CNBUSY	1
#define CNOFFL	2
#define	CNWARN	4
#define	CNERR	6

// Address Format:
/*
Addr.a.m_Extra  = 4 MSB's of Parameter Number
Addr.a.m_Offset when Table is a Parameter Value ( Addr,a,m_Table = 1 through 6 )
		00TT TSSS PPPP PPPP
		PPPP PPPP	= 8 LSB's of Parameter Number
		SSS		= 3 LSB's = Parameter Set Number.
		TTT		= 000 for S, 001 for P, 010 for A, 100 for C Parameter Table

  Addr.a.m_Offset when Table is a List Parameter (only P tables)
		-> LOWORD = 8 LSB's of List Item
		-> HIWORD = 8 LSB's of Parameter Number
*/

class CRexrothSISDriver : public CMasterDriver {

	public:
		// Constructor
		CRexrothSISDriver(void);

		// Destructor
		~CRexrothSISDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);


		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		CCODE MCALL Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE MCALL Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bSDrop;

			BYTE	m_bRDrop;

			BYTE	m_bModel;

			DWORD	m_ErrorCode;
		};

		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[64];
		BYTE	m_bRx[256];
		UINT	m_uPtr;
		BOOL	m_fReturnAsLong;
		UINT	m_uWriteDP;
		UINT	m_uRcvPtr;
		UINT	m_uReturnListCount;
		DWORD	m_S76[3];
		DWORD	m_S44[3];
		DWORD	m_S160[3];

		// Implementation
		BOOL ReadParameter(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL SendParameterReadReq(CAddress Addr, PDWORD pData, PDWORD pAttribute, BYTE bType);
		BOOL WriteParameter(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL ReadList(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL WriteList(AREF Addr, PDWORD pData, UINT * pCount);

		// Frame Building
		void ConfigureHeaders(UINT uTable, BYTE bOp);
		void AddUserHeader(UINT uTable);
		void StartFrame(BYTE bOp);
		void EndFrame(void);
		void AddParameter(UINT uAddress, BYTE bType);
		void AddByte(BYTE bData);
		void AddData(DWORD dData, DWORD dMask);
		void AddWriteData(DWORD dData, DWORD dAttribute);
		void AddDataPerSize(DWORD dData, DWORD dAttribute);
		UINT AssembleListData(PDWORD pData, PBYTE pb, UINT uCount, DWORD dAttribute);

		// Data Transfer
		BOOL Transact(void);
		BOOL GetReply(void);
		BOOL CheckCheck(BYTE bCheck);

		// Response Handlers
		BOOL  GetResponse(PDWORD pData, DWORD dAttribute);
		BOOL  GetListResponse(PDWORD pData, UINT uCount, DWORD dAttribute, UINT uDP);
		DWORD GetRxBufferData(UINT uCount, UINT uPos);
		DWORD FormatRcvData(DWORD dAttribute, DWORD dData, UINT uDP);

		// Port Access
		void Put(void);
		UINT Get(UINT uTimer);

		// Helpers
		UINT	GetListType(UINT uTable);
		UINT	MakeParamAddress(AREF Addr);
		BOOL	GetParameterAttribute(CAddress Addr, PDWORD pAttribute, BYTE bType);
		UINT	GetParServiceCode(BOOL fIsRead);
		UINT	GetListServiceCode(BOOL fIsRead);
		BOOL	IsList(UINT uTable);

		// Determine number of decimal point locations for non-real decimal values
		UINT	GetDecimalPositions(CAddress Addr, DWORD dAttribute, BYTE bType);
};

// End of File
