
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 I/O Module Support
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_NewCommsPortRack_HPP

#define INCLUDE_NewCommsPortRack_HPP

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Port
//

class DLLAPI CNewCommsPortRack : public CCommsPort
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CNewCommsPortRack(void);

	// Operations
	void DeleteModuleDevices(void);

	// UI Creation
	CViewWnd * CreateView(UINT uType);

	// Driver Creation
	ICommsDriver * CreateDriver(UINT uID);

	// Item Naming
	BOOL    IsHumanRoot(void) const;
	CString GetHumanName(void) const;
	CString GetItemOrdinal(void) const;

	// Persistance
	void Init(void);

	// Conversion
	void PostConvert(void);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

protected:
	// Meta Data Creation
	void AddMetaData(void);
};

// End of File

#endif
