
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Instance Pointer
//

global IStratonRetain * g_pRetain = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Retentive Base Class
//

// Constructor

CRetainBase::CRetainBase(void)
{
	memset(&m_dwDirty, 0, sizeof(m_dwDirty));
	}

// Implementation

UINT CRetainBase::GetSize(T5PTR_DBRETAIN pRetain)
{
	UINT uSize = 0;

	uSize += sizeof(DWORD);

	uSize += sizeof(m_dwDirty);

	uSize += sizeof(pRetain->dwCrc);

	uSize += pRetain->wSizeD8;
	uSize += pRetain->wSizeD16;
	uSize += pRetain->wSizeD32;
	uSize += pRetain->wSizeD64;
	uSize += pRetain->wSizeTime;
	uSize += pRetain->wSizeString;
	uSize += pRetain->dwSizeCT;

	return uSize;
	}

BOOL CRetainBase::IsDirty(T5PTR_DBRETAIN pRetain)
{
	DWORD dwCheck[7];

	dwCheck[0] = CRC32(PBYTE(pRetain->pD8), pRetain->wSizeD8);

	dwCheck[1] = CRC32(PBYTE(pRetain->pD16), pRetain->wSizeD16);

	dwCheck[2] = CRC32(PBYTE(pRetain->pD32), pRetain->wSizeD32);

	dwCheck[3] = CRC32(PBYTE(pRetain->pD64), pRetain->wSizeD64);

	dwCheck[4] = CRC32(PBYTE(pRetain->pTime), pRetain->wSizeTime);

	dwCheck[5] = CRC32(PBYTE(pRetain->pString), pRetain->wSizeString);
	
	dwCheck[6] = CRC32(PBYTE(pRetain->pCT), pRetain->dwSizeCT);

	for( UINT n = 0; n < elements(dwCheck); n ++ ) {

		if( dwCheck[n] ^ m_dwDirty[n] ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CRetainBase::MakeDirty(T5PTR_DBRETAIN pRetain)
{
	m_dwDirty[0] = CRC32(PBYTE(pRetain->pD8), pRetain->wSizeD8);

	m_dwDirty[1] = CRC32(PBYTE(pRetain->pD16), pRetain->wSizeD16);

	m_dwDirty[2] = CRC32(PBYTE(pRetain->pD32), pRetain->wSizeD32);

	m_dwDirty[3] = CRC32(PBYTE(pRetain->pD64), pRetain->wSizeD64);

	m_dwDirty[4] = CRC32(PBYTE(pRetain->pTime), pRetain->wSizeTime);

	m_dwDirty[5] = CRC32(PBYTE(pRetain->pString), pRetain->wSizeString);

	m_dwDirty[6] = CRC32(PBYTE(pRetain->pCT), pRetain->dwSizeCT);
	}

//////////////////////////////////////////////////////////////////////////
//
// Retentive by file
//

// Constructor

CRetainFile::CRetainFile(void)
{
	g_pRetain = this;

	m_pPath   = "\\straton\\t5.ret";
	}

// Operations

BOOL CRetainFile::CanLoad(DWORD dwCrc)
{
	CAutoGuard Guard;

	CAutoFile  File(m_pPath, "r");

	if( File ) {

		DWORD dwChk;

		if( File.Read(PBYTE(&dwChk), sizeof(dwChk)) ) {

			return dwChk == dwCrc;
			}
		}

	return FALSE;
	}

BOOL CRetainFile::Load(T5PTR_DBRETAIN pRetain)
{
	CAutoGuard Guard;

	CAutoFile  File(m_pPath, "r");

	if( File ) {

		DWORD dwCRC;

		if( File.Read(PBYTE(&dwCRC), sizeof(dwCRC)) ) {
			
			if( pRetain->dwCrc == dwCRC ) {

				Load(File, PBYTE(&m_dwDirty), sizeof(m_dwDirty));
				
				Load(File, pRetain->pD8,      pRetain->wSizeD8);

				Load(File, pRetain->pD16,     pRetain->wSizeD16);

				Load(File, pRetain->pD32,     pRetain->wSizeD32);

				Load(File, pRetain->pD64,     pRetain->wSizeD64);

				Load(File, pRetain->pTime,    pRetain->wSizeTime);

				Load(File, pRetain->pString,  pRetain->wSizeString);

				Load(File, pRetain->pCT,      pRetain->dwSizeCT);

				return T5RET_OK;
				}
			}
		}

	return FALSE;
	}

BOOL CRetainFile::CanSave(T5PTR_DBRETAIN pRetain)
{
	CAutoGuard Guard;

	CAutoFile  File(m_pPath, "r+", "w+");

	return File;
	}

void CRetainFile::Save(T5PTR_DBRETAIN pRetain)
{
	CAutoGuard Guard;

	if( GetSize(pRetain) > 0 ) {

		CAutoFile File(m_pPath, "r+", "w+");

		if( File ) {

			MakeDirty(pRetain);

			Save(File, PBYTE(&m_dwDirty), sizeof(m_dwDirty));

			Save(File, &pRetain->dwCrc,  sizeof(pRetain->dwCrc));

			Save(File, pRetain->pD8,     pRetain->wSizeD8);

			Save(File, pRetain->pD16,    pRetain->wSizeD16);

			Save(File, pRetain->pD32,    pRetain->wSizeD32);

			Save(File, pRetain->pD64,    pRetain->wSizeD64);

			Save(File, pRetain->pTime,   pRetain->wSizeTime);

			Save(File, pRetain->pString, pRetain->wSizeString);

			Save(File, pRetain->pCT,     pRetain->dwSizeCT);						

			File.Truncate();
			}

		return;
		}

	unlink(m_pPath);
	}

// Implementation

void CRetainFile::Save(CAutoFile &File, T5_PTR pData, UINT uSize)
{
	if( uSize ) {

		UINT uDone = File.Write(PBYTE(pData), uSize);

		AfxTouch(uDone);

		return;
		}
	}

void CRetainFile::Load(CAutoFile &File, T5_PTR pData, UINT uSize)
{
	if( uSize ) {

		UINT uDone = File.Read(PBYTE(pData), uSize);

		AfxTouch(uDone);
		
		return;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Retentive by MRAM
//

// Constant

#define	timePersist (1 * 1000)

// Constructor

CRetainMram::CRetainMram(void)
{
	g_pRetain = this;

	m_uAddr   = Mem(Control);

	m_uLast   = 0;
	}

// IStratonRetain

BOOL CRetainMram::CanLoad(DWORD dwCrc)
{
	UINT uAddr = m_uAddr;

	DWORD dwMagic;

	if( Load(uAddr, PBYTE(&dwMagic), sizeof(dwMagic)) ) {
		
		if( dwMagic == constMagic ) {

			DWORD dwChk;

			if( Load(uAddr, PBYTE(&dwChk), sizeof(dwChk)) ) {
				
				return dwCrc == dwChk;
				}
			}
		}

	return FALSE;
	}

BOOL CRetainMram::Load(T5PTR_DBRETAIN pRetain)
{
	UINT   uAddr = m_uAddr;

	DWORD dwMagic;

	if( Load(uAddr, PBYTE(&dwMagic), sizeof(dwMagic)) ) {
		
		if( dwMagic == constMagic ) {

			Load(uAddr, PBYTE(&m_dwDirty), sizeof(m_dwDirty));

			DWORD dwChk;

			if( Load(uAddr, PBYTE(&dwChk), sizeof(dwChk)) ) {

				if( pRetain->dwCrc == dwChk ) {

					Load(uAddr, pRetain->pD8,      pRetain->wSizeD8);

					Load(uAddr, pRetain->pD16,     pRetain->wSizeD16);

					Load(uAddr, pRetain->pD32,     pRetain->wSizeD32);

					Load(uAddr, pRetain->pD64,     pRetain->wSizeD64);

					Load(uAddr, pRetain->pTime,    pRetain->wSizeTime);

					Load(uAddr, pRetain->pString,  pRetain->wSizeString);

					Load(uAddr, pRetain->pCT,      pRetain->dwSizeCT);

					MakeDirty(pRetain);					
					
					return TRUE;
					}
				}
			}
		}
	
	return FALSE;
	}

BOOL CRetainMram::CanSave(T5PTR_DBRETAIN pRetain)
{
	if( GetSize(pRetain) < 8192 ) {		
		
		return TRUE;
		}

	return FALSE;
	}

void CRetainMram::Save(T5PTR_DBRETAIN pRetain)
{
	UINT uTime = GetTickCount();

	if( m_uLast == 0 ) {
		
		m_uLast = uTime;
		}
	else {
		if( uTime - m_uLast >= ToTicks(timePersist) ) {

			if( IsDirty(pRetain) ) {

				UINT    uAddr = m_uAddr;

				DWORD dwMagic = constMagic;

				if( Save(uAddr, PBYTE(&dwMagic), sizeof(dwMagic)) ) {

					if( GetSize(pRetain) > 0 ) {

						if( Save(uAddr, PBYTE(&m_dwDirty), sizeof(m_dwDirty)) ) {

							Save(uAddr, &pRetain->dwCrc,  sizeof(pRetain->dwCrc));

							Save(uAddr, pRetain->pD8,     pRetain->wSizeD8);

							Save(uAddr, pRetain->pD16,    pRetain->wSizeD16);

							Save(uAddr, pRetain->pD32,    pRetain->wSizeD32);

							Save(uAddr, pRetain->pD64,    pRetain->wSizeD64);

							Save(uAddr, pRetain->pTime,   pRetain->wSizeTime);

							Save(uAddr, pRetain->pString, pRetain->wSizeString);

							Save(uAddr, pRetain->pCT,     pRetain->dwSizeCT);

							MakeDirty(pRetain);
							}
						}
					}
				}

			m_uLast = uTime;
			}
		}
	}

// Implementation

BOOL CRetainMram::Save(UINT &uAddr, T5_PTR pData, UINT uSize)
{
	if( uSize ) {

		if( FRAMPutData(uAddr, PBYTE(pData), uSize) ) {

			uAddr += uSize;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CRetainMram::Load(UINT &uAddr, T5_PTR pData, UINT uSize)
{
	if( uSize ) {

		if( FRAMGetData(uAddr, PBYTE(pData), uSize) ) {

			uAddr += uSize;

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
