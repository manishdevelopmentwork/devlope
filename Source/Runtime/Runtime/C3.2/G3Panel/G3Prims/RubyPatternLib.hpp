
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyPatternLib_HPP
	
#define	INCLUDE_RubyPatternLib_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ruby Pattern Library
//

class CRubyPatternLib : public CItem
{
	public:
		// Constructors
		CRubyPatternLib(void);

		// Destructors
		~CRubyPatternLib(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Gdi Configuration
		PSHADER SetGdi(IGdi *pGdi, UINT uCode, COLOR Fore, COLOR Back);
		void    Release(IGdi *pGdi, UINT uCode);

	protected:
		// Data Members
		UINT m_List[128];

		// Static Data
		static BOOL  m_ShadeMode;
		static COLOR m_ShadeCol1;
		static COLOR m_ShadeCol2;

		// Color Mixing
		static COLOR Mix16(COLOR a, COLOR b, int p, int c);
		static DWORD Mix32(COLOR a, COLOR b, int p, int c);

		// Shaders
		static BOOL ShaderLinear(IGDI *pGDI, int p, int c);
		static BOOL ShaderMiddle(IGDI *pGDI, int p, int c);
		static BOOL ShaderTable(IGdi *pGdi, int p, int c, int size, int const *pos, int const *col);
		static BOOL ShaderChrome(IGDI *pGDI, int p, int c);
		static BOOL ShaderHorzCylinder(IGDI *pGDI, int p, int c);
		static BOOL ShaderVertCylinder(IGDI *pGDI, int p, int c);
	};

// End of File

#endif
