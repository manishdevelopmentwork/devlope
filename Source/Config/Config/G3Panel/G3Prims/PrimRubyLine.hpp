
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyLine_HPP
	
#define	INCLUDE_PrimRubyLine_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRuby.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyPenLine;
class CPrimRubyPenEdge;
	
//////////////////////////////////////////////////////////////////////////
//
// Ruby Line Primitive
//

class CPrimRubyLine : public CPrimRuby
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyLine(void);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);
		void GetRefs(CPrimRefList &Refs);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CPrimRubyPenLine * m_pLine;
		CPrimRubyPenEdge * m_pEdge;

	protected:
		// Data Members
		CRubyPath    m_pathTest;
		CRubyPath    m_pathLine;
		CRubyPath    m_pathEdge;
		CRubyPath    m_pathTrim;
		CRubyGdiList m_listLine;
		CRubyGdiList m_listEdge;
		CRubyGdiList m_listTrim;

		// Meta Data Creation
		void AddMetaData(void);

		// Fast Fill Control
		BOOL UseFastFill(void);

		// Path Management
		void InitPaths(void);
		void MakePaths(void);
		void MakeLists(void);
	};

// End of File

#endif
