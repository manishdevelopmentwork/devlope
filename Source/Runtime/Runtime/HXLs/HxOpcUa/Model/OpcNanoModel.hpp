
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcNanoModel_HPP

#define INCLUDE_OpcNanoModel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OpcDataModel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nano Data Model
//

class COpcNanoModel : public COpcDataModel
{
	public:
		// Operations
		void AddStandard(void);
	};

// End of File

#endif
