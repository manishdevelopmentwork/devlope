
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3DevCon_HPP

#define	INCLUDE_G3DevCon_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <G3Schema.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "G3DevCon.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3DEVCON

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "G3DevCon.lib")

#endif

#define DLLNOT 

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define STRONG_INLINE inline

// End of File

#endif
