
#include "Intern.hpp"

#include "SerialPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// System Headers
//

#include <asm/termbits.h>

#include <sys/ioctl.h>

#include <linux/serial.h>

#include <linux/poll.h>

//////////////////////////////////////////////////////////////////////////
//
// Serial Port
//

// Instantiators

IDevice * Create_BaseSerial(UINT uPort)
{
	CPrintf Name("/dev/ttyS%u", 1+uPort);

	struct  stat s;

	if( _stat(Name, &s) == 0 ) {

		return New CSerialPort(Name, 0, uPort);
	}

	return NULL;
}

IDevice * Create_PipeSerial(UINT uPort)
{
	CPrintf Name("/dev/ttyGS%u", uPort);

	struct  stat s;

	if( _stat(Name, &s) == 0 ) {

		return New CSerialPort(Name, 10, uPort);
	}

	return NULL;
}

IDevice * Create_SledSerial(UINT uSled, UINT uPort)
{
	CPrintf Name("/dev/ttyS%us%u", uPort, uSled);

	return New CSerialPort(Name, uSled, uPort);
}

// Constructor

CSerialPort::CSerialPort(CString Name, UINT uSled, UINT uPort)
{
	StdSetRef();

	m_Name     = Name;

	m_uSled    = uSled;

	m_uPort    = uPort;

	m_uUnit    = 10 * uSled + uPort;

	m_uTick    = 0;

	m_fOpen    = FALSE;

	m_fd       = 0;

	m_uRecv    = 0;

	m_pHandler = NULL;

	m_pSwitch  = NULL;

	AfxGetObject("dev.platform", 0, IPortSwitch, m_pSwitch);
}

// Destructor

CSerialPort::~CSerialPort(void)
{
	Close();

	AfxRelease(m_pSwitch);
}

// IUnknown

HRESULT CSerialPort::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPortObject);

	StdQueryInterface(IPortObject);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CSerialPort::AddRef(void)
{
	StdAddRef();
}

ULONG CSerialPort::Release(void)
{
	StdRelease();
}

// IDevice

BOOL METHOD CSerialPort::Open(void)
{
	return TRUE;
}

// IPortObject

void METHOD CSerialPort::Bind(IPortHandler *pHandler)
{
	m_pHandler = pHandler;

	m_pHandler->Bind(this);
}

void METHOD CSerialPort::Bind(IPortSwitch *pSwitch)
{
}

UINT METHOD CSerialPort::GetPhysicalMask(void)
{
	return m_pSwitch ? m_pSwitch->GetMask(m_uUnit) : Bit(physicalRS232);
}

BOOL METHOD CSerialPort::Open(CSerialConfig const &Config)
{
	if( !m_fOpen ) {

		m_Config = Config;

		if( PortOpen() ) {

			m_uWait = 0;

			if( PortSetFormat() && PortSetPhysical() ) {

				m_pHandler->AddRef();

				m_pHandler->OnOpen(m_Config);

				if( m_pSwitch ) {

					m_pSwitch->EnablePort(m_uUnit, true);
				}

				InitObjects();

				InitThread();

				StartTimer();

				m_fOpen = TRUE;

				return TRUE;
			}

			_close(m_fd);
		}
	}

	return FALSE;
}

void METHOD CSerialPort::Close(void)
{
	if( m_fOpen ) {

		WaitDone();

		TermThread();

		TermObjects();

		if( m_pSwitch ) {

			m_pSwitch->EnablePort(m_uUnit, false);
		}

		_close(m_fd);

		m_pHandler->OnClose();

		m_pHandler->Release();

		m_fOpen = FALSE;
	}
}

void METHOD CSerialPort::Send(BYTE bData)
{
	if( m_fd >= 0 ) {

		StartSend(bData);
	}
	else {
		BYTE bData;

		while( m_pHandler->OnTxData(bData) );

		m_pHandler->OnTxDone();
	}
}

void METHOD CSerialPort::SetBreak(BOOL fBreak)
{
	if( m_fd >= 0 ) {

		_ioctl(m_fd, fBreak ? TIOCSBRK : TIOCCBRK, NULL);
	}
}

void METHOD CSerialPort::EnableInterrupts(BOOL fEnable)
{
	fEnable ? LeaveGuarded() : EnterGuarded();
}

void METHOD CSerialPort::SetOutput(UINT uOutput, BOOL fOn)
{
}

BOOL METHOD CSerialPort::GetInput(UINT uInput)
{
	return FALSE;
}

DWORD METHOD CSerialPort::GetHandle(void)
{
	return DWORD(m_fd);
}

// IClientProcess

BOOL CSerialPort::TaskInit(UINT uTask)
{
	SetThreadName("SerialPump");

	// We created this pump thread at a regular priority, but we
	// switch it to real-time FIFO scheduling and give it a high
	// priority. The higher the port number, the more priority,
	// so as to give the backplane the best performance.

	int policy   = 1;

	int priority = 92 + m_uPort;

	_sched_setscheduler(0, policy, &priority);

	return TRUE;
}

INT CSerialPort::TaskExec(UINT uTask)
{
	for( ;;) {

		UINT uMask;

		if( WaitForEvent(uMask) ) {

			EnterGuarded();

			if( uMask & 1 ) {

				ReadEvent(m_term);

				OnTerm();

				LeaveGuarded();

				break;
			}

			if( uMask & 2 ) {

				ReadEvent(m_timer);

				if( !OnTimer() ) {

					LeaveGuarded();

					if( !PortRecycle() ) {

						break;
					}

					continue;
				}
			}

			if( uMask & 4 ) {

				ReadEvent(m_send);

				OnSend();
			}

			if( uMask & 8 ) {

				if( !OnRecv() ) {

					LeaveGuarded();

					if( !PortRecycle() ) {

						break;
					}

					continue;
				}
			}

			LeaveGuarded();
		}
	}

	return 0;
}

void CSerialPort::TaskStop(UINT uTask)
{
}

void CSerialPort::TaskTerm(UINT uTask)
{
}

// Handlers

bool CSerialPort::WaitForEvent(UINT &uMask)
{
	for( ;;) {

		int r;

		struct pollfd pfd[] = {

			{ m_term,  POLLIN,	     0 },
			{ m_timer, POLLIN,	     0 },
			{ m_send,  POLLIN,	     0 },
			{ m_fd,    POLLIN | POLLERR, 0 },

		};

		if( (r = _poll(pfd, elements(pfd), FOREVER)) >= 0 ) {

			uMask = 0;

			for( UINT n = 0; n < elements(pfd); n++ ) {

				if( pfd[n].revents & POLLERR ) {

					PortRecycle();

					continue;
				}

				if( pfd[n].revents & POLLIN ) {

					uMask |= (1<<n);
				}
			}

			return true;
		}
	}
}

void CSerialPort::OnTerm(void)
{
}

BOOL CSerialPort::OnTimer(void)
{
	if( m_uSled == 10 ) {

		if( ++m_uTick >= 200 ) {

			m_uTick = 0;

			CAutoFile File("/sys/module/dwc3_omap/initstate", "r");

			if( !File || File.GetLine() == "going" ) {

				return FALSE;
			}
		}
	}

	m_pHandler->OnTimer();

	return TRUE;
}

BOOL CSerialPort::OnRecv(void)
{
	UINT uTotal = 0;

	for( ;;) {

		BYTE bData[256];

		UINT uRead = _read(m_fd, bData, sizeof(bData));

		if( uRead ) {

			// If the last tranch of data arrived in a FIFO
			// sized chunk, then considering included a delay
			// before send on RS485 for line turn around. We
			// do not have to do this for smaller chunks as
			// the FIFO timeout provides the buffer...

			uTotal += uRead;

			m_uRecv = (uTotal % 48) ? 0 : getmonomicro();

			for( UINT n = 0; n < uRead; n++ ) {

				m_pHandler->OnRxData(bData[n]);
			}

			continue;
		}

		break;
	}

	if( uTotal ) {

		m_pHandler->OnRxDone();

		return TRUE;
	}

	return FALSE;
}

void CSerialPort::OnSend(void)
{
	BYTE bSend[256];

	UINT uSend = 1;

	bSend[0]   = m_bSend;

	if( m_uRecv && m_uWait ) {

		INT64 uTime = getmonomicro();

		INT64 uGone = uTime - m_uRecv;

		if( uTime < m_uRecv + m_uWait ) {

			UINT us = UINT(m_uRecv + m_uWait - uTime);

			_usleep(us);
		}

		m_uRecv = 0;
	}

	for( ;;) {

		BYTE &bData = bSend[uSend];

		if( m_pHandler->OnTxData(bData) ) {

			if( ++uSend == sizeof(bSend) ) {

				_write(m_fd, bSend, uSend);

				uSend = 0;
			}

			continue;
		}

		break;
	}

	if( uSend ) {

		_write(m_fd, bSend, uSend);
	}

	m_pHandler->OnTxDone();

	SetEvent(m_done);
}

// Comms

void CSerialPort::StartSend(BYTE bData)
{
	WaitDone();

	ClearEvent(m_done);

	m_bSend = bData;

	SetEvent(m_send);

	_sched_yield();
}

void CSerialPort::WaitDone(void)
{
	_select_read(m_done, FOREVER);
}

// Objects

void CSerialPort::InitObjects(void)
{
	m_term   = _eventfd(0);

	m_timer  = _timerfd_create(1, 0);

	m_send   = _eventfd(0);

	m_done   = _eventfd(1);

	m_pGuard = Create_Qutex();
}

void CSerialPort::StartTimer(void)
{
	_timerfd_settime(m_timer, 0, 5, true);
}

void CSerialPort::TermObjects(void)
{
	_close(m_term);

	_close(m_timer);

	_close(m_send);

	_close(m_done);

	AfxRelease(m_pGuard);
}

void CSerialPort::InitThread(void)
{
	m_pThread = CreateClientThread(this, 0, 0);
}

void CSerialPort::TermThread(void)
{
	SetEvent(m_term);

	m_pThread->Wait(FOREVER);

	m_pThread->Release();
}

void CSerialPort::SetEvent(int fd)
{
	static INT64 n = 1;

	_write(fd, &n, sizeof(n));
}

void CSerialPort::ClearEvent(int fd)
{
	static INT64 n = 0;

	_write(fd, &n, sizeof(n));
}

BOOL CSerialPort::ReadEvent(int fd)
{
	INT64 n = 0;

	_read(fd, &n, sizeof(n));

	return n ? TRUE : FALSE;
}

// Interrupts

void CSerialPort::EnterGuarded(void)
{
	m_pGuard->Wait(FOREVER);
}

void CSerialPort::LeaveGuarded(void)
{
	m_pGuard->Free();
}

// Implementation

BOOL CSerialPort::PortRecycle(void)
{
	AfxTrace("port %s removed\n", PCSTR(m_Name));

	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	PortClose();

	BOOL fInstall = TRUE;

	if( m_uSled == 10 ) {

		AfxTrace("removing g_serial\n");

		pLinux->CallProcess("/sbin/modprobe", "-r g_serial", NULL, NULL);
	}

	for( ;;) {

		if( _select_read(m_term, 200) == 1 ) {

			ReadEvent(m_term);

			return FALSE;
		}

		if( m_uSled == 10 ) {

			CAutoFile File("/sys/module/dwc3_omap/initstate", "r");

			if( !File || File.GetLine() != "live" ) {

				continue;
			}

			if( fInstall ) {

				AfxTrace("restoring g_serial\n");

				pLinux->CallProcess("/sbin/modprobe", "g_serial", NULL, NULL);

				AfxTrace("restored g_serial\n");

				fInstall = FALSE;
			}
		}

		if( PortOpen() ) {

			PortSetFormat();

			PortSetPhysical();

			if( m_pSwitch ) {

				m_pSwitch->EnablePort(m_uUnit, true);
			}

			AfxTrace("port %s restored\n", PCSTR(m_Name));

			return TRUE;
		}
	}
}

BOOL CSerialPort::PortOpen(void)
{
	if( (m_fd = _open(m_Name, O_RDWR | O_NOCTTY, 0)) >= 0 ) {

		return TRUE;
	}

	return FALSE;
}

void CSerialPort::PortClose(void)
{
	_close(m_fd);

	m_fd = -1;
}

BOOL CSerialPort::PortSetFormat(void)
{
	struct termios2 cfg = { 0 };

	cfg.c_iflag = IGNBRK;

	cfg.c_cflag = CREAD | CLOCAL | BOTHER | (BOTHER << IBSHIFT) | CS8;

	if( m_Config.m_uDataBits == 7 ) {

		cfg.c_cflag &= ~CSIZE;

		cfg.c_cflag |=  CS7;
	}

	if( m_Config.m_uStopBits == 2 ) {

		cfg.c_cflag |= CSTOPB;
	}

	if( m_Config.m_uParity == 1 ) {

		cfg.c_cflag |= PARENB | PARODD;
	}

	if( m_Config.m_uParity == 2 ) {

		cfg.c_cflag |= PARENB;
	}

	cfg.c_ispeed = m_Config.m_uBaudRate;

	cfg.c_ospeed = m_Config.m_uBaudRate;

	return !_ioctl(m_fd, TCSETS2, &cfg);
}

BOOL CSerialPort::PortSetPhysical(void)
{
	if( m_pSwitch ) {

		BOOL fCtrl = FALSE;

		switch( m_Config.m_uPhysical ) {

			case physicalRS232:

				m_pSwitch->SetPhysical(m_uUnit, FALSE);

				m_pSwitch->SetFull(m_uUnit, TRUE);

				return TRUE;

			case physicalRS422Master:

				fCtrl = m_pSwitch->SetPhysical(m_uUnit, TRUE);

				m_pSwitch->SetFull(m_uUnit, TRUE);

				break;

			case physicalRS422Slave:

				fCtrl = m_pSwitch->SetPhysical(m_uUnit, TRUE);

				m_pSwitch->SetFull(m_uUnit, TRUE);

				break;

			case physicalRS485:

				fCtrl = m_pSwitch->SetPhysical(m_uUnit, TRUE);

				m_pSwitch->SetFull(m_uUnit, FALSE);

				break;
		}

		if( m_uPort < 3 ) {

			if( m_Config.m_uPhysical == physicalRS485 ) {

				UINT wb = 1 + m_Config.m_uDataBits + (m_Config.m_uParity ? 1 : 0) + m_Config.m_uStopBits;

				UINT nb = 4 * wb + 12;

				m_uWait = nb * 1000000 / m_Config.m_uBaudRate;
			}
		}

		if( fCtrl ) {

			struct serial_rs485 r = { 0 };

			r.delay_rts_after_send  = 0;
			r.delay_rts_before_send = 0;
			r.flags		        = SER_RS485_ENABLED | SER_RS485_RTS_AFTER_SEND;

			_ioctl(m_fd, TIOCSRS485, &r);
		}
	}

	return TRUE;
}

// End of File
