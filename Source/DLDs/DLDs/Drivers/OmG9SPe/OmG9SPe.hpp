#include "ofins.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omron G9SP via Fins Master UDP Driver
//

class COmronFinsG9spMasterUDPDriver : public COmronFinsMasterDriver
{
	public:
		// Constructor
		COmronFinsG9spMasterUDPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(void ) Service(void);

		
	protected:

		struct CMacAddr
		{
			DWORD  m_IP;
			WORD   m_Port;
			};

		// Device Context
		struct CContext : COmronFinsMasterDriver::CBaseCtx
		{
			DWORD	 m_IP1;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			UINT	 m_uLast;
			DWORD	 m_IP2;
			BOOL	 m_fAux;
			BYTE	 m_bDna2;
			BYTE	 m_bDa12;
			BYTE	 m_bDa22;
			BOOL	 m_fDirty;
			UINT	 m_uPoll;
			UINT	 m_uData;
			UINT	 m_uTime;
			BOOL     m_fConn;
			BYTE     m_bBuff[300];
			
			};

		// Data Members
		CContext * m_pCtx;
		CBuffer  * m_pTxBuff;
		CBuffer  * m_pRxBuff;
		PBYTE      m_pTxData;
		CMacAddr   m_RxMac;
		ISocket  * m_pSock;
		
		// Transport Layer
		BOOL Send(void);
		BOOL RecvFrame(void);
		BOOL Transact(void);
		BOOL CheckFrame(void);

		// Implementation
		BOOL Start(void);
		void AddData(void);
		UINT GetData(AREF Addr, PDWORD pData, UINT uCount);
		void PutFinsHeader(void);
		void AddByte(BYTE bByte);

		void GetLongsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount);
		void GetWordsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount);
		void GetBytesAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount);
		void GetNibblesAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount);
		void GetBitsAt (PDWORD pData, UINT uAt, UINT uIdnex, UINT uCount);

		// Helpers
		BOOL IsTimedOut(void);
		BOOL IsWriteOnly(UINT uTable, UINT uOffset);
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);	

		// Transport Header

		void AddTransportHeader(void);
		BOOL StripTransportHeader(void);

		
	};

// End of File
