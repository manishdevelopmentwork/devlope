
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_APPOBJECT_GOOGLE_HPP

#define INCLUDE_APPOBJECT_GOOGLE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "AppObject.hpp"

#include "ConfigBlob.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Google Test Key
//

static BYTE m_bPrivate[] = {
	#include "google-private.pem.dat"
	0
	};

//////////////////////////////////////////////////////////////////////////
//
// Google Service
//

extern IService * Create_CloudServiceGoogle(void);

//////////////////////////////////////////////////////////////////////////
//
// Service Creation
//

void CAppObject::CreateService(void)
{
	m_pService = Create_CloudServiceGoogle();
	}

void CAppObject::CreateServiceBlob(CConfigBlob &Blob)
{
	// CloudServiceCrimson

	Blob.AddWord(0x1234);		// Marker
	Blob.AddCode(C3INT(1));		// Enable
	Blob.AddByte(0);		// Service
	Blob.AddCode(0);		// Status
	Blob.AddCode(L"");		// Ident
	}

void CAppObject::CreateOptionsBlob(CConfigBlob &Blob)
{
	// MqqtClientOptions

	CString Peer1 = "mqtt.googleapis.com";

	CString Peer2 = "";
	
	CString Name  = "";

	Blob.AddWord(0x1234);	// Marker
	Blob.AddByte(1);	// fDebug
	Blob.AddByte(1);	// fTls
	Blob.AddByte(0);	// uCheck
	Blob.AddWord(8883);	// uPort
	Blob.AddByte(1);	// PubQos
	Blob.AddByte(1);	// SubQos
	Blob.AddText(Peer1);	// PeerName[0]
	Blob.AddText(Peer2);	// PeerName[1]
	Blob.AddText(Name);	// ClientId
	Blob.AddText("");	// UserName
	Blob.AddText("");	// Password
	Blob.AddWord(30);	// ConnTimeout
	Blob.AddWord(15);	// SendTimeout
	Blob.AddWord(15);	// RecvTimeout
	Blob.AddWord(5);	// BackOffTime
	Blob.AddWord(30);	// BackOffMax
	Blob.AddWord(600);	// KeepAlive

	// MqttQueuedClientOptions

	Blob.AddByte(0);	// Buffer

	// MqttClientOptionsCrimson

	Blob.AddByte(0);	// Mode
	Blob.AddByte(0);	// Reconn

	// MqttClientOptionsJson

	Blob.AddByte(0);	// Root
	Blob.AddByte(0);	// Code

	// MqqtClientOptionsGoogle

	Blob.AddText("reliable-plasma-217900");	// Project
	Blob.AddText("us-central1");		// Region
	Blob.AddText("registry");		// Registry
	Blob.AddText("device-01");		// Device

	Blob.AddFile(m_bPrivate, sizeof(m_bPrivate)-1);
	}

// End of File

#endif
