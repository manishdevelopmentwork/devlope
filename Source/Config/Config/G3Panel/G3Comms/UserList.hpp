
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UserList_HPP

#define INCLUDE_UserList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUserItem;

//////////////////////////////////////////////////////////////////////////
//
// User List
//

class DLLNOT CUserList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUserList(void);

		// Item Location
		CUserItem * GetItem(INDEX Index) const;
		CUserItem * GetItem(UINT  uPos)  const;
	};

// End of File

#endif
