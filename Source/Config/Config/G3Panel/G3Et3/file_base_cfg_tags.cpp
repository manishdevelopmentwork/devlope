
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data - "base0/cfg/tags"
//

// Constructor

CFileDataBaseCfgTags::CFileDataBaseCfgTags(void) : CFileData("/base0/cfg/tags")
{
	m_pData = m_Data;

	m_uSize = sizeof(m_Data);
	}

// Development

void CFileDataBaseCfgTags::Dump(void)
{
	AfxTrace(L"===============\n");

	AfxTrace(L"Properties for file %s\n", CString(GetFile()));

	AfxTrace(L"\tnot implemented\n");

	AfxTrace(L"===============\n");

	}

// End of File
