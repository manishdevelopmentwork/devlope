
#include "Intern.hpp"

#include "UsbNetworkCdcExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Network Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbNetworkCdcExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = New CUsbNetworkCdcExpansion(pDriver);

	return p;
	}

// Constructor

CUsbNetworkCdcExpansion::CUsbNetworkCdcExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansion(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbNetworkCdcExpansion::GetName(void)
{
	if( m_wVendor == vidExar ) {

		return "EXAR-NIC";
		}

	if( m_wVendor == vidTelit ) {

		return "LTE-NCM";
		}

	return "GMENET";
	}

UINT METHOD CUsbNetworkCdcExpansion::GetClass(void)
{
	return rackClassNetwork;
	}

IDevice * METHOD CUsbNetworkCdcExpansion::MakeObject(IUsbHostFuncDriver *pDriver)
{
	return Create_UsbNetworkCdc(pDriver);
	}

// End of File
