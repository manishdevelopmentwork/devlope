
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDevNet_HPP

#define	INCLUDE_UsbDevNet_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb DeviceNet Port
//

class CUsbDevNet : public CUsbPort, public IDeviceNetPort, public IEventSink
{
public:
	// Constructor
	CUsbDevNet(IUsbHostFuncDriver *pDriver);

	// Destructor
	~CUsbDevNet(void);

	// Init
	void Bind(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IPortObject
	void  METHOD Bind(IPortHandler *pHandler);
	void  METHOD Bind(IPortSwitch  *pSwitch);
	UINT  METHOD GetPhysicalMask(void);
	BOOL  METHOD Open(CSerialConfig const &Config);
	void  METHOD Close(void);
	void  METHOD Send(BYTE bData);
	void  METHOD SetBreak(BOOL fBreak);
	void  METHOD EnableInterrupts(BOOL fEnable);
	void  METHOD SetOutput(UINT uOutput, BOOL fOn);
	BOOL  METHOD GetInput(UINT uInput);
	DWORD METHOD GetHandle(void);

	// IDeviceNetPort
	void METHOD SetMac(BYTE bMac);
	BOOL METHOD EnableBitStrobe(WORD wRecvSize, WORD wSendSize);
	BOOL METHOD EnablePolled(WORD wRecvSize, WORD wSendSize);
	BOOL METHOD EnableData(WORD wRecvSize, WORD wSendSize);
	BOOL METHOD IsOnLine(void);
	UINT METHOD Read(UINT uId, PBYTE pData, UINT uSize);
	UINT METHOD Write(UINT uId, PCBYTE pData, UINT uSize);

	// IEventSink
	void OnEvent(UINT uLine, UINT uParam);

protected:
	// Data
	struct CData
	{
		UINT	 m_uId;
		PBYTE	 m_pData;
		UINT	 m_uSize;
		bool	 m_fDirty;
	};

// Data
	IUsbHostDevNet * m_pDriver;
	ITimer         * m_pTimer;
	bool		 m_fOnLine;
	BYTE		 m_bMac;
	UINT		 m_uBaud;
	bool		 m_fWaitRecv;
	bool		 m_fWaitSend;
	BYTE		 m_bBuffRecv[1024];
	BYTE		 m_bBuffSend[1024];
	CData		 m_Data[4];
	UINT   volatile  m_uHeartbeat;
	WORD		 m_wBitRecv;
	WORD		 m_wBitSend;
	WORD		 m_wPollRecv;
	WORD		 m_wPollSend;
	WORD		 m_wDataRecv;
	WORD		 m_wDataSend;

	// Overridables
	void OnDriverBind(IUsbHostFuncDriver *pDriver);
	void OnInitDriver(void);
	void OnStartDriver(void);
	void OnStopDriver(void);
	void OnTermDriver(void);

	// Implementation 
	void InitData(void);
	void FreeData(void);
	void CheckRead(void);
	void WaitWrite(void);

	// Buffers
	void InitBuffers(void);
	UINT FindBuffer(UINT uId);
	UINT AllocBuffer(UINT uId, UINT uSize);
	void FreeBuffers(void);
};

// End of File

#endif
