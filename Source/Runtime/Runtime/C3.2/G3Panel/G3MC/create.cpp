
#include "intern.hpp"

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Externals
//

extern CModule * Create_GraphiteModule(void);
extern CModule * Create_GraphiteModule32(void);
extern CModule * Create_GMPID1(void);
extern CModule * Create_GMPID2(void);
extern CModule * Create_GMSG1(void);
extern CModule * Create_ManticoreModule(void);
extern CModule * Create_ManticorePid1Module(void);
extern CModule * Create_ManticorePid2Module(void);

//////////////////////////////////////////////////////////////////////////
//
// Module Configuration
//

CModule * CModule::Create(BYTE Model)
{
	switch( 0x1000 | Model ) {
		
		case ID_GMPID1:	   return Create_GMPID1();
		case ID_GMPID2:	   return Create_GMPID2();
		case ID_GMUIN4:	   return Create_GraphiteModule();
		case ID_GMOUT4:	   return Create_GraphiteModule();
		case ID_GMDIO14:   return Create_GraphiteModule();
		case ID_GMTC8:	   return Create_GraphiteModule();
		case ID_GMINI8:	   return Create_GraphiteModule();
		case ID_GMINV8:    return Create_GraphiteModule();
		case ID_GMSG1:	   return Create_GMSG1();
		case ID_GMRTD6:    return Create_GraphiteModule();
		case ID_GMRC:	   return Create_GraphiteModule32();
		case ID_DADIDO:	   return Create_ManticoreModule();
		case ID_DADIRO:	   return Create_ManticoreModule();
		case ID_DAUIN6:	   return Create_ManticoreModule();
		case ID_DAPID1_RA: return Create_ManticorePid1Module();
		case ID_DAPID1_SA: return Create_ManticorePid1Module();
		case ID_DAPID2_SM: return Create_ManticorePid2Module();
		case ID_DAPID2_RO: return Create_ManticorePid2Module();
		case ID_DAPID2_SO: return Create_ManticorePid2Module();
		case ID_DASG1:	   return Create_ManticoreModule();
		case ID_DAAO8:	   return Create_ManticoreModule();
		case ID_DARO8:	   return Create_ManticoreModule();
		case ID_DAMIX2:	   return Create_ManticoreModule();
		case ID_DAMIX4:	   return Create_ManticoreModule();
	}

	AfxTrace("no module for model %4.4X\n", 0x1000 | Model);

	HostBreak();

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Externals
//

#if 0

extern CModule * Create_SLC  (void);
extern CModule * Create_DLC  (void);
extern CModule * Create_IN8  (void);
extern CModule * Create_RTD6 (void);
extern CModule * Create_DIO14(void);
extern CModule * Create_SG   (void);
extern CModule * Create_OUT4 (void);
extern CModule * Create_TC8I (void);

#endif

//////////////////////////////////////////////////////////////////////////
//
// Module Configuration
//

#if 0

CModule * CModule::Create(BYTE Model)
{
	CModule *pModule = NULL;

	switch( Model ) {

		case ID_CSPID1:	  pModule = Create_SLC  ();	break;
		case ID_CSPID2:	  pModule = Create_DLC  ();	break;
		case ID_CSTC8:	  pModule = Create_IN8  ();	break;
		case ID_CSINI8:	  pModule = Create_IN8  ();	break;
		case ID_CSINV8:	  pModule = Create_IN8  ();	break;
		case ID_CSRTD6:	  pModule = Create_RTD6 ();	break;
		case ID_CSINI8L:  pModule = Create_IN8  ();	break;
		case ID_CSINV8L:  pModule = Create_IN8  ();	break;
		case ID_CSDIO14:  pModule = Create_DIO14();	break;
		case ID_CSSG:	  pModule = Create_SG   ();	break;
		case ID_CSOUT4:	  pModule = Create_OUT4 ();	break;
		case ID_CSTC8ISO: pModule = Create_TC8I ();	break;

		}

	if( !pModule ) {

		AfxTrace("no module for model %4.4X\n", Model);		
		}

	return pModule;
	}

#endif

// End of File
