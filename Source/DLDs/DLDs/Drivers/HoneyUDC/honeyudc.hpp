//////////////////////////////////////////////////////////////////////////
//
// Honeywell UDC9000 Data Spaces
//

#define	SPR	1	// Registers
#define	SPIW	2	// I/O Words
#define	SPIB	3	// I/O Bits - needed for Gateway Blocks
#define	SPM	4	// Mode Select
#define	SPS	5	// Status Request
#define	SPT	6	// Device/Comms Status
#define	SPB	7	// Control Block I/O
#define	SPC	8	// Control Block Config

// Identification
#define	IDLOOP	9	// Loopback
#define	IDREAD	0x9C	// Monitor + General Data xfer
#define	IDWRITE	0x1D	// Write Enable + General Data xfer + Write

// Commands
#define	CMODES	0x00	// Mode Select
#define	CSTATUS	0x01	// LCM and ELPM Status
#define	CLONG	0x02	// Contiguous 32 Bit Values
#define	CSREAL	0x03	// Scattered 32 Bit Registers
#define	CIOBIT	0x20	// Contiguous I/O Points
#define	CREGS	0x22	// Unsigned Registers
#define	CIOWORD	0x2A	// Packed I/O

// Relevant Response Positions
#define	RWLEN	1
#define	RBTASK	14
#define	RBSTAT	15
#define	RBID	16
#define	RBQTY	17
#define	RDATAM	17
#define	RLOOPB	17
#define	RWADD	18
#define	RDATAS	18
#define	RDATA32	18
#define	RDATA16	20
#define	RDATAIO	20

#define	LOOPBACKVALUE	0xAA

//////////////////////////////////////////////////////////////////////////
//
// Honeywell UDC9000 Driver
//

class CHoneywellUDC9000 : public CMasterDriver
{
	public:
		// Constructor
		CHoneywellUDC9000(void);

		// Destructor
		~CHoneywellUDC9000(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			ISocket *m_pSock;
			UINT	 m_uLast;
			UINT	 m_uStatus;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTx[300];
		BYTE	   m_bRx[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;

		// Read Handlers
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoBitRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoRealWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
		
		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
				
		// Transport Layer
		BOOL	SendFrame(void);
		BOOL	RecvFrame(void);
		BOOL	Transact(void);

		// Helpers
		CCODE	LoopBack(void); // Connection
		UINT	AddWordHeader(AREF Addr, UINT uCount);
		void	AddLongHeader(AREF Addr, UINT uCount);
		void	AddBitsHeader(AREF Addr, UINT uCount);

		// Socket Management
		BOOL	CheckSocket(void);
		BOOL	OpenSocket(void);
		void	CloseSocket(BOOL fAbort);

	};

// End of File
