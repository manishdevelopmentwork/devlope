
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_PersistManager_HPP

#define INCLUDE_PersistManager_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPersistPage;

//////////////////////////////////////////////////////////////////////////
//
// Persistent Data Manager
//

class DLLNOT CPersistManager : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPersistManager(void);

		// Destructor
		~CPersistManager(void);

		// Operations
		void  Register(DWORD dwAddr, UINT uSize);
		DWORD Allocate(UINT uSize);
		void  Free(DWORD dwAddr, UINT uSize);
		void  Free(void);

	protected:
		// Data Members
		CPersistPage * m_pPage[8];

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		UINT GetPageLimit(void);
	};

// End of File

#endif
