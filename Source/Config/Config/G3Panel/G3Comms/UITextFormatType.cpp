
#include "Intern.hpp"

#include "UITextFormatType.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Format Type
//

// Dynamic Class

AfxImplementDynamicClass(CUITextFormatType, CUITextEnumPick);

// Constructor

CUITextFormatType::CUITextFormatType(void)
{
	}

// Overridables

void CUITextFormatType::OnBind(void)
{
	CUITextElement::OnBind();

	if( m_UIData.m_Format == "n" ) {

		m_uMask = 0x0FF;
		}

	if( m_UIData.m_Format == "f" ) {

		m_uMask = 0x042;
		}

	if( m_UIData.m_Format == "s" ) {

		m_uMask = 0x103;
		}

	AddData();
	}

// Implementation

void CUITextFormatType::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

void CUITextFormatType::AddData(void)
{
	AddData(0, CString(IDS_GENERAL));	// 0x0001
	AddData(7, CString(IDS_LINKED));	// 0x0002
	AddData(1, CString(IDS_NUMERIC));	// 0x0004
	AddData(2, CString(IDS_SCIENTIFIC));	// 0x0008
	AddData(3, CString(IDS_TIME_AND_DATE));	// 0x0010
	AddData(4, CString(IDS_IP_ADDRESS));	// 0x0020
	AddData(5, CString(IDS_TWOSTATE));	// 0x0040
	AddData(6, CString(IDS_MULTISTATE));	// 0x0080
	AddData(8, CString(IDS_STRING));        // 0x0100
	}

// End of File
