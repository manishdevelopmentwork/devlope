
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SCALE_HPP
	
#define	INCLUDE_SCALE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CScaleHelper;
class CPrimVertScale;

//////////////////////////////////////////////////////////////////////////
//
// Scale Helper
//

class CScaleHelper
{
	public:
		// Constructors
		CScaleHelper(void);
		CScaleHelper(double Min, double Max);

		// Operations
		void SetLimits(double Min, double Max);
		void CalcDecimalStep(double Limit);
		void CalcTimeStep(double Limit);
		
		// Attributes
		double GetDrawStep(void);
		double GetStepValue(int n);		
		int    GetStepCount(void);
		int    GetStepSpan(void);
		
	protected:
		// Data Members
		double m_DataMin;
		double m_DataMax;
		double m_DataSpan;
		int    m_StepMin;
		int    m_StepMax;
		double m_DrawStep;

		// Implementation
		double FindDrawStep(double Limit);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Scale
//

class CPrimVertScale : public CPrimRich
{
	public:
		// Constructor
		CPrimVertScale(void);

		// Destructor
		~CPrimVertScale(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		UINT         m_Precise;
		UINT         m_UseAll;
		UINT         m_ShowLabels;
		UINT         m_ShowUnits;
		UINT         m_Orientation;
		CPrimColor * m_pLineCol;
		CPrimColor * m_pTextCol;
		CPrimColor * m_pUnitCol;
		UINT         m_TextFont;
		UINT         m_UnitFont;
		UINT	     m_FontBase;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR  m_LineCol;
			COLOR  m_TextCol;
			COLOR  m_UnitCol;
			C3REAL m_DataMin;
			C3REAL m_DataMax;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx         m_Ctx;

		// Layout Data
		int          m_x1;
		int          m_x2;
		int          m_y1;
		int          m_y2;
		int          m_fy;
		int          m_fm;
		int          m_ySize;
		CScaleHelper m_Helper;

		// Implementation
		C3REAL GetMinScale(void);
		C3REAL GetMaxScale(void);
		BOOL   AdjustLimit(C3REAL &v);

		// Context Creation
		void FindCtx(CCtx &Ctx);
		void FindLayout(IGDI *pGDI);
	};

// End of File

#endif
