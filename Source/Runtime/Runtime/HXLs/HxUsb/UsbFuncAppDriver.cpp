
#include "Intern.hpp"

#include "UsbFuncAppDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Function Application Driver
//

// Constructor

CUsbFuncAppDriver::CUsbFuncAppDriver(void)
{
	m_pLowerDrv = NULL;

	m_pRunning  = Create_ManualEvent();

	m_fOpen     = false;
	}

// Destructor

CUsbFuncAppDriver::~CUsbFuncAppDriver(void)
{
	m_pRunning->Release();

	AfxRelease(m_pLowerDrv);
	}

// IUnknown

HRESULT CUsbFuncAppDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbFuncEvents);

	StdQueryInterface(IUsbFuncEvents);

	StdQueryInterface(IUsbEvents);

	return E_NOINTERFACE;
	}

ULONG CUsbFuncAppDriver::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbFuncAppDriver::Release(void)
{
	StdRelease();
	}

// IUsbEvents

void CUsbFuncAppDriver::OnBind(IUsbDriver *pDriver)
{
	AfxRelease(m_pLowerDrv);

	m_pLowerDrv = NULL;

	pDriver->QueryInterface(AfxAeonIID(IUsbFuncCompDriver), (void **) &m_pLowerDrv);
	
	if( m_pLowerDrv ) {

		m_iInterface = m_pLowerDrv->FindInterface(this);
		}
	}

void CUsbFuncAppDriver::OnInit(void)
{
	}

void CUsbFuncAppDriver::OnStart(void)
{
	}

void CUsbFuncAppDriver::OnStop(void)
{
	}

// IUsbFuncEvents

void CUsbFuncAppDriver::OnState(UINT uState)
{
	if( uState == devConfigured ) {

		m_fOpen = true;

		m_pRunning->Set();
		}
	else {
		m_fOpen = false;

		m_pRunning->Clear();
		}
	}

BOOL CUsbFuncAppDriver::OnSetupClass(UsbDeviceReq &Req)
{
	return false;
	}

BOOL CUsbFuncAppDriver::OnSetupVendor(UsbDeviceReq &Req)
{
	return false;
	}

BOOL CUsbFuncAppDriver::OnSetupOther(UsbDeviceReq &Req)
{
	return false;
	}

BOOL CUsbFuncAppDriver::OnGetDevice(UsbDesc &Desc)
{
	return false;
	}

BOOL CUsbFuncAppDriver::OnGetQualifier(UsbDesc &Desc)
{
	return false;
	}

BOOL CUsbFuncAppDriver::OnGetConfig(UsbDesc &Desc)
{
	return false;
	}

BOOL CUsbFuncAppDriver::OnGetInterface(UsbDesc &Desc)
{
	return false;
	}

BOOL CUsbFuncAppDriver::OnGetEndpoint(UsbDesc &Desc)
{
	return false;
	}

BOOL CUsbFuncAppDriver::OnGetString(UsbDesc *&pDesc, UINT i)
{
	return false;
	}

// Transfer Helpers

BOOL CUsbFuncAppDriver::Shutdown(void)
{
	return m_pLowerDrv ? m_pLowerDrv->Stop() : false; 
	}

BOOL CUsbFuncAppDriver::WaitRunning(DWORD dwTimeout)
{
	return m_pRunning->Wait(dwTimeout);
	}

UINT CUsbFuncAppDriver::GetMaxPacket(UINT iEndpt)
{
	return m_pLowerDrv ? m_pLowerDrv->GetMaxPacket(iEndpt) : 0;
	}

BOOL CUsbFuncAppDriver::SendCtrlStatus(UINT iEndpt)
{
	return m_pLowerDrv ? m_pLowerDrv->SendCtrlStatus(iEndpt) : false;
	}

BOOL CUsbFuncAppDriver::RecvCtrlStatus(UINT iEndpt)
{
	return m_pLowerDrv ? m_pLowerDrv->RecvCtrlStatus(iEndpt) : false;
	}

BOOL CUsbFuncAppDriver::SendCtrl(UINT iEndpt, PCBYTE pData, UINT uLen)
{
	return m_pLowerDrv ? m_pLowerDrv->SendCtrl(iEndpt, pData, uLen) : false;
	}

BOOL CUsbFuncAppDriver::RecvCtrl(UINT iEndpt, PBYTE  pData, UINT uLen)
{
	return m_pLowerDrv ? m_pLowerDrv->RecvCtrl(iEndpt, pData, uLen) : false;
	}

UINT CUsbFuncAppDriver::SendBulk(UINT iEndpt, PCBYTE pData, UINT uLen, UINT uTimeout)
{
	return m_pLowerDrv ? m_pLowerDrv->SendBulk(iEndpt, pData, uLen, uTimeout) : NOTHING;
	}

UINT CUsbFuncAppDriver::RecvBulk(UINT iEndpt, PBYTE  pData, UINT uLen, UINT uTimeout)
{
	return m_pLowerDrv ? m_pLowerDrv->RecvBulk(iEndpt, pData, uLen, uTimeout) : NOTHING;
	}

BOOL CUsbFuncAppDriver::SetStall(UINT iEndpt, BOOL fStall)
{
	return m_pLowerDrv ? m_pLowerDrv->SetStall(iEndpt, fStall) : false;
	}

BOOL CUsbFuncAppDriver::GetStall(UINT iEndpt)
{
	return m_pLowerDrv ? m_pLowerDrv->GetStall(iEndpt) : false;
	}

// End of File
