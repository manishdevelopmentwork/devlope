
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NativeTcpSocket_HPP

#define INCLUDE_NativeTcpSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BaseSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP Socket Object
//

class CNativeTcpSocket : public CBaseSocket
{
public:
	// Constructor
	CNativeTcpSocket(void);

	// Destructor
	~CNativeTcpSocket(void);

	// ISocket
	HRM Listen(IPADDR const &IP, WORD Loc);
	HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc);
	HRM GetPhase(UINT &Phase);
	HRM SetOption(UINT uOption, UINT uValue);
	HRM Abort(void);
	HRM Close(void);

	// Initialization
	static void Init(void);
	static void Term(void);

protected:
	// Socket Binding
	struct CBind
	{
		CIpAddr	m_Addr;
		WORD    m_Port;
		WORD	m_Pad;

		CBind(CIpAddr const &Addr, WORD Port) : m_Addr(Addr)
		{
			m_Port = Port;
			m_Pad  = 0;
		}

		CBind(CBind const &That) : m_Addr(That.m_Addr)
		{
			m_Port = That.m_Port;
			m_Pad  = That.m_Pad;
		}

		friend int AfxCompare(CBind const &a, CBind const &b)
		{
			return memcmp(&a, &b, sizeof(CBind));
		}
	};

	// Listen Record
	struct CListen
	{
		CListen(CBind const &Bind, int fd) : m_Bind(Bind)
		{
			m_hSocket = fd;
			m_uCount  = 1;
		}

		CBind	m_Bind;
		int	m_hSocket;
		UINT	m_uCount;
	};

	// Access Lock
	static IMutex * m_pLock;

	// Listen Map
	static CMap<CBind, CListen *> m_Listen;

	// Data Members
	CListen * m_pListen;

	// Implementation
	BOOL CheckRemoteClose(BOOL fDone);
	BOOL HasError(void);
	void SetAbortMode(void);
	BOOL ListenAdjust(void);
	BOOL ListenDelete(void);
	BOOL ListenAbort(void);
};

// End of File

#endif
