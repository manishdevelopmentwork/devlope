
//////////////////////////////////////////////////////////////////////////
//
// Julabo Serial Driver
//

#define JULABO_ID 0x4068

#define	SZBUFF	80	// max string size

// Table Definitions
enum {
	JSPT	=  1,
	JPAR	=  2,
	JPV	=  3,
	JHIL	=  4,
	JMODE	=  5,
	JSTAT	=  6,
	JSTSTR	=  7,
	JVERS	=  8,
	JUSRS	= 15,
	JUSRC	= 16,
	JUSRR	= 17
	};

#define	NEGZERO	 0x80000000

#define	USRALLOW JULABO_ID // (16488)

//////////////////////////////////////////////////////////////////////////
//
// Julabo Driver
//

class CJulaboSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CJulaboSerialDriver(void);

		// Destructor
		~CJulaboSerialDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);
		DEFMETH(void)	CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Open(void);

		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			UINT	m_uUser;
			BOOL	m_fUseAddress;

			char	m_Drop[4];

			UINT	m_uWriteErrCt;

			BYTE	m_pbUSRCMD[SZBUFF];
			BYTE	m_pbUSRRSP[SZBUFF];
			};

		CContext *m_pCtx;

		// Data Members

		LPCTXT	m_pHex;
		LPCTXT	m_pIn;
		LPCTXT	m_pOut;
		LPCTXT	m_pStat;
		LPCTXT	m_pVers;
		LPCTXT	m_pSP;
		LPCTXT	m_pPar;
		LPCTXT	m_pPV;
		LPCTXT	m_pHil;
		LPCTXT	m_pMode;
		LPCTXT	m_pDisabled;

		BYTE	m_bTx[SZBUFF];
		BYTE	m_bRx[SZBUFF];

		UINT	m_uTxPtr;
		UINT	m_uRxPtr;
		UINT	m_uID;

		BOOL	m_fRead;
		BOOL	m_fList;
		BOOL	m_fPrevListItem;

		// Frame Building
		void	StartFrame(UINT uOffset);
		void	AddUnitAddress(void);
		void	AddCommand(UINT uOffset);
		void	AddInOut(void);
		void	AddStringCmd(void);
		void	AddTypeCmd(void);
		void	AddParameter(UINT uOffset);
		void	AddWriteData(DWORD dData, UINT uType);
		void	AddByte(BYTE bData);
		void	AddText(char * sText);
		void	EndFrame(void);

		// Transport Layer
		BOOL	Transact(BOOL fDataReply);
		void	Send(void);
		BOOL	GetReply(BOOL fDataReply);
		BOOL	CheckAddrHeader(UINT uStart);
		BOOL	GetResponse(PDWORD pData, UINT *pCount);
		void	StoreStringResponse(void);

		// Port Access
		void	Put(void);
		UINT	Get(UINT uTimer);

		// Internal access
		BOOL	NoReadTransmit (PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		BOOL	IsStringCmd(void);
		BOOL	IsSendStringReq(void);
		CCODE	CheckWrite(UINT uCount);
		BOOL	IsNumChar(char *p);
		BOOL	IsDecDigit(char c);
		void	DoBuffer(char *pDest, char *pSrc, UINT uCount, UINT uMax);
		BOOL	IsXON_XOFF(BYTE b);
		void	CheckList(UINT uCount);
		void	ClearBuff(PBYTE pBuff);
		void	CopyStringData(PDWORD pData, PBYTE pSrc, UINT uByteCount);
	};

// End of File
