
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PROFIBUSDRIVER_HPP
	
#define	INCLUDE_PROFIBUSDRIVER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Profibus Driver Options
//

class CProfibusDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProfibusDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Station;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Driver
//

class CProfibusDPDriver : public CBasicCommsDriver 
{
	public:
		// Constructor
		CProfibusDPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);


		// Implementation
		CString GetTypeModifier(UINT uType);
		UINT    TypeFromModifier(CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Master Driver
//

class CProfibusDPMasterDriver : public CProfibusDPDriver 
{
	public:
		// Constructor
		CProfibusDPMasterDriver(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP via MPI Option Card Driver
//

class CDPviaMPIDriver : public CProfibusDPDriver
{
	public:
		// Constructor
		CDPviaMPIDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Slave Driver
//

class CProfibusDPSlaveDriver : public CProfibusDPDriver 
{
	public:
		// Constructor
		CProfibusDPSlaveDriver(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Profibus Address Selection Dialog
//

class CProfibusDPDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CProfibusDPDialog(CProfibusDPDriver &Driver, CAddress &Addr);
		                
	protected:
		// Data
		CProfibusDPDriver  * m_pDriver;
		CAddress  	  * m_pAddr;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void SetCaption(void);
		void LoadBlocks(void);
		void LoadTypes(void);
		void LoadAddress(void);
		void GetAddress(void);
	};

// End of File

#endif
