
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MJ1939_HPP
	
#define	INCLUDE_MJ1939_HPP

//////////////////////////////////////////////////////////////////////////
//
// J1939 Base Driver
//

#include "j1939.hpp"


//////////////////////////////////////////////////////////////////////////
//
// Monico J1939 Driver
//

class CMonicoJ1939Driver : public CCANJ1939Driver
{
	public:
		// Constructor
		CMonicoJ1939Driver(void);
		
		// Data Access
		CString	GetFullPGString(CPGN * pPGN, BOOL fNum);
		CString	GetPGString(CPGN * pPGN, UINT uOffset);
	};

// End of File

#endif
