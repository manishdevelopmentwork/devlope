//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Extended Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

class CBSAPExtSMDriver;

struct	TIMESYNCFRAME {
	BYTE	Day;
	BYTE	Month;
	BYTE	YearLo;
	BYTE	YearHi;
	BYTE	Hour;
	BYTE	Minute;
	BYTE	Second;
	BYTE	JulianDayLo;	// JD 1 = 1 Jan 1977
	BYTE	JulianDayHi;	// JD 1 = 1 Jan 1977
	BYTE	Julian4SecLo;
	BYTE	Julian4SecHi;
	BYTE	Julian20mSec;
	};

struct	NRTFRAME {
	TIMESYNCFRAME TSF;
	BYTE	Version;
	BYTE	GlobAddrLo;
	BYTE	GlobAddrHi;
	BYTE	UpDnMaskLo;
	BYTE	UpDnMaskHi;
	BYTE	Level;
	BYTE	LSC[7];
	BYTE	LBM[7];
	};

struct BSAPDRVCFG {

	BYTE	NetLevel;
	BYTE	ThisLocal;
	BYTE	PollHigh;
	BYTE	LevelBitMask[7];	// these are used when NetLevel == 0
	UINT	PollTimeout;
	WORD	ThisGlobal;
	WORD	SourceGlobal;
	WORD	SendGlobal;
	BOOL	NRTSent;
	BOOL	NRTRcvd;
	BOOL	GlobalDest;
	TIMESYNCFRAME	TThis;
	TIMESYNCFRAME	TThat;
	TIMESYNCFRAME	TMast;
	NRTFRAME	NThis;
	NRTFRAME	NThat;
	NRTFRAME	NMast;
	BYTE	NRTCount;
	};

// String Constants

#define MAX_CHARS	80
#define MAX_LTEXT	(MAX_CHARS / sizeof(DWORD))

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Extended Master Driver
//

class CBSAPExtSMDriver : public CMasterDriver
{
	public:
		// Constructor
		CBSAPExtSMDriver(void);

		// Destructor
		~CBSAPExtSMDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);
		DEFMETH(void)	CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(WORD)	GetMasterFlags(void);
		DEFMETH(void)	Detach(void);

		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE)	Ping(void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(UINT)	DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

	protected:
		struct PENDING {

			CAddress	dAddr;
			UINT		uIndex;
			UINT		uCount;
			BYTE		LevDiff;
			WORD		wAppSeq;
			UINT		uState;
			};

#define	PENDSIZE (sizeof(PENDING))
#define PPENDING (PENDING *)
#define	PPPEND   (PENDING **)

		struct PPEND {

			UINT		uPendPtrCt;
			UINT		uListCt;
			PENDING		**ppPendList;
			PENDING		*pCurrent;
			};

		// String support
		struct CStringTag
		{
			char  m_Text[MAX_CHARS];
			};

		// Device Context
		struct CContext {

			BYTE	m_bLocal;
			WORD	m_wGlobal;
			BYTE	m_bLevel;
			BYTE	m_bPath;
			BYTE	m_bMLevel;
			BYTE	m_bSerNum;

			// Protocol information
			UINT	m_uMsgSeq;
			UINT	m_uAppSeq;
			UINT	m_uACCOLVers;
			UINT	m_uMSDVers;

			BYTE	m_bIsC3;

			// Tag information
			// Config Lists
			PBYTE	m_pbDevName;
			PDWORD	m_pdAddrList;
			PDWORD	m_pdLNumList;
			PWORD	m_pwMSDList;
			PBOOL   m_pfMSDList;
			PBYTE	m_pbTypeList;
			PBYTE	m_pbDevList;
			PBYTE	m_pbLevList;
			PBYTE	m_pbNameList;
			PWORD	m_pwNameIndex;
			PWORD	m_pwGlobList;
			PBYTE	m_pbDevAddList;
			PBYTE	m_pbLevAddList;
			BYTE	m_bDevAddList[16];
			BYTE	m_bLevAddList[16];
			UINT	m_uTagCount;

			// String Tags
			UINT	     m_uStrings;
			CStringTag * m_pStrings;

			// Alarm Settings
			DWORD	m_dAutoAckBool;
			DWORD	m_dAutoAckReal;

			DWORD	m_dCfgMagic;

			// Pending Response control
			DWORD	m_dPendMagic;
			PPEND	m_Pend;
			PPEND	*m_pPend;
			};

		CContext * m_pCtx;

		struct POLLRCVD
		{
			BYTE	Addr;
			BYTE	MsgSer;
			BYTE	DestFC;
			BYTE	PollPri;
			};

		struct	POLLRESP
		{
			BYTE	Addr;	// always 0;
			BYTE	MsgSer;
			BYTE	DestFC;
			BYTE	SlvAdd;
			BYTE	NodeSB;
			BYTE	NumBuff;
			};

		struct LOCMSGHEAD
		{
			BYTE	Addr;
			BYTE	MsgSer;
			};

		struct GLBMSGHEAD
		{
			BYTE	Addr;
			BYTE	MsgSer;
			BYTE	DAddrL;
			BYTE	DAddrH;
			BYTE	SAddrL;
			BYTE	SAddrH;
			BYTE	Control;
			};

		struct	ENDHEADLIST
		{
			BYTE	DestFC;
			BYTE	AppSeqL;
			BYTE	AppSeqH;
			BYTE	SrceFC;
			BYTE	NodeST;
			BYTE	DataSize;
			};

		struct	RBYADDRRX
		{
			BYTE	FuncCode;
			BYTE	FSS[5];
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	RBYNAMERX
		{
			BYTE	FuncCode;
			BYTE	FSS[5];
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	RSIGNALTX
		{
			BYTE	NodeStat;
			BYTE	RER;
			BYTE	NumElem;
			PBYTE	Elements;
			};

		struct	RBYLISTRX
		{
			BYTE	FuncCode;
			BYTE	FSS[5];
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	ListNum;
			BYTE	ListNum2L;
			BYTE	ListNum2H;
			};

		struct	RBYLISTTX
		{
			BYTE	NodeStat;
			BYTE	RER;
			BYTE	NumElem;
			BYTE	ListNum;
			};

		struct	WBYADDRRX
		{
			BYTE	FuncCode;
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	WBYNAMERX
		{
			BYTE	FuncCode;
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct VARINFO
		{
			DWORD	Addr;
			DWORD	Data;
			BOOL	Logical;
			UINT	NameInx;
			WORD	Offset;
			WORD	MSDAddr;
			WORD	Length;
			WORD	Index;
			BYTE	EER;
			BYTE	FSS[5];
			};

		struct READFRAME
		{
			UINT	uAddr;
			PDWORD	pData;
			UINT	uCount;
			UINT	uGood;
			BOOL	fDoMSD;
			BOOL	fReqMSD;
			BYTE	bSrcFC;
			};

		READFRAME	m_RFrame;
		READFRAME	*m_pRFrame;

		typedef VARINFO * PVARINFO;

		// Data Members
		CContext	*ContextList[15];
		IDevice		*DeviceList[15];

		// Extra Help
		IExtraHelper   * m_pExtra;

		// BSAP Device Config
		BSAPDRVCFG	m_BDrvCfg;
		BSAPDRVCFG	*m_pDC;

		// Master Controls
		BOOL		m_fGlobal;
		BYTE		m_bRER;		// Master Error Flag
		BYTE		m_bTimeSync;
		BOOL		m_fInAlarm;

		// Buffer Pointers
		PBYTE		m_pSave;
		TIMESYNCFRAME	TSyncFrameTx;
		TIMESYNCFRAME	TSyncFrameRx;
		NRTFRAME	NRTFrameTx;
		NRTFRAME	NRTFrameRx;

		// Variable info
		VARINFO	SVarInfo;
		VARINFO	*m_pVarInfo;
		WORD	m_wVarSel;
		WORD	m_uIndex;

		// Comms
		UINT	m_bDrop;
		UINT	m_uTPtr;
		BYTE	m_bTx [300];
		BYTE	m_bRx [300];
		BYTE	m_bPoll[16];
		BYTE	m_bAlm[24];
		BYTE	m_bNRT[40];
		PBYTE	m_pTx;
		UINT	m_uState;
		UINT	m_uETXPos;

		// List items
		UINT	m_uNameListItem;
		UINT	m_uListCount;
		BYTE	m_bList;

		// Allocated pointers
		PBYTE	m_pSend;

		// Field Selects
		BYTE	m_bFSS[5];

		// Global Pass Through
		CCODE	HandlePassThrough(CAddress Addr, PDWORD pData, UINT uCount, BOOL fIsRead);

		// utilities
		UINT	SetItemIndex(AREF Addr);
		UINT	GetItemCount(AREF Addr, UINT uMax);
		void	MakeBaseHeader(BYTE bSFC, BYTE bDFC, BOOL fIncSer);

		// Pending Handling
		UINT	PrepOperation(CAddress Addr, PDWORD pNewAddr, UINT *pCount, UINT *pGotData);
		DWORD	GetPendingRef(AREF Addr, UINT *pCount, UINT *pGotData);
		BOOL	SearchPending(AREF Addr, BOOL fAdd, BOOL fKill);
		BOOL	ExpandppList(PENDING *pNew, UINT uCurrentSize);

		// Read Handlers
		void	MakeReadHeader(void);
		CCODE	DoDataRead(AREF Addr, UINT uGotDataCount, UINT uReqCount);
		DWORD	GetReadData(AREF Addr, UINT *pOffset, PBYTE pEndString, UINT uIndex, UINT uReqCount);
		DWORD	GetStringData(AREF Addr, UINT &uPos, UINT uDPos, UINT uIndex, UINT uReqCount);
		BOOL	AddReadByName(void);
		BOOL	AddReadByAddr(void);
		void	StartReadByAddr(UINT uCount);
		BOOL	IsArrayRead(UINT *pNamePos);
		CCODE	DoArrayRead(CAddress Addr, PDWORD pData, UINT uCount, UINT uNamePos);
		BOOL	IsListRead(UINT *pNamePos);
		CCODE	DoListRead(CAddress Addr, PDWORD pData, UINT uCount, UINT uNamePos);
		BOOL	FindArraySpec(const char * Name, PWORD pConfig);
		BOOL	FindChar(char *pName, UINT *pPos, UINT uCharType);

		// Write routines
		CCODE	DoDataWrite(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	WriteByAddress(AREF Addr, PDWORD pData, UINT uCount);
		void    AddWriteData(UINT uType, DWORD dData);
		void	AddStringData(AREF Addr, PDWORD pData, UINT uIndex, UINT uCount);
		BOOL	WriteByName(void);
		void	AddWriteHeader(void);

		// String Support
		UINT GetStringContextIndex(AREF Addr);
		void SetStringContext(AREF Addr, PCTXT pText, UINT uCount);
		UINT GetStringCount(AREF Addr, UINT uPos, UINT uCount);
		UINT GetCharCount(PCTXT pText, UINT uOffset);
		void RemoveTrailing(PBYTE &pByte, BYTE bByte, UINT uBytes);

		// Polling
		UINT	MainPoll(void);
		UINT	PollPending(PENDING *pPending);
		void	StartPoll(void);
		BOOL	SynchPolls(void);
		void	SendDataAck(void);
		void	SendAlarmAck(void);
		BOOL	GetACCOLVersion(void);
		void	SendListRequest(UINT uList);

		// Construct Read Master Frame
		void	PutStart(BOOL fGlobal, BYTE bLocal);
		void	NextSeq(UINT *pSeq, BOOL fByte);
		WORD	DoCRC(PBYTE pBuff);

		// Field Select Helpers
		BOOL	ParseVarName(PCTXT Name, UINT uSelect, PBYTE pItem);
		BOOL	AddFieldSels(void);
		void	AddField1(void);
		void	AddField2(void);
		void	AddField3(void);

		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddReal(DWORD dwData);
		void	AddText(PCTXT pText);
		void	AddTextPlusNull(PCTXT pText);

		// Transport Layer
		void	MakeSend(void);
		UINT	SendFrame(void);
		BOOL	RecvFrame(void);
		UINT	Transact(BOOL fWantReply);

		// Handle Alarms
		UINT	CheckAlarms(CAddress Addr, PDWORD pData, UINT uCount);
		UINT	SendAlarmAck(PDWORD pData, UINT uCount, BYTE bType);
		BOOL	MakeAlarmAckFrame(BYTE bSFC, BYTE bDFC, PWORD pMSD, UINT uCount);
		BOOL	AlarmInit(void);
		void	SendAlarmReportAck(void);

		// NRT and Global Address
		void	MakeThisNRT(void);
		void	MakeThisGlobal(BOOL fDefault);
		void	BuildTimeSync(BOOL fForce);
		BOOL	BuildNRT(void);
		void	SendNRT(void);
		void	BuildLSC(void);
		BYTE	GetNRTLSC(BYTE bLevel);
		CCODE	HandleNRT(AREF Addr, PDWORD pData, UINT uCount);

		// RER
		void	SetRER(BYTE bRER);
		void	AddEER(BYTE bError);

		// Helpers
		void	InitGlobal(BOOL fInit);
		void	InitDevGlobal(void);
		BOOL	CheckDrop(BYTE bData);
		UINT	IsNRT(UINT uTable);
		UINT	IsBIT(UINT uTable);
		UINT	IsFLT(UINT uTable);
		UINT	IsSTR(UINT uTable);
		BOOL	IsSTRText(UINT uChar);
		UINT	SearchValue(UINT uValue, UINT uEnd, PDWORD pL, PWORD pW, UINT *pFound);
		void	AddContextPtr(CContext *pPtr, IDevice *pDev);
		UINT    GetCount(CAddress Addr, UINT uCount);
		UINT    ExpandCount(CAddress Addr, UINT uCount, UINT uMax = 0);

		// Tag Data Loading
		void	LoadTags(LPCBYTE &pData);
		void    InitStringSupport(LPCBYTE &pData);
		BOOL	StoreDevName(LPCBYTE &pData);
		void	StoreName(LPCBYTE &pData, UINT *pPosition, UINT uItem);

		// delete created buffers
		void	CloseDown(CContext *pPtr);
		void	PendDeAlloc(CContext *pPtr);

		// Test code for arrays
		BOOL	DoArrayTest(CAddress Addr, PDWORD pData, UINT uCount, BOOL fIsRead);
	};

enum {	// Poll Response Positions: DOWN, NODATA, NAK (= No Buffers Available)
	PLADDR	= 2,	// Address of Master (0)
	PLSERN	= 3,	// Serial Num of Poll
	PLFC	= 4,	// Function code of response
	PLSLV	= 5,	// Address of slave responding
	PLNSB	= 6,	// Node Status Byte
	PLBUFF	= 7,	// Number of buffers in use
	};

enum { // Local Serial Rx Header Positions

	RXDADD	= 0,	// Destination Drop - same for global
	RXSERN	= 1,	// Serial Number - same for global
	RXDFC	= 2,	// Destination Function code
	RXSPRI	= 3,	// POLL Priority Code (=0)
	RXLSQL	= 3,	// App Sequence Number LO
	RXLSQH	= 4,	// App Sequence Number HI
	RXLSFC	= 5,	// Source Function Code
	RXLNSB	= 6,	// Node Status Byte
	RXLDAT0	= 7,	// Packet Data Start
	};

enum { // Global Serial Rx Header Positions

//	RXDADD	= 0,	// Destination Drop - same for global as local
//	RXSERN	= 1,	// Serial Number - same for global as local
	RXGDAL	= 2,	// Destination Addr Low
	RXGDAH	= 3,	// Destination Addr High
	RXGSAL	= 4,	// Source Addr Low
	RXGSAH	= 5,	// Source Addr High
	RXGCTL	= 6,	// Control Byte
	RXGDFC	= 7,	// Destination Function Code
	RXGSQL	= 8,	// Application Sequence Number Low
	RXGSQH	= 9,	// Application Sequence Number High
	RXGSFC	= 10,	// Source Function Code
	RXGNSB	= 11,	// Node Status Byte
	RXGDAT0	= 12	// Packet Data Start
	};

enum { // List Serial Rx Data Positions

	RXSTRQ	= 0,	// List Command 0xC = Start, 0xD = Continue
	RXFUN	= 1,	// FSS define
	RXFSS1	= 2,	// FSS1
	RXFSS2	= 3,	// FSS2
	RXFSS3	= 4,	// FSS3
	RXFSS4	= 5,	// FSS4
	RXVERL	= 6,	// Version Low
	RXVERH	= 7,	// Version High
	RXSECC	= 8,	// Security Code
	RXLIST	= 9	// List Number
	};

#define	TXRERPOS	9

enum { // Message function codes
	MFCPEIPC  =  0x3, // for HMI
	MFCPEIRTU = 0x04, // to RTU
	MFCRDDB   = 0x0B, // Read Database
	MFCWRDB   = 0x0C, // Write Database
	MFCRDBYN  = 0x0D, // Read By Name in example file from BB
	MFCCOMREQ = 0x70, // Command Handler Requests
	MFCNRTMSG = 0x88, // TS/NRT Message (for Slave Ports only)
	MFCNRTREQ = 0x89, // Request for TS/NRT Message
	MFCRDBACC = 0xA0, // remote database access
	};

enum { // Operational function codes
	OFCRDBYA  =  0x0, // Read Signal by Address
	OFCEXTSFC =  0x1, // Extended Request Date/Time Sub Function code
	OFCEXTFC  =  0x2, // Extended Request Function Code / NRT SubFunction Code
	OFCRDBYN  =  0x4, // Read Signal By Name
	OFCEXTREQ =  0xA, // Extended Request Function Code
	OFCRARRS  = 0x14, // Read Array, Short Form
	OFCRARRG  = 0x18, // Read Array, General Form,
	OFCRDBYL  = 0x0C, // First call to read Signal(s) by list
	OFCRDBYLC = 0x0D, // Continue to read Signal(s) by list
	OFCREQNRT = 0x70, // Read NRT Function code
	OFCWRBYA  = 0x80, // Write Signal by Address
	OFCWRBYN  = 0x84, // Write By Name
	OFCWARRS  = 0x94, // Write Array, Short Form
	OFCWARRG  = 0x98, // Write Array, General Form
	OFCALACK  = 0xA8, // Alarm Acknowledgement Req & Rsp.
	OFCALMIN  = 0xA9, // Alarm Initialization Request
	};

enum { // Field Select 1 codes
	FS1TYPINH = 0x80,
	FS1VALUE  = 0x40,
	FS1UNITS  = 0x20,
	FS1MSDADD = 0x10,
	FS1NAME   = 0x08,
	FS1ALARM  = 0x04,
	FS1DESC   = 0x02,
	FS1ONOFF  = 0x01,
	};

enum { // Field Select 2 codes
	FS2PROTECT = 0x80,
	FS2ALMPRI  = 0x40,
	FS2BNAME   = 0x20,
	FS2BEXT    = 0x10,
	FS2BATT    = 0x08,
	FS2ALMDBLO = 0x04,
	FS2ALMDBHI = 0x02,
	FS2MSDVER  = 0x01,
	};

enum { // Field Select 3 codes
	FS3ALMLMLO = 0x80,
	FS3ALMLMHI = 0x40,
	FS3ALMEXLO = 0x20,
	FS3ALMEXHI = 0x10,
	FS3RBERSVD = 0x08,
	FS3LCKRSVD = 0x04,
	FS3SIGVAL  = 0x02,
	FS3NAMELNG = 0x01,
	};

enum { // Polls and Acks
	RTURESP  = 0x40, // Data Response
	ACKDSCD  = 0x83, // Ack, but discarded
	POLLMSG  = 0x85, // Poll message
	ACKDOWN  = 0x86, // Down Transmit ACK, Slave ACKing message
	ACKPOLL  = 0x87, // ACK No Data, in response to a Poll message
	NRTRETR  = 0x88, // Returned Node routing Table
	NRTREQ   = 0x89, // Request for NRT message
	POLLUPSL = 0x8A, // UP-ACK with poll (recognized by VSAT Slave)
	UPACK    = 0x8B, // UP-ACK, Master ACKing message
	NAKDATA  = 0x95, // NAK, No buffer available for received data
	ALARMACK = 0xA8, // Alarm Acknowledge
	ALARMNOT = 0xAA, // Alarm notification
	};

enum { // Write Field Descriptors
	WFDALDIS = 0x1,
	WFDALEN  = 0x2,
	WFDCNDIS = 0x3,
	WFDCNEN  = 0x4,
	WFDMNDIS = 0x5,
	WFDMNEN  = 0x6,
	WFDQUSET = 0x7,
	WFDQUCLR = 0x8,
	WFDLVON  = 0x9,
	WFDLVOFF = 0xA,
	WFDANALG = 0xB,
	WFDSTRNG = 0xD,
	WFDRDSEC = 0xE,
	WFDWRSEC = 0xF,
	};

enum { // RER Codes for standard requests
	RERMORE  = 0x81,	// Multiple errors in response
	RERMATCH = 0x84,	// no match on name, or too big
	RERAVPKD = 0x88,	// Analog value in packed logical
	RERNOMAT = 0x90,	// no match found
	RERVERNM = 0xA0,	// Version number mismatch
	RERINVDB = 0xC0,	// invalid data base function requested
	RERSET   = 0x80		// Error exists
	};

enum { // RER Codes for array request
	RERBOUND = 0x82,	// Array bounds exceeded
	RERARRCF = 0x84,	// Invalid array reference
	RERROARR = 0x88,	// write to a read-only array
//	RERINVDB = 0xC0		// invalid data base function request
	};

enum { // EER Codes
	EERRONLY = 0x02,	// attempt to write to read only
	EERIDENT = 0x04,	// Invalid name, list, MSDAddr
	EERSECUR = 0x05,	// Security violation
	EERINHIB = 0x06,	// Write a manual inhibited signal
	EERINVWR = 0x07,	// Invalid write attempt (eg analog=ON)
	EEREND_D = 0x08,	// Premature End_of_Data encountered
	EERINCOM = 0x09,	// Incomplete Remote DB request
	EERMATCH = 0x10		// No Match for Signal Name
	};

enum {	// Node Status Bits
	NSTALE	= 0x1,		// unreported alarms exist
	NSTDIE	= 0x2,		// Discrete Imput change (unused)
	NSTMORE	= 0x4,		// Slave has more data
	NSTPFE	= 0x8,		// Power failure recovery
	NSTDWNL	= 0x10,		// Download in progress
	NSTVSAT	= 0x20,		// message from VSAT port
	NSTMXNF	= 0x40,		// Destination message exchange not found (unused)
	NSTCOMF	= 0x80,		// Communication failure (unused)
	};

enum {				// table numbers
	SPBIT0		= 10,
	SPBIT1		= 11,
	SPBIT2		= 12,
	SPBIT3		= 13,
	SPBIT4		= 14,
	SPBIT5		= 15,
	SPBIT6		= 16,
	SPREAL0		= 20,
	SPREAL1		= 21,
	SPREAL2		= 22,
	SPREAL3		= 23,
	SPREAL4		= 24,
	SPREAL5		= 25,
	SPREAL6		= 26,
	SPSTRING0	= 30,
	SPSTRING1	= 31,
	SPSTRING2	= 32,
	SPSTRING3	= 33,
	SPSTRING4	= 34,
	SPSTRING5	= 35,
	SPSTRING6	= 36,
	SPBOOLACK	= 70,
	SPREALACK	= 71,
	SPNRT0		= 90,
	SPNRT1		= 91,
	SPNRT2		= 92,
	SPNRT3		= 93,
	SPNRT4		= 94,
	SPNRT5		= 95,
	SPNRT6		= 96,
	SPBYTE0		= 110,
	SPBYTE1		= 111,
	SPBYTE2		= 112,
	SPBYTE3		= 113,
	SPBYTE4		= 114,
	SPBYTE5		= 115,
	SPBYTE6		= 116,
	SPWORD0		= 120,
	SPWORD1		= 121,
	SPWORD2		= 122,
	SPWORD3		= 123,
	SPWORD4		= 124,
	SPWORD5		= 125,
	SPDWORD0	= 130,
	SPDWORD1	= 131,
	SPDWORD2	= 132,
	SPDWORD3	= 133,
	SPDWORD4	= 134,
	SPDWORD5	= 135,
	SPDWORD6	= 136,
	SPBYTEINT	= 140,
	SPWORDINT	= 150,
	SPDWORDINT	= 160,
	};

enum {				
	BTYPEBIT	= 0,
	BTYPEREAL	= 2,
	BTYPEBYTE	= 3,
	BTYPEWORD	= 4,
	BTYPELONG	= 5,
	BTYPESTRING	= 6,
	};

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#define	I2R(x)	(*((float *) &(x)))

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

// Slave Address Fields
#define	AMASK	0x3FF	// MSD Address = 0 - 1023
#define	LSHIFT	10	// List number is bit 10+
#define	NUMBUFF	 8	// maximum number of pending transactions
#define	MAXBUFF	(NUMBUFF - 1)

// Polling Alarm Enable Values
#define	YESALARMS	0
#define	NOALARMS	0x10

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	MAXSEND	240

//////////////////////////////////////////////////////////////////////////
//
// Time Support
//

extern DWORD Time(UINT h, UINT m, UINT s);
extern DWORD Date(UINT y, UINT m, UINT d);
extern void  GetWholeDate(DWORD t, UINT *p);
extern void  GetWholeTime(DWORD t, UINT *p);
extern UINT  GetYear(DWORD t);
extern UINT  GetMonth(DWORD t);
extern UINT  GetDate(DWORD t);
extern UINT  GetDays(DWORD t);
extern UINT  GetWeeks(DWORD t);
extern UINT  GetDay(DWORD t);
extern UINT  GetWeek(DWORD t);
extern UINT  GetWeekYear(DWORD t);
extern UINT  GetHours(DWORD t);
extern UINT  GetHour(DWORD t);
extern UINT  GetMin(DWORD t);
extern UINT  GetSec(DWORD t);
extern UINT  GetMonthDays(UINT y, UINT m);

/*
FUN Function code
bit 7 function bit
0 read function
1 write function
bits 6-4 = data type code
000 signal data
001 data array data
010 direct I/O data
100 memory data
111 Extended RDB function
bits 3-2 = select code
00 select via address or data array number, control
01 select via name or data array number, short form
10 select via match or data array number, general form
11 select via list
bits 1-0 = signal READ return code, WRITE Memory code
For READ:
00 return first match
01 return next match/continue list
10 return packed logical data (logical value data only)
For WRITE Memory,
00 Write Data to memory
01 OR Data with memory
10 AND Data with memory

NOTE:FIELD SELECTOR BYTES ARE ONLY USED IN ACCESSING SIGNAL
DATA WHEN PACKED LOGICAL DATA IS NOT USED.
FSS Field select selector defines which field select bytes are included in the request:
BITS 7-4 not used at this time (reserved for priority)
BIT 3 Field select byte 4 is used
BIT 2 Field select byte 3 is used
BIT 1 Field select byte 2 is used
BIT 0 Field select byte 1 is used

FS1 Field selector byte one (most common options):
BIT 7 Type and Inhibits byte
BIT 6 Value
BIT 5 Units/Logical text
BIT 4 Msd address
BIT 3 Name Text (includes Base.Extension.Attribute)
BIT 2 Alarm status
BIT 1 Descriptor text
BIT 0 Logical ON/OFF text

FS2 Field selector byte two:
BIT 7 Protection byte
BIT 6 Alarm priority
BIT 5 Base Name
BIT 4 Extension
BIT 3 Attribute
BIT 2 Low alarm deadband MSD address
BIT 1 High alarm deadband MSD address
BIT 0 MSD Version number

FS3 Field selector byte three:
BIT 7 Low alarm limit MSD address
BIT 6 High alarm limit MSD address
BIT 5 Extreme low alarm limit MSD address
BIT 4 Extreme high alarm limit MSD address
BIT 3 Reserved for Report by exception
BIT 2 Reserved (old Lock bit)
BIT 1 Signal value; Analogs return no Q-bit in FP mantissa.
BIT 0 When set, allows long signal names to be returned, instead of standard
length names. (Requires ControlWave firmware version 2.2 or newer)

FS4 Field selector byte four:
This byte is reserved for future use.
*/

// End of File
