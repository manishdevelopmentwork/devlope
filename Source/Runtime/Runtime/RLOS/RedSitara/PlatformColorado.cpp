
#include "Intern.hpp"

#include "PlatformColorado.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

#include "Ctrl437.hpp"

#include "Gpmc437.hpp"

#include "Elm437.hpp"

#include "Dss437.hpp"

#include "Pwm437.hpp"

#include "Pru437.hpp"

#include "Pcm437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Pru Code  
//

#include "Pru/Colorado.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice    * Create_RtcMcp(UINT uSlot);
extern IDevice    * Create_Rm25Cxx(UINT uChan);
extern IDevice    * Create_Leds(ILeds *pLeds);
extern IDevice    * Create_InputSwitch(IInputSwitch *pSwitch);
extern IDevice    * Create_Identity(UINT uAddr);
extern IDevice    * Create_Beeper437(CPwm437 *pPwm, UINT uChan);
extern IDevice    * Create_Uart437(UINT iIndex, CClock437 *pClock, CPru437 *pPru);
extern IDevice    * Create_SdHost437(CClock437 *pClock);
extern IDevice    * Create_Enet437(CClock437 *pClock);
extern IDevice    * Create_Display437(UINT uModel, CDss437 *pDss, CPwm437 *pPwm, UINT uChan);
extern IDevice    * Create_Touch437(BOOL fFlipX, BOOL fFlipY, UINT uMaxVar, UINT uMinVal);
extern IUsbDriver * Create_UsbFunc437(UINT iIndex);

//////////////////////////////////////////////////////////////////////////
//
// Prototypes
//

extern bool GetDssConfig(CDss437Config &Config, UINT Display);

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Colorado HMI
//

// Instantiator

IDevice * Create_PlatformColorado(UINT uModel)
{
	return (IDevice *) (CPlatformSitara *) New CPlatformColorado(uModel);
}

// Constructor

CPlatformColorado::CPlatformColorado(UINT uModel) : CPlatformSitara(FALSE)
{
	m_uModel = uModel;

	m_opp    = oppOPP50;
}

// Destructor

CPlatformColorado::~CPlatformColorado(void)
{
	delete m_pPwm[0];

	delete m_pPwm[1];

	delete m_pPwm[2];

	delete m_pPwm[3];

	delete m_pPwm[4];

	delete m_pPru;

	delete m_pDss;

	piob->RevokeGroup("dev.");
}

// IUnknown

HRESULT CPlatformColorado::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IPortSwitch);

	StdQueryInterface(IInputSwitch);

	StdQueryInterface(ILeds);

	return CPlatformSitara::QueryInterface(riid, ppObject);
}

ULONG CPlatformColorado::AddRef(void)
{
	return CPlatformSitara::AddRef();
}

ULONG CPlatformColorado::Release(void)
{
	return CPlatformSitara::Release();
}

// IDevice

BOOL CPlatformColorado::Open(void)
{
	if( CPlatformSitara::Open() ) {

		m_pPwm[0] = New CPwm437(0, m_pClock);

		m_pPwm[1] = New CPwm437(1, m_pClock);

		m_pPwm[2] = New CPwm437(2, m_pClock);

		m_pPwm[3] = New CPwm437(3, m_pClock);

		m_pPwm[4] = New CPwm437(5, m_pClock);

		m_pPru    = New CPru437(0, 0);

		m_pDss    = New CDss437(m_pClock);

		InitPwms();

		InitPru();

		piob->RegisterSingleton("dev.leds", 0, Create_Leds(this));

		piob->RegisterSingleton("dev.input-s", 0, Create_InputSwitch(this));

		piob->RegisterSingleton("dev.fram", 0, Create_Rm25Cxx(1));

		piob->RegisterSingleton("dev.rtc", 0, Create_RtcMcp(addrRtc));

		piob->RegisterSingleton("dev.ident", 0, Create_Identity(addrIdentity));

		piob->RegisterSingleton("dev.beeper", 0, Create_Beeper437(m_pPwm[1], 0));

		piob->RegisterSingleton("dev.sdhost", 0, Create_SdHost437(m_pClock));

		piob->RegisterSingleton("dev.uart", 0, Create_Uart437(1, m_pClock, NULL));

		piob->RegisterSingleton("dev.uart", 1, Create_Uart437(0, m_pClock, m_pPru));

		piob->RegisterSingleton("dev.nic", 0, Create_Enet437(m_pClock));

		piob->RegisterSingleton("dev.display", 0, Create_Display437(m_uModel, m_pDss, m_pPwm[3], 0));

		piob->RegisterSingleton("dev.touch", 0, Create_Touch437(TRUE, TRUE, 20, 5));

		piob->RegisterSingleton("dev.usbctrl-d", 0, Create_UsbFunc437(0));

		InitUarts();

		InitNicMac(0);

		FindSerial();

		RegisterRedCommon();

		return TRUE;
	}

	return FALSE;
}

// IPlatform

PCTXT CPlatformColorado::GetModel(void)
{
	switch( m_uModel ) {

		case MODEL_COLORADO_04: return "co04";
		case MODEL_COLORADO_07: return "co07";
		case MODEL_COLORADO_07EQ: return "co07eq";
		case MODEL_COLORADO_10: return "co10";
		case MODEL_COLORADO_10EV: return "co10ev";
		case MODEL_MANTICORE_10: return "da10";
	}

	return CPlatformSitara::GetModel();
}

PCTXT CPlatformColorado::GetVariant(void)
{
	return GetModel();
}

UINT CPlatformColorado::GetFeatures(void)
{
	return	0 * rfGraphiteModules |
		0 * rfSerialModules   |
		1 * rfDisplay;
}

// IPortSwitch

UINT METHOD CPlatformColorado::GetCount(UINT uUnit)
{
	switch( uUnit ) {

		case 0:
			return 1;

		case 1:
			return 2;
	}

	return 0;
}

UINT METHOD CPlatformColorado::GetMask(UINT uUnit)
{
	switch( uUnit ) {

		case 0:
			return (1 << physicalRS232);

		case 1:
			return (1 << physicalRS232)	  |
			       (1 << physicalRS485)       |
			       (1 << physicalRS422Master) |
			       (1 << physicalRS422Slave);
	}

	return physicalNone;
}

UINT METHOD CPlatformColorado::GetType(UINT uUnit, UINT uLog)
{
	switch( uUnit ) {

		case 0:
			return physicalRS232;

		case 1:
			switch( uLog ) {

				case 0:
					return physicalRS232;

				case 1:
					return physicalRS485;
			}
			
			break;
	}

	return physicalNone;
}

BOOL METHOD CPlatformColorado::EnablePort(UINT uUnit, BOOL fEnable)
{
	return true;
}

BOOL METHOD CPlatformColorado::SetPhysical(UINT uUnit, BOOL fRS485)
{
	UINT uPad = CCtrl437::padPullNo | CCtrl437::padRxActive;

	switch( uUnit ) {

		case 1:
			if( fRS485 ) {

				m_pCtrl->SetMux(CCtrl437::pinCAM1DATA8, CCtrl437::muxMode7 | uPad);

				m_pCtrl->SetMux(CCtrl437::pinMCASP0ACLKX, CCtrl437::muxMode5 | uPad);
			}
			else {
				m_pCtrl->SetMux(CCtrl437::pinMCASP0ACLKX, CCtrl437::muxMode7 | uPad);

				m_pCtrl->SetMux(CCtrl437::pinCAM1DATA8, CCtrl437::muxMode8 | uPad);
			}

			m_pGpio[5]->SetState(25, fRS485);

			return true;
	}

	return false;
}

BOOL METHOD CPlatformColorado::SetFull(UINT uUnit, BOOL fFull)
{
	switch( uUnit ) {

		case 1:
			m_pGpio[5]->SetState(10, !fFull);

			return true;
	}

	return false;
}

BOOL METHOD CPlatformColorado::SetMode(UINT uUnit, BOOL fAuto)
{
	return false;
}

// ILeds

void METHOD CPlatformColorado::SetLed(UINT uLed, UINT uState)
{
	UINT iSel = HIBYTE(uLed);

	UINT iLed = LOBYTE(uLed);

	if( iSel == 0 ) {

		switch( iLed ) {

			case 0: m_pPwm[4]->SetDuty(CPwm437::pwmChanA, uState == stateOff ? 0 : 100); break;
			case 1: m_pPwm[2]->SetDuty(CPwm437::pwmChanA, uState == stateOff ? 0 : 100); break;
			case 2: m_pPwm[0]->SetDuty(CPwm437::pwmChanA, uState == stateOff ? 0 : 100); break;
		}
	}
}

void METHOD CPlatformColorado::SetLedLevel(UINT uPercent, bool fPersist)
{
}

UINT METHOD CPlatformColorado::GetLedLevel(void)
{
	return 100;
}

// IInputSwitch

UINT METHOD CPlatformColorado::GetSwitches(void)
{
	return 0xFFFFFFFE | (GetSwitch(0) ? Bit(0) : 0);
}

UINT METHOD CPlatformColorado::GetSwitch(UINT uSwitch)
{
	if( uSwitch == 0 ) {

		return m_pGpio[5]->GetState(30);
	}

	return 1;
}

// Inititialisation

void CPlatformColorado::InitClocks(void)
{
	CPlatformSitara::InitClocks();

	m_pClock->SetClockMode(CClock437::clockADC0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM1, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM2, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM3, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM5, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPRU,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART1, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockMAC0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockCPSW, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUSB0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUSBPHY0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockDSS,	CClock437::modeSwWakup);
}

void CPlatformColorado::InitMpu(void)
{
	CDss437Config Config;

	GetDssConfig(Config, m_uModel);

	UINT o = m_pCtrl->GetRefFreq();

	UINT n = 40;

	UINT q = 4;

	UINT k = Min(180000000 / Config.m_uFrequency, 4u);

	UINT f = o / (n * q * k);

	UINT m = (Config.m_uFrequency + f / 2) / f;

	while( m > 2047 ) {

		m >>= 1;

		n <<= 1;
	}

	m_pClock->SetDispFreq(m, n - 1, q);

	switch( Config.m_uFrequency / 10000000 ) {

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			SetMpuBandwidth(300);
			break;

		case 5:
			SetMpuBandwidth(100);
			break;

		case 6:
			SetMpuBandwidth(70);
			break;

		case 7:
			SetMpuBandwidth(50);
			break;

		default:
			SetMpuBandwidth(40);
			break;
	}

	SetMpuOpp(m_opp);
}

void CPlatformColorado::InitMux(void)
{
	CPlatformSitara::InitMux();

	DWORD const muxLed[] = {

		CCtrl437::pinSPI0SCLK,    CCtrl437::muxMode7,
		CCtrl437::pinSPI0CS1,     CCtrl437::muxMode7,
		CCtrl437::pinUART3CTSN,   CCtrl437::muxMode7,
	};

	DWORD const muxSwi[] = {

		CCtrl437::pinGPMCWAIT0,   CCtrl437::muxMode9 | CCtrl437::padSlow | CCtrl437::padPullUp | CCtrl437::padRxActive,
	};

	DWORD const muxPwm[] = {

		CCtrl437::pinSPI0SCLK,    CCtrl437::muxMode3,
		CCtrl437::pinGPMCA2,      CCtrl437::muxMode6,
		CCtrl437::pinSPI0CS1,     CCtrl437::muxMode8,
		CCtrl437::pinCAM1FIELD,   CCtrl437::muxMode8,
		CCtrl437::pinUART3CTSN,   CCtrl437::muxMode6,
	};

	DWORD const muxPru[] = {

		CCtrl437::pinUART3RXD,    CCtrl437::muxMode5 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0ACLKX, CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0FSX,   CCtrl437::muxMode6 | CCtrl437::padPullNo | CCtrl437::padRxActive,
	};

	DWORD const muxSer[] = {

		CCtrl437::pinUART0RXD,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART0TXD,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA8,   CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA9,   CCtrl437::muxMode8 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA0,   CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA1,   CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA2,   CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA3,   CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinGPMCAD9,     CCtrl437::muxMode9 | CCtrl437::padPullNo | CCtrl437::padRxActive,
	};

	DWORD const muxSd[] = {

		CCtrl437::pinMMC0CMD,   CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0CLK,   CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT0,  CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT1,  CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT2,  CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT3,  CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM0DATA2, CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
	};

	DWORD const muxNic[] = {

		CCtrl437::pinMII1TXD0,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1TXD1,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1TXD2,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1TXD3,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1RXD0,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD1,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD2,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD3,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1TXEN,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1TXCLK,   CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXDV,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXCLK,   CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1CRS,     CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMDIOCLK,     CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMDIODATA,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinGPIO5_8,     CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
	};

	DWORD const muxDss[] = {

		CCtrl437::pinDSSDATA0,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA1,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA2,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA3,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA4,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA5,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA6,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA7,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA8,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA9,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA10,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA11,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA12,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA13,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA14,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA15,   CCtrl437::muxMode0,
		CCtrl437::pinDSSPCLK,     CCtrl437::muxMode0,
		CCtrl437::pinDSSACBIASEN, CCtrl437::muxMode0,
		CCtrl437::pinDSSHSYNC,    CCtrl437::muxMode0,
		CCtrl437::pinDSSVSYNC,    CCtrl437::muxMode0,
		CCtrl437::pinGPMCAD15,    CCtrl437::muxMode1,
		CCtrl437::pinGPMCAD14,    CCtrl437::muxMode1,
		CCtrl437::pinGPMCAD13,    CCtrl437::muxMode1,
		CCtrl437::pinGPMCAD12,    CCtrl437::muxMode1,
		CCtrl437::pinCAM0WEN,     CCtrl437::muxMode2,
		CCtrl437::pinCAM0FIELD,   CCtrl437::muxMode2,
		CCtrl437::pinCAM0VD,      CCtrl437::muxMode2,
		CCtrl437::pinGPMCAD8,     CCtrl437::muxMode1,
		};

	m_pCtrl->SetMux(muxLed, elements(muxLed));

	m_pCtrl->SetMux(muxSwi, elements(muxSwi));

	m_pCtrl->SetMux(muxPwm, elements(muxPwm));

	m_pCtrl->SetMux(muxPru, elements(muxPru));

	m_pCtrl->SetMux(muxSer, elements(muxSer));

	m_pCtrl->SetMux(muxSd, elements(muxSd));

	m_pCtrl->SetMux(muxNic, elements(muxNic));

	m_pCtrl->SetMux(muxDss, elements(muxDss));
}

void CPlatformColorado::InitGpio(void)
{
	CPlatformSitara::InitGpio();

	m_pGpio[1]->SetState(22, true);
	m_pGpio[1]->SetState(23, true);
	m_pGpio[5]->SetState(8,  false);

	m_pGpio[1]->SetDirection(19, true);
	m_pGpio[1]->SetDirection(22, true);
	m_pGpio[1]->SetDirection(23, true);
	m_pGpio[4]->SetDirection(24, false);
	m_pGpio[5]->SetDirection(4,  true);
	m_pGpio[5]->SetDirection(8,  true);
	m_pGpio[5]->SetDirection(10, true);
	m_pGpio[5]->SetDirection(25, true);
	m_pGpio[5]->SetDirection(30, false);
}

void CPlatformColorado::InitMisc(void)
{
	CPlatformSitara::InitMisc();

	m_pCtrl->SetPWM(0, true);

	m_pCtrl->SetPWM(1, true);

	m_pCtrl->SetPWM(2, true);

	m_pCtrl->SetPWM(3, true);

	m_pCtrl->SetPWM(5, true);

	m_pCtrl->SetMIIMode(0, CCtrl437::modeGMII);

	m_pCtrl->SetPhyPower(0, true);
}

void CPlatformColorado::InitPru(void)
{
	m_pPcm->EnablePru();

	m_pPru->Stop(0);

	m_pPru->Reset(0);

	m_pPru->SetMux(0);

	m_pPru->LoadCode(0, PRU_CODE);
}

void CPlatformColorado::InitUarts(void)
{
	for( UINT i = 0; i < 2; i++ ) {

		IPortObject *pPort = NULL;

		AfxGetObject("uart", i, IPortObject, pPort);

		pPort->Bind(this);

		pPort->Release();
	}
}

void CPlatformColorado::InitPwms(void)
{
	m_pPwm[0]->SetFreq(5000);

	m_pPwm[2]->SetFreq(5000);

	m_pPwm[4]->SetFreq(5000);
}

// End of File
