
/**
 * @file
 *
 * UDR message list header file.
 * 
 * Provides a generic message list structure and a series of functions
 * for accessing said structure.
 */

/*
 * Copyright 2012 SIXNET, LLC.
 *
 * Notice: This document contains proprietary, confidential information
 * owned by SIXNET, LLC and is protected as an unpublished work under
 * the Copyright Law of the United States. No part of this document may
 * be copied, disclosed to others or used for any purpose without the
 * written permission of SIXNET, LLC.
 */

#ifndef UDR_LIST_H
#define UDR_LIST_H

#include "udriver.h"

/**
 * Typedef predeclaration for a udr_message_list
 *
 * Since the linked-list is self-referential, we need this ahead of
 * it.
 */
typedef struct udr_message_list_item udr_message_list;

/**
 * UDR message linked list item
 *
 * @note MUST be freed with freeUDRMessageList
 */
struct udr_message_list_item {

    udr_message *item;            /**< UDR message */

    udr_message_list *nextNode;   /**< Next list item */
};

udr_message *newUDRMessage(void);
void freeUDRMessage(udr_message * message);
#if defined(__gnu_linux__) // Only on Linux.
int addUDRMessageToList(udr_message_list ** list,
                        udr_message * message);
void freeUDRMessageList(udr_message_list * head);
udr_message *getUDRListItem(udr_message_list * list);
udr_message_list *getNextUDRListNode(udr_message_list * current);
#endif

#endif
