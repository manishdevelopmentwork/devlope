
#include "Intern.hpp"

#include "Service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudDeviceDataSet_HPP

#define	INCLUDE_CloudDeviceDataSet_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cloud Tag Set
//

class CCloudDeviceDataSet : public CCloudDataSet
{
	public:
		// Constructor
		CCloudDeviceDataSet(void);

		// Destructor
		~CCloudDeviceDataSet(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Json Formatting
		BOOL GetJson(CJsonData *pRep, CJsonData *pDes, CString Ident, BOOL fRoot, UINT uMode);

		// List Formatting
		UINT GetCount(void);
		UINT GetProps(void);
		BOOL GetData (UINT uIndex, CString &Name, DWORD &Data, UINT &Type, BOOL &Free, BOOL fLive);
		BOOL GetProp (UINT uIndex, UINT uProp, CString &Name, DWORD &Data, UINT &Type);
		BOOL SetData (UINT uIndex, DWORD Data, UINT Type);

		// Data Members
		UINT m_Gps;
		UINT m_Cell;

	protected:
		// Implementation
		BOOL AddGpsData(CJsonData *pGps);
		BOOL AddCellData(CJsonData *pCell);
	};

// End of File

#endif
