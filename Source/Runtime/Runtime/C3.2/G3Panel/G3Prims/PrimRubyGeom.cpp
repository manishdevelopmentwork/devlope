
#include "intern.hpp"

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyTankFill.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Geometric Primitive
//

// Constructor

CPrimRubyGeom::CPrimRubyGeom(void)
{
	m_pFill = New CPrimRubyTankFill;
	
	m_pEdge = New CPrimRubyPenEdge;
	}

// Destructor

CPrimRubyGeom::~CPrimRubyGeom(void)
{
	delete m_pFill;

	delete m_pEdge;
	}

// Initialization

void CPrimRubyGeom::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyGeom", pData);

	DoLoad(pData);

	if( !m_pFill->IsNull() ) {

		m_fTrans = FALSE;
		}
	}

// Overridables

void CPrimRubyGeom::SetScan(UINT Code)
{
	m_pFill->SetScan(Code);

	m_pEdge->SetScan(Code);

	CPrimRubyWithText::SetScan(Code);
	}

void CPrimRubyGeom::MovePrim(int cx, int cy)
{
	m_listFill.Translate(cx, cy, !m_fFast);
	
	m_listEdge.Translate(cx, cy, true);
	
	m_listTrim.Translate(cx, cy, true);

	CPrimRubyWithText::MovePrim(cx, cy);
	}

void CPrimRubyGeom::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRubyWithText::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		if( m_pFill->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( !m_pFill->IsNull() || !m_pEdge->IsNull() ) {

			(m_fChange ? Erase : Trans).Append(m_bound);
			}
		}
	}

void CPrimRubyGeom::DrawPrim(IGDI *pGDI)
{
	m_pFill->Fill(pGDI, m_listFill, !m_fFast);

	m_pEdge->Fill(pGDI, m_listEdge, TRUE);

	m_pEdge->Trim(pGDI, m_listTrim, TRUE);

	CPrimRubyWithText::DrawPrim(pGDI);
	}

// Implementation

void CPrimRubyGeom::DoLoad(PCBYTE &pData)
{
	CPrimRubyWithText::Load(pData);

	m_fFast = GetByte(pData);

	m_pFill->Load(pData);
	m_pEdge->Load(pData);

	LoadList(pData, m_listFill);
	LoadList(pData, m_listEdge);
	LoadList(pData, m_listTrim);
	}

// End of File
