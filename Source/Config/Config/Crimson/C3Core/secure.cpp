
#include "intern.hpp"

#include "secure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Password Entry Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CPasswordDialog, CStdDialog);
		
// Constructors

CPasswordDialog::CPasswordDialog(BOOL fRead)
{
	m_fRead = fRead;

	SetName(L"PasswordDialog");
	}

// Attributes

CString CPasswordDialog::GetData(void) const
{
	return m_Data;
	}
		
// Message Map

AfxMessageMap(CPasswordDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)
	AfxDispatchCommand(200,      OnReadOnly     )

	AfxDispatchNotify(100, EN_CHANGE, OnEditChange)

	AfxMessageEnd(CPasswordDialog)
	};

// Message Handlers

BOOL CPasswordDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	GetDlgItem(IDOK).EnableWindow(FALSE);

	GetDlgItem(200 ).ShowWindow  (m_fRead ? SW_SHOW : SW_HIDE);

	return TRUE;
	}
	
// Command Handlers

BOOL CPasswordDialog::OnCommandOK(UINT uID)
{
	m_Data = GetDlgItem(100).GetWindowText();

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CPasswordDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

BOOL CPasswordDialog::OnReadOnly(UINT uID)
{
	m_Data.Empty();

	EndDialog(TRUE);

	return TRUE;
	}

// Notification Handlers

void CPasswordDialog::OnEditChange(UINT uID, CWnd &Wnd)
{
	GetDlgItem(IDOK).EnableWindow(Wnd.GetWindowTextLength() > 0);
	}

//////////////////////////////////////////////////////////////////////////
//
// Security Entry Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSecurityDialog, CStdDialog);
		
// Constructors

CSecurityDialog::CSecurityDialog(UINT Locked, CString Password, CString Email)
{
	m_Locked   = Locked;

	m_Password = Password;

	m_Email    = Email;

	SetName(L"SecurityDialog");
	}

// Attributes

UINT CSecurityDialog::GetLocked(void) const
{
	return m_Locked;
	}

CString CSecurityDialog::GetPassword(void) const
{
	return m_Password;
	}
		
CString CSecurityDialog::GetEmail(void) const
{
	return m_Email;
	}
		
// Message Map

AfxMessageMap(CSecurityDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxDispatchNotify (100, CBN_SELCHANGE, OnSelChange)

	AfxMessageEnd(CSecurityDialog)
	};

// Message Handlers

BOOL CSecurityDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(100);

	Combo.AddString(CString(IDS_FULL_ACCESS),     DWORD(0));
	
	Combo.AddString(CString(IDS_READONLY_ACCESS), DWORD(1));
	
	Combo.AddString(CString(IDS_NO_ACCESS),       DWORD(2));

	Combo.AddString(CString(IDS_PRIV_ACCESS),     DWORD(3));

	Combo.AddString(CString(IDS_DOWN_ONLY),	      DWORD(4));

	Combo.SelectData(DWORD(m_Locked));

	GetDlgItem(101).SetWindowText(m_Password);

	GetDlgItem(102).SetWindowText(m_Password);

	GetDlgItem(103).SetWindowText(m_Email);
	
	DoEnables();

	return TRUE;
	}
	
// Command Handlers

BOOL CSecurityDialog::OnCommandOK(UINT uID)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(100);

	if( (m_Locked = Combo.GetCurSelData()) ) {

		CString Pass1 = GetDlgItem(101).GetWindowText();

		CString Pass2 = GetDlgItem(102).GetWindowText();

		CString Email = GetDlgItem(103).GetWindowText();

		if( Pass1.IsEmpty() || Pass2.IsEmpty() ) {

			Error(CString(IDS_YOU_MUST_ENTER));

			SetDlgFocus(101);

			return TRUE;
			}

		if( Pass1.CompareC(Pass2) ) {

			Error(CString(IDS_PASSWORDS_DO_NOT));

			SetDlgFocus(101);

			return TRUE;
			}

		if( Email.Count('@') - 1 ) {

			CString Text;

			Text += CString(IDS_YOU_MUST_ENTER_2);

			Text += L"\n\n";

			Text += CString(IDS_RECOVERED);

			Error(Text);

			SetDlgFocus(103);

			return TRUE;
			}

		m_Password = Pass1;

		m_Email    = Email;
		}
	else {
		m_Password.Empty();

		m_Email.Empty();
		}

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CSecurityDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

// Notification Handlers

void CSecurityDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	DoEnables();
	}

// Implementation

void CSecurityDialog::DoEnables(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(100);

	GetDlgItem(101).EnableWindow(Combo.GetCurSelData() > 0);

	GetDlgItem(102).EnableWindow(Combo.GetCurSelData() > 0);

	GetDlgItem(103).EnableWindow(Combo.GetCurSelData() > 0);
	}

// End of File
