
#include "intern.hpp"

#include "acrmgtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Acromag TCP
//

// Instantiator

INSTANTIATE(CAcromagTCPDriver);

// Constructor

CAcromagTCPDriver::CAcromagTCPDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CAcromagTCPDriver::~CAcromagTCPDriver(void)
{
	}

// Configuration

void MCALL CAcromagTCPDriver::Load(LPCBYTE pData)
{ 
	}
	
// Management

void MCALL CAcromagTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CAcromagTCPDriver::Open(void)
{
	CModbusTCPMaster::Open();
	}

// Device

CCODE MCALL CAcromagTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	return CModbusTCPMaster::DeviceOpen(pDevice);
	}

CCODE MCALL CAcromagTCPDriver::DeviceClose(BOOL fPersist)
{
	CCODE c = CModbusTCPMaster::DeviceClose(fPersist);

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CAcromagTCPDriver::Ping(void)
{
	return CModbusTCPMaster::Ping();
	}

CCODE MCALL CAcromagTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress AcroAddr;

	AcroAddr.m_Ref = Addr.m_Ref;

	if( RemapAcromagSpaces(AcroAddr) == LONGOK ) {

		uCount = 1;
		}

	return CModbusTCPMaster::Read(AcroAddr, pData, uCount);
	}

CCODE MCALL CAcromagTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress AcroAddr;

	AcroAddr.m_Ref = Addr.m_Ref;

	switch( RemapAcromagSpaces(AcroAddr) ) {

		case USEASIS: break;

		case LONGOK:
			uCount = 1;
			break;

		case NOWRITE:
			return uCount; // Read-Only
		}

	return CModbusTCPMaster::Write(AcroAddr, pData, uCount);
	}

UINT MCALL CAcromagTCPDriver::DevCtrl(void * pContext, UINT uFunc, PCTXT Value)
{
	return CModbusTCPMaster::DevCtrl(pContext, uFunc, Value);
	}

BOOL CAcromagTCPDriver::RemapAcromagSpaces(CAddress &Addr)
{
	switch( Addr.a.m_Table ) {

		case CIC3:
		case CCT3:
		case CIR3:
			Addr.a.m_Table  = SPACE_ANALOG;
			return NOWRITE;

		case CCS3:
			Addr.a.m_Table  = SPACE_ANALOG;
			Addr.a.m_Offset = 20;
			return NOWRITE;

		case CDI4: Addr.a.m_Offset = 39; break;

		case CDO4: Addr.a.m_Offset = 40; break;

		case CCS4: Addr.a.m_Offset = 43; break;

		case CCH4: Addr.a.m_Offset = 44; break;

		case CCR4: Addr.a.m_Offset = 45; break;

		case CCP4:			
		case COR4: break;

		default:
			return USEASIS;
		}

	Addr.a.m_Table = SPACE_HOLDING;

	return Addr.a.m_Type == addrLongAsLong ? LONGOK : USEASIS;
	}

// End of File
