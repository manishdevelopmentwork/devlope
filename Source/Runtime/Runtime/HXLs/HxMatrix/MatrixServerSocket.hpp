
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MatrixServerSocket_HPP

#define INCLUDE_MatrixServerSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MatrixSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix Server Socket
//

class CMatrixServerSocket : public CMatrixSocket
{
	public:
		// Constructor
		CMatrixServerSocket(CMatrixSsl *pSsl, ISocket *pSock, sslKeys_t *pKeys);

		// Destructor
		~CMatrixServerSocket(void);

		// ISocket Methods
		HRM Listen(WORD Loc);

	protected:
		// Overridables
		void InitSession(void);
	};

// End of File

#endif
