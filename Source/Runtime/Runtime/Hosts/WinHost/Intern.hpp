
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Intern_HPP

#define INCLUDE_Intern_HPP

//////////////////////////////////////////////////////////////////////////
//
// Environment Selection
//

#define AEON_NEED_WIN32

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include "../../HSL/AeonHsl.hpp"

#include "HostModel.hpp"

#include "HostConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Host Configuration
//

extern CHostConfig g_Config;

//////////////////////////////////////////////////////////////////////////
//
// Wait Accumulation
//

global void AddToSlept(UINT uTime);

// End of File

#endif
