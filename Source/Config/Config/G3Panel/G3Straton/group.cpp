
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Type
//

CStratonGroup::CStratonGroup(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Group Helper
//

CStratonGroupHelper::CStratonGroupHelper(DWORD dwProject)
{
	m_dwProject = dwProject;
	}

// Operations

UINT CStratonGroupHelper::GetHandles(CLongArray &Handles)
{
	DWORD dwGlobal = afxDatabase->FindGroup( m_dwProject, CString(K5DBGROUPNAME_GLOBAL));

	DWORD dwRetain = afxDatabase->FindGroup( m_dwProject, CString(K5DBGROUPNAME_RETAIN));

	if( dwGlobal && dwRetain ) {
		
		Handles.Append(dwGlobal);

		Handles.Append(dwRetain);

		return 2;
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Data Type Descriptor
//

CStratonGroupDescriptor::CStratonGroupDescriptor(DWORD dwProject, DWORD dwHandle)
{
	m_dwProject = dwProject;

	m_dwHandle = dwHandle;

	m_Name     = afxDatabase->GetGroupDesc(dwProject, dwHandle, m_dwStyle);
	}

// Attributes

CString CStratonGroupDescriptor::GetStyleName(void)
{
	switch( m_dwStyle ) {

		case K5DBGROUP_GLOBAL:	return	L"Project";
		case K5DBGROUP_RETAIN:	return	L"Retain";
		case K5DBGROUP_INPUT:	return	L"Input";
		case K5DBGROUP_OUTPUT:	return	L"Output";
		case K5DBGROUP_LOCAL:	return	L"Local";
		case K5DBGROUP_UDFB:	return	L"UDFB";
		
		}
		
	return CString();
	}

BOOL CStratonGroupDescriptor::IsRetentive(void) const
{
	return m_dwStyle == K5DBGROUP_RETAIN;
	}

BOOL CStratonGroupDescriptor::IsGlobal(void) const
{
	return m_dwStyle == K5DBGROUP_GLOBAL;
	}

// End of File
