
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Colorado CO10 Model Data
//

static BYTE imageCO10[] = {

	#include "co10-x1.png.dat"
	0
	};

global CHostModel modelCO10 = {

	"",
	"CO10",
	"CR1000-10",
	"CR1000-10000",
	rfColorado,
	1,
	988,
	878,
	94,
	124,
	800,
	600,
	0,
	NULL,
	sizeof(imageCO10)-1,
	imageCO10
	};

// End of File
