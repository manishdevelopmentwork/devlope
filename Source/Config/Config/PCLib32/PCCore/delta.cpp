
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// String Delta
//

// Constructors

CStringDelta::CStringDelta(void)
{
	m_nOldPos = -1;
	
	m_nNewPos = -1;

	m_nOldLen =  0;

	m_nNewLen =  0;
	}

// Attributes

CString CStringDelta::GetVerb(void) const
{
	if( m_nOldLen && m_nNewLen ) {

		if( m_nOldPos == m_nNewPos ) {

			return CString(IDS_EDIT);
			}
		else
			return CString(IDS_MOVE);
		}
	else {
		if( m_nNewLen ) {

			return CString(IDS_INSERT);
			}
		else
			return CString(IDS_DELETE);
		}
	}

CString CStringDelta::GetDetails(void) const
{
	if( m_nOldLen && m_nNewLen ) {

		if( m_nOldPos == m_nNewPos ) {

			return CPrintf( L"Edit \"%s\" to \"%s\" at %d",
					m_OldText,
					m_NewText,
					m_nOldPos
					);
			}
		else
			return CPrintf( L"Move %d characters from %d to %d",
					m_nOldLen,
					m_nOldPos,
					m_nNewPos
					);
		}
	else {
		if( m_nNewLen ) {

			return CPrintf( L"Insert \"%s\" at %d",
					m_NewText,
					m_nNewPos
					);
			}
		else {
			return CPrintf( L"Delete \"%s\" from %d",
					m_OldText,
					m_nOldPos
					);
			}
		}
	}

BOOL CStringDelta::IsNull(void) const
{
	return m_nOldPos < 0 && m_nNewPos < 0;
	}

BOOL CStringDelta::IsInsert(void) const
{
	return m_nOldLen && !m_nNewLen;
	}

BOOL CStringDelta::IsDelete(void) const
{
	return !m_nOldLen && m_nNewLen;
	}

BOOL CStringDelta::IsEdit(void) const
{
	return m_nOldLen && m_nNewLen && m_nOldPos == m_nNewPos;
	}

BOOL CStringDelta::IsMove(void) const
{
	return m_nOldLen && m_nNewLen && m_nOldPos != m_nNewPos;
	}

BOOL CStringDelta::CanCoalesce(CStringDelta const &That) const
{
	// TODO -- Add support for coalescing adjacent deltas.

	return FALSE;
	}

// Operations

void CStringDelta::Empty(void)
{
	m_nOldPos = -1;
	
	m_nNewPos = -1;

	m_nOldLen =  0;

	m_nNewLen =  0;

	m_NewText.Empty();

	m_OldText.Empty();
	}

BOOL CStringDelta::Coalesce(CStringDelta const &That)
{
	// TODO -- Add support for coalescing adjacent deltas.

	return IsNull();
	}

// End of File
