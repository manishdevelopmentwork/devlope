
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Structured Text Editor Window
//

class CStructuredTextEditorWnd : public CEditorWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CStructuredTextEditorWnd(void);

	protected:
		// Data
		BOOL	m_fShowLines;
		BOOL	m_fShowColor;

		// Accelerator
		CAccelerator m_Accel;

		// Message Map
		AfxDeclareMessageMap();
	
		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Command Handlers
		BOOL OnEditGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);
		BOOL OnViewGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);
		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolCommand(UINT uID);
	
		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);

		// Edit Menu
		void OnEditSelectVariable(void);
		void OnEditSelectFunctionBlock(void);

		// Access
		CStratonSTWnd & GetEditor(void);

		// Implementation
		void LoadConfig(void);
		void SaveConfig(void);
		void LoadSettings(void);
		void ShowDefaultStatus(void);
	};

/////////////////////////////////////////////////////////////////////////
//
// Structured Text Editor Window
//

// Dynamic Class

AfxImplementDynamicClass(CStructuredTextEditorWnd, CEditorWnd);

// Constructor

CStructuredTextEditorWnd::CStructuredTextEditorWnd(void)
{
	m_fShowLines = FALSE;

	m_fShowColor = FALSE;

	m_Accel.Create(L"StructuredTextEditorMenu");

	m_pEdit = New CStratonSTWnd;
	}

// Message Map

AfxMessageMap(CStructuredTextEditorWnd, CEditorWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)

	AfxDispatchGetInfoType(IDM_EDIT,  OnEditGetInfo )
	AfxDispatchControlType(IDM_EDIT,  OnEditControl )
	AfxDispatchCommandType(IDM_EDIT,  OnEditCommand )
	AfxDispatchGetInfoType(IDM_VIEW,  OnViewGetInfo )
	AfxDispatchControlType(IDM_VIEW,  OnViewControl )
	AfxDispatchCommandType(IDM_VIEW,  OnViewCommand )
	AfxDispatchGetInfoType(IDM_TOOL,  OnToolGetInfo )
	AfxDispatchControlType(IDM_TOOL,  OnToolControl )
	AfxDispatchCommandType(IDM_TOOL,  OnToolCommand )

	AfxMessageEnd(CStructuredTextEditorWnd)
	};

// Accelerators

BOOL CStructuredTextEditorWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CStructuredTextEditorWnd::OnPostCreate(void)
{
	CEditorWnd::OnPostCreate();

	LoadConfig();

	LoadSettings();
	}

void CStructuredTextEditorWnd::OnPreDestroy(void)
{
	CEditorWnd::OnPreDestroy();

	SaveConfig();
	}

// Command Handlers

BOOL CStructuredTextEditorWnd::OnEditGetInfo(UINT uID, CCmdInfo &Info)
{
	CStratonSTWnd &Wnd = GetEditor();

	switch( uID ) {

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CStructuredTextEditorWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	CStratonSTWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_EDIT_SEL_VAR:

			Src.EnableItem(!Wnd.IsDebug());

			break;

		case IDM_EDIT_SEL_FB:

			Src.EnableItem(!Wnd.IsDebug());

			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CStructuredTextEditorWnd::OnEditCommand(UINT uID)
{
	CStratonSTWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_EDIT_SEL_VAR:

			OnEditSelectVariable();

			break;

		case IDM_EDIT_SEL_FB:

			OnEditSelectFunctionBlock();

			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CStructuredTextEditorWnd::OnViewGetInfo(UINT uID, CCmdInfo &Info)
{
	CStratonSTWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_VIEW_VALUE_SHOW:

			Info.m_Image = 0x60000012;

			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CStructuredTextEditorWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	CStratonSTWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_VIEW_LINES_SHOW:

			Src.EnableItem(TRUE);
			
			Src.CheckItem(m_fShowLines);
			
			break;

		case IDM_VIEW_COLOR_SHOW:

			Src.EnableItem(TRUE);
			
			Src.CheckItem(m_fShowColor);
			
			break;

		case IDM_VIEW_VALUE_SHOW:

			Src.EnableItem(TRUE);
			
			Src.CheckItem(Wnd.GetValueInText());
			
			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CStructuredTextEditorWnd::OnViewCommand(UINT uID)
{
	CStratonSTWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_VIEW_LINES_SHOW:

			Wnd.ShowStLine(m_fShowLines = !m_fShowLines);

			break;

		case IDM_VIEW_COLOR_SHOW:

			Wnd.EnableSyntax(m_fShowColor = !m_fShowColor);

			Wnd.Invalidate(TRUE);

			break;

		case IDM_VIEW_VALUE_SHOW:

			Wnd.SetValueInText(!Wnd.GetValueInText());

			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CStructuredTextEditorWnd::OnToolGetInfo(UINT uID, CCmdInfo &Info)
{
	CStratonSTWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_INDENT:			
			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CStructuredTextEditorWnd::OnToolControl(UINT uID, CCmdSource &Src)
{
	CStratonSTWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_INDENT:
			Src.EnableItem(!Wnd.IsDebug() && Wnd.CanIndent());
			break;

		case IDM_TOOL_INSERT_COMMENT:
			Src.EnableItem(!Wnd.IsDebug() && Wnd.CanInsertComment());
			break;

		case IDM_TOOL_REMOVE_COMMENT:
			Src.EnableItem(!Wnd.IsDebug() && Wnd.CanRemoveComment());
			break;

		case IDM_TOOL_FUNCTION:
			Src.EnableItem(!Wnd.IsDebug());
			break;

		case IDM_TOOL_VARIABLE:
			Src.EnableItem(!Wnd.IsDebug());
			break;

		default:
			return FALSE;
		}

	AfxTouch(Wnd);

	return TRUE;
	}

BOOL CStructuredTextEditorWnd::OnToolCommand(UINT uID)
{
	CStratonSTWnd &Wnd = GetEditor();

	switch( uID ) {

		case IDM_TOOL_INDENT:
			Wnd.Indent();
			break;

		case IDM_TOOL_INSERT_COMMENT:
			Wnd.InsertComment();
			break;

		case IDM_TOOL_REMOVE_COMMENT:
			Wnd.RemoveComment();
			break;

		case IDM_TOOL_FUNCTION:
			OnEditSelectFunctionBlock();
			break;

		case IDM_TOOL_VARIABLE:
			OnEditSelectVariable();
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Edit Menu

void CStructuredTextEditorWnd::OnEditSelectVariable(void)
{
	SelectVariable();
	}

void CStructuredTextEditorWnd::OnEditSelectFunctionBlock(void)
{
	// TODO -- Implement

	if( TRUE ) {

		CStratonSTWnd &Wnd = GetEditor();

		CString Path = Wnd.GetProjectPath();

		CString Name;

		int  nIn = 2;
	
		int nOut;

		DWORD dwIdent;

		BOOL fInst, fDb;

		CString Block = Straton_SelBlockEx( GetHandle(), 
						    Path, 
						    Name, 
						    nIn, 
						    nOut, 
						    dwIdent, 
						    fInst, 
						    fDb
						    );

		AfxTrace(L"block [%s]\n", Block);
		}
	}

// Access

CStratonSTWnd & CStructuredTextEditorWnd::GetEditor(void)
{	
	return (CStratonSTWnd &) *m_pEdit;
	}

// Implementation

void CStructuredTextEditorWnd::LoadSettings(void)
{
	CStratonSTWnd &Wnd = GetEditor();

	DWORD dwMask = AUTOEDIT_ALL;

	dwMask &= ~AUTOEDIT_SETVAR;

	Wnd.SetAutoEdit(dwMask);

	Wnd.PromptVarName(TRUE);

	//Wnd.PromptInstance(TRUE);

	//Wnd.AutoDeclareInst(TRUE);

	Wnd.SetTab(8);
	}

void CStructuredTextEditorWnd::ShowDefaultStatus(void)
{
	CStratonSTWnd &Wnd = GetEditor();

	CString Text;

	if( !Wnd.IsDebug() ) {
		
		CPoint Pos = Wnd.GetCaret();

		Text += CPrintf(CString(IDS_LINE_FMT_CHAR_FMT), Pos.y, Pos.x);
		}

	afxThread->SetStatusText(Text);
	}

void CStructuredTextEditorWnd::LoadConfig(void)
{
	CEditorWnd::LoadConfig();

	CStratonSTWnd &Wnd = GetEditor();

	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(GetClassName());

	m_fShowLines         = (Reg.GetValue(L"ShowLines",	   UINT(TRUE)));

	m_fShowColor         = (Reg.GetValue(L"ShowColor",	   UINT(TRUE)));

	BOOL fShowValue      = (Reg.GetValue(L"ShowValue",	   UINT(FALSE)));

	Wnd.ShowStLine(m_fShowLines);

	Wnd.EnableSyntax(m_fShowColor);
	
	Wnd.SetValueInText(fShowValue);
	}

void CStructuredTextEditorWnd::SaveConfig(void)
{
	CEditorWnd::SaveConfig();

	CStratonSTWnd &Wnd = GetEditor();

	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Straton");

	Reg.MoveTo(GetClassName());

	Reg.SetValue(L"ShowLines",		m_fShowLines);

	Reg.SetValue(L"ShowColor",		m_fShowColor);

	Reg.SetValue(L"ShowValue",		Wnd.GetValueInText());
	
	AfxTouch(Wnd);
	}

// End of File
