
#include "Intern.hpp"

#include "HttpClientDigestAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Digest Authentication Method
//

// Constructor

CHttpClientDigestAuth::CHttpClientDigestAuth(void)
{
	m_uState = 0;
	}

// Operations

BOOL CHttpClientDigestAuth::CanAccept(CString Meth, UINT &uPriority)
{
	if( Meth == "Digest" ) {

		if( uPriority < priorityDigest ) {

			uPriority = priorityDigest;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CHttpClientDigestAuth::ProcessRequest(CString Line, BOOL fFail)
{
	if( m_uState == 2 ) {

		return FALSE;
		}

	if( m_uState == 1 && !fFail ) {

		return TRUE;
		}

	if( ParseLine(Line) ) {

		if( m_uState == 0 || GetParam("stale") == "true" ) {

			if( m_d.ProcessRequest(this) ) {

				m_uState = 1;

				return TRUE;
				}
			}
		}

	m_uState = 2;

	return FALSE;
	}

BOOL CHttpClientDigestAuth::ProcessInfo(CString Line, CString Path, PCBYTE pBody, UINT uBody)
{
	if( m_uState == 1 ) {

		#if defined(_DEBUG)

		// TODO -- I haven't found a server to test this against
		// so it should not be enabled in production builds until
		// it has been validated.

		if( ParseLine(Line) ) {

			CString Value;

			if( (Value = GetParam("rspauth")).GetLength() ) {

				CString ClientNonce = GetParam("cnonce");

				if( ClientNonce == m_d.m_ClientNonce ) {

					UINT uNonceCount = strtoul( GetParam("nc"),
								    NULL,
								    16
								    );
					
					if( uNonceCount == m_d.m_uNonceCount ) {

						CString Res = m_d.GetResponse(  GetParam("qop"),
										"",
										Path,
										pBody,
										uBody,
										m_User,
										m_Pass
										);

						if( Res != Value ) {

							return FALSE;
							}
						}
					}
				}

			if( (Value = GetParam("nextnonce")).GetLength() ) {

				m_d.m_Nonce = Value;
				}
			}

		#endif
		}

	return TRUE;
	}

CString CHttpClientDigestAuth::GetAuthHeader(CString Verb, CString Path, CBytes Body)
{
	CString Head;

	Head += "username=\""  + m_User            + "\",";

	Head += "uri=\""       + Path              + "\",";

	Head += "algorithm=\"" + m_d.m_Algorithm   + "\",";

	Head += "qop=\""       + m_d.m_qop         + "\",";

	Head += "realm=\""     + m_d.m_Realm       + "\",";

	Head += "nonce=\""     + m_d.m_Nonce       + "\",";

	Head += "opaque=\""    + m_d.m_Opaque      + "\",";

	Head += "cnonce=\""    + m_d.m_ClientNonce + "\",";

	Head += "nc="          + CPrintf("%8.8x,", ++m_d.m_uNonceCount);

	CString Res = m_d.GetResponse(  m_d.m_qop,
					Verb,
					Path,
					Body.GetPointer(),
					Body.GetCount  (),
					m_User,
					m_Pass
					);

	Head += "response=\"" + Res + "\"";

	return "Authorization: Digest " + Head + "\r\n";
	}

// End of File
