
#include "Intern.hpp"

#include "Watchdog51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define Reg(x) (m_pBase[reg##x])

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Watchdog Timer
//

// Constructor

CWatchdog51::CWatchdog51(void)
{
	m_pBase = PVWORD(ADDR_WDOG1);
	}

// Operations

void CWatchdog51::Enable(UINT uTime)
{
	Reg(Control)   = (uTime << 8) | Bit(5) | Bit(4);

	Reg(Interrupt) = 0;

	Reg(Misc)      = 0;

	Reg(Control)  |= Bit(2);

	Kick();
	}

void CWatchdog51::Kick(void)
{
	Reg(Service) = 0x5555;

	Reg(Service) = 0xAAAA;
	}

// End of File
