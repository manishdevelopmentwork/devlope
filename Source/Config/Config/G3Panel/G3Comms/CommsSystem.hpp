
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsSystem_HPP

#define INCLUDE_CommsSystem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCodedItem;
class CCodedText;
class CColorManager;
class CCommsDevice;
class CCommsManager;
class CCommsMapping;
class CCommsPort;
class CCommsPortList;
class CControlManager;
class CDataLogger;
class CDataServer;
class CDevCon;
class CJsonData;
class CLangManager;
class CLexicon;
class CNameServer;
class COptionCardList;
class COptionCardRackList;
class CPersistManager;
class CProgramManager;
class CSecurityManager;
class CSqlQueryManager;
class CSystemLibrary;
class CTagManager;
class CTagSet;
class CWatchViewWnd;
class CWebServer;

//////////////////////////////////////////////////////////////////////////
//
// Communications System Item
//

class DLLAPI CCommsSystem : public CSystemItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CCommsSystem(BOOL fAux);

	// Destructor
	~CCommsSystem(void);

	// Model Control
	BOOL    SetModel(CString const &Model);
	CString GetModel(void) const;
	CString GetEmulatorModel(void) const;
	CString GetDisplayName(void) const;

	// Model Editing
	BOOL    SetModelSpec(CString const &Model);
	CString GetModelSpec(void);

	// Version Test
	BOOL NeedNewSoftware(void) const;

	// Conversion
	CString GetSpecies(void) const;

	// UI Creation
	CViewWnd * CreateView(UINT uType);

	// Extra Commands
	UINT    GetExtraCount(void);
	CString GetExtraText(UINT uCmd);
	BOOL    GetExtraState(UINT uCmd);
	BOOL    RunExtraCmd(CWnd &Wnd, UINT uCmd);

	// Searching
	BOOL IsTagUsed(UINT uTag);
	BOOL FindTagUsage(CString Tag, UINT uTag);
	BOOL FindHandleUsage(CString Type, CString Name, UINT hItem);
	BOOL FindDeviceUsage(CCommsDevice *pDevice);
	BOOL FindPortUsage(CCommsPort *pPort);
	BOOL FindBroken(void);
	BOOL FindCircular(void);
	BOOL GlobalFind(void);

	// Server Access
	CNameServer * GetNameServer(void) const;
	CDataServer * GetDataServer(void) const;

	// Attributes
	BOOL HasBroken(void) const;
	BOOL HasCircular(void) const;
	BOOL HasHardware(void) const;
	BOOL CanStepFragment(CString Code) const;
	BOOL HasNextAddress(void) const;
	BOOL IsOnWatch(CString Code) const;

	// Operations
	void ClearSysBlocks(void);
	void NotifyInit(void);
	void Validate(BOOL fExpand);
	void TagCheck(UINT uTag, BOOL fQuick);
	void ObjCheck(UINT uObj, BOOL fQuick);
	BOOL StepFragment(CString &Code, UINT uStep);
	void SetLastAddress(CString  Code);
	BOOL GetNextAddress(CString &Code, BOOL fInc);
	void Rebuild(UINT uAction);
	void ShowHardware(void);
	void UpdateHardware(void);
	void ClearWatch(void);
	BOOL AddToWatch(CMetaItem *pItem);
	BOOL AddToWatch(CDataRef const &Ref);
	BOOL AddToWatch(CString Code);
	BOOL UpdateWatch(BOOL fShow);

	// Conversion
	void PostConvert(void);

	// Control Project
	BOOL NeedBuild(void);
	BOOL PerformBuild(void);
	BOOL HasControl(void);

	// SQL Queries
	BOOL CheckSqlQueries(void);

	// Watch Window
	CWnd * GetWatchWindow(void);

	// Item Naming
	CString GetHumanName(void) const;
	CString GetFixedName(void) const;

	// Persistance
	void Init(void);
	void PostLoad(void);
	void Save(CTreeFile &Tree);

	// Property Save Filter
	BOOL SaveProp(CString const &Tag) const;

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// DLD Selection
	virtual CString GetProcessor(void) const;
	virtual CString GetDldFolder(void) const;

	// Linked Lists
	CCodedItem    * m_pHeadCoded;
	CCodedItem    * m_pTailCoded;
	CCommsMapping * m_pHeadMapping;
	CCommsMapping * m_pTailMapping;
	CTagSet       * m_pHeadTagSet;
	CTagSet       * m_pTailTagSet;

	// Item Properties
	UINT		   m_Format;
	UINT		   m_Major;
	UINT		   m_Level;
	CString		   m_LastAddr;
	CCommsManager    * m_pComms;
	CTagManager	 * m_pTags;
	CPersistManager	 * m_pPersist;
	CProgramManager  * m_pPrograms;
	CWebServer       * m_pWeb;
	CDataLogger      * m_pLog;
	CSecurityManager * m_pSecure;
	CLangManager     * m_pLang;
	CColorManager    * m_pColor;
	CSystemLibrary   * m_pSysLib;
	CTagSet          * m_pWatch;
	CControlManager  * m_pControl;
	CSqlQueryManager * m_pSql;
	CDevCon	         * m_pDevCon;

protected:
	// Lexicon Options
	typedef CMap <CCaseString, UINT> CLexOpts;

	// Lexicon Language
	struct CLexLang
	{
		UINT	 m_uMax;
		INDEX	 m_iMax;
		CLexOpts m_Opts;
	};

	// Lexicon Map
	typedef CMap <CCaseString, CArray <CLexLang *> *> CLexMap;

	// Data Members
	CNameServer   * m_pNameServer;
	CDataServer   * m_pDataServer;
	CWatchViewWnd * m_pWatchView;
	CString         m_LastFind;
	BOOL	        m_fAux;

	// Device Config
	UINT GetSoftwareGroup(void) const;
	BOOL HasExpansion(void) const;
	BOOL GetDeviceConfig(CByteArray &Data, UINT uItem) const;
	BOOL DisableDeviceConfig(UINT uItem);

	// System Data
	virtual void AddNavCats(void);
	virtual void AddResCats(void);

	// System Data
	void AddNavCatHead(void);
	void AddNavCatTail(void);
	void AddResCatHead(void);
	void AddResCatTail(void);

	// Validation
	virtual BOOL DoValidate(CCodedTree &Done, BOOL fExpand, UINT uStep);
	virtual BOOL DoTagCheck(CCodedTree &Done, CIndexTree &Tags, UINT uTag, UINT uStep);
	virtual BOOL DoObjCheck(CCodedTree &Done, CIndexTree &Objs, UINT uObj, UINT uStep);

	// Server Creation
	virtual void MakeServers(void);

	// System Hooks
	virtual void ChangeModel(void);
	virtual void ApplyHardware(BOOL fInit);

	// Meta Data
	void AddMetaData(void);

	// String Utilities
	BOOL OnExportStrings(CWnd &Wnd);
	BOOL OnImportStrings(CWnd &Wnd);
	BOOL DoExportStrings(CError &Error, ITextStream &Stm, WCHAR cSep);
	BOOL DoImportStrings(CError &Error, ITextStream &Stm, WCHAR cSep);

	// Lexicon Utilities
	BOOL OnExportLexicon(CWnd &Wnd);
	BOOL OnImportLexicon(CWnd &Wnd);
	BOOL DoExportLexicon(CError &Error, ITextStream &Stm, WCHAR cSep);
	BOOL DoImportLexicon(CError &Error, CLexicon &Lex);
	void KillList(CArray <CLexLang *> *pList);
	void Trim(CString &Base);
	void Trim(CString &Trans, CString const &Base);

	// Clear Translations
	BOOL OnClearTranslations(CWnd &Wnd);

	// Auto Translation
	BOOL OnAutoTranslate(CWnd &Wnd);

	// Database Utilities
	BOOL OnRecompile(CWnd &Wnd);
	BOOL OnRebuild(CWnd &Wnd);
	BOOL OnProgramize(CWnd &Wnd);

	// Implementation
	BOOL CheckFormat(void);
	BOOL ListCodedText(CArray <CCodedText *> &List);
	void FixClass(CCodedItem *pCode, CCodedItem *pFrom);
	void FindCodedWithRef(CStringArray &List, DWORD Data, DWORD Mask);
	void FindCodedWithDevice(CStringArray &List, UINT uDevice);
	void FindCodedWithPort(CStringArray &List, UINT uPort);
	void FindCodedWithText(CStringArray &List, CString Find);
	void FindTagSetWithTag(CStringArray &List, UINT uTag);
	void FindMappingWithTag(CStringArray &List, UINT uTag);
	void UpdateWindow(void);
	void Rebuild(void);
};

// End of File

#endif
