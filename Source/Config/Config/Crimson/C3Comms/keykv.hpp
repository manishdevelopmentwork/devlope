
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_KEYKV_HPP
	
#define	INCLUDE_KEYKV_HPP

//////////////////////////////////////////////////////////////////////////
//
// Keyence KV Series PLC Master Serial Device Options
//

class CKeyKVMasterSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKeyKVMasterSerialDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT m_Conn;
		UINT m_Drop;
					
	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	}; 


//////////////////////////////////////////////////////////////////////////
//
// Keyence KV Series PLC Master Serial Driver
//

class CKeyKVMasterSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CKeyKVMasterSerialDriver(void);  

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
	
       	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Keyence KV Series Space Wrapper
//

class CSpaceKeyKV : public CSpace
{
	public:
		// Constructors
		CSpaceKeyKV(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);
		
	};




// End of File

#endif
