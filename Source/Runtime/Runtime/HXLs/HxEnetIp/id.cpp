/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** ID.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Identity object
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

EtIPIdentityInfo gIDInfo;	/* Identity information */


/*---------------------------------------------------------------------------
** idInit( )
**
** Assign default values to the gIDInfo structure
**---------------------------------------------------------------------------
*/

void idInit()
{
	gIDInfo.iVendor = ID_VENDOR;
	gIDInfo.iProductType = ID_PRODUCT_TYPE;
	gIDInfo.iProductCode = ID_PRODUCT_CODE;
	gIDInfo.bMajorRevision = ID_MAJOR_REVISION;
	gIDInfo.bMinorRevision = ID_MINOR_REVISION;
	gIDInfo.lSerialNumber = ID_SERIAL_NUMBER;
	gIDInfo.iStatus = idGetStatus();		
	strcpy(gIDInfo.productName, ID_PRODUCT_NAME);
}

/*---------------------------------------------------------------------------
** idGetStatus( )
**
** Sets appropriate status bits based on the current state
**---------------------------------------------------------------------------
*/

UINT16 idGetStatus()
{
	UINT16 iStatus = ID_STATUS_CONFIGURED;
	
#ifdef ET_IP_SCANNER	
	if ( !gbRunMode )
#else
	if ( TRUE )
#endif
		iStatus |= ID_STATUS_IDLE;
	else if ( connectionGetConnectedCount() )
		iStatus |= ID_STATUS_CONNECTED;
	else
		iStatus |= ID_STATUS_AWAIT_CONN;

	return iStatus;
}
							
/*---------------------------------------------------------------------------
** idParseClassInstanceRequest( )
**
** Determine if it's request for the Class or for the particular instance 
**---------------------------------------------------------------------------
*/

void idParseClassInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 0 )
		idParseClassRequest( nRequest );
	else
		idParseInstanceRequest( nRequest );
}

/*---------------------------------------------------------------------------
** idParseClassRequest( )
**
** Respond to the class request
**---------------------------------------------------------------------------
*/

void idParseClassRequest( INT32 nRequest )
{
	switch( gRequests[nRequest].bService )
	{
		case SVC_GET_ATTR_ALL:
			idSendClassAttrAll( nRequest );
			break;
		default:		
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
			break;
	}
}

/*---------------------------------------------------------------------------
** idParseInstanceRequest( )
**
** Respond to the instance request
**---------------------------------------------------------------------------
*/

void idParseInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 1 )
	{
		switch( gRequests[nRequest].bService )
		{
			case SVC_GET_ATTR_ALL:
				idSendInstanceAttrAll( nRequest );
				break;
			case SVC_GET_ATTR_SINGLE:
				idSendInstanceAttrSingle( nRequest );
				break;
			case SVC_RESET:
				idReset( nRequest );
				break;
			default:
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
				break;
		}
	}
	else
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;
}

/*---------------------------------------------------------------------------
** idSendClassAttrAll( )
**
** GetAttributeAll service for Identity class is received 
**---------------------------------------------------------------------------
*/

void idSendClassAttrAll( INT32 nRequest )
{
	ID_CLASS_ATTRIBUTE idAttr;

	idAttr.iRevision = ENCAP_TO_HS(ID_CLASS_REVISION);
	idAttr.iMaxInstance = ENCAP_TO_HS(1);
	idAttr.iMaxClassAttr = 0;
	idAttr.iMaxInstanceAttr = 0;
	
	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&idAttr, sizeof(ID_CLASS_ATTRIBUTE) );			
}

/*---------------------------------------------------------------------------
** idSendInstanceAttrAll( )
**
** GetAttributeAll service for Identity instance is received
**---------------------------------------------------------------------------
*/

void idSendInstanceAttrAll( INT32 nRequest )
{	
	UINT8 bProductNameSize = UINT8(strlen(gIDInfo.productName));	/* Product name size */
	UINT8* pData;
	
	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, (UINT16)(ID_INSTANCE_ATTRIBUTE_SIZE + bProductNameSize + sizeof(UINT8)) );		

	if ( gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_NO_RESOURCE;	/* Out of memory */	
		return;
	}
		
	pData = MEM_PTR( gRequests[nRequest].iDataOffset );
	
	UINT16_SET(pData, gIDInfo.iVendor);
	pData += sizeof(UINT16);

	UINT16_SET(pData, gIDInfo.iProductType);
	pData += sizeof(UINT16);

	UINT16_SET(pData, gIDInfo.iProductCode);
	pData += sizeof(UINT16);

	*pData++ = gIDInfo.bMajorRevision;
	*pData++ = gIDInfo.bMinorRevision;

	gIDInfo.iStatus = idGetStatus();		
	UINT16_SET(pData, gIDInfo.iStatus);
	pData += sizeof(UINT16);

	UINT32_SET(pData, gIDInfo.lSerialNumber);
	pData += sizeof(UINT32);

	*pData++ = bProductNameSize;
		
	memcpy( pData, gIDInfo.productName, bProductNameSize );	
}


/*--------------------------------------------------------------------------------
** idSendInstanceAttrSingle( )
**
** GetAttributeSingle request has been received for the Identity object instance
**--------------------------------------------------------------------------------
*/

void idSendInstanceAttrSingle( INT32 nRequest )
{
	UINT8  bProductNameSize = UINT8(strlen(gIDInfo.productName));	/* Product name size */
	UINT8* pData;
	UINT32 lVal;
	UINT16 iVal;

	switch( gRequests[nRequest].iAttribute )
	{
		case ID_ATTR_VENDOR_ID:
			iVal = ENCAP_TO_HS(gIDInfo.iVendor);			
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case ID_ATTR_DEVICE_TYPE:
			iVal = ENCAP_TO_HS(gIDInfo.iProductType);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case ID_ATTR_PRODUCT_CODE:
			iVal = ENCAP_TO_HS(gIDInfo.iProductCode);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case ID_ATTR_REVISION:
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, sizeof(UINT16) );		
			if ( gRequests[nRequest].iDataOffset != INVALID_MEMORY_OFFSET )
			{
				pData = MEM_PTR(gRequests[nRequest].iDataOffset);			
				*pData = gIDInfo.bMajorRevision;
				*(pData+1) = gIDInfo.bMinorRevision;			
			}
			break;
		case ID_ATTR_STATUS:
			gIDInfo.iStatus = idGetStatus();		
			iVal = ENCAP_TO_HS(gIDInfo.iStatus);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case ID_ATTR_SERIAL_NBR:
			lVal = ENCAP_TO_HL(gIDInfo.lSerialNumber);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&lVal, sizeof(UINT32) );		
			break;
		case ID_ATTR_PRODUCT_NAME:
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, (UINT16)(bProductNameSize + sizeof(UINT8)) );		
			if ( gRequests[nRequest].iDataOffset != INVALID_MEMORY_OFFSET )
			{
				pData = MEM_PTR(gRequests[nRequest].iDataOffset);						
				*pData++ = bProductNameSize;			
				memcpy( (char*)pData, gIDInfo.productName, bProductNameSize );			
			}
			break;
		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}			
}


/*---------------------------------------------------------------------------
** idReset( )
**
** Reset request is recieved.
**---------------------------------------------------------------------------
*/

void idReset( INT32 nRequest )
{	
	UINT8* pData;
	UINT8  bReset;
	
	if ( gRequests[nRequest].iDataSize )
	{
		pData = MEM_PTR(gRequests[nRequest].iDataOffset);
		bReset = *pData;
		utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );	
		if ( bReset > 1 )	/* Only 0 or 1 could be passed */
		{
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_PARAMETER_VALUE;
			return;
		}
	}
	
	utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );	
	
	ucmmSendObjectResponse( nRequest );	/* We are about to reset - send the response first */
	
	connectionRemoveAll();
	requestRemoveAll();
}















