
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcVariableNode_HPP

#define INCLUDE_OpcVariableNode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OpcNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Variable Node
//

class COpcVariableNode : public COpcNode
{
public:
	// Constructors
	COpcVariableNode(COpcDataModel *pModel, UINT Namespace, UINT Value);
	COpcVariableNode(COpcDataModel *pModel, UINT Namespace, CString const &Value);
	COpcVariableNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value);
	COpcVariableNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value);
	COpcVariableNode(COpcVariableNode const &That);

	// Assignment
	COpcVariableNode operator = (COpcVariableNode const &That);

	// Attributes
	COpcNodeId const & GetDataType(void) const;
	INT		   GetValueRank(void) const;
	UINT               GetArrayDimensions(void) const;
	UINT		   GetArrayDimension(UINT n) const;
	BYTE		   GetAccessLevel(void) const;
	BYTE		   GetUserAccessLevel(void) const;
	bool		   IsHistorizing(void) const;
	UINT		   GetHistorySlot(void) const;

	// Operations
	void SetDataType(UINT nsType, UINT idType);
	void SetRank(INT Rank, UINT Dim1);
	void SetAccess(UINT Access);
	void SetHistory(UINT uHistory);

	// Validation
	bool Validate(void);

protected:
	// Data Members
	COpcNodeId m_DataType;
	INT        m_Rank;
	UINT       m_Dim1;
	UINT       m_Access;
	bool       m_fHistory;
	UINT       m_uHistory;
};

//////////////////////////////////////////////////////////////////////////
//
// Variable Node
//

// Attributes

STRONG_INLINE COpcNodeId const & COpcVariableNode::GetDataType(void) const
{
	return m_DataType;
}

STRONG_INLINE INT COpcVariableNode::GetValueRank(void) const
{
	return m_Rank;
}

STRONG_INLINE UINT COpcVariableNode::GetArrayDimensions(void) const
{
	switch( m_Rank ) {

		case OpcUa_ValueRanks_Any:
		case OpcUa_ValueRanks_Scalar:

			return 0;

		case OpcUa_ValueRanks_OneDimension:

			return 1;
	}

	return 0;
}

STRONG_INLINE UINT COpcVariableNode::GetArrayDimension(UINT n) const
{
	return m_Dim1;
}

STRONG_INLINE BYTE COpcVariableNode::GetAccessLevel(void) const
{
	return BYTE(m_Access);
}

STRONG_INLINE BYTE COpcVariableNode::GetUserAccessLevel(void) const
{
	return BYTE(m_Access);
}

STRONG_INLINE bool COpcVariableNode::IsHistorizing(void) const
{
	return m_fHistory;
}

STRONG_INLINE UINT COpcVariableNode::GetHistorySlot(void) const
{
	return m_uHistory;
}

// End of File

#endif
