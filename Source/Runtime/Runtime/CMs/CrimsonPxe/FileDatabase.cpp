
#include "Intern.hpp"

#include "FileDatabase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

////////////////////////////////////////////////////////////////////////
//
// Filing System Database
//

// Sync Function

clink int sync(void);

// Decompressor

clink int fastlz_decompress(void const *input, int length, void *output);

// Instantiator

global IDatabase * Create_FileDatabase(CString const &Root, UINT uBase, UINT uPool)
{
	return New CFileDatabase(Root, uBase, uPool);
}

// Constructor

CFileDatabase::CFileDatabase(CString const &Root, UINT uBase, UINT uPool)
{
	StdSetRef();

	m_Root       = Root;

	m_uBase      = uBase;

	m_pMutex     = Create_Mutex();

	m_uPoolSize  = uPool * 1024;

	m_uPoolUsed  = 0;

	m_pHead      = NULL;

	m_pTail      = NULL;

	m_pState     = New CState[dataItemCount];

	m_fDirty     = FALSE;

	mkdir(m_Root, 0755);
}

// Destructor

CFileDatabase::~CFileDatabase(void)
{
	delete[] m_pState;

	AfxRelease(m_pMutex);
}

// IUnknown

HRESULT CFileDatabase::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDatabase);

	StdQueryInterface(IDatabase);

	return E_NOINTERFACE;
}

ULONG CFileDatabase::AddRef(void)
{
	StdAddRef();
}

ULONG CFileDatabase::Release(void)
{
	StdRelease();
}

// IDatabase

void CFileDatabase::Init(void)
{
	if( IsValid() ) {

		if( MountItems() ) {

			return;
		}

		SetValid(FALSE);
	}

	Clear();
}

void CFileDatabase::Clear(void)
{
	if( !IsClear() ) {

		SetValid(FALSE);

		EraseAll();

		if( m_pHead ) {

			return;
		}
	}

	MountItems();

	SetClear();
}

BOOL CFileDatabase::IsValid(void)
{
	return GetValid() == magicFRAM;
}

void CFileDatabase::SetValid(BOOL fValid)
{
	if( fValid != IsValid() ) {

		PutValid(fValid ? magicFRAM : 0);
	}

	Commit();
}

void CFileDatabase::SetRunning(BOOL fRun)
{
	if( !fRun ) {

		for( UINT uSlot = 0; uSlot < dataItemCount; uSlot++ ) {

			CState &State = m_pState[uSlot];

			if( State.m_fValid ) {

				if( State.m_pData ) {

					delete[] State.m_pData;

					State.m_pData = NULL;
				}

				State.m_uLock = 0;
			}
		}

		m_uPoolUsed = 0;

		m_pHead     = NULL;

		m_pTail     = NULL;
	}
}

BOOL CFileDatabase::GarbageCollect(void)
{
	return FALSE;
}

BOOL CFileDatabase::GetVersion(PBYTE pGuid)
{
	if( m_fProps ) {

		memcpy(pGuid, &m_Guid, sizeof(m_Guid));

		return TRUE;
	}

	return FALSE;
}

DWORD CFileDatabase::GetRevision(void)
{
	if( m_fProps ) {

		return m_Revision;
	}

	return 0;
}

BOOL CFileDatabase::SetVersion(PCBYTE pData)
{
	if( !m_fProps || memcmp(&m_Guid, pData, 16) ) {

		memcpy(&m_Guid, pData, 16);

		CAutoFile File(MakeFile("version"), "r+", "w+");

		if( File ) {

			File.Write(&m_Guid, 1);

			m_fDirty = TRUE;

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CFileDatabase::SetRevision(DWORD dwRev)
{
	if( !m_fProps || m_Revision != dwRev ) {

		m_Revision = dwRev;

		m_fProps   = TRUE;

		CAutoFile File(MakeFile("revision"), "r+", "w+");

		if( File ) {

			File.Write(&m_Revision, 1);

			File.Close();

			m_fDirty = TRUE;

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CFileDatabase::CheckSpace(UINT uSize)
{
	return TRUE;
}

BOOL CFileDatabase::WriteItem(CItemInfo const &Info, PCVOID pData)
{
	UINT uSlot = Info.m_uItem + m_uBase;

	if( uSlot < dataItemCount ) {

		CString   Name(MakeFile(uSlot));

		CAutoFile File(Name, "w+");

		if( File ) {

			CState &State = m_pState[uSlot];

			State.m_Head.m_Class = Info.m_uClass;

			State.m_Head.m_CRC   = crc(PCBYTE(pData), Info.m_uComp);

			State.m_Head.m_Size  = Info.m_uSize;

			State.m_Head.m_Comp  = Info.m_uComp;

			State.m_Head.m_Gen   = State.m_Head.m_Gen + 1;

			DWORD Magic = magicItem;

			if( File.Write(&Magic, 1) == 1 ) {

				if( File.Write(&State.m_Head, 1) == 1 ) {

					if( File.Write(&Magic, 1) == 1 ) {

						if( File.Write(PCBYTE(pData), Info.m_uComp) == Info.m_uComp ) {

							if( File.Write(&Magic, 1) == 1 ) {

								State.m_fValid = TRUE;

								m_fDirty       = TRUE;

								return TRUE;
							}
						}
					}
				}
			}

			unlink(Name);

			m_fDirty = TRUE;
		}
	}

	return FALSE;
}

BOOL CFileDatabase::GetItemInfo(UINT uItem, CItemInfo &Info)
{
	UINT uSlot = uItem + m_uBase;

	if( uSlot < dataItemCount ) {

		CState const &State = m_pState[uSlot];

		if( State.m_fValid ) {

			Info.m_uItem  = uItem;

			Info.m_uClass = State.m_Head.m_Class;

			Info.m_uSize  = State.m_Head.m_Size;

			Info.m_uComp  = State.m_Head.m_Comp;

			Info.m_CRC    = State.m_Head.m_CRC;

			return TRUE;
		}
	}

	return FALSE;
}

PCVOID CFileDatabase::LockItem(UINT uItem, CItemInfo &Info)
{
	UINT uSlot = uItem + m_uBase;

	if( uSlot < dataItemCount ) {

		CState &State = m_pState[uSlot];

		if( State.m_fValid ) {

			if( m_pMutex->Wait(FOREVER) ) {

				if( !State.m_uLock++ && !State.m_pData ) {

					CAutoFile File(MakeFile(uSlot), "r");

					if( File ) {

						DWORD Magic;

						if( File.Read(&Magic, 1) == 1 && Magic == magicItem ) {

							File.SeekFromHere(sizeof(State.m_Head));

							if( File.Read(&Magic, 1) == 1 && Magic == magicItem ) {

								State.m_pData = New BYTE[State.m_Head.m_Comp];

								if( File.Read(State.m_pData, State.m_Head.m_Comp) == State.m_Head.m_Comp ) {

									if( File.Read(&Magic, 1) == 1 && Magic == magicItem ) {

										if( State.m_Head.m_Comp != State.m_Head.m_Size ) {

											PBYTE pWork = New BYTE[State.m_Head.m_Size];

											PBYTE pPrev = State.m_pData;

											fastlz_decompress(pPrev, State.m_Head.m_Comp, pWork);

											State.m_pData = pWork;

											delete[] pPrev;
										}

										AfxListAppend(m_pHead, m_pTail, (&State), m_pNext, m_pPrev);

										m_uPoolUsed += State.m_Head.m_Size;

										m_pMutex->Free();

										return State.m_pData;
									}
								}

								delete[] State.m_pData;

								State.m_pData = NULL;
							}
						}
					}

					State.m_uLock--;

					m_pMutex->Free();

					// Clear database and reset???!!!
				}
				else {
					Info.m_uItem  = uItem;

					Info.m_uClass = State.m_Head.m_Class;

					Info.m_uSize  = State.m_Head.m_Size;

					Info.m_uComp  = State.m_Head.m_Comp;

					Info.m_CRC    = State.m_Head.m_CRC;

					AfxListRemove(m_pHead, m_pTail, (&State), m_pNext, m_pPrev);

					AfxListAppend(m_pHead, m_pTail, (&State), m_pNext, m_pPrev);

					m_pMutex->Free();

					return State.m_pData;
				}
			}
		}
	}

	return NULL;
}

PCVOID CFileDatabase::LockItem(UINT uItem)
{
	CItemInfo Info;

	return LockItem(uItem, Info);
}

void CFileDatabase::PendItem(UINT uItem, BOOL fPend)
{
}

void CFileDatabase::LockPendingItems(BOOL fLock)
{
}

void CFileDatabase::FreeItem(UINT uItem)
{
	UINT uSlot = uItem + m_uBase;

	if( uSlot < dataItemCount ) {

		CState &State = m_pState[uSlot];

		if( State.m_fValid ) {

			m_pMutex->Wait(FOREVER);

			if( !--State.m_uLock ) {

				CheckPoolUsage();
			}

			m_pMutex->Free();
		}
	}
}

// Item State Array

bool CFileDatabase::MountItems(void)
{
	m_fProps   = FALSE;

	m_Revision = 0xAAAAAAAA;

	memset(&m_Guid, 0xAA, 16);

	memset(m_pState, 0, sizeof(CState) * dataItemCount);

	return FindItems();
}

bool CFileDatabase::FindItems(void)
{
	CAutoDirentList List;

	if( List.ScanFiles(m_Root) ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			dirent const *dir = List[n];

			if( strlen(dir->d_name) == 9 ) {

				if( !strcmp(dir->d_name + 4, ".item") ) {

					UINT uSlot = strtoul(dir->d_name, NULL, 16);

					if( uSlot < dataItemCount ) {

						CString   Name = MakeFile(dir->d_name);

						CAutoFile File(Name, "r");

						if( File ) {

							DWORD Magic;

							if( File.Read(&Magic, 1) == 1 && Magic == magicItem ) {

								CState &State = m_pState[uSlot];

								if( File.Read(&State.m_Head, 1) == 1 ) {

									State.m_fValid = TRUE;

									continue;
								}
							}
						}

						unlink(Name);
					}
				}
			}

			if( !strcmp(dir->d_name, "valid") ) {

				continue;
			}

			if( !strcmp(dir->d_name, "revision") ) {

				continue;
			}

			if( !strcmp(dir->d_name, "version") ) {

				continue;
			}

			unlink(MakeFile(dir->d_name));
		}
	}

	CAutoFile File1(MakeFile("version"), "r");

	CAutoFile File2(MakeFile("revision"), "r");

	if( File1 && File2 ) {

		if( File1.Read(&m_Guid, 1) == 1 && File2.Read(&m_Revision, 1) == 1 ) {

			m_fProps = TRUE;
		}
	}

	return true;
}

void CFileDatabase::EraseAll(void)
{
	CAutoDirentList List;

	if( List.ScanFiles(m_Root) ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			dirent const *dir = List[n];

			unlink(MakeFile(dir->d_name));
		}

		m_fDirty = TRUE;
	}
}

void CFileDatabase::CheckPoolUsage(void)
{
	if( m_uPoolUsed > 7 * m_uPoolSize / 8 ) {

		CState *pScan = m_pHead;

		while( m_uPoolUsed > 5 * m_uPoolSize / 8 ) {

			if( pScan ) {

				CState &State = *pScan;

				if( !State.m_uLock ) {

					delete[] State.m_pData;

					State.m_pData = NULL;

					m_uPoolUsed  -= State.m_Head.m_Size;

					pScan         = pScan->m_pNext;

					AfxListRemove(m_pHead, m_pTail, (&State), m_pNext, m_pPrev);

					continue;
				}

				pScan = pScan->m_pNext;

				continue;
			}

			break;
		}
	}
}

CString CFileDatabase::MakeFile(CString const &Special)
{
	CString File(m_Root);

	File.Append(Special);

	return File;
}

CString CFileDatabase::MakeFile(UINT uItem)
{
	CString File(m_Root);

	File.AppendPrintf("%4.4X.item", uItem);

	return File;
}

// Implementation

BOOL CFileDatabase::IsClear(void)
{
	return GetValid() == clearFRAM;
}

void CFileDatabase::SetClear(void)
{
	PutValid(clearFRAM);
}

DWORD CFileDatabase::GetValid(void)
{
	CAutoFile File(MakeFile("valid"), "r");

	if( File ) {

		DWORD dwData = 0;

		File.Read(&dwData, 1);

		return dwData;
	}

	return 0;
}

void CFileDatabase::PutValid(DWORD dwData)
{
	CAutoFile File(MakeFile("valid"), "r+", "w+");

	if( File ) {

		File.Write(&dwData, 1);

		m_fDirty = TRUE;
	}
}

void CFileDatabase::Commit(void)
{
	if( m_fDirty ) {

		sync();

		m_fDirty = FALSE;
	}
}

// End of File
