
//////////////////////////////////////////////////////////////////////////
//
// YaskawaFSP Drive Driver
//

// Read Pacing
#define	PACE	10 // cycle for actual reads
#define	STQTY	10 // 10 items stored

class CYaskawaFSPDriver : public CMasterDriver
{
	public:
		// Constructor
		CYaskawaFSPDriver(void);

		// Destructor
		~CYaskawaFSPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			DWORD	m_Data[4];
			UINT	m_Table[4];
			UINT	m_Offset[4];
			UINT	m_Recent[4];
			UINT	m_uTransactFail;
			DWORD	m_dError;
			};

		CContext *	m_pCtx;

		// Data Members

		// Hex Lookup
		LPCTXT	m_pHex;

		// Comm Frames

		#define	RXSIZE	64
		BYTE	m_bTx[RXSIZE];
		BYTE	m_bRx[64];
		BYTE	m_bPollTx[11];
		BYTE	m_bPollRx[RXSIZE];

		PBYTE	m_pTx;
		PBYTE	m_pRx;

		UINT	m_uPtr;
		BYTE	m_bCheck;
		UINT	m_uBaseCommand;
		BYTE	m_bSequence;
		BOOL	m_fSeqMismatch;
		BOOL	m_fHasChecksum;
		UINT	m_uSleepTime;
		UINT	m_uRcvCount;
		UINT	m_uWriteError;

		// Cached Item Data Storage
		// Immediate
		DWORD	m_dRFAI[2];
		DWORD	m_dS1OI[2];
		DWORD	m_dSNOI[2];
		DWORD	m_dSTXI[2];
		DWORD	m_dTQLI[2];
		DWORD	m_dWRII[2];
// -0- Exclude code if indirect Variable needed
// -1- Include code if indirect Variable needed
// -2- Include code if Sequential Mode needed
// -3- Exclude code if Sequential Mode needed
/* -2-
		// Sequential
		DWORD	m_dECES[2];
		DWORD	m_dENGS[2];
		DWORD	m_dFOSS[3];
		DWORD	m_dGOAS[2];
		DWORD	m_dGODS[2];
		DWORD	m_dHARS[2];
		DWORD	m_dHMSS[2];
		DWORD	m_dHSCS[2];
		DWORD	m_dMVAS[2];
		DWORD	m_dMVDS[2];
		DWORD	m_dRFAS[2];
		DWORD	m_dS1OS[2];
		DWORD	m_dSNOS[2];
		DWORD	m_dSTXS[2];
		DWORD	m_dWAIS[4];
		DWORD	m_dWAVS[3];
		DWORD	m_dWRIS[2];
-2- */
		// Process
		CCODE	DoRead(AREF Addr, PDWORD pData);
		CCODE	DoWrite(AREF Addr, PDWORD pData);
		// Execute Polling		
		UINT	DoPoll(PDWORD pData);
		BOOL	DoReadWrite(void);

		// Frame Building
		void	StartFrame(BYTE bMode);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dData);
		BOOL	AddRead(AREF Addr);
		BOOL	AddWrite(AREF Addr, DWORD dData);
		BOOL	AddWriteData(UINT uTable, DWORD dData);
//		BOOL	AddToWatch(AREF Addr);
//		UINT	GetWatchCount(void);
//		void	AddWatchPoll(void);
		
		// Transport Layer
		BOOL	Transact(BOOL fIsPoll);
		void	Send(void);
		BOOL	GetReply(void);

		// Reply Verification
		BOOL	CheckReply(void);
		BOOL	VerifyChecksum(void);

		// Handle Responses
		UINT	GetReadResponse(AREF Addr, PDWORD pData);
		BOOL	GetWriteResponse(void);
//		void	GetWatchResponse(AREF Addr, PDWORD pData);

		// Response Verification
		BOOL	CheckReadResponse(void);
		BOOL	ThisCommandResponse(void);
		BOOL	IsThisFunction(void);
		UINT	SetLength(UINT uCommand);
		BOOL	CheckLength(UINT uCheckVal);
		UINT	CheckErrorByte(AREF Addr);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		// Internal Access
		BOOL	ReadNoTransmit(UINT uTable, PDWORD pData);
		BOOL	WriteNoTransmit(UINT uTable, DWORD dData);
		BOOL	GetCachedData(UINT uTable, PDWORD pData);
		BOOL	PutCachedData(UINT uTable, DWORD dData);
		BYTE	GetDataSource(UINT uTable, DWORD dData);

		// Response Data
		DWORD	GetValue(UINT uPos, UINT uCount);

		// Utilities
		UINT	xtoin(BYTE b);
		BYTE	MakeRxByte(UINT uRxPos);
		BYTE	GetResponseStatus(void);
		void	SetBaseCommand(void);
		UINT	GetOpcode(UINT uTable);

	};

// Command definitions
#define	CP1	0x100
#define	CP2	0x200
#define	CP3	0x300
#define	CP4	0x400
#define	CP5	0x500

#define	CP1S	0x100 + SEQOFF
#define	CP2S	0x200 + SEQOFF
#define	CP3S	0x300 + SEQOFF
#define	CP4S	0x400 + SEQOFF
#define	CP5S	0x500 + SEQOFF

// Value, not parameter
#define	INTVAL	0
#define	VARPAR	2 // 2nd argument of Variable 

#define	MODEWATCHPOLL	0x1
#define	MODEWATCH	0x3
#define	MODECNTL	0x6
#define MODEPOLL	0x9
#define	MODEPOLLS	0xF
#define MODEIMMEDIATE	0xA
#define MODESEQUENTIAL	0xB

#define	TABLEIMMHIGH	0x3

// command retries
#define	READREPEAT	10
#define	WRITEREPEAT	READREPEAT + 1

// value to permit writing 0 to commands (5 dec 0's, 4 hex 0's)
#define	RTNZERO		409600000

// Transmit data positions
#define	TX_ADDR		 1
#define	TX_MODE		 2
#define	TX_SEQH		 3
#define	TX_SEQL		 4
#define	TX_FNCH		 5
#define	TX_FNCL		 6
#define	TX_PARNO	 9

// Read response data positions
#define	RSP_ADDR	 0
#define	RSP_STAT	 1
#define	RSP_SEQ		 2
#define	RSP_FNC		 4
#define	RSP_PAR		10
#define	RSP_TABLE	 8
#define	RSP_VERSION	 6
#define	RSP_POLL	 6
#define	RSP_POLLCHECK	 4
#define	RSP_POLLCS	10
#define	RSP_PARNO	 6
#define	RSP_WATCHCT	10
#define	RSP_WATCHDATA	12	

// Machine Status
#define	RSP_MSTATUS	 6

// Error Definition
#define	ERR_INVPAR	27
#define	ERR_NONE	 0
#define	ERR_NODATA	 1
#define	ERR_ERROR	 2
#define	ERR_SIZECK	 4
#define	POLL_LEN	13
#define	VER_LEN		13
#define	PAR_LEN		17
#define	VAR_LEN		19
#define	ARR_LEN		21

#define	NEXT_ITEM	CCODE_NO_DATA | CCODE_ERROR

// Read Response Status
#define	RSTAT_POLL	0	// Ack, no fault
#define	RSTAT_ERROR	1	// Ack, with fault
#define	RSTAT_READDATA	2	// Data Request Response
#define	RSTAT_WATCHVAR	3	// Ack, with watch var set
#define	RSTAT_UPLOAD	5	// program uploading
#define	RSTAT_OVERRUN	0x1C	// buffer overrun

// Table Numbers
//   Immediate
#define	TCMGFAI		1
#define	TCMPARI		2
#define	TCMVARI		3
#define	TCMPOL		10
#define	TCMACC		11
#define	TCMCON		12
#define	TCMGAI		13
#define	TCMGTV		14
#define	TCMJRK		15
#define	TCMRFA		16
#define	TCMRFA1		17	// Cached
#define	TCMRFA2		18	// Cached
#define	TCMRUN		19
#define	TCMS1O		20
#define	TCMS1O1		21	// Cached
#define	TCMS1O2		22	// Cached
#define	TCMSNO		23
#define	TCMSNO1		24	// Cached
#define	TCMSNO2		25	// Cached
#define	TCMSZA		26
#define	TCMSPD		27
#define	TCMSTA		28
#define	TCMSTP		29
#define	TCMSTX		30
#define	TCMSTX1		31	// Cached
#define	TCMSTX2		32	// Cached
#define	TCMSTM		33
#define	TCMTQL		34
#define	TCMTQL1		35	// Cached
#define	TCMTQL2		36	// Cached
#define	TCMWRI		37
#define	TCMWRI1		38	// Cached
#define	TCMWRI2		39	// Cached

#define	TCMERR		199
//* - 2 - Sequential
#define	TCMGFAS		4
#define	TCMPARS		5
#define	TCMVARS		6
#define	TCMACCS		100
#define	TCMCONS		101
#define	TCMDEL		102
#define	TCMECD		103
#define	TCMECE		104
#define	TCMECE1		105	// Cached
#define	TCMECE2		106	// Cached
#define	TCMENG		107
#define	TCMENG1		108	// Cached
#define	TCMENG2		109	// Cached
#define	TCMFOS		110
#define	TCMFOS1		111	// Cached
#define	TCMFOS2		112	// Cached
#define	TCMFOS3		113	// Cached
#define	TCMGAIS		114
#define	TCMGOA		115
#define	TCMGOA1		116	// Cached
#define	TCMGOA2		117	// Cached
#define	TCMGOD		118
#define	TCMGOD1		119	// Cached
#define	TCMGOD2		120	// Cached
#define	TCMGOH		121
#define	TCMHAR		122
#define	TCMHAR1		123	// Cached
#define	TCMHAR2		124	// Cached
#define	TCMHMC		125
#define	TCMHMS		126
#define	TCMHMS1		127	// Cached
#define	TCMHMS2		128	// Cached
#define	TCMHSC		129
#define	TCMHSC1		130	// Cached
#define	TCMHSC2		131	// Cached
#define	TCMJRKS		132	// Cached
#define	TCMLAT		133
#define	TCMMVA		134
#define	TCMMVA1		135	// Cached
#define	TCMMVA2		136	// Cached
#define	TCMMVD		137
#define	TCMMVD1		138	// Cached
#define	TCMMVD2		139	// Cached
#define	TCMMVH		140
#define	TCMMVR		141
#define	TCMRFAS		142
#define	TCMRFAS1	143	// Cached
#define	TCMRFAS2	144	// Cached
#define	TCMREG		145
#define	TCMRUNS		146
#define	TCMS1OS		147
#define	TCMS1OS1	148	// Cached
#define	TCMS1OS2	149	// Cached
#define	TCMSNOS		150
#define	TCMSNOS1	151	// Cached
#define	TCMSNOS2	152	// Cached
#define	TCMSZAS		153
#define	TCMSLD		154
#define	TCMSLN		155
#define	TCMSPDS		156
#define	TCMSPC		157
#define	TCMSTPS		158
#define	TCMSTXS		159
#define	TCMSTXS1	160	// Cached
#define	TCMSTXS2	161	// Cached
#define	TCMSTMS		162
#define	TCMTQE		163
#define	TCMTQA		164
#define	TCMWEX		168
#define	TCMWFS		169
#define	TCMWAI		170
#define	TCMWAI1		171	// Cached
#define	TCMWAI2		172	// Cached
#define	TCMWAI3		173	// Cached
#define	TCMWAI4		174	// Cached
#define	TCMWAS		175
#define	TCMWAV		176
#define	TCMWAV1		177	// Cached
#define	TCMWAV2		178	// Cached
#define	TCMWAV3		179	// Cached
#define	TCMWRIS		180
#define	TCMWRIS1	181	// Cached
#define	TCMWRIS2	182	// Cached
//- 2 - */
/* not implemented
#define	TCMCLE		999
#define	TCMECT		999
#define	TCMECR		999
#define	TCMECS		999
#define	TCMECP		999
#define	TCMECZ		999
#define	TCMEND		999
#define	TCMMAT		999
#define	TCMSAV		999
*/
//   Sequential
#define	CMGFAS	CMGFAI+TABLEIMMHIGH
#define	CMPARS	CMPARI+TABLEIMMHIGH
#define	CMVARS	CMVARI+TABLEIMMHIGH

#define	OPGFA	160
#define	OPPARR	 85
#define	OPVARR	 72
#define	OPVARF	 0x62 // using Flexworks method
#define	OPPARW	 80
#define	OPVARW	 81

// Named Items - Immediate Codes, Sequential adds 0x400
#define	CMPOL	0
#define	CMGTV	63
#define	CMACC	64

#define	CMCON	69
#define	CMGAI	71
#define	CMJRK	74
#define	CMRUN	78
#define	CMS1O	79
#define	CMSTA	82
#define	CMSPD	83
#define	CMSTP	84
#define	CMTQL	87
#define	CMSZA	95
#define	CMSTM	99
#define	CMSPC	100
#define	CMSLN	102
#define	CMTQA	103
#define	CMSNO	107
#define	CMWAI	109
#define	CMWAV	110
#define	CMGOA	112
#define	CMMVA	113
#define	CMSLD	115
#define	CMTQE	116
#define	CMGOH	117
#define	CMMVH	118
#define	CMMVR	119
#define	CMECE	121
#define	CMECD	122
#define	CMGOD	128
#define	CMMVD	129
#define	CMHSC	130
#define	CMHAR	131
#define	CMHMS	132
#define	CMHMC	133
#define	CMENG	136
#define	CMDEL	144
#define	CMWEX	145
#define	CMWFS	146
#define	CMWAS	148
#define	CMREG	151
#define	CMLAT	152
#define	CMSTX	153
#define	CMFOS	154
#define	CMWRI	158
#define	CMRFA	159
#define	CMERR	199

/* commands not implemented
#define	CMEND	70
#define	CMCLE	94
#define	CMSAV	96
#define	CMECT	123
#define	CMECR	124
#define	CMECS	125
#define	CMECP	126
#define	CMECZ	127
#define	CMMAT	134
*/
// Table Number Defines

// End of File
