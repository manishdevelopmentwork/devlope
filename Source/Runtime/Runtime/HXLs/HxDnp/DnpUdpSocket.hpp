
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DnpUdpSocket_HPP

#define	INCLUDE_DnpUdpSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "ChannelConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Class
//

class CDnpUdpSocket;

//////////////////////////////////////////////////////////////////////////
//
// UDP Socket
//

class CDnpUdpSocket : public ISocket
{
	public:
		// Constructor
		CDnpUdpSocket(IDnpChannelConfig * pConfig);

		// Destructor
		~CDnpUdpSocket(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISocket Methods
		HRM Listen(WORD Loc);
		HRM Listen(IPADDR const &IP, WORD Loc);
		HRM Connect(IPADDR const &IP, WORD Rem);
		HRM Connect(IPADDR const &IP, WORD Rem, WORD Loc);
		HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc);
		HRM Recv(PBYTE pData, UINT &uSize);
		HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
		HRM Send(PBYTE pData, UINT &uSize);
		HRM Recv(CBuffer * &pBuff);
		HRM Recv(CBuffer * &pBuff, UINT uTime);
		HRM Send(CBuffer   *pBuff);
		HRM GetLocal(IPADDR &IP);
		HRM GetRemote(IPADDR &IP);
		HRM GetLocal(IPADDR &IP, WORD &Port);
		HRM GetRemote(IPADDR &IP, WORD &Port);
		HRM GetPhase(UINT &Phase);
		HRM SetOption(UINT uOption, UINT uValue);
		HRM Abort(void);
		HRM Close(void);

	protected:
		// Data Members
		ULONG	    m_uRefs;
		ISocket	  * m_pSocket;
		CBuffer	  * m_pRxBuff;
		IPADDR	    m_IP;
		WORD	    m_wPort;

		// Implementation
		void RemHeader(void);
		void AddHeader(CBuffer * &pBuff);
};

// End of File

#endif
