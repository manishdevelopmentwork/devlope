
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyAnimImage_HPP
	
#define	INCLUDE_PrimRubyAnimImage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Image Primitive
//

class CPrimRubyAnimImage : public CPrimRubyGeom
{
	public:
		// Constructor
		CPrimRubyAnimImage(void);

		// Destructor
		~CPrimRubyAnimImage(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		UINT         m_Count;
		CCodedItem * m_pValue;
		CCodedItem * m_pColor;
		CCodedItem * m_pShow;
		CPrimImage * m_pImage[10];
		R2	     m_Margin;

	protected:
		// Data Members
		BOOL m_fAny;
		R2   m_ImageRect;

		// Context Record
		struct CCtx
		{
			// Data Members
			UINT m_uImage;
			BOOL m_fColor;
			BOOL m_fShow;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Implementation
		void FindImageRect(void);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
