
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceCloudGoogle_HPP

#define INCLUDE_ServiceCloudGoogle_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <wincrypt.h>

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceCloudJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Google Cloud Client Configuration
//

class CServiceCloudGoogle : public CServiceCloudJson
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CServiceCloudGoogle(void);

	// Type Access
	BOOL GetTypeData(CString Tag, CTypeDef &Type);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Persistance
	void Init(void);

	// Item Properties
	CCodedItem * m_pProject;
	CCodedItem * m_pRegion;
	CCodedItem * m_pRegistry;
	CCodedItem * m_pDevice;
	CByteArray   m_PriKey;
	CByteArray   m_PubKey;

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
	void InitOptions(void);
	BOOL CopyKey(void);
	BOOL SaveKey(void);
	BOOL LoadKey(void);
	BOOL LoadKey(PCSTR pText, UINT uText);
	BOOL MakeKeys(void);
	BOOL MakePems(BCRYPT_KEY_HANDLE hKey);
	BOOL MakePem(CByteArray &Pem, PCBYTE pData, UINT uData, PCSTR pEncoding);
};

// End of File

#endif
