
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LOGIX5_HPP
	
#define	INCLUDE_LOGIX5_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABLogix5DeviceOptions;
class CABLogix5Driver;
class CABLogix5Dialog;

//////////////////////////////////////////////////////////////////////////
//
// Address Decoders
//

#define IsTable_m(r)	((r).a.m_Table < addrNamed)

#define Index(r)	(IsTable_m(r) ? (r).a.m_Table : (r).a.m_Offset) 

//////////////////////////////////////////////////////////////////////////
//
// Address Seperators
//

enum {
	sepTableOpen	= '[',
	sepTableClose	= ']',
	};

//////////////////////////////////////////////////////////////////////////
//
// Address Types
//

enum {
	addrString	= 1,
	addrTimer	= 2,
	addrCounter	= 3,
	addrControl	= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Data
//

struct CTagDataL5 {
	
	CString  m_Name;
	DWORD    m_Addr;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Data Decoders
//

#define Addr(a)		((CAddress &) (a).m_Addr)

//////////////////////////////////////////////////////////////////////////
//
// Utility Templates
//

inline int AfxCompare(CTagDataL5 const &d1, CTagDataL5 const &d2)
{
	if( Index(Addr(d1)) > Index(Addr(d2)) )	return +1;

	if( Index(Addr(d1)) < Index(Addr(d2)) )	return -1;

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Tag Data Collections
//

typedef CArray <CTagDataL5> CTagDataL5Array;

//////////////////////////////////////////////////////////////////////////
//
// Constant
//

#define	AB_STR_MAX	80

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options
//

class CABLogix5DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABLogix5DeviceOptions(void);

		// Attributes
		CString GetDeviceName(void);

		// Operations
		BOOL    CreateTag(CError &Error, CString const &Name, CAddress Addr, BOOL fNew);
		void    DeleteTag(CAddress const &Addr, BOOL fKill);
		BOOL    RenameTag(CString const &Name, CAddress const &Addr);
		CString CreateName(void);
		void    ListTags(CTagDataL5Array &List);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Init(void);
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Push;

		// Type Definitions
		typedef CMap   <CString, DWORD> CTagsFwdMap;
		typedef CMap   <DWORD, CString> CTagsRevMap;
		typedef CArray <DWORD>          CAddrArray;

		// Tag Map
		CTagsFwdMap  m_TagsFwdMap;
		CTagsRevMap  m_TagsRevMap;
		CAddrArray   m_AddrArray;
		CStringArray m_Tags;

	protected:
		// Data Members
		CArray <UINT>   m_Errors;
		CTagDataL5Array m_List;

		// Meta Data Creation
		void AddMetaData(void);

		// Property Save Filter
		BOOL SaveProp(CString Tag) const;

		// Implementation
		void OnManage(CWnd *pWnd);
		void OnImport(CWnd *pWnd);
		void OnExport(CWnd *pWnd);
		
		CString CreateName(PCTXT pFormat);
		UINT    CreateIndex(void);

		void SaveTags(CTreeFile &Tree);
		void LoadTags(CTreeFile &Tree);

		void ValidateTags(void);

		// Address Helpers
		UINT GetOffset(CAddress const &Addr, CString const &Text);
		BOOL IsArray(CString const &Text);
		BOOL IsTable(CAddress const &Addr);
		BOOL IsArray(CAddress const &Addr);
		BOOL IsTriplet(CAddress const &Addr);
		BOOL IsString(CAddress const &Addr);
		UINT GetIndex(CAddress const &Addr);
		UINT GetOffset(CAddress const &Addr);
		void GetTriplets(CAddress const &Addr, CStringArray &List);
		BOOL AllowArray(CAddress const &Addr);
		
		CString FindDataTagType(CAddress const &Addr);
		CString GetTagName(CString const &Text);

		// Import/Export Support
		void Export(FILE *pFile);
		BOOL Import(FILE *pFile);
		BOOL ParseType(CAddress &Addr, CString Text);
		BOOL ExpandType(CString &Text, CAddress const &Addr);
		void ShowErrors(CWnd *pWnd);

		// Address Builders
		void MakeArray  (CAddress &Addr);

		// Address Overidables
		virtual BOOL MakeReal   (CAddress &Addr);
		virtual BOOL MakeLong   (CAddress &Addr);
		virtual BOOL MakeWord   (CAddress &Addr);
		virtual BOOL MakeByte   (CAddress &Addr);
		virtual BOOL MakeBool   (CAddress &Addr);
		virtual BOOL MakeTimer  (CAddress &Addr);
		virtual BOOL MakeCounter(CAddress &Addr);
		virtual BOOL MakeControl(CAddress &Addr);
		virtual BOOL MakeString (CAddress &Addr);

		// Download Support
		BOOL AddText(CInitData &Init, CString const &Text);

		// CD2 Import Support
		void CreateTags(CArray <CString> Names, CArray <UINT> Addrs);

		// Testing
		void MakeTestTags(void);

		// Friends
		friend class CABLogix5Dialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options - DF1 over EIP
//

class CABLogix5EIPMasterDeviceOptions : public CABLogix5DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABLogix5EIPMasterDeviceOptions(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Device;
		DWORD	m_Address;
		UINT    m_Routing;
		UINT	m_Slot;
		UINT	m_Time;	  		
				
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options - Serial Driver
//

class CABLogix5SerialMasterDeviceOptions : public CABLogix5DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABLogix5SerialMasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Device;
		UINT m_Dest;
		UINT m_Time1;	  		
		UINT m_Time2;
				
	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Address Overidables
		BOOL MakeBool   (CAddress &Addr);
		BOOL MakeTimer  (CAddress &Addr);
		BOOL MakeCounter(CAddress &Addr);
		BOOL MakeControl(CAddress &Addr);
		BOOL MakeString (CAddress &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver
//

class CABLogix5Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CABLogix5Driver(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver - Serial Master
//

class CABLogix5SerialMaster : public CABLogix5Driver
{
	public:
		// Constructor
		CABLogix5SerialMaster(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver - TCP Master
//

class CABLogix5EIPMasterDriver : public CABLogix5Driver
{
	public:
		// Constructor
		CABLogix5EIPMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Selection Dialog
//

class CABLogix5Dialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CABLogix5Dialog(CABLogix5Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart = FALSE);

		// Destructor
		~CABLogix5Dialog(void);

		// Initialisation
		void SetCaption(CString const &Text);
		void SetSelect(BOOL fSelect);
		                
	protected:
		// Data
		CABLogix5Driver         *m_pDriver;
		CAddress                *m_pAddr;
		CABLogix5DeviceOptions  *m_pConfig;
		BOOL		         m_fPart;
		CString                  m_Caption;
		HTREEITEM                m_hRoot;
		HTREEITEM                m_hSelect;
		DWORD			 m_dwSelect;
		CImageList               m_Images;
		BOOL                     m_fLoad;
		BOOL			 m_fSelect;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void OnTypeChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCreateTag(UINT uID);

		// Tag Name Loading
		void LoadNames(void);
		void LoadNames(CTreeView &Tree);
		void LoadRoot(CTreeView &Tree);
		void LoadTags(CTreeView &Tree);
		void LoadTag (CTreeView &Tree, CString Name, CAddress Addr);

		// Data Type Loading
		void LoadTypes(void);
		void AddReal   (CComboBox &Combo);
		void AddLong   (CComboBox &Combo);
		void AddWord   (CComboBox &Combo);
		void AddByte   (CComboBox &Combo);
		void AddBool   (CComboBox &Combo);
		void AddString (CComboBox &Combo);
		void AddTimer  (CComboBox &Combo);
		void AddCounter(CComboBox &Combo);
		void AddControl(CComboBox &Combo);

		// Tripet Text Loading
		void LoadTriplets(void);

		// Implementation
		void ShowDetails(CAddress Addr);
		void ShowAddress(CAddress Addr);
		void ShowStatus (CAddress Addr);
		
		CString GetTypeAsText (CAddress Addr);
		CString	GetOffsetText (CAddress Addr);
		CString	GetTripletText(CAddress Addr);

		BOOL DeleteTag(void);
		BOOL RenameTag(void);

		// Address Attributes
		BOOL IsTable   (CAddress const &Addr);
		BOOL IsArray   (CAddress const &Addr);
		BOOL IsTriplet (CAddress const &Addr);
		BOOL AllowArray(CAddress const &Addr);

		// Implementation
		BOOL IsDown(UINT uCode);
	};

// End of File

#endif
