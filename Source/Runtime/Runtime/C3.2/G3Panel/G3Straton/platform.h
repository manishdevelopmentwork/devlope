
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PLATFORM_H

#define	INCLUDE_PLATFORM_H

// ios.cpp

extern	T5_DWORD	iosGetMemorySizing(T5_PTR pDef);
extern	T5_RET		iosOpen(T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem);
extern	T5_RET		iosCanHotRestart(T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem);
extern	T5_RET		iosHotRestart (T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem);
extern	T5_RET		iosClose(T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem);
extern	T5_RET		iosExchange (T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem);

// memory.cpp

extern	T5_RET		memoryAlloc(T5PTR_MMB mmb, T5_DWORD dwSize, T5_PTR pConf);
extern	T5_RET		memoryLoad(T5PTR_MMB mmb, T5_PTCHAR szAppName, T5_PTR pConf);
extern	T5_RET		memoryFree(T5PTR_MMB mmb, T5_PTR pConf);
extern	T5_RET		memoryLink(T5PTR_MMB mmb, T5_PTR pConf);
extern	T5_RET		memoryUnlink(T5PTR_MMB mmb, T5_PTR pConf);

// retain.cpp

extern	T5_BOOL		retainCanLoad(T5_DWORD dwCrc, T5_PTR pArgs);
extern	T5_RET		retainLoad(T5PTR_DBRETAIN pRetain, T5_PTR pArgs);
extern	T5_BOOL		retainCanStore(T5PTR_DBRETAIN pRetain, T5_PTR pArgs);
extern	void		retainShutDown(T5PTR_DBRETAIN pRetain, T5_PTR pArgs);
extern	void		retainCycleExchange(T5PTR_DBRETAIN pRetain);

// rtc.cpp

extern	T5_BYTE		rtcGetString(T5_LONG uType, T5_PTCHAR pString);
extern	T5_DWORD	rtcGetCurDateStamp (void);
extern	T5_DWORD	rtcGetCurTimeStamp (void);
extern	void		rtcGetCurDateTimeStamp(T5_BOOL bLocal, T5_PTDWORD pdwDate, T5_PTDWORD pdwTime, T5_PTBOOL pbDST);

// serial.cpp

extern	void		serialInitialize(T5_PTSERIAL pSerial);
extern	T5_BOOL		serialIsValid (T5_PTSERIAL pSerial);
extern	T5_BOOL		serialOpen (T5_PTSERIAL pSerial, T5_PTCHAR szConfig);
extern	void		serialClose (T5_PTSERIAL pSerial);
extern	T5_WORD		serialSend (T5_PTSERIAL pSerial, T5_WORD wSize, T5_PTR pData);
extern	T5_WORD		serialReceive (T5_PTSERIAL pSerial, T5_WORD wSize, T5_PTR pData);

// socket.cpp

extern	T5_RET		socketInitialize(T5HND_CS pfCallback);
extern	void		socketTerminate (void);
extern	T5_RET		socketCreateListeningSocket (T5_WORD wPort, T5_WORD wMaxCnx, T5_PTSOCKET pSocket, T5HND_CS pfCallback);
extern	void		socketCloseSocket (T5_SOCKET sock);
extern	T5_SOCKET	socketAccept (T5_SOCKET sockListen, T5HND_CS pfCallback);
extern	T5_WORD		socketSend (T5_SOCKET sock, T5_WORD wSize, T5_PTR pData, T5_PTBOOL pbFail);
extern	T5_WORD		socketReceive (T5_SOCKET sock, T5_WORD wSize, T5_PTR pData, T5_PTBOOL pbFail);
extern	T5_RET		socketCreateConnectedSocket (T5_PTCHAR szAddr, T5_WORD wPort, T5_PTSOCKET pSocket, T5_PTBOOL pbWait, T5HND_CS pfCallback);
extern	T5_RET		socketCheckPendingConnect (T5_SOCKET sock, T5_PTBOOL pbFail);

#endif
