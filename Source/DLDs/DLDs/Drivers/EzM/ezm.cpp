#include "intern.hpp"

#include "ezm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EZ Master Serial Driver
//

// Instantiator

INSTANTIATE(CEZMasterSerialDriver);

// Constructor

CEZMasterSerialDriver::CEZMasterSerialDriver(void)
{
	m_Ident         = DRIVER_ID; 
	
	}

// Destructor

CEZMasterSerialDriver::~CEZMasterSerialDriver(void)
{
	}

// Configuration

void MCALL CEZMasterSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CEZMasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CEZMasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEZMasterSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CEZMasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bGroup  = GetByte(pData);
			m_pCtx->m_wUnit	  = GetWord(pData);
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CEZMasterSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Implementation

BOOL CEZMasterSerialDriver::Transact(void)
{
	if( Send() && RecvFrame() ) {
		
		return CheckFrame();
		}

	return FALSE; 
	}

BOOL CEZMasterSerialDriver::Send(void)
{
	WORD wCRC = CalcCRC(m_bTxBuff); 

	m_bTxBuff[m_uPtr++] = LOBYTE(wCRC);

	m_bTxBuff[m_uPtr++] = HIBYTE(wCRC);

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CEZMasterSerialDriver::RecvFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	UINT uTotal = 0;

	WORD wCRC = 0;

	m_uPtr = 0;

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( m_uPtr == 0 ) {

			if( uData != EZ_START ) {

				continue;
				}
			}

		else if( m_uPtr == 1 ) {

			uTotal = uData;
			}
		
		m_bRxBuff[m_uPtr++] = uData;

		if( uTotal > 0 ) {

			if( m_uPtr == uTotal - 1 ) {

				wCRC = CalcCRC(m_bRxBuff);
				}
			
			else if ( m_uPtr >= uTotal + 1 ) {

				WORD x = PU2(m_bRxBuff + uTotal - 1)[0];

				return ( wCRC == WORD(IntelToHost(x)) ); 
				}
			}

		Sleep(5);
		}

	return FALSE;
	}

BOOL CEZMasterSerialDriver::CheckFrame(void)
{
	for( UINT u = 2; u < 5; u++  ) {

		if( m_bRxBuff[u] != m_bTxBuff[u] ) {

			if( (m_bRxBuff[4] == 0) && (m_bRxBuff[5] == m_bTxBuff[4]) && (m_bRxBuff[6] == 0) ) {

				return TRUE;
				}

			return FALSE;
			}
		}
	
	return TRUE;
	}

WORD CEZMasterSerialDriver::CalcCRC(PBYTE pBuff)
{
	BYTE bHi = 0;

	BYTE bLo = 0;

	for( UINT u = 1; u < m_uPtr; u++ ) {

		BYTE bTemp = pBuff[u] ^ bHi;

		bTemp = (bTemp >> 4) ^ bTemp;

		bHi = (bTemp << 4) ^ bLo;

		bHi = (bTemp >> 3) ^ bHi;

		bLo = (bTemp << 5) ^ bTemp;
		}

	return MAKEWORD(bLo, bHi);
	}
