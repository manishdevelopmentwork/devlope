
#include "Intern.hpp"

#include "CommsPortNetwork.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDeviceList.hpp"

#include "CommsPortList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Network Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortNetwork, CCommsPort);

// Constructor

CCommsPortNetwork::CCommsPortNetwork(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding = bindEthernet;

	m_uImage  = IDI_ETHERNET_PROTO;
	}

// Persistance

void CCommsPortNetwork::Init(void)
{
	CCommsPort::Init();

	CCommsPortList *pList = (CCommsPortList *) GetParent();

	m_Name = pList->MakeUnique(CString(IDS_SYS_PROTOCOL));
	}

// Conversion

void CCommsPortNetwork::PostConvert(void)
{
	CCommsPort::PostConvert();

	ICommsDriver *pDriver = GetDriver();

	if( pDriver ) {

		if( !C3IsDriverAllowed(pDriver->GetID()) ) {

			ClearSettings();
			}
		}
	}

// Download Support

BOOL CCommsPortNetwork::MakeInitData(CInitData &Init)
{
	Init.AddByte(2);

	CCommsPort::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CCommsPortNetwork::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_SetName((IDS_NETWORK_PORT));
	}

// End of File
