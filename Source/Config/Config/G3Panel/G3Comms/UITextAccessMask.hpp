
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextAccessMask_HPP

#define INCLUDE_UITextAccessMask_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Access Mask
//

class CUITextAccessMask : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextAccessMask(void);

	protected:
		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL    OnExpand(CWnd &Wnd);

		// Implementaiton
		CString Format(UINT uData);
		UINT    Parse(CString Text);
	};

// End of File

#endif
