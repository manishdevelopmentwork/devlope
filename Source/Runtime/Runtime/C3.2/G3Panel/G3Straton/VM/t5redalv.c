/*****************************************************************************
T5RedAlv.c : Redundancy - alive signal
(c) COPALP 2006
*****************************************************************************/

#include "t5vm.h"
#include "t5redapi.h"

/****************************************************************************/

T5_BOOL T5RedAlv_Open (T5_PTCHAR szSettings)
{
    return TRUE;
}

void T5RedAlv_Close (void)
{
}

T5_RET T5RedAlv_ReadAliveSignal (void)
{
    return T5REDALV_UNKNOWN;
}

void T5RedAlv_SendAliveSignal (void)
{
}

void T5RedAlv_StopAliveSignal (void)
{
}

/* eof **********************************************************************/
