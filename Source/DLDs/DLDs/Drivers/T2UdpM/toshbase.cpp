
#include "intern.hpp"

#include "toshbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Toshiba Base Master Driver
//

// Constructor

CToshibaBaseMasterDriver::CToshibaBaseMasterDriver(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	m_pCtx = NULL;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;

	}

// Destructor

CToshibaBaseMasterDriver::~CToshibaBaseMasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CToshibaBaseMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 0x0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1); 
	}

CCODE MCALL CToshibaBaseMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
       	if( Start(uCount, Addr.a.m_Type) ) {

		PutRead(Addr, uCount);

		if( Transact() ) {

			GetData(pData, uCount, Addr.a.m_Type, Addr.a.m_Table, Addr.a.m_Extra);

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CToshibaBaseMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Start(uCount, Addr.a.m_Type) ) {

		if( PutWrite(Addr, pData, uCount) )  {

			if( Transact() ) {

				return uCount;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}
 
// Implementation

BOOL CToshibaBaseMasterDriver::PutRead(AREF Addr, UINT uCount)
{
	AddByte('D');

	AddByte('R');

	UINT uTable = Addr.a.m_Table;

	AddCommand(uTable, FALSE );

	AddOffset(uTable, Addr.a.m_Offset);
	
	AddCount(uCount, Addr.a.m_Type);

	AddCheck();

	AddByte(')');

	AddByte(CR);

	return TRUE;
		
	}

BOOL CToshibaBaseMasterDriver::PutWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	AddByte('D');
	
	AddByte('W');

	UINT uTable = Addr.a.m_Table;

	if( IsStatus(uTable, Addr.a.m_Extra) ) {

		return TRUE;
		}

	UINT uOffset = Addr.a.m_Offset;

	if( AddCommand(uTable, TRUE) ) {

		AddOffset(uTable, uOffset);

		AddCount(uCount, Addr.a.m_Type);

		AddByte(',');

		PutData(pData, uCount, Addr.a.m_Type, IsBit(uTable), uTable, Addr.a.m_Extra);

		AddCheck();

		AddByte(')');

		AddByte(CR);

		return TRUE;

		}

       	return FALSE;

	}

void CToshibaBaseMasterDriver::PutData(PDWORD pData, UINT uCount, UINT uType, BOOL fBit, UINT uTable, UINT uExtra)
{
	switch(uType) {

		case addrWordAsWord:
		
			PutWordWrite(pData, uCount, fBit, uTable, uExtra);
			return;

		case addrWordAsLong:
		case addrWordAsReal:
		case addrLongAsLong:

			PutLongWrite(pData, uCount, fBit, uType);
			return;

		case addrRealAsReal:

			PutRealWrite(pData, uCount, fBit, uType);
			return;
		}
	}

void CToshibaBaseMasterDriver::PutWordWrite(PDWORD pData, UINT uCount, BOOL fBit, UINT uTable, UINT uExtra)
{
	UINT u = 0;
	
	for( u = 0; u < uCount; u++ ) {

		AddHex(GetWordWrite(LOWORD(pData[u]), fBit), 0x1000);

		if( u < uCount - 1 ) {

			AddByte(',');
			}
		}

	if( HasDeviceData(uTable, uExtra) ) {

		for( u = 0; u < uCount; u++ ) {

			AddDeviceData(uExtra);

			}
		}

	}

void CToshibaBaseMasterDriver::PutLongWrite(PDWORD pData, UINT uCount, BOOL fBit, UINT uType)
{
	UINT uInc = uType == addrLongAsLong ? 2 : 1;

	for( UINT u = 0; u < uCount; u += uInc ) {

		DWORD x = pData[u];

		AddHex(GetWordWrite(HIWORD(x), fBit), 0x1000);

		AddByte(',');

		AddHex(GetWordWrite(LOWORD(x), fBit), 0x1000);

		if( u < uCount - 1 ) {

			AddByte(',');
			}
		}
	}

void CToshibaBaseMasterDriver::PutRealWrite(PDWORD pData, UINT uCount, BOOL fBit, UINT uType)
{
	UINT uInc = uType == addrRealAsReal ? 2 : 1;

	for( UINT u = 0; u < uCount; u += uInc ) {

		DWORD x = pData[u];

		AddHex(GetWordWrite(LOWORD(x), fBit), 0x1000);

		AddByte(',');

		AddHex(GetWordWrite(HIWORD(x), fBit), 0x1000);

		if( u < uCount - 1 ) {

			AddByte(',');
			}
		}
	}

void CToshibaBaseMasterDriver::GetData(PDWORD pData, UINT uCount, UINT uType, UINT uTable, UINT uExtra)
{
	switch(uType) {

		case addrWordAsWord:
		case addrBitAsBit:

			GetWordRead(pData, uCount, uTable, uExtra); 
			return;

		case addrWordAsLong:
		case addrLongAsLong:
		case addrWordAsReal:

			GetLongRead(pData, uCount, uType);
			return;

		case addrRealAsReal:

			GetRealRead(pData, uCount, uType);
			return;
		}
	}

void CToshibaBaseMasterDriver::GetWordRead(PDWORD pData, UINT uCount, UINT uTable, UINT uExtra)
{
	BOOL fDevice = IsStatus(uTable, uExtra);
	
	UINT s = fDevice ? 5 + uCount * 4 : 5;

	UINT m = fDevice ? 2 : 4;

	UINT c = uCount;

	for( UINT u = 0; u < c; u++ ) {

		PCTXT pText = PTXT(m_bRxBuff + s + (u * m));

		WORD x = xtoin(pText, m);

		pData[u] = LONG(SHORT(x));

		}
	}

void CToshibaBaseMasterDriver::GetLongRead(PDWORD pData, UINT uCount, UINT uType)
{
	UINT uInc = uType == addrLongAsLong ? 4 : 8;

	for( UINT u = 0; u < uCount; u++ ) {

		PCTXT pText = PTXT(m_bRxBuff + 5 + (u * uInc));

		WORD h = xtoin(pText, 4);

		WORD l = xtoin(pText + 4, 4);

		pData[u] = MAKELONG(l,h);
		}
	}

void CToshibaBaseMasterDriver::GetRealRead(PDWORD pData, UINT uCount, UINT uType)
{
	UINT uInc = uType == addrRealAsReal ? 4 : 8;

	for( UINT u = 0; u < uCount; u++ ) {

		PCTXT pText = PTXT(m_bRxBuff + 5 + (u * uInc));

		WORD l = xtoin(pText, 4);

		WORD h = xtoin(pText + 4, 4);

		pData[u] = MAKELONG(l,h);
		}
	}

BOOL CToshibaBaseMasterDriver::IsBit(UINT uTable)
{
	switch( uTable ) {

		case 4:
		case 5:
		case 6:

			return TRUE;
		}

	return FALSE;
	}

UINT CToshibaBaseMasterDriver::GetWordWrite(UINT uWord, BOOL fBit)
{
	if( fBit ) {

		if( uWord ) {

			return 1;
			}

		return 0;
		}

	return uWord;
	}

// Frame Building

BOOL CToshibaBaseMasterDriver::Start(UINT &uCount, UINT uType)
{
	m_uPtr = 0;

	m_uCheck = 0;

	AddByte('(');

	AddByte('A'); 

	UINT uMax = uType == addrWordAsWord ? 32 : 16;
	
	MakeMin(uCount, uMax);

	AddDrop();

 	return TRUE;

	}

void CToshibaBaseMasterDriver::AddDrop(void) 
{
	AddByte(m_pHex[m_pCtx->m_bDrop / 10]);

	AddByte(m_pHex[m_pCtx->m_bDrop % 10]);
	}

void CToshibaBaseMasterDriver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uPtr++] = bByte;

	m_uCheck += bByte;
	
	}

BOOL CToshibaBaseMasterDriver::AddCommand(UINT uTable, BOOL fWrite) 
{
	switch( uTable ) {

		case 1:
			if( m_pCtx->m_bSeries != SERIESEX ) {

				AddByte('D');

				}
			
			break;

		case 2:
		       	if( fWrite ) {

				return FALSE;
				}

			AddByte('X');

			AddByte('W');

			break;
		
		case 3:
			AddByte('Y');

			AddByte('W');

			break;

		case 4:	
		       	AddByte('C');

			break;
	       
		case 5:
			AddByte('R');

			break;

		case 6:
			AddByte('Z');

			break;

		case 7:
			AddByte('R');

			AddByte('W');

			break;

		case 8:
			AddByte('Z');

			AddByte('W');

			break;

		case 9:
			AddByte('T');

			break;

		case 10:
			AddByte('C');

			break;

		case 11:
			AddByte('F');
			
			break;

		default:

			return FALSE;

		}

	return TRUE;

	}

void CToshibaBaseMasterDriver::AddOffset(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case 2:
		case 3:
			if( m_pCtx->m_bSeries != SERIESS ) AddHex(uOffset, 0x10);

			else AddDec(uOffset, 100);

			break;

		default:
			AddDec(uOffset, GetDigits(uOffset));

			break;
		}

	AddByte(',');
	
	}

void CToshibaBaseMasterDriver::AddCount(UINT uCount, UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrWordAsReal:
			uCount *= 2;
			break;

// Since addresses aren't aligned, F200 - F204 count=5, but want 3*2, ie 200(1),202(3),204(5)
		case addrLongAsLong:
		case addrRealAsReal:
			uCount = uCount + 1;
			break;
		}
	
	AddDec(uCount, GetDigits(uCount));

	}

void CToshibaBaseMasterDriver::AddDec(UINT n, UINT f)
{
	while( f )  {

		AddByte('0' + ( n / f ) % 10);

		f /= 10;
	
		}
	}

void CToshibaBaseMasterDriver::AddHex(UINT n, UINT f)
{
   	while( f ) {

		AddByte(m_pHex[ (n / f) % 16 ]);

		f /= 16;
		
		}

	}

void CToshibaBaseMasterDriver::AddCheck(void)
{
	if( m_pCtx->m_fCheck ) {

		AddByte('&');

		BYTE bCheck = m_uCheck;

		AddByte(m_pHex[bCheck / 16]);

		AddByte(m_pHex[bCheck % 16]);
		}
	}

void CToshibaBaseMasterDriver::AddDeviceData(UINT uExtra)
{
	AddByte(',');
	
	AddByte('0');

	AddByte(!(uExtra & 0x01) + 0x30);

	}

WORD CToshibaBaseMasterDriver::xtoin(PCTXT pText, UINT uCount)
{
	WORD t = 0;
	
	while( uCount-- ) {
	
		char c = *(pText++);
		
		if( c >= '0' && c <= '9' )
			t = 16 * t + c - '0';

		else if( c >= 'A' && c <= 'F' )
			t = 16 * t + c - 'A' + 10;

		else if( c >= 'a' && c <= 'f' )
			t = 16 * t + c - 'a' + 10;
		}
		
	return t;
	}

// Transport Layer

BOOL CToshibaBaseMasterDriver::Transact(void)
{
	return FALSE;
	}

// Helpers

BOOL CToshibaBaseMasterDriver::HasDeviceData(UINT uTable, UINT uExtra) 
{
       if( m_pCtx->m_bSeries != SERIESEX ) {

		if( uTable == 9 || uTable == 10 ) {

			if( uExtra != 2 ) {

				return TRUE;
				}
			}
		}

	return FALSE;

	}

BOOL CToshibaBaseMasterDriver::IsStatus(UINT uTable, UINT uExtra) 
{
	if( m_pCtx->m_bSeries != SERIESEX ) {

		if( uTable == 9 || uTable == 10 ) {

			if( uExtra == 2 ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

UINT CToshibaBaseMasterDriver::GetDigits(UINT uValue)
{
	UINT uDigits = 1;

	while( uValue > 9 ) {

		uValue /= 10;

		uDigits *= 10;
		}

	return uDigits;
	}

// End of File
