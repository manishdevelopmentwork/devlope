
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_S71K_HPP
	
#define	INCLUDE_S71K_HPP

/////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Data Block Constants
//

#define S7_1K_DEBUG	0

#define S7_DELIM	'\"'
#define S7_DB_DELIM	0x09

enum Boundary {

	boundMin = 0x0A,
	boundMax = addrNamed - 1,
	boundExt = 1024,
	};

enum Block {

	blockMin = 1,
	blockMax = 59999,
	};

enum DB {

	dbName = 0,
	dbType = 1,
	dbNum  = 2,
	dbVal  = 3,
	};

enum Tag {

	tagName = 0,
	tagDesc = 1,
	tagType = 2,
	tagTag  = 3,
	};

enum Space {

	spaceQB	= 0x1,
	spaceIB = 0x2,
	spaceMB = 0x3,
	spaceDB = 0xA,
	};

/////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Data Block Definition
//

struct CDataBlockRef {

	DWORD	m_Ref;
	WORD	m_Block;
	WORD	m_Offset;
	BYTE	m_Type;
	CString	m_Text;
	WORD	m_Extent;
	};

typedef CArray <CDataBlockRef> CDbRefArray;

// Sort Help

inline int AfxCompare(CDataBlockRef const &s1, CDataBlockRef const &s2)
{
	if( s1.m_Block  > s2.m_Block  )	return +1;

	if( s1.m_Block  < s2.m_Block  )	return -1;

	if( s1.m_Offset > s2.m_Offset ) return +1;

	if( s1.m_Offset < s2.m_Offset ) return -1;

	if( s1.m_Type   > s2.m_Type   ) return +1;

	if( s1.m_Type   < s2.m_Type   ) return -1;

	return 0;
	}

/////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Forward Declarations
//

class CSpaceS71K;

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Driver Options
//

class CS71KDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS71KDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Device Options
//

class CS71KDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS71KDeviceOptions(void);

		// Destructor
		~CS71KDeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Addr;
		UINT m_Port;
		UINT m_Type;
		UINT m_Rack;
		UINT m_Slot;
		UINT m_Addr2;
		UINT m_Type2;
		UINT m_Rack2;
		UINT m_Slot2;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_TSAP;
		UINT m_STsap;
		UINT m_CTsap;
		UINT m_STsap2;
		UINT m_CTsap2;
		
		// Data Block Access
		BOOL IsDataBlock(UINT uTable);
		BOOL IsDataBlock(CString Text);
		BOOL ExpandDataBlock(DWORD dwRef, WORD &wBlock, WORD &wOffset);

		// Data Block Operations
		DWORD AppendDataBlock(CDataBlockRef &Block);
		void  UpdateDataBlocks(CAddress const &Address, INT nSize);
		void  RebuildDataBlocks(void);
				
		// Device Access
		CString GetDeviceName(void);

		// Import Operations
		BOOL	ImportDB(FILE *pFile, CString File, IMakeTags *pTags, CString &Error);
		BOOL	ImportTags(FILE *pFile, CString File, IMakeTags *pTags, CString &Error);
		CString FindFilename(CString Path);
				
	protected:

		// Meta Data Creation
		void AddMetaData(void);

		// Data Members
		CDbRefArray *	m_pBlocks;
		DWORD		m_LastRef;

		// Data Block Implementation
		BOOL  DataBlockExist(CDataBlockRef &Exist);
		BOOL  UpdateDataBlock(CDataBlockRef &Add);
		DWORD FindNextDataBlockRef(void);
		BOOL  IsEndOfBlock(WORD wOffset, CDataBlockRef &Block);
		BOOL  IsThisDataBlock(CDataBlockRef * pThis, CDataBlockRef * pBlock);
		BOOL  IsUpdateCandidate(CDataBlockRef * pBlock, CDataBlockRef * pAdd);
		BOOL  IsUpdateCandidate(CDataBlockRef * pBlock, DWORD dwRef);
		BOOL  IsDataBlockMatch(CDataBlockRef * pBlock, CDataBlockRef * pAdd);
		BOOL  IsDataBlockMatch(CDataBlockRef * pBlock, DWORD dwRef);
		BOOL  IsDataBlockOverlap(CDataBlockRef * pBlock, CDataBlockRef * pAdd, WORD &wOffset, WORD &wExtent);
		BOOL  GetReference(CDataBlockRef &Block, CDataBlockRef &Add);
		WORD  GetDataBlockExtent(CDataBlockRef * pBlock);
		BYTE  GetSizeOfEntity(UINT uType);
		DWORD GetDataBlockMaxRef(CDataBlockRef * pBlock);
		void  PurgeDataBlocks(void);
		void  SortDataBlocks(void);
		void  WalkDataBlocks(void);
		void  CompressDataBlocks(void);
		BOOL  IsDataBlockSpanning(CDataBlockRef &Block, CDataBlockRef &Next);
		void  PrintDataBlocks(void);

		// Tag Creation
		void MakeDBTag(UINT uDB, CStringArray Array, IMakeTags * pTags, CString &Error, UINT uExtent);
		void MakeTag(CString TExt, IMakeTags * pTag, CString &Error);
		
		// Helpers
		CString	GetErrorText(CString Name, CString Tag);
		void	Validate(CString &String);
		void	Print(CString Text);
		CString	AdjustType(CString Type);
		BOOL	IsReal(CString Type);
		BOOL	IsFlag(CString Type);
		
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Device Options UI Page
//

class CS71KDeviceOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CS71KDeviceOptionsUIPage(CS71KDeviceOptions * pOption);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CS71KDeviceOptions * m_pOption;
	}; 


//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Driver
//

class CS71KDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CS71KDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Driver Data
		UINT GetFlags(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Notifications
		void NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig);
		void NotifyInit(CItem * pConfig);
		
		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL CheckAlignment(CSpace *pSpace);

		// Tag Import
		BOOL MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);
					
       protected:

		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simatic S7 via MPI Address Selection
//

class CS71KDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CS71KDialog(CS71KDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		
		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Space Wrapper Class
//

class CSpaceS71K : public CSpace
{
	public:
		// Constructors
		CSpaceS71K(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);

		// Data Access
		WORD GetBlock(CAddress Addr);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);

	protected:
		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 Import Error Dialog
//

class CS7ImportErrorDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CS7ImportErrorDialog(CStringArray List);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCopy(UINT uID);
					
	protected:

		CStringArray m_Errors;
		
		// Implementation

		void LoadErrorList(void);
		
	};


// End of File

#endif
