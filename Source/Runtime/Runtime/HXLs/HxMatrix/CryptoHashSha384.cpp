
#include "Intern.hpp"

#include "CryptoHashSha384.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SHA384 Cryptographic Hash
//

// Instantiator

static IUnknown * Create_CryptoHashSha384(PCTXT pName)
{
	return New CCryptoHashSha384;
	}

// Registration

global void Register_CryptoHashSha384(void)
{
	piob->RegisterInstantiator("crypto.hash-sha384", Create_CryptoHashSha384);
	}

// Constructor

CCryptoHashSha384::CCryptoHashSha384(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;

	psSha384PreInit(&m_Ctx);
	}

// ICryptoHash

CString CCryptoHashSha384::GetName(void)
{
	return "sha384";
	}

void CCryptoHashSha384::GetHashOid(CByteArray &oid)
{
	BYTE b[] = { 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x02 };

	oid.Empty();

	oid.Append(b, sizeof(b));
	}

void CCryptoHashSha384::Initialize(void)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	psSha384Init(&m_Ctx);

	m_uState = stateActive;
	}

void CCryptoHashSha384::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	psSha384Update(&m_Ctx, pData, uData);
	}

void CCryptoHashSha384::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	psSha384Final(&m_Ctx, m_pHash);

	m_uState = stateDone;
	}

// End of File
