
#include "Intern.hpp"

#include "ArpCache.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ARP Cache
//

// Constants

static UINT const arpTimeout = 30;

// Constructor

CArpCache::CArpCache(void)
{
	m_uLast = NOTHING;

	for( UINT n = 0; n < elements(m_List); n++ ) {

		CEntry &Entry = m_List[n];

		Entry.m_fUsed = FALSE;
		}

	m_pLock = Create_Rutex();
	}

// Constructor

CArpCache::~CArpCache(void)
{
	m_pLock->Release();
	}

// Diagnostics

UINT CArpCache::RunDiagCmd(IDiagOutput *pOut, UINT uCmd)
{
	switch( uCmd ) {

		case 1:
			return DiagFlush(pOut);

		case 2:
			return DiagShow(pOut);
		}

	return 0;
	}

// Attributes

BOOL CArpCache::Find(IPREF Ip, CMacAddr &Mac)
{
	CAutoLock Lock(m_pLock);

	if( m_uLast < NOTHING ) {

		CEntry &Entry = m_List[m_uLast];

		if( Entry.m_fUsed ) {

			if( Entry.m_Ip == Ip ) {

				Mac = Entry.m_Mac;

				return TRUE;
				}
			}
		}

	for( UINT n = 0; n < elements(m_List); n++ ) {

		CEntry &Entry = m_List[n];

		if( Entry.m_fUsed ) {

			if( Entry.m_Ip == Ip ) {

				Mac     = Entry.m_Mac;

				m_uLast = n;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Operations

void CArpCache::Add(IPREF Ip, MACREF Mac)
{
	UINT uFreeSlot = NOTHING;

	UINT uTimeSlot = NOTHING;

	UINT uTimeGone = 0;

	UINT uTime     = GetTickCount();

	CAutoLock Lock(m_pLock);

	UINT n;

	for( n = 0; n < elements(m_List); n++ ) {

		CEntry &Entry = m_List[n];

		if( Entry.m_fUsed ) {
			
			if( Entry.m_Ip == Ip ) {

				Entry.m_Mac  = Mac;

				Entry.m_Time = uTime;

				return;
				}
			else {
				UINT uGone = uTime - Entry.m_Time;

				if( ++uGone >= uTimeGone ) {

					uTimeGone = uGone;

					uTimeSlot = n;
					}
				}
			}
		else {
			if( uFreeSlot == NOTHING ) {

				uFreeSlot = n;
				}
			}
		}

	if( (n = uFreeSlot) == NOTHING ) {

		if( Mac.IsEmpty() ) {

			return;
			}

		n = uTimeSlot;
		}

	CEntry &Entry = m_List[n];

	Entry.m_fUsed = TRUE;

	Entry.m_Ip    = Ip;

	Entry.m_Mac   = Mac;

	Entry.m_Time  = uTime;
	}

void CArpCache::Validate(MACREF Mac)
{
	CAutoLock Lock(m_pLock);

	for( UINT n = 0; n < elements(m_List); n++ ) {

		CEntry &Entry = m_List[n];

		if( Entry.m_fUsed ) {

			if( Entry.m_Mac == Mac ) {

				Entry.m_Time = GetTickCount();

				break;
				}
			}
		}
	}

void CArpCache::Poll(IPREF Ip)
{
	CAutoLock Lock(m_pLock);

	for( UINT n = 0; n < elements(m_List); n++ ) {

		CEntry &Entry = m_List[n];

		if( Entry.m_fUsed ) {
			
			if( !Entry.m_Mac.IsEmpty() ) {
				
				if( Entry.m_Ip != Ip ) {

					UINT uTime = GetTickCount();

					UINT uGone = uTime - Entry.m_Time;

					if( uGone >= arpTimeout * ToTicks(1000) ) {

						Entry.m_fUsed = FALSE;
						}
					}
				}
			}
		}
	}

// Diagnostics

UINT CArpCache::DiagFlush(IDiagOutput *pOut)
{
	CAutoLock Lock(m_pLock);

	for( UINT n = 0; n < elements(m_List); n++ ) {

		CEntry &Entry = m_List[n];

		Entry.m_fUsed = FALSE;
		}

	Lock.Free();

	pOut->Print("arp cache flushed\n");

	return 0;
	}

UINT CArpCache::DiagShow(IDiagOutput *pOut)
{
	pOut->AddTable(3);

	pOut->SetColumn(0, "Ip",  "%s"       );
	pOut->SetColumn(1, "Mac", "%s"       );
	pOut->SetColumn(2, "Age", "%4u.%2.2u");

	pOut->AddHead();

	pOut->AddRule('-');

	UINT uTime = GetTickCount();

	for( UINT n = 0; n < elements(m_List); n++ ) {

		CEntry &Entry = m_List[n];

		if( Entry.m_fUsed ) {

			pOut->AddRow();

			pOut->SetData(0, PCTXT(Entry.m_Ip.GetAsText()));

			pOut->SetData(1, PCTXT(Entry.m_Mac.GetAsText()));

			UINT uAge = Entry.m_Mac.IsEmpty() ? 0 : ToTime(uTime - Entry.m_Time);

			pOut->SetData(2, uAge / 1000, uAge % 1000 / 10);

			pOut->EndRow();
			}
		}

	pOut->AddRule('-');

	pOut->EndTable();

	return 0;
	}

// End of File
