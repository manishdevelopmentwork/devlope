
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceItem_HPP

#define INCLUDE_ServiceItem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Service Base Class
//

class CServiceItem : public CCodedHost, public IService
{
	public:
		// Constructor
		CServiceItem(void);

		// IService
		void LoadService(PCBYTE &pData);
		UINT GetID      (void);
		void GetTaskList(CTaskList &List);
	};

// End of File

#endif
