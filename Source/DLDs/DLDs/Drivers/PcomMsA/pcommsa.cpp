#include "intern.hpp"

#include "pcommsa.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Master Serial Driver
//
//

// Instantiator

INSTANTIATE(CPcomMsADriver);

// Constructor

CPcomMsADriver::CPcomMsADriver(void)
{
	m_Ident         = DRIVER_ID; 
	}

// Destructor

CPcomMsADriver::~CPcomMsADriver(void)
{
	}

// Configuration

void MCALL CPcomMsADriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CPcomMsADriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CPcomMsADriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CPcomMsADriver::Open(void)
{
	}

// Device

CCODE MCALL CPcomMsADriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bUnit  = GetByte(pData);
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CPcomMsADriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Implementation

BOOL CPcomMsADriver::Transact(void)
{
	if( Send() && RecvFrame() && CheckFrame() ) {

		return TRUE;
		}

	return FALSE; 
	}

BOOL CPcomMsADriver::Send(void)
{
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CPcomMsADriver::RecvFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uPtr = 0;

	UINT uSTX = NOTHING;

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uSTX == 1 ) {

			if( uData == CR ) {

				return TRUE;
				}

			m_bRx[m_uPtr] = uData;

			m_uPtr ++;
			}

		else if( uData == '/' ) {

			uSTX = 0;

			m_bRx[m_uPtr] = uData;

			m_uPtr ++;
			}

		else if( uData == 'A' && uSTX == 0 ) {

			uSTX++;

			m_bRx[m_uPtr] = uData;

			m_uPtr ++;
			}
		
		Sleep(10);
		}

	return FALSE;
	}

BOOL CPcomMsADriver::CheckFrame(void)
{
	m_uCheck = 0;

	UINT u = 0;
	
	for( u = 2; u < m_uPtr - 2; u++ ) {

		m_uCheck += m_bRx[u];
		}

	BYTE bCheck = ByteFromAscii(m_bRx[u]) << 4;

	u++;

	bCheck     |= ByteFromAscii(m_bRx[u]);
      
	if (bCheck == (m_uCheck % 256)) {

		if( m_bRx[2] == m_pHex[m_pCtx->m_bUnit / 0x10] &&
		    m_bRx[3] == m_pHex[m_pCtx->m_bUnit % 0x10] ) {

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CPcomMsADriver::GetCount(UINT uType)
{
	switch( uType ) {

		case addrLongAsLong:
		case addrRealAsReal:

			return 12;

		case addrBitAsBit:

			return 32;
		}

	return 24;
	}

// End of file

