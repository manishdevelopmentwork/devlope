
#include "Intern.hpp"

#include "UsbHostLoopback.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Loopback Driver
//

IUsbHostFuncDriver * Create_LoopbackDriver(void)
{
	CUsbHostLoopback *p = New CUsbHostLoopback;

	return (IUsbHostBulkDriver *) p;
	}

// Constructor

CUsbHostLoopback::CUsbHostLoopback(void)
{
	m_pName = "Host Loopback Driver";

	m_Debug = debugWarn;

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostLoopback::~CUsbHostLoopback(void)
{
	Trace(debugInfo, "Driver Destroyed");
	}

// IHostFuncDriver

UINT CUsbHostLoopback::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostLoopback::GetSubClass(void)
{
	return devVendor;
	}

UINT CUsbHostLoopback::GetProtocol(void)
{
	return devVendor;
	}

// End of File
