#ifndef	INCLUDE_Dnp3IpS_HPP

#define	INCLUDE_Dnp3IpS_HPP

#include "dnp3s.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 TCP Slave Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

class CDnp3IpSlave : public CDnp3Slave
{
public:
	// Constructor
	CDnp3IpSlave(void);

	// Destructor
	~CDnp3IpSlave(void);

	// Device
	DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
	DEFMETH(CCODE) DeviceClose(BOOL fPersist);

protected:

	struct CIpCtx : CDnp3Slave::CContext {

			DWORD	 m_IP1;
			DWORD	 m_IP2;
			WORD	 m_TcpPort;
			WORD     m_UdpPort;
			UINT	 m_uTime1;

			IDnpChannel * m_pChannel;

			IDnpChannelConfig * m_pConfig;
		};

		// Data Members
		CIpCtx * m_pIpCtx;

		// Channels
		virtual	void OpenChannel(void);
		virtual BOOL CloseChannel(void);

};

#endif

// End of File
