
#include "Intern.hpp"

#include "UIIntlString.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "LangManager.hpp"
#include "TransEditCtrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- International String
//

// Dynamic Class

AfxImplementDynamicClass(CUIIntlString, CUIControl);

// Constructor

CUIIntlString::CUIIntlString(void)
{
	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pPickLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pDataCtrl   = New CTransEditCtrl(this);

	m_pPickCtrl   = NewHotLink();

	m_cfCode      = RegisterClipboardFormat(L"C3.1 Code Fragment");
	}

// Core Overridables

void CUIIntlString::OnBind(void)
{
	CUIElement::OnBind();

	m_pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	m_pLang   = m_pSystem->m_pLang;

	if( m_pText->HasFlag(textDefault) ) {

		m_pDataCtrl->SetDefault(m_pText->GetDefault());
		}

	if( !m_fTable ) {

		m_Label = GetLabel() + L":";
		}

	m_Verb = CString(IDS_TRANSLATE);
	}

void CUIIntlString::OnLayout(CLayFormation *pForm)
{
	UINT uSize    = m_pText->GetWidth();

	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayItemText(uSize,   1, 2);

	m_pPickLayout = New CLayItemText(m_Verb, CSize(6, 4), CSize(1, 1));

	m_pMainLayout = New CLayFormRow;

	m_pMainLayout->AddItem(New CLayFormPad(m_pDataLayout, horzLeft | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pPickLayout, horzNone | vertCenter));

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIIntlString::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pDataCtrl->Create( ES_AUTOHSCROLL | WS_BORDER | WS_TABSTOP,
			     GetDataCtrlRect(),
			     Wnd,
			     uID++
			     );

	m_pPickCtrl->Create( m_Verb,
			     BS_PUSHBUTTON | WS_TABSTOP,
			     m_pPickLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	m_pPickCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetSlot(m_pLang, 0);

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl, EN_KILLFOCUS);

	AddControl(m_pPickCtrl);
	}

void CUIIntlString::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(GetDataCtrlRect(), TRUE);

	m_pPickCtrl->MoveWindow(m_pPickLayout->GetRect(), TRUE);
	}

// Data Overridables

void CUIIntlString::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	UINT    uPos = Data.Find(LANG_SEP);

	m_pDataCtrl->SetWindowText(Data.Left(uPos));

	m_pDataCtrl->SetModify(FALSE);

	m_pDataCtrl->SetSel(CRange(TRUE));
	}

UINT CUIIntlString::OnSave(BOOL fUI)
{
	if( m_pDataCtrl->GetModify() ) {

		CString Text = m_pDataCtrl->GetWindowText();

		return SaveCurrent(fUI, Text);
		}

	return saveSame;
	}

BOOL CUIIntlString::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	if( CanAcceptText(pData, m_cfCode) ) {

		dwEffect = DROPEFFECT_LINK;

		return TRUE;
		}

	if( CanAcceptText(pData, CF_UNICODETEXT) ) {

		dwEffect = DROPEFFECT_COPY;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIIntlString::OnAcceptData(IDataObject *pData)
{
	m_pDataCtrl->SetFocus();

	if( AcceptText(pData, m_cfCode, L"=") ) {

		return TRUE;
		}

	if( AcceptText(pData, CF_UNICODETEXT) ) {

		return TRUE;
		}

	return FALSE;
	}

// Notification Handlers

BOOL CUIIntlString::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pPickCtrl->GetID() ) {

		if( m_pText->ExpandData(*m_pPickCtrl) ) {

			LoadUI();

			return TRUE;
			}

		return FALSE;
		}

	return CUIControl::OnNotify(uID, uCode);
	}

// Implementation

CRect CUIIntlString::GetDataCtrlRect(void)
{
	CRect Rect;

	Rect = m_pDataLayout->GetRect() + 2;

	Rect.left += 1;

	return Rect;
	}

UINT CUIIntlString::SaveCurrent(BOOL fUI, CString Data)
{
	CError Error = CError(fUI);

	UINT   uCode = saveSame;

	if( Data.GetLength() ) {

		if( Data[0] == '=' ) {

			CString Last = m_pText->GetAsText();

			if( Data.CompareC(Last) ) {

				uCode = m_pText->SetAsText(Error, Data);
				}
			}
		else {
			CString Last = m_pText->GetAsText();

			if( Last[0] == '=' ) {

				uCode = m_pText->SetAsText(Error, Data);
				}
			else {
				Last  = Last.Mid(Last.Find(LANG_SEP));

				uCode = m_pText->SetAsText(Error, Data + Last);
				}
			}
		}
	else {
		if( m_pText->GetAsText()[0] ) {

			uCode = m_pText->SetAsText(Error, Data);
			}
		}

	if( Error.IsOkay() ) {

		LoadUI();

		return uCode;
		}

	if( Error.AllowUI() ) {

		Error.Show(CWnd::GetActiveWindow());
		}

	return saveError;
	}

// End of File
