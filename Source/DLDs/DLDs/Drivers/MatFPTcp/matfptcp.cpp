
#include "intern.hpp"

#include "matfptcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series TCP/IP Master Driver
//

// Instantiator

INSTANTIATE(CMatsushitaFPTcpDriver);

// Constructor

CMatsushitaFPTcpDriver::CMatsushitaFPTcpDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CMatsushitaFPTcpDriver::~CMatsushitaFPTcpDriver(void)
{
	}

// Configuration

void MCALL CMatsushitaFPTcpDriver::Load(LPCBYTE pData)
{
	if( GetWord( pData ) == 0x1234 ) {

		m_Source = GetWord(pData);

		m_Port   = GetWord(pData);

		return;
		}
	}
	
// Management

void MCALL CMatsushitaFPTcpDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CMatsushitaFPTcpDriver::Open(void)
{
	}

// Device

CCODE MCALL CMatsushitaFPTcpDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pTcpCtx = (CTcpContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pTcpCtx = new CTcpContext;

			m_pCtx = m_pTcpCtx;

			m_pTcpCtx->m_IP		= GetAddr(pData);
			m_pTcpCtx->m_uPort	= GetWord(pData);
			m_pCtx->m_bDrop		= GetWord(pData) & 0xFF;
			m_pTcpCtx->m_fKeep	= GetByte(pData);
			m_pTcpCtx->m_fPing	= GetByte(pData);
			m_pTcpCtx->m_uTime1	= GetWord(pData);
			m_pTcpCtx->m_uTime2	= GetWord(pData);
			m_pTcpCtx->m_uTime3	= GetWord(pData);
			m_pTcpCtx->m_bETLan	= GetByte(pData);
			m_pTcpCtx->m_bRs1	= GetByte(pData);
			m_pTcpCtx->m_bRs2	= GetByte(pData);
			m_pTcpCtx->m_bRs3	= GetByte(pData);
			m_pTcpCtx->m_bRs1R	= GetByte(pData);
			m_pTcpCtx->m_bRs2R	= GetByte(pData);
			m_pTcpCtx->m_bRs3R	= GetByte(pData);
			m_pTcpCtx->m_wTrans	= 0;
			m_pTcpCtx->m_pSock	= NULL;
			m_pTcpCtx->m_uLast	= GetTickCount();
					
			pDevice->SetContext(m_pTcpCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx = m_pTcpCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CMatsushitaFPTcpDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pTcpCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pTcpCtx;

		m_pTcpCtx = NULL;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);

	}

// Entry Points

CCODE MCALL CMatsushitaFPTcpDriver::Ping(void)
{
	if( m_pTcpCtx->m_fPing ) {

		if( CheckIP(m_pTcpCtx->m_IP, m_pTcpCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}
		}

	return CMatFP2BaseDriver::Ping();
      	}

// Socket Management

BOOL CMatsushitaFPTcpDriver::CheckSocket(void)
{
	if( m_pTcpCtx->m_pSock ) {

		UINT Phase;

		m_pTcpCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CMatsushitaFPTcpDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pTcpCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pTcpCtx->m_uLast;

		UINT tt = ToTicks(m_pTcpCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pTcpCtx->m_pSock = CreateSocket(IP_TCP)) ) {
	
		IPADDR const &IP   = (IPADDR const &) m_pTcpCtx->m_IP;

		UINT uPort = m_pTcpCtx->m_uPort;

		UINT uOK = 0;

		if( m_pTcpCtx->m_bETLan > ETLAN_NONE ) {

			uOK = m_pTcpCtx->m_pSock->Connect(IP, WORD(uPort), WORD(m_Port));
			}
		else {
			uOK = m_pTcpCtx->m_pSock->Connect(IP, WORD(uPort));
			}

		if( uOK == S_OK ) {

			m_uKeep++;

			SetTimer(m_pTcpCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pTcpCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}
				if( Phase == PHASE_CLOSING ) {

					m_pTcpCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}
				
			CloseSocket(TRUE); 

			return FALSE;
			}
		}    

	return FALSE;
	}

void CMatsushitaFPTcpDriver::CloseSocket(BOOL fAbort)
{
	if( m_pTcpCtx->m_pSock ) {

		if( fAbort )
			m_pTcpCtx->m_pSock->Abort();
		else
			m_pTcpCtx->m_pSock->Close();

		m_pTcpCtx->m_pSock->Release();

		m_pTcpCtx->m_pSock = NULL;

		m_pTcpCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

void CMatsushitaFPTcpDriver::StartFrame(void)
{	
	m_uPtr  = 0;

	if( m_pTcpCtx->m_bETLan > ETLAN_NONE ) {

		AddByte(0x10);

		AddByte(0x00);

		AddByte(0x00);		// Data Size (L)

		AddByte(0x00);		// Data Size (H);

		AddByte(0x00);

		AddByte(0x00);

		AddByte(0x00);

		AddByte(0x00);

		AddByte(0x00);		// Hierarchy Level
		       
		AddByte(0x00);		// Hierarchy Depth

		AddStationNumbers();

		AddByte(m_pCtx->m_bDrop);

		AddByte(LOBYTE(m_Source));

		AddRouteNumbers();
		}

	m_bCheck = 0;

	AddByte( '%' );
		
	AddByte( m_pHex[m_pCtx->m_bDrop / 16] );
	AddByte( m_pHex[m_pCtx->m_bDrop % 16] );

	AddByte( '#' );
	}

BOOL CMatsushitaFPTcpDriver::Transact(void)
{	
	if( OpenSocket() ) {

		if( SendFrame() && GetFrame() ) {
		
			return TRUE;
			} 

		CloseSocket(TRUE); 
		}

	return FALSE;
	}

BOOL CMatsushitaFPTcpDriver::SendFrame(void)
{
	AddCheck();

	UINT uSize = m_uPtr - FindETLanOffset();

	if( m_pTcpCtx->m_bETLan ) {
	
		m_bTxBuff[2] = LOBYTE(uSize);

		m_bTxBuff[3] = HIBYTE(uSize);
		}
	
	uSize = m_uPtr;

	if( m_pTcpCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CMatsushitaFPTcpDriver::GetFrame(void)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	UINT uTotal = 0;

	UINT uBegin = 0;

	SetTimer(m_pTcpCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pTcpCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {	

			uPtr += uSize;

			if( m_pTcpCtx->m_bETLan > ETLAN_NONE ) {

				if( m_bRxBuff[0] != 0x10 ) {

					uPtr = 0;
					
					continue;
					}
				
				if( CheckRoutes() ) {
				   
					uTotal = MAKEWORD(m_bRxBuff[2], m_bRxBuff[3]);

					uBegin = FindETLanOffset();
					}
							
				if( uTotal != uPtr - uBegin ) {

					continue;
					}
 				}
							
			if( m_bRxBuff[uBegin] == '%' && m_bRxBuff[uPtr - 1] == CR  ) {

				memcpy(m_bRxBuff, m_bRxBuff + uBegin + 1, uPtr - uBegin - 2);

				return CheckReply(uPtr - uBegin - 2);
				}
			
			continue;
			}

		if( !CheckSocket() ) {
			
			return FALSE;
			}

		Sleep(10);
		} 
	
	return FALSE;
	}

// Helpers

UINT CMatsushitaFPTcpDriver::FindETLanOffset(void)
{
	if( m_pTcpCtx->m_bETLan > ETLAN_NONE ) {

		return m_pTcpCtx->m_bETLan == 1 ? 12 : 12 + 2 * m_bTxBuff[8];
		}
	
	return 0;
	}

void CMatsushitaFPTcpDriver::AddStationNumbers(void)
{
	if( m_pTcpCtx->m_bETLan == ETLAN_OTHER ) {
	
		if( m_pTcpCtx->m_bRs1 ) {

			AddByte(m_pTcpCtx->m_bRs1);

			m_bTxBuff[8] = 1;

			m_bTxBuff[9] = 1;

			if( m_pTcpCtx->m_bRs2 ) {

				AddByte(m_pTcpCtx->m_bRs2);

				m_bTxBuff[8] = 2;

				m_bTxBuff[9] = 2;

				if( m_pTcpCtx->m_bRs3 ) {

					AddByte(m_pTcpCtx->m_bRs3);

					m_bTxBuff[8] = 3;

					m_bTxBuff[9] = 3;
					}
				}
			}
		}
	}

void CMatsushitaFPTcpDriver::AddRouteNumbers(void)
{
	if( m_pTcpCtx->m_bETLan == ETLAN_OTHER ) {

		for( UINT u = 0; u < m_bTxBuff[8]; u++ ) {

			switch( u ) {

				case 0:
					AddByte(m_pTcpCtx->m_bRs1R);
					break;
				case 1:
					AddByte(m_pTcpCtx->m_bRs2R);
					break;
				case 2:
					AddByte(m_pTcpCtx->m_bRs3R);
					break;
				}
			}
		}
	}

BOOL CMatsushitaFPTcpDriver::CheckRoutes(void)
{
	if( m_pTcpCtx->m_bETLan == ETLAN_SAME ) {

		return m_bRxBuff[10] == LOBYTE(m_Source) &&  m_bRxBuff[11] == m_pCtx->m_bDrop;
		}
			
	if( m_pTcpCtx->m_bETLan == ETLAN_OTHER ) {

		if( m_bRxBuff[8] == 0 && m_bRxBuff[9] == m_bTxBuff[9] ) {

			UINT uIndex = 10;
			
			for( UINT x = 0; x < m_bRxBuff[9]; x++, uIndex++ ) {

				if( m_bTxBuff[uIndex] != m_bRxBuff[uIndex + 1] ) {

					return FALSE;
					}
				}

			if( m_bTxBuff[uIndex] != m_bRxBuff[10] ) {

				return FALSE;
				}

			uIndex++;

			for( UINT y = 0; y < m_bRxBuff[9]; y++, uIndex++ ) {

				if( m_bTxBuff[uIndex] != m_bRxBuff[uIndex] ) {

					return FALSE;
					}
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
