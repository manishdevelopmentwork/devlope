
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cross Reference
//

// Runtime Class

AfxImplementRuntimeClass(CControlCrossReference, CObject);

// Constructor

CControlCrossReference::CControlCrossReference(CControlProject *pProject)
{
	m_pProject = pProject;
	}

// Operations

UINT CControlCrossReference::FindInFiles(CString Find)
{
	CString   Path = m_pProject->GetProjectPath();

	CFilename File = Path + L"usage.txt";

	if( BuildUsageFile(File, Find) ) {

		CTextStreamMemory Stream;

		Stream.SetPrev();

		if( Stream.LoadFromFile(File) ) {

			CBuildFile File(m_pProject);
		
			if( File.OpenLoad(Stream) ) {

				CString Head = CPrintf(CString(IDS_USAGE_FOR_CONTROL), Find);

				File.BuildFindList(Head);

				CString Text;

				Text += CString(IDS_USAGE_OCURRENCES);
				Text += CString(IDS_GLOBAL_RESULTS);

				CWnd::GetActiveWindow().Error(Text);

				return 0;
				}
			}

		}

	CBuildFile(m_pProject).ClearFindList();

	CString Text = CPrintf(CString(IDS_FMT_IS_NOT_USED), Find);

	CWnd::GetActiveWindow().Error(Text);

	return 0;
	}

// Implementation

BOOL CControlCrossReference::BuildUsageFile(CFilename File, CString Name)
{
	CString Path = m_pProject->GetProjectPath();

	CListBox *pList = New CListBox;

	DWORD dwStyle = XS_LISTBOX | LBS_HASSTRINGS;

	if( pList->Create( dwStyle, CRect(), *afxMainWnd, 100) ) {

		BOOL fResult = Straton_XRefFindInFiles( Path, 
							Name, 
							pList->GetHandle(), 
							NULL
							);

		UINT c = pList->GetCount();

		CTextStreamMemory Stream;

		if( Stream.OpenSave() ) {

			for( UINT n = 0; n < c; n ++ ) {

				CString Text;
				
				Text += pList->GetText(n);

				Text += L"\r\n";

				Stream.PutLine(Text);
				}

			Stream.SaveToFile(File, saveRaw);
			}

		pList->DestroyWindow(TRUE);

		AfxTouch(fResult);

		return fResult;
		}
	
	delete pList;

	return FALSE;
	}

// End of File
