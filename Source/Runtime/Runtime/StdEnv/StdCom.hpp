
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_StdCom_HPP

#define INCLUDE_StdCom_HPP

//////////////////////////////////////////////////////////////////////////
//
// COM Status Codes
//

#define	S_OK		0x00000000L
#define S_FALSE		0x00000001L
#define E_FAIL		0x80004005L
#define E_NOINTERFACE	0x80000004L

//////////////////////////////////////////////////////////////////////////
//
// Basic COM Macros
//

#define interface struct

#define	REFIID    IID const &

//////////////////////////////////////////////////////////////////////////
//
// Basic COM Definitions
//

// We put all of this in the win32 namespace and import it to
// ensure that we get identical name mangling between libraries
// which use win32 and libraries which do not.

namespace win32
{
	// GUID Structure

	struct _GUID
	{
		unsigned long  Data1;
		unsigned short Data2;
		unsigned short Data3;
		unsigned char  Data4[8];
	
		};

	// Type Definitions

	typedef struct _GUID GUID;

	typedef struct _GUID IID;

	typedef long         HRESULT;

	// Base Interface

	interface IUnknown
	{
		virtual HRM QueryInterface(REFIID riid, void **ppObject) = 0;
		virtual ULM AddRef(void)                                 = 0;
		virtual ULM Release(void)                                = 0;
		};
	};

//////////////////////////////////////////////////////////////////////////
//
// Basic COM Imports
//

using win32::IUnknown;

using win32::GUID;

using win32::IID;

using win32::HRESULT;

// End of File

#endif
