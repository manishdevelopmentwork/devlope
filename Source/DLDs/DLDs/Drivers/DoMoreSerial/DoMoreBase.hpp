
#ifndef INCLUDE_DO_MORE_BASE

#define INCLUDE_DO_MORE_BASE

// Referenced classes

class CMxAppData;
class CMxAppRequest;

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC Base Driver
//

#define	BUFF_SIZE	1492
#define MAX_STRING	256

class CDoMoreBaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CDoMoreBaseDriver(void);
		
		// Destructor
		~CDoMoreBaseDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		struct CBaseCtx
		{
			WORD	m_wSession;
			BOOL	m_fSessionOpen;
			PTXT	m_pPassword;
			BOOL	m_fUsePing;
			UINT	m_uPingReg;
			};

		// Data Members	
		BYTE		  m_bTxBuff[BUFF_SIZE];
		BYTE		  m_bRxBuff[BUFF_SIZE];
		UINT		  m_uPtr;
		CBaseCtx	* m_pBaseCtx;

		// Session Management
		BOOL CheckSession(void);
		BOOL StartSession(CBaseCtx *pCtx);
		BOOL OpenSession(CBaseCtx *pCtx);
		void CloseSession(void);
		BYTE GetRandomKey(void);

		// MX App Requests
		BOOL StartRead(CBaseCtx *pCtx, AREF Addr, CMxAppData *pRead);
		BOOL StartBulkRead(CBaseCtx *pCtx, AREF Addr, CMxAppData *pRead);
		BOOL StartBulkWrite(CBaseCtx *pCtx, AREF Addr, CMxAppData *pWrite);
		BOOL StartWrite(CBaseCtx *pCtx, AREF Addr, CMxAppData *pWrite);
		BOOL StartMaskedWrite(CBaseCtx *pCtx, AREF Addr, CMxAppData *pWrite, BOOL fIsBit, UINT uBitOffset);

		// Address Helpers
		UINT GetTypeSize(UINT uType);
		BOOL IsStructured(UINT uTable);
		BOOL IsString(UINT uTable);
		WORD GetMaxStringLen(UINT uTable);

		// Frame Building
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddData(PBYTE pData, UINT uCount);

		// Overridables
		virtual void StartHeader(void)			= 0;
		virtual BOOL StartRequest(CMxAppRequest &Req)	= 0;

		// Reading
		CCODE DoStringRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoStructRead(AREF Addr, PDWORD pData, UINT uCount);

		// Writing
		CCODE DoBitWrite(AREF Addr, UINT uSpan, CMxAppData *pWrite);
		CCODE DoWrite(AREF Addr, UINT uSpan, CMxAppData *pWrite);
		CCODE DoStructWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoStringWrite(AREF Addr, PDWORD pData, UINT uCount);
	};

#endif

// End of File
