
//////////////////////////////////////////////////////////////////////////
//
// S7 PPI Driver
//

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define OffWord(x)	(DWORD(x) << 3)

#define OffBit(x, y)	((DWORD(x) << 3) | ((y) & 7))

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	FDL_STATUS	0x49
#define	NAK_RR		0x02
#define	NAK_RS		0x03
#define	SD1		0x10
#define	SD2		0x68
#define	SD4		0xDC
#define	SC		0xE5
#define	ED		0x16

//////////////////////////////////////////////////////////////////////////
//
// Table Equalities
//
#define	VW	1
#define	IW	2
#define	QW	3
#define	MW	4
#define	AI	5
#define	AQ	6
#define TM	7
#define CN	8

//////////////////////////////////////////////////////////////////////////
//
// Rx Codes
//

#define	RX_NONE		0
#define	RX_ERROR	1
#define	RX_FRAME	2
#define	RX_SC		3
#define	RX_NAK		4

//////////////////////////////////////////////////////////////////////////
//
//Type Codes
//

#define	TC_BOOL		0x01
#define	TC_BYTE		0x02
#define	TC_WORD		0x04
#define	TC_DWORD	0x06
#define	TC_COUNTER	0x1E
#define	TC_TIMER	0x1F
#define	TC_HSC		0x20

//////////////////////////////////////////////////////////////////////////
//
// Area Codes
//

#define	AC_S		0x04
#define	AC_SM		0x05
#define	AC_AI		0x06
#define	AC_AQ		0x07
#define	AC_C		0x1E
#define	AC_T		0x1F
#define	AC_HC		0x20
#define	AC_I		0x81
#define	AC_Q		0x82
#define	AC_M		0x83
#define	AC_V		0x84

//////////////////////////////////////////////////////////////////////////
//
// Standard PPI Functions
//

#define	MSG_ASSOC	0
#define	MSG_READ	1
#define	MSG_WRITE	2
#define	MSG_READ_TOD	3
#define	MSG_SET_TOD	4

//////////////////////////////////////////////////////////////////////////
//
// Response Data
//

#define MSGSTART	3
#define	ROCSTR		(MSGSTART +  1)
#define PDU_REF 	(MSGSTART +  4)
#define	NUM_VARS	(MSGSTART + 13)
#define	ACCESS_RESULT	(MSGSTART + 14)

//////////////////////////////////////////////////////////////////////////
//
// Timing Values
//

#define	START_TIMEOUT	400
#define	FRAME_TIMEOUT	800
#define	POLL_DELAY	40
#define	BACKOFF_TIME	80

class CS7PPIDriver : public CMasterDriver
{
	public:
		// Constructor
		CS7PPIDriver(void);

		// Destructor
		~CS7PPIDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Hex Lookup
		LPCTXT m_pHex;
		
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			};
		CContext * m_pCtx;
		
		// Comms Data
		BYTE m_bTxBuff[256];
		BYTE m_bRxBuff[256];

		// Data Members
		BYTE 	m_bSrcAddr;
		BYTE	m_bCheck;
		BYTE	m_bTxPtr;
		WORD	m_wPollBase;
		WORD	m_wPollTimer;
		WORD	m_wPDU_REF;
		WORD	m_wMsgType;
		BYTE	m_bLastFC;
		WORD	m_wLastRx;
		BYTE	m_bNakCode;
		DWORD	m_Status;
		WORD	m_ErrorInject;

		// Implementation
		void StartFrame(BYTE bLen);
		BOOL ProcessWrite(void);
		BOOL ProcessRead(PDWORD pData, UINT uType);
		void NewRequest(WORD wType);
		void ClearFrame(void);
		BYTE GetFC(void);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddDword(DWORD dwData);
		void LowByte(BYTE bData);
		void EndFrame(void);
		void TxFrame(void);
		BOOL Transact(void);
		void AddHeader(UINT uParaLen, UINT uDataLen);
		void AddParameterBlock(BYTE bService, UINT uCount, AREF Addr);
		WORD GetElements(BYTE bType);
		DWORD GetOffset(WORD wAddr, BYTE bType);
		WORD NextAddress(WORD wAddr, BYTE bType);
		void Wait(UINT uPeriod);
		BOOL Poll(BOOL fQuick);
		void SetPollTimer(WORD wSetting);
		WORD GetPollTimer(void);
		WORD RxData(void);
		BOOL CheckSD2Response(void);
		PBYTE GetpWord(PWORD pw, PBYTE b);
		PBYTE GetpDword(PDWORD pdw, PBYTE b);
		BOOL  IsLong(UINT uType);
		UINT  GetType(UINT uType);
		UINT  GetAddedBytes(UINT uTable);

	};

// End of File
