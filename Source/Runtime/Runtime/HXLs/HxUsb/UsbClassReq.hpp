
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbClassReq_HPP

#define	INCLUDE_UsbClassReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbDeviceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Device Class Requeset
//

class CUsbClassReq : public CUsbDeviceReq
{
	public:
		// Constructor
		CUsbClassReq(void);

		// Init
		void Init(void);
	};

// End of File

#endif
