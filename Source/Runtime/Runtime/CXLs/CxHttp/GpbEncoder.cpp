
#include "Intern.hpp"

#include "GpbEncoder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Google Protocol Buffer Encoder
//

// Implementation

STRONG_INLINE void CGpbEncoder::AddField(UINT uField, UINT uWire)
{
	m_Data.Append(BYTE((uField << 3) | uWire));
}

STRONG_INLINE UINT CGpbEncoder::SetVarInt32(PBYTE pData, UINT32 Value) const
{
	UINT uSize = 0;

	while( Value >= 128 ) {

		pData[uSize++] = BYTE(0x80 | (Value & 0x7F));

		Value >>= 7;
	}

	pData[uSize++] = BYTE(Value);

	return uSize;
}

STRONG_INLINE UINT CGpbEncoder::SetVarInt64(PBYTE pData, UINT64 Value) const
{
	UINT uSize = 0;

	while( Value >= 128 ) {

		pData[uSize++] = BYTE(0x80 | (Value & 0x7F));

		Value >>= 7;
	}

	pData[uSize++] = BYTE(Value);

	return uSize;
}

STRONG_INLINE UINT CGpbEncoder::SetZigZag32(PBYTE pData, SINT32 Value) const
{
	Value = (Value >> 31) | (Value << 1);

	return SetVarInt32(pData, Value);
}

STRONG_INLINE UINT CGpbEncoder::SetZigZag64(PBYTE pData, SINT64 Value) const
{
	Value = (Value >> 63) | (Value << 1);

	return SetVarInt64(pData, Value);
}

// Operations

bool CGpbEncoder::Encode(CBuffer *pBuff)
{
	memcpy(pBuff->AddTail(m_Data.GetCount()), m_Data.GetPointer(), m_Data.GetCount());

	return true;
}

bool CGpbEncoder::Encode(CByteArray &Data)
{
	Data.Empty();

	Data.Append(m_Data);

	return false;
}

void CGpbEncoder::Reset(UINT uSize)
{
	m_Data.SetCount(uSize);
}

// Data Access

CByteArray const & CGpbEncoder::GetData(void) const
{
	return m_Data;
}

// Attributes

UINT CGpbEncoder::GetSize(void) const
{
	return m_Data.GetCount();
}

// Field Operations

bool CGpbEncoder::AddFieldAsInt32(UINT uField, INT32 Value)
{
	AddField(uField, wireVarint);

	BYTE bData[12];

	UINT uSize = SetVarInt32(bData, UINT32(Value));

	m_Data.Append(bData, uSize);

	return true;
}

bool CGpbEncoder::AddFieldAsInt64(UINT uField, INT64 Value)
{
	AddField(uField, wireVarint);

	BYTE bData[12];

	UINT uSize = SetVarInt64(bData, UINT64(Value));

	m_Data.Append(bData, uSize);

	return true;
}

bool CGpbEncoder::AddFieldAsUInt32(UINT uField, UINT32 Value)
{
	AddField(uField, wireVarint);

	BYTE bData[12];

	UINT uSize = SetVarInt32(bData, UINT32(Value));

	m_Data.Append(bData, uSize);

	return true;
}

bool CGpbEncoder::AddFieldAsUInt64(UINT uField, UINT64 Value)
{
	AddField(uField, wireVarint);

	BYTE bData[12];

	UINT uSize = SetVarInt64(bData, UINT64(Value));

	m_Data.Append(bData, uSize);

	return true;
}

bool CGpbEncoder::AddFieldAsSInt32(UINT uField, SINT32 Value)
{
	AddField(uField, wireVarint);

	BYTE bData[12];

	UINT uSize = SetZigZag32(bData, Value);

	m_Data.Append(bData, uSize);

	return true;
}

bool CGpbEncoder::AddFieldAsSInt64(UINT uField, SINT64 Value)
{
	AddField(uField, wireVarint);

	BYTE bData[12];

	UINT uSize = SetZigZag64(bData, Value);

	m_Data.Append(bData, uSize);

	return true;
}

bool CGpbEncoder::AddFieldAsBool(UINT uField, bool Value)
{
	AddField(uField, wireVarint);

	BYTE bData[12];

	UINT uSize = SetVarInt32(bData, UINT32(Value ? 1 : 0));

	m_Data.Append(bData, uSize);

	return true;
}

bool CGpbEncoder::AddFieldAsEnum(UINT uField, int Value)
{
	AddField(uField, wireVarint);

	BYTE bData[12];

	UINT uSize = SetVarInt32(bData, UINT32(Value));

	m_Data.Append(bData, uSize);

	return true;
}

bool CGpbEncoder::AddFieldAsFixed32(UINT uField, UINT32 Value)
{
	AddField(uField, wire32Bit);

	BYTE bData[4];

	*PDWORD(bData) = HostToIntel(DWORD(Value));

	m_Data.Append(bData, 4);

	return true;
}

bool CGpbEncoder::AddFieldAsSFixed32(UINT uField, SINT32 Value)
{
	AddField(uField, wire32Bit);

	BYTE bData[4];

	*PDWORD(bData) = HostToIntel(DWORD(Value));

	m_Data.Append(bData, 4);

	return true;
}

bool CGpbEncoder::AddFieldAsFixed64(UINT uField, UINT64 Value)
{
	AddField(uField, wire64Bit);

	BYTE bData[12];

	*PINT64(bData) = HostToIntel(INT64(Value));

	m_Data.Append(bData, 8);

	return true;
}

bool CGpbEncoder::AddFieldAsSFixed64(UINT uField, SINT64 Value)
{
	AddField(uField, wire64Bit);

	BYTE bData[12];

	*PINT64(bData) = HostToIntel(INT64(Value));

	m_Data.Append(bData, 8);

	return true;
}

bool CGpbEncoder::AddFieldAsFloat(UINT uField, float Value)
{
	AddField(uField, wire32Bit);

	BYTE bData[4];

	*PDWORD(bData) = HostToIntel(*PDWORD(&Value));

	m_Data.Append(bData, 4);

	return true;
}

bool CGpbEncoder::AddFieldAsDouble(UINT uField, double Value)
{
	AddField(uField, wire64Bit);

	BYTE bData[12];

	*PINT64(bData) = HostToIntel(*PINT64(&Value));

	m_Data.Append(bData, 4);

	return true;
}

bool CGpbEncoder::AddFieldAsString(UINT uField, CString Value)
{
	EncodeUtf(Value);

	AddField(uField, wireLengthDelimited);

	BYTE bData[12];

	UINT uSize = SetVarInt32(bData, UINT32(Value.GetLength()));

	m_Data.Append(bData, uSize);

	m_Data.Append(PCBYTE(PCTXT(Value)), Value.GetLength());

	return true;
}

bool CGpbEncoder::AddFieldAsUnicode(UINT uField, CUnicode Value)
{
	CString Uft16 = UtfConvert(Value);

	AddField(uField, wireLengthDelimited);

	BYTE bData[12];

	UINT uSize = SetVarInt32(bData, UINT32(Uft16.GetLength()));

	m_Data.Append(bData, uSize);

	m_Data.Append(PCBYTE(PCTXT(Uft16)), Uft16.GetLength());

	return true;
}

bool CGpbEncoder::AddMessage(UINT uField, CByteArray const &Data)
{
	AddField(uField, wireLengthDelimited);

	BYTE bData[12];

	UINT uSize = SetVarInt32(bData, UINT32(Data.GetCount()));

	m_Data.Append(bData, uSize);

	m_Data.Append(Data);

	return true;
}

bool CGpbEncoder::AddMessage(UINT uField, CGpbEncoder const &gpb)
{
	AddField(uField, wireLengthDelimited);

	BYTE bData[12];

	UINT uSize = SetVarInt32(bData, UINT32(gpb.GetSize()));

	m_Data.Append(bData, uSize);

	m_Data.Append(gpb.GetData());

	return true;
}

// End of File
