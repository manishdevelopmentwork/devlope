
#include "Intern.hpp"

#include "ServiceItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Possible Classes
//

/*
#include "CodedItem.hpp"
#include "CodedText.hpp"
#include "DataServer.hpp"
#include "NameServer.hpp"
*/

//////////////////////////////////////////////////////////////////////////
//
// Service Base Class
//

// Dynamic Class

AfxImplementDynamicClass(CServiceItem, CCodedHost);

// Constructor

CServiceItem::CServiceItem(void)
{
	m_Handle = HANDLE_NONE;
}

// Attributes

BOOL CServiceItem::IsEnabled(void) const
{
	return m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B;
}

UINT CServiceItem::GetTreeImage(void) const
{
	AfxAssert(FALSE);

	return 0;
}

// Conversion

void CServiceItem::PostConvert(void)
{
}

// Meta Data Creation

void CServiceItem::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddInteger(Handle);

	Meta_SetName((IDS_GENERIC_SERVICE));
}

// End of File
