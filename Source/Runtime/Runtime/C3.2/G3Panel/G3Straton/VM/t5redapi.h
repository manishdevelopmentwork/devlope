/*****************************************************************************
T5RedApi.h : Redundancy - server - definitions
(c) COPALP 2006
*****************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************************************/
/* integration */

extern T5_BOOL T5Red_ShouldTerminate (T5_DWORD dwVMID);
extern void T5Red_Trace (T5_PTCHAR str, ...);
#ifdef T5DEF_REDBUSDRVEXT
extern void T5Red_OnSwitchError (T5_DWORD dwVMID);
#endif /*T5DEF_REDBUSDRVEXT*/

#define T5REDLOG T5Red_Trace
#define T5RED_EXT_TIMEOUT
#define T5RED_EXT_RETAIN

/****************************************************************************/
/* packing */

#ifndef T5RED_PACK_SIZE
#define T5RED_PACK_SIZE     2048
#endif /*T5RED_PACK_SIZE*/

/****************************************************************************/
/* RETAIN settings */

#ifdef T5RED_EXT_RETAIN

extern T5_PTR T5Red_GetRetainSettings (T5_DWORD dwVMID);

#define T5RED_RETAINSETTINGS(id) (T5Red_GetRetainSettings (id))

#else /*T5RED_EXT_RETAIN*/

#define T5RED_RETAINSETTINGS NULL

#endif /*T5RED_EXT_RETAIN*/

/****************************************************************************/
/* threads */

typedef void (*PFT5REDTH)(T5_PTR pArgs);

/****************************************************************************/
/* timeouts */

#ifdef T5RED_EXT_TIMEOUT

extern T5_DWORD T5Red_GetTimeout_SRV (void);
extern T5_DWORD T5Red_GetTimeout_CLI (void);
extern T5_DWORD T5Red_GetTimeout_VM (void);
extern T5_DWORD T5Red_GetTimeout_GTW (void);
extern T5_DWORD T5Red_GetTimeout_STR (void);
extern T5_DWORD T5Red_GetTimeout_CNX (void);

#define T5RED_SRV_TIMEOUT   (T5Red_GetTimeout_SRV ())
#define T5RED_CLI_TIMEOUT   (T5Red_GetTimeout_CLI ())
#define T5RED_VM_TIMEOUT    (T5Red_GetTimeout_VM ())
#define T5RED_GTW_TIMEOUT   (T5Red_GetTimeout_GTW ())
#define T5RED_STR_TIMEOUT   (T5Red_GetTimeout_STR ())
#define T5RED_CNX_TIMEOUT   (T5Red_GetTimeout_CNX ())

#else /*T5RED_EXT_TIMEOUT*/

#define T5RED_SRV_TIMEOUT   1000    /* the server surveys the client */
#define T5RED_CLI_TIMEOUT   1000    /* the client surveys the server */
#define T5RED_VM_TIMEOUT    500     /* the VM waits for next cycle */
#define T5RED_GTW_TIMEOUT   5000    /* gateway exchanges */
#define T5RED_STR_TIMEOUT   10000   /* the server waits for client to start */
#define T5RED_CNX_TIMEOUT   1000    /* connection to the partner */

#endif /*T5RED_EXT_TIMEOUT*/

/****************************************************************************/
/* frame: partner identification */

#define T5RED_UNKNOWN       '?'     /* uninitialized */
#define T5RED_MASTER        'M'     /* master */
#define T5RED_SLAVE         'S'     /* slave */

/****************************************************************************/
/* frame: partner status */

#define T5REDS_UNKNOWN      '?'     /* uninitialized */
#define T5REDS_IDLE         '0'     /* idle state */
#define T5REDS_CODECRC      '1'     /* exchange code CRC and size */
#define T5REDS_CODETFR      '2'     /* exchange code */
#define T5REDS_CODECHG      '3'     /* notify that code has changed */
#define T5REDS_RUNNING      'R'     /* state = running app */
#define T5REDS_SENDDB       'D'     /* state = sending VMDB */

#define T5REDS_SLVIDLE      "{S0}"  /* slave - idle message */
#define T5REDS_SLVCODECRC   "{S1}"  /* slave - ask for code CRC and size */
#define T5REDS_SLVCODETFR   "{S2}"  /* slave - ask for code */
#define T5REDS_SLVRUNNING   "{SR}"  /* slave - running */

/****************************************************************************/
/* alive signal */

#define T5REDALV_UNKNOWN    0       /* not yet detected */
#define T5REDALV_ALIVE      1       /* detected as alive */
#define T5REDALV_DEAD       2       /* signal lost */

/****************************************************************************/
/* Services: server */

extern T5_PTR   T5RedSrv_Open (T5_DWORD dwVMID, T5_WORD wPort);
extern void     T5RedSrv_Close (T5_PTR pSrvData);
extern void     T5RedSrv_SetState (T5_PTR pSrvData, T5_CHAR bNewState);
extern T5_BOOL  T5RedSrv_Busy (T5_PTR pSrvData);
extern void     T5RedSrv_NotifyCodeChanged (T5_PTR pSrvData);
extern void     T5RedSrv_SetRunState (T5_PTR pSrvData, T5_BOOL bState);
extern void     T5RedSrv_AllocDBMem (T5_PTR pSrvData,
                                     T5_DWORD dwSize, T5_PTR pDBcur);
extern void     T5RedSrv_ReleaseDBMem (T5_PTR pSrvData);
extern void     T5RedSrv_StartExchange (T5_PTR pSrvData, T5_PTR pDBcur);
extern void     T5RedSrv_StopExchange (T5_PTR pSrvData);
extern T5_BOOL  T5RedSrv_HasClient (T5_PTR pSrvData);
extern T5_BOOL  T5RedSrv_HasClientReady (T5_PTR pSrvData);
extern void     T5RedSrv_BePassive (T5_PTR pSrvData);
extern T5_BOOL  T5RedSrv_UserReq (T5_PTR pSrvData,
                                  T5PTR_CS pCS, T5_WORD wCaller);
extern T5_BOOL  T5RedSrv_SwitchQueried (T5_PTR pSrvData);
extern void     T5RedSrv_RegisterSwitch (T5_PTR pSrvData, T5_DWORD dwSwitchTime);

/****************************************************************************/
/* Services: client */

extern T5_PTR   T5RedCli_Init (void);
extern void     T5RedCli_Exit (T5_PTR pClientData);

extern T5_BOOL  T5RedCli_IsClientEstablished (T5_PTR pClientData);
extern T5_BOOL  T5RedCli_WaitToBeMaster (T5_DWORD dwVMID, T5_PTR pClientData,
                                         T5_PTCHAR szPartner, T5_WORD wPort,
                                         T5PTR_CS pCS, T5_WORD wGtwPort,
                                         T5PTR_MM pMM, T5_PTR pEV,
                                         T5_PTDWORD pdwSwitchTime);

extern T5_BOOL  T5RedCli_WaitToBeMasterLoop (T5_DWORD dwVMID, T5_PTR pClientData,
                                         T5_PTCHAR szPartner, T5_WORD wPort,
                                         T5PTR_CS pCS, T5_WORD wGtwPort,
                                         T5PTR_MM pMM, T5_PTR pEV,
                                         T5_PTDWORD pdwSwitchTime);

/****************************************************************************/
/* Services: VM */

extern void     T5RedVM_Run (T5PTR_CS pCS, T5PTR_MM pMM, T5_PTR pEV);

/****************************************************************************/
/* Services: alve link */

extern T5_BOOL  T5RedAlv_Open (T5_PTCHAR szSettings);
extern T5_RET   T5RedAlv_ReadAliveSignal (void);
extern void     T5RedAlv_SendAliveSignal (void);
extern void     T5RedAlv_StopAliveSignal (void);
extern void     T5RedAlv_Close (void);

/****************************************************************************/
/* Services: tools - externally implemented */


/****************************************************************************/
/* System calls */

extern void     T5RedSys_StartThread (PFT5REDTH pfThread, T5_BOOL bHighPrio,
                                      T5_PTR pArgs);
extern void     T5RedSys_SetCurThreadPriority (T5_BOOL bHighPrio);
extern void     T5RedSys_Sleep (T5_DWORD dwMilliseconds);
extern T5_DWORD T5RedSys_GetTickCount (void);
extern T5_PTR   T5RedSys_Malloc (T5_DWORD dwSize);
extern void     T5RedSys_Free (T5_PTR ptr);

/****************************************************************************/
/* Replication link */

extern T5_DWORD  T5RedRpl_GetThisAddr (void);
extern T5_DWORD  T5RedRpl_GetPartnerAddr (T5_PTCHAR szPartner);
extern void      T5RedRpl_SetBlockingSocket (T5_SOCKET sock);
extern T5_RET    T5Rpl_CreateListeningSocket (T5_WORD wPort,
                                        T5_WORD wMaxCnx, T5_PTSOCKET pSocket);
extern void      T5Rpl_CloseSocket (T5_SOCKET sock);
extern T5_SOCKET T5Rpl_Accept (T5_SOCKET sockListen);
extern T5_WORD   T5Rpl_Send (T5_SOCKET sock, T5_WORD wSize, T5_PTR pData,
                                T5_PTBOOL pbFail);
extern T5_WORD   T5Rpl_Receive (T5_SOCKET sock, T5_WORD wSize,
                                   T5_PTR pData, T5_PTBOOL pbFail);
extern T5_RET    T5Rpl_CreateConnectedSocket (T5_PTCHAR szAddr, T5_WORD wPort,
                                              T5_PTSOCKET pSocket,
                                              T5_PTBOOL pbWait);
extern T5_RET    T5Rpl_CheckPendingConnect (T5_SOCKET sock, T5_PTBOOL pbFail);

/****************************************************************************/
/* Services: tools - ready to use */

extern T5_PTR   T5RedUti_AllocData (void);
extern void     T5RedUti_FreeData (T5_PTR pData);

extern void     T5RedUti_ResetLocalCodeCRC (T5_PTR pSrvData);
extern void     T5RedUti_SetLocalCodeCRC (T5_PTR pSrvData, T5PTR_MM pMM);
extern T5_DWORD T5RedUti_GetLocalCodeCRC (T5_PTR pSrvData, T5_PTDWORD pdwSize);
extern T5_PTR   T5RedUti_GetLocalCode (T5_PTR pSrvData, T5PTR_MMB pmmb,
                                       T5_PTDWORD pdwSize);
extern void     T5RedUti_ReleaseLocalCode (T5_PTR pSrvData, T5PTR_MMB pmmb);

extern void     T5RedUti_RegisterAppCode (T5_PTR pClientData, T5_PTR pCode);

extern T5_DWORD T5RedUti_RegisterPartnerCycle (void);
extern T5_DWORD T5RedUti_RegisterSwitch (T5_DWORD dwLastReg);

/****************************************************************************/
/* function blocks */

T5_DWORD GETACTIVERTSTAT (T5_WORD wCommand, T5PTR_DB pBase, T5_PTR pClass,
                          T5_PTR pInst, T5_PTWORD pArgs);
T5_DWORD ACTIVERTSWITCH (T5_WORD wCommand, T5PTR_DB pBase, T5_PTR pClass,
                         T5_PTR pInst, T5_PTWORD pArgs);

/****************************************************************************/

#ifdef __cplusplus
}
#endif
