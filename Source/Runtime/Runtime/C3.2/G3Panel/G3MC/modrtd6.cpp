
#include "intern.hpp"

#include "modrtd6.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// CSRTD6 Module Configuration
//

// Instantiator

CModule * Create_RTD6(void)
{
	return New CRTD6Module;
	}

// Constructor

CRTD6Module::CRTD6Module(void)
{
	m_FirmID = FIRM_RTD6;
	}

// Destructor

CRTD6Module::~CRTD6Module(void)
{
	}

// Overridables

void CRTD6Module::OnLoad(PCBYTE &pData)
{
	GetWord(pData);
	}

// End of File
