
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSetAddressReq_HPP

#define	INCLUDE_UsbSetAddressReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbStandardReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Address Standard Device Requeset
//

class CUsbSetAddressReq : public CUsbStandardReq
{
	public:
		// Constructor
		CUsbSetAddressReq(WORD wAddress);

		// Operations
		void Init(WORD wAddress);
	};

// End of File

#endif
