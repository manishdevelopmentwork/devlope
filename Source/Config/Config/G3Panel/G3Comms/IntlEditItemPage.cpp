
#include "Intern.hpp"

#include "IntlEditItemPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "IntlEditItem.hpp"
#include "IntlString.hpp"
#include "IntlStringList.hpp"
#include "LangManager.hpp"
#include "UITransButton.hpp"

//////////////////////////////////////////////////////////////////////////
//
// International String Editing Page
//

// Runtime Class

AfxImplementRuntimeClass(CIntlEditItemPage, CUIStdPage);

// Constructor

CIntlEditItemPage::CIntlEditItemPage(CIntlEditItem *pIntlEditItem)
{
	m_pIntlEditItem = pIntlEditItem;
	}

// Operations

BOOL CIntlEditItemPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(L"Translations", 1);

	UINT uCount = m_pIntlEditItem->m_pList->GetItemCount();

	for( UINT n = 0; n < uCount; n ++ ) {

		CUIData UIData;

		UIData.m_Tag       = L"Text";

		UIData.m_Label     = CPrintf(L"%u", n);

		UIData.m_ClassText = AfxNamedClass(L"CUITextString");

		UIData.m_ClassUI   = AfxNamedClass(L"CUITranslation");

		UIData.m_Format    = L"400||50";

		CIntlString *pText = m_pIntlEditItem->m_pList->GetItem(n);

		pView->AddUI(pText, L"root", &UIData);
		}

	pView->EndGroup(TRUE);

	CCommsSystem *pSystem = (CCommsSystem *) pItem->GetDatabase()->GetSystemItem();

	CLangManager *pLang   = pSystem->m_pLang;

	if( pLang->GetUsedCount() > 1 ) {

		pView->StartGroup(CString(IDS_COMMANDS), 1);

		CString Logo = L"";

		if( pLang->m_LexMode < 2 ) {

			switch( pLang->m_Server ) {

				case 0:
					Logo = L"goog";
					break;

				case 1:
					Logo = L"msft";
					break;
				}
			}

		pView->AddElement(New CUITransButton(Logo));

		pView->EndGroup(FALSE);
		}
	else {
		pView->StartGroup(CString(IDS_COMMANDS), 1);

		pView->AddNarrative(CString(IDS_TO_ENABLE));

		pView->EndGroup(FALSE);
		}

	return FALSE;
	}

// End of File
