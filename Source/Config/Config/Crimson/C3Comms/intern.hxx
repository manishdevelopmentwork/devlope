
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "..\build.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Additional Button IDs
//

#define	IDEDIT			500

#define	IDCLEAR			501

//////////////////////////////////////////////////////////////////////////
//
// Styles
//

#define	XS_LISTBOX_STYLE	XS_LISTBOX | LBS_SORT | LBS_USETABSTOPS

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_AB_ADDR_PERIOD      0x4000
#define IDS_AB_ADDR_RANGE       0x4001
#define IDS_AB_EXPANDED_TAGS    0x4002
#define IDS_AB_EXPANDING_TAGS   0x4003
#define IDS_AB_FILE_RANGE       0x4004
#define IDS_AB_IMPORT_TAGS      0x4005
#define IDS_AB_READING_TAGS     0x4006
#define IDS_AB_SELECT_TAG       0x4007
#define IDS_AB_TAG              0x4008
#define IDS_AB_VIEW_TAG         0x4009
#define IDS_ACCESS              0x400A
#define IDS_ADDITIONAL          0x400B
#define IDS_ADDRESS_FORMAT_IS   0x400C
#define IDS_ADDRESS_NAME        0x400D
#define IDS_ADDRESS_NOT_FOUND   0x400E
#define IDS_ADDRESS_OUT_OF      0x400F
#define IDS_ALL_EVENTSMOST      0x4010
#define IDS_ANALOG_INPUT        0x4011
#define IDS_APPLICATION         0x4012
#define IDS_ARE_YOU_SURE_YOU    0x4013
#define IDS_ARRAY_DIMENSIONS    0x4014
#define IDS_ARRAY_OFFSET_OUT    0x4015
#define IDS_BBBSAP_TAGS         0x4016 /* NOT USED */
#define IDS_BECKHOFFPLUS_TAGS   0x4017 /* NOT USED */
#define IDS_BROWSE_FILE         0x4018
#define IDS_CAN_CHNG_INC        0x4019
#define IDS_CAN_EDS             0x401A /* NOT USED */
#define IDS_CAN_EDS_UPDATE      0x401B /* NOT USED */
#define IDS_CDE_FILES_CDECDE    0x401C
#define IDS_COPY_IS_COMPLETE    0x401D
#define IDS_CREATE_TAGS_FROM    0x401E
#define IDS_CREATE_TAGS_FROM_2  0x401F
#define IDS_CSV_HEADER_IS       0x4020
#define IDS_CT_UDRV_CHNG_MODE   0x4021
#define IDS_CT_UDRV_EXPORT      0x4022
#define IDS_CT_UDRV_IMPORT      0x4023
#define IDS_CT_UDRV_ITEMRANGE   0x4024
#define IDS_CT_UDRV_ONIMPORT    0x4025
#define IDS_CURRENT_VALUE       0x4026
#define IDS_DATA_BLOCK_FILES    0x4027
#define IDS_DATA_BLOCK_MUST     0x4028
#define IDS_DEVICE              0x4029
#define IDS_DEVICE_2            0x402A
#define IDS_DEVICE_SETTINGS     0x402B
#define IDS_DIRECT_CONNECT      0x402C
#define IDS_DOWNLOAD            0x402D
#define IDS_DOWNLOAD_2          0x402E
#define IDS_DOWNLOAD_UTILITY    0x402F
#define IDS_DRIVER_ADDRRANGE    0x4030
#define IDS_DRIVER_ADDR_COLON   0x4031
#define IDS_DRIVER_ADDR_INVALID 0x4032
#define IDS_DRIVER_ADDR_RANGE   0x4033
#define IDS_DRIVER_BAD_ADDR     0x4034
#define IDS_DRIVER_CAN          0x4035
#define IDS_DRIVER_CAPTION      0x4036
#define IDS_DRIVER_CAPTION1     0x4037
#define IDS_DRIVER_CATLINK      0x4038
#define IDS_DRIVER_DEVICENET    0x4039
#define IDS_DRIVER_ETHERNET     0x403A
#define IDS_DRIVER_FIREWIRE     0x403B
#define IDS_DRIVER_INFO         0x403C /* NOT USED */
#define IDS_DRIVER_J1939        0x403D
#define IDS_DRIVER_MPI          0x403E
#define IDS_DRIVER_NONE         0x403F
#define IDS_DRIVER_NONESELECTED 0x4040
#define IDS_DRIVER_NOSELECTION  0x4041
#define IDS_DRIVER_OPTIONS      0x4042
#define IDS_DRIVER_PORTCAPTION  0x4043
#define IDS_DRIVER_PROFIBUS     0x4044
#define IDS_DRIVER_RAW          0x4045
#define IDS_DRIVER_STD          0x4046
#define IDS_DUPLICATE           0x4047
#define IDS_DUP_LIMHI           0x4048
#define IDS_DUP_LIMLO           0x4049
#define IDS_DUP_TIMEDAY         0x404A
#define IDS_DUP_TIMEOFF         0x404B
#define IDS_DUP_TIMEON          0x404C
#define IDS_ELEMENTS_MUST_BE    0x404D
#define IDS_ERROR_ADDR          0x404E
#define IDS_ERROR_ALIGN         0x404F /* NOT USED */
#define IDS_ERROR_BADOFFSET     0x4050
#define IDS_ERROR_OFFSETRANGE   0x4051
#define IDS_ERROR_RANGE         0x4052 /* NOT USED */
#define IDS_ERR_DATA_TYPE       0x4053
#define IDS_ERR_EXIST_AS        0x4054
#define IDS_ERR_IMPORT          0x4055
#define IDS_ERR_TOO_LONG        0x4056
#define IDS_EVENT_BUFFER        0x4057
#define IDS_EVENT_BUFFER_FULL   0x4058
#define IDS_EVENT_MODE          0x4059
#define IDS_EXCHANGE            0x405A
#define IDS_FALLBACK            0x405B
#define IDS_FLOW_FORMAT         0x405C
#define IDS_FMT_ALREADY         0x405D
#define IDS_FMT_DOES_NOT        0x405E
#define IDS_FMT_LIST_IS_NOT     0x405F
#define IDS_FMT_TO_FMT          0x4060
#define IDS_FORMAT              0x4061
#define IDS_FORMAT_2            0x4062
#define IDS_GENERIC             0x4063
#define IDS_HC_DEVICE_OPTIONS   0x4064
#define IDS_HC_NETWORK_DEVICE   0x4065
#define IDS_HC_SERIAL_DEVICE    0x4066
#define IDS_IDENTIFICATION      0x4067
#define IDS_IDENTIFIERS_MUST    0x4068
#define IDS_IDENTIFIER_FMT_IS   0x4069
#define IDS_IDTNO_SELECTION     0x406A
#define IDS_IMPORT_OPERATION    0x406B
#define IDS_INCORRECT_NUMBER    0x406C
#define IDS_INFORMATION         0x406D
#define IDS_INVALID_ADDRESS     0x406E
#define IDS_ITEM                0x406F
#define IDS_J_DEVICE_OPTIONS    0x4070
#define IDS_J_DRIVER_OPTIONS    0x4071
#define IDS_J_PGN               0x4072
#define IDS_J_SPN               0x4073
#define IDS_LECOM_CODE_RANGE    0x4074
#define IDS_LECOM_SUB_RANGE     0x4075
#define IDS_LGX5_NAME           0x4076 /* NOT USED */
#define IDS_LGX5_TAGS           0x4077 /* NOT USED */
#define IDS_ML_ADDRESS          0x4078
#define IDS_ML_BLOCKS           0x4079
#define IDS_ML_OPTIONS          0x407A
#define IDS_MODEL               0x407B
#define IDS_NAMED_ADDR_INVALID  0x407C
#define IDS_NO                  0x407D
#define IDS_NO_AVAILABLE_SLOT   0x407E
#define IDS_NO_DRIVERS          0x407F
#define IDS_OFFSET_IS           0x4080
#define IDS_OFFSET_IS_OUT_OF    0x4081
#define IDS_ONLY_THOSE_TAG      0x4082
#define IDS_ON_CHANGE_VALUE     0x4083
#define IDS_OPC_FILE            0x4084
#define IDS_OPERATION_IS        0x4085
#define IDS_P6K_AXIS            0x4086 /* NOT USED */
#define IDS_P6K_BIT             0x4087 /* NOT USED */
#define IDS_P6K_BITRANGE        0x4088 /* NOT USED */
#define IDS_P6K_BRICK           0x4089 /* NOT USED */
#define IDS_P6K_IN              0x408A /* NOT USED */
#define IDS_P6K_INTRT           0x408B /* NOT USED */
#define IDS_P6K_OUT             0x408C /* NOT USED */
#define IDS_P6K_VAR             0x408D /* NOT USED */
#define IDS_PCOM_COL            0x408E
#define IDS_PCOM_DEVICE_OPTIONS 0x408F
#define IDS_PCOM_TABLE          0x4090
#define IDS_PDO_NUMBER          0x4091
#define IDS_PLEASE_ENTER        0x4092
#define IDS_PLEASE_ENTER_2      0x4093
#define IDS_PROTOCOL_OPTIONS    0x4094
#define IDS_READ                0x4095
#define IDS_READWRITE           0x4096
#define IDS_READ_ONLY           0x4097
#define IDS_RLC_FORMAT          0x4098
#define IDS_RSLOGIX_EXPORT      0x4099
#define IDS_S5_BLOCK            0x409A /* NOT USED */
#define IDS_S5_ELEMENT          0x409B /* NOT USED */
#define IDS_S5_MAX              0x409C /* NOT USED */
#define IDS_S5_MIN              0x409D /* NOT USED */
#define IDS_S5_RANGE            0x409E /* NOT USED */
#define IDS_S7_BLOCK            0x409F /* NOT USED */
#define IDS_S7_ELEMENT          0x40A0 /* NOT USED */
#define IDS_S7_MAX              0x40A1 /* NOT USED */
#define IDS_S7_MIN              0x40A2 /* NOT USED */
#define IDS_S7_RANGE            0x40A3 /* NOT USED */
#define IDS_SAVE_BROWSE_FILE    0x40A4
#define IDS_SECURITY            0x40A5
#define IDS_SELECTED_ADDRESS    0x40A6
#define IDS_SELECTED_FILE       0x40A7
#define IDS_SETTINGS            0x40A8
#define IDS_SIMO_CTRL           0x40A9
#define IDS_SIMO_FREQ           0x40AA
#define IDS_SIMO_PARAM          0x40AB
#define IDS_SIMO_REAL           0x40AC
#define IDS_SIMO_SEND           0x40AD
#define IDS_SIMO_STAT           0x40AE
#define IDS_SNP_ID_ERROR        0x40AF
#define IDS_SNP_ID_ERROR1       0x40B0
#define IDS_SPACE_BAB           0x40B1
#define IDS_SPACE_BABY          0x40B2
#define IDS_SPACE_BAL           0x40B3
#define IDS_SPACE_BAR           0x40B4
#define IDS_SPACE_BASE          0x40B5
#define IDS_SPACE_BAW           0x40B6
#define IDS_SPACE_BINARY        0x40B7
#define IDS_SPACE_BIT           0x40B8
#define IDS_SPACE_BYABY         0x40B9
#define IDS_SPACE_BYAL          0x40BA
#define IDS_SPACE_BYAR          0x40BB
#define IDS_SPACE_BYAW          0x40BC
#define IDS_SPACE_BYTE          0x40BD
#define IDS_SPACE_DECIMAL       0x40BE
#define IDS_SPACE_HEX           0x40BF
#define IDS_SPACE_LAL           0x40C0
#define IDS_SPACE_LAR           0x40C1
#define IDS_SPACE_LONG          0x40C2
#define IDS_SPACE_MIXED         0x40C3
#define IDS_SPACE_OCTAL         0x40C4
#define IDS_SPACE_RAR           0x40C5
#define IDS_SPACE_REAL          0x40C6
#define IDS_SPACE_WAL           0x40C7
#define IDS_SPACE_WAR           0x40C8
#define IDS_SPACE_WAW           0x40C9
#define IDS_SPACE_WORD          0x40CA
#define IDS_SPECIFY_MAXIMUM     0x40CB
#define IDS_SPECIFY_WHICH       0x40CC
#define IDS_STRING              0x40CD
#define IDS_STRING_REFERENCES   0x40CE
#define IDS_SUBELEMENT_SIZES    0x40CF
#define IDS_TAG_NAME_CONTAINS   0x40D0
#define IDS_THAT_ADDRESS_DOES   0x40D1
#define IDS_THAT_IS             0x40D2
#define IDS_THAT_NAME_IS        0x40D3
#define IDS_THIS_DEVICE         0x40D4
#define IDS_THIS_DEVICE_2       0x40D5
#define IDS_THIS_DIMENSIONAL    0x40D6
#define IDS_THIS_LIST_CAN_NOT   0x40D7
#define IDS_THIS_MAPPING        0x40D8
#define IDS_THIS_OPERATION      0x40D9
#define IDS_TOTAL_OF_DRIVER     0x40DA
#define IDS_TOTAL_OF_FMT        0x40DB
#define IDS_TYPE                0x40DC
#define IDS_TYPE_SUFFIX_MUST    0x40DD
#define IDS_UNABLE_TO_CREATE    0x40DE
#define IDS_UNABLE_TO_MAP       0x40DF
#define IDS_UNABLE_TO_OPEN      0x40E0
#define IDS_UNABLE_TO_OPEN_2    0x40E1
#define IDS_UNIQUE              0x40E2
#define IDS_UNSUPPORTED_FILE    0x40E3
#define IDS_VALID_VALUES_ARE    0x40E4
#define IDS_VALID_VALUES_ARE_2  0x40E5
#define IDS_VALID_VALUES_ARE_3  0x40E6
#define IDS_VERSION             0x40E7
#define IDS_WILL_BE_USED_TO     0x40E8
#define IDS_WRITE               0x40E9
#define IDS_WRITE_ONLY          0x40EA
#define IDS_YES                 0x40EB
#define IDS_YOU_ARE_ABOUT_TO    0x40EC
#define IDS_YOU_ARE_ABOUT_TO_2  0x40ED
#define IDS_YOU_ARE_ABOUT_TO_3  0x40EE

// End of File

#endif
