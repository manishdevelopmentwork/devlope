
#include "intern.hpp"

#include "emfxser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emfxser Driver
//

// Instantiator

INSTANTIATE(CEmfxserDriver);

// Constructor

CEmfxserDriver::CEmfxserDriver(void)
{
	m_Ident	 = DRIVER_ID; // 4067

	m_uTxPtr = 0;
	m_uRxPtr = 0;
	}

// Destructor

CEmfxserDriver::~CEmfxserDriver(void)
{
	}

// Configuration

void MCALL CEmfxserDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CEmfxserDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEmfxserDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmfxserDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx			= new CContext;

			m_pCtx->m_Drop		= GetByte(pData);

			m_pCtx->m_uWriteErrCt	= 0;
			m_pCtx->m_bTerminator	= CR;

			m_pCtx->m_pStrSrc[0]    = NULL;
			m_pCtx->m_pStrSrc[CAIR] = m_pCtx->m_pbAIR;
			m_pCtx->m_pStrSrc[CCBR] = m_pCtx->m_pbCBR;
			m_pCtx->m_pStrSrc[CDUR] = m_pCtx->m_pbDUR;
			m_pCtx->m_pStrSrc[CEQR] = m_pCtx->m_pbEQR;
			m_pCtx->m_pStrSrc[CIDR] = m_pCtx->m_pbIDR;
			m_pCtx->m_pStrSrc[CPIR] = m_pCtx->m_pbPIR;
			m_pCtx->m_pStrSrc[CQCR] = m_pCtx->m_pbQCR;
			m_pCtx->m_pStrSrc[CTMR] = m_pCtx->m_pbTMR;
			m_pCtx->m_pStrSrc[CTWR] = m_pCtx->m_pbTWR;
			m_pCtx->m_pStrSrc[CVUR] = m_pCtx->m_pbVUR;
			m_pCtx->m_pStrSrc[CERR] = m_pCtx->m_pbERR;

			for( UINT i = BEGSTRR; i <= ENDSTRR; i++ ) {

				PBYTE p = m_pCtx->m_pStrSrc[i];

				memset(p, 0, SZSTR);
				}

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmfxserDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmfxserDriver::Ping(void)
{
	DWORD Data[1];

	CAddress Addr;

	Addr.a.m_Table	= UINT('F');
	Addr.a.m_Offset	= ('S' - 'A') << 11;
	Addr.a.m_Type	= addrLongAsLong;
	Addr.a.m_Extra	= 0;

	m_uEchoTO	= 500;

	CCODE c		= Read(Addr, Data, 1);

	m_uEchoTO	= 20;

	if( c == 1 ) {

		BYTE bData = LOBYTE(Get(10));

		if( bData == LF || bData == CR ) {

			m_pCtx->m_bTerminator = bData;
			}
		}

	return c;
	}

CCODE MCALL CEmfxserDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	m_uID = FormID(Addr.a.m_Table, Addr.a.m_Offset);

	if( NoReadTransmit(Addr, pData, uCount) ) {

		return uCount;
		}

//**/	AfxTrace3("\r\nT=%x O=%x ID=%x ", Addr.a.m_Table, Addr.a.m_Offset, m_uID);
//**/    if( !IsStringItem(Addr.a.m_Table, Addr.a.m_Offset) ) {
//**/		AfxTrace2("\r\nACro=%c%c ", Addr.a.m_Table, 'A' + (Addr.a.m_Offset >> 11));
//**/		UINT uT = Addr.a.m_Offset & TYPMASK;
//**/		AfxTrace1("Type=%c ", uT == DATADEC ? 'D' : uT == DATABIN ? 'B' : 'H');
//**/		AfxTrace1("Par=%d ", Addr.a.m_Offset & 0xFF);
//**/		}

	StartFrame(Addr, DOQUERY, ADDSYM);

	return Transact() && GetResponse(pData, Addr.a.m_Offset) ? 1 : CheckResponse(FALSE);
	}

CCODE MCALL CEmfxserDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	m_uID = FormID(Addr.a.m_Table, Addr.a.m_Offset);

	if( NoWriteTransmit(Addr, pData, uCount) ) {

		return uCount;
		}

	UINT uT        = Addr.a.m_Table;
	UINT uO        = Addr.a.m_Offset;
	UINT uIsString = IsStringItem(uT, uO);

//**/	AfxTrace3("\r\n\n*** WRITE *** %x %x %x ", uT, uO, m_uID);
//**/		AfxTrace3("\r\n%8.8lx %c%c ", *pData, uT, 'A' + (uO >> 11));
//**/		uT = Addr.a.m_Offset & TYPMASK;
//**/		AfxTrace1("%c ", uT == DATADEC ? 'D' : uT == DATABIN ? 'B' : uT == DATAHEX ? 'H' : 'A');
//**/		AfxTrace2("Off=%d Par=%x ", uO & 0xFF, uO & PARBIT);

	if( !DoStringItem(Addr, pData) ) {

		if( !IsExeCmd() ) {

			AddWriteData(pData, uO);
			}
		}

	if( Transact() ) {

		if( uIsString == ISSCMD ) {

			StoreStringResponse();

			return uCount;
			}
		}

	return CheckResponse(TRUE);
	}

// Frame Building

BOOL CEmfxserDriver::DoStringItem(AREF Addr, PDWORD pData)
{
	if( !IsStringItem(Addr.a.m_Table, Addr.a.m_Offset) ) {

		StartFrame(Addr, DOWRITE, ADDSYM);

		return FALSE;
		}

	DWORD dData  = *pData;

	BOOL fDoRead = dData < 0x100;

	StartFrame(Addr, fDoRead, fDoRead); // if read, skip adding '?' or '=' in StartFrame

	if( !fDoRead ) {

		if( HIBYTE(HIWORD(dData)) < ' ' ) {

			*pData |= 0x20000000;
			}

		BYTE b;

		for( UINT i = 0; i < 16; i++ ) {

			dData = pData[i];

			UINT uShift = 32;

			for( UINT j = 0; j < 4; j++ ) {

				uShift -= 8;

				b = dData >> uShift;

				if( b >= ' ' && b <= 0x7F ) AddByte(b);

				else return TRUE;
				}
			}

		return TRUE;
		}

	if( fDoRead ) { // reading data

		char c[3] = {0};

		SPrintf( c, "%d", *pData ); // use data as parameter to read

		AddText(c); // add desired item number

		AddByte('?');
		}

	return TRUE;
	}

void CEmfxserDriver::StartFrame(AREF Addr, BOOL fIsQuery, BOOL fSkipOp)
{
	m_uTxPtr = 0;

	AddByte(m_pCtx->m_Drop);

	AddCommand(Addr.a.m_Table, Addr.a.m_Offset);

	if( !(fSkipOp || IsExeCmd()) ) {

		AddByte(fIsQuery ? '?' : '=');
		}
	}

void CEmfxserDriver::AddByte(BYTE bData)
{
	if( m_uTxPtr < sizeof(m_bTx) ) {

		m_bTx[m_uTxPtr++] = bData;
		}
	}

void CEmfxserDriver::AddText(char * sText)
{
	for( UINT i = 0; i < strlen(sText); i++ ) AddByte( sText[i] );
	}

void CEmfxserDriver::AddCommand(UINT uTable, UINT uOffset)
{
	AddByte(LOBYTE(uTable));

	BYTE bMod = GetModifier(uOffset);

	if( uTable != 'G' || bMod != 'F' ) { // special case, send 'G' only when GF selected

		AddByte(bMod);

		AddParameter(uOffset);
		}
	}

void CEmfxserDriver::AddParameter(UINT uOffset)
{
	if( uOffset & PARBIT ) {

		char c[5] = {0};

		SPrintf( c, "%d", uOffset & MAXPAR );

		AddText(c);
		}
	}

void CEmfxserDriver::AddWriteData(PDWORD pData, UINT uType)
{
	char c[12] = {0};

	uType &= TYPMASK;

	switch( uType ) {
	
		case DATAHEX:
			SPrintf( c, "%x", *pData );
			break;

		case DATABIN:
			c[0] = *pData & 1 ? '1' : '0';
			break;

		case DATADEC:
		default:
			SPrintf( c, "%d", *pData );
			break;
		}

	AddText(c);
	}

void CEmfxserDriver::EndFrame(void)
{
	AddByte(CR);
	}

// Transport Layer

BOOL CEmfxserDriver::Transact(void)
{
	EndFrame();

	Send();

	return GetReply();
	}

void CEmfxserDriver::Send(void)
{
	m_pData->ClearRx();

	memset(m_bRx, 0, 3);

	Put();
	}

BOOL CEmfxserDriver::GetReply(void)
{
	BYTE bEnd  = m_pCtx->m_bTerminator;

	UINT uTime = 1000;

	UINT uPtr  = m_uRxPtr;

	BOOL fEcho = m_fHasEcho;

	WORD wData;

	SetTimer(uTime);

//**/	AfxTrace0("\r\n");
//**/	AfxTrace2("{%x %x} <", m_pCtx->m_bTerminator, m_fHasEcho);
	while( (uTime = GetTimer()) ) {

		if( (wData = Get(uTime) ) == LOWORD(NOTHING) ) {
		
			Sleep(20);
			
			continue;
			}

		BYTE bData = LOBYTE(wData);

//**/		AfxTrace1("<%2.2x>", bData);

		m_bRx[uPtr++] = bData;

		if( bData == bEnd ) {

			if( !fEcho ) {

				m_uRxPtr = uPtr;

//**/				AfxTrace0(">");

				return TRUE;
				}

			fEcho = FALSE;
			}

		if( uPtr >= SZRX ) {

			break;
			}
		}

	m_uRxPtr = uPtr;

	SetERRString();

	return FALSE;
	}

BOOL CEmfxserDriver::GetResponse(PDWORD pData, UINT uType)
{
	UINT u    = 0;

	BYTE b    = m_bRx[u++];

	BOOL fFound = FALSE;

	while( !fFound && u < m_uRxPtr ) {

		fFound = m_bRx[u++] == '=';
		}

	if( u >= m_uRxPtr ) {

		switch( m_uID ) {

			case IDQU:
				u = m_uRxPtr - (m_pCtx->m_bTerminator == LF ? 3 : 2);
				break;

			default:
				return FALSE;
			}
		}

	uType &= TYPMASK;

	if( uType == DATADEC ) {

		*pData = DWORD(ATOI((char *)&m_bRx[u]));
		return TRUE;
		}

	DWORD d = 0;

	while( u < m_uRxPtr ) {

		b = m_bRx[u++];

		switch( uType ) {

			case DATAHEX:

				if( GetHex(&b) ) {

					d <<= 4;
					d  += b;
					}

				else {
					*pData = d;

					return TRUE;
					}
				break;

			case DATABIN:

				if( b == '0' || b == '1' ) {

					d <<= 1;
					d  += (b - '0');
					}

				else {
					*pData = d;

					return TRUE;
					}
				break;
			}
		}

	return FALSE;
	}

void  CEmfxserDriver::StoreStringResponse(void)
{
	PBYTE pDest;

	switch( m_uID ) {

		case IDAI:	pDest = m_pCtx->m_pbAIR;	break;
		case IDCB:	pDest = m_pCtx->m_pbCBR;	break;
		case IDDU:	pDest = m_pCtx->m_pbDUR;	break;
		case IDEQ:	pDest = m_pCtx->m_pbEQR;	break;
		case IDID:	pDest = m_pCtx->m_pbIDR;	break;
		case IDPI:	pDest = m_pCtx->m_pbPIR;	break;
		case IDQC:	pDest = m_pCtx->m_pbQCR;	break;
		case IDTM:	pDest = m_pCtx->m_pbTMR;	break;
		case IDTW:	pDest = m_pCtx->m_pbTWR;	break;
		case IDVU:	pDest = m_pCtx->m_pbVUR;	break;

		default: return;
		}

	memset(pDest, 0, SZSTR);

	if( !m_bRx[0] ) {

		memset(pDest, '-', 3);

		return;
		}

	memcpy(pDest, m_bRx, m_uRxPtr);
	}

CCODE CEmfxserDriver::CheckResponse(BOOL fIsWrite)
{
	UINT i = 0;

	BYTE b = m_bRx[i++];

	if( b ) {

		if( m_fHasEcho ) {

			while( b != m_pCtx->m_bTerminator && i < m_uTxPtr ) {

				b = m_bRx[i++];
				}
			}

		while( i < m_uRxPtr ) {

			switch( m_bRx[i] ) {

				case '?':
					if( m_bRx[0] == m_pCtx->m_Drop ) {

						return 1;
						}
					break;

				case '=':
				case '*':
					return 1;
				}

			i++;
			}

		if( fIsWrite ) {

			if( ++m_pCtx->m_uWriteErrCt >= 3 ) {

				m_pCtx->m_uWriteErrCt = 0;

				return 1;
				}
			}
		}

	SetERRString();

	return CCODE_ERROR;
	}

// Port Access

void CEmfxserDriver::Put(void)
{
	WORD wData;

	m_uRxPtr   = 0;

	UINT uRcv  = 0;

//**/	AfxTrace0("\r\n[");
//**/	for( UINT k = 0; k < m_uTxPtr; k++ ) AfxTrace1("%c", m_bTx[k] >= ' ' ? m_bTx[k] : m_bTx[k] + 'a');
//**/	AfxTrace0("]\r\n");

	m_pData->Write(m_bTx[0], FOREVER);

	m_fHasEcho = FALSE;

	if( (wData = Get(m_uEchoTO)) < LOWORD(NOTHING) ) {

		m_bRx[m_uRxPtr++] = LOBYTE(wData);

		m_fHasEcho        = TRUE;

//**/		AfxTrace1("<%c> ", m_bRx[0]);
		}

	m_pData->Write(&m_bTx[1], m_uTxPtr-1, FOREVER);
	}

UINT CEmfxserDriver::Get(UINT uTimer)
{
	return m_pData->Read(uTimer);
	}

// Internal access
BOOL CEmfxserDriver::NoReadTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uT     = Addr.a.m_Table;
	UINT uO     = Addr.a.m_Offset;

	UINT uIsStr = IsStringItem(uT, uO);

	if( uIsStr == ISSRSP ) {

		UINT u = uCount * sizeof(DWORD);

		memset(pData, 0, u);

		UINT i    = 0;
		UINT j    = 0;
		UINT uPos = 0;
		PBYTE p   = m_pCtx->m_pStrSrc[uT];
		UINT l    = strlen((const char *)p);
		DWORD d   = 0;

		for( i = 0; i < uCount; i++ ) {

			d = 0;

			uPos = i * 4;

			for( j = 0; j < 4; j++, uPos++ ) {

				d <<= 8;
				d += uPos < l && p[uPos] >= ' ' ? p[uPos] : 0;
				}

			pData[i] = d;
			}

		return TRUE;
		}

	if( uIsStr == ISSCMD ) {

		*pData = uO & PARBIT ? 0 : 0xFF;

		return TRUE;
		}

	if( IsExeCmd() ) {

		*pData = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CEmfxserDriver::NoWriteTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uT     = Addr.a.m_Table;
	UINT uO     = Addr.a.m_Offset;

	UINT uIsStr = IsStringItem(uT, uO);

	if( uIsStr == ISSRSP ) {

		memset(m_pCtx->m_pStrSrc[uT], 0, SZSTR);

		return TRUE;
		}

	if( uIsStr == ISSCMD ) {

		return !(BOOL(*pData) || !(uO & PARBIT));
		}

	if( IsExeCmd() ) {

		return BOOL(!*pData );
		}

	return FALSE;
	}

// Helpers
BYTE CEmfxserDriver::GetModifier(UINT uOffset)
{
	return 'A' + (uOffset >> 11);
	}

UINT CEmfxserDriver::IsStringItem(UINT uTable, UINT uOffset)
{
	if( uTable >= BEGSTRR && uTable <= ENDSTRR ) {

		return ISSRSP;
		}

	switch( m_uID ) {

		case IDAI:
		case IDCB:
		case IDDU:
		case IDEQ:
		case IDID:
		case IDPI:
		case IDQC:
		case IDTM:
		case IDTW:
		case IDVU:
			return ISSCMD;
		}

	return ISSNOT;
	}

BOOL CEmfxserDriver::IsExeCmd(void)
{
	switch( m_uID ) {

		case IDCL:
		case IDHM:
		case IDRR:
		case IDRS:
		case IDSJ:
		case IDST:
		case IDZR:
			return TRUE;
		}

	return FALSE;
	}

UINT CEmfxserDriver::FormID(UINT uTable, UINT uOffset)
{
	return (uTable << 8) + GetModifier(uOffset);
	}

BOOL CEmfxserDriver::GetHex(PBYTE pb)
{
	BYTE b = *pb;

	if( b >= '0' && b <= '9' ) {

		*pb = b - '0';
		return TRUE;
		}

	if( b >= 'A' && b <= 'F' ) {

		*pb = b - '7';
		return TRUE;
		}

	if( b >= 'a' && b <= 'f' ) {

		*pb = b - 'W';
		return TRUE;
		}

	return FALSE;
	}

void CEmfxserDriver::SetERRString(void)
{
	PBYTE p  = m_pCtx->m_pStrSrc[CERR];

	UINT uCt = 0;

	memset(p, 0, SZSTR);

	if( !m_fHasEcho || !m_bRx[0] ) {

		uCt = m_uTxPtr;

		memcpy(p, m_bTx, uCt);

		p[uCt++] = ' ';

		p       += uCt;
		}

	if( !m_bRx[0] ) {

		memset(p, '?', 4);

		return;
		}

	memcpy(p, m_bRx, min(m_uRxPtr, SZSTR - uCt));
	}

// End of File
