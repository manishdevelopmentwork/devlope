
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Canyon CA07 Model Data
//

static BYTE imageCA07[] = {

	#include "ca07-x1.png.dat"
	0
	};

global CHostModel modelCA07 = {

	"",
	"CA07",
	"CR3000-07",
	"CR3000-07000",
	rfCanyon,
	1,
	988,
	758,
	94,
	124,
	800,
	480,
	0,
	NULL,
	sizeof(imageCA07)-1,
	imageCA07
	};

// End of File
