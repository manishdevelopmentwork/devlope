
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Display Format
//

// Constructor

CDispFormatMulti::CDispFormatMulti(void)
{
	m_pList    = New CDispFormatMultiList;

	m_pLimit   = NULL;

	m_pDefault = NULL;

	m_Range    = 0;
	}

// Destructor

CDispFormatMulti::~CDispFormatMulti(void)
{
	delete m_pList;

	delete m_pLimit;

	delete m_pDefault;
	}

// Initialization

void CDispFormatMulti::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatMulti", pData);

	GetCoded(pData, m_pLimit);

	GetCoded(pData, m_pDefault);

	m_Range = GetByte(pData);

	m_pList->Load(pData);
	}

// Type Checking

BOOL CDispFormatMulti::IsMulti(void)
{
	return TRUE;
	}

// Scan Control

BOOL CDispFormatMulti::IsAvail(void)
{
	return m_pList->IsAvail();
	}

BOOL CDispFormatMulti::SetScan(UINT Code)
{
	return m_pList->SetScan(Code);
	}

// Formatting

CUnicode CDispFormatMulti::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		return L"";
		}

	if( Type == typeVoid ) {

		CUnicode d = m_pDefault->GetText();

		UINT     c = GetCount();

		UINT     m = d.GetLength();

		for( UINT n = 0; n < c; n++ ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(n);

			if( pItem ) {

				if( pItem->m_pText ) {

					CUnicode t = pItem->m_pText->GetText();

					UINT     l = t.GetLength();

					MakeMax(m, l);
					}
				}
			}

		return CUnicode('-', min(5, m));
		}

	if( Type == typeInteger || Type == typeReal ) {

		CUnicode d = m_pDefault->GetText();

		UINT     c = GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(c - 1 - n);
			
			if( pItem ) {
				
				if( pItem->MatchData(Data, Type, m_Range, d) ) {
					
					return d;
					}
				}
			}

		if( d.IsEmpty() ) {

			d = GeneralFormat(Data, Type, Flags);

			d = L'(' + d + L')';
			}
		
		return d;
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Parsing

BOOL CDispFormatMulti::Parse(DWORD &Data, CString Text, UINT Type)
{
	if( Type == typeInteger || Type == typeReal ) {

		UINT c = GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(n);

			if( pItem->m_pData ) {

				CString Test = UniConvert(pItem->m_pText->GetText());

				if( Test == Text ) {

					Data = pItem->m_pData->Execute(Type);

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	return GeneralParse(Data, Text, Type);
	}

// Presentation

CString CDispFormatMulti::GetStates(void)
{
	CString States;

	UINT c = GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CDispFormatMultiEntry *pItem = m_pList->GetItem(n);

		if( pItem->m_pData ) {

			CString Text = UniConvert(pItem->m_pText->GetText());

			Text.Replace("&", "&amp;");
			Text.Replace(",", "&sep;");
			Text.Replace("<", "&lt;" );
			Text.Replace(">", "&gt;" );

			if( !States.IsEmpty() ) {

				States += ",";
				}

			States += Text;
			}
		}
	
	return States;
	}

// Editing

UINT CDispFormatMulti::Edit(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == 0x00 ) {

		if( pEdit->m_uFlags & flagTwoMulti ) {

			pEdit->m_uKeypad  = keypadNudge1;
			}
		else
			pEdit->m_uKeypad  = keypadNudge2;

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		return editUpdate;
		}

	if( uCode == 0x1B ) {

		if( pEdit->m_uFlags & flagTwoMulti ) {

			if( !pEdit->m_fDefault ) {

				pEdit->m_uCursor  = cursorAll;

				pEdit->m_fDefault = TRUE;

				return editUpdate;
				}
			}

		return editAbort;
		}

	if( uCode == 0x0D ) {

		if( pEdit->m_uFlags & flagTwoMulti ) {

			if( !pEdit->m_fDefault ) {

				DWORD Data = pEdit->m_pValue->Execute(pEdit->m_Type);

				if( pEdit->m_Data == Data ) {

					return editAbort;
					}

				if( pEdit->m_pValue->SetValue(pEdit->m_Data, pEdit->m_Type, setNone) ) {

					return editCommit;
					}

				return editUpdate;
				}

			return editAbort;
			}

		return editNone;
		}

	if( uCode == 0x0B ) {

		UINT uCount = GetCount();

		UINT uFind  = m_pList->FindData(pEdit->m_Data);

		UINT uNext  = m_pList->FindNext(uFind, uCount);

		while( uNext < NOTHING ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(uNext);

			DWORD                  Data  = pItem->m_pData->ExecVal();

			if( Validate(pEdit, Data) ) {

				if( pEdit->m_uFlags & flagTwoMulti ) {

					pEdit->m_Data     = Data;

					pEdit->m_Edit     = Format(pEdit->m_Data, pEdit->m_Type, fmtStd);

					pEdit->m_uCursor  = cursorLast;

					pEdit->m_fDefault = FALSE;
					}
				else {
					UINT Type = pItem->m_pData->GetType();

					pEdit->m_pValue->SetValue(Data, Type, setDirect);
					}

				return editUpdate;
				}

			uFind  = m_pList->FindData(Data);

			uNext  = m_pList->FindNext(uFind, uCount);
			}

		return editNone;
		}

	if( uCode == 0x0A ) {

		UINT uCount = GetCount();

		UINT uFind  = m_pList->FindData(pEdit->m_Data);

		UINT uPrev  = m_pList->FindPrev(uFind, uCount);

		while( uPrev < NOTHING ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(uPrev);

			DWORD                  Data  = pItem->m_pData->ExecVal();

			if( Validate(pEdit, Data) ) {

				if( pEdit->m_uFlags & flagTwoMulti ) {

					pEdit->m_Data     = Data;

					pEdit->m_Edit     = Format(pEdit->m_Data, pEdit->m_Type, fmtStd);

					pEdit->m_uCursor  = cursorLast;

					pEdit->m_fDefault = FALSE;
					}
				else {
					UINT Type = pItem->m_pData->GetType();

					pEdit->m_pValue->SetValue(Data, Type, setDirect);
					}

				return editUpdate;
				}

			uFind  = m_pList->FindData(Data);

			uPrev  = m_pList->FindPrev(uFind, uCount);
			}

		return editNone;
		}

	return editNone;
	}

// Limit Access

DWORD CDispFormatMulti::GetMin(UINT Type)
{
	if( Type == typeInteger || Type == typeReal ) {

		DWORD m = 0;

		BOOL  f = TRUE;

		UINT  c = GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(n);
			
			if( pItem && pItem->m_pData ) {

				if( f ) {

					m = pItem->m_pData->ExecVal();
					
					f = FALSE;
					}
				else {
					// REV3 -- Not very efficient...

					if( pItem->CompareData(m, Type) < 0 ) {
						
						m = pItem->m_pData->ExecVal();
						}
					}
				}
			}

		return m;
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatMulti::GetMax(UINT Type)
{
	if( Type == typeInteger || Type == typeReal ) {

		DWORD m = 0;

		BOOL  f = TRUE;

		UINT  c = GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(n);
			
			if( pItem && pItem->m_pData ) {

				if( f ) {

					m = pItem->m_pData->ExecVal();
					
					f = FALSE;
					}
				else {
					// REV3 -- This is not very efficient.

					if( pItem->CompareData(m, Type) > 0 ) {
						
						m = pItem->m_pData->ExecVal();
						}
					}
				}
			}

		return m;
		}

	return CDispFormat::GetMax(Type);
	}

// Implementation

UINT CDispFormatMulti::GetCount(void)
{
	UINT uCount = m_pList->GetItemCount();

	if( m_pLimit ) {

		UINT uLimit = m_pLimit->ExecVal();

		return max(1, min(uLimit, uCount));
		}

	return uCount;
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi-State List
//

// Constructor

CDispFormatMultiList::CDispFormatMultiList(void)
{
	m_uCount = 0;

	m_pEntry = NULL;
	}

// Destructor

CDispFormatMultiList::~CDispFormatMultiList(void)
{
	delete [] m_pEntry;
	}

// Initialization

void CDispFormatMultiList::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatMultiList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_pEntry = New CDispFormatMultiEntry [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			m_pEntry[n].Load(pData);
			}
		}
	}

// Item Access

CDispFormatMultiEntry * CDispFormatMultiList::GetItem(UINT uPos) const
{
	return m_pEntry + uPos;
	}

// Scan Control

BOOL CDispFormatMultiList::IsAvail(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDispFormatMultiEntry *pItem = GetItem(n);
		
		if( pItem ) {

			if( !pItem->IsAvail() ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CDispFormatMultiList::SetScan(UINT Code)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDispFormatMultiEntry *pItem = GetItem(n);
		
		if( pItem ) {

			pItem->SetScan(Code);
			}
		}

	return TRUE;
	}

// Attributes

UINT CDispFormatMultiList::GetItemCount(void) const
{
	return m_uCount;
	}

// Navigation

UINT CDispFormatMultiList::FindData(DWORD Data)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDispFormatMultiEntry *pItem = GetItem(n);
		
		if( pItem && pItem->m_pData ) {

			DWORD Find = pItem->m_pData->ExecVal();

			if( Find == Data ) {
		
				return n;
				}
			}
		}

	return NOTHING;
	}

UINT CDispFormatMultiList::FindNext(UINT uSlot, UINT uCount)
{
	if( uSlot < NOTHING ) {

		if( uSlot == uCount - 1 ) {

			return NOTHING;
			}

		uSlot = uSlot + 1;
		}
	else
		uSlot = 0;

	for( UINT n = uSlot; n < uCount; n++ ) {

		CDispFormatMultiEntry *pItem = GetItem(n);
		
		if( pItem && pItem->m_pData ) {

			return n;
			}
		}

	return NOTHING;
	}

UINT CDispFormatMultiList::FindPrev(UINT uSlot, UINT uCount)
{
	if( uSlot < NOTHING ) {

		if( uSlot == 0 ) {

			return NOTHING;
			}

		uSlot = uSlot - 1;
		}
	else
		uSlot = uCount - 1;

	for( UINT n = uSlot; n < NOTHING; n-- ) {

		CDispFormatMultiEntry *pItem = GetItem(n);
		
		if( pItem && pItem->m_pData ) {

			return n;
			}
		}

	return NOTHING;
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Entry
//

// Constructor

CDispFormatMultiEntry::CDispFormatMultiEntry(void)
{
	m_pData = NULL;

	m_pText = NULL;
	}

// Destructor

CDispFormatMultiEntry::~CDispFormatMultiEntry(void)
{
	delete m_pData;

	delete m_pText;
	}

// Initialization

void CDispFormatMultiEntry::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatMultiEntry", pData);

	GetCoded(pData, m_pData);

	GetCoded(pData, m_pText);
	}

// Scan Control

BOOL CDispFormatMultiEntry::IsAvail(void)
{
	if( !IsItemAvail(m_pText) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pData) ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CDispFormatMultiEntry::SetScan(UINT Code)
{
	SetItemScan(m_pText, Code);

	SetItemScan(m_pData, Code);

	return TRUE;
	}

// Data Matching

BOOL CDispFormatMultiEntry::MatchData(DWORD Data, UINT Type, BOOL fRange, CUnicode &Text)
{
	if( m_pData ) {

		DWORD Test = m_pData->Execute(Type);

		if( fRange ) {

			if( Type == typeInteger ) {

				if( INT(Data) >= INT(Test) ) {

					Text = m_pText->GetText();

					return TRUE;
					}
				}

			if( Type == typeReal ) {

				if( I2R(Data) >= I2R(Test) ) {

					Text = m_pText->GetText();

					return TRUE;
					}
				}

			return FALSE;
			}

		if( Test == Data ) {

			Text = m_pText->GetText();
			
			return TRUE;
			}
		}

	return FALSE;
	}

// Data Testing

int CDispFormatMultiEntry::CompareData(DWORD Data, UINT Type)
{
	if( m_pData ) {

		DWORD Test = m_pData->ExecVal();

		if( Type == typeInteger ) {

			if( C3INT(Test) < C3INT(Data) ) {
				
				return -1;
				}

			if( C3INT(Test) > C3INT(Data)  ) {
				
				return +1;
				}
			}

		if( Type == typeReal ) {

			if( I2R(Test) < I2R(Data) ) {
				
				return -1;
				}

			if( I2R(Test) > I2R(Data)  ) {
				
				return +1;
				}
			}		
		}

	return 0;
	}

// End of File
