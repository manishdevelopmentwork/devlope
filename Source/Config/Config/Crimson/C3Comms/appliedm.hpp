
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_APPLIEDMOTION_HPP
	
#define	INCLUDE_APPLIEDMOTION_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Forward Declarations
//

class CAppliedMotionDriver;
class CAppliedMotionAddrDialog;

#define	LL	addrLongAsLong
#define	RR	addrRealAsReal
#define	AN	addrNamed

// Register Table Spaces
#define	RGD	 1 // Register Decrement
#define	RGI	 2 // Register Increment
#define	RGL	 3 // Register Load Immediate
#define	RGM	 4 // Register Move
#define	RGR	 5 // Register Read
#define	RGW	 6 // Register Write
#define	RGX	 7 // Register Load Buffered

// Commands involving High/Low/Rising/Falling
#define	FEB	 8 // Follow Encoder Buffered
#define	FEX	 9 // Follow Encoder Immediate
#define	FMB	10 // Feed to Sensor, Mask Distance Buf
#define	FMX	11 // Feed to Sensor, Mask Distance Imm.
#define	FSB	12 // Feed to Sensor, Buffered
#define	FSX	13 // Feed to Sensor, Immediate
#define	FYB	14 // Feed to Sensor-Safety Dist. Buf.
#define	FYX	15 // Feed to Sensor Safety Dist. Imm.
#define	HWB	16 // Hand Wheel, Buffered
#define	HWX	17 // Hand Wheel, Immediate
#define	RCB	18 // Register Counter, Buffered
#define	RCX	19 // Register Counter, Immediate
#define	SHB	20 // Seek Home, Buffered
#define	SHX	21 // Seek Home, Immediate

#define	FIR	22 // Filter Input (range)
#define	IAV	23 // Immediate Analog
#define	SOU	24 // Set Output
#define	SOY	25 // Set Output Y
#define	FOL	26 // Feed to Length + Set Out
#define	FOY	27 // Feed to Length + Set Out, Y
#define	RST	28 // Request Status
#define	FDA	29 // Feed Double Sensor, Input 1
#define	FDB	30 // Feed Double Sensor, Condition 1
#define	FDC	31 // Feed Double Sensor, Input 2
#define	FDD	32 // Feed Double Sensor, Condition 2

// New offering post-June 08
#define	ALLOWSGLANDQRSP	FALSE
#define	SGL	33 // Send User-defined String

// Not User Selectable, used in runtime
#define	PRO	0xF0 // Protocol setting
#define	IMF	0xF1 // Immediate Data Format

// Header Table Definitions
#define	FNCMOT	201 // Motion Commands
#define	FNCCON	202 // Configuration Commands
#define	FNCIO	203 // I/O Commands
#define	FNCCOM	204 // Communication Commands
#define	FNCQ	205 // Q Program Commands
#define	FNCDRV	206 // Drive Commands
#define	FNCREG	207 // Register Commands
#define	FNCIMM	208 // Immediate Commands

// Info Table Definitions
#define	INFO0	221
#define	INFO1	222
#define	INFO2	223
#define	INFO3	224
#define	ENDINFO	INFO3

// List Bits
#define	LST01	0x01
#define	LST02	0x02
#define	LST04	0x04
#define	LST08	0x08
#define	LST10	0x10
#define	LST20	0x20
#define	LST40	0x40
#define	LST80	0x80

// List Bits for Extra
#define	EXTMOT	1
#define	EXTCON	2
#define	EXTIO	3
#define	EXTCOM	4
#define	EXTQ	5
#define	EXTDRV	6
#define	EXTREG	7
#define	EXTIMM	8

// List Numbers
#define	LSTMOT	(LST01 << 8)
#define	LSTCON	(LST02 << 8)
#define	LSTIO	(LST04 << 8)
#define	LSTCOM	(LST08 << 8)
#define	LSTQ	(LST10 << 8)
#define	LSTDRV	(LST20 << 8)
#define	LSTREG	(LST40 << 8)
#define	LSTIMM	(LST80 << 8)
#define	LSTALL	0xFF00

// Parse Codes
#define	ISNOTLETTER	0
#define	ISREGLETTER	1
#define	ISINPLETTER	2

//////////////////////////////////////////////////////////////////////////
//
// Applied Motion Device Options
//

class CAppliedMotionDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAppliedMotionDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		CString	m_Drop;
		BOOL	m_fPM2Disable;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Helper
		BOOL InvalidDropChar(TCHAR c);
	};

//////////////////////////////////////////////////////////////////////////
//
// Applied Motion Comms Driver
//

class CAppliedMotionDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CAppliedMotionDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Dialog Support
		void	AddHeaderSpaces(UINT uTop);
		void	AddDialogSpaces(BOOL fPart);
		void	SetListSelect(UINT *pListSelect);
		UINT	GetListFromTable(UINT uTable, UINT uOffset);

	protected:
		// Implementation
		void	AddMotionSpaces(void);
		void	AddConfigSpaces(void);
		void	AddIOSpaces(void);
		void	AddCommSpaces(void);
		void	AddQSpaces(void);
		void	AddDriveSpaces(void);
		void	AddRegisterSpaces(void);
		void	AddImmediateSpaces(void);
		void	AddSpaces(void);

		// Helpers
		void	ValidateListSelect(UINT uTable, UINT uOffset);
		UINT	IsLetterSelect(CSpace *pSpace);
		UINT	SetListForNamed(UINT uOffset);

		// Data
		UINT	m_uListSelect;
	};

//////////////////////////////////////////////////////////////////////////
//
//  Applied Motion Selection Dialog
//

class CAppliedMotionAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAppliedMotionAddrDialog(CAppliedMotionDriver &Driver, CAddress &Addr, BOOL fPart);

		// Destructor
		~CAppliedMotionAddrDialog(void);
		                
	protected:
		CAppliedMotionDriver * m_pDriverData;

		UINT	m_CurrentList;
		BOOL	m_fListTypeChg;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// List Selection/Display
		void	LoadList(void);
		void	LoadEntry(CListBox *pBox, CString Prefix, CString Caption, DWORD n);
		void	DoDesignatedHeader(CListBox *pBox, CSpaceList &List, INDEX n);		
		INDEX	DoSelection(CListBox *pBox, CSpaceList &List, INDEX n);
		void	DoRemainingHeaders(CListBox *pBox, CSpaceList &List, INDEX n);
		BOOL	MatchList(CSpace *pSpace);
		BOOL	IsInfo(UINT uTable);

 		// Other Overridables
		BOOL	AllowSpace(CSpace * pSpace);

		// Helpers
		BOOL	IsHeader(UINT uTable);
		BOOL	IsFuncHeader(UINT uTable);
		void	SetAllowSpace(UINT uTable);
		UINT	GetListFromTable(UINT uTable, UINT uOffset);
		UINT	GetListFromHeader(UINT uTable);
		UINT	GetListFromExtra(UINT uExtra);
		void	PickSpaces(void);
		void	DoCurrentList(UINT uList);
		UINT	SetExtra(void);
		void	SCLShowAddress(CAddress Addr);
	};

// End of File

#endif
