
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpClientAuth_HPP

#define	INCLUDE_HttpClientAuth_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Authentication Method
//

class DLLAPI CHttpClientAuth : public CHttpAuth
{
	public:
		// Operations
		virtual void	Reset(void);
		virtual void	SetCredentials(CString User, CString Pass);
		virtual BOOL	CanAccept(CString Meth, UINT &uPriority);
		virtual BOOL    ProcessRequest(CString Line, BOOL fFail);
		virtual BOOL    ProcessInfo(CString Line, CString Path, PCBYTE pBody, UINT uBody);
		virtual CString GetAuthHeader(CString Verb, CString Path, CBytes Body);

	protected:
		// Data Members
		UINT    m_uState;
		CString	m_User;
		CString	m_Pass;
	};

// End of File

#endif
