
//////////////////////////////////////////////////////////////////////////
//
// HBM FIT Driver
//

#define HBMFIT_ID	0x4013

// Response Types
#define	RESPONSEFAIL	0
#define	NORESPONSE	1
#define	READRESPONSE	2
#define	WRITERESPONSE	3
#define	LONGRESPONSE	4
#define	ASCIIRESPONSE	5
#define	MULTIRESPONSE	6
#define	HEXRESPONSE	7

#define	LONGTIMEOUT	20 // some commands take up to 5 seconds to respond.

// Password Controls
#define	PASSWORDNOTSET	0
#define	PASSWORDONESET	1
#define	PASSWORDTWOSET	2
#define	PASSWORDALLSET	3

// Parameter Size Types
#define	TNONE		0
#define	TBOOL		1
#define	TINT		2
#define	TSTRING		3

// Port Selection
#define	IS2WIRE		0
#define IS232		1
#define	IS4WIRE		2
#define	ISETHPORT	3
#define	IS20MPORT	4
#define NOPORT		5

// m_COF Definitions
#define	BIN4WIRE	0
#define	ASCNOT2WIRE	3
#define	BIN2WIRE	64
#define	ASC2WIRE	67

#define MAVNODATA	0x80000000

struct FAR HBMFITCmdDef {
	char	Com[5];
	UINT	Table;
	};

class CHBMFITDriver : public CMasterDriver
{
	public:
		// Constructor
		CHBMFITDriver(void);

		// Destructor
		~CHBMFITDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_Password[7];
			DWORD	m_dMAVCache;
			};

		CContext *	m_pCtx;

		// Data Members

		BYTE	m_bTx[64];
		BYTE	m_bRx[64];
		UINT	m_uPtr;
		UINT	m_bCheck;
		UINT	m_COF;
		UINT	m_LongResponseCounter;
		DWORD	m_LIV1[4];
		DWORD	m_LIV2[4];
		BYTE	m_SetPW[7];
		DWORD	m_TRC[5];
		BYTE	m_PreviousAddress;
		UINT	m_uPhysical;

		//
		// List access
		static HBMFITCmdDef CODE_SEG CL[];
		HBMFITCmdDef	FAR * m_pCL;
		HBMFITCmdDef	FAR * m_pItem;

		// Hex Lookup
		LPCTXT	m_pHex;
	
		// Opcode Handler
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, DWORD dFactor);
		void	AddCommand(void);
		void	PutText(LPCTXT pCmd);
		UINT	PutReadInfo(void);
		UINT	PutWriteInfo(DWORD dData);
		
		// Transport Layer
		BOOL	Transact(UINT uResponseType);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	GetResponse(PDWORD pData, UINT uResponseType);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		void	SelectDevice(void);
		BOOL	SetCOF(void);
		BOOL	InvalidCOF(DWORD dData);
		BOOL	NoReadTransmit(PDWORD pData);
		BOOL	NoWriteTransmit(DWORD dData);
		UINT	GetParameterSize(void);
		void	SetpItem(UINT uID);
		void	PutGeneric(DWORD dData);
		BOOL	GetGeneric(UINT * pPos, PDWORD pData);
		DWORD	GetAscii(UINT * pPos);
		void	StorePassword(DWORD dData, PBYTE pb, UINT uCt);
		void	GetPassword( PDWORD pData, PBYTE pb, UINT uCt);
		UINT	IsValidPWChar(BYTE b);
		BOOL	IsValidPW(UINT * pPos, PBYTE pPW);
		void	ClearPW(PBYTE pPW);
		BOOL	AddENU(DWORD dData);
		BOOL	DataToASCII(DWORD dData, UINT uCt);
		DWORD	CheckSign(DWORD dData);
		BOOL	NeedPlus(void);
		BOOL	IsMeasurementRequest(void);
		void	Dbg(UINT u, DWORD d);

	};
	
#define ACL	1  // Not in FIT
#define ADR	2
#define ASF	3
#define ASS	4  // Not in FIT
#define	BRK	89 // FIT Specific
#define CAL	5  // Not in FIT
#define	CBK	90 // FIT Specific
#define	CFD	91 // FIT Specific
#define COF	6
#define CRC	7
#define	CSN	92 // FIT Specific
#define CWN	8
#define	CWL	87
#define DP1	9
#define DP2	10

#define	DPW	11
#define ENU	12
#define	EPT	93 // FIT Specific
#define ESR	13
#define	EWT	94 // FIT Specific
#define	FBK	95 // FIT Specific
#define	FFD	96 // FIT Specific
#define	FFM	97 // FIT Specific
#define FMD	14
#define	FRS	98 // FIT Specific
#define	FWT	99 // FIT Specific
#define GRU	15 // Not in FIT
#define ICR	16
#define ID0	17
#define ID1	18
#define ID2	19
#define ID3	20

#define ID4	21
#define ID5	22
#define ID6	23
#define ID7	24
#define ID8	25
#define IMD	26
#define LDC	27
#define	LDV	28
#define LFT	29
#define	LC0	30

#define	LC1	31
#define	LC2	32
#define	LC3	33
#define	LR1M	34
#define	LR1I	35
#define	LR1O	36
#define	LR1F	37
#define	LR2M	38
#define	LR2I	39
#define	LR2O	40

#define	LR2F	41
#define	LW1M	42
#define	LW1I	43
#define	LW1O	44
#define	LW1F	45
#define	LW2M	46
#define	LW2I	47
#define	LW2O	48
#define	LW2F	49
#define LIV	50

#define	LTC	100 // FIT Specific
#define	LTF	101 // FIT Specific
#define	LTL	102 // FIT Specific
#define LWC	51
#define LWV	52
#define MAV	53
#define MSV	54
#define MTD	55
#define	NDS	103 // FIT Specific
#define NOV	56
#define	OMD	104 // FIT Specific
#define	OSN	105 // FIT Specific
#define PW1	57
#define PW2	88
#define POR	58
#define RES	59
#define	RFT	106 // FIT Specific
#define	RSN	107 // FIT Specific
#define	RUN	108 // FIT Specific
#define	SDO	109 // FIT Specific
#define SEL	60

#define SFC	61  // Not in FIT
#define SFV	62  // Not in FIT
#define SP1	63
#define SP2	64
#define SPW	65
#define STR	66
#define	STT	110 // FIT Specific
#define	SUM	111 // FIT Specific
#define	SYD	112 // FIT Specific
#define SZC	67  // Not in FIT
#define SZV	68  // Not in FIT
#define	TAD	113 // FIT Specific
#define TAR	69
#define TAS	70

#define TAV	71
#define TCR	72
#define TDD	73
#define	TMD	114 // FIT Specific
#define TR1	74
#define TR2	75
#define TR3	76
#define TR4	77
#define TR5	78
#define TW1	79
#define TW2	80

#define TW3	81
#define TW4	82
#define TW5	83
#define TRC	84
#define	UTL	115 // FIT Specific
#define ZSE	85
#define ZTR	86

#define TEX	120 // not user accessible


// End of File
