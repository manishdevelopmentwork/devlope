
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Waitable Object Class
//

// Dynamic Class

AfxImplementDynamicClass(CWaitable, CWinObject);

// Constructors

CWaitable::CWaitable(void)
{
	}

CWaitable::CWaitable(CWinObject const &That) : CWinObject(That)
{
	}

CWaitable::CWaitable(CWaitable const &That) : CWinObject(That)
{
	}

CWaitable::CWaitable(HANDLE hObject) : CWinObject(hObject)
{
	}

// Destructor

CWaitable::~CWaitable(void)
{
	}

// Assignment Operators

CWaitable const & CWaitable::operator = (CWaitable const &That)
{
	Close();

	HANDLE hProcess = GetCurrentProcess();

	DuplicateHandle( hProcess, 
			 That.m_hObject,
			 hProcess, 
			 &m_hObject,
			 0,
			 FALSE,
			 DUPLICATE_SAME_ACCESS
			 );

	return ThisObject;
	}

CWaitable const & CWaitable::operator = (HANDLE hObject)
{
	Close();

	m_hObject = hObject;

	return ThisObject;
	}

// Operations

UINT CWaitable::WaitForObject(DWORD Timeout) const
{
	if( m_hObject ) {

		DWORD r = WaitForSingleObject(m_hObject, Timeout);

		switch( r ) {

			case WAIT_OBJECT_0:
			case WAIT_ABANDONED:
				
				return waitSignal;

			case WAIT_TIMEOUT:
				
				return waitTimeout;
			}
		}

	AfxTrace(L"ERROR: Failed on wait for object\n");

	return waitFailed;
	}

UINT CWaitable::WaitForObjectAndMsg(DWORD Timeout) const
{
	if( m_hObject ) {

		DWORD r = MsgWaitForMultipleObjects( 1,
						     (void **) &m_hObject,
						     TRUE,
						     Timeout,
						     QS_ALLINPUT
						     );

		switch( r ) {

			case WAIT_OBJECT_0:
			case WAIT_OBJECT_0 + 1:
			case WAIT_ABANDONED:
				
				return waitSignal;

			case WAIT_TIMEOUT:
				
				return waitTimeout;
			}
		}

	AfxTrace(L"ERROR: Failed on wait for object\n");

	return waitFailed;
	}

UINT CWaitable::WaitForObjectOrMsg(DWORD Timeout) const
{
	if( m_hObject ) {

		DWORD r = MsgWaitForMultipleObjects( 1,
						     (void **) &m_hObject,
						     FALSE,
						     Timeout,
						     QS_ALLINPUT
						     );

		switch( r ) {

			case WAIT_OBJECT_0:
			case WAIT_ABANDONED:
				
				return waitSignal;
				
			case WAIT_OBJECT_0 + 1:

				return waitMessage;

			case WAIT_TIMEOUT:

				return waitTimeout;
			}
		}

	AfxTrace(L"ERROR: Failed on wait for object\n");

	return waitFailed;
	}

// End of File
