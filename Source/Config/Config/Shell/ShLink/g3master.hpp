
//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3MASTER_HPP
	
#define	INCLUDE_G3MASTER_HPP

////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3link.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class ILinkTransport;
class CLoaderBase;
class CBootLoader;
class CPromLoader;
class CTunnelKing;
class CConfigLoader;
class CDataAccess;
class CStratonOnline;
class CCrimsonAuth;

//////////////////////////////////////////////////////////////////////////
//
// Transport Types
//

enum
{
	typeNone,
	typeSerial,
	typeUSB,
	typeNetwork,
	
	};

//////////////////////////////////////////////////////////////////////////
//
// Device Transitions
//

enum
{
	transNone,
	transBootToApp,
	transAppToBoot,
	transAppToApp,
	
	};

//////////////////////////////////////////////////////////////////////////
//
// Device Product Idents
//

#define PID_TESTER	0x0001
#define	PID_LOADER	0x0100
#define PID_HMI		0x0200
#define PID_TYPE_MASK	0x0300

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Transport
//

class ILinkTransport
{
	public:
		// Deletion
		virtual void Release(void)	 = 0;

		// Management
		virtual BOOL Open(void)		 = 0;
		virtual void Recycle(UINT uType) = 0;
		virtual void Close(void)	 = 0;
		virtual void Terminate(void)	 = 0;

		// Attributes
		virtual UINT    GetType(void)		 const = 0; 
		virtual UINT    GetBulkLimit(BOOL fDown) const = 0;
		virtual CString GetErrorText(void)	 const = 0;

		// Transport
		virtual BOOL Transact(CLinkFrame &Req, CLinkFrame &Rep, BOOL fExpectReply) = 0;

		// Notifications
		virtual void OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;
		virtual void OnBind(HWND hWnd)		                      = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// G3 Loader Base Class
//

class CLoaderBase
{
	public:
		// Constructor
		CLoaderBase(void);
		
		// Binding
		void Bind(ILinkTransport *pTrans, BOOL fAsync);

		// Attributes
		CString GetErrorText(void) const;
		BOOL    DidAuthFail(void) const;

		// Comms Operations
		virtual BOOL Transact(void);

	protected:
		// Data Members
		ILinkTransport * m_pTrans;
		BOOL		 m_fAsync;
		CString		 m_Error;
		BOOL		 m_bMatch;
		BOOL		 m_fCycle;
		BOOL		 m_fAcceptNak;
		BOOL		 m_fAcceptAck;
		CLinkFrame	 m_Req;
		CLinkFrame	 m_Rep;
		UINT		 m_uDevice;

		// Implementation
		virtual BOOL SyncTransact(BYTE bMatch, BOOL fCycle, BOOL fAcceptNak, BOOL fAcceptAck = FALSE);
		virtual UINT FindTransition(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader Support
//

class CBootLoader : public CLoaderBase
{
	public:
		// Constructor
		CBootLoader(void);

		// Attributes
		BOOL IsSameVersion(void) const;
		BOOL CanUseDatabase(void) const;
		BOOL IsSameModel(void) const;
		BOOL IsSameOem(BOOL fProm) const;
		BOOL GetModel(CString &Name) const;
		BOOL GetRevision(DWORD &dwVer) const;
		BOOL GetLevel(UINT &uLevel) const;
		BOOL GetAutoDetect(CString &Name) const;

		// Operations
		BOOL CheckModel(void);
		BOOL CheckOem(CString Config);
		BOOL CheckVersion(GUID Guid, DWORD Dbase);
		BOOL ForceReset(BOOL fTimeout);
		BOOL CheckHardware(void);
		BOOL CheckLoader(BOOL fSkip);
		BOOL ClearProgram(BYTE bBlocks);
		BOOL WriteProgram(DWORD dwAddr, PBYTE pData, WORD wCount, BOOL fLast);
		BOOL WriteProgramEx(DWORD dwAddr, PBYTE pData, WORD wCount, BOOL fLast);
		BOOL WriteVersion(GUID Guid);
		BOOL StartProgram(void);
		BOOL WriteMAC(PBYTE pMAC, UINT uCount);
		BOOL WriteSerialNumber(PBYTE pSerial, UINT uCount);
		BOOL CheckLevel(void);
		BOOL AutoDetect(void);

	protected:
		// Data Members
		CString m_Config;

		// Implementation
		UINT FindTransition(void);
		WORD FindSum(PBYTE pData, WORD wCount);
	};

//////////////////////////////////////////////////////////////////////////
//
// G3 Prom Loader
//

class CPromLoader : public CLoaderBase
{
	public:
		// Constructor
		CPromLoader(void);

		// Attributes
		BOOL IsRunningLoader(void) const;

		// Operations
		BOOL CheckLoader(void);
		BOOL WriteProgram(DWORD dwAddr, PBYTE pData, WORD wCount);
		BOOL UpdateProgram(UINT uProm);
		BOOL StartProgram(void);

	protected:
		// Implementation
		UINT FindTransition(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MC2 Tunnel King
//

class CTunnelKing : public CLoaderBase
{
	public:
		// Constructor
		CTunnelKing(void);

		// Attributes
		BOOL RecvData(PBYTE  pData, UINT uSize) const;

		// Operations
		BOOL SendData(PCBYTE pData, UINT uSize);
	};

//////////////////////////////////////////////////////////////////////////
//
// G3 Configuration Support
//

class CConfigLoader : public CLoaderBase
{
	public:
		// Constructor
		CConfigLoader(void);

		// Attributes
		BOOL GetReply(void) const;
		UINT GetItemSize(void) const;
		void GetItemData(PBYTE pData, UINT uCount) const;
		BOOL GetCompression(BOOL &fCompress) const;
		BOOL GetExecution(BOOL &fExecute) const;
		BOOL GetControl(BOOL &fControl) const;
		BOOL GetSystemHalt(BOOL &fHalted) const;
		BOOL GetEditFlags(BYTE &bFlags, PDWORD pCheck) const;
		UINT GetIdentity(PBYTE pData, UINT uCount) const;

		// Operations
		BOOL CheckVersion(GUID Guid);
		BOOL ClearConfig(void);
		BOOL ClearGarbage(void);
		BOOL CheckItem(UINT uItem, UINT uClass, DWORD CRC);
		BOOL WriteItem(UINT uItem, UINT uClass, UINT uSize);
		BOOL WriteItemEx(UINT uItem, UINT uClass, UINT uSize, UINT uComp);
		BOOL WriteData(DWORD dwAddr, PBYTE pData, WORD wCount);
		BOOL WriteVersion(GUID Guid);
		BOOL HaltSystem(void);
		BOOL StartSystem(void);
		BOOL WriteTime(void);
		BOOL ReadItem(UINT uItem);
		BOOL ReadItem(UINT uItem, UINT uSubItem);
		BOOL ReadData(UINT uItem, UINT uAddr, UINT uCount);
		BOOL ReadDataDP(UINT uItem, UINT uAddr, UINT uCount);
		BOOL FlashMount(void);
		BOOL FlashDismount(void);
		BOOL FlashVerify(BOOL fRaw);
		BOOL FlashFormat(void);
		BOOL CheckCompression(void);
		BOOL CheckExecution(void);
		BOOL CheckControl(void);
		BOOL CheckEditFlags(void);
		BOOL ClearEditFlags(BYTE bMask);
		BOOL Identify(BYTE bCode);

	protected:
		// Data Members
		UINT m_uSize;
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Access
//

class CDataAccess : public CLoaderBase
{
	public:
		// Constructor
		CDataAccess(void);

		// Attributes
		BOOL GetReadData(PDWORD pData, PBOOL pAvail, UINT uCount) const;

		// Operations
		BOOL ListRead(PCDWORD pRefs, UINT uCount);
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Online
//

class CStratonOnline : public CLoaderBase
{
	public:
		// Constructor
		CStratonOnline(void);

		// Attributes
		UINT Recv(PBYTE pData, UINT uSize) const;

		// Operations
		BOOL Send(PCBYTE pData, UINT uSize);
	};

//////////////////////////////////////////////////////////////////////////
//
// Crimson Authentication
//

class CCrimsonAuth : public CLoaderBase
{
public:
	// Constructor
	CCrimsonAuth(void);

	// Attributes
	BOOL ReadChallenge(CByteArray &Device, CByteArray &Challenge);
	BOOL CheckResponse(void);

	// Operations
	BOOL GetChallenge(void);
	BOOL SendResponse(CByteArray const &User, CByteArray const &Response);
};

//////////////////////////////////////////////////////////////////////////
//
// Transport Creation
//

ILinkTransport * Create_TransportSerial(UINT uPort);

ILinkTransport * Create_TransportUSB(WORD wBase);

ILinkTransport * Create_TransportNetwork(CString Name, UINT uPort, BOOL fSlow);

// End of File

#endif
