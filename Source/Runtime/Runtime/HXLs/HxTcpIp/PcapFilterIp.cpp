
#include "Intern.hpp"

#include "PcapFilterIp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IpHeader.hpp"

#include "TcpHeader.hpp"

#include "UdpHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Packet Capture Filter with IP Support
//

// Filtering

BOOL CPcapFilterIp::StoreIp(CIpHeader *pIP, BOOL fSend) const
{
	if( pIP->m_Protocol == IP_ICMP ) {

		return StoreIcmp(fSend);
		}

	if( pIP->m_Protocol == IP_TCP ) {

		if( m_wTcp ) {

			PBYTE       pData = PBYTE(pIP + 1) + pIP->GetOptsLength();

			CTcpHeader *pTcp  = (CTcpHeader *) pData;

			return StoreTcp(pTcp->m_LocPort, pTcp->m_RemPort, fSend);
			}

		return FALSE;
		}

	if( pIP->m_Protocol == IP_UDP ) {

		if( m_wUdp ) {

			PBYTE       pData = PBYTE(pIP + 1) + pIP->GetOptsLength();

			CUdpHeader *pUdp  = (CUdpHeader *) pData;

			return StoreUdp(pUdp->m_LocPort, pUdp->m_RemPort, fSend);
			}

		return FALSE;
		}

	return FALSE;
	}

// End of File
