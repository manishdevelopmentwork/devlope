
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Program Item
//

// External Data Access

enum {
	readReferenced		= 0,
	readAlways		= 1,
	readExecuted		= 2,
	};

// Constructor

CProgramItem::CProgramItem(void)
{
	m_pCode   = NULL;

	m_BkGnd   = 0;
	
	m_Comms   = readReferenced;
	
	m_Timeout = 0;

	m_Run     = 0;

	m_fTemp   = FALSE;

	m_fList   = FALSE;

	m_pArgs   = NULL;
	}

// Destructor

CProgramItem::~CProgramItem(void)
{
	delete m_pCode;
	}

// Initialization

void CProgramItem::Load(PCBYTE &pData)
{
	ValidateLoad("CProgramItem", pData);

	GetCoded(pData, m_pCode);

	m_Name    = GetWide(pData);
	m_BkGnd   = GetByte(pData);
	m_Comms   = GetByte(pData);
	m_Timeout = GetWord(pData);
	m_Run     = GetByte(pData);
	}

// Operations

BOOL CProgramItem::IsAvail(void)
{
	if( m_pCode ) {

		switch( m_Comms ) {

			case readReferenced:
			case readAlways:
				return m_Run ? TRUE : m_pCode->IsAvail();

			case readExecuted:
				return TRUE;			
			}		
		}

	return FALSE;
	}

BOOL CProgramItem::SetScan(UINT Code)
{
	if( m_pCode ) {

		switch( m_Comms ) {

			case readReferenced:
				return m_pCode->SetScan(Code);

			case readAlways:
			case readExecuted:
				return TRUE;
			}
		}

	return FALSE;
	}

BOOL CProgramItem::PreRegister(void)
{
	if( m_pCode ) {

		switch( m_Comms ) {

			case readAlways:
				return m_pCode->SetScan(scanTrue);

			case readReferenced:
			case readExecuted:
				return TRUE;
			}
		}

	return FALSE;
	}

DWORD CProgramItem::Execute(PDWORD pParam)
{
	if( m_pCode ) {

		if( m_Comms == readExecuted ) {

			m_pCode->SetScan(scanOnce);

			UINT t = ToTicks(m_Timeout * 100);

			SetTimer(t);

			while( GetTimer() ) {

				if( m_Run || m_pCode->IsAvail() ) {

					return m_pCode->Execute(typeVoid, pParam);
				}

				Sleep(25);
			}

			return 0;
		}

		return m_pCode->Execute(typeVoid, pParam);
	}

	return 0;
}

// End of File
