
//////////////////////////////////////////////////////////////////////////
//
// EZ Constants
//

#define EZ_START	0xAA

//////////////////////////////////////////////////////////////////////////
//
// EZ Master Base Driver
//

class CEZMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CEZMasterDriver(void);

		// Destructor
		~CEZMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Context
		struct CBaseCtx
		{
			BYTE	m_bGroup;
			WORD	m_wUnit;
			};

	protected:
		// Data Members
		CBaseCtx * m_pBase;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
			
		// Implementation
		void DoBlockRead(UINT uTable, UINT uOffset, UINT uCount);
		void DoMultiRead(UINT uTable, UINT uOffset, UINT uCount);
		void Start(void);
		void AddByte(BYTE bByte);
		void AddID(void);
		void AddCommandCode(UINT uTable);
		void AddType(UINT uTable);
		void AddWord(UINT uOffset);
		void AddCount(UINT uTable, UINT uCount);
		void AddData(AREF Addr, PDWORD pData, UINT uCount);
		void AddWordData(AREF Addr, PDWORD pData, UINT uCount);
		void AddLongData(AREF Addr, PDWORD pData, UINT uCount);
		UINT GetData(UINT uType, UINT uCount, PDWORD pData, UINT uOffset);
		UINT GetDiscreteData(UINT uCount,  PDWORD pData, UINT uOffset);
		UINT GetWordData(UINT uCount, PDWORD pData, UINT uOffset);
		UINT GetLongData(UINT uCount, PDWORD pData, UINT uOffset);
		UINT GetType(UINT uTable);
		
		// Helpers
		BOOL IsDiscrete(UINT uTable);
		BOOL IsLong(UINT uType); 
								
		// Transport Layer
		virtual BOOL Transact(void);
	
	};

// End of File
