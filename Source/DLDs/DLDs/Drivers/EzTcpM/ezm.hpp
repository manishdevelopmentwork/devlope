#include "ez.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EZ Master Serial Driver
//

class CEZMasterSerialDriver : public CEZMasterDriver
{
	public:
		// Constructor
		CEZMasterSerialDriver(void);

		// Destructor
		~CEZMasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
	protected:
		// Device Context
		struct CContext : CEZMasterDriver::CBaseCtx
		{
		
			};

		// Data Members
		CContext * m_pCtx;

		// Transport
		virtual BOOL Transact(void);
		virtual BOOL CheckFrame(void);

		BOOL Send(void);
		BOOL RecvFrame(void);

		// Helpers
		WORD CalcCRC(PBYTE pBuff);

		
	};

// End of File
