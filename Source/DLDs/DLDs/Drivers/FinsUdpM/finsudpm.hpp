#include "ofins.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omron Fins Master UDP Driver
//

class COmronFinsMasterUDPDriver : public COmronFinsMasterDriver
{
	public:
		// Constructor
		COmronFinsMasterUDPDriver(void);

		// Destructor
		~COmronFinsMasterUDPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
	protected:
		// Device Context
		struct CContext : COmronFinsMasterDriver::CBaseCtx
		{
			DWORD	 m_IP1;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			DWORD	 m_IP2;
			BOOL	 m_fAux;
			BYTE	 m_bDna2;
			BYTE	 m_bDa12;
			BYTE	 m_bDa22;
			BOOL	 m_fDirty;
			UINT	 m_uPoll;
			UINT	 m_uData;
			UINT	 m_uTime;
			BOOL	 m_fIgnoreNonFatal;
			BOOL	 m_fIgnoreFatal;
			BYTE	 m_bFaultMask;
			};

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Transport Layer
		BOOL Send(void);
		BOOL RecvFrame(UINT uTotal);
		BOOL Transact(BYTE bMRes, BYTE bSRes, UINT uCount, UINT uType);
		BOOL CheckFrame(BYTE bMRes, BYTE bSRes, BOOL fCheck);

		BOOL Start(void);
		void PutFinsHeader(void);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);		
	};

// End of File
