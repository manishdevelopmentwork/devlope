#include "intern.hpp"

#include "metasys3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Johnson Controls MetaSys N2 System Slave Driver - Multi Device - Currently Values Only
//

// Instantiator

INSTANTIATE(CMetaSysN2SystemDriver3);

// Constructor
 
CMetaSysN2SystemDriver3::CMetaSysN2SystemDriver3(void)
{
	m_Ident = DRIVER_ID;

	for( UINT u = 0; u < elements(m_pList); u++ ) {

		m_pList[u] = NULL;
		}

	m_uDevs = 0;
       	}

// Destructor

CMetaSysN2SystemDriver3::~CMetaSysN2SystemDriver3(void)
{
	for( UINT u = 0; u < elements(m_pList); u++ ) {

		delete m_pList[u];
		}
	}
 

// Config

void MCALL CMetaSysN2SystemDriver3::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, FALSE);
	}

void MCALL CMetaSysN2SystemDriver3::Attach(IPortObject *pPort)
{
	m_pHandler = new CMetaSysHandler(m_pHelper, this);

	pPort->Bind(m_pHandler);
	}

void MCALL CMetaSysN2SystemDriver3::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CMetaSysN2SystemDriver3::Open(void)
{
	}

// Device

CCODE MCALL CMetaSysN2SystemDriver3::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop   = GetByte(pData);

			m_pCtx->m_fOnline = FALSE;

			pDevice->SetContext(m_pCtx);

			CreateDevice(m_pCtx->m_bDrop);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}
      
	return CCODE_SUCCESS;
		
	}

CCODE MCALL CMetaSysN2SystemDriver3::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMetaSysN2SystemDriver3::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	PBYTE pValues = m_pHandler->GetData(m_pCtx->m_bDrop, Addr.a.m_Table, Addr.a.m_Offset);

	if( pValues ) {

		memcpy((PBYTE)pData, pValues, uCount * 4);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CMetaSysN2SystemDriver3::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pHandler->SetData(m_pCtx->m_bDrop, Addr.a.m_Table, Addr.a.m_Offset, (PBYTE)pData, uCount) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

// Implementation

void CMetaSysN2SystemDriver3::CreateDevice(BYTE bDevice)
{
	m_pList[m_uDevs] = new CDevice;

	m_pList[m_uDevs]->m_bDrop = bDevice;

	m_pList[m_uDevs]->m_fOnline = FALSE;

	memset(PBYTE(m_pList[m_uDevs]->m_Table), 0, sizeof(CTable));
	
	m_uDevs++;
	} 

CDevice * CMetaSysN2SystemDriver3::FindDevice(BYTE bDevice)
{
	for( UINT u = 0; u < m_uDevs; u++ ) {

		if( m_pList[u]->m_bDrop == bDevice ) {

			return m_pList[u];
			}
		}

	return NULL;
	}

// End of File
