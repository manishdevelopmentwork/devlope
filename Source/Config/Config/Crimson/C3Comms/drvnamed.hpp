
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DRVNAMED_HPP
	
#define	INCLUDE_DRVNAMED_HPP

//////////////////////////////////////////////////////////////////////////
//
// Test Named Comms Driver
//

class CTestNamedCommsDriver : public CNamedCommsDriver
{
	public:
		// Constructor
		CTestNamedCommsDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
