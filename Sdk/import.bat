@echo off

rem *** Switch to the project directory.

cd /d "%~1"

rem *** Clean up any left-over folders.

if exist Bin\     rmdir /s /q Bin\
if exist Build\   rmdir /s /q Build\
if exist Include\ rmdir /s /q Include\
if exist Tools\   rmdir /s /q Tools\

rem *** Expand the archive into working folders.

7z x sdk.zip 2>&1 >nul

rem *** Link the source tree into these folders.

if not exist "%~2..\Include\"      mklink /j "%~2..\Include" Include 
if not exist "%~2..\Source\Build\" mklink /j "%~2..\Source\Build" Build 
if not exist "%~2..\Tools\"        mklink /j "%~2..\Tools" Tools 

rem *** Copy the executables to the output folder.

xcopy /D /S /Y Bin "%~3..\..\" 2>&1 >nul

rem *** Done.
