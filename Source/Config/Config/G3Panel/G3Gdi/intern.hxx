
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Embedded GDI Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//
//

#define IDS_BASE                0x4000
#define IDS_AQUA                0x4000
#define IDS_BLACK               0x4001
#define IDS_BLUE                0x4002
#define IDS_FORMAT              0x4003
#define IDS_FUCHSIA             0x4004
#define IDS_GRAY                0x4005
#define IDS_GREEN               0x4006
#define IDS_LIME                0x4007
#define IDS_MAROON              0x4008
#define IDS_NAVY                0x4009
#define IDS_OLIVE               0x400A
#define IDS_PURPLE              0x400B
#define IDS_RED                 0x400C
#define IDS_SILVER              0x400D
#define IDS_TEAL                0x400E
#define IDS_WHITE               0x400F
#define IDS_YELLOW              0x4010

// End of File

#endif
