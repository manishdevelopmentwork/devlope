
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AppObject_HPP

#define INCLUDE_AppObject_HPP

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

class CAppObject : public IClientProcess, public IDiagProvider
{
	public:
		// Constructor
		CAppObject(void);

		// Destructor
		~CAppObject(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Data Members
		ULONG m_uRefs;
		UINT  m_uProv;

		// Diagnostics
		BOOL DiagRegister(void);
		BOOL DiagRevoke(void);
		UINT DiagTest(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
