
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SPI_HPP

#define	INCLUDE_SPI_HPP

//////////////////////////////////////////////////////////////////////////
//
// SPI Master Device Options
//

class CSpiMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSpiMasterDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:

		// Data Members
		UINT m_DevID;
		UINT m_Addr;
		UINT m_BU[2];
	
		// Meta Data Creation
		void AddMetaData(void);
	
	};


//////////////////////////////////////////////////////////////////////////
//
// Spi Master Driver
//

class CSpiMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CSpiMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

	protected:
	
		// Implementation
		void AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Spi Master Driver Address Selection
//

class CSpiMasterDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CSpiMasterDialog(CSpiMasterDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:

		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);


	
	};


#endif

// End of File

