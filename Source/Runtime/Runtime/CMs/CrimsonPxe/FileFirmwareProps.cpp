
#include "intern.hpp"

#include "FileFirmwareProps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Filing System Firmware Properties Object
//

// Instantiator

global IFirmwareProps *	Create_FileFirmwareProps(CString const &Root)
{
	return New CFileFirmwareProps(Root);
}

// Constructor

CFileFirmwareProps::CFileFirmwareProps(CString const &Root)
{
	m_Root = Root;

	mkdir(m_Root, 0755);

	StdSetRef();
}

// Destructor

CFileFirmwareProps::~CFileFirmwareProps(void)
{
}

// IUnknown

HRESULT CFileFirmwareProps::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IFirmwareProps);

	StdQueryInterface(IFirmwareProps);

	return E_NOINTERFACE;
}

ULONG CFileFirmwareProps::AddRef(void)
{
	StdAddRef();
}

ULONG CFileFirmwareProps::Release(void)
{
	StdRelease();
}

// IFirmwareProps

bool CFileFirmwareProps::IsCodeValid(void)
{
	return TRUE;
}

PCBYTE CFileFirmwareProps::GetCodeVersion(void)
{
	CAutoFile File(m_Root + "guid.bin", "r");

	if( File ) {

		File.Read(&m_Guid, 1);
	}
	else {
		m_Guid = CGuid();
	}

	return PCBYTE(&m_Guid);
}

UINT CFileFirmwareProps::GetCodeSize(void)
{
	AfxAssert(FALSE);

	return 0;
}

PCBYTE CFileFirmwareProps::GetCodeData(void)
{
	AfxAssert(FALSE);

	return NULL;
}

// End of File
