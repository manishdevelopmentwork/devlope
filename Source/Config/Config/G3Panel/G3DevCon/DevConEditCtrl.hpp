
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConEditCtrl_HPP

#define INCLUDE_DevConEditCtrl_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConElement;

//////////////////////////////////////////////////////////////////////////
//
// Notification Codes
//

#define EN_RETURN	0x8801
#define EN_TAB_AWAY	0x8802
#define EN_CANCEL	0x8803
#define EN_DROP		0x8804

//////////////////////////////////////////////////////////////////////////
//
// Content Modes
//

#define EC_ANY		1
#define	EC_HOSTNAME	2
#define EC_NUMBER	3
#define EC_FLOAT	4
#define EC_IP		5

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Edit Control
//

class CDevConEditCtrl : public CEditCtrl, public IDropTarget
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConEditCtrl(CDevConElement *pElem);

	// IUnknown
	HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
	ULONG   METHOD AddRef(void);
	ULONG   METHOD Release(void);

	// IDropTarget
	HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragLeave(void);
	HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

	// Attributes
	BOOL GetError(void) const;

	// Operations
	void SetContent(UINT uContent);
	void SetScroll(BOOL fScroll);
	void SetError(BOOL fError);
	void SetDefault(PCTXT pDefault);

protected:
	// Static Data
	static UINT m_timerQuick;

	// Data Members
	CDevConElement * m_pElem;
	CString          m_Default;
	UINT		 m_uContent;
	BOOL	         m_fScroll;
	BOOL	         m_fFirst;
	BOOL	         m_fQuick;
	BOOL	         m_fError;
	BOOL	         m_fDying;
	CDropHelper      m_DropHelp;
	DWORD	         m_dwEffect;
	UINT             m_uDrop;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	void OnPostCreate(void);
	void OnPreDestroy(void);
	UINT OnNCCalcSize(BOOL fCalcRects, NCCALCSIZE_PARAMS &Params);
	void OnNCPaint(void);
	void OnPaint(void);
	void OnKillFocus(CWnd &Wnd);
	void OnLButtonDblClk(UINT uFlags, CPoint Pos);
	void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
	UINT OnGetDlgCode(MSG *pMsg);
	void OnKeyDown(UINT uCode, DWORD dwData);
	void OnKeyUp(UINT uCode, DWORD dwData);
	void OnChar(UINT uCode, DWORD dwData);
	void OnTimer(UINT uID, TIMERPROC *pfnProc);

	// Command Handlers
	BOOL OnPasteControl(UINT uID, CCmdSource &Src);
	BOOL OnPasteCommand(UINT uID);

	// Implementation
	void   Construct(void);
	void   HandleTab(void);
	CWnd & FindParent(void);
	BOOL   SetDrop(UINT uDrop);
	BOOL   InEditMode(void);
	BOOL   IsItemReadOnly(void);
};

// End of File

#endif
