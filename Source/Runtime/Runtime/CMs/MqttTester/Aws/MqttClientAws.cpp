
#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientAws.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudServiceAws.hpp"

#include "MqttClientOptionsAws.hpp"

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for AWS
//

// Constructor

CMqttClientAws::CMqttClientAws(CCloudServiceAws *pService, CMqttClientOptionsAws &Opts) : CMqttClientJson(pService, Opts), m_Opts(Opts)
{
	m_ObjTop      = "state";

	m_ObjRep      = "reported";

	m_ObjDes      = "desired";

	m_ThingTopic  = "$aws/things/" + m_Opts.m_ClientId;

	m_ShadowTopic = m_ThingTopic   + "/shadow/update";

	m_Will.SetTopic(m_ShadowTopic);

	m_Will.SetData ("{\"state\":{\"reported\":" + GetWillData() + "}}");

	m_fTwin = TRUE;
	}

// Client Hooks

void CMqttClientAws::OnClientLoadIdentity(void)
{
	m_pMatrix->LoadIdentity( m_Opts.m_pCertData, m_Opts.m_uCertData,
				 m_Opts.m_pPrivData, m_Opts.m_uPrivData,
				 NULL,
				 m_Opts.m_pAuthData, m_Opts.m_uAuthData
				 );
	}

void CMqttClientAws::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseInitial ) {

		if( HasWrites() ) {

			AddToSubList(subDelta, m_ShadowTopic + "/delta");
			}
		}

	CMqttClientJson::OnClientPhaseChange();
	}

BOOL CMqttClientAws::OnClientNewData(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode == subDelta ) {

		CJsonData Json;

		if( pMsg->GetJson(Json) ) {

			OnWrite(Json, "state");
			}

		return TRUE;
		}

	return TRUE;
	}

// Publish Hook

BOOL CMqttClientAws::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	if( CMqttClientJson::GetPubMessage(uTopic, uTime, fTemp, uMode, pMsg) ) {

		pMsg->SetTopic(m_ShadowTopic);

		return TRUE;
		}

	return FALSE;
	}

// End of File
