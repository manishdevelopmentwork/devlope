
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Prototypes
//

static BOOL HexConst(PCTXT pText, CByteArray * pSource, CByteArray *pObject);
static void PutConst(CByteArray *pObject, UINT n);

//////////////////////////////////////////////////////////////////////////
//
// Expression Compiler
//

DLLAPI BOOL C3CompileExpr( CError	  & Error,
			   CRange	  & Range,
			   PCTXT	    pText,
			   CTypeDef       & Type,
			   UINT		    uParam,
			   CFuncParam     * pParam,
			   INameServer    * pName,
			   CByteArray     * pSource,
			   CLongArray     * pRefList,
			   CByteArray     * pObject
			   )
{
	if( Type.m_Type == typeInteger && Type.m_Flags == flagNone ) {

		if( HexConst(pText, pSource, pObject) ) {

			Type.m_Type  = typeInteger;

			Type.m_Flags = flagConstant;

			return TRUE;
			}
		}

	CExprParser *pParser = New CExprParser;

	CTypeDef     Need    = Type;

	if( pParser->ParseExpr(Error, pText, Type, uParam, pParam, pName) ) {

		CTypeDef Real = Type;

		if( pObject ) {

			CParseTree &Tree = pParser->GetTree();

			if( Tree.GetCount() ) {

				CTreeOptimizer *pOptim = New CTreeOptimizer(Tree);

				if( pOptim->Optimize(Error) ) {

					CCodeGenerator *pCoder = New CCodeGenerator(Tree);

					UINT uDepth = pParser->GetDepth() + 1;

					UINT uLocal = pParser->GetLocal();

					pCoder->Prepare(uDepth, uLocal, uParam, pParam);

					if( !(Need.m_Flags & flagSoftWrite) ) {

						if( !(Need.m_Flags & flagWritable) ) {

							Real.m_Flags &= ~flagWritable;

							Type.m_Flags &= ~flagWritable;
							}
						}

					if( !(Need.m_Flags & flagInherent) ) {

						if( !(Need.m_Flags & flagWritable) ) {

							Real.m_Type = Need.m_Type;

							Type.m_Type = Need.m_Type;
							}
						}

					if( pCoder->GenerateExpr(Error, Real) ) {

						pObject->Empty();

						pObject->Append(pCoder->GetCode());
						}
					else
						Range.Empty();

					delete pCoder;
					}
				else
					Range = pOptim->GetErrorPos();

				delete pOptim;
				}
			}

		if( pSource ) {

			pSource->Empty();

			pSource->Append(pParser->GetSource());
			}

		if( pRefList ) {

			pRefList->Empty();

			pRefList->Append(pParser->GetRefList());
			}
		}
	else
		Range = pParser->GetTokenPos();

	delete pParser;

	return Error.IsOkay();
	}

//////////////////////////////////////////////////////////////////////////
//
// Constant Accelerator
//

static BOOL HexConst(PCTXT pText, CByteArray * pSource, CByteArray *pObject)
{
	if( pText[0] == '0' && pText[1] == 'x' ) {

		if( wstrlen(pText) <= 10 ) {

			for( UINT n = 2; pText[n]; n++ ) {

				if( !isxdigit(pText[n]) ) {

					return FALSE;
					}
				}

			if( pSource ) {

				for( int n = 0; pText[n]; n++ ) {

					pSource->Append(BYTE(pText[n]));
					}

				pSource->Append(0);
				}

			if( pObject ) {

				pObject->Append(BYTE(typeInteger));

				pObject->Append(1);

				pObject->Append(0);

				pObject->Append(0);

				DWORD n = wcstoul(pText + 2, NULL, 16);

				PutConst(pObject, n);
				}
			
			return TRUE;
			}
		}

	return FALSE;
	}

static void PutConst(CByteArray *pObject, UINT n)
{
	if( HIWORD(n) ) {

		pObject->Append(0xB4);

		pObject->Append(HIBYTE(HIWORD(n)));
		pObject->Append(LOBYTE(HIWORD(n)));
		pObject->Append(HIBYTE(LOWORD(n)));
		pObject->Append(LOBYTE(LOWORD(n)));
		}
	else {
		pObject->Append(0xB3);

		pObject->Append(HIBYTE(n));
		pObject->Append(LOBYTE(n));
		}

	pObject->Append(0x8D);
	}

// End of File
