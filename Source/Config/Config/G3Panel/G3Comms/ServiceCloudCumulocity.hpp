
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceCloudCumulocity_HPP

#define INCLUDE_ServiceCloudCumulocity_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagSet;
class CHttpClientConnectionOptions;

//////////////////////////////////////////////////////////////////////////
//
// Cumulocity Cloud Configuration
//

class CServiceCloudCumulocity : public CServiceItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CServiceCloudCumulocity(void);

		// Initial Values
		void SetInitValues(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL IsEnabled(void) const;
		UINT GetTreeImage(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Type Definitions
		typedef CHttpClientConnectionOptions COpts;

		// Item Properties
		CCodedItem    * m_pEnable;
		UINT		m_Service;
		CString		m_Host;
		CString		m_Device;
		CString		m_Node;
		UINT		m_AutoCred;
		CString		m_User;
		CString		m_Pass;
		CCodedItem    * m_pCredTag;
		UINT		m_Scan;
		UINT		m_Batch;
		UINT		m_Close;
		CTagSet	      *	m_pSet;
		COpts	      * m_pOpts;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void InitOptions(void);
	};

// End of File

#endif
