
#include "Intern.hpp"

#include "CTVueTagImport.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsManager.hpp"
#include "CommsSystem.hpp"

#include "CUSTOMTAGIMPINFO.hpp"

//////////////////////
//                  //
// CTVue Tag Import //
//                  //
//////////////////////

AfxImplementRuntimeClass(CCTVueTagImport, CObject);

// Constructor
CCTVueTagImport::CCTVueTagImport(void)
{
	memset(m_ColDef, 0xFF, sizeof(m_ColDef));
	}

BOOL CCTVueTagImport::IsCTVueHeader(PTXT sLine)
{
	return MatchText( (CString)sLine, (CString)"CONTROL TECHNIQUES" );
	}

void CCTVueTagImport::SetCTInfoPtr(CUSTOMTAGIMPINFO *pInfo)
{
	m_pInfo = pInfo;

	m_pInfo->uIDS    |= CTSELECTED;

	pInfo->uFileLine  = 0;

	pInfo->uItem      = 0;
	}

CString CCTVueTagImport::ProcessCTVueHeader(PTXT sLine)
{
	CUSTOMTAGIMPINFO * pInfo = m_pInfo;

	pInfo->uFileLine++;

	CString s = (CString)sLine;

	if( s[0] == ';' ) {

		if( MatchText( s, (CString)"Access") ) {	// column definition line found

			SetColDefs(s);
			}
		}

	else {
		if( pInfo->uFileLine > 4 ) {

			return (CString)sLine;
			}
		}

	return L"";
	}

CString CCTVueTagImport::GetCTVueLine(void)
{
	CUSTOMTAGIMPINFO * pInfo = m_pInfo;

	m_pInfo->uItem++;

	CString sRtn    = pInfo->sTagLine;

	CCommsManager *pMgr = m_pInfo->pCSys->m_pComms;

	CString sDev = FindCommaN(sRtn, m_ColDef[L_DEV]);

	sDev = FixDevc(sDev);

	CCommsDevice  *pDev = pMgr->FindDevice(sDev);

	if( !pDev ) return L"No Device";

	sRtn = FindCommaN(sRtn, m_ColDef[L_TYPE]);

	pInfo->uFormat = GetCTVueFormatType(StripLeft(sRtn));

	if( pInfo->uFormat == 8 ) {

		sRtn.Printf( L"[String.%1.1d.0]\r\n\0", pInfo->uFormat );
		}

	else {
		sRtn.Printf( L"[Numeric.%1.1d.0]\r\n\0", pInfo->uFormat );
		}

	return sRtn;
	}

CString CCTVueTagImport::GetColHeaders(void)
{
	CString s = L"";

	switch( m_pInfo->uFormat ) {

		case 5:
			m_BitColDef = (CString)L"Name,Value,Access,FormType\r\n\0";
			return m_BitColDef;

		case 6:
			s.Printf( L"Name,Value,Extent,TreatAs,Access,RdMode,FormType,Format / Count%s\r\n\0",

				GetCHRange()
				);

			m_ArrColDef = s;
			return s;

		case 8:
			m_StrColDef = (CString)L"Name,Value,Length,Encode,Access,FormType\r\n\0";
			return m_StrColDef;
		}

	s.Printf( L"Name,Value,TreatAs,Access,FormType%s%s,Format / Signed,Format / Before,Format / After%s\r\n\0",

		GetCHRange(),
		GetCHDec(),
		GetCHUnit()
		);

	m_NumColDef = s;

	return s;
	}

CString CCTVueTagImport::GetCTVueTagData(void)
{
	CString sRtn;

	CString s = L"";

	CStringArray Fields;

	m_pInfo->sTagLine.Tokenize( Fields, ',' );

	for( UINT i = 0; i < Fields.GetCount(); i++ ) {

		CString x = Fields[i] + (CString)"\r\n";

		x = x.Left(x.GetLength() - 2);
		}

	switch( m_pInfo->uFormat ) {

		case 5:
			s = GetCTVue2St(Fields);
			break;

		case 8:
			s = GetCTVueStr(Fields);
			break;

		default:
			s = GetCTVueNum(Fields);
			break;
		}

	sRtn.Printf( L"%s,[%s.%s%s],%s",

		FixName(Fields[m_ColDef[L_NAME]]),
		FixDevc(Fields[m_ColDef[L_DEV ]]),
		FixRegr(Fields),
		m_pInfo->sType,
		s
		);

	return sRtn;
	}

// Implementation

CString CCTVueTagImport::GetCTVueNum(CStringArray Fields)
{
	CString sType	= StripLeft(Fields[m_ColDef[L_TYPE]]);

	UINT c = sType[0];

	CString sRange	= GetRangeString(Fields);
	CString sUnits	= GetUnitsString(Fields);

	UINT uAfter	= GetDecim(Fields);

	UINT uBefore	= 8;

	if( uAfter == 10 ) {

		uAfter = c == 'F' ? 3 : 0;
		}

	uBefore = min( uBefore, 10 - uAfter );

	CString s;

//Name,Value are common to all, and added later
// This creates:
//TreatAs,Access,FormType=Numeric"
//LimitLow,LimitHigh (single string item)
//Format Radix (always decimal),Format Signed,Format Before,Format After
//Format Units

	s.Printf(L"%s,%s,Numeric%s,Decimal,%s,%d,%d%s\r\n\0",

		GetCTVueTreatAsType(),					// Treat As
		GetCTVueRWType(StripLeft(Fields[m_ColDef[L_ACC]])),	// Access
		sRange,
		c == 'U' ? L"Unsigned" : L"SoftSign",			// Format Signed
		uBefore,						// Format Before

		uAfter,							// Format After
		sUnits
		);

	return s;
	}

CString CCTVueTagImport::GetCTVue2St(CStringArray Fields)
{
	CString s;

	s.Printf(L"%s,Two-State\r\n\0",

		GetCTVueRWType(StripLeft(Fields[m_ColDef[L_ACC]]))		// Access
		);

	return s;
	}

CString CCTVueTagImport::GetCTVueStr(CStringArray Fields)
{
	return L"16,ASCII Big-Endian,Read and Write,String";
	}

UINT CCTVueTagImport::GetCTVueFormatType(CString Type)
{
	Type.MakeUpper();

	if( Type == "BOOL" || Type == "OUTPUTEVENT" || Type == "INPUTEVENT" ) {

		m_pInfo->sType = L".BIT";

		return 5;
		}

	if( Type == "UINT32" || Type == "INT32" || Type == "SINT32" ) {

		m_pInfo->sType = L".LONG";

		return 0;
		}

	if( Type == "UINT16" || Type == "INT16" || Type == "SINT16" ) {

		m_pInfo->sType = L"";

		return 1;
		}

	if( Type == "STRING" ) {

		m_pInfo->sType = L".LONG";

		return 8;
		}

	if( Type == "ENUM" ) {

		m_pInfo->sType = L"";

		return 1;
		}

	if( Type == "FLOAT32" ) {

		m_pInfo->sType = ".REAL";

		return 1;
		}

	m_pInfo->sType = L".LONG";

	return 0;
	}

CString CCTVueTagImport::GetCTVueTreatAsType(void)
{
	CString sType = m_pInfo->sType;

	sType.MakeUpper();

	if( sType[0] == 'R' || sType[1] == 'R' ) {

		return L"Floating Point";
		}

	return L"Default Integer";
	}

CString CCTVueTagImport::GetCTVueRWType(CString sIn)
{
	sIn.MakeUpper();

	if( sIn == L"RO" ) {

		return L"Read Only";
		}

	if( sIn[0] == 'W' ) {

		return L"Write Only";
		}

	return L"Read And Write";
	}

CString CCTVueTagImport::GetCHUnit(void)
{
	return m_ColDef[L_UNIT] < CT_LIST_LEN ? L",Format / Units" : L"";
	}

CString CCTVueTagImport::GetCHDec(void)
{
	return m_ColDef[L_DEC] < CT_LIST_LEN ? L",Format / Radix" : L"";
	}

CString CCTVueTagImport::GetCHRange(void)
{
	return m_ColDef[L_RNG] < CT_LIST_LEN ? L",LimitMin,LimitMax,LimitType" : L"";
	}

// Helpers
void CCTVueTagImport::SetColDefs(CString sLine)
{
	CStringArray ListA;
	CStringArray ListB;

	CString sList = L"VARIABLE,DATATYPE,DEVICE NAME,MODBUS ADDRESS,ACCESS,UNITS,RANGE,DECIMAL";

	sLine.Tokenize( ListA, ',' );
	sList.Tokenize( ListB, ',' );

	memset(m_ColDef, 0xFF, sizeof(m_ColDef));

	UINT uACnt = ListA.GetCount();
	UINT uBCnt = ListB.GetCount();

	for( UINT u = 0; u < uBCnt; u++ ) {

		CString s = ListB[u];

		UINT i    = 0;

		UINT uCol = 0xFF;

		while( i < uACnt ) {

			CString t = ListA[i];

			if( MatchText( t, s ) ) {

				uCol = i;

				break;
				}

			i++;
			}

		m_ColDef[u] = uCol;
		}
	}

BOOL CCTVueTagImport::MatchText(CString Type, CString Match)
{
	CString s1 = Type;
	CString s2 = Match;

	s1.MakeUpper();
	s2.MakeUpper();

	return s1.Find(s2) < NOTHING;
	}

CString CCTVueTagImport::FixName(CString sName)
{
	CString s = StripRight(StripLeft(sName));

	while( TRUE ) {

		UINT u = s.Find('.');

		if( u < NOTHING ) {

			s = s.Left(u) + L"_" + s.Mid(u + 1);
			}

		else break;
		}

	return s;
	}

CString CCTVueTagImport::FixDevc(CString sDev)
{
	CString s = StripRight(StripLeft(sDev));

	while( TRUE ) {

		UINT u = s.Find(' ');

		if( u < NOTHING ) {

			s = s.Left(u) + s.Mid(u + 1);
			}

		else break;
		}

	return s;
	}

CString CCTVueTagImport::FixRegr(CStringArray Fields)
{
	CString sReg = StripRight(StripLeft(Fields[m_ColDef[L_REG]]));

	TCHAR Mod = sReg[0];

	if( !isdigit(Mod) ) {

		return sReg;
		}

	BOOL fBig = FALSE;

	CString sH = L"01";

	BOOL fIsString	= m_pInfo->uFormat == 8;	// use straight modbus for String items

	if( !fIsString ) {	// check if Pre-mapped has been selected. Reg form is to be mm_nn_aaaaa

		CCommsManager *pMgr = m_pInfo->pCSys->m_pComms;

		CString sDev = FixDevc(Fields[m_ColDef[L_DEV]]);

		CCommsDevice  *pDev = pMgr->FindDevice(sDev);

		CItem         *pCfg = pDev->GetConfig();

		UINT udm  = DMPRE;

		if( pCfg ) {

			udm = pCfg->GetDataAccess("DispMode")->ReadInteger(pCfg);
			}

		if( udm == DMPRE ) {	// is a premapped controller

			UINT uM   = 1;		// menu number - minimum of 1
			UINT uP   = 0;		// parameter number

			UINT uFind = sReg.Find('_');

			fBig = uFind == 3;

			if( uFind <= 3 ) {	// leading part may be formatted correctly

				uM = max(1, wcstoul( sReg.Left(uFind), NULL, 10));
				uP = wcstoul(sReg.Mid(uFind + 1), NULL, 10);

				UINT uFind2 = sReg.FindRev('_');

				if( (uFind2 > uFind) && (uFind2 < uFind + 4) ) {	// menu/param formatted correctly

					Mod = sReg[uFind2 + 1];				// Modbus range number
					}

				else {
					Mod = '4';					// default to holding registers
					}

				if( fBig ) {

					sH.Printf( L"%3.3d", uM);			// menu > 99
					}

				else {
					sH.Printf( L"%2.2d", uM );
					}

				sReg.Printf( L"%s_%2.2d_%c%s%2.2d",

					sH,
					uP,
					Mod,
					sH,
					uP
					);
				}

			else {	// assume it is formatted as standard Modbus
				UINT uNum = wcstoul(sReg.Mid(1), NULL, 10);

				uM = uNum / 100;
				uP = uNum % 100;

				if( uNum > 9999 ) {

					sH.Printf( L"%3.3d", uM );
					}

				else {
					if( uNum > 99 ) {	// structurally valid menu number and parameter

						sH.Printf( L"%2.2d", uM );
						}
					}

				sReg.Printf( L"%s_%2.2d_%c%s%2.2d",

					sH,
					uP,
					Mod,
					sH,
					uP
					);
				}

			return sReg;
			}
		}

// Is a string or is not pre-mapped - therefore Tnnnnn form
	CString sPre = L"D4";

	if( !fIsString ) {

		switch( m_pInfo->sType[0] ) {

			case 'L':
			case 'R':
				break;

			default:
				sPre = L"H4";
				break;
			}
		}

	UINT uNum = wcstoul( sReg.Mid(1), NULL, 10 );

	fBig = uNum > 9999;

	if( fBig ) {

		sH.Printf( L"%5.5d", uNum );
		}

	else {
		sH.Printf( L"%4.4d", uNum );
		}

	if( uNum ) {

		sReg.Printf( L"%s%s",
			sPre,
			sH
			);
		}

	else {
		sReg = sPre + L"0101";
		}

	return sReg;
	}

CString CCTVueTagImport::StripLeft(CString sIn)
{
	UINT i = sIn.GetLength();

	while( i ) {

		if( sIn[0] == ' ' ) {

			sIn = sIn.Mid(1);

			i--;
			}

		else break;
		}

	return sIn;
	}

CString CCTVueTagImport::StripRight(CString sIn)
{
	UINT uLen = sIn.GetLength();

	while( uLen > 1 ) {

		if( sIn[uLen - 1] <= ' ' ) {

			sIn  = sIn.Left(uLen - 1);

			uLen = sIn.GetLength();
			}

		else {
			return sIn;
			}
		}

	return sIn;
	}

CString CCTVueTagImport::FindCommaN(CString sLine, UINT uNum)
{
	CString sRtn	= sLine;

	UINT uFound	= sRtn.Find(',');

	while( uNum ) {

		sRtn	= sRtn.Mid(uFound + 1);

		uFound	= sRtn.Find(',');

		uNum--;
		}

	return sRtn.Left(uFound);
	}

CString CCTVueTagImport::GetRangeString(CStringArray Fields)
{
	UINT uVal = m_ColDef[L_RNG];

	if( uVal > CT_LIST_LEN ) {

		return L"";
		}

	CString s	= Fields[uVal];

	CString sRange	= L" ";

	UINT u		= 0;
	UINT n		= 0;

	BOOL fR		= MatchText( s, (CString)"Range" );

	BOOL fE		= FALSE;

	BOOL fAdd	= FALSE;

	BOOL fF		= m_pInfo->sType[0] == 'R';

	if( !fR ) {

		n = s.Find('=');

		if( n < NOTHING ) {		// <Text>x=<Low><Text>y=<High>

			s  = s.Mid(n + 1);

			fE = TRUE;		// equal sign found, start there
			}

		else {				// just pick the 1st two numerics
			fE = FALSE;
			}

		n  = 0;
		}

	while( n < s.GetLength() ) {

		UINT c	= s[n];

		fAdd	= IsNumericChar(c, n);

		if( fAdd ) {

			if( !u ) sRange = L",";

			if( c != '.' || fF ) {

				sRange += (char)c;
				}
			}

		n++;

		switch( u ) {

			case 0:
				if( fAdd ) {		// found first useable numeric

					u = 1;
					}

				break;

			case 1:
				if( !fAdd ) {		// done finding numerics for low range value

					sRange += ',';

					u = 2;

					if( fE ) {	// move to '='

						n = s.FindRev('=');
						}
					}

				break;

			case 2:
				if( fAdd ) {		// found first numeric for high range value

					u = 3;
					}

				break;

			case 3:
				if( !fAdd ) {

					return sRange;	// done finding Range numerics
					}
				break;
			}
		}

	return sRange + L",From Format";
	}

UINT CCTVueTagImport::GetDecim(CStringArray Fields)
{
	UINT uVal = m_ColDef[L_DEC];

	if( uVal < CT_LIST_LEN ) {

		CString s = StripLeft(Fields[uVal]);

		if( isdigit(s[0]) ) {

			return s[0] - '0';
			}
		}

	return 10;
	}

CString CCTVueTagImport::GetUnitsString(CStringArray Fields)
{
	UINT uVal = m_ColDef[L_UNIT];

	CString s = L"";

	if( uVal < CT_LIST_LEN ) {

		s = L"," + StripLeft(Fields[uVal]);
		}

	return s;
	}

BOOL CCTVueTagImport::IsNumericChar(UINT c, UINT uPos)
{
	if( isdigit(c) ) return TRUE;

	switch( c ) {

		case '+':
		case '-':
		case '.':
			return TRUE;
		}

	return FALSE;
	}

// End of File
