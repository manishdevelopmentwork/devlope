
#include "Intern.hpp"

#include "ServiceCloudAws.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "MqttClientOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AWS Cloud Service
//

// Base Class

#undef  CBaseClass

#define CBaseClass CServiceCloudJson

// Dynamic Class

AfxImplementDynamicClass(CServiceCloudAws, CBaseClass);

// Constructor

CServiceCloudAws::CServiceCloudAws(void)
{
	m_bServCode = servCloudAws;

	m_Shadow    = 1;

	m_pPub      = NULL;

	m_pSub      = NULL;
}

// Type Access

BOOL CServiceCloudAws::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Pub" || Tag == L"Sub" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	return CBaseClass::GetTypeData(Tag, Type);
}

// UI Update

void CServiceCloudAws::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" || Tag == "Shadow" ) {

			DoEnables(pHost);
		}
	}

	CBaseClass::OnUIChange(pHost, pItem, Tag);
}

// Persistance

void CServiceCloudAws::Init(void)
{
	CBaseClass::Init();

	InitOptions();
}

void CServiceCloudAws::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name = Tree.GetName();

		CMetaData const *pMeta = NULL;

		if( Name == L"ScanDev" || Name == L"ScanTags" || Name == L"WriteTags" ) {

			// These values might be in older databases, but aren't valid
			// and need to be skipped without giving an error. We therefore
			// pretend to read them, and get on with the load.

			Tree.GetValueAsInteger();

			continue;
		}

		if( Name == L"Set" ) {

			// This object might be in older databases, but aren't valid
			// and need to be skipped without giving an error. We therefore
			// pretend to read it, and get on with the load. We don't have
			// to worry about the contents, as it should always be empty.

			Tree.GetObject();

			while( !Tree.IsEndOfData() ) {

				Tree.GetName();
			}

			Tree.EndObject();

			continue;
		}

		if( (pMeta = m_pList->FindData(Name)) ) {

			LoadProp(Tree, pMeta);

			continue;
		}
	}

	RegisterHandle();
}

// Property Save Filter

BOOL CServiceCloudAws::SaveProp(CString Tag) const
{
	if( Tag == L"CertFile" ) {

		return FALSE;
	}

	if( Tag == L"PrivFile" ) {

		return FALSE;
	}

	if( Tag == L"AuthFile" ) {

		return FALSE;
	}

	return CBaseClass::SaveProp(Tag);
}

// Conversion

void CServiceCloudAws::PostConvert(void)
{
	ImportCert(L"AWS", m_CertFile, m_PrivFile);

	ImportCert(L"AWS", m_AuthFile);

	CBaseClass::PostConvert();
}

// Download Support

BOOL CServiceCloudAws::MakeInitData(CInitData &Init)
{
	CBaseClass::MakeInitData(Init);

	Init.AddByte(BYTE(m_Shadow));

	Init.AddItem(itemVirtual, m_pPub);

	Init.AddItem(itemVirtual, m_pSub);

	return TRUE;
}

// Meta Data Creation

void CServiceCloudAws::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddInteger(Shadow);
	Meta_AddVirtual(Pub);
	Meta_AddVirtual(Sub);
	Meta_AddString(CertFile);
	Meta_AddString(PrivFile);
	Meta_AddString(AuthFile);

	Meta_SetName(IDS_CLOUD_MANAGER);
}

// Implementation

void CServiceCloudAws::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	pHost->EnableUI(this, "Shadow", fEnable);

	pHost->EnableUI(this, "Pub", fEnable && m_Shadow == 0);
	
	pHost->EnableUI(this, "Sub", fEnable && m_Shadow == 0);
}

void CServiceCloudAws::InitOptions(void)
{
	m_pOpts->m_Check    = 3;

	m_pOpts->m_Advanced = 1;

	m_pOpts->SetClientId(L"CrimsonThing01");

	m_pOpts->SetPeerName(L"a1hbbi4z1w5rtq.iot.us-west-2.amazonaws.com");
}

// End of File
