
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dialog View Window
//

// Dynamic Class

AfxImplementDynamicClass(CDialogViewWnd, CViewWnd);

// Static Data

CDialogViewWnd * CDialogViewWnd::m_pHead = NULL;

CDialogViewWnd * CDialogViewWnd::m_pTail = NULL;
		
// Constructor

CDialogViewWnd::CDialogViewWnd(void)
{
	ModelessListAppend();
	}

// Destructor

CDialogViewWnd::~CDialogViewWnd(void)
{
	ModelessListRemove();
	}

// Operations

BOOL CDialogViewWnd::IsDialogMessage(MSG &Msg)
{
	if( m_hWnd ) {

		if( FilterMessage(Msg) ) {

			return TRUE;
			}

		return ::IsDialogMessage(m_hWnd, &Msg);
		}

	return FALSE;
	}

void CDialogViewWnd::SetRadioGroup(UINT uFrom, UINT uTo, UINT uData)
{
	for( UINT uID = uFrom; uID <= uTo; uID++ ) {
	
		BOOL fCheck = (uID - uFrom == uData);

		CButton &Button = (CButton &) GetDlgItem(uID);
		
		Button.SetCheck(fCheck);
		}
	}

UINT CDialogViewWnd::GetRadioGroup(UINT uFrom, UINT uTo) const
{
	for( UINT uID = uFrom; uID <= uTo; uID++ ) {
	
		CButton &Button = (CButton &) GetDlgItem(uID);
		
		if( Button.IsChecked() ) {
		
			UINT uIndex = uID - uFrom;
			
			return uIndex;
			}
		}
	
	return 0;
	}

void CDialogViewWnd::SetDlgFocus(CWnd &Wnd)
{
	AfxValidateObject(Wnd);
	
	if( Wnd.IsClass(L"BUTTON") ) {
		
		DWORD dwStyle = Wnd.GetWindowLong(GWL_STYLE);
		
		if( (dwStyle & BS_TYPEMASK) == BS_AUTORADIOBUTTON ) {
		
			for( UINT uID = Wnd.GetID();; uID++ ) {
			
				CButton &Button = (CButton &) GetDlgItem(uID);
				
				if( !Button ) {
				
					Wnd.SetFocus();
					
					return;
					}
				
				if( Button.IsChecked() ) {
					
					Button.SetFocus();
					
					return;
					}
				}
			}
		}
		
	if( Wnd.IsClass(L"COMBOBOX") ) {
	
		CComboBox &Combo = (CComboBox &) Wnd;
		
		Combo.SetEditSel(CRange(TRUE));
		}
		
	if( Wnd.IsClass(L"EDIT") ) {
	
		CEditCtrl &Edit = (CEditCtrl &) Wnd;

		if( Edit.SendMessage(WM_GETDLGCODE) & DLGC_HASSETSEL ) {
		
			Edit.SetSel(CRange(TRUE));
			}
		}
		
	Wnd.SetFocus();
	}

void CDialogViewWnd::SetDlgFocus(UINT uID)
{
	SetDlgFocus(GetDlgItem(uID));
	}

// Modeless Translation

BOOL CDialogViewWnd::IsModelessMessage(MSG &Msg)
{
	CDialogViewWnd *pDialog = m_pHead;
	
	while( pDialog ) {

		if( pDialog->IsDialogMessage(Msg) ) {

			return TRUE;
			}
			
		pDialog = pDialog->m_pNext;
		}
	
	return FALSE;
	}

// Message Filter

BOOL CDialogViewWnd::FilterMessage(MSG &Msg)
{
	return FALSE;
	}

// Message Procedures

LRESULT CDialogViewWnd::DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( m_pfnSuper ) {

		AfxAssert(IsWindow());

		return CallWindowProc(m_pfnSuper, m_hWnd, uMessage, wParam, lParam);
		}

	return DefDlgProc(m_hWnd, uMessage, wParam, lParam);
	}

// Default Class Name

PCTXT CDialogViewWnd::GetDefaultClassName(void) const
{
	return L"AfxDialogViewClass";
	}

// Default Class Definition

BOOL CDialogViewWnd::GetClassDetails(WNDCLASSEX &Class) const
{
	Class.style         = CS_DBLCLKS;

	Class.hCursor       = afxModule->LoadCursor(IDC_ARROW);

	Class.hbrBackground = NULL;

	Class.cbWndExtra    = DLGWINDOWEXTRA;

	return TRUE;
	}

// Routing Control

BOOL CDialogViewWnd::OnRouteMessage(MSG const &Msg, LRESULT &lResult)
{
	if( !HasFocus() ) {

		CWnd *pFocus = &GetFocus();

		while( pFocus->IsWindow() ) {

			CWnd *pParent = &pFocus->GetParent();

			if( pParent == this ) {
			
				if( pFocus->RouteMessage(Msg, lResult) ) {

					return TRUE;
					}

				break;
				}

			pFocus = pParent;
			}
		}
		
	return CCmdTarget::OnRouteMessage(Msg, lResult);
	}

// Message Map

AfxMessageMap(CDialogViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_INITMENUPOPUP)

	AfxMessageEnd(CDialogViewWnd)
	};

// Message Handlers

BOOL CDialogViewWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	LPARAM lParam = m_MsgCtx.Msg.lParam;

	if( uID >= 0x8000 && uID <= 0xEFFF ) {

		RouteCommand(uID, lParam);

		return TRUE;
		}
	
	SendMessage(WM_AFX_COMMAND, uID, lParam);

	return TRUE;
	}

void CDialogViewWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	UINT uCount = Menu.GetMenuItemCount();

	for( UINT uPos = 0; uPos < uCount; uPos++ ) {

		UINT uID = Menu.GetMenuItemID(uPos);

		if( uID ) {

			CCmdSourceMenu Source(Menu, uID);

			if( uID < 0xF000 ) {

				Source.PrepareSource();
				}

			RouteControl(uID, Source);
			}
		}
	}

// Implementation

void CDialogViewWnd::ModelessListAppend(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

void CDialogViewWnd::ModelessListRemove(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// End of File
