
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// About Message Box
//

// Runtime Class

AfxImplementRuntimeClass(CAboutBox, CMessageBox);

// Constructor

CAboutBox::CAboutBox(PCTXT pText) : CMessageBox(pText, NULL, MB_ICONINFORMATION | MB_OK)
{
	m_pPattern = L"OMH";

	m_uDefault = 2;

	m_uAlert   = 0;

	UINT uPos  = m_Text.Find(L"@@@");

	if( uPos < NOTHING ) {

		m_Text.Delete(uPos, 3);

		m_fShow = TRUE;

		return;
		}

	m_fShow = FALSE;
	}

// Message Map

AfxMessageMap(CAboutBox, CMessageBox)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_SETCURSOR)

	AfxMessageEnd(CAboutBox)
	};

// Message Handlers

UINT CAboutBox::OnCreate(CREATESTRUCT &Create)
{
	CMessageBox::OnCreate(Create);

	if( m_fShow ) {

		m_IconOrg.y -= 7 * m_FullSize.cy / 2;
		}
	else
		m_IconOrg.y -= 2 * m_FullSize.cy / 2;

	return 0;
	}

BOOL CAboutBox::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID == 100 ) {

		CString Text;

		Text += CString(IDS_OPEN_SOURCE);
		Text += CString(IDS_CONTAINS_ZINT);
		Text += CString(IDS_COPYRIGHT_ROBIN);
		Text += CString(IDS_PORTIONS);
		Text += CString(IDS_CONTAINS_LIBG);

		(*m_pMsgBox)(m_hWnd, Text, NULL, MB_OK, NULL);

		return TRUE;
		}

	if( uID == 101 ) {

		CString Text;

		Text += CString(IDS_CRIMSON);

		Text += CString(IDS_MIKE_GRANBY);
		Text += CString(IDS_PAUL_PLOWRIGHT);
		Text += CString(IDS_ROBERT_SPENCER);
		Text += CString(IDS_KATHY_SNELL);
		Text += CString(IDS_MARK_STEPHENS);
		Text += CString(IDS_NATHAN_CHADMAZIRA);
		Text += CString(IDS_JEREMY_HOWELL);
		Text += CString(IDS_BEN_DRUCK_TESTERN);
		Text += CString(IDS_LOUISE_YINGLING);

		(*m_pMsgBox)(m_hWnd, Text, NULL, MB_OK, NULL);

		return TRUE;
		}

	return CMessageBox::OnCommand(uID, uNotify, Wnd);
	}

void CAboutBox::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	Paint(DC);

	if( m_fShow ) {

		CPoint Pos1 = m_TextOrg + CSize(1, 10 * m_FullSize.cy / 2);

		CPoint Pos2 = Pos1      + CSize(0, 13);

		CFont Font1(L"tahoma", CSize(0, 10), TRUE);

		CFont Font2(L"rlc31",  CSize(0, 38), FALSE);

		DC.SetBkMode(TRANSPARENT);

		DC.Select(Font2);

		DC.SetTextColor(afxColor(BLACK));

		DC.TextOut(Pos2, L"\x049");

		DC.SetTextColor(afxColor(RED));

		DC.TextOut(Pos2, L"\x069");

		int    yp = Pos1.y + 10;

		int    x1 = Pos1.x - 1;

		int    x2 = Pos1.x + DC.GetTextExtent(L"\x049").cx;

		int    y2 = Pos2.y + DC.GetTextExtent(L"\x069").cy;

		int    cx = x2 - x1;

		int    hx = cx / 2;

		if( hx ) {

			CColor c1 = afxColor(RED);

			CColor c3 = afxColor(3dFace);

			for( int xp = 0; xp <= cx; xp++ ) {

				int  n = (xp < hx) ? 0 : (100 * (xp - hx) / hx);

				BYTE r = BYTE((c3.GetRed  () * n + c1.GetRed  () * (100 - n)) / 100);

				BYTE g = BYTE((c3.GetGreen() * n + c1.GetGreen() * (100 - n)) / 100);

				BYTE b = BYTE((c3.GetBlue () * n + c1.GetBlue () * (100 - n)) / 100);

				DC.SetPixel(x1 + xp, yp, CColor(r, g, b));
				}

			DC.Replace(Font1);

			DC.SetTextColor(afxColor(BLACK));

			DC.TextOut(Pos1, CString(IDS_POWERED_BY));
			}

		DC.Deselect();

//		m_Logo = CRect(Pos1.x - 1, Pos1.y, x2, y2);

		AfxTouch(y2);
		}
	}

void CAboutBox::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( m_Logo.PtInRect(Pos) ) {

		ShellExecute( NULL,
			      L"open",
			      L"https://www.redlion.net/link/wp.asp?id=6159",
			      NULL,
			      NULL,
			      SW_SHOWNORMAL
			      );
		}
	}

BOOL CAboutBox::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( Wnd.GetHandle() == m_hWnd ) {

		CPoint Pos = GetCursorPos();

		ScreenToClient(Pos);

		if( m_Logo.PtInRect(Pos) ) {

			SetCursor(LoadCursor(NULL, IDC_HELP));

			return TRUE;
			}

		SetCursor(LoadCursor(NULL, IDC_ARROW));

		return TRUE;
		}

	return FALSE;
	}

// Button Location

BOOL CAboutBox::FindButton(TCHAR cTag, CButtonInfo &Info)
{
	if( cTag == 'T' ) {

		Info.Label = CString(IDS_TEAM);

		Info.uID   = 101;

		return TRUE;
		}

	if( cTag == 'M' ) {

		Info.Label = CString(IDS_OSS_INFO);

		Info.uID   = 100;

		return TRUE;
		}

	return CMessageBox::FindButton(cTag, Info);
	}

// End of File
