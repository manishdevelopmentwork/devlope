
#include "intern.hpp"

#include "select.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Selector Switch
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacySelector, CPrimRich);

// Constructor

CPrimLegacySelector::CPrimLegacySelector(void)
{
	m_Entry      = 1;
	
	m_pKnob      = New CPrimBrush;

	m_pPanel     = New CPrimBrush;

	m_pEdge      = New CPrimPen;

	m_pBack      = New CPrimColor;

	m_ShowStates = 1;

	m_ShowMask   = propEntry | propFormat| propColor;
	}

// UI Creation

BOOL CPrimLegacySelector::OnLoadPages(CUIPageList *pList)
{
	LoadFirstPage(pList);

	pList->Append(New CPrimRichPage(this, CString(IDS_FIGURE), 2));

	LoadRichPages(pList);
	
	return TRUE;
	}

// UI Update

void CPrimLegacySelector::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	CPrimRich::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

void CPrimLegacySelector::SetInitState(void)
{
	CPrimRich::SetInitState();

	m_pKnob ->Set(GetRGB(12,12,24));

	m_pPanel->Set(GetRGB(8,8,8));
		     
	m_pBack ->Set(GetRGB(12,12,12));

	m_pEdge->m_Corner = 0;

	SetInitSize(100, 120);
	}

// Download Support

BOOL CPrimLegacySelector::MakeInitData(CInitData &Init)
{
	CPrimRich::MakeInitData(Init);

	Init.AddByte(BYTE(m_ShowStates));
	
	Init.AddItem(itemSimple, m_pKnob);
	
	Init.AddItem(itemSimple, m_pEdge);
	
	Init.AddItem(itemSimple, m_pBack);

	Init.AddItem(itemSimple, m_pPanel);

	return TRUE;
	}

// Meta Data

void CPrimLegacySelector::AddMetaData(void)
{
	CPrimRich::AddMetaData();

	Meta_AddObject (Knob);
	Meta_AddObject (Panel);
	Meta_AddObject (Edge);
	Meta_AddObject (Back);
	Meta_AddInteger(ShowStates);

	Meta_SetName((IDS_RICH_SELECTOR));
	}

// Field Requirements

UINT CPrimLegacySelector::GetNeedMask(void) const
{
	return propFormat | propColor;
	}

// Implementation

void CPrimLegacySelector::DrawKnob(IGDI *pGDI, R2 Rect, int nPos, UINT uMode)
{
	int x1 = Rect.x1;
	int y1 = Rect.y1;
	int x2 = Rect.x2;
	int y2 = Rect.y2;

	pGDI->ResetAll();
	
	m_pEdge->DrawEllipse(pGDI, Rect, etWhole, uMode);
	
	DeflateRect(Rect, 3, 3);

	pGDI->SetBrushFore(m_pBack->GetColor());
	
	pGDI->FillEllipse(PassRect(Rect), etWhole);

	m_pEdge->DrawEllipse(pGDI, Rect, etWhole, uMode);
	
	double th = nPos / 180.0 * acos(double(-1));

	double r1 = min(x2-x1, y2-y1) * 0.25;
	double r2 = 3 * r1;

	double co = cos(th);
	double si = sin(th);

	int xp = (x1+x2)/2;
	int yp = (y1+y2)/2;

	P2 Pt[4];
	
	Pt[0].x = int(xp - si * r2/2 - co * r1/2 + 0.5);
	Pt[0].y = int(yp - co * r2/2 + si * r1/2 + 0.5);

	Pt[1].x = int(Pt[0].x + co * r1 + 0.5);
	Pt[1].y = int(Pt[0].y - si * r1 + 0.5);

	Pt[2].x = int(Pt[1].x + si * r2 + 0.5);
	Pt[2].y = int(Pt[1].y + co * r2 + 0.5);

	Pt[3].x = int(Pt[2].x - co * r1 + 0.5);
	Pt[3].y = int(Pt[2].y + si * r1 + 0.5);

	m_pKnob->FillPolygon(pGDI, Pt, elements(Pt), 0);
	
	m_pEdge->DrawPolygon(pGDI, Pt, elements(Pt), 0, uMode);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Two-State Selector Switch
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacySelectorTwo, CPrimLegacySelector);

// Constructor

CPrimLegacySelectorTwo::CPrimLegacySelectorTwo(void)
{
	m_uType  = 0x2D;

	m_Format = AfxRuntimeClass(CDispFormatFlag);
	}

// Overridables

void CPrimLegacySelectorTwo::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	int x1 = Rect.x1;
	int y1 = Rect.y1;
	int x2 = Rect.x2;
	int y2 = Rect.y2;

	if( m_ShowStates && (y2 - y1) > (x2 - x1) ) {

		DWORD Data   = 0;

		UINT  Type   = typeVoid;

		COLOR Color  = 0xFFFF;
		
		COLOR Shadow = 0xFFFF;

		FindColors( pGDI,
			    Color,
			    Shadow,
			    Data,
			    Type,
			    FALSE
			    );

		if( !m_TagColor ) {

			m_pPanel->FillRect(pGDI, Rect);
			}
		else
			pGDI->FillRect(x1, y1, x2, y2);

		Rect.y1 = y2 - (x2 - x1);		
		
		SelectFont(pGDI, m_Font);
		
		int yp = y1 + (Rect.y1 - y1 - pGDI->GetTextHeight(L"X")) / 2;

		if( yp >= y1 ) {

			CDispFormat *pFormat = NULL;

			if( GetDataFormat(pFormat) ) {

				CString s0 = pFormat->Format(0, typeInteger, fmtPad);

				CString s1 = pFormat->Format(1, typeInteger, fmtPad);

				pGDI->TextOut( x1 + 4, 
					       yp, 
					       UniVisual(s0)
					       );

				pGDI->TextOut( x2 - 4 - pGDI->GetTextWidth(s1), 
					       yp, 
					       UniVisual(s1)
					       );
				}
			}
		
		DeflateRect(Rect, 4, 4);
		}

	DrawKnob(pGDI, Rect, 45, uMode);
	}

// Meta Data

void CPrimLegacySelectorTwo::AddMetaData(void)
{
	CPrimLegacySelector::AddMetaData();

	Meta_SetName((IDS_RICH_SELECTOR_TWO));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Multi-State Selector Switch
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacySelectorMulti, CPrimLegacySelector);

// Constructor

CPrimLegacySelectorMulti::CPrimLegacySelectorMulti(void)
{
	m_uType       = 0x2E;

	m_Format      = AfxRuntimeClass(CDispFormatMulti);
	}

// Overridables

void CPrimLegacySelectorMulti::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	int x1 = Rect.x1;
	int y1 = Rect.y1;
	int x2 = Rect.x2;
	int y2 = Rect.y2;

	if( m_ShowStates && (y2 - y1) > (x2 - x1) ) {		

		DWORD Data   = 0;

		UINT  Type   = typeVoid;

		COLOR Color  = 0xFFFF;
		
		COLOR Shadow = 0xFFFF;

		FindColors( pGDI,
			    Color,
			    Shadow,
			    Data,
			    Type,
			    FALSE
			    );

		if( !m_TagColor ) {

			m_pPanel->FillRect(pGDI, Rect);
			}
		else
			pGDI->FillRect(x1, y1, x2, y2);

		Rect.y1 = y2 - (x2 - x1);		
		
		SelectFont(pGDI, m_Font);

		int cy = pGDI->GetTextHeight(L"X");

		int yp = y1 + (Rect.y1 - y1 - cy) / 2;

		if( yp >= y1 ) {

			UINT uCount = GetCount();

			UINT uIndex = 0;

			CDispFormat *pFormat = NULL;			

			if( uCount && GetDataFormat(pFormat) ) {

				int dx = (x2 - x1) / (2 * uCount);

				CItemList *pList = ((CDispFormatMulti *) pFormat)->m_pList;

				for( UINT n = 0; n < uCount; n++ ) {

					CDispFormatMultiEntry *pEntry = (CDispFormatMultiEntry *) pList->GetItem(n);

					if( !pEntry->m_pText->GetText().IsEmpty() ) {

						CString s = pEntry->m_pText->GetText();

						int    cx = pGDI->GetTextWidth(s);

						int    xp = x1 + dx * (1 + 2 * n) - cx/2;

						pGDI->TextOut(xp, yp, UniVisual(s));

						if( n == uIndex ) {
							
							pGDI->SelectPen(penFore);

							pGDI->DrawLine(xp, yp+cy, xp+cx, yp+cy);
							}
						}
					}
				}
			}
		
		DeflateRect(Rect, 4, 4);
		}

	DrawKnob(pGDI, Rect, 45, uMode);
	}

// Meta Data

void CPrimLegacySelectorMulti::AddMetaData(void)
{
	CPrimLegacySelector::AddMetaData();

	Meta_SetName((IDS_RICH_SELECTOR_MULTI));
	}

// Implementation

UINT CPrimLegacySelectorMulti::GetCount(void)
{
	UINT uCount = 0;

	CDispFormat *pFormat = NULL;			

	if( GetDataFormat(pFormat) ) {

		CItemList *pList = ((CDispFormatMulti *) pFormat)->m_pList;
		
		for( UINT n = 0; n < pList->GetItemCount(); n++ ) {
			
			CDispFormatMultiEntry *pEntry = (CDispFormatMultiEntry *) pList->GetItem(n);

			if( !pEntry->m_pText->GetText().IsEmpty() ) {
				
				uCount++;
				}
			}
		}

	return uCount;
	}

// End of File
