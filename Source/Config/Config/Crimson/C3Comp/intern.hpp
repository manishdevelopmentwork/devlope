
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "c3comp.hpp"

#include "intern.hxx"

#include <malloc.h>

//////////////////////////////////////////////////////////////////////////
//
// Foward Declarations
//

class CLexConst;

//////////////////////////////////////////////////////////////////////////
//
// Lexical Constant
//

class CLexConst
{
	public:
		// Constructors
		CLexConst(void);
		CLexConst(CLexConst const &That);
		CLexConst(C3INT  Value);
		CLexConst(C3REAL Value);
		CLexConst(PCTXT  pText);

		// Destructor
		~CLexConst(void);

		// Copy Assignment
		CLexConst const & operator = (CLexConst const &That);

		// Value Assignment
		CLexConst const & operator = (C3INT  Value);
		CLexConst const & operator = (C3REAL Value);
		CLexConst const & operator = (PCTXT  pText);

		// Attributes
		UINT	GetType(void) const;
		BOOL	IsType(UINT Type) const;
		CString Describe(void) const;

		// Operations
		void Empty(void);

		// Value Access
		C3INT   GetIntegerValue(void) const;
		C3REAL  GetRealValue(void) const;
		CString GetStringValue(void) const;

		// Forced Access
		C3INT  ForceInteger(void) const;
		C3INT  ForceLogical(void) const;
		C3REAL ForceReal(void) const;

		// Value Modification
		void SetValue(C3INT  Value);
		void SetValue(C3REAL Value);
		void SetValue(PCTXT  pText);

	protected:
		// Data Members
		UINT m_Type;

		// Value Union
		union {
			C3INT	m_Integer;
			C3REAL	m_Real;
			PTXT	m_pString;
			};

		// Implementation
		void AllocString(PCTXT pText);
		void EmptyString(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Foward Declarations
//

class CLexTables;
class CLexToken;

//////////////////////////////////////////////////////////////////////////
//
// Source Encoding
//

enum SourceCode
{
	srcLiteral	= 0x80,
	srcToken	= 0xF0,
	srcFunction	= 0xF1,
	srcIdent	= 0xF2,
	srcDirect	= 0xF3,
	srcProp1	= 0xF4,
	srcProp2	= 0xF5,
	srcUnicode	= 0xFE,
	srcUpper	= 0xFF,
	
	};

//////////////////////////////////////////////////////////////////////////
//
// Token Groups
//

enum TokenGroups
{
	groupEndOfText,
	groupWhiteSpace,
	groupConstant,
	groupKeyword,
	groupIdentifier,
	groupSeparator,
	groupOperator,
	groupGarbage,

	};

//////////////////////////////////////////////////////////////////////////
//
// Token Codes
//

enum TokenCode
{
	// Null Token

        tokenNull			= 0x00,

	// Specials

	tokenArgument			= 0x01,

	// Unary Operators

	tokenPostIncrement		= 0x02,
	tokenPostDecrement		= 0x03,
	tokenPreIncrement		= 0x04,
	tokenPreDecrement		= 0x05,
	tokenLogicalNot			= 0x06,
	tokenLogicalTest		= 0x07,
	tokenBitwiseNot			= 0x08,
	tokenUnaryMinus			= 0x09,
	tokenUnaryPlus			= 0x0A,
	tokenMakeInteger		= 0x4E,

	// Binary Operators

	tokenBitSelect			= 0x0B,
	tokenMultiply			= 0x0C,
	tokenDivide			= 0x0D,
	tokenRemainder			= 0x0E,
	tokenAdd			= 0x0F,
	tokenSubtract			= 0x10,
	tokenLeftShift			= 0x11,
	tokenRightShift			= 0x12,
	tokenLessThan			= 0x13,
	tokenLessOr			= 0x14,
	tokenGreaterThan		= 0x15,
	tokenGreaterOr			= 0x16,
	tokenEqual			= 0x17,
	tokenNotEqual			= 0x18,
	tokenBitwiseAnd			= 0x19,
	tokenBitwiseXor			= 0x1A,
	tokenBitwiseOr			= 0x1B,
	tokenLogicalAnd			= 0x1C,
	tokenLogicalOr			= 0x1D,
	tokenComma			= 0x1E,
	tokenCondition			= 0x4D,
	
	// Ternary Operators
	
	tokenQuestion			= 0x1F,
	
	// Assignment
	
	tokenOldAssignment		= 0x20,
	tokenNewAssignment		= 0x21,
	tokenMultiplyAssign		= 0x22,
	tokenDivideAssign		= 0x23,
	tokenRemainderAssign		= 0x24,
	tokenAddAssign			= 0x25,
	tokenSubtractAssign		= 0x26,
	tokenLeftShiftAssign		= 0x27,
	tokenRightShiftAssign		= 0x28,
	tokenAndAssign			= 0x29,
	tokenXorAssign			= 0x2A,
	tokenOrAssign			= 0x2B,

	// Keywords

	tokenBreak			= 0x2C,
	tokenCase			= 0x2D,
	tokenConst			= 0x2E,
	tokenContinue			= 0x2F,
	tokenDefault			= 0x30,
	tokenDispatch			= 0x50,
	tokenDo				= 0x31,
	tokenElse			= 0x32,
	tokenFalse			= 0x33,
	tokenFor			= 0x34,
	tokenIf				= 0x35,
	tokenReturn			= 0x36,
	tokenRun			= 0x4C,
	tokenSwitch			= 0x37,
	tokenTrue			= 0x38,
	tokenUsing			= 0x4F,
	tokenWhile			= 0x39,

	// Type Names

	tokenVoid			= 0x3A,
	tokenBool			= 0x3B,
	tokenInt			= 0x3C,
	tokenFloat			= 0x3D,
	tokenNumeric			= 0x3E,
	tokenString			= 0x3F,
	tokenClass			= 0x40,

	// Separators

	tokenBracketOpen		= 0x41,
	tokenBracketClose		= 0x42,
	tokenIndexOpen			= 0x43,
	tokenIndexClose			= 0x44,
	tokenBraceOpen			= 0x45,
	tokenBraceClose			= 0x46,
	tokenColon			= 0x47,
	tokenSemicolon			= 0x48,
	
	// Comments
	
	tokenBlockComment		= 0x49,
	tokenLineComment		= 0x4A,
	tokenWasComment			= 0x4B,

	// Unknown

	tokenUnknown			= 0x51,

	// End Marker

	tokenLastCode			= 0x52,

	// Specials

	tokenIndexString		= 0x70,
	tokenIndexExtended		= 0x71,
	tokenBitSelectLong		= 0x72,
	tokenGetProperty		= 0x73,
	
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CLexTables;
class CLexToken;
class CLexical;

//////////////////////////////////////////////////////////////////////////
//
// Access Macro
//

#define	afxLexTabs (CLexTables::FindInstance())

//////////////////////////////////////////////////////////////////////////
//
// Lexical Tables
//

class CLexTables
{
	public:
		// Object Location
		static CLexTables * FindInstance(void);

		// Constructor
		CLexTables(void);

		// Destructor
		~CLexTables(void);

		// Lookup
		UINT Lookup(UINT Group, CString const &Text);

		// Expansion
		CString Expand(UINT Code);

	protected:
		// Static Data
		static CLexTables *m_pThis;

		// Type Definitions
		typedef CMap < CString, CZeroed <UINT> > CForeMap;
		typedef CMap < CZeroed <UINT>, CString > CBackMap;

		// Data Members
		CForeMap m_KeywordMap;
		CForeMap m_SeparatorMap;
		CForeMap m_OperatorMap;
		CBackMap m_ReverseMap;

		// Implementation
		void MakeKeywordMap(void);
		void MakeSeparatorMap(void);
		void MakeOperatorMap(void);
		void AddReverseOperators(void);
		void AddReverseEntries(CForeMap const &Map);
	};

//////////////////////////////////////////////////////////////////////////
//
// Lexical Token
//

class CLexToken
{
	public:
		// Constructors
		CLexToken(void);
		CLexToken(UINT Code);
		CLexToken(CLexToken const &That);
		CLexToken(CLexConst const &Const);

		// Assignment
		CLexToken const & operator = (CLexToken const &That);

		// Core Attributes
		UINT   GetGroup(void) const;
		UINT   GetCode(void) const;
		CRange GetPos(void) const;
		UINT   GetStart(void) const;
		UINT   GetEnd(void) const;
		UINT   GetLine(void) const;
		
		// Attribute Tests
		BOOL IsEndOfText(void) const;
		BOOL IsGroup(UINT Group) const;
		BOOL IsCode(UINT Code) const;

		// Characterization
		BOOL IsBitSelect(void) const;
		BOOL IsCastOperator(void) const;
		BOOL IsUnaryOperator(void) const;
		BOOL IsPrefixOperator(void) const;
		BOOL IsPostfixOperator(void) const;
		BOOL IsBinaryOperator(void) const;
		BOOL IsIncDecOperator(void) const;
		BOOL IsAssignOperator(void) const;
		BOOL IsSimpleAssign(void) const;
		BOOL IsComplexAssign(void) const;
		BOOL IsLogicalOperator(void) const;
		BOOL IsLogicalSequence(void) const;
		BOOL IsStringAssign(void) const;
		BOOL IsStringBinary(void) const;
		BOOL IsFloatAssign(void) const;
		BOOL IsFloatBinary(void) const;
		UINT GetPriority(void) const;

		// Constant Access
		CLexConst const & GetConst(void) const;
		CLexConst       & GetConst(void);
		
		// Token Expansion
		CString Expand(void) const;

		// Token Description
		CString Describe(void) const;
		
		// Core Operations
		void Empty(void);
		void Set(UINT Group, UINT Code);
		void SetGroup(UINT Group);
		void SetCode(UINT Code);
		
		// Type Adjustments
		void AdjustPostfix(void);
		void AdjustPrefix(void);
		void AdjustBinary(void);

		// Table Management
		static void TableInit(void);
		static void TableTerm(void);

	public:
		// Static Data
		static BOOL m_fPrecFix;

		// Data Members
		UINT	  m_Group;
		UINT	  m_Code;
		CLexConst m_Const;
		CRange    m_Range;
		UINT	  m_uLine;

	protected:
		// Implementation
		void    FindGroup(void);
		CString DescribeIdentifier(void) const;
		CString DescribeSeparator(void) const;
		CString Describe(ENTITY ID) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Lexical Analyser
//

class CLexical
{
	public:
		// Constructor
		CLexical(void);

		// Attributes
		BOOL GetCommentState(void) const;
		
		// Operations
		void Attach(PCTXT pText);
		void Attach(PCTXT pText, UINT uSize);
		void SetSource(CByteArray &Source);
		BOOL GetToken(CError &Error, CLexToken &Token);
		void PartialMode(BOOL fComment);
		void AcceptPlcRef(void);
		
	protected:
		// Data Members
		PCTXT	     m_pText;
		UINT	     m_uSize;
		CError     * m_pError;
		CByteArray * m_pSource;
		TCHAR	     m_cData;
		UINT	     m_uPos;
		UINT	     m_uLine;
		UINT	     m_uThisPos;
		UINT	     m_uThisLine;
		UINT	     m_uPrevPos;
		UINT	     m_uPrevLine;
		CLexToken    m_Token;
		BOOL         m_fPLC;
		BOOL         m_fPartial;
		BOOL	     m_fComment;
		UINT         m_uPLC;

		// Error Helpers
		void CheckException(EXCEPTION Exception);
		
		// Main Analsyer
		void ParseToken(void);
		BOOL ParseEndOfText(void);
		BOOL ParseSpace(void);
		BOOL ParseConstant(void);
		BOOL ParseReal(INT64 Integer);
		BOOL ParseCharConst(void);
		BOOL ParseStringConst(void);
		BOOL ParseIdentifier(void);
		BOOL ParseSeparator(void);
		BOOL ParseOperator(void);
		BOOL ParseGarbage(void);

		// Comment Handling
		BOOL SkipBlockComment(void);
		BOOL SkipLineComment(void);
		void SkipWasComment(void);
		
		// Extended Characters
		TCHAR ParseCharacter(BOOL fChar);
		TCHAR ParseEscape(INT nRadix, BOOL fWide);
		
		// General Support
		void  ReadChar(BOOL fStore);
		PCTXT Find(PCTXT pList) const;
		void  ThrowError(void);
		
		// Characterisers
		BOOL IsBreak(void) const;
		BOOL IsSpace(void) const;
		BOOL IsNumeric(void) const;
		BOOL IsAlpha(void) const;
		BOOL IsAlphaNum(void) const;
		BOOL IsIdentFirst(void) const;
		BOOL IsIdentRest(void) const;
		BOOL IsPlcRef(BOOL fFirst) const;
		
		// Conversion
		TCHAR ToUpper(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CParseNode;
class CBasicParser;
class CExprParser;
class CFuncParser;

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CArray <CParseNode> CParseTree;

//////////////////////////////////////////////////////////////////////////
//
// Identifier Usages
//

enum IdentUsage
{
	usageVariable,
	usageFunction,
	usageDirectRef,
	usageFullRef,
	usageLocal,
	usageProperty
	
	};

//////////////////////////////////////////////////////////////////////////
//
// Parse Tree Node
//

// cppcheck-suppress copyCtorAndEqOperator

class CParseNode : public CLexToken
{
	public:
		// Constructor
		CParseNode(void);
		CParseNode(CParseNode const &That);
		CParseNode(UINT uOrder, CLexToken const &Token, UINT Type);

		// Attributes
		UINT	GetOrder(void) const;
		UINT	GetType(void) const;
		CString Describe(void) const;

	public:
		// Data Members
		UINT m_uOrder;
		UINT m_Type;
	};

//////////////////////////////////////////////////////////////////////////
//
// Basic Parser
//

class CBasicParser
{
	public:
		// Constructor
		CBasicParser(void);
		
		// Token Position
		CRange GetTokenPos  (void) const;
		UINT   GetTokenStart(void) const;
		UINT   GetTokenEnd  (void) const;
		UINT   GetTokenLine (void) const;

	protected:
		// Data Members
		CLexical  m_Lex;
		CLexToken m_Token;
		CLexToken m_Before;
		CError *  m_pError;
		
		// Overridables
		virtual BOOL FilterToken(void);

		// Implementation
		void GetToken(void);
		void ThrowError(CLexToken const &Token);
		void ThrowError(void);
		void Expected(ENTITY ID);
		void Expected(PCTXT pText);
		void MatchCode(UINT Code);
	};

//////////////////////////////////////////////////////////////////////////
//
// Expression Parser
//

class CExprParser : public CBasicParser
{
	public:
		// Constructor
		CExprParser(void);

		// Destructor
		~CExprParser(void);

		// Attributes
		UINT GetDepth(void) const;
		UINT GetLocal(void) const;

		// Result Access
		CByteArray & GetSource(void);
		CParseTree & GetTree(void);
		CLongArray & GetRefList(void);

	protected:
		// Type Definitions
		typedef CMap   <CString, DWORD> CLocalMap;
		typedef CArray <INDEX>		CLocalCtx;

		// Data Members
		INameServer   *	m_pName;
		CLexToken	m_CodeStack[32];
		CTypeDef	m_TypeStack[32];
		UINT		m_uCodePtr;
		UINT		m_uTypePtr;
		BOOL		m_fActive;
		BOOL		m_fDispatch;
		UINT		m_uDepth;
		UINT		m_uLocal;
		CLocalCtx	m_LocalCtx;
		CLocalMap	m_LocalMap;
		CStringArray    m_Using;
		CByteArray	m_Source;
		CParseTree	m_Tree;
		CLongArray	m_Refs;
		UINT		m_Hint;
		UINT            m_Switch;

		// Operations
		BOOL ParseExpr(CTypeDef const &In, CTypeDef &Out);

		// Initialization
		void InitParser(void);
		void ReadParam(CFuncParam const *pParam, UINT uParam);
		void ReadUsing(CStringArray const &Using);

		// Expression Parser
		BOOL ParseExpression(CLexToken &Value);
		BOOL ParseSection(BOOL fArg, CLexToken &Value);
		void ParseNested(UINT End);
		BOOL ParseOperandSeq(BOOL fNeed);
		BOOL ParseOperand(void);
		BOOL ParseBinaryOperator(void);
		BOOL ParseTernaryOperator(void);
		BOOL ParsePrefixOperator(void);
		void ParseCastOperator(void);
		void ParseCastFunction(void);
		void ParsePostfixSeq(void);
		BOOL ParsePostfixOperator(void);
		BOOL ParseIndexOperator(void);
		void PurgeCode(CLexToken const &ThisToken);
		void PurgeAndPush(CLexToken const &ThisToken);
		void ParseIdentifier(void);
		BOOL ParseLocal(UINT uPos, PCTXT pName);
		BOOL ParseVariable(UINT uPos, CString &Name, WORD *pID, BOOL fMake);
		void ParseRun(void);
		void ParseFunction(CLexToken const &Save, UINT uPos, PCTXT pName, UINT Type);
		void ParseDirectRef(void);

		// Output Processing
		void ProcessTrue(void);
		void ProcessFalse(void);
		void ProcessConstant(void);
		void ProcessLocal(UINT Slot, UINT Type);
		void ProcessVariable(WORD ID, CTypeDef const &Type);
		void ProcessProp(WORD PropID, CTypeDef const &Type);
		void ProcessFunction(WORD ID, CTypeDef const &Type, UINT uCount);
		void ProcessParam(PCTXT pFunc, PCTXT pName, CTypeDef const &Type, CLexToken const &Token);
		void ProcessDirectRef(DWORD ID, CTypeDef const &Type);
		void ProcessOperator(CLexToken const &Token);
		void ProcessAssign(CLexToken const &Token);
		void ProcessBinary(CLexToken const &Token);
		void ProcessTernary(CLexToken const &Token);
		void ProcessUnary(CLexToken const &Token);
		void ProcessIncDec(CLexToken const &Token);
		void ProcessCast(CLexToken const &Token);
		void ProcessBitSelect(CLexToken const &Token);
		void ProcessSequence(CLexToken const &Token);
		void ProcessCondition(CLexToken const &Token);
		void ProcessIndex(CLexToken const &Token);
		void ProcessNode(CParseNode const &Node);

		// Node Flipping
		void ReverseNode(void);
		UINT GetNodeDepth(UINT uPos);
		UINT GetDepthFrom(UINT &uPos);
		
		// Code Stack
		void              ClearCodeStack(void);
		void              CheckCodeStack(void);
		void              PushCode(CLexToken const &Token);
		CLexToken const & ReadCode(UINT uBack) const;
		CLexToken const & PullCode(void);

		// Type Stack
		void             ClearTypeStack(void);
		void             CheckTypeStack(void);
		void             PushType(CTypeDef const &Type);
		void             PushType(UINT Type, UINT Flags);
		CTypeDef const & ReadType(UINT uBack);
		CTypeDef const & PullType(void);

		// Type Combination
		UINT GetBasicType(UINT Type);
		UINT GetResultType(UINT Type1, UINT Type2);

		// Token Characterisation
		BOOL IsPrefixOperator(void);
		BOOL IsPostfixOperator(void);
		BOOL IsBinaryOperator(void);
		BOOL IsRightToLeft(UINT Level);

		// Error Checking
		BOOL CheckScalar(UINT n, CLexToken const &Token, ENTITY Side, BOOL fVoid);
		BOOL CheckScalar(CTypeDef const &Type, CLexToken const &Token, ENTITY Side, BOOL fVoid);
		BOOL CheckWriteable(CTypeDef const &Type, CLexToken const &Token, ENTITY Side);
		BOOL CheckNumeric(CTypeDef const &Type, CLexToken const &Token, ENTITY Side);
		BOOL CheckInteger(CTypeDef const &Type, CLexToken const &Token, ENTITY Side);
		BOOL CheckString(CTypeDef const &Type, CLexToken const &Token, ENTITY Side);
		BOOL CheckNumeric(CTypeDef const &Type, CLexToken const &Token);
		BOOL CheckInteger(CTypeDef const &Type, CLexToken const &Token);
		BOOL CheckString(CTypeDef const &Type, CLexToken const &Token);
		BOOL CheckVoid(CTypeDef const &Type, CLexToken const &Token);

		// Conversion Check
		void CheckConvert(PCTXT pWhat, CTypeDef const &From, CTypeDef const &Type, CLexToken const &Token);

		// Common Errors
		void NotWithString(CLexToken const &Token);
		void NotWithReal(CLexToken const &Token);
		void NotWithBitRef(CLexToken const &Token);
		void NotGoodCast(CLexToken const &Token);
		void NoComplexAssign(CLexToken const &Token);

		// Source Building
		void AppendSource(CString const &Data, UINT uAdd);
		void InsertSource(UINT uPos, CString const &Data, UINT uAdd);
		void InsertSource(UINT uPos, PCBYTE pData, UINT uSize);

		// Object Naming
		CString GetObjectName(UINT Type);
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Parser
//

class CFuncParser : public CExprParser
{
	public:
		// Constructor
		CFuncParser(void);

		// Destructor
		~CFuncParser(void);

		// Operations
		BOOL ParseCode(CCompileIn const &In, CCompileOut &Out, BOOL fFunc);

	protected:
		// Data Members
		CTypeDef m_Type;
		UINT	 m_uLoop;
		UINT	 m_uCase;
		
		// Operations
		BOOL ParseFunc(CTypeDef const &In, CTypeDef &Out);

		// Initialization
		void InitParser(void);

		// Statement Parser
		void ParseStatement(void);
		void ParseKeywordStatement(void);
		void ParseLocalVar(UINT Type);
		void CheckLocalVar(UINT Type);
		void ParseBreakStatement(CLexToken Token);
		void ParseContinueStatement(CLexToken Token);
		void ParseDoStatement(CLexToken Token);
		void ParseForStatement(CLexToken Token);
		void ParseIfStatement(CLexToken Token);
		void ParseReturnStatement(CLexToken Token);
		void ParseSwitchStatement(CLexToken Token);
		void ParseUsingStatement(CLexToken Token);
		void ParseWhileStatement(CLexToken Token);
		void ParseSeparatorStatement(void);
		void ParseCompoundStatement(CLexToken Token);
		void ParseNullStatement(CLexToken Token);
		void ParseExpressionStatement(CLexToken Token);
		UINT ParseConstantExpression(void);
		void ParseControlExpression(void);
		BOOL ParseOptionalExpression(BOOL fCheck);
		void ParseInitialData(PCTXT pName, UINT Type);

		// Output Processing
		void ProcessCompound(UINT uCount);
		UINT ProcessSwitchBlock(UINT &uBlock);

		// Switch Errors
		void RepeatedCase(CLexToken const &Token, UINT n);
		void MultipleDefaults(void);
		void CaseAfterDefault(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Syntax Parser
//

class CSyntaxParser : public CBasicParser
{
	public:
		// Constructor
		CSyntaxParser(void);

		// Operations
		BOOL ParseLine(CError &Error, BOOL &fComment, PCTXT pText, UINT uSize);
		
		// List Building
		BOOL BuildColorList(CArray <CColorSpan> &List);
		BOOL BuildTokenList(CArray <CTokenSpan> &List);

	protected:
		// Context
		struct CTokenCtx
		{
			UINT m_Group;
			UINT m_Code;
			UINT m_Type;
			UINT m_uCount;
			};

		// Data Members
		BOOL		   m_fComment;
		CArray <CTokenCtx> m_List;

		// Overridables
		BOOL FilterToken(void);

		// Implementation
		UINT ToColorSpan(CTokenCtx const &Token); 
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCodeOptimizer;
class CCodeGenerator;

//////////////////////////////////////////////////////////////////////////
//
// Tree Optimizer
//

class CTreeOptimizer
{
	public:
		// Constructor
		CTreeOptimizer(CParseTree &Tree);

		// Operations
		BOOL Optimize(CError &Error);
		
	protected:
		// Parse Tree
		CParseTree & m_Tree;
		CError	   * m_pError;
		UINT         m_uError;
		UINT	     m_uPos;

		// Optimization
		UINT OptimizeNode(void);
		UINT OptimizeNode(UINT uPos);

		// Constant Statements
		UINT CheckConstStatement(UINT uPos);
		UINT CheckConstIf(UINT uPos, BOOL fTest);
		UINT CheckConstWhile(UINT uPos, BOOL fTest);
		UINT CheckConstDo(UINT uPos, BOOL fTest);

		// Constant Index
		UINT CheckConstIndex(UINT uPos);

		// Constant Bit
		UINT CheckConstBit(UINT uPos);

		// Ternary Operator
		UINT CheckTernary(UINT uPos);

		// Compares to Zero
		UINT CheckCompare(UINT uPos);

		// Sequence Operator
		UINT CheckSequence(UINT uPos);

		// Logical Sequences
		UINT CheckLogical(UINT uPos);
		UINT CheckLogical(UINT uPos, BOOL fTest);

		// Identity Operations
		UINT CheckIdentities(UINT uPos);

		// Constant Folding
		UINT FoldConstants(UINT uPos);
		BOOL FoldOperator(UINT Code, C3INT  a1, C3INT  a2, C3INT  &r);
		BOOL FoldOperator(UINT Code, C3REAL a1, C3REAL a2, C3REAL &r);
		BOOL FoldOperator(UINT Code, C3INT  a, C3INT  &r);
		BOOL FoldOperator(UINT Code, C3REAL a, C3REAL &r);

		// Node Characterisation
		BOOL IsActiveNode(UINT uPos);
		BOOL IsActiveFrom(UINT &uPos);
		UINT GetNodeDepth(UINT uPos);
		UINT GetDepthFrom(UINT &uPos);

		// Token Characterisation
		BOOL IsActiveOperator(UINT Code);
		BOOL HasIdentity0(UINT Code);
		BOOL HasIdentity1(UINT Code);
		BOOL DoesCommute(UINT Code);
		BOOL ZeroFromLeft(UINT Code);
		BOOL ZeroFromRight(UINT Code);

		// Tree Editing
		void ReplaceWithNull(UINT uPos);
		void ReplaceWithUnary(UINT uPos, UINT Code, UINT Type);
		void ReplaceWithBinary(UINT uPos, UINT Code, UINT Type);
		void ReplaceWithConst(UINT uPos, CLexConst const &Const);
		void ReplaceWithFullRef(UINT uPos, DWORD ID, UINT Type);
		void DeleteNode(UINT uPos, UINT uDepth);

		// Implementation
		BOOL IsConst0(CLexConst const &Const);
		BOOL IsConst1(CLexConst const &Const);
		void CheckBit(C3INT nBit);
		void ThrowError(void);
		void ShowTree(PCTXT pName);
	};

//////////////////////////////////////////////////////////////////////////
//
// Code Generator
//

class CCodeGenerator
{
	public:
		// Constructor
		CCodeGenerator(CParseTree &Tree);

		// Preparation
		void Prepare(UINT uDepth, UINT uLocal, CCompileIn const &In);

		// Operations
		BOOL GenerateCode(CCompileOut &Out, BOOL fFunc);

		// Byte Code Access
		CByteArray const & GetCode(void) const;
		
	protected:
		// Loop Context
		struct CLoopCtx
		{
			UINT m_uLabel[2];
			BOOL m_fEmit [2];
			};

		// Loop Slots
		enum LoopSlot
		{
			loopBreak    = 0,
			loopContinue = 1,
			};

		// Data Members
		CParseTree & m_Tree;
		CTypeDef     m_Type;
		CError	   * m_pError;
		BOOL         m_fOptim;
		BOOL	     m_fDebug;
		WORD	     m_wDebug;
		UINT	     m_uInit;
		CByteArray   m_Code;
		UINT	     m_uPos;
		UINT	     m_uLabel;
		UINT	     m_uExit;
		CLoopCtx   * m_pLoop;
		CByteArray   m_LocString;

		// Debug Information
		CString		    m_DebugScope;
		CArray <CFuncParam> m_DebugParam;

		// Statement Walking
		void WalkStatement(void);
		void WalkNullStatement(void);
		void WalkExprStatement(void);
		void WalkCompStatement(void);
		void WalkIfStatement(void);
		void WalkWhileStatement(void);
		void WalkDoStatement(void);
		void WalkForStatement(void);
		void WalkBreakStatement(void);
		void WalkContinueStatement(void);
		void WalkSwitchStatement(void);
		void WalkReturnStatement(void);
		void WalkAllocLocal(UINT Type);
		void FreeLocals(UINT uStart);
		void ExitFunction(void);

		// Expression Walking
		void WalkExpression(UINT Type);
		void WalkNullExpression(UINT Type);
		void WalkFunction(UINT Type);
		void WalkVariable(UINT Type);
		void WalkFullRef(UINT Type);
		void WalkProperty(UINT Type);
		void WalkConstant(UINT Type);
		void WalkCast(UINT Type, UINT Cast);
		void WalkSimpleAssign(UINT Type);
		void WalkComplexAssign(UINT Type);
		void WalkPrefix(UINT Type);
		void WalkPostfix(UINT Type);
		void WalkIndexArray(UINT Type, BOOL fExtend);
		void WalkIndexString(UINT Type);
		void WalkBitSelect(UINT Type);
		void WalkGetProperty(UINT Type);
		void WalkLogicalTest(UINT Type);
		void WalkLogical(UINT Type, BOOL fTest);
		void WalkSequence(UINT Type);
		void WalkTernary(UINT Type);
		void WalkCondition(UINT Type);
		void WalkOperator(UINT Type);

		// Code Generation
		void EmitHeader(UINT Type, UINT uDepth, UINT uLocal);
		void EmitValueConversion(UINT From, UINT Type);
		void EmitNullValue(UINT Type);
		void EmitConstant(CLexConst const &Const, UINT Type);
		void EmitLoadAddress(CParseNode const &Node);
		void EmitLoadValue(CParseNode const &Node);
		void EmitSaveValue(CParseNode const &Node);
		void EmitLoadProperty(UINT Type);
		void EmitLoadIndirect(UINT Type);
		void EmitSaveIndirect(UINT Type);
		void EmitOperator(UINT Code, UINT Type);
		void EmitIntegerOperator(UINT Code);
		void EmitRealOperator(UINT Code);
		void EmitStringOperator(UINT Code);
		void EmitConversion(UINT From, UINT Type);
		void EmitFromInteger(UINT Type);
		void EmitFromLogical(UINT Type);
		void EmitFromReal(UINT Type);
		void EmitFromString(UINT Type);
		void EmitDupTop(UINT Type);
		void EmitDup3rd(UINT Type);
		void EmitPop(UINT Type);

		// Low-Level Coding
		void EmitCode(BYTE bCode);
		void EmitByte(BYTE bCode);
		void EmitWord(WORD wCode);
		void EmitLong(DWORD dwCode);
		void EmitBool(BOOL fState);
		void EmitLocalName(CParseNode const &Node);
		void EmitName(CParseNode const &Node);

		// Branch Support
		UINT EmitFwdJump(void);
		UINT EmitFwdBranch(BOOL fTest);
		void EmitJump(UINT uLabel);
		void EmitBranch(UINT uLabel, BOOL fTest);
		BOOL EmitLabel(UINT uLabel);
		UINT EmitLabel(void);

		// Assignment Conversion
		UINT GetEquivalent(UINT Code);

		// Operand Characterization
		BOOL IsNullExpression(void);
		UINT GetRightType(CParseNode const &Node);
		BOOL CanSaveValue(CParseNode const &Node);

		// Loop Support
		PVOID LoopCreate(UINT uBreak, UINT uContinue);
		BOOL  LoopCheckBranch(void);
		void  LoopEmit(UINT uSlot, BOOL fCond);
		BOOL  LoopFixup(UINT uSlot);
		void  LoopDelete(PVOID pLast);

		// Final Passes
		void Optimize(void);
		void FixJumps(void);

		// Opcode Scanning
		BOOL IsBranch(BYTE bCode);
		BYTE GetOpcodeSize(BYTE bCode);
		UINT GetNextOpcode(UINT uPos);

		// Implementation
		void CheckBit(C3INT nBit);
		void SkipNode(void);
		void ThrowInternal(void);
		void ThrowError(void);
		void ShowCode(PCTXT p);

		// Debug Support
		void InitDebugData(UINT uLocal, CCompileIn const &In);
		void AddDebugData(void);
		void AddDebugString(CByteArray &Data, CLongArray &Refs, CString const &Text);
		void AddDebugParam(CByteArray &Data, CLongArray &Refs, CFuncParam const &Param);
		void FixDebugStrings(CLongArray const &Refs, UINT uBase);
	};

//////////////////////////////////////////////////////////////////////////
//
// Peephole Code Optimizer
//

class CCodeOptimizer
{
	public:
		// Constructor
		CCodeOptimizer(CByteArray &Code, UINT uInit);

		// Operations
		BOOL Optimize(CError &Error);

	protected:
		// Data Members
		CByteArray & m_Code;
		UINT         m_uInit;
		CError	   * m_pError;

		// Optimizations
		BOOL CheckJumpOverJump(BYTE bCode, UINT uPos);
		BOOL CheckInvertAndJump(BYTE bCode, UINT uPos);
		BOOL CheckTestAndJump(BYTE bCode, UINT uPos);
		BOOL CheckDoubleInvert1(BYTE bCode, UINT uPos);
		BOOL CheckDoubleInvert2(BYTE bCode, UINT uPos);
		BOOL CheckNegativeAdd(BYTE bCode, UINT uPos);
		BOOL CheckInvertCompare(BYTE bCode, UINT uPos);
		BOOL CheckDoubleLoad(BYTE bCode, UINT uPos);
		BOOL CheckSaveAndLoad(BYTE bCode, UINT uPos);
		BOOL CheckDeadPutLocal(BYTE bCode, UINT uPos);
		BOOL CheckDeadValue(BYTE bCode, UINT uPos);
		BOOL CheckDeadLoad(BYTE bCode, UINT uPos);
		BOOL CheckDeadCodeAfterReturn(BYTE bCode, UINT uPos);
		BOOL CheckDeadCodeAfterBranch(BYTE bCode, UINT uPos);
		BOOL CheckNullJump(BYTE bCode, UINT uPos);
		BOOL CheckDupPop(BYTE bCode, UINT uPos);

		// Implementation
		BOOL IsNumericValue(BYTE bCode);
		BOOL IsStringValue(BYTE bCode);
		BOOL IsBranch(BYTE bCode);
		BOOL IsCondition(BYTE bCode);
		BYTE GetOpcodeSize(BYTE bCode);
		UINT GetNextOpcode(UINT uPos);
	};

// End of File

#endif
