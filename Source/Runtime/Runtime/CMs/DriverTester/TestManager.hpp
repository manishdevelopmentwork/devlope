
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_TestManager_HPP

#define INCLUDE_TestManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Driver Test Manager
//

class CTestManager : public IClientProcess,
		     public ICommsDevice,
		     public ISlaveHelper,
		     public IDiagProvider
{
	public:
		// Constructor
		CTestManager(void);

		// Destructor
		~CTestManager(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// ICommsDevice
		LPCBYTE METHOD GetConfig(void);
		UINT    METHOD GetConfigSize(void);
		void    METHOD SetContext(PVOID pData);
		PVOID   METHOD GetContext(void);

		// ISlaveHelper
		void  METHOD Bind (ICommsDevice *pDevice);
		CCODE METHOD Read (AREF Addr, PDWORD pData, UINT uCount);
		CCODE METHOD Read (AREF Addr, PDWORD pData, IMetaData **ppm);
		CCODE METHOD Write(AREF Addr, PDWORD pData, UINT uCount);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Stats
		struct CError 
		{
			UINT m_uGood;
			UINT m_uHard;
			UINT m_uBusy;
			UINT m_uSilent;
			UINT m_uRetry;
			UINT m_uData;
			UINT m_uConfig;
			};

		// Data Members
		ULONG          m_uRefs;
		UINT	       m_uProv;
		BOOL	       m_fOpen;
		IPortObject  * m_pPort;
		IThread      * m_pThread;
		IEvent       * m_pStop;
		ICommsTester * m_pTester;
		ICommsDriver * m_pDriver;
		ICommsMaster * m_pMaster;
		ICommsSlave  * m_pSlave;
		PVOID          m_pContext;
		BYTE           m_bConfig[8192];
		BYTE           m_bDevice[8192];
		UINT	       m_uConfig;
		UINT           m_uDevice;
		CError	       m_Stats;

		// Operations
		void Open(void);
		void Close(void);

		// Driver
		BOOL DriverCreate(void);
		BOOL DriverLoad(void);
		BOOL DriverOpen(void);
		BOOL DriverService(void);
		BOOL DriverClose(void);
		BOOL DriverFree(void);
		BOOL DeviceOpen(void);
		BOOL DeviceClose(void);
		void InitConfig(void);
		void InitDevice(void);

		// Tester
		BOOL TesterCreate(void);
		BOOL TesterFree(void);

		// Statistics
		void ClearStats(void);
		void UpdateStats(CCODE Code);

		// Diagnostics
		BOOL DiagRegister(void);
		BOOL DiagRevoke(void);
		UINT DiagOpen (IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagClose(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagStats(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
