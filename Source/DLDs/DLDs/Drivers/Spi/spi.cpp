#include "intern.hpp"

#include "spi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SPI Master Driver
//

// Instantiator

INSTANTIATE(CSpiDriver);

// Constructor

CSpiDriver::CSpiDriver(void)
{
	m_Ident         = DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;
	}

// Destructor

CSpiDriver::~CSpiDriver(void)
{
	}

// Configuration

void MCALL CSpiDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CSpiDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CSpiDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSpiDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CSpiDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDevID = GetByte(pData);

			m_pCtx->m_bAddr  = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}
		

CCODE MCALL CSpiDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CSpiDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 3;
		
	Addr.a.m_Offset = 0x2020;
		
	Addr.a.m_Type   = addrByteAsByte;

	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CSpiDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsValid(Addr.a.m_Offset)) {
	
		MakeMin(uCount, 1);
	
		for( UINT uTry = 0; uTry < 3; uTry++ ) {
	
			PollRequest(Addr.a.m_Offset);

			if( Transact(recvPoll) ) {

				switch( Addr.a.m_Type ) {

					case addrByteAsByte:	return	GetByteData(pData, uCount);	break;
					case addrWordAsWord:	return	GetWordData(pData, uCount);	break;
					case addrLongAsLong:	
					case addrLongAsReal:	return	GetLongData(pData, uCount);	break;
					}
				}
			}

		return CCODE_ERROR; 
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE MCALL CSpiDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsValid(Addr.a.m_Offset)) {
	
		for( UINT uTry = 0; uTry < 3; uTry++ ) {
	
			SelectRequest(Addr.a.m_Offset);

			if( Transact(recvSel) ) { 

				SelectSequence(Addr.a.m_Type, pData, uCount);

				if( Transact(recvAck) ) {

					Begin();

					AddByte(EOT);
					
					Send();

					return uCount > 1 ? 2 : 1;
					}

				break;
				} 
			} 

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

// Implementation

void CSpiDriver::PollRequest(UINT uOffset)
{
	Begin();

	AddByte(EOT);

	AddByte(m_pCtx->m_bDevID);

	AddByte(m_pCtx->m_bAddr);

	AddByte((uOffset & 0xFF00) >> 8);

	AddByte(uOffset & 0xFE);

	AddByte(0x20);

	AddByte(ENQ);
	}

void CSpiDriver::SelectRequest(UINT uOffset)
{
	Begin();

	AddByte(EOT);

	AddByte(m_pCtx->m_bDevID);

	AddByte(m_pCtx->m_bAddr);

	AddByte((uOffset & 0xFF00) >> 8);

	AddByte((uOffset & 0xFF) | 0x1);

	AddByte(0x20);

	AddByte(ENQ);
	}

void CSpiDriver::SelectSequence(UINT uType, PDWORD pData, UINT uCount)
{
	Begin();

	AddByte(DLE);

	AddByte(STX);

	switch( uType ) {

		case addrByteAsByte:	AddByte(pData[0] & 0x00FF, TRUE);	break;
		case addrWordAsWord:	AddWord(pData[0] & 0xFFFF, TRUE);	break;
		case addrLongAsLong:	
		case addrLongAsReal:	AddLong(pData[0]         , TRUE);	break;
		}
	
	AddByte(DLE);

	AddByte(ETX);

	AddWord(m_CRC.GetValue());
	}

void CSpiDriver::Begin(void)
{
	m_uPtr = 0;

	m_CRC.Clear();
	}

void CSpiDriver::AddByte(BYTE bByte, BOOL fData)
{
	if( fData && (bByte == DLE || bByte == STX) ) { 

		m_bTx[m_uPtr++] = DLE;
		}
	
	m_bTx[m_uPtr++] = bByte;

	AddToCheck(bByte, fData);
	}

void CSpiDriver::AddWord(WORD wWord, BOOL fData)
{
	AddByte(HIBYTE(wWord), fData);

	AddByte(LOBYTE(wWord), fData);
	}

void CSpiDriver::AddLong(DWORD dwWord, BOOL fData)
{
	AddWord(HIWORD(dwWord), fData);

	AddWord(LOWORD(dwWord), fData);
	}

void CSpiDriver::AddToCheck(BYTE bByte, BOOL fData)
{
	if( !fData ) {

		switch( bByte ) {

			case DLE:
			case SYN:
			case SOH:
			case STX:

				return;
			}
		}

	m_CRC.Add(bByte);
	}

UINT CSpiDriver::GetByteData(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		if( m_uPtr - 4 < 7 + n ) {

			return n + 1;
			}
		
		pData[n] = m_bRx[7 + n];
		}

	return uCount;
	}

UINT CSpiDriver::GetWordData(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		if( m_uPtr - 4 < 7 + n * 2 ) {

			return n + 1;
			}
		
		WORD x   = PU2(m_bRx + 7)[n];
			
		pData[n] = LONG(SHORT(MotorToHost(x)));
		}

	return uCount;
	}

UINT CSpiDriver::GetLongData(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		if( m_uPtr - 4 < 7 + n * 4 ) {

			return n + 1;
			} 
		
		DWORD x   = PU4(m_bRx + 7)[n];
			
		pData[n] = MotorToHost(x);
		}

	return uCount;
	}

// Transport

BOOL CSpiDriver::Transact(UINT uMode)
{
	if( Send() && Recv(uMode) && Check() ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CSpiDriver::Send(void)
{
	m_pData->ClearRx();

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_uPtr; u++ ) {
	
		AfxTrace("%2.2x ", m_bTx[u]);
		}

*/	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	
	return TRUE;
	}

BOOL CSpiDriver::Recv(UINT uMode)
{
	UINT uTimer = 0;

	UINT uByte  = 0;
	
	BOOL fDLE   = FALSE;

	BOOL fMsg   = FALSE;

	UINT uCheck = 0;

	m_uPtr = 0;

//	AfxTrace("\nRx : ");

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {

		if ( ( uByte = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

//		AfxTrace("%2.2x ", uByte);

		if( uMode == recvEOT && uByte == EOT ) {

			return TRUE;
			}

		if( uMode != recvSel && m_uPtr == 0 ) {

			if( uByte == DLE ) {

				fDLE = TRUE;

				continue;
				}

			if( fDLE ) {

				switch( uByte ) {

					case SYN:	
					case SOH:	
					case STX:	
						
						fDLE = FALSE; 

					case DLE:

						fMsg = TRUE;	
					
						continue;

					case 0x31:
						
						if( uMode == recvAck) {

							return TRUE;
						}
					}

				return FALSE;
				}
			}

		if( m_uPtr == 1 && uMode == recvSel ) {

			if( uByte == NAK ) {

				return FALSE;
				}
			}

		if( fMsg || uMode == recvSel ) {

			if( uCheck > 0 ) {

				m_bRx[m_uPtr++] = uByte;

				if( m_uPtr > uCheck + 2 ) {
				
					return TRUE;
					}

				continue;
				}	

			if( uByte == DLE ) {

				if( fDLE ) {

					m_bRx[m_uPtr++] = uByte;

					fDLE = FALSE;

					continue;
					}

				fDLE = TRUE;

				continue;
				}

			if( fDLE ) {

				switch( uByte ) {

					
					case ETB:
					case ETX:
					case ENQ:

						uCheck = m_uPtr;

						m_bRx[m_uPtr++] = uByte; 

						fDLE = FALSE;

						continue;
						
					case STX:

						m_bRx[m_uPtr++] = uByte; 

						fDLE = FALSE;

						continue;

					case 0x30:

						return !memcmp(m_bTx + 1, m_bRx, 4);
				       	}

				return FALSE;
				}

			m_bRx[m_uPtr++] = uByte;
			}
		}

	return FALSE;
	}

BOOL CSpiDriver::Check(void)
{
	if( m_uPtr > 6 ) {
	
		if( !memcmp(m_bTx + 1, m_bRx, 5) ) {

			m_CRC.Clear();

			for( UINT u = 0; u < m_uPtr - 2; u++ ) {

				m_CRC.Add(m_bRx[u]);
				}

			UINT uCRC = m_CRC.GetValue();

			if( uCRC > 0 ) {

				if( (uCRC & 0xFF) == m_bRx[m_uPtr - 1] ) {

					if( (uCRC >> 8) == m_bRx[m_uPtr - 2] )  {

						Begin();

						AddByte(DLE);

						AddByte(0x31);

						Transact(recvEOT);

						return TRUE;
						}
					}
			
				return FALSE;
				}

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CSpiDriver::IsValid(UINT uOffset)
{
	return uOffset >= 0x2020;
	}

// End of file
