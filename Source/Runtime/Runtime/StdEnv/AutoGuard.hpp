
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AutoGuard_HPP

#define	INCLUDE_AutoGuard_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Automatic Thread Guard
//

class DLLAPI CAutoGuard
{
public:
	// Constructors

	STRONG_INLINE CAutoGuard(void)
	{
		m_fGuard = TRUE;

		GuardThread(TRUE);
	}

	STRONG_INLINE CAutoGuard(BOOL fGuard)
	{
		if( (m_fGuard = fGuard) ) {

			GuardThread(TRUE);
		}
	}

	// Destructor

	STRONG_INLINE ~CAutoGuard(void)
	{
		if( m_fGuard ) {

			GuardThread(FALSE);
		}
	}

	// Operations

	STRONG_INLINE void Guard(BOOL fGuard)
	{
		if( m_fGuard != fGuard ) {

			GuardThread((m_fGuard = fGuard));
		}
	}

	STRONG_INLINE void Guard(void)
	{
		Guard(TRUE);
	}

	STRONG_INLINE void Free(void)
	{
		Guard(FALSE);
	}

protected:
	// Data Members
	BOOL m_fGuard;

private:
	// No Assign or Copy

	void operator = (CAutoGuard const &That) const {};

	CAutoGuard(CAutoGuard const &That) {};
};

// End of File

#endif
