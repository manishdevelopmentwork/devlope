
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Et3 Configuration Support
//

// Constructor

CConfigLoader::CConfigLoader(void)
{
	m_uCode = codeIdle;
	}

// Attributes

CString CConfigLoader::GetErrorText(void) const
{
	return m_Error;
	}

// Attributes

BOOL CConfigLoader::GetBaseModel(UINT &uBase, UINT &uModule) 
{
	if( m_BaseArcFactory.IsValid() ) {
		
		uBase   = m_BaseArcFactory.GetBaseType();

		uModule = m_BaseArcFactory.GetModuleId();

		return TRUE;
		}

	return FALSE;
	}

BOOL CConfigLoader::GetModuleFirmwareRevision(UINT &uRevision)
{	
	if( m_ModuleArcFactory.IsValid() ) {

		uRevision = m_ModuleArcFactory.GetFirmwareRevision();

		return TRUE;
		}

	return FALSE;
	}

BOOL CConfigLoader::GetBaseNetModeJumper(UINT &uMode)
{
	if( m_BaseCfgSetup.IsValid() ) {

		uMode = m_BaseCfgSetup.GetNetworkConfigJumper();

		return TRUE;
		}

	return FALSE;
	}

BOOL CConfigLoader::GetBaseSourceSinkJumper(UINT &uMode)
{
	if( m_BaseCfgSetup.IsValid() ) {

		uMode = m_BaseCfgSetup.GetBaseSourceSinkJumper();

		return TRUE;
		}

	return FALSE;
	}

BOOL CConfigLoader::CopyImageIntoFile(HANDLE hFile)
{
	if( m_BaseTmpImage.IsValid() ) {

		PBYTE pData = m_BaseTmpImage.GetDataBuffer();

		DWORD uSize = m_BaseTmpImage.GetDataSize();

		::WriteFile(hFile, pData, uSize, &uSize, NULL);		

		return TRUE;
		}

	return FALSE;
	}

BOOL CConfigLoader::GetBaseSerialNumber(UINT &uSerialNumber)
{
	uSerialNumber = m_BaseArcFactory.GetBaseSN();
	
	if( m_BaseArcFactory.IsValid() ) {

		uSerialNumber = m_BaseArcFactory.GetBaseSN();
		
		return TRUE;
		}

	return FALSE;
	}
	
// Operations

BOOL CConfigLoader::ReadFirmwareRevision(void)
{
	m_pData = &m_ModuleArcFactory;

	m_uCode = codeReadFile;

	return TRUE;
	}

BOOL CConfigLoader::CheckBaseModel(void)
{
	m_pData = &m_BaseArcFactory;

	m_uCode = codeReadFile;

	return TRUE;
	}

BOOL CConfigLoader::ReadCfgSetup(void)
{
	m_pData = &m_BaseCfgSetup;

	m_uCode = codeReadFile;

	return TRUE;
	}

BOOL CConfigLoader::ReadImage(void)
{
	m_pData = &m_BaseTmpImage;

	m_uCode = codeReadImage;

	return TRUE;
	}

BOOL CConfigLoader::ReadFile(IFileData *pData)
{
	m_pData = pData;

	m_uCode = codeReadFile;

	return TRUE;
	}

BOOL CConfigLoader::WriteFile(IFileData *pData)
{
	m_pData = pData;

	m_uCode = codeWriteFile;

	return TRUE;
	}

BOOL CConfigLoader::ClearFile(IFileData *pData)
{
	m_pData = pData;

	m_uCode = codeClearFile;

	return TRUE;
	}

BOOL CConfigLoader::ResetStation(void)
{
	m_uCode = codeSendReset;
	
	return TRUE;
	}

BOOL CConfigLoader::PrepareItem(void)
{
	m_uCode = codePrepare;
	
	return TRUE;
	}

// Binding

void CConfigLoader::Bind(ILinkTransport *pTrans)
{
	m_pTrans = pTrans;
	}

// Comms Operations

BOOL CConfigLoader::Transact(void)
{
	switch( m_uCode ) {
		
		case codeIdle:
			break;
		
		case codeReadFile:

			if( !m_pTrans->Transact(m_pData, UINT(2) ) ) {

				m_Error = m_pTrans->GetErrorText();
				
				return FALSE;
				}

			break;

		case codeWriteFile:

			if( !m_pTrans->Transact(m_pData, UINT(3) ) ) {

				m_Error = m_pTrans->GetErrorText();
				
				return FALSE;
				}

			break;

		case codeReadImage:

			if( !m_pTrans->Transact(m_pData, UINT(1) ) ) {

				m_Error = m_pTrans->GetErrorText();
				
				return FALSE;
				}

			break;

		case codeClearFile:

			if( !m_pTrans->Transact(m_pData, UINT(0) ) ) {

				m_Error = m_pTrans->GetErrorText();
				
				return FALSE;
				}

			break;

		case codePrepare:
			break;

		case codeSendReset:

			return m_pTrans->ResetStation();
				
		default:
			return FALSE;
		}

	return TRUE;
	}

// End of File
