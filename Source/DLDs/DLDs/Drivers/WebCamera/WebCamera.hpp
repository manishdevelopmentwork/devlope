
/////////////////////////////////////////////////////////////////////////
//
// Generic Web Camera Driver
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_WEB_CAMERA_HPP

#define INCLUDE_WEB_CAMERA_HPP

/////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define HTTP_SIZE	1024

#define HASH_SIZE	33

/////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CWebCamera;

/////////////////////////////////////////////////////////////////////////
//
// Image Update Modes
//

enum {
	updatePeriodic = 0,
	updateManual   = 1
	};

/////////////////////////////////////////////////////////////////////////
//
// Image Format Modes
//

enum {
	imgModeAuto = 0,
	imgModeJPG  = 1,
	imgModeBMP  = 2,
	imgModePNG  = 3
	};

/////////////////////////////////////////////////////////////////////////
//
// Image Format Constants
//

enum {
	imgFormatJPG     = 0,
	imgFormatPNG     = 1,
	imgFormatBMP     = 2,
	imgFormatUnknown = 3
	};

/////////////////////////////////////////////////////////////////////////
//
// HTTP Header Field
//

struct CHttpHeaderField
{
	PCTXT              m_pName;
	PCTXT              m_pValue;
	CHttpHeaderField * m_pNext;
	CHttpHeaderField * m_pPrev;
	};

/////////////////////////////////////////////////////////////////////////
//
// HTTP Authentication Types
//

enum {
	httpAuthNone,
	httpAuthBasic,
	httpAuthDigest
	};

/////////////////////////////////////////////////////////////////////////
//
// HTTP Authentication
//

class CHttpAuth
{
	public:
		// Constructor
		CHttpAuth(CWebCamera *pDriver, PCTXT pUser, PCTXT pPass, PCTXT pPath);

		// Destructor
		~CHttpAuth(void);

		// Authorization Helpers
		PCTXT CalculateResponse(void);
		PCTXT GetRealm(void);
		PCTXT GetNonce(void);
		PCTXT GetNonceCount(void);
		PCTXT GetCNonce(void);
		PCTXT GetQop(void);

		// Data Members
		UINT  m_uType;
		UINT  m_uNonceCount;
		PCTXT m_pRealm;
		PCTXT m_pNonce;
		PCTXT m_pQop;
		PCTXT m_pUser;
		PCTXT m_pPass;
		PCTXT m_pUri;
		PTXT  m_pResponse;
		PTXT  m_pCNonce;
		PTXT  m_pNC;

	protected:
		// Implementation
		BOOL GetHash(PCTXT pText, PBYTE pHash);
		void UpdateCNonce(void);
		void BytesToHex(PBYTE pSrc, UINT uCount, PTXT pDest);
		
		// Members
		CWebCamera * m_pDriver;
	};

/////////////////////////////////////////////////////////////////////////
//
// Generic Web Camera Driver
//

class CWebCamera : public CCameraDriver
{
	public:
		// Constructor
		CWebCamera(void);

		// Destructor
		~CWebCamera(void);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT ) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) ReadImage(PBYTE &pData);
		DEFMETH(void)  SwapImage(PBYTE pData);
		DEFMETH(void)  KillImage(void);
		DEFMETH(void)  Service(void);
		
		DEFMETH(PCBYTE) GetData(UINT uDevice);
		DEFMETH(UINT  ) GetInfo(UINT uDevice, UINT uParam);

		// Helpers
		void Format(PTXT p, LPCTXT pText, ...);
		void Trace(LPCTXT pText, ...);
		void GetRandomBytes(PBYTE pRandom, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			CContext * m_pNext;
			CContext * m_pPrev;
			DWORD	   m_IP;
			UINT	   m_uPort;
			UINT       m_Mode;
			BOOL	   m_fKeep;
			BOOL       m_fPing;
			UINT	   m_uTime1;
			UINT	   m_uTime2;
			UINT	   m_uTime3;
			UINT	   m_uTime4;
			UINT	   m_uLast3;
			UINT	   m_uLast4;
			UINT       m_uFrame;
			PBYTE	   m_pData;
			UINT	   m_uInfo[4];
			UINT       m_uDevice;
			ISocket  * m_pSock;
			BOOL       m_fDirty;
			char	   m_Http[HTTP_SIZE];
			PTXT       m_Path;
			PTXT       m_User;
			PTXT       m_Pass;
			UINT       m_uAuth;
			UINT       m_uScale;
			BOOL       m_fForce;
			UINT       m_uUpdate;
			UINT       m_uPeriod;
			UINT       m_uLastImage;

			CHttpHeaderField * m_pHttpHead;
			CHttpHeaderField * m_pHttpTail;
			CHttpAuth        * m_Auth[2];
			};

		// Data Members
		CContext * m_pCtx;
		CContext * m_pHead;
		CContext * m_pTail;
		UINT	   m_uKeep;
		UINT       m_uCount;
						
		// Extra Help
		IExtraHelper   * m_pExtra;
								
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Limited HTTP Support
		void  BuildHttpGetRequest(CContext *pCtx, UINT uAuth);
		void  AddBasicAuth(void);
		void  AddDigestAuth(void);
		void  AddParam(PTXT pDest, PCTXT pName, PCTXT pValue, BOOL fQuoted);
		UINT  RecvHttpResponse(void);
		PBYTE RecvImageData(UINT uLen);
		void  AddHttpField(CHttpHeaderField *pField);
		PCTXT GetHttpField(PCTXT pName, UINT uIndex);
		void  ClearHttpFields(void);
		UINT  GetRepsonseCode(PCTXT pText);
		PCTXT FindAndAllocName(PCTXT pText);
		PCTXT FindAndAllocValue(PCTXT pText);
		BOOL  ParseAuth(CHttpAuth *pAuth, PCTXT pText);
		
		// Implementation
		BOOL GetImage(PBYTE &pData);
		UINT FindImageFormat(void);
		UINT GuessImageFormat(void);
		BOOL ShouldUpdateImage(void);
		BOOL Read(PBYTE pData, UINT uCount);
		BOOL Send(ISocket *pSock, PCTXT pText, PTXT pArgs);
		BOOL Send(ISocket *pSock, PCBYTE pText, UINT uSize);
		void GetValue(PCTXT &pVal, PCTXT pText);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
	};

// End of File

#endif
