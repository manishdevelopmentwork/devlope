
//////////////////////////////////////////////////////////////////////////
//
// R307 Tacoma - RTD6 Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RTD6DBASE_H

#define	INCLUDE_RTD6DBASE_H

//////////////////////////////////////////////////////////////////////////
//
// Out of Range Value
//

#define	UPSCALE		((LONG) 0x7000000)

//////////////////////////////////////////////////////////////////////////
//
// RTD Types
//

#define RTD_TYPE385	1
#define RTD_TYPE392	2
#define RTD_TYPE428	3
#define RTD_TYPE672	4
#define RTD_OHMS	5

//////////////////////////////////////////////////////////////////////////
//
// Calibration Data
//

typedef struct tagCalibRTD6
{
	INT	RTDSignal[6];
	INT	RTDExcite[6];
	WORD	RTDK[6];

	} CALIBRTD6;

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfigRTD6
{
	BYTE	ChanEnable[6];
	WORD	InputFilter;
	BYTE	InputType[6];
	WORD	InputSlope[6];
	INT	InputOffset[6];
	UINT	TempUnits;
	BYTE	GUID[16];
	BYTE	Valid;

	} CONFIGRTD6;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatusRTD6
{
	BYTE	InputAlarm[6];
	LONG	Input[6];
	float	PV[6];
	float	DeltaT[6];
	BYTE	Running;

	} STATUSRTD6;
	
// End of File

#endif
