
#include "Intern.hpp"

#include "TagFlagPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispColor.hpp"
#include "DispFormat.hpp"
#include "SecDesc.hpp"
#include "TagFlag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Page
//

// Runtime Class

AfxImplementRuntimeClass(CTagFlagPage, CUIStdPage);

// Constructor

CTagFlagPage::CTagFlagPage(CTagFlag *pTag, CString Title, UINT uPage)
{
	m_pTag  = pTag;

	m_Title = Title;

	m_Class = AfxRuntimeClass(CTagFlag);

	m_uPage = uPage;
	}

// Operations

BOOL CTagFlagPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	if( m_uPage == 2 ) {

		LoadLimits(pView);

		LoadFormat(pView);

		pView->NoRecycle();
		}

	if( m_uPage == 3 ) {

		LoadColor(pView);

		pView->NoRecycle();
		}

	if( m_uPage == 6 ) {

		LoadSecurity(pView);

		pView->NoRecycle();
		}

	return TRUE;
	}

// Implementation

BOOL CTagFlagPage::LoadLimits(IUICreate *pView)
{
	CDispFormat *pFormat = m_pTag->m_pFormat;

	if( !pFormat || pFormat->NeedsLimits() ) {

		CUIPage *pPage = New CUIStdPage(AfxPointerClass(m_pTag), m_uPage + 1);

		pPage->LoadIntoView(pView, m_pTag);

		delete pPage;

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagFlagPage::LoadFormat(IUICreate *pView)
{
	CDispFormat *pFormat = m_pTag->m_pFormat;

	if( pFormat ) {

		CUIPageList List;

		pFormat->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pFormat);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagFlagPage::LoadColor(IUICreate *pView)
{
	CDispColor *pColor = m_pTag->m_pColor;

	if( pColor ) {

		CUIPageList List;

		pColor->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pColor);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTagFlagPage::LoadSecurity(IUICreate *pView)
{
	CSecDesc *pSec = m_pTag->m_pSec;

	if( pSec ) {

		CUIPageList List;

		pSec->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pSec);

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
