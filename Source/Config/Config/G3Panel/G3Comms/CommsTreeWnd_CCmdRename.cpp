
#include "Intern.hpp"

#include "CommsTreeWnd_CCmdRename.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Rename Command
//

// Runtime Class

AfxImplementRuntimeClass(CCommsTreeWnd::CCmdRename, CStdCmd);

// Constructor

CCommsTreeWnd::CCmdRename::CCmdRename(CMetaItem *pItem, CString Name)
{
	m_Menu = CFormat(IDS_RENAME, pItem->GetName());

	m_Item = pItem->GetFixedPath();

	m_Prev = pItem->GetName();

	m_Name = Name;
	}

// End of File
