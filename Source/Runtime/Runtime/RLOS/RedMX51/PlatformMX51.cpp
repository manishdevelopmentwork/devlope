
#include "Intern.hpp"

#include "PlatformMX51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IoMux51.hpp"

#include "Dpll51.hpp"

#include "Ccm51.hpp"

#include "Gpio51.hpp"

#include "Pmui51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice * Create_InputQueueRLOS(void);
extern IDevice * Create_SpiInterface51(CIoMux51 *pMux, CCcm51 *pCcm);
extern IDevice * Create_I2cInterface51(CCcm51 *pCcm);
extern IDevice * Create_Entropy(void);

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for iMX51 Device
//

// Constructor

CPlatformMX51::CPlatformMX51(void) : CPlatformRed(TRUE)
{
	m_pTimer = CreateTimer();

	m_uModel = 0;

	m_uPmui  = 10;

	DiagRegister();
}

// Destructor

CPlatformMX51::~CPlatformMX51(void)
{
	phal->DebugRevoke();

	DiagRevoke();

	piob->RevokeGroup("dev.");

	m_pTimer->Release();

	delete m_pDpll[0];

	delete m_pDpll[1];

	delete m_pDpll[2];

	delete m_pCcm;

	delete m_pMux;

	delete m_pPmui;
}

// IUnknown

HRESULT CPlatformMX51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IDiagProvider);

	return CPlatformBase::QueryInterface(riid, ppObject);
}

ULONG CPlatformMX51::AddRef(void)
{
	return CPlatformBase::AddRef();
}

ULONG CPlatformMX51::Release(void)
{
	return CPlatformBase::Release();
}

// IDevice

BOOL CPlatformMX51::Open(void)
{
	phal->DebugWait();

	InitPriorities();

	m_pDpll[0] = New CDpll51(0);

	m_pDpll[1] = New CDpll51(1);

	m_pDpll[2] = New CDpll51(2);

	m_pCcm     = New CCcm51(m_pDpll);

	InitClocks();

	piob->RegisterSingleton("dev.gpio", 0, m_pGpio[0] = New CGpio51(0));

	piob->RegisterSingleton("dev.gpio", 1, m_pGpio[1] = New CGpio51(1));

	piob->RegisterSingleton("dev.gpio", 2, m_pGpio[2] = New CGpio51(2));

	piob->RegisterSingleton("dev.gpio", 3, m_pGpio[3] = New CGpio51(3));

	InitGpio();

	m_pMux = New CIoMux51();

	InitMux();

	piob->RegisterSingleton("dev.spi", 0, Create_SpiInterface51(m_pMux, m_pCcm));

	m_pPmui = New CPmui51();

	InitRails();

	piob->RegisterSingleton("dev.i2c", 0, Create_I2cInterface51(m_pCcm));

	MakeDevice("dev.input-d", 1, Create_InputQueueRLOS);

	MakeDevice("dev.input-x", 1, Create_InputQueueRLOS);

	MakeDevice("dev.entropy", 1, Create_Entropy);

	// TODO -- Add device-specific entropy source from serial number and MACs !!!

	RegisterBaseCommon();

	InitTimer();

	phal->DebugRegister();

	return TRUE;
}

// IPlatform

PCTXT CPlatformMX51::GetFamily(void)
{
	return "Graphite(R)";
}

PCTXT CPlatformMX51::GetModel(void)
{
	switch( m_uModel ) {

		case MODEL_EDGE: return "GC";
		case MODEL_CORE: return "GC";
		case MODEL_GRAPHITE_07: return "G07";
		case MODEL_GRAPHITE_09: return "G09";
		case MODEL_GRAPHITE_10S: return "G10s";
		case MODEL_GRAPHITE_10V: return "G10v";
		case MODEL_GRAPHITE_12: return "G12";
		case MODEL_GRAPHITE_15: return "G15";
	}

	return "none";
}

PCTXT CPlatformMX51::GetVariant(void)
{
	return GetModel();
}

UINT CPlatformMX51::GetFeatures(void)
{
	return	1 * rfGraphiteModules |
		0 * rfSerialModules   |
		1 * rfDisplay;
}

UINT CPlatformMX51::GetGroup(void)
{
	return SW_GROUP_4;
}

BOOL CPlatformMX51::IsService(void)
{
	return FALSE;
}

BOOL CPlatformMX51::IsEmulated(void)
{
	return FALSE;
}

BOOL CPlatformMX51::HasMappings(void)
{
	return FALSE;
}

// IEventSink

void CPlatformMX51::OnEvent(UINT uLine, UINT uParam)
{
	if( !m_uPmui-- ) {

		m_pPmui->ChargerResetTimer();

		m_uPmui = (1 * 60 * 60);
	}
}

// IDiagProvider

UINT CPlatformMX51::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)

	switch( pCmd->GetCode() ) {

		case 1:
			return DiagClocks(pOut, pCmd);

		case 2:
			return DiagRails(pOut, pCmd);
	}

	#endif

	return 0;
}

// Initialisation

void CPlatformMX51::InitPriorities(void)
{
	phal->SetLinePriority(INT_EPIT1, 0);
	phal->SetLinePriority(INT_GPIO1_L0, 1);
	phal->SetLinePriority(INT_GPIO1_L1, 1);
	phal->SetLinePriority(INT_GPIO2_L0, 1);
	phal->SetLinePriority(INT_GPIO2_L1, 1);
	phal->SetLinePriority(INT_GPIO3_L0, 1);
	phal->SetLinePriority(INT_GPIO3_L1, 1);
	phal->SetLinePriority(INT_GPIO4_L0, 1);
	phal->SetLinePriority(INT_GPIO4_L1, 1);
	phal->SetLinePriority(INT_I2C1, 1);
	phal->SetLinePriority(INT_GPT, 1);
	phal->SetLinePriority(INT_CSPI1, 2);
}

void CPlatformMX51::InitTimer(void)
{
	m_pTimer->SetPeriod(1000);

	m_pTimer->SetHook(this, 0);

	m_pTimer->Enable(true);
}

void CPlatformMX51::InitClocks(void)
{
}

void CPlatformMX51::InitMux(void)
{
	DWORD const muxEcspi[] = {

		CIoMux51::MUX_CSPI1_MOSI,  0,
		CIoMux51::MUX_CSPI1_MISO,  0,
		CIoMux51::MUX_CSPI1_SS0,   0,
		CIoMux51::MUX_CSPI1_SS1,   0,
		CIoMux51::MUX_CSPI1_SCLK,  0,

		CIoMux51::PAD_CSPI1_MOSI,  CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_CSPI1_MISO,  CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_CSPI1_SS0,   CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_CSPI1_SS1,   CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_CSPI1_SCLK,  CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
	};

	DWORD const muxI2c[] = {

		CIoMux51::SEL_I2C1_SCL,    2,
		CIoMux51::SEL_I2C1_SDA,    2,
		CIoMux51::MUX_SD2_CMD,     CIoMux51::MOD_FORCE_INPUT | 1,
		CIoMux51::MUX_SD2_CLK,     CIoMux51::MOD_FORCE_INPUT | 1,
		CIoMux51::PAD_SD2_CMD,     CIoMux51::PAD_OPEN_DRAIN  | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD2_CLK,     CIoMux51::PAD_OPEN_DRAIN  | CIoMux51::PAD_DRIVE_HI,
	};

	m_pMux->Set(muxEcspi, elements(muxEcspi));

	m_pMux->Set(muxI2c, elements(muxI2c));
}

void CPlatformMX51::InitGpio(void)
{
	m_pGpio[3]->SetState(24, false);

	m_pGpio[3]->SetState(25, false);

	m_pGpio[3]->SetDirection(24, true);

	m_pGpio[3]->SetDirection(25, true);
}

void CPlatformMX51::InitRails(void)
{
}

// Diagnostics

bool CPlatformMX51::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "proc");

		pDiag->RegisterCommand(m_uProv, 1, "clocks");

		pDiag->RegisterCommand(m_uProv, 2, "rails");

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

bool CPlatformMX51::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

UINT CPlatformMX51::DiagClocks(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)

	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		pOut->AddProp("PLL1", "%9d", m_pCcm->GetFreq(CCcm51::clkPll1));
		pOut->AddProp("PLL2", "%9d", m_pCcm->GetFreq(CCcm51::clkPll2));
		pOut->AddProp("PLL3", "%9d", m_pCcm->GetFreq(CCcm51::clkPll3));
		pOut->AddProp("ARM ROOT", "%9d", m_pCcm->GetFreq(CCcm51::clkArmRoot));
		pOut->AddProp("AXI_A", "%9d", m_pCcm->GetFreq(CCcm51::clkAxia));
		pOut->AddProp("AXI_B", "%9d", m_pCcm->GetFreq(CCcm51::clkAxib));
		pOut->AddProp("AHB", "%9d", m_pCcm->GetFreq(CCcm51::clkAhb));
		pOut->AddProp("EMI", "%9d", m_pCcm->GetFreq(CCcm51::clkEmi));
		pOut->AddProp("IPG", "%9d", m_pCcm->GetFreq(CCcm51::clkIpg));
		pOut->AddProp("PER", "%9d", m_pCcm->GetFreq(CCcm51::clkPer));
		pOut->AddProp("GPU2", "%9d", m_pCcm->GetFreq(CCcm51::clkGpu2));
		pOut->AddProp("AXI", "%9d", m_pCcm->GetFreq(CCcm51::clkArmAxi));
		pOut->AddProp("IPU HSP", "%9d", m_pCcm->GetFreq(CCcm51::clkIpuHsp));
		pOut->AddProp("GPU", "%9d", m_pCcm->GetFreq(CCcm51::clkGpu));
		pOut->AddProp("VPU", "%9d", m_pCcm->GetFreq(CCcm51::clkVpu));
		pOut->AddProp("USBOH3", "%9d", m_pCcm->GetFreq(CCcm51::clkUsboh3));
		pOut->AddProp("SDHC1", "%9d", m_pCcm->GetFreq(CCcm51::clkSdhc1));
		pOut->AddProp("UART", "%9d", m_pCcm->GetFreq(CCcm51::clkUart));
		pOut->AddProp("USBPHY", "%9d", m_pCcm->GetFreq(CCcm51::clkUsbPhy));
		pOut->AddProp("ECSPI", "%9d", m_pCcm->GetFreq(CCcm51::clkEcspi));
		pOut->AddProp("DDR", "%9d", m_pCcm->GetFreq(CCcm51::clkDdr));
		pOut->AddProp("NAND", "%9d", m_pCcm->GetFreq(CCcm51::clkEnfc));

		pOut->EndPropList();

		return 0;
	}

	pOut->Error(NULL);

	#endif

	return 1;
}

UINT CPlatformMX51::DiagRails(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)

	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		pOut->AddProp("AUD", "%d.%03dV", m_pPmui->GetVAud()  / 1000, m_pPmui->GetVAud()  % 1000);
		pOut->AddProp("VID", "%d.%03dV", m_pPmui->GetVVid()  / 1000, m_pPmui->GetVVid()  % 1000);
		pOut->AddProp("PLL", "%d.%03dV", m_pPmui->GetVPll()  / 1000, m_pPmui->GetVPll()  % 1000);
		pOut->AddProp("DIG", "%d.%03dV", m_pPmui->GetVDig()  / 1000, m_pPmui->GetVDig()  % 1000);
		pOut->AddProp("CAM", "%d.%03dV", m_pPmui->GetVCam()  / 1000, m_pPmui->GetVCam()  % 1000);
		pOut->AddProp("SD", "%d.%03dV", m_pPmui->GetVSd()   / 1000, m_pPmui->GetVSd()   % 1000);
		pOut->AddProp("GEN1", "%d.%03dV", m_pPmui->GetVGen1() / 1000, m_pPmui->GetVGen1() % 1000);
		pOut->AddProp("GEN2", "%d.%03dV", m_pPmui->GetVGen2() / 1000, m_pPmui->GetVGen2() % 1000);
		pOut->AddProp("GEN3", "%d.%03dV", m_pPmui->GetVGen3() / 1000, m_pPmui->GetVGen3() % 1000);
		pOut->AddProp("SW1", "%d.%03dV", m_pPmui->GetSw1()   / 1000, m_pPmui->GetSw1()   % 1000);
		pOut->AddProp("SW2", "%d.%03dV", m_pPmui->GetSw2()   / 1000, m_pPmui->GetSw2()   % 1000);
		pOut->AddProp("SW3", "%d.%03dV", m_pPmui->GetSw3()   / 1000, m_pPmui->GetSw3()   % 1000);
		pOut->AddProp("SW4", "%d.%03dV", m_pPmui->GetSw4()   / 1000, m_pPmui->GetSw4()   % 1000);

		pOut->EndPropList();

		return 0;
	}

	pOut->Error(NULL);

	#endif

	return 1;
}

// End of File
