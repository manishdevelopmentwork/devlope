
#ifndef	INCLUDE_IMEIP_HPP

#define	INCLUDE_IMEIP_HPP

#include "segment.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EIP Master Driver Interface
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

interface IEIPMaster
{
	// Action
	DEFMETH(BOOL)	Send(BYTE bCode, CCIPPath &Path, CDataBuf &Data)					= 0;
	DEFMETH(BOOL)	Send(BYTE bCode, CCIPPath &Path, WORD wClass, WORD wInst, PBYTE pData, UINT uSize)	= 0;
	DEFMETH(BOOL)	Recv(CBuffer * &pBuff)									= 0;
	DEFMETH(BYTE)	GetLastError(void)									= 0;

	// Debug
	DEFMETH(void)	Dump(CBuffer *pBuff)									= 0;
	DEFMETH(void)	Dump(PBYTE pData, UINT uSize)								= 0;
	DEFMETH(void)	Trace(LPCTXT pText, ...)								= 0;
};

// End of File

#endif
