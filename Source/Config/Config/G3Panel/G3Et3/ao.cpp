
#include "intern.hpp"

#include "io.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Page
//

class CCAnalogOutputPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCAnalogOutputPage(CAnalogOutputItem *pOutput);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CAnalogOutputItem * m_pOutput;
		CEt3CommsSystem   * m_pSystem;

		// Implementation
		void LoadRange(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Page
//

// Runtime Class

AfxImplementRuntimeClass(CCAnalogOutputPage, CUIStdPage);

// Constructor

CCAnalogOutputPage::CCAnalogOutputPage(CAnalogOutputItem *pOutput)
{
	m_Class   = AfxRuntimeClass(CAnalogOutputItem);	

	m_pOutput  = pOutput;

	m_pSystem = (CEt3CommsSystem *) pOutput->GetDatabase()->GetSystemItem();
	}

// Operations

BOOL CCAnalogOutputPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);	

	LoadRange(pView);

	return TRUE;
	}

// Implementation

void CCAnalogOutputPage::LoadRange(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysAOs()) ) {

		pView->StartTable(CString(IDS_CHANNELS), 1);

		pView->AddColHead(CString(IDS_RANGE));
		
		for( UINT n = 0; n < uCount; n ++ ) {

			CString Form;
				
			Form += L"";	Form += L"0|Disabled";	// 0
			Form += L"|";	Form += L"1|+-10.0V";	// 1
			Form += L"|";	Form += L"2|+-5V";	// 2
			Form += L"|";	Form += L"3|0-10V";	// 3
			Form += L"|";	Form += L"4|0-5V";	// 4
			Form += L"|";	Form += L"10|4-20mA";	// 5
			Form += L"|";	Form += L"11|0-20mA";	// 6

			pView->AddRowHead(CPrintf(L"AO%d", 1 + n));

			CUIData Data;

			Data.m_Tag	 = CPrintf(L"Range%2.2d", 1 + n);

			Data.m_Label	 = CPrintf(L"Channel %d Mode", 1 + n);

			Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

			Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

			Data.m_Format	 = Form;

			Data.m_Tip	 = CString(IDS_FORMAT);

			pView->AddUI(m_pOutput, L"root", &Data);
			}

		pView->EndTable();
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogOutputItem, CIOItem);

// Constructor

CAnalogOutputItem::CAnalogOutputItem(void)
{
	m_dwMask = 0x30;
	}

// Overridables

UINT CAnalogOutputItem::GetTreeImage(void)
{
	return IDI_AO;
	}

// UI Creation

BOOL CAnalogOutputItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CCAnalogOutputPage(this));

	return FALSE;
	}

// UI Update

void CAnalogOutputItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		for( UINT n = 0; n < elements(m_Range); n ++ ) {

			LimitEnum(pHost, CPrintf(L"Range%2.2d", 1+n), m_Range[n], m_dwMask);
			}

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistence

void CAnalogOutputItem::Init(void)
{
	CIOItem::Init();

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		m_Range[n] = 0;
		}
	}

// Download Support

void CAnalogOutputItem::PrepareData(void)
{
	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	UINT c = m_pSystem->GetPhysAOs();	

	for( UINT n = 0; n < c; n ++ ) {
		
		CFileDataBaseCfgSetup::CAnalogOutput &AO = File.m_AOs[n];

		UINT uRange = m_Range[n];

		AO.SetRange(uRange);
		}
	}

// Meta Data Creation

void CAnalogOutputItem::AddMetaData(void)
{
	CIOItem::AddMetaData();

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		UINT uOffset = (PBYTE(m_Range + n) - PBYTE(this));

		AddMeta( CPrintf("Range%2.2d", 1 + n), metaInteger, uOffset );
		}
	}

// Implementation

void CAnalogOutputItem::DoEnables(IUIHost *pHost)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogOutputMix20882Item, CAnalogOutputItem);

// Constructor

CAnalogOutputMix20882Item::CAnalogOutputMix20882Item(void)
{
	m_dwMask = 0x60;
	}

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogOutputMix20884Item, CAnalogOutputItem);

// Constructor

CAnalogOutputMix20884Item::CAnalogOutputMix20884Item(void)
{
	m_dwMask = 0x70;
	}

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogOutputAO20MItem, CAnalogOutputItem);

// Constructor

CAnalogOutputAO20MItem::CAnalogOutputAO20MItem(void)
{
	m_dwMask = 0x20;
	}

// End of File
