
#include "intern.hpp"

#include "MTSSlave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MTS Ascii Slave Comms Driver
//

// Instantiator

ICommsDriver *	Create_MTSAsciiSlaveDriver(void)
{
	return New CMTSAsciiSlaveDriver;
	}

// Constructor

CMTSAsciiSlaveDriver::CMTSAsciiSlaveDriver(void)
{
	m_wID		= 0x3407;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "MTS";
	
	m_DriverName	= "DDA Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "MTS DDA Slave";

	AddSpaces();
	}

// Binding Control

UINT	CMTSAsciiSlaveDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CMTSAsciiSlaveDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 4800;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void	CMTSAsciiSlaveDriver::AddSpaces(void)
{
	AddSpace( New CSpace(1, "ADD",	"Select Address to Monitor",		10, 1, 8,  addrByteAsByte ) );
	AddSpace( New CSpace(2, "OUT1",	"Output Level 1 (Product)",		10, 1, 8,  addrRealAsReal ) );
	AddSpace( New CSpace(3, "OUT2",	"Output Level 2 (Interface)",		10, 1, 8,  addrRealAsReal ) );
	AddSpace( New CSpace(4, "TAVG",	"Average Temperature",			10, 1, 8,  addrRealAsReal ) );
	AddSpace( New CSpace(5, "TIDT",	"Individual DT Temperature (5/ADD)",	10, 1, 40, addrRealAsReal ) );
	AddSpace( New CSpace(6, "CADD",	"Change Unit Address",			10, 0, 0,  addrByteAsByte ) );
	}

// End of File
