 
#include "intern.hpp"

#include "bsapfull.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLTagsDeviceOptions, CUIItem);

// Constructor

CBSAPFULLTagsDeviceOptions::CBSAPFULLTagsDeviceOptions(void)
{
	m_ArraySelect = 0;
	m_ArrayValue  = 0;
	m_ArrayString = L"";

	NameList.Empty();
	TableList.Empty();
	OffsetList.Empty();
	MSDList.Empty();
	LNumList.Empty();
	TypeList.Empty();
	DevList.Empty();
	LevList.Empty();
	GlobList.Empty();
	AStatusList.Empty();

	m_Name		= L"";
	m_Table		= SPREAL1;
	m_Offset	= 0;
	m_Index		= 0xFFFF;
	m_MSD		= 0;
	m_LNum		= 0;
	m_Type		= 0;
	m_Dev		= 1;
	m_Lev		= 1;
	m_ArrayStatus	= 0;

	memset(DevNumList, 0, 15 * sizeof(WORD));

	m_pViewWnd	= NULL;

	m_uStrIndex     = 0;

	m_TagsFwdMap.Empty();
	m_TagsRevMap.Empty();

	CreateNRTTags();
	}

// Attributes

CString CBSAPFULLTagsDeviceOptions::GetDeviceName(void)
{
	return ((CItem *) GetParent())->GetHumanName();
	}

// Operations

BOOL CBSAPFULLTagsDeviceOptions::CreateTag(CError &Error, CString const &Name, CAddress Addr, BOOL fNew, BOOL fUpdate)
{
	INDEX Index;

	if( fUpdate ) {					// Addr Check

		Index = m_TagsRevMap.FindName(Addr.m_Ref);

		return m_TagsFwdMap.Failed(Index);
		}

	Index = m_TagsFwdMap.FindName(Name);

	if( !m_TagsFwdMap.Failed(Index) ) {

		Error.Set(TEXT(STRUSED), 0);

		return FALSE;
		}

	if( fNew ) {

		if( !Addr.a.m_Table ) {

			Addr.a.m_Table	= SPREAL1;
			Addr.a.m_Offset	= 1;
			Addr.a.m_Type	= addrRealAsReal;
			Addr.a.m_Extra	= 0;
			}

		UINT uTable	= Addr.a.m_Table;

		UINT uTableType	= uTable / 10;

		BOOL fStr	= IsSTRINGTable(uTable);

		UINT u		= AGetArrayCount();

		if( (BOOL)u ) {		// there are items in the list

			UINT d = Addr.a.m_Offset;

			UINT a = fStr ? MAXDWORDLEN : 1;

			UINT n = 0;

			while( n < u ) {

				if( uTableType == ((UINT)TableList[n] / 10) ) {

					if( d == OffsetList[n] ) {

						d += a;

						if( !fStr && !(d % MAXDWORDLEN) ) {

							d++;	// x * MAXDWORDLEN values reserved for strings
							}

						n = 0;
						}

					continue;
					}
				n++;
				}

			Addr.a.m_Offset = d;	// unique offset
			}

		else {
			Addr.a.m_Offset	= fStr ? 0 : 1;
			Addr.a.m_Extra  = 1;
			}
		}

	SetStringOffset(Addr);

	m_TagsFwdMap.Insert(Name, Addr.m_Ref);
	m_TagsRevMap.Insert(Addr.m_Ref, Name);

	return TRUE;
	}

BOOL CBSAPFULLTagsDeviceOptions::IsBITTable(UINT uTable)
{
	return uTable >= SPBIT0 && uTable <= SPBIT6;
	}

BOOL CBSAPFULLTagsDeviceOptions::IsBYTETable(UINT uTable)
{
	return uTable >= SPBYTE0 && uTable <= SPBYTE6;
	}

BOOL CBSAPFULLTagsDeviceOptions::IsWORDTable(UINT uTable)
{
	return uTable >= SPWORD0 && uTable <= SPWORD6;
	}

BOOL CBSAPFULLTagsDeviceOptions::IsDWORDTable(UINT uTable)
{
	return uTable >= SPDWORD0 && uTable <= SPDWORD6;
	}

BOOL CBSAPFULLTagsDeviceOptions::IsREALTable(UINT uTable)
{
	return uTable >= SPREAL0 && uTable <= SPREAL6;
	}

BOOL CBSAPFULLTagsDeviceOptions::IsSTRINGTable(UINT uTable)
{
	return uTable >= SPSTRING0 && uTable <= SPSTRING6;
	}

BOOL CBSAPFULLTagsDeviceOptions::IsNRTTable(UINT uTable)
{
	return uTable >= SPNRT0 && uTable <= SPNRT6;
	}

BOOL CBSAPFULLTagsDeviceOptions::IsNRTName(CString sName)
{
	CString s = sName.Left(4);

	return s == L"zTMS" || s == L"zNRT";
	}

BOOL CBSAPFULLTagsDeviceOptions::IsACK(UINT uTable)
{
	return uTable == SPBOOLACK || uTable == SPREALACK || uTable == SPWORDACK || uTable == SPDWORDACK;
	}

void CBSAPFULLTagsDeviceOptions::DeleteTag(CAddress const &Addr, BOOL fKill)
{
	INDEX Index = m_TagsFwdMap.FindData(Addr.m_Ref);

//	if( Addr.a.m_Table == SPBOOLACK || Addr.a.m_Table == SPREALACK ) return;

	if( !m_TagsFwdMap.Failed(Index) ) {

		UINT k = 0;

		UINT c = NameList.GetCount();

		BOOL This = FALSE;

		while( k < c ) {

			if(
				Addr.a.m_Offset == OffsetList[k] &&
				Addr.a.m_Table  == TableList[k]  &&
				Addr.a.m_Type   == TypeList[k]   &&
				LevList[k]	== HIBYTE(DevNumList[Addr.a.m_Extra])
				) {

				This = TRUE;
				break;
				}

			else k++;
			}

		if( This ) {

			NameList.SetAt(k, "*");
			OffsetList.SetAt(k, 0);
			MSDList.SetAt(k, 0xFFFF);
			LNumList.SetAt(k, 0);
			AStatusList.SetAt(k, 0);
			DevList.SetAt(k, 0);
			LevList.SetAt(k, 0);

			if( fKill ) {

				UINT uIndex = GetIndex(Addr);
		
				m_AddrArray.Append(uIndex);
				}

			RebuildMaps(NOTHING);
			}
		}
	}

BOOL CBSAPFULLTagsDeviceOptions::RenameTag(CString const &Name, CAddress const &Addr)
{
	CString Old;

	ExpandAddress(Old, Addr);
	
	DeleteTag(Addr, FALSE);

	CError Error(TRUE);

	if( !CreateTag(Error, Name, Addr, FALSE, FALSE) ) {
		
		Error.Show(*afxMainWnd);

		CreateTag(Error, Old, Addr, FALSE, FALSE);

		return FALSE;
		}

	return TRUE;
	}

CString CBSAPFULLTagsDeviceOptions::CreateName(CAddress Addr, CString PostFix)
{
	CString Type   = GetVarType(Addr);

	CString Text;

	Text.Printf(TEXT("@GV.%s_V%s"), Type, TEXT("%u"));

	return CreateName(Text, PostFix);
	}

void CBSAPFULLTagsDeviceOptions::ListTags(CTagDataEArray &List)
{
	INDEX Index = m_TagsFwdMap.GetHead();

	while( !m_TagsFwdMap.Failed(Index) ) {

		CTagDataE Data;

		Data.m_Name = m_TagsFwdMap.GetName(Index);
		
		Data.m_Addr = m_TagsFwdMap.GetData(Index);

		List.Append(Data);
		
		m_TagsFwdMap.GetNext(Index);
		}
	}

// Address Management

BOOL CBSAPFULLTagsDeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CString Name = Text;

	Addr.m_Ref   = GetAddrFromName(Name);

	if( Addr.m_Ref > 0 ) {

		return TRUE;
		}

	// adjust length of Device Number to allow for different formatting

	UINT uFind = Name.Find('#');

	if( uFind < NOTHING ) {

		if( (Addr.m_Ref = GetAddrFromName(Name.Left(uFind))) ) {

			return TRUE;	// no PostFix attached in stored name (for old databases)
			}

		uFind = Name.FindRev('D');

		if( uFind < NOTHING ) {

			UINT uDev = tatoi(Name.Mid(uFind+1));	// device number of name

			CString s;

			s.Printf( L"%sD%1.1d#", Name.Left(uFind), uDev );

			if( (Addr.m_Ref = GetAddrFromName(s)) > 0 ) {

				return TRUE;
				}

			s.Printf( L"%sD%2.2d#", Name.Left(uFind), uDev);

			if( (Addr.m_Ref = GetAddrFromName(s)) > 0 ) {

				return TRUE;
				}

			s.Printf( L"%sD%3.3d#", Name.Left(uFind), uDev);

			return (Addr.m_Ref = GetAddrFromName(s)) > 0;
			}
		}

	return FALSE;
	}

BOOL CBSAPFULLTagsDeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
// Since CreateNRT occurs upon opening, set up Maps for NRT immediately
	if( Addr.a.m_Table == SPNRT0 ) {

		UINT uOffset = (Addr.a.m_Offset & 0xF) + 1;

		CString s = uOffset > 3 ? L"zNRT" : L"zTMS";

		Text.Printf( L"%s%d", s, uOffset );

		m_TagsFwdMap.Insert(Text, Addr.m_Ref);
		m_TagsRevMap.Insert(Addr.m_Ref, Text);
		}

	CAddress Find = Addr;

	INDEX Index = m_TagsRevMap.FindName(Find.m_Ref);

	if( !m_TagsRevMap.Failed(Index) ) {

		Text = m_TagsRevMap.GetData(Index);

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CBSAPFULLTagsDeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	return FALSE;
	}

BOOL CBSAPFULLTagsDeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {
			
			m_List.Empty();
			
			ListTags(m_List);

			if( m_List.GetCount() < 2 ) {

				return FALSE;
				}
			}
		else {
			if( uItem >= m_List.GetCount() ) {

				return FALSE;
				}
			}
		
		CTagDataE &TagData = (CTagDataE &) m_List[uItem];

		CAddress Addr = (CAddress &) TagData.m_Addr;
			
		Data.m_Name   = TagData.m_Name;
		Data.m_Addr   = Addr;
		Data.m_fPart  = FALSE;

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	Data.m_Type = TEXT("Flag");	break;

			case addrByteAsByte:	Data.m_Type = TEXT("Byte");	break;

			case addrWordAsWord:	Data.m_Type = TEXT("Word");	break;

			case addrLongAsLong:	Data.m_Type = TEXT("Long");	break;

			default:		Data.m_Type = TEXT("Real");	break;
			}

		if( IsSTRINGTable(Addr.a.m_Table) ) {

			Data.m_Type = TEXT("String");
			}

		return TRUE;
		}

	return FALSE;
	}

// UI Management

void CBSAPFULLTagsDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	m_pViewWnd = pWnd;
	}

CViewWnd * CBSAPFULLTagsDeviceOptions::GetViewWnd(void) {

	return m_pViewWnd;
	}

// Persistance

void CBSAPFULLTagsDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == TEXT("BSAPTags") ) {

			Tree.GetObject();

			LoadArrays(Tree);

			Tree.EndObject();
			
			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CBSAPFULLTagsDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);	

	if( m_TagsFwdMap.GetCount() ) {

		Tree.PutObject(TEXT("BSAPTags"));

		SaveArrays(Tree);

		Tree.EndObject();
		}
	}

void  CBSAPFULLTagsDeviceOptions::PostLoad(void)
{
	CUIItem::PostLoad();

	AccessArrayValue();
	}

// Download Support

void CBSAPFULLTagsDeviceOptions::PrepareData(void)
{
	UINT uBitRealCnt = NameList.GetCount();

	if( uBitRealCnt ) {

		// Set up properties of Lists
		m_ListIndexes.Empty();
		m_ListNumbers.Empty();
		m_ListItemPtrs.Empty();

		LNSort.Empty();

		if( CountLNumItems(uBitRealCnt) < NOTHING ) {		// set up properties of all lists

			SortLNums();					// sort list items by index, and count total number
			}

		SortBBTags(uBitRealCnt);
		}
	}

BOOL CBSAPFULLTagsDeviceOptions::MakeInitData(CInitData &Init)
{
	UINT uBitRealCnt    = NameList.GetCount();

	UINT uNameArraySize = 0;

	Init.AddWord(0xBCDE);			// identify Crimson Type

	Init.AddText(GetDeviceName());

	Init.AddWord(WORD(uBitRealCnt));	// actual number of useable tags

	DWORD d;
	WORD  w;
	BYTE  b;
	UINT  i;

	for( i = 0; i < uBitRealCnt; i++ ) {		// Address Offset

		d = MakeMREF(i);

		Init.AddLong(d);
		}

	for( i = 0; i < uBitRealCnt; i++ ) {		// MSD Value

		w = MSDList[i];

		Init.AddWord(w);
		}

	w = LOWORD(LNSort.GetCount());

	Init.AddWord(w);			// Number of items assigned to Lists

	for( i = 0; i < w; i++ ) {		// Index / List number / List Position

		d = LNSort.GetAt(i);

		Init.AddLong(d);
		}

	for( i = 0; i < uBitRealCnt; i++ ) {		// Runtime Data Type

		b = MakeRuntimeType(i);

		Init.AddByte(b);
		}

	for( i = 0; i < uBitRealCnt; i++ ) {		// Global Addresses

		w = GlobList[i];

		Init.AddWord(w);
		}

	for( i = 0; i < 15; i++ ) {			// Level / Device number

		w = DevNumList[i];

		Init.AddWord(w);
		}

	WORD wAQuant = m_fIsMaster ? 0 : CountNumOfArrays(uBitRealCnt);	// number of items assigned to arrays

	Init.AddWord(wAQuant);

	if( (BOOL)wAQuant ) {

		for( UINT i = 0; i < uBitRealCnt; i++ ) {

			w = LOWORD(AStatusList[i]);

			b = LOBYTE(w);				// array number

			if( (BOOL)b ) {				// is assigned to array

				Init.AddWord(LOWORD(i));	// starting index of array
				Init.AddByte(b);		// array number
				Init.AddByte(HIBYTE(w));	// number of columns (!= 0 if array[0])
				}
			}
		}

	CNameArray List;

	CString    Name;

	for( i = 0; i < uBitRealCnt; i++ ) {		// Get Name Text Size

		Name = NameList[i];

		UINT u = Name.GetLength();

		UINT uFind = Name.Find(L"#L");

		if( uFind < NOTHING && uFind > 1 ) {

			if( Name[u-1] == '#' ) {

				Name = Name.Left(uFind);	// remove postfix
				}

			u = Name.GetLength();
			}

		List.Append(Name);

		uNameArraySize += (2 * u) + 4;	// Add 2 bytes for count, 2 bytes per char, 2 bytes for Null
		}

	Init.AddWord(WORD(uNameArraySize));

	for( i = 0; i < uBitRealCnt; i++ ) {		// Add Names

		Name = List[i];

		Init.AddText(Name);
		}

	return TRUE;
	}

// Download Support Protected
DWORD CBSAPFULLTagsDeviceOptions::MakeMREF(UINT uSelect)
	{
	CString Name = NameList[uSelect];

	INDEX Index  = m_TagsFwdMap.FindName(Name);

	if( !m_TagsFwdMap.Failed(Index) ) {
	
		DWORD d     = m_TagsFwdMap.GetData(Index);

		UINT uTable = LOBYTE(HIWORD(d));

		if( IsACK(uTable) ) {

			d &= 0xF0FFFFFF;	// clear Extra
			}

		return d;
		}

	return 0;
	}

void CBSAPFULLTagsDeviceOptions::SortBBTags(UINT uCount) {

	afxThread->SetStatusText(L"Optimizing Runtime...");

	CArray <CString> CA;

	UINT i;

	for( i = 0; i < uCount; i++ ) {

		CAddress A;

		A.m_Ref = MakeMREF(i);

		CString Line;

		Line.Printf( "%10.10u,%s,%u,%u,%u,%u,%u,%u,%u,%u,%u\0",

			A.m_Ref,
			NameList[i],
			OffsetList[i],
			MSDList[i],
			LNumList[i],
			TypeList[i],
			TableList[i],
			DevList[i],
			LevList[i],
			GlobList[i],
			AStatusList[i]
			);

		CA.Append(Line);
		}

	CArray <CString> Sorted = CA;

	Sorted.Sort();

	CStringArray List;

	for( i = 0; i < uCount; i++ ) {

		CString Line;

		List.Empty();

		Line = (CString)Sorted[i];

		Line.Tokenize(List, ',');

		NameList.SetAt(i, List[1]);

		WORD w = LOWORD(tatoi(List[2]));
		OffsetList.SetAt(i, w);

		w = LOWORD(tatoi(List[3]));
		MSDList.SetAt(i, w);

		w = LOWORD(tatoi(List[4]));
		LNumList.SetAt(i, w);

		BYTE b = LOBYTE(tatoi(List[5]));
		TypeList.SetAt(i, b);

		b = LOBYTE(tatoi(List[6]));
		TableList.SetAt(i, b);

		b = LOBYTE(tatoi(List[7]));
		DevList.SetAt(i, b);

		b = LOBYTE(tatoi(List[8]));
		LevList.SetAt(i, b);

		w = LOWORD(tatoi(List[9]));
		GlobList.SetAt(i, w);

		w = LOWORD(tatoi(List[10]));
		AStatusList.SetAt(i, w);
		}

	afxThread->SetStatusText(L"");
	}

UINT CBSAPFULLTagsDeviceOptions::CountLNumItems(UINT uCount)
{
	UINT i;

	for( i = 0; i < uCount; i++ ) {

		CheckListNum(LNumList[i] | (i << 16));
		}

	uCount   = m_ListNumbers.GetCount();

	UINT uOk = 0;


	for( i = 0; i < uCount && uOk < NOTHING; i++ ) {

		UINT uListNum	= m_ListNumbers.GetAt(i);

		PBYTE pItemList	= m_ListItemPtrs[i];

		UINT uListSize	= pItemList[LIDPOS];		// number of items assigned to list

		if( uListSize  != pItemList[LICPOS] ) {		// number of unique item numbers

			if( m_pViewWnd != NULL ) {

				m_pViewWnd->Information( CPrintf( L"List %d invalid configuration. Lists not downloaded", uListNum) );
				}

			uOk = NOTHING;
			}

		for( UINT k = 0; k < uListSize && uOk < NOTHING; k++ ) {

			if( pItemList[k] == 255 ) {	// list is badly configured

				if( m_pViewWnd != NULL ) {

					m_pViewWnd->Information( CPrintf( L"List %d - Item %d not configured. Lists not downloaded", uListNum, k));
					}

				uOk = NOTHING;
				}
			}
		}

	for( i = 0; i < m_ListItemPtrs.GetCount(); i++ ) {

		delete m_ListItemPtrs.GetAt(i);
		}

	m_ListItemPtrs.Empty();

	if( uOk == NOTHING ) {

		m_ListIndexes.Empty();
		}

	return m_ListIndexes.GetCount();		// number of configured list items
	}

BOOL CBSAPFULLTagsDeviceOptions::CheckListNum(UINT uLNum) {

	BYTE bListNum = HIBYTE(LOWORD(uLNum));

	if( bListNum ) {

		BYTE bListItem = LOBYTE(LOWORD(uLNum));

		PBYTE pItemsList;

		UINT uPos = m_ListNumbers.Find(bListNum);

		if( uPos == NOTHING ) {				// this list number array not initialized

			m_ListIndexes.Append(uLNum);		// store index, list number, item number

			m_ListNumbers.Append(bListNum);

			pItemsList = new BYTE[258];

			memset(pItemsList, 255, 256);

			m_ListItemPtrs.Append(pItemsList);

			pItemsList[bListItem] = bListItem;

			pItemsList[LICPOS] = 1;

			pItemsList[LIDPOS] = 1;
			}

		else {
			pItemsList = m_ListItemPtrs[uPos];

			pItemsList[LIDPOS] += 1;		// count configured list items

			if( bListItem || pItemsList[0] ) {

				pItemsList[bListItem] = bListItem;

				pItemsList[LICPOS] += 1;	// count actual list items

				m_ListIndexes.Append(uLNum);

				return TRUE;
				}
			}
		}

	return FALSE;	// do not add a value to the List Number List
	}

void CBSAPFULLTagsDeviceOptions::SortLNums(void) {

	LNSort.Empty();

	UINT uListCount = m_ListIndexes.GetCount();

	if( !uListCount ) {

		return;
		}

	if( uListCount == 1 ) {

		LNSort.Append( m_ListIndexes.GetAt(0) );

		return;
		}

	CArray <DWORD> ISort;

	DWORD d;

	UINT  i;

	for( i = 0; i < uListCount; i++ ) {

		d = m_ListIndexes.GetAt(i);

		d = MAKELONG(HIWORD(d), LOWORD(d));	// swap words

		ISort.Append(d);
		}

	ISort.Sort();	// sort according to list number/item

	for( i = 0; i < uListCount; i++ ) {

		d = ISort.GetAt(i);

		d = MAKELONG(HIWORD(d), LOWORD(d));

		LNSort.Append(d);			// swap words
		}
	}

WORD CBSAPFULLTagsDeviceOptions::CountNumOfArrays(UINT uCount) {

	WORD wArrayCount = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		if( LOBYTE(LOWORD(AStatusList[i])) ) wArrayCount++;	// total number of assigned arrays
		}

	return wArrayCount;
	}

BYTE CBSAPFULLTagsDeviceOptions::MakeRuntimeType(UINT uSelect)
{
	BYTE bT = TypeList[uSelect];

	switch( bT ) {

		case addrBitAsBit:
			bT = BTYPEBIT;
			break;

		case addrByteAsByte:
			bT = BTYPEBYTE;
			break;

		case addrWordAsWord:
			bT = BTYPEWORD;
			break;

		case addrLongAsLong:	
			bT = BTYPELONG;
			break;

		default:
			bT = BTYPEREAL;
			break;
		}

	return bT;
	}

// Persistance help
void CBSAPFULLTagsDeviceOptions::LoadArrays(CTreeFile &Tree)
{
	NameList.Empty();
	OffsetList.Empty();
	MSDList.Empty();
	LNumList.Empty();
	TypeList.Empty();
	DevList.Empty();
	LevList.Empty();
	GlobList.Empty();
	TableList.Empty();
	AStatusList.Empty();

	memset(DevNumList, 0, 15 * sizeof(WORD));

	m_TagsFwdMap.Empty();
	m_TagsRevMap.Empty();

	CString  RefName;
	CAddress Addr;

	UINT  u = 0;
	UINT  uDevNumIndex = 0;

	BOOL fNRT;

	while( !Tree.IsEndOfData() ) {

		CString sName = Tree.GetName();

		if( sName == L"BSAPNAME" ) {

			RefName = Tree.GetValueAsString();	// name
			NameList.Append(RefName);
			fNRT	= IsNRTName(RefName);

			/*sName = */Tree.GetName();			// offset
			u = Tree.GetValueAsInteger();
			OffsetList.Append(LOWORD(u));

			/*sName = */Tree.GetName();			// MSD
			u = Tree.GetValueAsInteger();
			MSDList.Append(LOWORD(u));

			/*sName = */Tree.GetName();			// LNUM
			u = Tree.GetValueAsInteger();
			LNumList.Append(LOWORD(u));

			/*sName = */Tree.GetName();			// Type
			u = Tree.GetValueAsInteger();
			TypeList.Append(LOBYTE(LOWORD(u)));

			/*sName = */Tree.GetName();			// Device number
			u = Tree.GetValueAsInteger();
			DevList.Append(LOBYTE(LOWORD(u)));

			/*sName = */Tree.GetName();			// Level
			u = Tree.GetValueAsInteger();
			LevList.Append(LOBYTE(LOWORD(u)));

			/*sName = */Tree.GetName();			// Global
			u = Tree.GetValueAsInteger();
			GlobList.Append(LOBYTE(LOWORD(u)));

			/*sName = */Tree.GetName();			// Table Number
			u = Tree.GetValueAsInteger();
			TableList.Append(fNRT ? SPNRT0 : LOBYTE(LOWORD(u))); // old database fix

			/*sName = */Tree.GetName();			// Array Item flag
			u = Tree.GetValueAsInteger();
			AStatusList.Append(LOWORD(u));

			continue;
			}

		if( sName == L"BSAPDNM" ) {

			u = Tree.GetValueAsInteger();
			DevNumList[uDevNumIndex++] = LOWORD(u);

			continue;
			}

		if( sName == L"RefName" ) {	// Name/Address Maps

			RefName = Tree.GetValueAsString();

			sName   = Tree.GetName();

			Addr.m_Ref = Tree.GetValueAsInteger();

			if( IsNRTName(sName) ) {

				Addr.a.m_Extra	= 0;
				Addr.a.m_Type	= addrLongAsLong;
				Addr.a.m_Table	= SPNRT0;
				}

			CreateTag( CError(FALSE), RefName, Addr, FALSE, FALSE);

			continue;
			}
		}

	CreateNRTTags();
	}

void CBSAPFULLTagsDeviceOptions::SaveArrays(CTreeFile &Tree) {

	UINT uCnt = NameList.GetCount();

	for( UINT n = 0; n < uCnt; n++ ) {

		CString ss = NameList[n];

		UINT uTabl = IsNRTName(ss) ? SPNRT0 : TableList[n];	// old database fix

		if( ss[0] != '*' ) {

			Tree.PutValue( L"BSAPNAME", NameList[n]);
			Tree.PutValue( L"BSAPOFFS", OffsetList[n]);
			Tree.PutValue( L"BSAPMSD",  MSDList[n]);
			Tree.PutValue( L"BSAPLNUM", LNumList[n]);
			Tree.PutValue( L"BSAPTYPE", TypeList[n]);
			Tree.PutValue( L"BSAPDEV",  DevList[n]);
			Tree.PutValue( L"BSAPLEV",  LevList[n]);
			Tree.PutValue( L"BSAPGLB",  GlobList[n]);
			Tree.PutValue( L"BSAPTAB",  uTabl);
			Tree.PutValue( L"BSAPAST", AStatusList[n]);
			}
		}

	for( UINT n = 0; n < 15; n++ ) {

		UINT w = DevNumList[n];

		Tree.PutValue( L"BSAPDNM",  w);
		}

	RebuildMaps(uCnt);	// don't skip any items

	CTagDataEArray List;
	
	ListTags(List);

	uCnt = List.GetCount();

	CString s;
	UINT    u;

	for( UINT n = 0; n < uCnt; n ++ ) {

		s = List[n].m_Name;
		u = List[n].m_Addr;

		Tree.PutValue( L"RefName", s);	//List[n].m_Name);
		Tree.PutValue( L"RefData", u);	//List[n].m_Addr);
		}
	}

// Meta Data Creation

void CBSAPFULLTagsDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	
	Meta_AddInteger(ArraySelect);
	Meta_AddInteger(ArrayValue);
	Meta_AddString (ArrayString);
	}

// Property Save Filter

BOOL CBSAPFULLTagsDeviceOptions::SaveProp(CString Tag) const
{
	if( Tag == TEXT("Push") ) {
		
		return FALSE;
		}

	return CUIItem::SaveProp(Tag);
	}

void CBSAPFULLTagsDeviceOptions::CreateNRTTags(void) {

	for( UINT k = 0; k < NameList.GetCount(); k++ ) {

		if( IsNRTName(NameList[k]) ) {

			MSDList.SetAt(k, 0);	// old database fix

			return;	// NRT already defined
			}
		}

	CAddress Addr;

	Addr.a.m_Table	= SPNRT0;
	Addr.a.m_Type	= addrLongAsLong;
	Addr.a.m_Extra	= 0;

	UINT uOffset	= 0x8000;

	for( UINT i = 0; i < 8; i++ ) {

		Addr.a.m_Offset = uOffset + i;

		CString sName = MakeNRTTagName(i + 1);

		SetNRTData(sName, Addr.m_Ref);

		CError Error;

		ParseAddress(Error, Addr, sName);
		}
	}

CString CBSAPFULLTagsDeviceOptions::MakeNRTTagName(UINT uSuffix) {

	CString sName;

	sName.Printf( L"z%s%d", (uSuffix < 4) ? L"TMS" : L"NRT", uSuffix);

	return sName;
	}

void CBSAPFULLTagsDeviceOptions::SetNRTData(CString sName, DWORD dRef) {

	ASetName(sName);
	ASetTable(SPNRT0);
	ASetOffs(LOWORD(dRef));
	ASetType(addrLongAsLong);
	ASetMSD (0);
	ASetLNum(0);
	ASetDevAdd(0);
	ASetLevel(0);
	ASetGlobal(0);
	ASetArrayStatus(0);

	AppendToLists(dRef);
	}

BOOL CBSAPFULLTagsDeviceOptions::AccessArrayValue(void)
{
	UINT uFunc	= m_ArraySelect;

	UINT uCurrCount	= NameList.GetCount();

	BOOL fHasList	= (BOOL)uCurrCount;

	DWORD dValue	= m_ArrayValue;

	m_ArraySelect   = 0;

	switch( uFunc ) {

		case WARRNAM:
			ASetName(m_ArrayString);
			return TRUE;

		case WARRMSD:
			ASetMSD(dValue);
			return TRUE;

		case WARRTYP:
			ASetType(dValue);
			return TRUE;

		case WARRLNA:
			ASetLNum(dValue);
			return TRUE;

		case WARRLNM:
			UINT x;

			x = (dValue << 8) & 0xFF00;

			ASetLNum((DWORD)LOWORD((BOOL)x ? x | (m_LNum & 0xFF) : 0));

			return TRUE;

		case WARRLPS:
			m_LNum &= 0xFF00;

			ASetLNum((DWORD)((BOOL)m_LNum ? m_LNum | LOBYTE(LOWORD(dValue)) : 0));

			return TRUE;

		case WARROFF:
			ASetOffs(dValue);
			return TRUE;

		case WARRLVL:
			ASetLevel(dValue);
			return TRUE;

		case WARRDVA:
			ASetDevAdd(dValue);
			return TRUE;

		case WARRGLB:
			ASetGlobal(dValue);
			return TRUE;

		case WARRMLV:
			ASetMLevel(dValue);
			return TRUE;

		case WARRPAT:
			ASetPath(dValue);
			return TRUE;

		case WARRTAB:
			ASetTable(dValue);
			return TRUE;

		case WARRDGL:
			m_DevGlobal = LOWORD(dValue);
			return TRUE;

		case WARRDNM:
			dValue = ASetDevNum(dValue);
			break;

		case WARRAST:
			ASetArrayStatus(dValue);
			break;

		case RARRNAM:
			if( fHasList ) {

				m_ArrayString = NameList[dValue];
				}

			else {
				m_ArrayString = L"";
				}
			break;

		case RARRMSD:
			dValue	= fHasList ? AGetMSD(dValue) : 1;
			break;

		case RARRTYP:
			dValue	= fHasList ? AGetType(dValue) : 0;
			break;

		case RARRLNA:
			dValue	= fHasList ? AGetLNum(dValue) : 0;
			break;

		case RARRLNM:
			dValue	= fHasList ? (AGetLNum(dValue) >> 8) & 0xFF : 0;
			break;

		case RARRLPS:
			dValue	= fHasList ? AGetLNum(dValue) & 0xFF : 0;
			break;

		case RARROFF:
			dValue	= fHasList ? AGetOffs(dValue) : 1;
			break;

		case RARRLVL:
			dValue	= fHasList ? AGetLevel(dValue) : 1;
			break;

		case RARRDVA:
			if( fHasList ) {

				dValue	= AGetDevAdd(dValue);
				}

			else {
				dValue  = m_bLocalAddress + (m_fIsMaster ? 0 : 1);
				}
			break;

		case RARRGLB:
			dValue = AGetGlobal(dValue);
			break;

		case RARRAST:
			dValue = AGetArrayStatus(dValue);
			break;

		case RARRMLV:
			dValue = AGetMLevel(dValue);
			break;

		case RARRPAT:
			dValue = AGetPath(dValue);
			break;

		case RARRTAB:
			dValue = AGetTable(dValue);
			break;

		case RARRDGL:
			dValue = m_DevGlobal;
			break;

		case RARRDNM:
			m_ArrayValue = (DWORD)DevNumList[dValue % 15];
			return TRUE;

		case WARRITM:
			m_uItem	 = min(uCurrCount, (UINT)dValue);
			return TRUE;

		case RARRCNT:
			dValue = uCurrCount;
			break;

		case FINDNAM:
			dValue = AFindName();
			break;

		case FNXTMSD:
			dValue = AFindUnUsedMSD();
			break;

		case CHKMSD:
			if( fHasList ) {

				if( !ANoMSDMatch(dValue) ) {

					dValue = NOTHING;
					}
				}

			else {
				dValue = 1;
				}
			break;

		case RARRITM:
			dValue = AFindIndex(dValue);
			break;

		case WFREEZE:
			m_Index = LOWORD(dValue);
			return TRUE;

		case WCASTAT:
			for( dValue = 0; dValue < uCurrCount; dValue++ ) {

				AStatusList.SetAt( (UINT)dValue, 0);
				}

			return TRUE;

		case EARRFNC:	// enter new
			m_uItem = uCurrCount;	// position for appending (used in Import)

			AppendToLists(dValue);

			return TRUE;

		case FAPPEND:
			AppendToLists(dValue);

			return TRUE;

		case FINSERT:

			if( !fHasList ) {

				AppendToLists(dValue);	// current item values
				}

			else {
				InsertListItem(dValue);
				}

			return TRUE;

		case FDELETE:

			if( fHasList ) {

				DeleteListItem(dValue);
				}

			return TRUE;

		case FREPLCE:

			if( fHasList ) {

				UINT u;

				u = m_Index;

				m_LNum = LOWORD(CheckLineNumbers(m_LNum));

				if( u < 0xFFFF ) {

					NameList.SetAt(u, m_Name);
					OffsetList.SetAt(u, LOWORD(dValue));
					TableList.SetAt(u, m_Table);
					MSDList.SetAt(u, m_MSD);
					LNumList.SetAt(u, m_LNum);
					TypeList.SetAt(u, m_Type);
					DevList.SetAt(u, m_Dev);
					LevList.SetAt(u, m_Lev);
					GlobList.SetAt(u, m_Global);
					AStatusList.SetAt(u, m_ArrayStatus);

					RebuildMaps(u);
					}
				}

			m_Index = 0xFFFF;

			return TRUE;

		case FCLEAR:
			m_Name	= L"";
			m_MSD	= 0;
			m_LNum	= 0;
			m_Type	= addrRealAsReal;
			m_Dev	= m_bLocalAddress;
			m_Lev	= 1;
			m_Global = 100;
			m_ArrayStatus = 0;

			return TRUE;

		case FMASTER:
			if( m_ArrayValue > 1 ) {	// read current value

				m_ArrayValue = m_fIsMaster;
				}

			else {
				m_fIsMaster = (BOOL)m_ArrayValue;
				}

			return TRUE;

		case ADDRLOC:
			m_bLocalAddress = LOBYTE(m_ArrayValue);
			return TRUE;

		case REPALOC:
			m_bLocalAddress = LOBYTE(m_ArrayValue);
			return TRUE;

		case GETALOC:
			m_ArrayValue   = (DWORD)m_bLocalAddress;
			return TRUE;

		case MAKENRT:
			CreateNRTTags();
			return TRUE;

		default:
			return FALSE;
		}

	m_ArrayValue = (UINT)dValue;

	return TRUE;
	}

void CBSAPFULLTagsDeviceOptions::AppendToLists(DWORD dValue)
{
	UINT uCnt = NameList.GetCount();

	BOOL fDeleted = FALSE;

	UINT k = 0;

	while( k < uCnt ) {

		CString sName;

		sName = NameList[k];

		if( sName[0] == '*' ) {	// previous item was deleted, use this space

			NameList.SetAt(k, m_Name);
			TableList.SetAt(k, m_Table);
			OffsetList.SetAt(k, LOWORD(dValue));
			MSDList.SetAt(k, m_MSD);
			LNumList.SetAt(k, m_LNum);
			TypeList.SetAt(k, m_Type);
			DevList.SetAt(k, m_Dev);
			LevList.SetAt(k, m_Lev);
			GlobList.SetAt(k, m_Global);
			AStatusList.SetAt(k, m_ArrayStatus);

			fDeleted = TRUE;

			m_uItem  = k;
			break;
			}

		k++;
		}

	if( !fDeleted ) {

		NameList.Append(m_Name);
		TableList.Append(m_Table);
		OffsetList.Append(LOWORD(dValue));
		MSDList.Append(m_MSD);
		LNumList.Append(m_LNum);
		TypeList.Append(m_Type);
		DevList.Append(m_Dev);
		LevList.Append(m_Lev);
		GlobList.Append(m_Global);
		AStatusList.Append(m_ArrayStatus);
		}

	if( m_Table != SPNRT0 ) {

		WORD w = (m_Lev << 8) | m_Dev;

		DWORD d = ASetDevNum(0x10000 | w);

		if( d < NOTHING ) {

			DevNumList[d % 15] = w;

			dValue &= 0xF0FFFFFF;

			dValue |= d << 24;
			}
		}

	if( dValue ) {

		m_TagsFwdMap.Insert(m_Name, dValue);
		m_TagsRevMap.Insert(dValue, m_Name);
		}
	}

void CBSAPFULLTagsDeviceOptions::InsertListItem(DWORD dValue)
{
	UINT uCount = NameList.GetCount();

	if( m_uItem < uCount ) {

		if( dValue ) {

			m_TagsFwdMap.Insert(m_Name, dValue);
			m_TagsRevMap.Insert(dValue, m_Name);
			}
		}
	}

void CBSAPFULLTagsDeviceOptions::DeleteListItem(DWORD dVal)
{
	UINT uCount = NameList.GetCount();

	if( m_uItem < uCount ) {

		m_TagsFwdMap.Remove(NameList[m_uItem]);
		m_TagsRevMap.Remove(dVal);
		}
	}

void CBSAPFULLTagsDeviceOptions::RebuildMaps(UINT uSkip)
	{
	m_TagsFwdMap.Empty();
	m_TagsRevMap.Empty();

	CAddress Addr;

	for( UINT i = 0; i < NameList.GetCount(); i++ ) {

		if( i != uSkip ) {

			if( IsValidName(NameList[i]) ) {

				Addr.a.m_Offset = OffsetList[i];
				Addr.a.m_Table  = TableList[i];
				Addr.a.m_Type   = TypeList[i];

				WORD w = (LevList[i] << 8) +  DevList[i];

				UINT k = 0;

				Addr.a.m_Extra = 0;

				while( k < 15 ) {

					if( DevNumList[k] == w ) {

						Addr.a.m_Extra = k;

						break;
						}

					k++;
					}

				if( IsACK(Addr.a.m_Table) ) {

					Addr.a.m_Extra = 0;
					}

				CString sName = NameList[i];

				m_TagsFwdMap.Insert(sName, Addr.m_Ref);
				m_TagsRevMap.Insert(Addr.m_Ref, sName);
				}
			}
		}
	}

BOOL CBSAPFULLTagsDeviceOptions::IsValidName(CString sName)
	{
	return sName[0] != '*';
	}

void CBSAPFULLTagsDeviceOptions::ASetName(CString sName)
	{
	m_Name = sName;
	}

void CBSAPFULLTagsDeviceOptions::ASetMSD(DWORD dNew)
	{
	m_MSD = LOWORD(dNew);
	}

void CBSAPFULLTagsDeviceOptions::ASetType(DWORD dNew)
{
	m_Type = LOBYTE(dNew);
	}

void CBSAPFULLTagsDeviceOptions::ASetLNum(DWORD dNew)
{
	m_LNum = LOWORD(CheckLineNumbers((UINT)dNew));
	}

void CBSAPFULLTagsDeviceOptions::ASetOffs(DWORD dNew)
	{
	m_Offset = LOWORD(dNew);
	}

void CBSAPFULLTagsDeviceOptions::ASetLevel(DWORD dNew) {

	m_Lev = LOBYTE(dNew);
	}

void CBSAPFULLTagsDeviceOptions::ASetDevAdd(DWORD dNew) {

	m_Dev = LOBYTE(dNew);
	}

void CBSAPFULLTagsDeviceOptions::ASetGlobal(DWORD dNew) {

	m_Global = LOWORD(dNew);
	}

void CBSAPFULLTagsDeviceOptions::ASetArrayStatus(DWORD dNew) {

	m_ArrayStatus = LOWORD(dNew);
	}

void CBSAPFULLTagsDeviceOptions::ASetMLevel(DWORD dNew) {

	m_MLevel = LOBYTE(dNew);
	}

void CBSAPFULLTagsDeviceOptions::ASetPath(DWORD dNew) {

	m_Path = LOBYTE(dNew);
	}

void CBSAPFULLTagsDeviceOptions::ASetTable(DWORD dNew) {

	m_Table = LOBYTE(dNew);
	}

DWORD CBSAPFULLTagsDeviceOptions::ASetDevNum(DWORD dItem) {

	if( !NameList.GetCount() && dItem & 0x10000 ) {	// need starting index

		return 0;
		}

	WORD wLD    = LOWORD(dItem);		// level / device address to be used
	PWORD p     = DevNumList;
	UINT uFirst = 16;
	UINT i      = 0;

	while( i < 15 ) {

		WORD wItm = *p;

		if( wItm == wLD ) {		// level / device address already entered

			return i;
			}

		if( !wItm ) {

			if( uFirst > 15 ) uFirst = i;

			if( dItem & 0x10000 ) {	// want only available index

				return uFirst;
				}
			}

		i++;
		p++;
		}

	if( uFirst == 16 ) {				// table is full

		return NOTHING;
		}

	if( !(dItem & 0x10000) ) {		// if tag creation, don't save at this point

		DevNumList[uFirst] = wLD;	// add level / device number entry to list
		}

	return uFirst;			// return index
	}

UINT CBSAPFULLTagsDeviceOptions::CheckLineNumbers(UINT uLNum) {

	if( !HIBYTE(LOWORD(uLNum)) ) {

		return 0;
		}

	if( LOBYTE(LOWORD(uLNum)) == 255 ) {

		uLNum--;
		}

	return uLNum;
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetArrayCount(void)
{
	return NameList.GetCount();
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetMSD(UINT uItem)
{
	return MSDList[uItem];
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetType(UINT uItem)
{
	return (DWORD)TypeList[uItem];
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetLNum(UINT uItem)
{
	return (DWORD)(LNumList[uItem]);
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetOffs(UINT uItem)
	{
	return (DWORD)(OffsetList[uItem]);
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetLevel(UINT uItem) {

	return (DWORD)(LevList[uItem]);
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetDevAdd(UINT uItem) {

	return (DWORD)(DevList[uItem]);
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetGlobal(UINT uItem) {

	return (DWORD)(GlobList[uItem]);
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetArrayStatus(UINT uItem) {

	return (DWORD)(AStatusList[uItem]);
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetMLevel(UINT uItem) {

	return (DWORD)m_MLevel;
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetPath(UINT uItem) {

	return (DWORD)m_Path;
	}

DWORD CBSAPFULLTagsDeviceOptions::AGetTable(UINT uItem) {

	return (DWORD)LOBYTE(TableList[uItem]);
	}

DWORD CBSAPFULLTagsDeviceOptions::AFindName(void) {

	for( UINT i = 0; i < NameList.GetCount(); i++ ) {

		if( NameList[i] == m_ArrayString ) {

			return i;
			}
		}

	return 0;
	}

UINT CBSAPFULLTagsDeviceOptions::AFindIndex(DWORD dSel)
	{
	UINT u = 0;
	UINT c = OffsetList.GetCount();

	CAddress A;

	A.m_Ref = dSel;

	UINT o = A.a.m_Offset;
	UINT t = A.a.m_Table;
	UINT y = A.a.m_Type;
	UINT a = DevNumList[A.a.m_Extra];

	while( u < c ) {

		if( o == OffsetList[u] && t == TableList[u] && y == TypeList[u] ) {

			if( IsNRTTable(t) || (BOOL)a ) return u;
			}

		u++;
		}

	return NOTHING;
	}

DWORD CBSAPFULLTagsDeviceOptions::AFindUnUsedMSD(void)
	{
	DWORD d = DeletedItemCheck();

	if( d < NOTHING ) {

		return d;
		}

	UINT uCount =  MSDList.GetCount();

	WORD w = 1;

	UINT n = 0;

	while( n < uCount ) {	// find unique d

		if( w == MSDList[n] ) {

			w++;

			n = 0;			// list cannot be of infinite length
			}

		else {
			n++;
			}
		}

	return (DWORD)w;	// uCount = 0, or no match found
	}

BOOL CBSAPFULLTagsDeviceOptions::ANoMSDMatch(DWORD dData)
	{
	WORD wData = LOWORD(dData);

	for( UINT n = 0; n < MSDList.GetCount(); n++ ) {

		if( wData == MSDList[n] ) {

			return FALSE;
			}
		}

	return TRUE;
	}

DWORD CBSAPFULLTagsDeviceOptions::DeletedItemCheck(void)
	{
	for( UINT i = 0; i < NameList.GetCount(); i++ ) {

		if( !IsValidName(NameList[i]) ) {

			return (DWORD)i;	// reuse deleted items
			}
		}

	return NOTHING;
	}

// Import/Export
void CBSAPFULLTagsDeviceOptions::OnImport(CWnd *pWnd)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("Bristol Babcock BSAP"));

	Dlg.SetCaption(CPrintf("Import Tags for %s", GetDeviceName()));

	Dlg.SetFilter(TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;

		CString sFileName = Dlg.GetFilename();
		
		if( !(pFile = fopen(sFileName, TEXT("rt"))) ) {

			CString Text = TEXT("Unable to open file for reading.");

			pWnd->Error(Text);
			}
		else {
			if( m_TagsFwdMap.GetCount() ) {

				CString Text = TEXT("All tags will be replaced with data from the selected file.\n\nDo you want to continue?");

				if( pWnd->YesNo(Text) == IDYES ) {

					m_TagsFwdMap.Empty();
					m_TagsRevMap.Empty();
				
					m_AddrArray.Empty();

					NameList.Empty();
					OffsetList.Empty();
					MSDList.Empty();
					TypeList.Empty();
					LNumList.Empty();
					LevList.Empty();
					DevList.Empty();
					GlobList.Empty();
					TableList.Empty();
					AStatusList.Empty();
					}
				else {
					fclose(pFile);
					
					return;
					}
				}

			afxThread->SetWaitMode(TRUE);

			if( Import(pFile) ) {

				SetDirty();

				Dlg.SaveLastPath(TEXT("Bristol Babcock BSAP"));
				}

			else ShowErrors(pWnd);
			
			fclose(pFile);

			afxThread->SetWaitMode(FALSE);
			}
		}
	}

void CBSAPFULLTagsDeviceOptions::OnExport(CWnd *pWnd)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("Bristol Babcock BSAP"));

	Dlg.SetCaption(CPrintf("Export Tags for %s", GetDeviceName()));

	Dlg.SetFilter (TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( pFile = fopen(Dlg.GetFilename(), TEXT("rb")) ) {
			
			fclose(pFile);

			CString Text = TEXT("The selected file already exists.\r\nDo you want to overwrite it?");

			if( pWnd->YesNo(Text) == IDNO ) {

				return;
				}
			}

		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			CString Text = TEXT("Unable to open file for writing.");

			pWnd->Error(Text);

			return;
			}

		Export(pFile);

		fclose(pFile);

		Dlg.SaveLastPath(TEXT("Bristol Babcock BSAP"));
		}
	}

// Make Tags
CString CBSAPFULLTagsDeviceOptions::CreateName(PCTXT pFormat, CString PostFix)
{
	for( UINT n = 1;; n ++ ) {

		CPrintf Name(pFormat, n);

		CString NameTemp;

		NameTemp.Printf( L"%s%s", Name, PostFix );

		INDEX Index = m_TagsFwdMap.FindName(NameTemp);

		if( m_TagsFwdMap.Failed(Index) ) {
			
			return Name;
			}
		}
	}

UINT CBSAPFULLTagsDeviceOptions::CreateIndex(BOOL fUpdate)
{
	if( !fUpdate && m_AddrArray.GetCount() ) {

		UINT n = m_AddrArray[0];

		m_AddrArray.Remove(0);

		return n;
		}

	return GetNewIndex();
	}

UINT CBSAPFULLTagsDeviceOptions::GetNewIndex(void)
{
	return AGetArrayCount();
	}

CString CBSAPFULLTagsDeviceOptions::GetVarType(CAddress const &Addr)
{
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	return TEXT("f");
		case addrByteAsByte:	return TEXT("b");
		case addrWordAsWord:	return TEXT("w");
		case addrLongAsLong:	return IsSTRINGTable(Addr.a.m_Table) ? TEXT("s") : TEXT("d");
		}

	return TEXT("r");
	}

CString CBSAPFULLTagsDeviceOptions::GetTagName(CString const &Text)
{
	return Text;
	}

UINT CBSAPFULLTagsDeviceOptions::GetIndex(CAddress const &Addr)
{
	return AFindIndex(Addr.m_Ref);
	}

BOOL CBSAPFULLTagsDeviceOptions::SetStringOffset(CAddress &Addr)
{
	if( IsSTRINGTable(Addr.a.m_Table) ) {

		Addr.a.m_Offset = m_uStrIndex;

		m_uStrIndex += MAXDWORDLEN;

		return TRUE;
		}

	return FALSE;
	}

// Import/Export Support

void CBSAPFULLTagsDeviceOptions::Export(FILE *pFile)
{
	CTagDataEArray List;

	ListTags(List);

	List.Sort();

	fprintf(pFile, TEXT("%s%s\n\0"), TEXT(STRHDR), PCTXT(GetDeviceName()));

	fprintf(pFile, TEXT("NAME,INDEX,MSD,TYPE,LEVEL,LOCAL,GLOBAL,LIST,LITEM,ARRAY,COLUMNS\n\0"));

	UINT uIndex = 0;

	UINT n;

	for( n = 0; n < List.GetCount(); n ++ ) {

		CString Type;

		Type = NameList[n].Left(4);

		if( !IsNRTName(Type) ) {	// skip TMS/NRT for now

			ExpandType(Type, AGetType(n), IsSTRINGTable(TableList[n]));

			WORD lnum = LOWORD(AGetLNum(n));

			CString Name = NameList[n];

			UINT uFind = Name.Find('#');

			if( uFind < NOTHING ) {

				Name = Name.Left(uFind);
				}

			fprintf(pFile,
				TEXT("%s,%d,%d,%s,%1.1d,%2.2d,%d,%d,%d,%d,%d\n\0"), 
				PCTXT(Name),
				uIndex,
				MSDList[n],
				PCTXT(Type),
				AGetLevel(n),
				AGetDevAdd(n),
				AGetGlobal(n),
				HIBYTE(lnum),
				LOBYTE(lnum),
				LOBYTE(AStatusList[n]),
				HIBYTE(AStatusList[n])
				);

			uIndex++;
			}
		}

	// Append TMS/NRT

	for( n = 1; n < 4; n++ ) {

		fprintf(pFile, L"zTMS%d,%d,0,NRT,0,0,0,0,0,0,0\n\0", n, 32767 + n);
		}

	for( n = 4; n < 9; n++ ) {
		fprintf(pFile, L"zNRT%d,%d,0,NRT,0,0,0,0,0,0,0\n\0", n, 32767 + n);
		}
	}

BOOL CBSAPFULLTagsDeviceOptions::Import(FILE *pFile)
{
	m_Errors.Empty();

	m_uStrIndex = 0;

	DWORD dwPos = ftell(pFile);

	UINT uLine = 0;

	fseek(pFile, dwPos, SEEK_SET);

	BOOL fNoNRT = TRUE;

	UINT Columns[10];

	memset(Columns, 0, sizeof(Columns));

	memset(&m_ImportInx, 0, sizeof(m_ImportInx));

	CString sStatus;

	sStatus.Printf("Starting Import...");

	afxThread->SetStatusText(sStatus);

	while( !feof(pFile) ) {

		char sLine[256] = {0};

		fgets(sLine, sizeof(sLine), pFile);

		CStringArray List;

		CString Line;

		UINT uLCt = 0;

		if( !uLine ) {

			sLine[strlen(sLine)-1] = 0;

			Line = CString(sLine);

			CString s = Line;

			s.MakeUpper();

			if( s.Find(L"BRISTOL") == NOTHING || s.Find(L"BABCOCK") == NOTHING ) return FALSE;

			fgets(sLine, sizeof(sLine), pFile);

			sLine[strlen(sLine) - 1] = 0;

			Line = CString(sLine);

			Line.Tokenize(List, ',');

			uLCt = List.GetCount();

			if( uLCt < 3 ) {

				return FALSE;
				}

			if( !AssignColumns(Columns, sLine) ) return FALSE;

			uLine = 2;

			fgets(sLine, sizeof(sLine), pFile);

			sLine[strlen(sLine) - 1] = 0;
			}

		CAddress Addr;

		Addr.a.m_Table	= SPREAL1;
		Addr.a.m_Offset	= 1;
		Addr.a.m_Type	= addrRealAsReal;
		Addr.a.m_Extra	= 0;

		CString sName	= L"";
		WORD	wTabl	= SPREAL1;
		WORD	wOffs	= 1;
		WORD	wMSD	= 1;
		WORD	wLNum	= 0;
		WORD	wType	= addrRealAsReal;
		WORD	wLevel	= 1;
		WORD	wDevAdd	= 1;
		WORD	wAStat	= 0;
		WORD	wGlob	= 0;

		if( sLine[0] ) {

			List.Empty();

			Line = CString(sLine);

			Line.Tokenize(List, ',');

			uLCt = List.GetCount();

			CString sType;

			sType	= List[Columns[0]];					// Name
			sType.Remove('"');
			sName	= sType;

			if( sName[0] != 'z' ) {

				CString sL = List[Columns[3]];
				CString sD = List[Columns[4]];

				sL.Remove('"');
				sD.Remove('"');
			
				sName.Printf( L"%s#L%sD%2.2X#", sName, sL.Right(1), tatoi(sD));		// Add Level/Device Postfix for uniqueness

				wLevel	= MakeImportValue(sL);					// Device Level

				wDevAdd	= MakeImportValue(sD);					// Local Address

				wGlob	= MakeImportValue(List[Columns[5]]);			// Global Address

				if( m_fIsMaster ) {

					wMSD	= 0;
					wLNum	= 0;
					wAStat	= 0;
					}

				else {
					wMSD	= MakeImportValue(List[Columns[1]]);		// MSD

					WORD w	= MakeImportValue(List[Columns[6]]) << 8;	// List Number
					wLNum	= w | MakeImportValue(List[Columns[7]]);	// List Item;

					w	= MakeImportValue(List[Columns[9]]) << 8;	// Array Item 0
					wAStat	= w | MakeImportValue(List[Columns[8]]);	// Array Number
					}
				}

			sType	= List[Columns[2]];	// Data Type

			if( sType == "BIT" ) {

				wType		= addrBitAsBit;
				Addr.a.m_Table	= SPBIT0 + wLevel;
				Addr.a.m_Type	= wType;
				}
			
			else if( sType == "BYTE" ) {

				wType		= addrByteAsByte;
				Addr.a.m_Table	= SPBYTE0 + wLevel;
				Addr.a.m_Type	= wType;
				}

			else if( sType == "WORD" ) {

				wType		= addrWordAsWord;
				Addr.a.m_Table	= SPWORD0 + wLevel;
				Addr.a.m_Type	= wType;
				}

			else if( sType == "DWORD" ) {

				wType		= addrLongAsLong;
				Addr.a.m_Table	= SPDWORD0 + wLevel;
				Addr.a.m_Type	= wType;
				}

			else if( sType == "STRING" ) {

				wType		= addrLongAsLong;
				Addr.a.m_Table	= SPSTRING0 + wLevel;
				Addr.a.m_Type	= wType;
				}

			else {
				wType		= addrRealAsReal;
				Addr.a.m_Table	= SPREAL0 + wLevel;
				Addr.a.m_Type	= wType;
				}

			wTabl = Addr.a.m_Table;

			if( !IsNRTTable(wTabl) && !IsSTRINGTable(wTabl) ) {

				Addr.a.m_Extra = ASetDevNum((wLevel << 8) + wDevAdd);

				wOffs = m_fIsMaster ? (wTabl * 1000) + MakeImportIndex(wTabl) : wMSD;

				Addr.a.m_Offset = wOffs;
				}
			}

		else {
			break;
			}

		if( CreateTag(CError(FALSE), sName, Addr, FALSE, FALSE) ) {

			ASetName(sName);
			ASetTable((DWORD)wTabl);
			ASetOffs((DWORD)wOffs);
			ASetMSD ((DWORD)wMSD);
			ASetLNum((DWORD)wLNum);
			ASetType((DWORD)wType);
			ASetLevel((DWORD)wLevel);
			ASetDevAdd((DWORD)wDevAdd);
			ASetGlobal((DWORD)wGlob);
			ASetArrayStatus((DWORD)wAStat);

			AppendToLists(Addr.m_Ref);

			sStatus.Printf("Importing Name %d %s...", NameList.GetCount(), sName);

			afxThread->SetStatusText(sStatus);
			}

		else {
			m_Errors.Append(uLine);
			}

		uLine++;
		}

	sStatus.Printf("%d Names Imported", NameList.GetCount());

	afxThread->SetStatusText(sStatus);

//	SortBBTags(AGetArrayCount());	// Testing the sort process

	if( fNoNRT ) CreateNRTTags();

	return m_Errors.GetCount() == 0;
	}

WORD CBSAPFULLTagsDeviceOptions::MakeImportIndex(UINT uTable) {

	PWORD pItem  = NULL;

	if( IsBITTable(uTable) ) {

		pItem = &m_ImportInx.wBit[uTable - SPBIT0];
		}

	else if( IsBYTETable(uTable) ) {

		pItem = &m_ImportInx.wByte[uTable - SPBYTE0];
		}

	else if( IsWORDTable(uTable) ) {

		pItem = &m_ImportInx.wWord[uTable - SPWORD0];
		}

	else if( IsDWORDTable(uTable) ) {

		pItem = &m_ImportInx.wDword[uTable - SPDWORD0];
		}

	else if( IsREALTable(uTable) ) {

		pItem = &m_ImportInx.wReal[uTable - SPREAL0];
		}

	else if( IsSTRINGTable(uTable) ) {

		pItem = &m_ImportInx.wString[uTable - SPSTRING0];
		}

	else if( uTable == SPBITINT ) {

		pItem = &m_ImportInx.wBitInt;
		}

	else if( uTable == SPBYTEINT ) {

		pItem = &m_ImportInx.wByteInt;
		}

	else if( uTable == SPWORDINT ) {

		pItem = &m_ImportInx.wWordInt;
		}

	else if( uTable == SPDWORDINT ) {

		pItem = &m_ImportInx.wDwordInt;
		}

	else if( uTable == SPREALINT ) {

		pItem = &m_ImportInx.wRealInt;
		}

	else if( uTable == SPSTRINGINT ) {

		pItem = &m_ImportInx.wStringInt;
		}

	else if( uTable == SPBOOLACK ) {

		pItem = &m_ImportInx.wBoolAck;
		}

	else if( uTable == SPBYTEACK ) {

		pItem = &m_ImportInx.wByteAck;
		}

	else if( uTable == SPWORDACK ) {

		pItem = &m_ImportInx.wWordAck;
		}

	else if( uTable == SPDWORDACK ) {

		pItem = &m_ImportInx.wDwordAck;
		}

	else if( uTable >= SPREALACK ) {

		pItem = &m_ImportInx.wRealAck;
		}

	WORD wResult = 0;

	if( pItem ) {

		wResult = *pItem;

		(*pItem)++;
		}

	return wResult;
	}

WORD CBSAPFULLTagsDeviceOptions::MakeImportValue(CString s) {

	s.Remove('"');

	return LOWORD(tatoi(s));
	}

BOOL CBSAPFULLTagsDeviceOptions::AssignColumns(UINT *pColumns, char *sLine) {

	CString Line;

	Line = CString(sLine);

	Line.MakeUpper();

	pColumns[0]	= FindColItem(CString(L"NAME"),   Line);
	pColumns[1]	= FindColItem(CString(L"MSD"),    Line);
	pColumns[2]	= FindColItem(CString(L"TYPE"),   Line);
	pColumns[3]	= FindColItem(CString(L"LEVEL"),  Line);
	pColumns[4]	= FindColItem(CString(L"LOCAL"),  Line);
// the above 5 are Mandatory
	pColumns[5]	= FindColItem(CString(L"GLOBAL"),  Line);
	pColumns[6]	= FindColItem(CString(L"LIST"),    Line);
	pColumns[7]	= FindColItem(CString(L"LITEM"),   Line);
	pColumns[8]	= FindColItem(CString(L"ARRAY"),   Line);
	pColumns[9]	= FindColItem(CString(L"COLUMNS"), Line);

	if(	(BOOL)pColumns[0]  &&
		(BOOL)pColumns[1]  &&
		(BOOL)pColumns[2]  &&
		(BOOL)pColumns[3]  &&
		(BOOL)pColumns[4]) {

		for( UINT k = 0; k < 10; k++ ) {	// normalize to 0 based

			pColumns[k] -= 1;
			}

		return TRUE;
		}

	return FALSE;
	}

UINT CBSAPFULLTagsDeviceOptions::FindColItem(CString s, CString Line) {

	CStringArray List;

	Line.Tokenize(List, ',');

	UINT uCount = List.GetCount();

	UINT uTestL = s.GetLength();

	for( UINT i = 0; i < uCount; i++ ) {

		if( s == List[i].Left(uTestL) ) {

			return i + 1;
			}
		}

	return 0;
	}

void CBSAPFULLTagsDeviceOptions::ExpandType(CString &Text, UINT uItem, BOOL fStr)
{
	switch( uItem ) {

		case addrBitAsBit:      Text = TEXT("BIT");	                        return;
		case addrByteAsByte:	Text = TEXT("BYTE");	                        return;
		case addrWordAsWord:	Text = TEXT("WORD");	                        return;
		case addrLongAsLong:	Text = fStr ? TEXT("STRING") : TEXT("DWORD");	return;
		}

	Text = TEXT("REAL");
	}

void CBSAPFULLTagsDeviceOptions::ShowErrors(CWnd *pWnd)
{
	if( m_Errors.GetCount() ) {
		
		afxThread->SetWaitMode(TRUE);
		
		CString Lines;

		UINT uErrMax = 101;
		
		for( UINT i = 0; i < m_Errors.GetCount(); ) {

			if( i )
				Lines += ", ";
			
			Lines += CPrintf("%d", m_Errors[i]);

			if( ++ i >= uErrMax ) {

				CString Text = TEXT("...\nFile contains more than %d errors");
				
				Lines += CPrintf(Text, uErrMax);

				break;
				}
			}

		afxThread->SetWaitMode(FALSE);

		CString Text1 = TEXT("The following lines in the file contained errors :\n\n%s.");
		CString Text2 = TEXT("\n\nIt is possibly a duplicated name, or a bad data type.");
		CString Text3 = TEXT("\n\nThe lines without errors were still imported.");

		CString Text  = Text1 + Text2 + Text3;

		pWnd->Error(CPrintf(Text, Lines));
		}
	else {
		CString Text = TEXT("The operation completed without error.");

		pWnd->Information(Text);
		}
	}

DWORD CBSAPFULLTagsDeviceOptions::GetAddrFromName(CString sName) {

	INDEX  Index = m_TagsFwdMap.FindName(sName);

	if( !m_TagsFwdMap.Failed(Index) ) {

		return m_TagsFwdMap.GetData(Index);
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLTagsUDPDeviceOptions, CBSAPFULLTagsDeviceOptions);

// Constructor

CBSAPFULLTagsUDPDeviceOptions::CBSAPFULLTagsUDPDeviceOptions(void)
{
	}

// Download Support
BOOL CBSAPFULLTagsUDPDeviceOptions::MakeInitData(CInitData &Init)
{
	return CBSAPFULLTagsDeviceOptions::MakeInitData(Init);
	}

// Meta Data Creation

void CBSAPFULLTagsUDPDeviceOptions::AddMetaData(void)
{
	CBSAPFULLTagsDeviceOptions::AddMetaData();

	Meta_AddInteger(Push);
	}

// UI Management

void CBSAPFULLTagsUDPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	CBSAPFULLTagsDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Address Management

BOOL CBSAPFULLTagsUDPDeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	return CBSAPFULLTagsDeviceOptions::ParseAddress(Error, Addr, Text);
	}

BOOL CBSAPFULLTagsUDPDeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	return CBSAPFULLTagsDeviceOptions::ExpandAddress(Text, Addr);
	}

BOOL CBSAPFULLTagsUDPDeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	return CBSAPFULLTagsDeviceOptions::ListAddress(pRoot, uItem, Data);
	}

BOOL CBSAPFULLTagsUDPDeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CBSAPFULLTagsUDPDriver Driver;
	
	CBSAPFULLTagsUDPDialog Dlg(Driver, Addr, this, fPart);

	CString Title;

	if( fPart ) {

		CString Name;

		ExpandAddress(Name, Addr);

		Title = CPrintf("Select Address for Tag %s", Name); 
		}

	else
		Title = CPrintf("Select Variable for %s", GetDeviceName());

	Dlg.SetCaption(Title);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation

void CBSAPFULLTagsUDPDeviceOptions::OnManage(CWnd *pWnd)
{
	CBSAPFULLTagsDeviceOptions::CreateNRTTags();	// if not already defined

	CAddress Addr;

	Addr.m_Ref = 0;

	CBSAPFULLTagsUDPDriver Driver;
	
	CBSAPFULLTagsUDPDialog Dlg(Driver, Addr, this);

	Dlg.SetCaption(CPrintf("Variable Names for %s", GetDeviceName()));

	Dlg.SetSelect(FALSE);

	Dlg.Execute(*pWnd);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLUDPDeviceOptions, CBSAPFULLTagsUDPDeviceOptions);

// Constructor

CBSAPFULLUDPDeviceOptions::CBSAPFULLUDPDeviceOptions(void)
{
	m_IPAddr	= DWORD(MAKELONG(MAKEWORD(20, 21), MAKEWORD(168, 192) ));

	m_Local		= 1;

	m_Global	= 100;

	m_Level		= 0;

	m_Path		= 2;

	m_MLevel	= 0;

	m_Socket	= 1234;

	m_Keep		= TRUE;

	m_Time1		= 5000;

	m_Time2		= 2500;

	m_Time3		= 200;

	m_fIsMaster	= 1;
	}

// UI Management
void CBSAPFULLUDPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == TEXT("Push") ) {

			switch( m_Push ) {

				case 0:
					OnManage(pWnd);
					break;

				case 1:
					CBSAPFULLTagsDeviceOptions::OnImport(pWnd);
					break;

				case 2:
					CBSAPFULLTagsDeviceOptions::OnExport(pWnd);
					break;
				}
			}

		if( Tag.IsEmpty() || Tag == "Local" || Tag == "Global" ) {

			m_ArraySelect = ADDRLOC;
			m_ArrayValue  = m_Local;

			AccessArrayValue();

			m_ArraySelect = WARRDGL;
			m_ArrayValue  = m_Global;

			AccessArrayValue();
			}

		if( Tag.IsEmpty() || Tag == "Level" || Tag == "MLevel" || Tag == "PATH" ) {

			m_Level = max( m_Level, m_MLevel);

			pWnd->UpdateUI("Level");

			pWnd->EnableUI("Path", m_Level > m_MLevel);

			m_ArraySelect = WARRMLV;
			m_ArrayValue  = m_MLevel;
			AccessArrayValue();

			m_ArraySelect = WARRGLB;
			m_ArrayValue = m_Global;
			AccessArrayValue();

			m_ArraySelect = WARRPAT;
			m_ArrayValue  = m_Path;
			AccessArrayValue();
			}

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}

		if( Tag.IsEmpty() ) CreateNRTTags();
		}

	CBSAPFULLTagsUDPDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CBSAPFULLUDPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Local));
	Init.AddWord(WORD(m_Global));
	Init.AddByte(BYTE(m_Level));
	Init.AddByte(BYTE(m_Path));
	Init.AddByte(BYTE(m_MLevel));
	Init.AddLong(LONG(m_IPAddr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return CBSAPFULLTagsUDPDeviceOptions::MakeInitData(Init);
	}

// Meta Data Creation

void CBSAPFULLUDPDeviceOptions::AddMetaData(void)
{
	CBSAPFULLTagsUDPDeviceOptions::AddMetaData();

	Meta_AddInteger(Local);
	Meta_AddInteger(Global);
	Meta_AddInteger(Level);
	Meta_AddInteger(Path);
	Meta_AddInteger(MLevel);
	Meta_AddInteger(IPAddr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);

	m_ArraySelect = ADDRLOC;
	m_ArrayValue  = m_Local;

	AccessArrayValue();

	m_ArraySelect = WARRDGL;
	m_ArrayValue  = m_Global;

	AccessArrayValue();
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLTagsSerialDeviceOptions, CBSAPFULLTagsDeviceOptions);

// Constructor

CBSAPFULLTagsSerialDeviceOptions::CBSAPFULLTagsSerialDeviceOptions(void)
{
	}

// Download Support
BOOL CBSAPFULLTagsSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	return CBSAPFULLTagsDeviceOptions::MakeInitData(Init);
	}

void CBSAPFULLTagsSerialDeviceOptions::AddMetaData(void)
{
	CBSAPFULLTagsDeviceOptions::AddMetaData();

	Meta_AddInteger(Push);
	}

// UI Management

void CBSAPFULLTagsSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	CBSAPFULLTagsDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Address Management

BOOL CBSAPFULLTagsSerialDeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	return CBSAPFULLTagsDeviceOptions::ParseAddress(Error, Addr, Text);
	}

BOOL CBSAPFULLTagsSerialDeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	return CBSAPFULLTagsDeviceOptions::ExpandAddress(Text, Addr);
	}

BOOL CBSAPFULLTagsSerialDeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	return CBSAPFULLTagsDeviceOptions::ListAddress(pRoot, uItem, Data);
	}

BOOL CBSAPFULLTagsSerialDeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CBSAPFULLTagsSerialDriver Driver;
	
	CBSAPFULLTagsSerialDialog Dlg(Driver, Addr, this, fPart);

	CString Title;

	if( fPart ) {

		CString Name;

		ExpandAddress(Name, Addr);

		Title = CPrintf("Select Address for Tag %s", Name); 
		}

	else
		Title = CPrintf("Select Variable for %s", GetDeviceName());

	Dlg.SetCaption(Title);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation

void CBSAPFULLTagsSerialDeviceOptions::OnManage(CWnd *pWnd)
{
	CBSAPFULLTagsDeviceOptions::CreateNRTTags();

	CAddress Addr;

	Addr.m_Ref = 0;

	CBSAPFULLTagsSerialDriver Driver;
	
	CBSAPFULLTagsSerialDialog Dlg(Driver, Addr, this);

	Dlg.SetCaption(CPrintf("Variable Names for %s", GetDeviceName()));

	Dlg.SetSelect(FALSE);

	Dlg.Execute(*pWnd);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLSerialDeviceOptions, CBSAPFULLTagsSerialDeviceOptions);

// Constructor

CBSAPFULLSerialDeviceOptions::CBSAPFULLSerialDeviceOptions(void)
{
	m_Local  = 1;
	m_Global = 100;
	m_Level  = 0;
	m_Path   = 2;
	m_MLevel = 0;
	m_fIsMaster = 1;
	}

// UI Management

void CBSAPFULLSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == TEXT("Push") ) {

			switch( m_Push ) {

				case 0:
					OnManage(pWnd);
					break;

				case 1:
					CBSAPFULLTagsDeviceOptions::OnImport(pWnd);
					break;

				case 2:
					CBSAPFULLTagsDeviceOptions::OnExport(pWnd);
					break;
				}
			}

		if( Tag.IsEmpty() || Tag == "Local" || Tag == "Global" ) {

			m_ArraySelect = ADDRLOC;
			m_ArrayValue  = m_Local;

			AccessArrayValue();

			m_ArraySelect = WARRDGL;
			m_ArrayValue  = m_Global;

			AccessArrayValue();
			}

		if( Tag.IsEmpty() || Tag == "Level" || Tag == "MLevel" || Tag == "PATH" ) {

			m_Level = max( m_Level, m_MLevel);

			pWnd->UpdateUI("Level");

			pWnd->EnableUI("Path", m_Level > m_MLevel);

			m_ArraySelect = WARRMLV;
			m_ArrayValue  = m_MLevel;

			AccessArrayValue();

			m_ArraySelect = WARRGLB;
			m_ArrayValue = m_Global;

			AccessArrayValue();

			m_ArraySelect = WARRPAT;
			m_ArrayValue  = m_Path;

			AccessArrayValue();
			}

		if( Tag.IsEmpty() ) CreateNRTTags();
		}

	CBSAPFULLTagsSerialDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CBSAPFULLSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Local));
	Init.AddWord(WORD(m_Global));
	Init.AddByte(BYTE(m_Level));
	Init.AddByte(BYTE(m_Path));
	Init.AddByte(BYTE(m_MLevel));

	return CBSAPFULLTagsSerialDeviceOptions::MakeInitData(Init);
	}

// Meta Data Creation

void CBSAPFULLSerialDeviceOptions::AddMetaData(void)
{
	CBSAPFULLTagsSerialDeviceOptions::AddMetaData();

	Meta_AddInteger(Local);
	Meta_AddInteger(Global);
	Meta_AddInteger(Level);
	Meta_AddInteger(Path);
	Meta_AddInteger(MLevel);

	m_ArraySelect = ADDRLOC;
	m_ArrayValue  = m_Local;

	AccessArrayValue();

	m_ArraySelect = WARRDGL;
	m_ArrayValue  = m_Global;

	AccessArrayValue();
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Slave Tags Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLTagsUDPSDeviceOptions, CBSAPFULLTagsDeviceOptions);

// Constructor

CBSAPFULLTagsUDPSDeviceOptions::CBSAPFULLTagsUDPSDeviceOptions(void)
{
	}

// Download Support
BOOL CBSAPFULLTagsUDPSDeviceOptions::MakeInitData(CInitData &Init)
{
	return CBSAPFULLTagsDeviceOptions::MakeInitData(Init);
	}

// Meta Data Creation

void CBSAPFULLTagsUDPSDeviceOptions::AddMetaData(void)
{
	CBSAPFULLTagsDeviceOptions::AddMetaData();

	Meta_AddInteger(Push);
	}

// UI Management

void CBSAPFULLTagsUDPSDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	CBSAPFULLTagsDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Address Management

BOOL CBSAPFULLTagsUDPSDeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	return CBSAPFULLTagsDeviceOptions::ParseAddress(Error, Addr, Text);
	}

BOOL CBSAPFULLTagsUDPSDeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	return CBSAPFULLTagsDeviceOptions::ExpandAddress(Text, Addr);
	}

BOOL CBSAPFULLTagsUDPSDeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	return CBSAPFULLTagsDeviceOptions::ListAddress(pRoot, uItem, Data);
	}

BOOL CBSAPFULLTagsUDPSDeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CBSAPFULLTagsUDPSDriver Driver;
	
	CBSAPFULLTagsUDPSDialog Dlg(Driver, Addr, this, fPart);

	CString Title;

	if( fPart ) {

		CString Name;

		ExpandAddress(Name, Addr);

		Title = CPrintf("Select Address for Tag %s", Name); 
		}

	else
		Title = CPrintf("Select Variable for %s", GetDeviceName());

	Dlg.SetCaption(Title);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation

void CBSAPFULLTagsUDPSDeviceOptions::OnManage(CWnd *pWnd)
{
	CBSAPFULLTagsDeviceOptions::CreateNRTTags();

	CAddress Addr;

	Addr.m_Ref = 0;

	CBSAPFULLTagsUDPSDriver Driver;
	
	CBSAPFULLTagsUDPSDialog Dlg(Driver, Addr, this);

	Dlg.SetCaption(CPrintf("Variable Names for %s", GetDeviceName()));

	Dlg.SetSelect(FALSE);

	Dlg.Execute(*pWnd);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Slave Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLUDPSDeviceOptions, CBSAPFULLTagsUDPDeviceOptions);

// Constructor

CBSAPFULLUDPSDeviceOptions::CBSAPFULLUDPSDeviceOptions(void)
{
	m_fIsMaster = 0;
	}

// UI Management
void CBSAPFULLUDPSDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == TEXT("Push") ) {

			switch( m_Push ) {

				case 0:
					OnManage(pWnd);
					break;

				case 1:
					CBSAPFULLTagsDeviceOptions::OnImport(pWnd);
					break;

				case 2:
					CBSAPFULLTagsDeviceOptions::OnExport(pWnd);
					break;
				}
			}

		if( Tag.IsEmpty() ) CreateNRTTags();
		}

	CBSAPFULLTagsUDPDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CBSAPFULLUDPSDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	return CBSAPFULLTagsUDPDeviceOptions::MakeInitData(Init);
	}

// Meta Data Creation

void CBSAPFULLUDPSDeviceOptions::AddMetaData(void)
{
	CBSAPFULLTagsUDPDeviceOptions::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Tags Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLTagsSerialSDeviceOptions, CBSAPFULLTagsDeviceOptions);

// Constructor

CBSAPFULLTagsSerialSDeviceOptions::CBSAPFULLTagsSerialSDeviceOptions(void)
{
	}

// Download Support
BOOL CBSAPFULLTagsSerialSDeviceOptions::MakeInitData(CInitData &Init)
{
	return CBSAPFULLTagsDeviceOptions::MakeInitData(Init);
	}

void CBSAPFULLTagsSerialSDeviceOptions::AddMetaData(void)
{
	CBSAPFULLTagsDeviceOptions::AddMetaData();

	Meta_AddInteger(Push);
	}

// UI Management

void CBSAPFULLTagsSerialSDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	CBSAPFULLTagsDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Address Management

BOOL CBSAPFULLTagsSerialSDeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	return CBSAPFULLTagsDeviceOptions::ParseAddress(Error, Addr, Text);
	}

BOOL CBSAPFULLTagsSerialSDeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	return CBSAPFULLTagsDeviceOptions::ExpandAddress(Text, Addr);
	}

BOOL CBSAPFULLTagsSerialSDeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	return CBSAPFULLTagsDeviceOptions::ListAddress(pRoot, uItem, Data);
	}

BOOL CBSAPFULLTagsSerialSDeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CBSAPFULLTagsSerialSDriver Driver;
	
	CBSAPFULLTagsSerialSDialog Dlg(Driver, Addr, this, fPart);

	CString Title;

	if( fPart ) {

		CString Name;

		ExpandAddress(Name, Addr);

		Title = CPrintf("Select Address for Tag %s", Name); 
		}

	else
		Title = CPrintf("Select Variable for %s", GetDeviceName());

	Dlg.SetCaption(Title);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation

void CBSAPFULLTagsSerialSDeviceOptions::OnManage(CWnd *pWnd)
{
	CBSAPFULLTagsDeviceOptions::CreateNRTTags();

	CAddress Addr;

	Addr.m_Ref = 0;

	CBSAPFULLTagsSerialSDriver Driver;
	
	CBSAPFULLTagsSerialSDialog Dlg(Driver, Addr, this);

	Dlg.SetCaption(CPrintf("Variable Names for %s", GetDeviceName()));

	Dlg.SetSelect(FALSE);

	Dlg.Execute(*pWnd);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBSAPFULLSerialSDeviceOptions, CBSAPFULLTagsSerialSDeviceOptions);

// Constructor

CBSAPFULLSerialSDeviceOptions::CBSAPFULLSerialSDeviceOptions(void)
{
	m_fIsMaster = 0;
	}

// UI Management

void CBSAPFULLSerialSDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == TEXT("Push") ) {

			switch( m_Push ) {

				case 0:
					OnManage(pWnd);
					break;

				case 1:
					CBSAPFULLTagsDeviceOptions::OnImport(pWnd);
					break;

				case 2:
					CBSAPFULLTagsDeviceOptions::OnExport(pWnd);
					break;
				}
			}

		if( Tag.IsEmpty() ) CreateNRTTags();
		}

	CBSAPFULLTagsSerialSDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CBSAPFULLSerialSDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	return CBSAPFULLTagsSerialSDeviceOptions::MakeInitData(Init);
	}

// Meta Data Creation

void CBSAPFULLSerialSDeviceOptions::AddMetaData(void)
{
	CBSAPFULLTagsSerialSDeviceOptions::AddMetaData();
	}

// End of File
