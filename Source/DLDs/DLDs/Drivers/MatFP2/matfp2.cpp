#include "intern.hpp"

#include "matfp2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Driver
//

// Instantiator

INSTANTIATE(CMatFP2SerialDriver);

// Constructor

CMatFP2SerialDriver::CMatFP2SerialDriver(void)
{
	m_Ident	      = DRIVER_ID;
	}

// Destructor

CMatFP2SerialDriver::~CMatFP2SerialDriver(void)
{
	}

// Configuration

void MCALL CMatFP2SerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CMatFP2SerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CMatFP2SerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMatFP2SerialDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CMatFP2SerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS; 
	}

CCODE MCALL CMatFP2SerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// End of File
