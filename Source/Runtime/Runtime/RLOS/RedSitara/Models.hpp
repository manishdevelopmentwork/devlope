
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Models_HPP

#define INCLUDE_Models_HPP

//////////////////////////////////////////////////////////////////////////
//
// Sitara Models
//

#define MODEL_CANYON_04		31
#define MODEL_CANYON_07		32
#define MODEL_CANYON_07EQ	33
#define MODEL_CANYON_10		34
#define MODEL_CANYON_10EV	35
#define MODEL_CANYON_15		36
#define MODEL_COLORADO_04	41
#define MODEL_COLORADO_07	42
#define MODEL_COLORADO_07EQ	43
#define MODEL_COLORADO_10	44
#define MODEL_COLORADO_10EV	45
#define MODEL_MANTICORE_10	50
#define MODEL_MANTICORE_30	51
#define MODEL_MANTICORE_50	52
#define MODEL_MANTICORE_70	53

// End of File

#endif
